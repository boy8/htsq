var Cookie = {};

Cookie.setCookie = function (key, value, t) {
    if (t > 0) {
        var oDate = new Date();
        oDate.setDate(oDate.getTime() + 60 * 1000 * t);//过期时间 t分钟
        document.cookie = key + "=" + encodeURI(value) + "; expires=" + oDate.toDateString();
    }
}

Cookie.getCookie = function (key) {
    var arr1 = document.cookie.split("; ");//由于cookie是通过一个分号+空格的形式串联起来的，所以这里需要先按分号空格截断,变成[name=Jack,pwd=123456,age=22]数组类型；
    for (var i = 0; i < arr1.length; i++) {
        var arr2 = arr1[i].split("=");//通过=截断，把name=Jack截断成[name,Jack]数组；
        if (arr2[0] == key) {
            return decodeURI(arr2[1]);
        }
    }
}

Cookie.removeCookie = function (key) {
    Cookie.setCookie(key, "", -1);//把cookie设置为过期
}

$(function () {
    var menu = "<div class='left_col scroll-view'>"
        + "<div class='navbar nav_title' style='border: 0;'>"
        + "<a href='index.html' class='site_title'><i class='fa fa-paw'></i> <span>核桃省钱后台管理</span></a>"
        + "</div>"
        + "<div class='clearfix'></div>"
        + "<br/>"
        + "<!-- sidebar menu -->"
        + "<div id='sidebar-menu' class='main_menu_side hidden-print main_menu'>"
        + "<div class='menu_section'>"
        + "<ul class='nav side-menu'>"
        + "<li><a><i class='fa fa-home'></i> 常规管理 <span class='fa fa-chevron-down'></span></a>"
        + "<ul class='nav child_menu'>"
        + "<li><a href='baseConfig.html'>基础设置</a></li>"
        + "<li><a href='parameterConfig.html'>参数设置</a></li>"
        + "<li><a href='initConfig.html'>初始化设置</a></li>"
        + "</ul>"
        + "</li>"
        + "<li><a><i class='fa fa-home'></i> 订单商品管理 <span class='fa fa-chevron-down'></span></a>"
        + "<ul class='nav child_menu'>"
        + "<li><a href='orderManage.html'>三方订单</a></li>"
        + "<li><a href='ownGoodsOrderManage.html'>自营订单</a></li>"
        + "<li><a href='ownGoodsManage.html'>自营商品</a></li>"
        + "<li><a href='zuimeiGoods.html'>小美贝商品</a></li>"
        + "<li><a href='businessDaily.html'>每日爆款商品</a></li>"
        + "<li><a href='siftGoods.html'>核桃精选商品</a></li>"
        + "<li><a href='siftOrderManage.html'>核桃精选订单</a></li>"
        + "</ul>"
        + "</li>"
        + "<li>"
        + "<a href='userManage.html'><i class='fa fa-edit'></i> 用户管理 </a>"
        + "</li>"
        + "<li>"
        + "<a href='specialCode.html'><i class='fa fa-edit'></i> 特殊邀请码管理 </a>"
        + "</li>"
        + "<li>"
        + "<a href='businessManage.html'><i class='fa fa-edit'></i> 商学院 </a>"
        + "</li>"
        + "<li>"
        + "<a href='BrandNavigation.html'><i class='fa fa-edit'></i> 品牌导航 </a>"
        + "</li>"
        + "<li>"
        + "<a><i class='fa fa-desktop'></i> 广告管理 <span class='fa fa-chevron-down'></span></a>"
        + "<ul class='nav child_menu'>"
        + "<li><a href='advManage.html'>广告管理</a></li>"
        + "<li><a href='jgPush.html'>极光推送</a></li>"
        + "<li><a href='noticeManage.html'>公告管理</a></li>"
        + "<li><a href='zeroPurchase.html'>0元购商品</a></li>"
        + "</ul>"
        + "</li>"
        + "<li>"
        + "<a href='financeManage.html'><i class='fa fa-table'></i> 财务管理 </a>"
        + "</li>"
        + "<li>"
        + "<a><i class='fa fa-coffee'></i> 帮助中心 <span class='fa fa-chevron-down'></span></a>"
        + "<ul class='nav child_menu'>"
        + "<li><a href='problemType.html'>问题分类</a></li>"
        + "<li><a href='problemManage.html'>问题管理</a></li>"
        + "</ul>"
        + "</li>"
        + "<li>"
        + "<a href='channelInfo.html'><i class='fa fa-clone'></i> 渠道统计信息 </a>"
        + "</li>"
        + "<li>"
        + "<a><i class='fa fa-bar-chart-o'></i> 报表数据 <span class='fa fa-chevron-down'></span></a>"
        + "<ul class='nav child_menu'>"
        + "<li><a href='dataReportChannel.html'>渠道汇总</a></li>"
        + "<li><a href='dataReportTerrace.html'>平台汇总</a></li>"
        + "<li><a href='dataReportOrder.html'>收益汇总</a></li>"
        + "<li><a href='dataReportOwnOrder.html'>自营订单</a></li>"
        + "<li><a href='dataReportActivity.html'>活跃统计</a></li>"
        + "<li><a href='dataReportRate.html'>订单统计</a></li>"
        + "<li><a href='dataReportInviteActivity.html'>邀请赚统计</a></li>"
        + "</ul>"
        + "</li>"
        + "<li><a><i class='fa fa-home'></i> H5独立 商品以及订单 <span class='fa fa-chevron-down'></span></a>"
        + "<ul class='nav child_menu'>"
        + "<li><a href='htmlGoodsManage.html'>商品</a></li>"
        + "<li><a href='HtmlOwnGoodsOrderManage.html'>订单</a></li>"
        + "</ul>"
        + "</li>"
        // +"<li>"
        // +"<a href='AtWillUpdata.html'><i class='fa fa-edit'></i> 测试端后台数据修改功能 </a>"
        // +"</li>"
        + "</ul>"
        + "</div>"
        + "</div>"
        + "</div>"
        + "<script src='../build/js/custom.min.js'></script>";
    $("#menu").html(menu);
    $('#menu_toggle').on('click', function () {
        if ($BODY.hasClass('nav-md')) {
            $SIDEBAR_MENU.find('li.active ul').hide();
            $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
        } else {
            $SIDEBAR_MENU.find('li.active-sm ul').show();
            $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
        }
        $BODY.toggleClass('nav-md nav-sm');
        $('.dataTable').each(function () {
            $(this).dataTable().fnDraw();
        });
    });

    var username = Cookie.getCookie("qbsUsername");
    var type = Cookie.getCookie("qbsType");
    if (!username || !type) {
        window.location.href = "login.html";
    } else {
        var html = location.href.split('/');
        var name = html[html.length - 1];
        if (type == "2" && name != "channelDataInfo.html") {//应用宝
            window.location.href = "login.html";
        }
    }
    $("#userName").text(username);
});