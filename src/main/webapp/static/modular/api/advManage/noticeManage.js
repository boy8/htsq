/**
 * 公告管理
 */
var noticeManage = {
    id: "noticeManageTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    noticeManageInfoData : {}
};

/**
 * 初始化表格的列
 */
noticeManage.initColumn = function () {
    return [
        {title: '内容',field: 'title',visible: true,align: 'center',valign: 'middle',formatter: noticeManage.formatField},
        {title: '商品平台', field: 'type', visible: true, align: 'center', valign: 'middle', formatter: noticeManage.formatType},
        {title: '跳转类型',field: 'jump_type',visible: true,align: 'center',valign: 'middle',formatter: noticeManage.formatJumpType},
        {title: '跳转地址',field: 'jump_url',visible: true,align: 'center',valign: 'middle'},
        {title: '是否需要登录',field: 'is_login',visible: true,align: 'center',valign: 'middle',formatter: noticeManage.formatIsLogin},
        {title: '识别id',field: 'discern_id',visible: true,align: 'center',valign: 'middle'},
        {title: '开始日期',field: 'start_time',visible: true,align: 'center',valign: 'middle',formatter: noticeManage.formatField},
        {title: '截止日期',field: 'end_time',visible: true,align: 'center',valign: 'middle',formatter: noticeManage.formatField},
        {title: '状态',field: 'status',visible: true,align: 'center',valign: 'middle',formatter: noticeManage.formatStatus},
        {title: '创建时间',field: 'create_time',visible: true,align: 'center',valign: 'middle',formatter: noticeManage.formatField},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: noticeManage.formatOperate}
    ];
};

noticeManage.formatOperate = function (val, row, index) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='noticeManage.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='noticeManage.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

noticeManage.formatStatus = function (val, row, index) {
    switch (val) {
        case 0:
            return "禁用";
        case 1:
            return "启用";
        default:
            return "-";
    }
}

noticeManage.formatIsLogin = function (val, row, index) {
    switch (val) {
        case  1:
            return "是";
        case  0:
            return "否";
        default:
            return "-";
    }
}

noticeManage.formatType = function (val, row, index) {
    switch (val) { //'平台类型（0自有 1淘宝;2京东;3拼多多;4唯品会）',
        case 0:
            return "自有商品";
        case 1:
            return "淘宝";
        case 2:
            return "京东";
        case 3:
            return "拼多多";
        case 4:
            return "唯品会";
        default:
            return "-";
    }
}

noticeManage.formatJumpType = function (val, row, index) {
    switch (val) { //'跳转类型（1app内部;2跳转第三方; 3内部接口跳转重新获取URL 4 跳h5页面）',
        case 1:
            return "app内部";
        case 2:
            return "第三方";
        case 3:
            return "内部接口跳转重新获取URL";
        case 4:
            return "跳h5页面";
    }
}

noticeManage.formatField = function (val, row, index) {
    if (val == null || val == "" || val.length < 0) {
        return "-";
    }
    return val;
}


/**
 * 点击弹出增加公告弹框
 */
noticeManage.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加公告',
        area: ['50%', '60%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/advManage/noticeManage_add.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出修改公告弹框
 */
noticeManage.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改公告',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/advManage/noticeManage_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 清除数据
 */
noticeManage.clearData = function() {
    this.noticeManageInfoData = {};
}

/**
 * 新增广告
 */
noticeManage.addSubmit = function() {

    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    queryData['title'] = $("#title").val();
    queryData['content'] = $("#content").val();
    queryData['goods_id'] = $("#goods_id").val();
    queryData['img_url'] = $("#img_url").val();
    queryData['jump_url'] = $("#jump_url").val();

    queryData['type'] = $("#type").val();
    queryData['jump_type'] = $("#jump_type").val();
    queryData['is_login'] = $("#is_login").val();
    queryData['is_need_userid'] = $("#is_need_userid").val();
    queryData['start_time'] = $("#start_time").val();
    queryData['end_time'] = $("#end_time").val();
    queryData['status'] = status;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/notice/add", function(data){
        if(data.code == 200){
            parent.noticeManage.table.refresh();
            noticeManage.close();
            Feng.success("新增成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
noticeManage.close = function() {
    parent.layer.closeAll();
}

/**
 * 删除单个项目操作
 */
noticeManage.delete = function (id) {

    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/notice/delete/"+ id, function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                noticeManage.table.refresh();
            }
        });
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);

};

/**
 * 查询项目操作列表
 */
noticeManage.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    noticeManage.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = noticeManage.initColumn();
    var table = new BSTable(noticeManage.id, "/notice/list", defaultColunms);
    table.setPaginationType("client");
    noticeManage.table = table.init();
});
