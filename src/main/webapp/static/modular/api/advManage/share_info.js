/**
 * 初始化项目操作详情对话框
 */
var shareInfoDlg = {
    seItem : null,
    editor : null,
    zeroPurchaseInfoData : {}
};

/**
 * 关闭此对话框
 */
shareInfoDlg.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改零元购
 */
shareInfoDlg.updateSubmit = function() {
    var queryData = {};
    var id = window.parent.$("#updateId").val()
    var type = $('input[name="type"]:checked').val();
    // var content = shareInfoDlg.editor.txt.html();
    var content = $("#editor").val();
    queryData['id'] = id;
    queryData['name'] = $("#name").val();
    queryData['head_img'] = $("#head_img").val();
    queryData['content'] = content;
    queryData['picture'] = $("#picture").val();
    queryData['video'] = $("#video").val();
    queryData['type'] = type;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/share/update", function(data){
        if(data.code == 200){
            parent.share.table.refresh();
            shareInfoDlg.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/share/detail/" + id, function(data){
        if (data.data != null) {
            var type = data.data.type;
            $("#id").val(id);
            $("#goods_name").val(data.data.goods_name);
            // editor.txt.html(data.data.content);
            $("#editor").val(data.data.content);
            $("#name").val(data.data.name);
            $("#head_img").val(data.data.head_img);
            $("#picture").val(data.data.picture);
            $("#video").val(data.data.video);
            if (type == 1){
                $("input[name='type'][value='1']").attr("checked", true);
            }
            if (type == 2){
                $("input[name='type'][value='2']").attr("checked", true);
            }
            if (type == 3){
                $("input[name='type'][value='3']").attr("checked", true);
            }
        }
    });
    ajax.start();
});
