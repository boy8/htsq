/**
 * 初始化项目操作详情对话框
 */
var noticeManageInfoDlg = {
    seItem:null,
    AdvManageInfoData : {}
};

/**
 * 关闭此对话框
 */
noticeManageInfoDlg.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改公告
 */
noticeManageInfoDlg.updateSubmit = function() {

    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    queryData['id'] = $("#id").val();
    queryData['title'] = $("#title").val();
    queryData['content'] = $("#content").val();
    queryData['goods_id'] = $("#goods_id").val();
    queryData['img_url'] = $("#img_url").val();
    queryData['jump_url'] = $("#jump_url").val();

    queryData['type'] = $("#type").val();
    queryData['jump_type'] = $("#jump_type").val();
    queryData['is_login'] = $("#is_login").val();
    queryData['is_need_userid'] = $("#is_need_userid").val();
    queryData['discern_id'] = $("#discern_id").val();
    queryData['start_time'] = $("#start_time").val();
    queryData['end_time'] = $("#end_time").val();
    queryData['status'] = status;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/notice/update", function(data){
        if(data.code == 200){
            parent.noticeManage.table.refresh();
            noticeManageInfoDlg.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/notice/detail/" + id, function(data){
        if (data.data != null) {
            var status = data.data.status;
            $("#id").val(id);
            $("#title").val(data.data.title);
            $("#content").val(data.data.content);
            $("#goods_id").val(data.data.goods_id);
            $("#img_url").val(data.data.img_url);
            $("#type").val(data.data.type);
            $("#jump_type").val(data.data.jump_type);
            $("#jump_url").val(data.data.jump_url);
            $("#is_login").val(data.data.is_login);
            $("#is_need_userid").val(data.data.is_need_userid);
            $("#start_time").val(data.data.start_time);
            $("#end_time").val(data.data.end_time);
            if (status == 0){
                $("input[name='status'][value='0']").attr("checked", true);
            }else if (status == 1){
                $("input[name='status'][value='1']").attr("checked", true);
            }
        }
    });
    ajax.start();
});
