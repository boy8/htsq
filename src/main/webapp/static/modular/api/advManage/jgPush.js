/**
 * 极光推送配置
 */
var jgPush = {
    id: "jgPushTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    jgPushInfoData: {}
};

/**
 * 初始化表格的列
 */
jgPush.initColumn = function () {
    return [
        {title: '内容',field: 'title',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatField},
        // {title: '商品Id', field: 'goods_id',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatField},
        {title: '商品平台', field: 'type', visible: true, align: 'center', valign: 'middle', formatter: jgPush.formatType},
        {title: '跳转类型',field: 'jump_type',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatJumpType},
        {title: '跳转地址',field: 'jump_url',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatJumpUrl},
        {title: '推送开始时间段',field: 'startTime',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatField},
        {title: '是否需要登录',field: 'is_login',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatIsLogin},
        // {title: '是否需要拼接userid',field: 'is_need_userid',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatIsNeedUserid},
        // {title: '内部识别id',field: 'discern_id',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatDiscernId},
        {title: '推送起始日期',field: 'start_time',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatField},
        {title: '推送截止日期',field: 'end_time',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatField},
        {title: '状态',field: 'status',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatStatus},
        {title: '创建时间',field: 'create_time',visible: true,align: 'center',valign: 'middle',formatter: jgPush.formatField},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: jgPush.formatOperate}
    ];
};

jgPush.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='jgPush.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='jgPush.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

jgPush.formatField = function (val, row, index) {
    if (val == "" || val < 0 || val == null) {
        return "-";
    }
    return val;
}

jgPush.formatType = function (val, row, index) {
    switch (val) { //'平台类型（0自有 1淘宝;2京东;3拼多多;4唯品会）',
        case 0:
            return "自有商品";
        case 1:
            return "淘宝";
        case 2:
            return "京东";
        case 3:
            return "拼多多";
        case 4:
            return "唯品会";
    }
}
jgPush.formatJumpType = function (val, row, index) {
    switch (val) { //'跳转类型（1app内部;2跳转第三方; 3内部接口跳转重新获取URL 4 跳h5页面）',
        case 1:
            return "app内部";
        case 2:
            return "第三方";
        case 3:
            return "内部接口跳转重新获取URL";
        case 4:
            return "跳h5页面";
    }
}
jgPush.formatStatus = function (val, row, index) {
    switch (val) {
        case 0:
            return "禁用";
        case 1:
            return "启用";
    }
}

jgPush.formatIsLogin = function (val, row, index) {
    switch (val) {
        case  1:
            return "是";
        case  0:
            return "否";
    }
}
jgPush.formatIsNeedUserid = function (val, row, index) {
    switch (val) {
        case  1:
            return "是";
        case  0:
            return "否";
    }
}
jgPush.formatDiscernId = function (val, row, index) {
    switch (val) { //'识别id(1淘宝返利 2京东返利 3拼多多 4唯品会 5天猫返利 69块9 7每日红包 8VIP权益 9高佣精品 10邀请好友)'
        case 1:
            return "淘宝返利";
        case 2:
            return "京东返利";
        case 3:
            return "拼多多";
        case 4:
            return "唯品会";
        case 5:
            return "天猫返利";
        case 6:
            return "9块9";
        case 7:
            return "每日红包";
        case 8:
            return "VIP权益";
        case 9:
            return "高佣精品";
        case 10:
            return "邀请好友";
    }
}

/**
 * 点击弹出修改广告弹框
 */
jgPush.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改信息',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/advManage/jgPush_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出增加推送弹框
 */
jgPush.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加推送信息',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/advManage/jgPush_add.html'
    });
    this.layerIndex = index;
};

/**
 * 删除单个项目操作
 */
jgPush.delete = function (id) {
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/push/delete", function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                jgPush.table.refresh();
            }
        });
        ajax.set("id",id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 新增极光推送
 */
jgPush.addSubmit = function () {
    var queryData = {};

    queryData['title'] = $("#title").val();
    queryData['content'] = $("#content").val();
    queryData['goods_id'] = $("#goods_id").val();
    queryData['img_url'] = $("#img_url").val();
    queryData['jump_url'] = $("#jump_url").val();

    queryData['type'] = $("#type").val();
    queryData['jump_type'] = $("#jump_type").val();
    queryData['is_login'] = $("#is_login").val();
    queryData['is_need_userid'] = $("#is_need_userid").val();
    queryData['discern_id'] = $("#discern_id").val();
    queryData['start_time'] = $("#start_time").val();
    queryData['end_time'] = $("#end_time").val();
    queryData['push_type'] = $("#push_type").val();
    queryData['time'] = $("#time").val();



    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/push/pushMsg", function (data) {
        if (data.code == 200) {
            parent.jgPush.table.refresh();
            jgPush.close();
            Feng.success("推送成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
jgPush.close = function () {
    parent.layer.closeAll();
}

$(function () {
    var defaultColunms = jgPush.initColumn();
    var table = new BSTable(jgPush.id, "/push/pushList?page=1&size=100", defaultColunms);
    table.setPaginationType("client");
    jgPush.table = table.init();
});
