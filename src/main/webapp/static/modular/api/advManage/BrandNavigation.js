/**
 * 品牌导航
 */
var BrandNavigation = {
    id: "BrandNavigationTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    BrandNavigationInfoData : {},
    delIds:[]           // 批量删除
};

/**
 * 初始化表格的列
 */
BrandNavigation.initColumn = function () {
    return [
        {title: '编号', formatter: function (value, row, index) {return index+1;}, visible: true, align: 'center', valign: 'middle'},
        {title: '店铺名', field: 'store_name', visible: true, align: 'center', valign: 'middle'},
        {title: '描述', field: 'descption', visible: true, align: 'center', valign: 'middle'},
        {title: '图片地址', field: 'img_url', visible: true, align: 'center', valign: 'middle'},
        {title: '跳转链接', field: 'jump_url', visible: true, align: 'center', valign: 'middle'},
        {title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle', formatter: BrandNavigation.formatType},
        {title: '创建时间', field: 'create_time', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle', formatter: BrandNavigation.formatStatus},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: BrandNavigation.formatOperate}
    ];
};

BrandNavigation.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary' onclick='BrandNavigation.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger' onclick='BrandNavigation.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

BrandNavigation.formatStatus = function (val, row) {
    if(row.status == "0"){
        return "停用";
    }
    if(row.status == "1") {
        return "启用";
    }
       return "-";
}

BrandNavigation.formatType = function (val, row) {
    if(val == "1"){
        return "品牌";
    }
    if(val == "2") {
        return "旗舰店";
    }
       return "-";
}


/**
 * 点击弹出修改品牌导航弹框
 */
BrandNavigation.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改品牌导航',
        area: ['50%', '60%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/advManage/BrandNavigation_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出新增品牌导航弹框
 */
BrandNavigation.add = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改品牌导航',
        area: ['50%', '60%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/advManage/BrandNavigation_add.html'
    });
    this.layerIndex = index;
};

/**
 * 清除数据
 */
BrandNavigation.clearData = function() {
    this.BrandNavigationInfoData = {};
}

/**
 * 新增品牌导航
 */
BrandNavigation.addSubmit = function() {

    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    var type = $('input[name="type"]:checked').val();
    queryData['store_name'] = $("#store_name").val();
    queryData['descption'] = $("#descption").val();
    queryData['img_url'] = $("#img_url").val();
    queryData['jump_url'] = $("#jump_url").val();
    queryData['status'] = status;
    queryData['type'] = type;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/brandNavigation/insert", function(data){
        if(data.code == 200){
            parent.BrandNavigation.table.refresh();
            BrandNavigation.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
BrandNavigation.close = function() {
    parent.layer.closeAll();
}

/**
 * 删除单个项目操作
 */
BrandNavigation.delete = function (id) {
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/brandNavigation/delete", function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                BrandNavigation.table.refresh();
            }
        });
        ajax.set("id",id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);

};

/**
 * 查询项目操作列表
 */
BrandNavigation.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    BrandNavigation.table.refresh({query: queryData});
};


$(function () {
    var defaultColunms = BrandNavigation.initColumn();
    var table = new BSTable(BrandNavigation.id, "/brandNavigation/list", defaultColunms);
    table.setPaginationType("client");
    BrandNavigation.table = table.init();
});
