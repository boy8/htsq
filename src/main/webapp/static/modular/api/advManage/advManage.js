/**
 * 广告管理
 */
var advManage = {
    id: "advManageTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    advManageInfoData : {},
    delIds:[]           // 批量删除
};

/**
 * 初始化表格的列
 */
advManage.initColumn = function () {
    return [
        {title: '编号', formatter: function (value, row, index) {return index+1;}, visible: true, align: 'center', valign: 'middle'},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '位置', field: 'block', visible: true, align: 'center', valign: 'middle',formatter: advManage.formatPosition},
        {title: '跳转链接', field: 'jump_url', visible: true, align: 'center', valign: 'middle', formatter: advManage.formatJumpUrl},
        {title: '跳转类型', field: 'jump_type', visible: true, align: 'center', valign: 'middle', formatter: advManage.formatBType},
        {title: '图片地址', field: 'img_url', visible: true, align: 'center', valign: 'middle'},
        {title: '是否需要登录', field: 'is_login', visible: true, align: 'center', valign: 'middle',formatter: advManage.formatLogin},
        {title: '排序', field: 'sequence', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle', formatter: advManage.formatStatus},
        {title: '结束时间', field: 'end_time', visible: true, align: 'center', valign: 'middle'},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: advManage.formatOperate}
    ];
};

advManage.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='advManage.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='advManage.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

advManage.formatStatus = function (val, row) {
    if(row.status == "0"){
        return "停用";
    }
    if(row.status == "1") {
        return "启用";
    }
       return "-";
}
advManage.formatLogin = function (val, row) {
    if(val == "0"){
        return "否";
    }
    if(val == "1") {
        return "是";
    }
       return "-";
}
advManage.formatJumpUrl = function (val, row) {
    if(val.length < 1){
        return "-";
    }
       return val;
}
advManage.formatPosition = function (val) {
    if(val == "1"){
        return "首页banner";
    }else if(val == "2") {
        return "开屏广告";
    }else if(val == "3") {
        return "首页gif";
    }else if(val == "4") {
        return "签到动图";
    }else if(val == "5") {
        return "首页浮图";
    }else if(val == "6") {
        return "个人中心banner";
    }else if(val == "7") {
        return "活动专区";
    }else if(val == "8") {
        return "首页活动区背景图";
    }else if(val == "9") {
        return "插屏";
    }else if(val == "10") {
        return "核桃精选商品列表banner";
    }else if(val == "11") {
        return "品牌导航分类";
    }else if(val == "12") {
        return "首页中部限时抢购旁边核桃精选图";
    }
}
advManage.formatBType = function (val) {
    if(val == "1"){
        return "app内部";
    }
    if(val == "2") {
        return "跳转第三方";
    }
    if(val == "3") {
        return "内部跳转重新获取URL";
    }
    if(val == "4") {
        return "跳h5页面";
    }
       return val;
}


/**
 * 点击弹出增加广告弹框
 */
advManage.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加广告',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/advManage/advManage_add.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出修改广告弹框
 */
advManage.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改广告',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/advManage/advManage_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 清除数据
 */
advManage.clearData = function() {
    this.advManageInfoData = {};
}

/**
 * 新增广告
 */
advManage.addSubmit = function() {

    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    var is_login = $('input[name="is_login"]:checked').val();
    var is_need_userid = $('input[name="is_need_userid"]:checked').val();
    queryData['name'] = $("#name").val();
    queryData['img_url'] = $("#img_url").val();
    queryData['color'] = $("#color").val();
    queryData['jump_url'] = $("#jump_url").val();
    queryData['goods_id'] = $("#goods_id").val();
    queryData['jump_type'] = $("#jump_type").val();
    queryData['sequence'] = $("#sequence").val();
    queryData['type'] = $("#type").val();
    queryData['block'] = $("#block").val();
    queryData['end_time'] = $("#end_time").val();
    queryData['status'] = status;
    queryData['is_login'] = is_login;
    queryData['is_need_userid'] = is_need_userid;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/advManage/insert", function(data){
        if(data.code == 200){
            parent.advManage.table.refresh();
            advManage.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
advManage.close = function() {
    parent.layer.closeAll();
}

/**
 * 删除单个项目操作
 */
advManage.delete = function (id) {

    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/advManage/delete", function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                advManage.table.refresh();
            }
        });
        ajax.set("id",id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 查询项目操作列表
 */
advManage.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    advManage.table.refresh({query: queryData});
};

// 封装批量删除的ids
advManage.dealDelIds = function (selected) {
    advManage.delIds = "";
    for(var i=0;i<selected.length;i++){
        if (selected[i] != null && i != selected.length -1){
            advManage.delIds += selected[i].id+",";
        }else {
            advManage.delIds += selected[i].id;
        }
    }
};

$(function () {
    var defaultColunms = advManage.initColumn();
    var table = new BSTable(advManage.id, "/advManage/list", defaultColunms);
    table.setPaginationType("client");
    advManage.table = table.init();
});
