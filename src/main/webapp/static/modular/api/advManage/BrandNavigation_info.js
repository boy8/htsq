/**
 * 初始化项目操作详情对话框
 */
var BrandNavigationInfoFlg = {
    seItem:null,
};

/**
 * 关闭此对话框
 */
BrandNavigationInfoFlg.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改广告
 */
BrandNavigationInfoFlg.updateSubmit = function() {

    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    var type = $('input[name="type"]:checked').val();
    queryData['id'] = $("#id").val();
    queryData['store_name'] = $("#store_name").val();
    queryData['descption'] = $("#descption").val();
    queryData['img_url'] = $("#img_url").val();
    queryData['jump_url'] = $("#jump_url").val();
    queryData['status'] = status;
    queryData['type'] = type;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/brandNavigation/update", function(data){
        if(data.code == 200){
            parent.BrandNavigation.table.refresh();
            BrandNavigationInfoFlg.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/brandNavigation/detailInfo/" + id, function(data){
        if (data.data != null) {
            var status = data.data.status;
            var type = data.data.type;
            $("#id").val(id);
            $("#store_name").val(data.data.store_name);
            $("#descption").val(data.data.descption);
            $("#img_url").val(data.data.img_url);
            $("#jump_url").val(data.data.jump_url);
            if (status == 0){
                $("input[name='status'][value='0']").attr("checked", true);
            }else if (status == 1){
                $("input[name='status'][value='1']").attr("checked", true);
            }
            if (type == 1){
                $("input[name='type'][value='1']").attr("checked", true);
            }else if (type == 2){
                $("input[name='type'][value='2']").attr("checked", true);
            }
        }
    });
    ajax.start();
});
