/**
 * 分享
 */
var share = {
    id: "shareTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    shareInfoData : {},
    editor : null,
    delIds:[]           // 批量删除
};

/**
 * 初始化表格的列
 */
share.initColumn = function () {
    return [
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '头像', field: 'head_img', visible: true, align: 'center', valign: 'middle'},
        {title: '内容', field: 'content', visible: true,align: 'center', valign: 'middle'},
        {title: '图片', field: 'picture', visible: true, align: 'center', valign: 'middle', formatter: share.formatUrl},
        {title: '视频', field: 'video', visible: true, align: 'center', valign: 'middle', formatter: share.formatUrl},
        {title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle', formatter: share.formatType},
        {title: '创建时间', field: 'create_time', visible: true, align: 'center', valign: 'middle'},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: share.formatOperate}
    ];
};

share.formatOperate = function (val, row, index) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='share.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='share.deleteOne(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

share.formatType = function (val, row, index) {
    if(row.type == "1"){
        return "商品推荐";
    }
    if(row.type == "2") {
        return "营销素材";
    }
    if(row.type == "3") {
        return "新手必发";
    }
       return "-";
}

share.formatUrl = function (val, row, index) {
    if(val < 1){
        return "-";
    }
       return val;
}

/**
 * 点击弹出新增
 */
share.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加分享素材',
        area: ['52%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/advManage/share_add.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出修改分享素材
 */
share.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改分享素材',
        area: ['52%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/advManage/share_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 清除数据
 */
share.clearData = function() {
    this.shareInfoData = {};
}

/**
 * 新增分享素材
 */
share.addSubmit = function() {
    var queryData = {};
    var type = $('input[name="type"]:checked').val();
    // var content = share.editor.txt.html();
    var content = $("#editor").val();
    queryData['name'] = $("#name").val();
    queryData['head_img'] = $("#head_img").val();
    queryData['content'] = content;
    queryData['picture'] = $("#picture").val();
    queryData['video'] = $("#video").val();
    queryData['type'] = type;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/share/insert", function(data){
        if(data.code == 200){
            parent.share.table.refresh();
            share.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
share.close = function() {
    parent.layer.closeAll();
}

/**
 * 删除单个项目操作
 */
share.deleteOne = function (id) {
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/share/delete/"+id, function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                share.table.refresh();
            }
        });
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 查询项目操作列表
 */
share.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    share.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = share.initColumn();
    var table = new BSTable(share.id, "/share/list", defaultColunms);
    table.setPaginationType("client");
    share.table = table.init();
});
