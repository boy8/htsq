/**
 * 初始化项目操作详情对话框
 */
var AdvManageInfoDlg = {
    seItem:null,
    AdvManageInfoData : {}
};

/**
 * 关闭此对话框
 */
AdvManageInfoDlg.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改广告
 */
AdvManageInfoDlg.updateSubmit = function() {

    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    var is_login = $('input[name="is_login"]:checked').val();
    var is_need_userid = $('input[name="is_need_userid"]:checked').val();
    queryData['id'] = $("#id").val();
    queryData['name'] = $("#name").val();
    queryData['img_url'] = $("#img_url").val();
    queryData['color'] = $("#color").val();
    queryData['type'] = $("#type").val();
    queryData['jump_url'] = $("#jump_url").val();
    queryData['goods_id'] = $("#goods_id").val();
    queryData['jump_type'] = $("#jump_type").val();
    queryData['sequence'] = $("#sequence").val();
    queryData['end_time'] = $("#end_time").val();
    queryData['block'] = $("#block").val();
    queryData['status'] = status;
    queryData['is_login'] = is_login;
    queryData['is_need_userid'] = is_need_userid;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/advManage/update", function(data){
        if(data.code == 200){
            parent.advManage.table.refresh();
            AdvManageInfoDlg.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/advManage/advDetailInfo/" + id, function(data){
        if (data.data != null) {
            var status = data.data.status;
            var is_login = data.data.is_login;
            var is_need_userid = data.data.is_need_userid;
            $("#id").val(id);
            $("#name").val(data.data.name);
            $("#img_url").val(data.data.img_url);
            $("#color").val(data.data.color);
            $("#goods_id").val(data.data.goods_id);
            $("#jump_url").val(data.data.jump_url);
            $("#sequence").val(data.data.sequence);
            $("#type").val(data.data.type);
            $("#end_time").val(data.data.end_time);
            $("#block").val(data.data.block);
            $("#jump_type").val(data.data.jump_type);
            $("#goods_id").val(data.data.goods_id);
            if (status == 0){
                $("input[name='status'][value='0']").attr("checked", true);
            }else if (status == 1){
                $("input[name='status'][value='1']").attr("checked", true);
            }
            if (is_login == 0){
                $("input[name='is_login'][value='0']").attr("checked", true);
            }else if (is_login == 1){
                $("input[name='is_login'][value='1']").attr("checked", true);
            }
            if (is_need_userid == 0){
                $("input[name='is_need_userid'][value='0']").attr("checked", true);
            }else if (is_need_userid == 1){
                $("input[name='is_need_userid'][value='1']").attr("checked", true);
            }
        }
    });
    ajax.set(this.advManageInfoData);
    ajax.start();
});
