/**
 * 登录
 */
var Login = {
    loginData : {}
};

/**
 * 清除数据
 */
Login.clearData = function() {
    this.loginData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
Login.set = function(key, val) {
    this.loginData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
Login.get = function(key) {
    return $("#" + key).val();
}

/**
 * 收集数据
 */
Login.collectData = function() {
    // 添加自己需要新增的字段信息，与新增页面的input框的id相匹配
    this.set('username').set("pwd");
}

Login.setCookie = function(key, value, t) {
    if (t > 0) {
        var oDate = new Date();
        oDate.setDate(oDate.getTime() + 60 * 1000 * t);//过期时间 t分钟
        document.cookie = key + "=" + encodeURI(value) + "; expires=" + oDate.toDateString();
    }
}

/**
 * app登录
 */
Login.appsubmit = function() {
    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/login/login", function(data){
        if (data.code == 200){
            var user = data.data.userName;
            var type = data.data.type;
            Login.setCookie("qbsUsername",user,15);
            Login.setCookie("qbsType",type,15);
            if (type == "1"){
                window.location.href="index.html";
            }else if (type == "2"){//应用宝
                window.location.href="channelDataInfo.html";
            }
        }else {
            alert(data.message);
        }
    });
    ajax.set(this.loginData);
    ajax.start();
}

/**
 * 登录
 */
Login.submit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/login/login", function(data){
        if (data.code == 200){
            var user = data.data.userName;
            Login.setCookie("yfUsername",user,15);
            if (user == "yingyongbao"){
                window.location.href="channelDataInfo.html";
            }else {
                window.location.href="index.html";
            }
        }else {
            alert(data.message);
        }
    });
    ajax.set(this.loginData);
    ajax.start();
}

$(function() {
});
