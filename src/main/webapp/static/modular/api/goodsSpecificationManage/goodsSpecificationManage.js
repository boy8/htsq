/**
 * 自营商品管理
 */
var goodsSpecificationManage = {
    id: "goodsSpecificationManageTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
goodsSpecificationManage.initColumn = function () {
    return [
        {title: '规格分类名称', field: 'etalon_name', visible: true, align: 'center', valign: 'middle'},
        {title: '规格名称', field: 'etalon_value', visible: true, align: 'center', valign: 'middle'},
        {title: '图片路径', field: 'file_path', visible: true, align: 'center', valign: 'middle'},
        {title: '排序', field: 'sequence', visible: true, align: 'center', valign: 'middle'},
        {
            title: '状态',
            field: 'status',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: goodsSpecificationManage.formatStatus
        },
        {
            title: '操作',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: goodsSpecificationManage.formatOperate
        }
    ];
};

goodsSpecificationManage.formatStatus = function (val, row) {
    if (val == 1) {
        return "启动";
    } else if (val == 0) {
        return "禁用";
    } else {
        return "-";
    }
}

goodsSpecificationManage.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='goodsSpecificationManage.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='goodsSpecificationManage.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

//会员等级(0普通 1超级会员 2运营商 3联创)
goodsSpecificationManage.formatState = function (val) {
    if (val == 0) {
        return "停止";
    } else if (val == 1) {
        return "启用";
    }
    return "-";
}

/**
 * 删除单个项目操作
 */
goodsSpecificationManage.delete = function (id) {
    var operation = function () {
        var ajax = new $ax(Feng.ctxPath + "/goodsSpecificationBase/delete", function (data) {
            if (data.code == 200) {
                Feng.success("删除成功!");
                goodsSpecificationManage.table.refresh();
            }
        });
        ajax.set("id", id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 点击弹出增加商品弹框
 */
goodsSpecificationManage.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/goodsSpecificationManage/goodsSpecification_add.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出修改商品弹窗
 */
goodsSpecificationManage.update = function (id) {
    $("#goodsSpecifcationId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改商品信息',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/goodsSpecificationManage/goodsSpecification_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
goodsSpecificationManage.formParams = function () {
    var queryData = {};
    queryData['title'] = $("#title").val();
    queryData['state'] = $("#state").val();

    return queryData;
}

/**
 * 关闭此对话框
 */
goodsSpecificationManage.close = function () {
    parent.layer.closeAll();
}

/**
 * 新增广告
 */
goodsSpecificationManage.addSubmit = function () {
    var queryData = {};
    var state = $('input[name="state"]:checked').val();
    var id = window.parent.$("#updateId").val();
    queryData['etalon_name'] = $("#etalon_name").val();
    queryData['etalon_value'] = $("#etalon_value").val();
    queryData['sequence'] = $("#sequence").val();
    queryData['file_path'] = $("#file_path").val();
    queryData['state'] = state;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/goodsSpecificationBase/addSpecifcation", function (data) {
        if (data.code == 200) {
            parent.goodsSpecificationManage.table.refresh();
            goodsSpecificationManage.close();
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 查询列表
 */
goodsSpecificationManage.search = function () {
    goodsSpecificationManage.table.refresh({query: goodsSpecificationManage.formParams()});
};

$(function () {
    var id = window.parent.$("#updateId").val();
    $("#goodsDetailId").val(id);
    var defaultColunms = goodsSpecificationManage.initColumn();
    var table = new BSTable(goodsSpecificationManage.id, "/goodsSpecificationBase/selectSpecifcation?id=" + id, defaultColunms);
    table.setPaginationType("client");
    goodsSpecificationManage.table = table.init();
});
