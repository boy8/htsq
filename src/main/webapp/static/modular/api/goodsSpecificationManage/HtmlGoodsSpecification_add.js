/**
 * 自营商品管理
 */
var HtmlGoodsSpecificationAdd = {
    id: "HtmlGoodsSpecificationAddTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 关闭此对话框
 */
HtmlGoodsSpecificationAdd.close = function () {
    parent.layer.closeAll();
}

/**
 * 新增广告
 */
HtmlGoodsSpecificationAdd.addSubmit = function () {
    var queryData = {};
    var state = $('input[name="status"]:checked').val();
    var id = window.parent.$("#goodsDetailId").val();
    queryData['etalon_name'] = $("#etalon_name").val();
    queryData['etalon_value'] = $("#etalon_value").val();
    queryData['sequence'] = $("#sequence").val();
    queryData['file_path'] = $("#file_path").val();
    queryData['file_path'] = $("#file_path").val();
    queryData['state'] = state;
    queryData['goods_id'] = id;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/htmlOrderBase/addSpecifcation", function (data) {
        if (data.code == 200) {
            HtmlGoodsSpecificationAdd.close();
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}
