/**
 * 初始化项目操作详情对话框
 */
var HtmlOwnGoodsInfoDlg = {
    seItem : null,
    editor : null,
    zeroPurchaseInfoData : {}
};

/**
 * 关闭此对话框
 */
HtmlOwnGoodsInfoDlg.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改商品信息
 */
HtmlOwnGoodsInfoDlg.updateSubmit = function() {
    var queryData = {};
    var id = window.parent.$("#updateId").val()
    var state = $('input[name="state"]:checked').val();
    queryData['dealer_id'] = id;
    queryData['title'] = $("#title").val();
    queryData['describe'] = $("#describe").val();
    queryData['former_price'] = $("#former_price").val();
    queryData['coupons_price'] = $("#coupons_price").val();
    queryData['now_price'] = $("#now_price").val();
    queryData['inventory'] = $("#inventory").val();
    queryData['video'] = $("#video").val();
    queryData['show_images'] = $("#show_images").val();
    queryData['carousel_imags'] = $("#carousel_imags").val();
    queryData['goods_units'] = $("#goods_units").val();
    queryData['state'] = state;
    queryData['type'] = $("#type").val();
    queryData['sequence'] = $("#sequence").val();
    queryData['postage'] = $("#postage").val();
    queryData['restrict'] = $("#restrict").val();




    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/htmlOrderBase/update", function(data){
        if(data.code == 200){
            parent.HtmlOwnGoodsManage.table.refresh();
            HtmlOwnGoodsInfoDlg.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/htmlOrderBase/detail/" + id, function(data){
        if (data.data != null) {
            var state = data.data.state;
            $("#id").val(id);
            $("#title").val(data.data.title);
            $("#describe").val(data.data.describe);
            $("#former_price").val(data.data.former_price);
            $("#coupons_price").val(data.data.coupons_price);
            $("#now_price").val(data.data.now_price);
            $("#inventory").val(data.data.inventory);
            $("#video").val(data.data.video);
            $("#show_images").val(data.data.show_images);
            $("#carousel_imags").val(data.data.carousel_imags);
            $("#goods_units").val(data.data.goods_units);
            $("#type").val(data.data.type);
            $("#sequence").val(data.data.sequence);
            $("#postage").val(data.data.postage);
            $("#restrict").val(data.data.restrict);
            if (state == 0){
                $("input[name='state'][value='0']").attr("checked", true);
            }else if (state == 1){
                $("input[name='state'][value='1']").attr("checked", true);
            }
        }
    });
    ajax.start();
});
