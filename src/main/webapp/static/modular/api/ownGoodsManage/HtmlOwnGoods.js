/**
 * 自营商品管理
 */
var HtmlOwnGoodsManage = {
    id: "HtmlOwnGoodsManageTable",	//表格id
    table: null,
    layerIndex: -1,
};
//
/**
 * 初始化表格的列
 */
HtmlOwnGoodsManage.initColumn = function () {
    return [
        {title: '商品id', field: 'dealer_id', visible: true, align: 'center', valign: 'middle'},
        {title: '商品标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: '价格', field: 'former_price', visible: true, align: 'center', valign: 'middle'},
        {title: '库存', field: 'inventory', visible: true, align: 'center', valign: 'middle'},
        {title: '售卖', field: 'sell', visible: true, align: 'center', valign: 'middle'},
        {
            title: '状态',
            field: 'state',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: HtmlOwnGoodsManage.formatState
        },
        {
            title: '分类',
            field: 'type',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: HtmlOwnGoodsManage.formattype
        },
        {title: '排序号', field: 'sequence', visible: true, align: 'center', valign: 'middle'},
        {title: '单位', field: 'goods_units', visible: true, align: 'center', valign: 'middle'},
        {
            title: '操作',
            field: 'id',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: HtmlOwnGoodsManage.formatOperate
        }
    ];
};

HtmlOwnGoodsManage.formatOperate = function (val, row) {
    var id = row.dealer_id;
    var btn_edit = "<button class='btn btn-primary'   onclick='HtmlOwnGoodsManage.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='HtmlOwnGoodsManage.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    var btn_specif = "<button class='btn btn-specif'   onclick='HtmlOwnGoodsManage.specif(" + id + ")'><i class='fa fa-trash-specif'></i> 设置规格</button>";

    return btn_edit + btn_del + btn_specif;
}

HtmlOwnGoodsManage.formattype = function (val) {
    if (val == 1) {
        return "正在热卖";
    } else if (val == 2) {
        return "热销爆款";
    } else if (val == 3) {
        return "即将上市";
    } else {
        return "未知类型";
    }
}

//会员等级(0普通 1超级会员 2运营商 3联创)
HtmlOwnGoodsManage.formatState = function (val) {
    if (val == 0) {
        return "停止";
    } else if (val == 1) {
        return "启用";
    }
    return "-";
}

/**
 * 删除单个项目操作
 */
HtmlOwnGoodsManage.delete = function (id) {
    var operation = function () {
        var ajax = new $ax(Feng.ctxPath + "/htmlOrderBase/delete", function (data) {
            if (data.code == 200) {
                Feng.success("删除成功!");
                HtmlOwnGoodsManage.table.refresh();
            }
        });
        ajax.set("id", id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 点击弹出增加商品弹框
 */
HtmlOwnGoodsManage.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/ownGoods/HtmlOwnGoods_add.html'
    });
    this.layerIndex = index;
};


/**
 * 点击弹出增加商品弹框
 */
HtmlOwnGoodsManage.specif = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '设置规格',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/app/production/HtmlGoodsSpecificationManage.html'
    });
    this.layerIndex = index;
};


/**
 * 点击弹出修改商品弹窗
 */
HtmlOwnGoodsManage.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改商品信息',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/ownGoods/HtmlOwnGoods_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
HtmlOwnGoodsManage.formParams = function () {
    var queryData = {};
    queryData['title'] = $("#title").val();
    queryData['state'] = $("#state").val();

    return queryData;
}

/**
 * 关闭此对话框
 */
HtmlOwnGoodsManage.close = function () {
    parent.layer.closeAll();
}

/**
 * 新增广告
 */
HtmlOwnGoodsManage.addSubmit = function () {
    var queryData = {};
    var state = $('input[name="state"]:checked').val();
    queryData['title'] = $("#title").val();
    queryData['describe'] = $("#describe").val();
    queryData['former_price'] = $("#former_price").val();
    queryData['coupons_price'] = $("#coupons_price").val();
    queryData['now_price'] = $("#now_price").val();
    queryData['inventory'] = $("#inventory").val();
    queryData['video'] = $("#video").val();
    queryData['show_images'] = $("#show_images").val();
    queryData['carousel_imags'] = $("#carousel_imags").val();
    queryData['goods_units'] = $("#goods_units").val();
    queryData['state'] = state;
    queryData['type'] = $("#type").val();
    queryData['sequence'] = $("#sequence").val();
    queryData['postage'] = $("#postage").val();
    queryData['restrict'] = $("#restrict").val();



    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/htmlOrderBase/insertOwnGoods", function (data) {
        if (data.code == 200) {
            parent.HtmlOwnGoodsManage.table.refresh();
            HtmlOwnGoodsManage.close();
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 查询列表
 */
HtmlOwnGoodsManage.search = function () {
    HtmlOwnGoodsManage.table.refresh({query: HtmlOwnGoodsManage.formParams()});
};

$(function () {
    var defaultColunms = HtmlOwnGoodsManage.initColumn();
    var table = new BSTable(HtmlOwnGoodsManage.id, "/htmlOrderBase/selectAll", defaultColunms);
    table.setPaginationType("client");
    HtmlOwnGoodsManage.table = table.init();
});
