/**
 * 渠道信息统计
 */
var channelInfoManager = {
    id: "channelInfoTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
channelInfoManager.initColumn = function () {
    return [
        {title: '渠道ID', field: 'channel_id', align: 'center', valign: 'middle', visible: true},
        {title: '注册用户数', field: 'userNum',visible: true,align: 'center', valign: 'middle'},
        {title: '下单人数', field: 'orderPeopleNum',visible: true,align: 'center', valign: 'middle'},
        {title: '订单数量', field: 'orderNum',visible: true,align: 'center', valign: 'middle'},
        {title: '用户总活跃数', field: 'userActiveNum', visible: true,align: 'center', valign: 'middle'},
        {title: '新增用户数', field: 'newlyUserNum',visible: true,align: 'center', valign: 'middle'},
        {title: '时间', field: 'time',visible: true, align: 'center', valign: 'middle', formatter: channelInfoManager.formatTime},
    ];
};

channelInfoManager.formatTime = function (val, row, index) {
    if(val.length < 1){
        return "-";
    }
    return val;
}

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
channelInfoManager.formParams = function(type) {
    var queryData = {};

    queryData['channel_id'] = $("#channel_id").val();
    queryData['app_name'] = $("#app_name").val();

    if (type == 0){
        queryData['startTime'] = $("#startTime").val();
        queryData['endTime'] = $("#endTime").val();
    }else if (type == -1) {//今天
        var curDate = new Date();
        var nowDate = curDate.format("Y-m-d");
        queryData['startTime'] = nowDate + " 00:00:00";
        queryData['endTime'] = nowDate + " 23:59:59";
    }else if (type == 1){//昨天
        var curDate = new Date();
        curDate.setDate(curDate.getDate() - 1);
        var nowDate = curDate.format("Y-m-d");
        queryData['startTime'] = nowDate + " 00:00:00";
        queryData['endTime'] = nowDate + " 23:59:59";
    }else if (type == 2){//本月
        var thisMonth = dataReportChannel.getCurrentMonth();
        queryData['startTime'] = thisMonth[0];
        queryData['endTime'] = thisMonth[1];
    }else if (type == 3){//上月
        var thisMonth = dataReportChannel.getPreviousMonth();
        queryData['startTime'] = thisMonth[0];
        queryData['endTime'] = thisMonth[1];
    }
    return queryData;
}

/**
 * 查询列表
 */
channelInfoManager.search = function (type) {
    channelInfoManager.table.refresh({query: channelInfoManager.formParams(type)});
};

$(function () {
    var defaultColunms = channelInfoManager.initColumn();
    var table = new BSTable(channelInfoManager.id, "/channel/list", defaultColunms);
    table.setPaginationType("client");
    channelInfoManager.table = table.init();
});
