/**
 * 最美商品
 */
var zuimeiGoods = {
    id: "zuimeiGoodsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    zuimeiGoodsInfoData : {},
    delIds:[]           // 批量删除
};

/**
 * 初始化表格的列
 */
zuimeiGoods.initColumn = function () {
    return [
        {title: '商品名称', field: 'goods_name', width:'10%', visible: true, align: 'center', valign: 'middle'},
        {title: '商品ID', field: 'goods_id', visible: true, align: 'center', valign: 'middle'},
        {title: '商品描述', field: 'itemdesc', width:'13%', visible: true,align: 'center', valign: 'middle'},
        {title: '图片地址', field: 'pic', width:'13%', visible: true, align: 'center', valign: 'middle'},
        {title: '价格', field: 'price', visible: true, align: 'center', valign: 'middle'},
        {title: '销量', field: 'sales', visible: true, align: 'center', valign: 'middle'},
        {title: '是否有券', field: 'ifcoupon', visible: true, align: 'center', valign: 'middle', formatter: zuimeiGoods.formatIfcoupon},
        {title: '券开始时间', field: 'coupon_startTime', visible: true, align: 'center', valign: 'middle'},
        {title: '券结束时间', field: 'coupon_endTime', visible: true, align: 'center', valign: 'middle'},
        {title: '券价格', field: 'price_coupons', visible: true, align: 'center', valign: 'middle'},
        {title: '券后价', field: 'price_after_coupons', visible: true, align: 'center', valign: 'middle'},
        {title: '购买返', field: 'anti_growth', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'create_time', visible: true, align: 'center', valign: 'middle'},
        {title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle', formatter: zuimeiGoods.formatType},
        {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle', formatter: zuimeiGoods.formatStatus},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: zuimeiGoods.formatOperate}
    ];
};

zuimeiGoods.formatOperate = function (val, row, index) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='zuimeiGoods.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='zuimeiGoods.deleteOne(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

zuimeiGoods.formatStatus = function (val, row, index) {
    if(row.status == "0"){
        return "不显示";
    }
    if(row.status == "1") {
        return "显示";
    }
       return "-";
}

zuimeiGoods.formatIfcoupon = function (val, row, index) {
    if(val == "0"){
        return "否";
    }
    if(val == "1") {
        return "是";
    }
       return "-";
}

zuimeiGoods.formatType = function (val, row, index) {
    if(row.type == "1"){
        return "淘宝";
    }
    if(row.type == "2") {
        return "京东";
    }
    if(row.type == "3") {
        return "拼多多";
    }
    if(row.type == "4") {
        return "唯品会";
    }
       return "-";
}

/**
 * 点击弹出增加最美商品弹框
 */
zuimeiGoods.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加小美贝商品',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/zuimeiGoods/zuimeiGoods_add.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出修改最美商品
 */
zuimeiGoods.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改小美贝商品',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/zuimeiGoods/zuimeiGoods_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 清除数据
 */
zuimeiGoods.clearData = function() {
    this.zuimeiGoodsInfoData = {};
}

/**
 * 新增最美商品
 */
zuimeiGoods.addSubmit = function() {
    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    var ifcoupon = $('input[name="ifcoupon"]:checked').val();
    var type = $('input[name="type"]:checked').val();
    var coupon_startTime = $("#coupon_startTime").val();
    var coupon_endTime = $("#coupon_endTime").val();
    queryData['goods_name'] = $("#goods_name").val();
    queryData['goods_id'] = $("#goods_id").val();
    queryData['pic'] = $("#pic").val();
    queryData['itemdesc'] = $("#itemdesc").val();
    queryData['shopname'] = $("#shopname").val();
    queryData['price'] = $("#price").val();
    queryData['price_coupons'] = $("#price_coupons").val();
    queryData['price_after_coupons'] = $("#price_after_coupons").val();
    queryData['anti_growth'] = $("#anti_growth").val();
    if (coupon_startTime.length > 0 && coupon_endTime.length >0){
        queryData['coupon_startTime'] = coupon_startTime;
        queryData['coupon_endTime'] = coupon_endTime;
    }
    queryData['sales'] = $("#sales").val();
    queryData['status'] = status;
    queryData['type'] = type;
    queryData['ifcoupon'] = ifcoupon;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/zuimei/insert", function(data){
        if(data.code == 200){
            parent.zuimeiGoods.table.refresh();
            zuimeiGoods.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
zuimeiGoods.close = function() {
    parent.layer.closeAll();
}

/**
 * 删除单个项目操作
 */
zuimeiGoods.deleteOne = function (id) {
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/zuimei/delete/"+id, function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                zuimeiGoods.table.refresh();
            }
        });
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 查询项目操作列表
 */
zuimeiGoods.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    zuimeiGoods.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = zuimeiGoods.initColumn();
    var table = new BSTable(zuimeiGoods.id, "/zuimei/list", defaultColunms);
    table.setPaginationType("client");
    zuimeiGoods.table = table.init();
});
