/**
 * 初始化项目操作详情对话框
 */
var zuimeiGoodsInfoDlg = {
    seItem:null,
    zeroPurchaseInfoData : {}
};

/**
 * 关闭此对话框
 */
zuimeiGoodsInfoDlg.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改零元购
 */
zuimeiGoodsInfoDlg.updateSubmit = function() {
    var queryData = {};
    var id = window.parent.$("#updateId").val()
    var status = $('input[name="status"]:checked').val();
    var type = $('input[name="type"]:checked').val();
    var ifcoupon = $('input[name="ifcoupon"]:checked').val();
    queryData['id'] = id;
    queryData['goods_name'] = $("#goods_name").val();
    queryData['goods_id'] = $("#goods_id").val();
    queryData['pic'] = $("#pic").val();
    queryData['itemdesc'] = $("#itemdesc").val();
    queryData['shopname'] = $("#shopname").val();
    queryData['price'] = $("#price").val();
    queryData['price_coupons'] = $("#price_coupons").val();
    queryData['price_after_coupons'] = $("#price_after_coupons").val();
    queryData['anti_growth'] = $("#anti_growth").val();
    queryData['coupon_startTime'] = $("#coupon_startTime").val();
    queryData['coupon_endTime'] = $("#coupon_endTime").val();
    queryData['sales'] = $("#sales").val();
    queryData['status'] = status;
    queryData['type'] = type;
    queryData['ifcoupon'] = ifcoupon;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/zuimei/update", function(data){
        if(data.code == 200){
            parent.zuimeiGoods.table.refresh();
            zuimeiGoodsInfoDlg.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $axRest(Feng.ctxPath + "/zuimei/detail","get", function(data){
        if (data.data != null) {
            var status = data.data.status;
            var type = data.data.type;
            var ifcoupon = data.data.ifcoupon;
            $("#id").val(id);
            $("#goods_name").val(data.data.goods_name);
            $("#goods_id").val(data.data.goods_id);
            $("#pic").val(data.data.pic);
            $("#itemdesc").val(data.data.itemdesc);
            $("#shopname").val(data.data.shopname);
            $("#price").val(data.data.price);
            $("#price_coupons").val(data.data.price_coupons);
            $("#price_after_coupons").val(data.data.price_after_coupons);
            $("#anti_growth").val(data.data.anti_growth);
            $("#coupon_startTime").val(data.data.coupon_startTime);
            $("#coupon_endTime").val(data.data.coupon_endTime);
            $("#sales").val(data.data.sales);
            if (status == 0){
                $("input[name='status'][value='0']").attr("checked", true);
            }
            if (status == 1){
                $("input[name='status'][value='1']").attr("checked", true);
            }
            if (ifcoupon == 0){
                $("input[name='ifcoupon'][value='0']").attr("checked", true);
            }
            if (ifcoupon == 1){
                $("input[name='ifcoupon'][value='1']").attr("checked", true);
            }
            if (type == 1){
                $("input[name='type'][value='1']").attr("checked", true);
            }
            if (type == 2){
                $("input[name='type'][value='2']").attr("checked", true);
            }
            if (type == 3){
                $("input[name='type'][value='3']").attr("checked", true);
            }
            if (type == 4){
                $("input[name='type'][value='4']").attr("checked", true);
            }
        }
    });
    ajax.set("id",id);
    ajax.start();
});
