/**
 * 用户管理
 */
var userManager = {
    id: "userManagerTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
userManager.initColumn = function () {
    return [
        {title: 'ID', field: 'id', visible: true, align: 'center',valign: 'middle'},
        {title: '昵称', field: 'nick_name', visible: true, align: 'center',valign: 'middle'},
        {title: '邀请码', field: 'friend_code',visible: true, align: 'center',valign: 'middle'},
        {title: '推荐者', field: 'introducer', visible: true, align: 'center',valign: 'middle'},
        {title: '推荐者码', field: 'introducer_code', visible: true, align: 'center',valign: 'middle'},
        {title: '会员等级', field: 'level',visible: true, align: 'center',valign: 'middle',formatter: userManager.formatLevel},
        {title: '电话', field: 'phone', visible: true, align: 'center',valign: 'middle'},
        {title: '微信号', field: 'wx_num', visible: true, align: 'center',valign: 'middle'},
        {title: '普通佣金预估', field: 'owner_estimate',visible: true, align: 'center', valign: 'middle'},
        {title: '累计普通佣金', field: 'estimate',visible: true, align: 'center', valign: 'middle'},
        {title: '奖励金', field: 'bounty_accumulated',visible: true, align: 'center', valign: 'middle'},
        {title: '活动金', field: 'activity_accumulated',visible: true, align: 'center', valign: 'middle'},
        {title: '粉丝预估', field: 'fans_estimate',visible: true, align: 'center', valign: 'middle'},
        {title: '直粉', field: 'direct_fans',visible: true, align: 'center', valign: 'middle'},
        {title: '间粉', field: 'indirect_fans',visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'status',visible: true, align: 'center', valign: 'middle', formatter: userManager.formatStatus},
        {title: '会员到期时间', field: 'lv_expire_time',visible: true, valign: 'middle', align: 'center', formatter: userManager.formatTime},
        {title: '注册时间', field: 'create_time',visible: true, align: 'center', valign: 'middle'},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: userManager.formatOperate}
    ];
};

userManager.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='userManager.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    return btn_edit;
}

//会员等级(0普通 1超级会员 2运营商 3联创)
userManager.formatLevel = function(val) {
    if (val == 0){
        return "普通";
    }else if (val == 1){
        return "超级会员";
    }else if (val == 2){
        return "运营商";
    }else if (val == 3){
        return "联创";
    }
    return "-";
}

userManager.formatStatus = function(val) {
    if (val == 1){
        return "启用";
    }else if (val == 2){
        return "未激活";
    }else if (val == 0){
        return "冻结";
    }
    return "-";
}

userManager.formatTime = function(val) {
    if (null == val || val.length < 1){
        return "-";
    }
    return val;
}

/**
 * 点击弹出修改用户弹窗
 */
userManager.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改用户会员等级',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/userManage/user_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
userManager.formParams = function() {
    var queryData = {};
    queryData['phone'] = $("#phone").val();
    queryData['friend_code'] = $("#friend_code").val();
    queryData['introducer'] = $("#introducer").val();
    queryData['wx_num'] = $("#wx_num").val();
    queryData['level'] = $("#level").val();
    queryData['status'] = $("#status").val();

    return queryData;
}

/**
 * 查询列表
 */
userManager.search = function () {
    userManager.table.refresh({query: userManager.formParams()});
};

$(function () {
    var defaultColunms = userManager.initColumn();
    var table = new BSTablePG(userManager.id, "/userManage/list", defaultColunms);
    table.setPaginationType("server");
    userManager.table = table.init();
});
