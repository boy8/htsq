/**
 * 初始化项目操作详情对话框
 */
var userInfoDlg = {
    seItem : null,
    editor : null,
    zeroPurchaseInfoData : {}
};

/**
 * 关闭此对话框
 */
userInfoDlg.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改用户等级
 */
userInfoDlg.updateSubmit = function() {
    var queryData = {};
    var id = window.parent.$("#updateId").val()
    queryData['user_id'] = id;
    queryData['level'] = $("#level").val();
    queryData['status'] = $("#status").val();
    queryData['alipay_user_name'] = $("#alipay_user_name").val();
    queryData['alipay_account'] = $("#alipay_account").val();
    queryData['lv_expire_time'] = $("#lv_expire_time").val();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/userManage/update", function(data){
        if(data.code == 200){
            parent.userManager.table.refresh();
            userInfoDlg.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/userManage/detail/" + id, function(data){
        if (data.data != null) {
            $("#user_id").val(data.data.id);
            $("#nick_name").val(data.data.nick_name);
            $("#level").val(data.data.level);
            $("#status").val(data.data.status);
            $("#alipay_user_name").val(data.data.alipay_user_name);
            $("#alipay_account").val(data.data.alipay_account);
            $("#lv_expire_time").val(data.data.lv_expire_time);
        }
    });
    ajax.start();
});
