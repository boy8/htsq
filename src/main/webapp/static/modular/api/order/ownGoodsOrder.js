/**
 * 自营订单管理
 */
var ownGoodsOrderManager = {
    id: "ownGoodsOrderManagerTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
ownGoodsOrderManager.initColumn = function () {
    return [
        {title: '订单编号', field: 'order_id', visible: true, align: 'center', valign: 'middle'},
        {
            title: '商品名称',
            field: 'product_name',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: ownGoodsOrderManager.formatProduct_name
        },
        {
            title: '付款',
            field: 'actual_amount',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: ownGoodsOrderManager.formatColor
        },
        {
            title: '手机号',
            field: 'phone',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: ownGoodsOrderManager.formatFiled
        },
        {
            title: '订单状态',
            field: 'status',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: ownGoodsOrderManager.formatStatus
        },
        {
            title: '支付方式',
            field: 'paytype',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: ownGoodsOrderManager.formatPaytype
        },
        {title: '物流单号', field: 'logistics_num', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'create_time', visible: true, align: 'center', valign: 'middle'},
        {
            title: '操作',
            field: 'id',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: ownGoodsOrderManager.formatOperate
        }
    ];
};

ownGoodsOrderManager.formatProduct_name = function (val, row, index) {
    var name = val;
    if (val == null || val.length > 60) {
        name = val.substr(0, 60) + "...";
    }
    return name;
}

ownGoodsOrderManager.formatColor = function (val, row, index) {
    return '<span style="color:red">' + val + '</span>';
}

ownGoodsOrderManager.formatFiled = function (val) {
    if (val == null || val.length < 1) {
        return "-";
    }
    return val;
}

ownGoodsOrderManager.formatStatus = function (val) {
    if (val == 0) {
        return "未支付";
    } else if (val == 1) {
        return "已完成";
    } else if (val == 2) {
        return "待发货";
    } else if (val == 3) {
        return "待收货";
    }
    return val;
}
ownGoodsOrderManager.formatPaytype = function (val) {
    if (val == 1) {
        return "支付宝";
    } else if (val == 2) {
        return "微信";
    }
    return val;
}

ownGoodsOrderManager.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary' onclick='ownGoodsOrderManager.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    return btn_edit;

}

/**
 * 点击弹出修改用户弹窗
 */
ownGoodsOrderManager.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改商品状态',
        area: ['45%', '60%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/order/ownGoodsOrder_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
ownGoodsOrderManager.formParams = function () {
    var queryData = {};
    queryData['start_time'] = $("#start_time").val();
    queryData['end_time'] = $("#end_time").val();
    queryData['status'] = $("#status").val();
    queryData['alipay_account'] = $("#alipay_account").val();
    queryData['order_id'] = $("#order_id").val();
    queryData['product_name'] = $("#product_name").val();

    return queryData;
}

/**
 * 查询列表
 */
ownGoodsOrderManager.excelExport = function () {
    var startTime = $("#start_time").val();
    var endTime = $("#end_time").val();
    var status = $("#status").val();
    var order_id = $("#order_id").val();
    var product_name = $("#product_name").val();
    var workProcessTime = $("#workProcessTime option:selected").val();
    var url = Feng.ctxPath + "/ownGoods/excelExport?start_time=" + startTime + "&end_time=" +
        endTime + "&status=" + status + "&order_id=" + order_id + "&product_name=" + product_name;
    $("#excelExport").attr("href", url);
};

/**
 * 查询列表
 */
ownGoodsOrderManager.search = function () {
    ownGoodsOrderManager.table.refresh({query: ownGoodsOrderManager.formParams()});
};

$(function () {
    var defaultColunms = ownGoodsOrderManager.initColumn();
    var table = new BSTable(ownGoodsOrderManager.id, "/ownGoods/orderList", defaultColunms);
    table.setPaginationType("client");
    ownGoodsOrderManager.table = table.init();
});
