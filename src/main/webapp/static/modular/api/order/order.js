/**
 * 订单管理
 */
var orderManager = {
    id: "orderManagerTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
orderManager.initColumn = function () {
    return [
        // {title: 'ID', field: 'userid', visible: true, align: 'center',valign: 'middle',formatter: orderManager.formatFiled},
        {title: '订单编号', field: 'order_id', visible: true, align: 'center',valign: 'middle'},
        {title: '商品名称', field: 'product_name', visible: true, align: 'center',valign: 'middle',width:'33%',formatter: orderManager.formatProduct_name},
        {title: '订单类型', field: 'type',visible: true, align: 'center',valign: 'middle'},
        {title: '实际付款', field: 'actual_amount', visible: true, align: 'center',valign: 'middle',formatter: orderManager.formatColor},
        {title: '电话号码', field: 'phone',visible: true, align: 'center',valign: 'middle',formatter: orderManager.formatFiled},
        {title: '订单佣金', field: 'rake_back', visible: true, align: 'center',valign: 'middle',formatter:orderManager.formatColor},
        {title: '用户预估返利', field: 'user_rake_back_yugu', visible: true, align: 'center',valign: 'middle'},
        {title: '购买类型', field: 'buytype',visible: true, valign: 'middle', align: 'center',formatter: orderManager.formatBuytype},
        {title: '结算时间', field: 'clear_time',visible: true, valign: 'middle', align: 'center',formatter: orderManager.formatClear_time},
        {title: '创建时间', field: 'create_time',visible: true, align: 'center', valign: 'middle'},
        {title: '订单状态', field: 'status', visible: true, align: 'center',valign: 'middle'},
    ];
};

orderManager.formatProduct_name = function(val, row, index) {
    var name = val;
    if (val.length > 60){
        name = val.substr(0,60) + "...";
    }
    if (name.length > 0 && name.indexOf("<") != -1){
        var rename = name.replace(/</g,"《").replace(/>/g,"》");
        return rename
    }
    return name;
}

orderManager.formatColor = function(val, row, index) {
    return '<span style="color:red">'+val+'</span>';
}

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
orderManager.formParams = function(type) {
    var queryData = {};

    queryData['type'] = $("#type").val();
    queryData['startTime'] = $("#startTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['status'] = $("#status").val();
    queryData['orderId'] = $("#orderId").val();
    queryData['productName'] = $("#productName").val();
    queryData['nick_name'] = $("#nick_name").val();
    queryData['phone'] = $("#phone").val();
    queryData['app_name'] = $("#app_name").val();
    queryData['buytype'] = $("#buytype").val();
    queryData['serach_type'] = type;//1小美贝

    return queryData;
}

orderManager.formatClear_time = function(val, row) {
    if (row.clear_time.length < 1){
        return "-";
    }
    return row.clear_time;
}

orderManager.formatApp_packageName = function(val, row) {
    if (val.length < 1){
        return "核桃省钱";
    }
    return val;
}

orderManager.formatFiled = function(val) {
    if (val.length < 1){
        return "-";
    }
    return val;
}
orderManager.formatBuytype = function(val, row, index) {
    if (val == 0){
        return "自购";
    }
    if (val == 1){
        return "分享购";
    }
    return "-";
}

/**
 * 查询列表
 */
orderManager.search = function (type) {
    orderManager.table.refresh({query: orderManager.formParams(type)});
};

/**
 * 淘宝上传弹窗
 */
$('.uporderTB').click(function () {
    //页面层
    layer.open({
        type: 1,
        skin: 'layui-layer-rim', //加上边框
        area: ['520px', '140px'], //宽高
        content: '<div class="box-body"><form id="formTB" enctype="multipart/form-data">\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="col-sm-3 control-label">更新淘宝订单</label>\n' +
        '                            <div class="col-sm-7" style="overflow: hidden">\n' +
        '                                <input class="inputBg" type="file" name="file"/>格式为xls\n\n' +
        '                            </div>\n' +
        '                  <span onclick="orderManager.importTBExcel()" class="btn btn-danger uporderTB" >提交</span>      </div>\n' +
        '                    </form></div>'
    });
});

orderManager.importTBExcel = function() {
    var formData = new FormData($( "#formTB" )[0]);
    formData.append("file",$("#file")[0]);
    $.ajax({
        //几个参数需要注意一下
        type: "POST",//方法类型
        dataType: "json",//预期服务器返回的数据类型
        url: Feng.ctxPath + "/orderManager/taobaoImport" ,//url
        data:formData,
        contentType: false,
        processData: false,
        success: function (result) {
            alert(result.message);
            window.parent.layer.closeAll();
            orderManager.table.refresh({query: orderManager.formParams()});
        },
        error : function() {
            alert("异常！");
        }
    });
}

/**
 * 京东上传弹窗
 */
$('.uporderJD').click(function () {
    //页面层
    layer.open({
        type: 1,
        skin: 'layui-layer-rim', //加上边框
        area: ['520px', '140px'], //宽高
        content: '<div class="box-body"><form id="formJD" enctype="multipart/form-data">\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="col-sm-3 control-label">更新京东订单</label>\n' +
        '                            <div class="col-sm-7" style="overflow: hidden">\n' +
        '                                <input class="inputBg" type="file" name="file"/>格式为csv\n\n' +
        '                            </div>\n' +
        '                  <span onclick="orderManager.importJDExcel()" class="btn btn-danger uporderJD" >提交</span>      </div>\n' +
        '                    </form></div>'
    });
});

orderManager.importJDExcel = function() {
    var formData = new FormData($( "#formJD" )[0]);
    formData.append("file",$("#file")[0]);
    $.ajax({
        //几个参数需要注意一下
        type: "POST",//方法类型
        dataType: "json",//预期服务器返回的数据类型
        url: Feng.ctxPath + "/orderManager/jdImport" ,//url
        data:formData,
        contentType: false,
        processData: false,
        success: function (result) {
            alert(result.message);
            window.parent.layer.closeAll();
            orderManager.table.refresh({query: orderManager.formParams()});
        },
        error : function() {
            alert("异常！");
        }
    });
}

/**
 * 拼多多上传弹窗
 */
$('.uporderPDD').click(function () {
    //页面层
    layer.open({
        type: 1,
        skin: 'layui-layer-rim', //加上边框
        area: ['520px', '140px'], //宽高
        content: '<div class="box-body"><form id="formPDD" enctype="multipart/form-data">\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="col-sm-3 control-label">更新拼多多订单</label>\n' +
        '                            <div class="col-sm-7" style="overflow: hidden">\n' +
        '                                <input class="inputBg" type="file" name="file"/>格式为csv\n\n' +
        '                            </div>\n' +
        '                  <span onclick="orderManager.importPDDExcel()" class="btn btn-danger uporderPDD" >提交</span>      </div>\n' +
        '                    </form></div>'
    });
});

orderManager.importPDDExcel = function() {
    var formData = new FormData($( "#formPDD" )[0]);
    formData.append("file",$("#file")[0]);
    $.ajax({
        //几个参数需要注意一下
        type: "POST",//方法类型
        dataType: "json",//预期服务器返回的数据类型
        url: Feng.ctxPath + "/orderManager/pddImport" ,//url
        data:formData,
        contentType: false,
        processData: false,
        success: function (result) {
            alert(result.message);
            window.parent.layer.closeAll();
            orderManager.table.refresh({query: orderManager.formParams()});
        },
        error : function() {
            alert("异常！");
        }
    });
}

/**
 * 唯品会上传弹窗
 */
$('.uporderWPH').click(function () {
    //页面层
    layer.open({
        type: 1,
        skin: 'layui-layer-rim', //加上边框
        area: ['520px', '140px'], //宽高
        content: '<div class="box-body"><form id="formWPH" enctype="multipart/form-data">\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="col-sm-3 control-label">更新唯品会订单</label>\n' +
        '                            <div class="col-sm-7" style="overflow: hidden">\n' +
        '                                <input class="inputBg" type="file" name="file"/>格式为csv\n\n' +
        '                            </div>\n' +
        '                  <span onclick="orderManager.importWPHExcel()" class="btn btn-danger uporderWPH" >提交</span>      </div>\n' +
        '                    </form></div>'
    });
});

orderManager.importWPHExcel = function() {
    var formData = new FormData($( "#formWPH" )[0]);
    formData.append("file",$("#file")[0]);
    $.ajax({
        //几个参数需要注意一下
        type: "POST",//方法类型
        dataType: "json",//预期服务器返回的数据类型
        url: Feng.ctxPath + "/orderManager/wphImport" ,//url
        data:formData,
        contentType: false,
        processData: false,
        success: function (result) {
            alert(result.message);
            window.parent.layer.closeAll();
            orderManager.table.refresh({query: orderManager.formParams()});
        },
        error : function() {
            alert("异常！");
        }
    });
}

$(function () {
    var defaultColunms = orderManager.initColumn();
    var table = new BSTablePG(orderManager.id, "/orderManager/list", defaultColunms);
    table.setPaginationType("server");
    orderManager.table = table.init();
});
