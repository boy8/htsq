/**
 * 自营订单管理
 */
var HtmlOwnGoodsOrderManager = {
    id: "HtmlOwnGoodsOrderManagerTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
HtmlOwnGoodsOrderManager.initColumn = function () {
    return [
        {title: '订单编号', field: 'order_id', visible: true, align: 'center', valign: 'middle'},
        {
            title: '商品名称',
            field: 'product_name',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: HtmlOwnGoodsOrderManager.formatProduct_name
        },
        {
            title: '付款',
            field: 'actual_amount',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: HtmlOwnGoodsOrderManager.formatColor
        },
        {
            title: '收货人',
            field: 'shipping_user_name',
            visible: true,
            align: 'center',
            valign: 'middle'
        },
        {
            title: '手机号',
            field: 'shipping_phone',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: HtmlOwnGoodsOrderManager.formatFiled
        },
        {
            title: '订单状态',
            field: 'status',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: HtmlOwnGoodsOrderManager.formatStatus
        },
        {
            title: '支付方式',
            field: 'paytype',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: HtmlOwnGoodsOrderManager.formatPaytype
        },
        {title: '物流单号', field: 'logistics_num', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'create_time', visible: true, align: 'center', valign: 'middle'},
        {
            title: '操作',
            field: 'id',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: HtmlOwnGoodsOrderManager.formatOperate
        }
    ];
};

HtmlOwnGoodsOrderManager.formatProduct_name = function (val, row, index) {
    var name = val;
    if (val == null || val.length > 60) {
        name = val.substr(0, 60) + "...";
    }
    return name;
}

HtmlOwnGoodsOrderManager.formatColor = function (val, row, index) {
    return '<span style="color:red">' + val + '</span>';
}

HtmlOwnGoodsOrderManager.formatFiled = function (val) {
    if (val == null || val.length < 1) {
        return "-";
    }
    return val;
}

HtmlOwnGoodsOrderManager.formatStatus = function (val,row) {
    var refund_status = row.refund_status;
    var status = row.status;
    var status_str = "";
    var refund_status_str = "";
    if(refund_status == 2){
         refund_status_str = "已退款";
    }
    if (status == 0) {
        status_str= "未支付";
    } else if (status == 1) {
        status_str= "已完成";
    } else if (status == 2) {
        status_str= "待发货";
    } else if (status == 3) {
        status_str= "待收货";
    }
    return status_str + "-" + refund_status_str;
}
HtmlOwnGoodsOrderManager.formatPaytype = function (val) {
    if (val == 1) {
        return "支付宝";
    } else if (val == 2) {
        return "微信";
    }
    return val;
}

HtmlOwnGoodsOrderManager.formatOperate = function (val, row) {
    var status = row.status;
    var id = row.id;
    var refund_status = row.refund_status;
    var btn_edit = "<button class='btn btn-primary' onclick='HtmlOwnGoodsOrderManager.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_applyFor = "<button class='btn btn-primary' onclick='HtmlOwnGoodsOrderManager.applyFor(" + id + ")'><i class='fa fa-edit'></i> 退款</button>";

    if ((status > 0) && (refund_status == 0 || refund_status == "0")) {
        return btn_edit + btn_applyFor;
    }
    return btn_edit;
}

/**
 * 点击弹出修改用户弹窗
 */
HtmlOwnGoodsOrderManager.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改商品状态',
        area: ['45%', '60%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/order/HtmlOwnGoodsOrder_edit.html'
    });
    this.layerIndex = index;
};


/**
 * 点击退款
 */
HtmlOwnGoodsOrderManager.applyFor = function (id) {
    console.log("id:" + id);
    var operation = function () {
        var ajax = new $ax(Feng.ctxPath + "/htmlOrderBase/applyFor", function (data) {
            if (data.code == 200) {
                Feng.success("删除成功!");
                HtmlOwnGoodsOrderManager.table.refresh();
            }
        });
        ajax.set("id", id);
        ajax.start();
    };
    Feng.confirm("确定退款?", operation);
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
HtmlOwnGoodsOrderManager.formParams = function () {
    var queryData = {};
    queryData['start_time'] = $("#start_time").val();
    queryData['end_time'] = $("#end_time").val();
    queryData['status'] = $("#status").val();
    queryData['alipay_account'] = $("#alipay_account").val();
    queryData['order_id'] = $("#order_id").val();
    queryData['product_name'] = $("#product_name").val();
    queryData['shipping_phone'] = $("#shipping_phone").val();

    return queryData;
}

/**
 * 导出
 */
HtmlOwnGoodsOrderManager.excelExport = function () {
    var startTime = $("#start_time").val();
    var endTime = $("#end_time").val();
    var status = $("#status").val();
    var order_id = $("#order_id").val();
    var shipping_phone = $("#shipping_phone").val();
    var product_name = $("#product_name").val();
    var workProcessTime = $("#workProcessTime option:selected").val();
    var url = Feng.ctxPath + "/htmlOrderBase/excelExport?start_time=" + startTime + "&end_time=" +
        endTime + "&status=" + status + "&order_id=" + order_id + "&product_name=" + product_name + "&shipping_phone=" + shipping_phone;
    $("#excelExport").attr("href", url);
};

/**
 * 查询列表
 */
HtmlOwnGoodsOrderManager.search = function () {
    HtmlOwnGoodsOrderManager.table.refresh({query: HtmlOwnGoodsOrderManager.formParams()});
};

$(function () {
    var defaultColunms = HtmlOwnGoodsOrderManager.initColumn();
    var array = new Array();
    var table = new BSTable(HtmlOwnGoodsOrderManager.id, "/htmlOrderBase/orderList", defaultColunms);
    table.setPaginationType("client");
    HtmlOwnGoodsOrderManager.table = table.init();
});
