/**
 * 初始化项目操作详情对话框
 */
var HtmlOwnGoodsInfoDlg = {
    seItem : null,
    editor : null,
    zeroPurchaseInfoData : {}
};

/**
 * 关闭此对话框
 */
HtmlOwnGoodsInfoDlg.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改订单状态
 */
HtmlOwnGoodsInfoDlg.updateSubmit = function() {
    var queryData = {};
    var id = window.parent.$("#updateId").val()
    queryData['id'] = id;
    queryData['status'] = $("#status").val();
    queryData['logistics_num'] = $("#logistics_num").val();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/htmlOrderBase/orderUpdate", function(data){
        if(data.code == 200){
            parent.HtmlOwnGoodsOrderManager.table.refresh();
            HtmlOwnGoodsInfoDlg.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/htmlOrderBase/orderDetail/" + id, function(data){
        if (data.data != null) {
            $("#product_name").val(data.data.product_name);
            $("#order_id").val(data.data.order_id);
            $("#status").val(data.data.status);
            $("#logistics_num").val(data.data.logistics_num);
        }
    });
    ajax.start();
});
