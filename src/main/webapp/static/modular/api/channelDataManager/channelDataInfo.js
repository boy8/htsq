/**
 * 渠道数据信息
 */
var channelDataInfo = {
    id: "channelDataInfoTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
channelDataInfo.initColumn = function () {
    return [
        {title: '渠道ID', field: 'channel', align: 'center', valign: 'middle', visible: true},
        {title: '安装数', field: 'install_num',visible: true,align: 'center', valign: 'middle'},
        {title: '时间', field: 'time',visible: true, align: 'center', valign: 'middle', formatter: channelDataInfo.formatTime},
    ];
};

channelDataInfo.formatTime = function (val) {
    if(val.length < 1){
        return "-";
    }
    return val;
}

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
channelDataInfo.formParams = function(type) {
    var queryData = {};
    if (type == 0){
        queryData['startTime'] = $("#startTime").val();
        queryData['endTime'] = $("#endTime").val();
    }
    return queryData;
}

/**
 * 查询列表
 */
channelDataInfo.search = function (type) {
    channelDataInfo.table.refresh({query: channelDataInfo.formParams(type)});
};

/**
 * 查询列表
 */
channelDataInfo.excelExport = function () {
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var workProcessTime = $("#workProcessTime option:selected").val();
    var url = Feng.ctxPath + "/channelData/excelExport?startTime="+startTime+"&endTime="+endTime;
    $("#excelExport").attr("href",url);
};

$(function () {
    var defaultColunms = channelDataInfo.initColumn();
    var table = new BSTable(channelDataInfo.id, "/channelData/channelDataInfo", defaultColunms);
    table.setPaginationType("client");
    channelDataInfo.table = table.init();
});
