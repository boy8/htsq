/**
 * 修改渠道数据
 */
var channelDataInfo = {
    seItem:null,
    channelData : {}
};

/**
 * 关闭此对话框
 */
channelDataInfo.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改渠道数据
 */
channelDataInfo.updateSubmit = function() {

    var queryData = {};
    queryData['id'] = $("#id").val();
    queryData['channel'] = $("#channel").val();
    queryData['install_num'] = $("#install_num").val();
    queryData['time'] = $("#time").val();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/channelData/update", function(data){
        if(data.code == 200){
            parent.channelDataManager.table.refresh();
            channelDataInfo.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/channelData/detail/" + id, function(data){
        if (data.data != null) {
            $("#id").val(id);
            $("#channel").val(data.data.channel);
            $("#install_num").val(data.data.install_num);
            $("#time").val(data.data.time);
        }
    });
    ajax.start();
});
