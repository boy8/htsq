/**
 * 渠道数据管理
 */
var channelDataManager = {
    id: "channelDataManagerTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    channelDataManagerInfoData : {},
    delIds:[]           // 批量删除
};

/**
 * 初始化表格的列
 */
channelDataManager.initColumn = function () {
    return [
        {title: '渠道ID', field: 'channel', align: 'center', valign: 'middle', visible: true},
        {title: '安装数', field: 'install_num',visible: true,align: 'center', valign: 'middle'},
        {title: '时间', field: 'time',visible: true, align: 'center', valign: 'middle', formatter: channelDataManager.formatTime},
        {title: '创建时间', field: 'create_time',visible: true,align: 'center', valign: 'middle'},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: channelDataManager.formatOperate}
    ];
};

channelDataManager.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='channelDataManager.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='channelDataManager.deleteOne(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    if (row.channel != "总计"){
        return btn_edit + "&nbsp;&nbsp;&nbsp;" + btn_del;
    }
}

channelDataManager.formatTime = function (val) {
    if(val.length < 1){
        return "-";
    }
    return val;
}

/**
 * 点击弹出增加渠道数据
 */
channelDataManager.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加渠道数据',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/channelData/channelDataManager_add'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出修改渠道数据
 */
channelDataManager.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改渠道数据',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/channelData/channelDataManager_update/'+ id
    });
    this.layerIndex = index;
};

/**
 * 清除数据
 */
channelDataManager.clearData = function() {
    this.channelDataManagerInfoData = {};
}

/**
 * 新增广告
 */
channelDataManager.addSubmit = function() {

    var queryData = {};
    queryData['channel'] = $("#channel").val();
    queryData['install_num'] = $("#install_num").val();
    queryData['time'] = $("#time").val();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/channelData/insert", function(data){
        if(data.code == 200){
            parent.channelDataManager.table.refresh();
            channelDataManager.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
channelDataManager.close = function() {
    parent.layer.closeAll();
}

/**
 * 删除单个项目操作
 */
channelDataManager.deleteOne = function (id) {
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/channelData/delete", function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                channelDataManager.table.refresh();
            }
        });
        ajax.set("id",id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);

};

/**
 * 查询项目操作列表
 */
channelDataManager.search = function () {
    var queryData = {};
    queryData['startTime'] = $("#startTime").val();
    queryData['endTime'] = $("#endTime").val();
    channelDataManager.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = channelDataManager.initColumn();
    var table = new BSTable(channelDataManager.id, "/channelData/channelDataInfo", defaultColunms);
    table.setPaginationType("client");
    channelDataManager.table = table.init();
});
