/**
 * 初始化项目操作详情对话框
 */
var atWillUpdataEdit = {
    seItem: null,
    editor: null,
    zeroPurchaseInfoData: {}
};

/**
 * 关闭此对话框
 */
atWillUpdataEdit.close = function () {
    parent.layer.closeAll();
}

/**
 * 修改用户等级
 */
atWillUpdataEdit.updateSubmit = function () {
    var queryData = {};
    var user_id = window.parent.$("#atWillUpdata_id").val();
    queryData['user_id'] = user_id;
    queryData['estimate_commission'] = $("#estimate_commission").val();
    queryData['estimate_bounty'] = $("#estimate_bounty").val();
    queryData['estimate_fans'] = $("#estimate_fans").val();
    queryData['owner_estimate'] = $("#owner_estimate").val();
    queryData['accumulated'] = $("#accumulated").val();
    queryData['activity_accumulated'] = $("#activity_accumulated").val();
    queryData['fans_estimate'] = $("#fans_estimate").val();
    queryData['bounty_accumulated'] = $("#bounty_accumulated").val();
    queryData['direct_fans'] = $("#direct_fans").val();
    queryData['indirect_fans'] = $("#indirect_fans").val();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/atWillUpdata/updateFsDetail", function (data) {
        if (data.code == 200) {
            parent.AtWillUpdataManager.table.refresh();
            atWillUpdataEdit.close();
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function () {
    var user_id = window.parent.$("#atWillUpdata_id").val();
    var ajax = new $ax(Feng.ctxPath + "/atWillUpdata/getFsDetail?user_id=" + user_id, function (data) {
        if (data.data != null) {
            $("#estimate_commission").val(data.data.estimate_commission);
            $("#estimate_bounty").val(data.data.estimate_bounty);
            $("#estimate_fans").val(data.data.estimate_fans);
            $("#owner_estimate").val(data.data.owner_estimate);
            $("#accumulated").val(data.data.accumulated);
            $("#activity_accumulated").val(data.data.activity_accumulated);
            $("#fans_estimate").val(data.data.fans_estimate);
            $("#bounty_accumulated").val(data.data.bounty_accumulated);
            $("#direct_fans").val(data.data.direct_fans);
            $("#indirect_fans").val(data.data.indirect_fans);

        }
    });
    ajax.start();
});
