/**
 * 用户管理
 */
var AtWillUpdataManager = {
    id: "AtWillUpdataManagerTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
AtWillUpdataManager.initColumn = function () {
    return [
        {title: '会员ID', field: 'user_id', visible: true, align: 'center', valign: 'middle'},
        {title: '昵称', field: 'nick_name', visible: true, align: 'center', valign: 'middle'},
        {
            title: '会员等级',
            field: 'level',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: AtWillUpdataManager.formatLevel
        },
        {title: '上月预估收益', field: 'budgetEarnings', visible: true, align: 'center', valign: 'middle'},
        {title: '累计预估收益', field: 'accumulativeTotalEarnings', visible: true, align: 'center', valign: 'middle'},
        {title: '直接粉丝数', field: 'direct_fans', visible: true, valign: 'middle', align: 'center'},
        {title: '间接粉丝数', field: 'indirect_fans', visible: true, align: 'center', valign: 'middle'},
        {
            title: '操作',
            field: 'id',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: AtWillUpdataManager.formatOperate
        }
    ];
};

AtWillUpdataManager.formatOperate = function (val, row) {
    var user_id = row.user_id;
    var btn_edit = "<button class='btn btn-primary'   onclick='AtWillUpdataManager.update(" + user_id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    return btn_edit;
}

//会员等级(0普通 1超级会员 2运营商 3联创)
AtWillUpdataManager.formatLevel = function (val) {
    if (val == 0) {
        return "普通";
    } else if (val == 1) {
        return "超级会员";
    } else if (val == 2) {
        return "运营商";
    } else if (val == 3) {
        return "联创";
    }
    return "-";
}


/**
 * 点击弹出修改用户弹窗
 */
AtWillUpdataManager.update = function (id) {
    $("#atWillUpdata_id").val(id);
    var index = layer.open({
        type: 2,
        title: '修改粉丝收益信息',
        area: ['60%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/AtWillUpdata/atWillUpdata_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 根据用户ID 修改用户某天报表收益数据
 * @returns {{}}
 */
AtWillUpdataManager.formUserParamsFomrUpdata = function () {
    //获取预估收益：今日/明日
    var da = $("#estimateDay").val();
    var dd = new Date();
    if (da == -1) {
        dd.setDate(dd.getDate() + -1);
    }
    var y = dd.getFullYear();
    var m = dd.getMonth() + 1;//获取当前月份的日期
    var d = dd.getDate();
    var time = y + "-" + m + "-" + d;

    var queryData = {};
    queryData['id'] = $("#je_fomr_id").val();
    queryData['user_id'] = $("#je_fomr_user_id").val();
    queryData['time'] = time;
    queryData['estimate_commission'] = $("#estimate_commission").val();
    queryData['estimate_bounty'] = $("#estimate_bounty").val();
    queryData['estimate_fans'] = $("#estimate_fans").val();

    return queryData;
}


AtWillUpdataManager.formUserParamsOrderUpdata = function () {
    var queryData = {};
    var queryData = {};
    queryData['user_id'] = $("#je_user_id").val();
    queryData['user_rake_back_yugu'] = $("#user_rake_back_yugu").val();
    queryData['id'] = $("#je_order_user_id").val();
    return queryData;
}

AtWillUpdataManager.formUserParamsOrderGiftRakeUpdata = function () {
    var queryData = {};
    queryData['user_id'] = $("#je_user_id").val();
    queryData['user_rake_back'] = $("#user_rake_back").val();
    queryData['id'] = $("#je_order_gift_rake_id").val();

    return queryData;
}

AtWillUpdataManager.formUserParamsUpdataTaskCoinUpdata = function () {
    var queryData = {};
    queryData['user_id'] = $("#je_user_id").val();
    queryData['gold_coin'] = $("#gold_coin").val();
    queryData['coin_num'] = $("#coin_num").val();
    queryData['my_coin_num'] = $("#my_coin_num").val();


    return queryData;
}

AtWillUpdataManager.formUserParamsUpdataTaskUpdata = function () {
    var inputList = $("#rwzx_from input");
    var labelList = $("#rwzx_from label");
    var queryData = {};
    var arr = "";
    for (var i = 0; i < inputList.size(); i++) {
        var str = labelList.eq(i).html().split(" / ");
        var inStr = inputList.eq(i);
        var id_type = inStr.attr("name").split("_");
        if(arr != ""){
            arr += "|";
        }
        //格式： id/任务类型/需求完成次数/填写完成次数
        arr += id_type[0] + "/" + id_type[1] + "/" + str[1] + "/" + inStr.val();
    }

    queryData["str"] = arr;
    queryData['user_id'] = $("#je_user_id").val();
    return queryData;
}


/**
 * 根据用户ID 修改用户收益数据
 * @returns {{}}
 */
AtWillUpdataManager.formUserParamsUpdata = function () {
    var queryData = {};
    queryData['user_id'] = $("#je_user_id").val();
    queryData['owner_estimate'] = $("#owner_estimate").val();
    queryData['normal_balance'] = $("#normal_balance").val();
    queryData['bounty_balance'] = $("#bounty_balance").val();
    queryData['accumulated'] = $("#accumulated").val();
    queryData['activity_balance'] = $("#activity_balance").val();
    queryData['activity_accumulated'] = $("#activity_accumulated").val();
    queryData['fans_estimate'] = $("#fans_estimate").val();
    queryData['bounty_accumulated'] = $("#bounty_accumulated").val();
    queryData['direct_fans'] = $("#direct_fans").val();
    queryData['indirect_fans'] = $("#indirect_fans").val();
    return queryData;
}

/**
 * 根据用户ID 查询粉丝列表数据
 * @returns {{}}
 */
AtWillUpdataManager.formFSParams = function () {
    var queryData = {};
    queryData['user_id'] = $("#in_user_id").val();
    queryData['fs_user_id'] = $("#fs_user_id").val();
    queryData['directRelation'] = $("#directRelation").val();
    queryData['is_vip'] = $("#is_vip").is(':checked') ? 1 : 0;
    return queryData;
}
/**
 * 根据用户ID 查询用户收益数据
 * @returns {{}}
 */
AtWillUpdataManager.formUserParams = function () {
    var queryData = {};
    queryData['user_id'] = $("#user_id").val();
    return queryData;
}

/**
 * 查询某用户下粉丝列表
 */
AtWillUpdataManager.searchFSlist = function () {
    AtWillUpdataManager.table.refresh({query: AtWillUpdataManager.formFSParams()});
};

/**
 * 查询用户 的 金额部分 以及 粉丝列表
 */
AtWillUpdataManager.searchUser = function () {
    //获取用户收益信息
    var id = $("#user_id").val();
    $("#in_user_id").val(id);
    $("#je_user_id").val(id);
    $("#je_fomr_user_id").val(id);

    if (id == "") {
        return;
    }
    var ajax = new $ax(Feng.ctxPath + "/atWillUpdata/getUserProfit?user_id=" + id, function (data) {
        if (data.data != null) {
            $("#owner_estimate").val(data.data.owner_estimate);
            $("#normal_balance").val(data.data.normal_balance);
            $("#bounty_balance").val(data.data.bounty_balance);
            $("#accumulated").val(data.data.accumulated);
            $("#activity_balance").val(data.data.activity_balance);
            $("#activity_accumulated").val(data.data.activity_accumulated);
            $("#fans_estimate").val(data.data.fans_estimate);
            $("#bounty_accumulated").val(data.data.bounty_accumulated);
            $("#direct_fans").val(data.data.direct_fans);
            $("#indirect_fans").val(data.data.indirect_fans);
        }
    });
    ajax.start();

    //获取预估收益：今日/明日
    var da = $("#estimateDay").val();
    var dd = new Date();
    if (da == -1) {
        dd.setDate(dd.getDate() + -1);
    }
    var y = dd.getFullYear();
    var m = dd.getMonth() + 1;//获取当前月份的日期
    var d = dd.getDate();
    var time = y + "-" + m + "-" + d;
    var ajaxFomr = new $ax(Feng.ctxPath + "/atWillUpdata/getUserFomr?user_id=" + id + "&time=" + time, function (data) {
        if (data.data != null) {
            $("#estimate_commission").val(data.data.estimate_commission);
            $("#estimate_bounty").val(data.data.estimate_bounty);
            $("#estimate_fans").val(data.data.estimate_fans);
            $("#je_fomr_id").val(data.data.id);
            $("#je_fomr_user_id").val(data.data.user_id);


        }
    });
    ajaxFomr.start();

    //获取今日预估佣金
    var ajaxOrder = new $ax(Feng.ctxPath + "/atWillUpdata/getOrderMoney?user_id=" + id, function (data) {
        if (data.data != null) {
            $("#user_rake_back_yugu").val(data.data.user_rake_back_yugu);
            $("#je_order_user_id").val(data.data.id);
        }
    });
    ajaxOrder.start();


    //获取今日预估奖励
    var ajaxOrderRakeBack = new $ax(Feng.ctxPath + "/atWillUpdata/getOrderGiftRakeMoney?user_id=" + id, function (data) {
        if (data.data != null) {
            $("#user_rake_back").val(data.data.user_rake_back);
            $("#je_order_gift_rake_id").val(data.data.id);
        }
    });
    ajaxOrderRakeBack.start();

    //获取任务中心金币信息
    var ajaxCoin = new $ax(Feng.ctxPath + "/taskBase/selectCoin?user_id=" + id, function (data) {
        if (data.data != null) {
            $("#gold_coin").val(data.data.gold_coin);
            $("#coin_num").val(data.data.coin_num);
            $("#my_coin_num").val(data.data.my_coin_num);
        }
    });
    ajaxCoin.start();

    //动态加载 任务中心 任务列表
    var ajaxTask = new $ax(Feng.ctxPath + "/taskBase/selectTaskList?user_id=" + id, function (data) {
        $("#rwzx_from div").html('');
        if (data.data != null) {
            var html = '';
            var form_start = ' <div class="form-group">';
            var form_end = ' </div>';
            var c = 0;
            for (var i = 0; i < data.data.length / 4 + 1; i++) {
                html += form_start;
                while (c < data.data.length) {
                    var da = data.data[c];
                    var inpu_html = '<label class="col-sm-1 control-label">' + da.NAME + ' / ' + da.times + '</label>\n' +
                        '<div class="col-sm-2">\n' +
                        '<input class="form-control search-input" name="' + da.id + '_' + da.type + '"value="' + (da.type == 1 ? da.base_times : da.da_times) + '" \n' +
                        'type="number" min="0" step="0.01"\n' +
                        'style="border: 0;outline: none; background-color: rgba(0, 0, 0, 0);">\n' +
                        '</div>';
                    html += inpu_html;
                    c++;
                    if ((c + 1) % 3 == 0) {
                        break;
                    }
                }
                html += form_end;
            }
            //动态追加
            $("#rwzx_from div").append(html);

        }
    });
    ajaxTask.start();


    //刷新粉丝列表
    AtWillUpdataManager.searchFSlist();
};

/**
 * 修改 用户收益信息
 * */
AtWillUpdataManager.searchUpdata = function () {
    var queryData = AtWillUpdataManager.formUserParamsUpdata();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/atWillUpdata/updataUserProfit", function (data) {
        if (data.code == 200) {
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();

    //刷新
    AtWillUpdataManager.searchUser();
}

/**
 * 修改 用户收益信息
 * */
AtWillUpdataManager.searchUpdataFomr = function () {
    var queryData = AtWillUpdataManager.formUserParamsFomrUpdata();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/atWillUpdata/updataFomr", function (data) {
        if (data.code == 200) {
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();

    //刷新
    AtWillUpdataManager.searchUser();
}

/**
 * 修改 用户order
 * */
AtWillUpdataManager.searchUpdataOrder = function () {
    var queryData = AtWillUpdataManager.formUserParamsOrderUpdata();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/atWillUpdata/addAndUpdataOrder", function (data) {
        if (data.code == 200) {
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();

    //刷新
    AtWillUpdataManager.searchUser();
}


/**
 * 修改 用户orderGiftake
 * */
AtWillUpdataManager.searchUpdataOrderGiftRake = function () {
    var queryData = AtWillUpdataManager.formUserParamsOrderGiftRakeUpdata();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/atWillUpdata/addAndUpdataOrderGiftRake", function (data) {
        if (data.code == 200) {
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();

    //刷新
    AtWillUpdataManager.searchUser();
}

/**
 * 修改 用户任务中心金币数量 jz_task_coin
 * */
AtWillUpdataManager.searchUpdataTaskCoin = function () {
    var queryData = AtWillUpdataManager.formUserParamsUpdataTaskCoinUpdata();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/taskBase/updateMyComeGoOutGold", function (data) {
        if (data.code == 200) {
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();

    //刷新
    AtWillUpdataManager.searchUser();
}

/**
 * 修改 用户任务中心金币数量 jz_task_coin
 * */
AtWillUpdataManager.searchUpdataTask = function () {
    var queryData = AtWillUpdataManager.formUserParamsUpdataTaskUpdata();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/taskBase/updateTaskS", function (data) {
        if (data.code == 200) {
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();

    //刷新
    AtWillUpdataManager.searchUser();
}


$(function () {
    var defaultColunms = AtWillUpdataManager.initColumn();
    var table = new BSTablePG(AtWillUpdataManager.id, "/atWillUpdata/getFSList", defaultColunms);
    table.setPaginationType("server");
    AtWillUpdataManager.table = table.init();
});
