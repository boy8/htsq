/**
 * 订单管理
 */
var initConfigManage = {
    initConfigManageData : {}
};

/**
 * 清除数据
 */
initConfigManage.clearData = function() {
    this.initConfigManageData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
initConfigManage.set = function(key, val) {
    this.initConfigManageData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
initConfigManage.get = function(key) {
    return $("#" + key).val();
}

/**
 * 收集数据
 */
initConfigManage.collectData = function() {
    // 添加自己需要新增的字段信息，与新增页面的input框的id相匹配
    var android_oauth_mode = $('input[name="android_oauth_mode"]:checked').val();
    var ios_oauth_mode = $('input[name="ios_oauth_mode"]:checked').val();
    this.set('id').set('oauth_url').set("access_key").set("secret_key").set("img_url").set("head_pic_url").set("verify_version")
        .set("api_url").set("android_oauth_mode",android_oauth_mode).set("ios_oauth_mode",ios_oauth_mode);
}

/**
 * 修改
 */
initConfigManage.submit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/normalManage/updateInit", function(data){
        if (data.code == 200){
            Feng.success("修改成功!");
        }else {
            Feng.error("修改失败!");
        }
    });
    ajax.set(this.initConfigManageData);
    ajax.start();
}


$(function () {
    var ajax = new $ax(Feng.ctxPath + "/normalManage/initInfo", function (data) {
        if (data.data != null){
            $("#id").val(data.data.id);
            $("#oauth_url").val(data.data.oauth_url);
            $("#access_key").val(data.data.access_key);
            $("#secret_key").val(data.data.secret_key);
            $("#img_url").val(data.data.img_url);
            $("#head_pic_url").val(data.data.head_pic_url);
            $("#verify_version").val(data.data.verify_version);
            $("#api_url").val(data.data.api_url);
            var android_oauth_mode =  data.data.android_oauth_mode;
            var ios_oauth_mode =  data.data.ios_oauth_mode;
            if (android_oauth_mode == 1){
                $("input[name='android_oauth_mode'][value='1']").attr("checked", true);
            }
            if (android_oauth_mode == 2){
                $("input[name='android_oauth_mode'][value='2']").attr("checked", true);
            }
            if (ios_oauth_mode == 1){
                $("input[name='ios_oauth_mode'][value='1']").attr("checked", true);
            }
            if (ios_oauth_mode == 2){
                $("input[name='ios_oauth_mode'][value='2']").attr("checked", true);
            }
        }
    });
    ajax.start();
});
