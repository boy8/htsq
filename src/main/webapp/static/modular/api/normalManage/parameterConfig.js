/**
 * 订单管理
 */
var normalManager = {
    normalManagerData : {}
};

/**
 * 清除数据
 */
normalManager.clearData = function() {
    this.normalManagerData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
normalManager.set = function(key, val) {
    this.normalManagerData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
normalManager.get = function(key) {
    return $("#" + key).val();
}

/**
 * 收集数据
 */
normalManager.collectData = function() {
    // 添加自己需要新增的字段信息，与新增页面的input框的id相匹配
    var sms_type = $('input[name="sms_type"]:checked').val();
    this.set('id').set("jd_app_key").set("jd_app_secret").set("jd_access_token").set("jd_tokenExpireTime").
    set("jg_app_key").set("jg_app_secret").set("sms_type",sms_type);
}

/**
 * 修改
 */
normalManager.submit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/normalManage/updateParameterInfo", function(data){
        if (data.code == 200){
            Feng.success("修改成功!");
        }else {
            Feng.error("修改失败:"+data.message);
        }
    });
    ajax.set(this.normalManagerData);
    ajax.start();
}


$(function () {
    var ajax = new $ax(Feng.ctxPath + "/normalManage/parameterInfo", function (data) {
        if (data.data != null){
            var sms_type = data.data.sms_type;
            $("#id").val(data.data.id);
            $("#jd_app_key").val(data.data.jd_app_key);
            $("#jd_app_secret").val(data.data.jd_app_secret);
            $("#jd_access_token").val(data.data.jd_access_token);
            $("#jd_tokenExpireTime").val(data.data.jd_tokenExpireTime);
            $("#jg_app_key").val(data.data.jg_app_key);
            $("#jg_app_secret").val(data.data.jg_app_secret);
            if (sms_type == 0){
                $("input[name='sms_type'][value='0']").attr("checked", true);
            }else if (sms_type == 1){
                $("input[name='sms_type'][value='1']").attr("checked", true);
            }
        }
    });
    ajax.start();

});
