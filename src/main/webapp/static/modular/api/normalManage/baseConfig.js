/**
 * 订单管理
 */
var normalManager = {
    editor1: null,
    editor2: null,
    // editor3: null,
    normalManagerData : {}
};

/**
 * 清除数据
 */
normalManager.clearData = function() {
    this.normalManagerData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
normalManager.set = function(key, val) {
    this.normalManagerData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
normalManager.get = function(key) {
    return $("#" + key).val();
}

/**
 * 收集数据
 */
normalManager.collectData = function() {
    // 添加自己需要新增的字段信息，与新增页面的input框的id相匹配
    var register_agreement = normalManager.editor1.txt.html();
    var user_agreement = normalManager.editor2.txt.html();
    this.set("id");
    this.set("register_agreement",register_agreement);
    this.set("user_agreement",user_agreement);
}

/**
 * 修改
 */
normalManager.submit = function() {
    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/normalManage/updateBaseInfo", function(data){
        if (data.code == 200){
            Feng.success("修改成功!");
            window.history.back(-1);
        }else {
            Feng.error("修改失败!");
        }
    });
    ajax.set(this.normalManagerData);
    ajax.start();
}


$(function () {
    //初始化编辑器
    var E = window.wangEditor;
    var editor1 = new E('#editor1');
    editor1.create();
    normalManager.editor1 = editor1;

    var editor2 = new E('#editor2');
    editor2.create();
    normalManager.editor2 = editor2;

    // var editor3 = new E('#editor3');
    // editor3.create();
    // normalManager.editor3 = editor3;


    var ajax = new $ax(Feng.ctxPath + "/normalManage/baseInfo", function (data) {
        if (data.data != null){
            $("#id").val(data.data.id);
            // $("#reback_money").val(data.data.reback_money);
            // $("#reback_one_money").val(data.data.reback_one_money);
            // $("#reback_two_money").val(data.data.reback_two_money);
            editor1.txt.html(data.data.register_agreement);
            editor2.txt.html(data.data.user_agreement);
            // editor3.txt.html(data.data.about_us);
        }
    });
    ajax.start();

});
