/**
 * 初始化项目操作详情对话框
 */
var specialCodeInfo = {
    seItem:null,
    AdvManageInfoData : {}
};

/**
 * 关闭此对话框
 */
specialCodeInfo.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改邀请码
 */
specialCodeInfo.updateSubmit = function() {

    var queryData = {};
    queryData['id'] = $("#id").val();
    queryData['firend_code'] = $("#firend_code").val();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/specialCode/update", function(data){
        if(data.code == 200){
            parent.specialCode.table.refresh();
            specialCodeInfo.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/specialCode/detail/" + id, function(data){
        if (data.data != null) {
            $("#id").val(id);
            $("#firend_code").val(data.data.firend_code);
        }
    });
    ajax.set(this.specialCodeInfo);
    ajax.start();
});
