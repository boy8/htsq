/**
 * 广告管理
 */
var specialCode = {
    id: "specialCodeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    specialCodeInfoData : {},
    delIds:[]           // 批量删除
};

/**
 * 初始化表格的列
 */
specialCode.initColumn = function () {
    return [
        {title: 'ID编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '邀请码', field: 'firend_code', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'create_time', visible: true, align: 'center', valign: 'middle'},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: specialCode.formatOperate}
    ];
};

specialCode.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary' onclick='specialCode.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger' onclick='specialCode.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

/**
 * 点击弹出增加广告弹框
 */
specialCode.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加邀请码',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/specialCode/specialCode_add.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出修改广告弹框
 */
specialCode.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改广告',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/specialCode/specialCode_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 清除数据
 */
specialCode.clearData = function() {
    this.specialCodeInfoData = {};
}

/**
 * 新增邀请码
 */
specialCode.addSubmit = function() {

    var queryData = {};
    queryData['firend_code'] = $("#firend_code").val();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/specialCode/insert", function(data){
        if(data.code == 200){
            parent.specialCode.table.refresh();
            specialCode.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
specialCode.close = function() {
    parent.layer.closeAll();
}

/**
 * 删除单个项目操作
 */
specialCode.delete = function (id) {

    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/specialCode/delete/"+id, function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                specialCode.table.refresh();
            }
        });
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 点击弹出增加邀请码
 */
specialCode.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加邀请码',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/specialCode/specialCode_add.html'
    });
    this.layerIndex = index;
};

/**
 * 查询项目操作列表
 */
specialCode.search = function () {
    var queryData = {};
    queryData['firend_code'] = $("#firend_code").val();
    specialCode.table.refresh({query: queryData});
};

// 封装批量删除的ids
specialCode.dealDelIds = function (selected) {
    specialCode.delIds = "";
    for(var i=0;i<selected.length;i++){
        if (selected[i] != null && i != selected.length -1){
            specialCode.delIds += selected[i].id+",";
        }else {
            specialCode.delIds += selected[i].id;
        }
    }
};

$(function () {
    var defaultColunms = specialCode.initColumn();
    var table = new BSTablePG(specialCode.id, "/specialCode/list", defaultColunms);
    specialCode.table = table.init();
});
