/**
 * 自营商品管理
 */
var ownGoodsManage = {
    id: "ownGoodsManageTable",	//表格id
    table: null,
    layerIndex: -1,
};
//
/**
 * 初始化表格的列
 */
ownGoodsManage.initColumn = function () {
    return [
        {title: '商品标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: '价格', field: 'former_price', visible: true, align: 'center', valign: 'middle'},
        {title: '库存', field: 'inventory', visible: true, align: 'center', valign: 'middle'},
        {title: '售卖', field: 'sell', visible: true, align: 'center', valign: 'middle'},
        {
            title: '状态',
            field: 'state',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: ownGoodsManage.formatState
        },
        {
            title: '分类',
            field: 'type',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: ownGoodsManage.formattype
        },
        {title: '排序号', field: 'sequence', visible: true, align: 'center', valign: 'middle'},
        {title: '单位', field: 'goods_units', visible: true, align: 'center', valign: 'middle'},
        {
            title: '操作',
            field: 'id',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: ownGoodsManage.formatOperate
        }
    ];
};

ownGoodsManage.formatOperate = function (val, row) {
    var id = row.dealer_id;
    var btn_edit = "<button class='btn btn-primary'   onclick='ownGoodsManage.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='ownGoodsManage.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    var btn_specif = "<button class='btn btn-specif'   onclick='ownGoodsManage.specif(" + id + ")'><i class='fa fa-trash-specif'></i> 设置规格</button>";

    return btn_edit + btn_del + btn_specif;
}

ownGoodsManage.formattype = function (val) {
    if (val == 1) {
        return "正在热卖";
    } else if (val == 2) {
        return "热销爆款";
    } else if (val == 3) {
        return "即将上市";
    } else {
        return "未知类型";
    }
}

//会员等级(0普通 1超级会员 2运营商 3联创)
ownGoodsManage.formatState = function (val) {
    if (val == 0) {
        return "停止";
    } else if (val == 1) {
        return "启用";
    }
    return "-";
}

/**
 * 删除单个项目操作
 */
ownGoodsManage.delete = function (id) {
    var operation = function () {
        var ajax = new $ax(Feng.ctxPath + "/ownGoods/delete", function (data) {
            if (data.code == 200) {
                Feng.success("删除成功!");
                ownGoodsManage.table.refresh();
            }
        });
        ajax.set("id", id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 点击弹出增加商品弹框
 */
ownGoodsManage.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/ownGoods/ownGoods_add.html'
    });
    this.layerIndex = index;
};


/**
 * 点击弹出增加商品弹框
 */
ownGoodsManage.specif = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '设置规格',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/app/production/goodsSpecificationManage.html'
    });
    this.layerIndex = index;
};


/**
 * 点击弹出修改商品弹窗
 */
ownGoodsManage.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改商品信息',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/ownGoods/ownGoods_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
ownGoodsManage.formParams = function () {
    var queryData = {};
    queryData['title'] = $("#title").val();
    queryData['state'] = $("#state").val();

    return queryData;
}

/**
 * 关闭此对话框
 */
ownGoodsManage.close = function () {
    parent.layer.closeAll();
}

/**
 * 新增广告
 */
ownGoodsManage.addSubmit = function () {
    var queryData = {};
    var state = $('input[name="state"]:checked').val();
    queryData['title'] = $("#title").val();
    queryData['describe'] = $("#describe").val();
    queryData['former_price'] = $("#former_price").val();
    queryData['coupons_price'] = $("#coupons_price").val();
    queryData['now_price'] = $("#now_price").val();
    queryData['inventory'] = $("#inventory").val();
    queryData['video'] = $("#video").val();
    queryData['show_images'] = $("#show_images").val();
    queryData['carousel_imags'] = $("#carousel_imags").val();
    queryData['goods_units'] = $("#goods_units").val();
    queryData['state'] = state;
    queryData['type'] = $("#type").val();
    queryData['sequence'] = $("#sequence").val();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ownGoods/insertOwnGoods", function (data) {
        if (data.code == 200) {
            parent.ownGoodsManage.table.refresh();
            ownGoodsManage.close();
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 查询列表
 */
ownGoodsManage.search = function () {
    ownGoodsManage.table.refresh({query: ownGoodsManage.formParams()});
};

$(function () {
    var defaultColunms = ownGoodsManage.initColumn();
    var table = new BSTable(ownGoodsManage.id, "/ownGoods/selectAll", defaultColunms);
    table.setPaginationType("client");
    ownGoodsManage.table = table.init();
});
