/**
 * 商学院管理
 */
var businessManage = {
    editor: null,
    id: "businessManageTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
businessManage.initColumn = function () {
    return [
        {title: '标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: '一级标题',field: 'first_module',visible: true,align: 'center',valign: 'middle',formatter: businessManage.formatFirst},
        {title: '二级标题',field: 'second_module',visible: true,align: 'center',valign: 'middle',formatter: businessManage.formatSecond},
        {title: '三级标题',field: 'third_module',visible: true,align: 'center',valign: 'middle',formatter: businessManage.formatThird},
        {title: '查看等级',field: 'show_lv',isible: true,align: 'center',valign: 'middle',formatter: businessManage.formatLevel},
        {title: '状态',field: 'state',visible: true,align: 'center',valign: 'middle',formatter: businessManage.formatState},
        {title: '创建时间', field: 'create_time', visible: true, align: 'center', valign: 'middle'},
        {title: '操作',field: 'business_id',visible: true,align: 'center',valign: 'middle',formatter: businessManage.formatOperate}
    ];
};

businessManage.formatOperate = function (val, row) {
    var id = row.business_id;
    var btn_edit = "<button class='btn btn-primary'   onclick='businessManage.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='businessManage.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

businessManage.formatState = function (val) {
    if (val == 0) {
        return "停止";
    } else if (val == 1) {
        return "启用";
    }
    return "-";
}

businessManage.formatFirst = function (val) {
    if (val == 1) {
        return "每日爆款";
    } else if (val == 2) {
        return "宣传素材";
    } else if (val == 3) {
        return "商学院";
    }
    return "-";
}

businessManage.formatSecond = function (val, row, index) {
    var first_module = row.first_module;
    if (first_module == 1) {
        if (val == 1) {
            return "淘宝";
        } else if (val == 2) {
            return "VIP";
        } else if (val == 3) {
            return "唯品会";
        } else if (val == 4) {
            return "拼多多";
        } else if (val == 5) {
            return "京东";
        }
    } else if (first_module == 2) {
        if (val == 1) {
            return "拉新素材";
        } else if (val == 2) {
            return "新人必发";
        }
    } else if (first_module == 3) {
        if (val == 1) {
            return "小白必看";
        } else if (val == 2) {
            return "大咖风采";
        }else if (val == 3) {
            return "百问百答";
        }else if (val == 4) {
            return "初级课堂";
        }else if (val == 5) {
            return "中级课堂";
        }else if (val == 6) {
            return "高级课堂";
        }
    }
    return "-";
}

businessManage.formatThird = function (val) {
    if (null == val || val.length < 1) {
        return "-";
    }
    return "第" + val + "天";
}

//会员等级(0普通 1超级会员 2运营商 3联创)
businessManage.formatLevel = function (val) {
    if (val == 0) {
        return "普通";
    } else if (val == 1) {
        return "超级会员";
    } else if (val == 2) {
        return "运营商";
    } else if (val == 3) {
        return "联创";
    }
    return "-";
}

/**
 * 删除单个项目操作
 */
businessManage.delete = function (id) {
    var operation = function () {
        var ajax = new $ax(Feng.ctxPath + "/businessManage/delete", function (data) {
            if (data.code == 200) {
                Feng.success("删除成功!");
                businessManage.table.refresh();
            }
        });
        ajax.set("id", id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 点击弹出增加弹框
 */
businessManage.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加内容',
        area: ['45%', '85%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/businessManage/business_add.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出修改商品弹窗
 */
businessManage.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改信息',
        area: ['45%', '85%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/businessManage/business_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
businessManage.formParams = function () {
    var queryData = {};
    queryData['title'] = $("#title").val();
    queryData['state'] = $("#state").val();
    queryData['first_module'] = $("#first_module").val();
    var second_module = $("#second_module").val();
    if (second_module == 6 || second_module == 8) {
        second_module = 1;
    }else if (second_module == 7 || second_module == 9) {
        second_module = 2;
    } else if (second_module == 10) {
        second_module = 3;
    } else if (second_module == 11) {
        second_module = 4;
    } else if (second_module == 12) {
        second_module = 5;
    }else if (second_module == 13) {
        second_module = 6;
    }
    queryData['second_module'] = second_module;
    queryData['third_module'] = $("#third_module").val();

    return queryData;
}

/**
 * 关闭此对话框
 */
businessManage.close = function () {
    parent.layer.closeAll();
}

/**
 * 新增
 */
businessManage.addSubmit = function () {
    var queryData = {};
    var state = $('input[name="state"]:checked').val();
    queryData['title'] = $("#title").val();
    queryData['home_image'] = $("#home_image").val();
    queryData['first_module'] = $("#first_module").val();
    var second_module = $("#second_module").val();
    if (second_module == 6 || second_module == 8) {
        second_module = 1;
    } else if (second_module == 7 || second_module == 9) {
        second_module = 2;
    } else if (second_module == 10) {
        second_module = 3;
    } else if (second_module == 11) {
        second_module = 4;
    } else if (second_module == 12) {
        second_module = 5;
    }else if (second_module == 13) {
        second_module = 6;
    }
    var text = $("#first_module").val() == 3 ? businessManage.editor.txt.html() : $("#editor2").val();
    queryData['second_module'] = second_module;
    queryData['third_module'] = $("#third_module").val();
    queryData['type'] = $("#type").val();
    queryData['video'] = $("#video").val();
    queryData['ima_s'] = $("#ima_s").val();
    queryData['show_lv'] = $("#show_lv").val();
    queryData['biz_id'] = $("#biz_id").val();
    queryData['state'] = state;
    queryData['text'] = text;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/businessManage/insert", function (data) {
        if (data.code == 200) {
            parent.businessManage.table.refresh();
            businessManage.close();
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 查询列表
 */
businessManage.search = function () {
    businessManage.table.refresh({query: businessManage.formParams()});
};

$(function () {
    $("#editor2").hide();
    $("#editor").hide();
    if ($("#first_module option:selected").val() == 3) {
        $("#editor").show();
        $("#editor2").hide();
    } else {
        $("#editor").hide();
        $("#editor2").show();
    }


    var defaultColunms = businessManage.initColumn();
    var table = new BSTable(businessManage.id, "/businessManage/list", defaultColunms);
    table.setPaginationType("client");
    businessManage.table = table.init();


    $("#first_module").change(function () {
        //获取当前select选择的值
        var selectVal = $("#first_module option:selected").val();
        if (selectVal == 3) {
            $("#editor").show();
            $("#editor2").hide();
        } else {
            $("#editor").hide();
            $("#editor2").show();
        }

    });
});


