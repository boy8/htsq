/**
 * 初始化项目操作详情对话框
 */
var businessInfoDlg = {
    seItem: null,
    editor: null,
    zeroPurchaseInfoData: {}
};

/**
 * 关闭此对话框
 */
businessInfoDlg.close = function () {
    parent.layer.closeAll();
}

/**
 * 修改信息
 */
businessInfoDlg.updateSubmit = function () {
    var queryData = {};
    var id = window.parent.$("#updateId").val()
    var text = businessInfoDlg.editor.txt.html();
    var state = $('input[name="state"]:checked').val();
    queryData['business_id'] = id;
    queryData['title'] = $("#title").val();
    queryData['home_image'] = $("#home_image").val();
    queryData['first_module'] = $("#first_module").val();
    var second_module = $("#second_module").val();
    if (second_module == 6 || second_module == 8) {
        second_module = 1;
    }
    if (second_module == 7 || second_module == 9) {
        second_module = 2;
    } else if (second_module == 10) {
        second_module = 3;
    } else if (second_module == 11) {
        second_module = 4;
    } else if (second_module == 12) {
        second_module = 5;
    }else if (second_module == 13) {
        second_module = 6;
    }
    var text = $("#first_module").val() == 3 ? businessInfoDlg.editor.txt.html() : $("#editor2").val();
    queryData['second_module'] = second_module;
    queryData['third_module'] = $("#third_module").val();
    queryData['type'] = $("#type").val();
    queryData['video'] = $("#video").val();
    queryData['ima_s'] = $("#ima_s").val();
    queryData['show_lv'] = $("#show_lv").val();
    queryData['biz_id'] = $("#biz_id").val();
    queryData['state'] = state;
    queryData['text'] = text;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/businessManage/update", function (data) {
        if (data.code == 200) {
            parent.businessManage.table.refresh();
            businessInfoDlg.close();
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function () {
    //初始化编辑器
    var E = window.wangEditor;
    var editor = new E('#editor');
    editor.create();
    businessInfoDlg.editor = editor;

    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/businessManage/detail/" + id, function (data) {
        if (data.data != null) {
            var state = data.data.state;

            $("#id").val(id);
            $("#title").val(data.data.title);
            $("#home_image").val(data.data.home_image);
            var first_module = data.data.first_module;
            if(first_module == 3){
                editor.txt.html(data.data.text);
            }else{
                $("#editor2").val(data.data.text);
            }
            $("#first_module").val(first_module);
            var second_module = data.data.second_module;
            if (first_module == 2) {
                if (second_module == 1) {
                    second_module = 6;
                } else if (second_module == 2) {
                    second_module = 7;
                }
            } else if (first_module == 3) {
                if (second_module == 1) {
                    second_module = 8;
                } else if (second_module == 2) {
                    second_module = 9;
                } else if (second_module == 3) {
                    second_module = 10;
                } else if (second_module == 4) {
                    second_module = 11;
                } else if (second_module == 5) {
                    second_module = 12;
                }else if (second_module == 6) {
                    second_module = 13;
                }
            }
            $("#second_module").val(second_module);
            $("#third_module").val(data.data.third_module);
            $("#type").val(data.data.type);
            $("#video").val(data.data.video);
            $("#ima_s").val(data.data.ima_s);
            $("#show_lv").val(data.data.show_lv);
            $("#biz_id").val(data.data.biz_id);
            $("#biz_id").val(data.data.biz_id);
            if (state == 0) {
                $("input[name='state'][value='0']").attr("checked", true);
            } else if (state == 1) {
                $("input[name='state'][value='1']").attr("checked", true);
            }
        }
    });
    ajax.start();

    $("#editor2").hide();
    $("#editor").hide();
    if ($("#first_module option:selected").val() == 3) {
        $("#editor").show();
        $("#editor2").hide();
    } else {
        $("#editor").hide();
        $("#editor2").show();
    }

    $("#first_module").change(function () {
        //获取当前select选择的值
        var selectVal = $("#first_module option:selected").val();
        if (selectVal == 3) {
            $("#editor").show();
            $("#editor2").hide();
        } else {
            $("#editor").hide();
            $("#editor2").show();
        }

    });
});
