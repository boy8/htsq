/**
 * 每日爆款
 */
var businessDaily = {
    id: "businessDailyTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    businessDailyInfoData : {},
    delIds:[]           // 批量删除
};

/**
 * 初始化表格的列
 */
businessDaily.initColumn = function () {
    return [
        {title: '商品名称', field: 'goods_name', visible: true, align: 'center', valign: 'middle'},
        {title: '商品ID', field: 'goods_id', visible: true, align: 'center', valign: 'middle'},
        {title: '价格', field: 'price', visible: true, align: 'center', valign: 'middle'},
        {title: '券额', field: 'coupon', visible: true, align: 'center', valign: 'middle'},
        {title: '佣金', field: 'rake_back', visible: true, align: 'center', valign: 'middle'},
        {title: '是否有券', field: 'ifcoupon', visible: true, align: 'center', valign: 'middle',formatter: businessDaily.formatIfCoupon},
        {title: '类别', field: 'category', visible: true, align: 'center', valign: 'middle', formatter: businessDaily.formatCategory},
        {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle', formatter: businessDaily.formatStatus},
        {title: '创建时间', field: 'create_time', visible: true, align: 'center', valign: 'middle'},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: businessDaily.formatOperate}
    ];
};

businessDaily.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='businessDaily.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='businessDaily.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

businessDaily.formatStatus = function (val, row) {
    if(row.status == "0"){
        return "停用";
    }else if(row.status == "1") {
        return "启用";
    }
       return "-";
}
businessDaily.formatCategory = function (val, row) {
    if(val == "1"){
        return "美妆";
    }else if(val == "2") {
        return "母婴";
    }else if(val == "3") {
        return "服饰";
    }else if(val == "4") {
        return "数码";
    }else if(val == "5") {
        return "食品";
    }else if(val == "6") {
        return "其他";
    }
       return "-";
}
businessDaily.formatIfCoupon = function (val, row) {
    if(val == "0"){
        return "否";
    }else if(val == "1") {
        return "是";
    }
       return "-";
}

/**
 * 点击弹出修改广告弹框
 */
businessDaily.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改广告',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/businessDaily/businessDaily_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 清除数据
 */
businessDaily.clearData = function() {
    this.businessDailyInfoData = {};
}

/**
 * 关闭此对话框
 */
businessDaily.close = function() {
    parent.layer.closeAll();
}

/**
 * 删除单个项目操作
 */
businessDaily.delete = function (id) {
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/businessDaily/delete", function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                businessDaily.table.refresh();
            }
        });
        ajax.set("id",id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 查询项目操作列表
 */
businessDaily.search = function () {
    var queryData = {};
    queryData['goods_name'] = $("#goods_name").val();
    queryData['goods_id'] = $("#goods_id").val();
    queryData['category'] = $("#category").val();
    queryData['status'] = $("#status").val();
    businessDaily.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = businessDaily.initColumn();
    var table = new BSTablePG(businessDaily.id, "/businessDaily/list", defaultColunms);
    table.setPaginationType("server");
    businessDaily.table = table.init();
});
