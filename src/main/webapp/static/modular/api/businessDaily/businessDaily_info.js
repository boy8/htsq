/**
 * 初始化项目操作详情对话框
 */
var businessDailyInfoDlg = {
    seItem:null,
    AdvManageInfoData : {}
};

/**
 * 关闭此对话框
 */
businessDailyInfoDlg.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改广告
 */
businessDailyInfoDlg.updateSubmit = function() {

    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    var ifcoupon = $('input[name="ifcoupon"]:checked').val();
    queryData['id'] = $("#id").val();
    queryData['goods_name'] = $("#goods_name").val();
    queryData['goods_id'] = $("#goods_id").val();
    queryData['price'] = $("#price").val();
    queryData['coupon'] = $("#coupon").val();
    queryData['rake_back'] = $("#rake_back").val();
    queryData['ifcoupon'] = ifcoupon;
    queryData['category'] = $("#category").val();
    queryData['status'] = status;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/businessDaily/update", function(data){
        if(data.code == 200){
            parent.businessDaily.table.refresh();
            businessDailyInfoDlg.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/businessDaily/detailInfo/" + id, function(data){
        if (data.data != null) {
            var status = data.data.status;
            var ifcoupon = data.data.ifcoupon;
            $("#id").val(id);
            $("#goods_name").val(data.data.goods_name);
            $("#goods_id").val(data.data.goods_id);
            $("#price").val(data.data.price);
            $("#coupon").val(data.data.coupon);
            $("#rake_back").val(data.data.rake_back);
            $("#category").val(data.data.category);
            if (status == 0){
                $("input[name='status'][value='0']").attr("checked", true);
            }else if (status == 1){
                $("input[name='status'][value='1']").attr("checked", true);
            }
            if (ifcoupon == 0){
                $("input[name='ifcoupon'][value='0']").attr("checked", true);
            }else if (ifcoupon == 1){
                $("input[name='ifcoupon'][value='1']").attr("checked", true);
            }
        }
    });
    ajax.set(this.advManageInfoData);
    ajax.start();
});
