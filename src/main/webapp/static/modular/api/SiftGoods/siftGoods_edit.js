/**
 * 初始化项目操作详情对话框
 */
var siftGoodsEdit = {
    seItem : null,
    editor : null,
    zeroPurchaseInfoData : {}
};

/**
 * 关闭此对话框
 */
siftGoodsEdit.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改商品信息
 */
siftGoodsEdit.updateSubmit = function() {
    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    queryData['id'] = $("#goods_id").val();
    queryData['goods_type'] = $("#goods_type").val();
    queryData['goods_url'] = $("#goods_url").val();
    queryData['small_icon'] = $("#small_icon").val();
    queryData['top_image_list'] = $("#top_image_list").val();
    queryData['top_home_image'] = $("#top_home_image").val();
    queryData['top_void'] = $("#top_void").val();
    queryData['title_s'] = $("#title_s").val();
    queryData['title'] = $("#title").val();
    queryData['content'] = $("#content").val();
    queryData['former_price'] = $("#former_price").val();
    queryData['now_price'] = $("#now_price").val();
    queryData['sell'] = $("#sell").val();
    queryData['inventory'] = $("#inventory").val();
    queryData['share_count'] = $("#share_count").val();
    queryData['dateil_type'] = $("#dateil_type").val();
    queryData['dateil_h5'] = $("#dateil_h5").val();
    queryData['dateil_list'] = $("#dateil_list").val();
    queryData['sort'] = $("#sort").val();
    queryData['status'] = status;
    queryData['sequence'] = $("#sequence").val();
    queryData['goods_units'] = $("#goods_units").val();
    queryData['rake_back'] = $("#rake_back").val();
    queryData['consignee'] = $("#consignee").val();
    queryData['consignee_p'] = $("#consignee_p").val();
    queryData['consignee_site'] = $("#consignee_site").val();
    var discounts_type = $('input[name="discounts_type"]:checked').val();
    queryData['discounts_type'] = discounts_type;


    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/siftGoodsBase/update", function(data){
        if(data.code == 200){
            parent.siftGoodsManage.table.refresh();
            siftGoodsEdit.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#siftgoods_id").val()
    var ajax = new $ax(Feng.ctxPath + "/siftGoodsBase/queryAll/" + id, function(data){
        if (data.data != null) {
            $("#goods_id").val(data.data.id);
            $("#goods_type").val(data.data.goods_type);
            $("#goods_url").val(data.data.goods_url);
            $("#small_icon").val(data.data.small_icon);
            $("#top_image_list").val(data.data.top_image_list);
            $("#top_home_image").val(data.data.top_home_image);
            $("#top_void").val(data.data.top_void);
            $("#title_s").val(data.data.title_s);
            $("#title").val(data.data.title);
            $("#content").val(data.data.content);
            $("#former_price").val(data.data.former_price);
            $("#now_price").val(data.data.now_price);
            $("#sell").val(data.data.sell);
            $("#inventory").val(data.data.inventory);
            $("#share_count").val(data.data.share_count);
            $("#dateil_type").val(data.data.dateil_type);
            $("#dateil_h5").val(data.data.dateil_h5);
            $("#dateil_list").val(data.data.dateil_list);
            $("#sort").val(data.data.sort);
            $("#id").val(data.data.id);
            $("#rake_back").val(data.data.rake_back);
            $("#goods_units").val(data.data.goods_units);
            $("#consignee").val(data.data.consignee);
            $("#consignee_p").val(data.data.consignee_p);
            $("#consignee_site").val(data.data.consignee_site);
            $("#sequence").val(data.data.sequence);
            var status = data.data.status;
            if (status == 0){
                $("input[name='status'][value='0']").attr("checked", true);
            }else if (status == 1){
                $("input[name='status'][value='1']").attr("checked", true);
            }
            var discounts_type = data.data.discounts_type;
            if (discounts_type == 0){
                $("input[name='discounts_type'][value='0']").attr("checked", true);
            }else if (discounts_type == 1){
                $("input[name='discounts_type'][value='1']").attr("checked", true);
            }
        }
    });
    ajax.start();
});
