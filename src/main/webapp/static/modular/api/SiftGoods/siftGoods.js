/**
 * 核桃精选商品管理
 */
var siftGoodsManage = {
    id: "siftGoodsManageTable",	//表格id
    table: null,
    layerIndex: -1,
};
//
/**
 * 初始化表格的列
 */
siftGoodsManage.initColumn = function () {
    return [
        {title: '商品短标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: 'ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '库存', field: 'inventory', visible: true, align: 'center', valign: 'middle'},
        {title: '售卖', field: 'sell', visible: true, align: 'center', valign: 'middle'},
        {title: '原价', field: 'former_price', visible: true, align: 'center', valign: 'middle'},
        {title: '现价', field: 'now_price', visible: true, align: 'center', valign: 'middle'},
        {title: '佣金', field: 'rake_back', visible: true, align: 'center', valign: 'middle'},
        {title: '分享次数', field: 'share_count', visible: true, align: 'center', valign: 'middle'},
        {
            title: '商品优惠类型',
            field: 'discounts_type',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftGoodsManage.formatDiscountsType
        },
        {
            title: '状态',
            field: 'status',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftGoodsManage.formatState
        },

        {
            title: '操作',
            field: 'id',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftGoodsManage.formatOperate
        }
    ];
};

siftGoodsManage.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='siftGoodsManage.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='siftGoodsManage.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    var btn_specif = "<button class='btn btn-specif'   onclick='siftGoodsManage.specif(" + id + ")'><i class='fa fa-trash-specif'></i> 设置规格</button>";

    return btn_edit + btn_del + btn_specif;
}

siftGoodsManage.formattype = function (val) {
    if (val == 1) {
        return "正在热卖";
    } else if (val == 2) {
        return "热销爆款";
    } else if (val == 3) {
        return "即将上市";
    } else {
        return "未知类型";
    }
}

//会员等级(0 禁用 1 启用)
siftGoodsManage.formatState = function (val) {
    if (val == 0) {
        return "禁用";
    } else if (val == 1) {
        return "启用";
    }
    return "-";
}
//商品售卖类型：0核桃精选 1线下折扣
siftGoodsManage.formatDiscountsType = function (val) {
    if (val == 0) {
        return "核桃精选";
    } else if (val == 1) {
        return "线下折扣";
    }
    return "-";
}

/**
 * 删除单个项目操作
 */
siftGoodsManage.delete = function (id) {
    var operation = function () {
        var ajax = new $ax(Feng.ctxPath + "/siftGoodsBase/delete", function (data) {
            if (data.code == 200) {
                Feng.success("删除成功!");
                siftGoodsManage.table.refresh();
            }
        });
        ajax.set("id", id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 点击弹出增加商品弹框
 */
siftGoodsManage.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/app/production/siftGoods_add.html'
    });
    this.layerIndex = index;
};


/**
 * 点击弹出增加商品弹框
 */
siftGoodsManage.specif = function (id) {
    $("#siftgoods_id").val(id);
    var index = layer.open({
        type: 2,
        title: '设置规格',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/app/production/siftGoodsSpecificationManage.html'
    });
    this.layerIndex = index;
};


/**
 * 点击弹出修改商品弹窗
 */
siftGoodsManage.update = function (id) {
    $("#siftgoods_id").val(id);
    var index = layer.open({
        type: 2,
        title: '修改商品信息',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/app/production/siftGoods_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
siftGoodsManage.formParams = function () {
    var queryData = {};
    queryData['title'] = $("#title").val();
    queryData['status'] = $("#status").val();
    queryData['discounts_type'] = $("#discounts_type").val();


    return queryData;
}

/**
 * 关闭此对话框
 */
siftGoodsManage.close = function () {
    parent.layer.closeAll();
}

/**
 * 新增广告
 */
siftGoodsManage.addSubmit = function () {
    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    queryData['id'] = $("#goods_id").val();
    queryData['goods_type'] = $("#goods_type").val();
    queryData['goods_url'] = $("#goods_url").val();
    queryData['small_icon'] = $("#small_icon").val();
    queryData['top_image_list'] = $("#top_image_list").val();
    queryData['top_home_image'] = $("#top_home_image").val();
    queryData['top_void'] = $("#top_void").val();
    queryData['title_s'] = $("#title_s").val();
    queryData['title'] = $("#title").val();
    queryData['content'] = $("#content").val();
    queryData['former_price'] = $("#former_price").val();
    queryData['now_price'] = $("#now_price").val();
    queryData['sell'] = $("#sell").val();
    queryData['inventory'] = $("#inventory").val();
    queryData['share_count'] = $("#share_count").val();
    queryData['dateil_type'] = $("#dateil_type").val();
    queryData['dateil_h5'] = $("#dateil_h5").val();
    queryData['dateil_list'] = $("#dateil_list").val();
    queryData['sort'] = $("#sort").val();
    queryData['status'] = status;
    queryData['sequence'] = $("#sequence").val();
    queryData['goods_units'] = $("#goods_units").val();
    queryData['rake_back'] = $("#rake_back").val();
    queryData['consignee'] = $("#consignee").val();
    queryData['consignee_p'] = $("#consignee_p").val();
    queryData['consignee_site'] = $("#consignee_site").val();
    var discounts_type = $('input[name="discounts_type"]:checked').val();
    queryData['discounts_type'] = discounts_type;



    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/siftGoodsBase/insert", function (data) {
        if (data.code == 200) {
            parent.siftGoodsManage.table.refresh();
            siftGoodsManage.close();
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 查询列表
 */
siftGoodsManage.search = function () {
    siftGoodsManage.table.refresh({query: siftGoodsManage.formParams()});
};

$(function () {
    var defaultColunms = siftGoodsManage.initColumn();
    var table = new BSTable(siftGoodsManage.id, "/siftGoodsBase/pageSelect", defaultColunms);
    table.setPaginationType("client");
    siftGoodsManage.table = table.init();
});
