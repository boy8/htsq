/**
 * 自营商品管理
 */
var siftGoodsSpecificationManage = {
    id: "siftGoodsSpecificationManageTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
siftGoodsSpecificationManage.initColumn = function () {
    return [
        {title: '规格分类名称', field: 'etalon_name', visible: true, align: 'center', valign: 'middle'},
        {title: '规格名称', field: 'etalon_value', visible: true, align: 'center', valign: 'middle'},
        {title: '图片路径', field: 'file_path', visible: true, align: 'center', valign: 'middle'},
        {title: '排序', field: 'sequence', visible: true, align: 'center', valign: 'middle'},
        {
            title: '状态',
            field: 'status',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftGoodsSpecificationManage.formatStatus
        },
        {
            title: '操作',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftGoodsSpecificationManage.formatOperate
        }
    ];
};

siftGoodsSpecificationManage.formatStatus = function (val, row) {
    if (val == 1) {
        return "启动";
    } else if (val == 0) {
        return "禁用";
    } else {
        return "-";
    }
}

siftGoodsSpecificationManage.formatOperate = function (val, row) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='siftGoodsSpecificationManage.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='siftGoodsSpecificationManage.delete(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

//会员等级(0普通 1超级会员 2运营商 3联创)
siftGoodsSpecificationManage.formatState = function (val) {
    if (val == 0) {
        return "停止";
    } else if (val == 1) {
        return "启用";
    }
    return "-";
}

/**
 * 删除单个项目操作
 */
siftGoodsSpecificationManage.delete = function (id) {
    var operation = function () {
        var ajax = new $ax(Feng.ctxPath + "/siftGoodsSpecificationBase/delete", function (data) {
            if (data.code == 200) {
                Feng.success("删除成功!");
                siftGoodsSpecificationManage.table.refresh();
            }
        });
        ajax.set("id", id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);
};

/**
 * 点击弹出增加商品弹框
 */
siftGoodsSpecificationManage.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/SiftGoods/siftGoodsSpecification_add.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出修改商品弹窗
 */
siftGoodsSpecificationManage.update = function (id) {
    $("#id").val(id);
    var index = layer.open({
        type: 2,
        title: '修改商品信息',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/SiftGoods/siftGoodsSpecification_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
siftGoodsSpecificationManage.formParams = function () {
    var queryData = {};
    return queryData;
}

/**
 * 关闭此对话框
 */
siftGoodsSpecificationManage.close = function () {
    parent.layer.closeAll();
}


/**
 * 查询列表
 */
siftGoodsSpecificationManage.search = function () {
    siftGoodsSpecificationManage.table.refresh({query: siftGoodsSpecificationManage.formParams()});
};

$(function () {
    var id = window.parent.$("#siftgoods_id").val();
    $("#goods_id").val(id);
    var defaultColunms = siftGoodsSpecificationManage.initColumn();
    var table = new BSTable(siftGoodsSpecificationManage.id, "/siftGoodsSpecificationBase/pageSelect/" + id, defaultColunms);
    table.setPaginationType("client");
    siftGoodsSpecificationManage.table = table.init();
});
