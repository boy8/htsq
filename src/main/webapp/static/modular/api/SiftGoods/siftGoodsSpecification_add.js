/**
 * 自营商品管理
 */
var siftGoodsSpecificationAdd = {
    id: "siftGoodsSpecificationAddTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 关闭此对话框
 */
siftGoodsSpecificationAdd.close = function () {
    parent.layer.closeAll();
}

/**
 * 新增广告
 */
siftGoodsSpecificationAdd.addSubmit = function () {
    var queryData = {};
    var status = $('input[name="status"]:checked').val();
    var id = window.parent.$("#goods_id").val();
    queryData['etalon_name'] = $("#etalon_name").val();
    queryData['etalon_value'] = $("#etalon_value").val();
    queryData['sequence'] = $("#sequence").val();
    queryData['file_path'] = $("#file_path").val();
    queryData['file_path'] = $("#file_path").val();
    queryData['status'] = status;
    queryData['goods_id'] = id;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/siftGoodsSpecificationBase/insert", function (data) {
        if (data.code == 200) {
            siftGoodsSpecificationAdd.close();
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}
