/**
 * 初始化项目操作详情对话框
 */
var siftGoodsSpecificationEdit = {
    seItem: null,
    editor: null,
    siftGoodsSpecification: {}
};

/**
 * 关闭此对话框
 */
siftGoodsSpecificationEdit.close = function () {
    parent.layer.closeAll();
}

/**
 * 修改商品信息
 */
siftGoodsSpecificationEdit.updateSubmit = function () {
    var queryData = {};
    var id = window.parent.$("#id").val();
    var goods_id = window.parent.$("#goods_id").val();
    var status = $('input[name="status"]:checked').val();
    queryData['status'] = status;
    queryData['etalon_name'] = $("#etalon_name").val();
    queryData['etalon_value'] = $("#etalon_value").val();
    queryData['sequence'] = $("#sequence").val();
    queryData['id'] = id;
    queryData['goods_id'] = goods_id;
    queryData['file_path'] = $("#file_path").val();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/siftGoodsSpecificationBase/update", function (data) {
        if (data.code == 200) {
            siftGoodsSpecificationEdit.close();
            Feng.success("修改成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function () {
    var id = window.parent.$("#id").val();
    var ajax = new $ax(Feng.ctxPath + "/siftGoodsSpecificationBase/queryAll/" + id, function (data) {
        if (data.data != null) {
            var status = data.data.status;

            $("#etalon_name").val(data.data.etalon_name);
            $("#etalon_value").val(data.data.etalon_value);
            $("#sequence").val(data.data.sequence);
            $("#file_path").val(data.data.file_path);
            if (status == 0) {
                $("input[name='status'][value='0']").attr("checked", true);
            } else if (status == 1) {
                $("input[name='status'][value='1']").attr("checked", true);
            }
        }
    });
    ajax.start();
});
