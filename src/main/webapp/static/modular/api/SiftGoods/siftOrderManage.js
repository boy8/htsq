/**
 * 自营订单管理
 */
var siftOrderManager = {
    id: "siftOrderManagerTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
siftOrderManager.initColumn = function () {
    return [
        {title: '订单编号', field: 'order_id', visible: true, align: 'center', valign: 'middle'},
        {
            title: '商品名称',
            field: 'product_name',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftOrderManager.formatProduct_name
        },
        {
            title: '付款',
            field: 'actual_amount',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftOrderManager.formatColor
        },
        {
            title: '手机号',
            field: 'shipping_phone',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftOrderManager.formatFiled
        },
        {
            title: '收件人',
            field: 'shipping_user_name',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftOrderManager.formatFiled
        },
        {
            title: '订单优惠类型',
            field: 'discounts_type',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftOrderManager.formatDiscountsType
        },
        {
            title: '订单状态',
            field: 'status',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftOrderManager.formatStatus
        },
        {
            title: '支付方式',
            field: 'paytype',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftOrderManager.formatPaytype
        },
        {title: '物流单号', field: 'logistics_num', visible: true, align: 'center', valign: 'middle'},
        {title: '退款快递信息', field: '', visible: true, align: 'center', valign: 'middle', formatter:siftOrderManager.formatParcel},
        {
            title: '创建时间',
            field: 'create_time',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftOrderManager.formatTime
        },
        {
            title: '操作',
            field: 'id',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: siftOrderManager.formatOperate
        }
    ];
};

siftOrderManager.formatProduct_name = function (val, row, index) {
    var name = val;
    if (val == null || val.length > 60) {
        name = val.substr(0, 60) + "...";
    }
    return name;
}

siftOrderManager.formatColor = function (val, row, index) {
    return '<span style="color:red">' + val + '</span>';
}

siftOrderManager.formatFiled = function (val) {
    if (val == null || val.length < 1) {
        return "-";
    }
    return val;
}

siftOrderManager.formatStatus = function (val) {
    if (val == 0) {
        return "未支付";
    } else if (val == 1) {
        return "已完成";
    } else if (val == 2) {
        return "待发货";
    } else if (val == 3) {
        return "待收货";
    } else if (val == 4) {
        return "订单失效";
    }
    return val;
}
siftOrderManager.formatPaytype = function (val) {
    if (val == 1) {
        return "支付宝";
    } else if (val == 2) {
        return "微信";
    }
    return val;
}


siftOrderManager.formatParcel = function (val, row) {
    var number =row.parcel_number;
    var type = row.parcel_type;

    if (number == '' || type == '') {
        return "-";
    }

    return type + ":" + number;

}
siftOrderManager.formatTime = function (val, row) {
    var time = new Date(row.create_time);
    return time.getFullYear() + "-" + (time.getMonth() + 1) + "-" + time.getDate() + " " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
}
siftOrderManager.formatOperate = function (val, row) {
    var id = row.id;
    var status = row.status;
    var user_id = row.user_id;
    var order_id = row.order_id;
    var discounts_type = row.discounts_type;
    var btn_edit = "<button class='btn btn-primary' onclick='siftOrderManager.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var refund_status = row.refund_status;
    var refund_status_yes = "<button class='btn btn-primary' onclick='siftOrderManager.refundStatusUpdate(" + id + ",\"" + order_id + "\"," + user_id + "," + "2)'><i class='fa fa-edit'></i> 退款</button>";
    var refund_status_no = "<button class='btn btn-primary' onclick='siftOrderManager.refundStatusUpdate(" + id + ",\"" + order_id + "\"," + user_id + "," + "3)'><i class='fa fa-edit'></i> 驳回</button>";

    if(discounts_type == 0){ //核桃精选订单操作
        if (refund_status == 1) {
            return "退款申请:" + refund_status_yes + refund_status_no;
        } else if (refund_status == 2) { //已退款订单不做任何操作
            return "订单已退款";
        } else {
            return btn_edit;
        }
    }else if(discounts_type == 1){//线下折扣操作
        var ht = "";
        if(status != 0 && status != 1 && status != 4){
            ht += btn_edit;
        }
        if(status != 0   && status != 4){
            ht += refund_status_yes;
        }
        return ht;
    }


}
//商品售卖类型：0核桃精选 1线下折扣
siftOrderManager.formatDiscountsType = function (val,row) {
    if (row.discounts_type == 0) {
        return "核桃精选";
    } else if (row.discounts_type == 1) {
        return "线下折扣";
    }
    return "-";
}

/**
 * 点击弹出修改用户弹窗
 */
siftOrderManager.update = function (id) {
    $("#siftOrder_id").val(id);
    var index = layer.open({
        type: 2,
        title: '修改商品状态',
        area: ['45%', '60%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/SiftGoods/siftOrder_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 退款中订单处理
 */
siftOrderManager.refundStatusUpdate = function (id, order_id, user_id, refund_status) {
    var queryData = {};
    queryData['id'] = id;
    queryData['refund_status'] = refund_status;
    queryData['order_id'] = order_id;
    queryData['user_id'] = user_id;
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/siftOrderBase/refundStatusUpdate", function (data) {
        if (data.code == 200) {
            parent.siftOrderManager.table.refresh();
            Feng.success("审核成功!");
        } else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
siftOrderManager.formParams = function () {
    var queryData = {};
    queryData['start_time'] = $("#start_time").val();
    queryData['end_time'] = $("#end_time").val();
    queryData['status'] = $("#status").val();
    queryData['alipay_account'] = $("#alipay_account").val();
    queryData['order_id'] = $("#order_id").val();
    queryData['product_name'] = $("#product_name").val();

    return queryData;
}

/**
 * 查询列表
 */
siftOrderManager.excelExport = function () {
    var startTime = $("#start_time").val();
    var endTime = $("#end_time").val();
    var status = $("#status").val();
    var order_id = $("#order_id").val();
    var product_name = $("#product_name").val();
    var workProcessTime = $("#workProcessTime option:selected").val();
    var url = Feng.ctxPath + "/siftOrderBase/excelExport?start_time=" + startTime + "&end_time=" +
        endTime + "&status=" + status + "&order_id=" + order_id + "&product_name=" + product_name;
    $("#excelExport").attr("href", url);
};

/**
 * 查询列表
 */
siftOrderManager.search = function () {
    siftOrderManager.table.refresh({query: siftOrderManager.formParams()});
};

$(function () {
    var defaultColunms = siftOrderManager.initColumn();
    var table = new BSTable(siftOrderManager.id, "/siftOrderBase/pageSelect", defaultColunms);
    table.setPaginationType("client");
    siftOrderManager.table = table.init();
});
