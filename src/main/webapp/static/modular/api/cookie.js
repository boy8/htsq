var Cookie = {
};

Cookie.setCookie = function(key, value, t) {
    if (t > 0) {
        var oDate = new Date();
        oDate.setDate(oDate.getTime() + 60 * 1000 * t);//过期时间 t分钟
        document.cookie = key + "=" + encodeURI(value) + "; expires=" + oDate.toDateString();
    }
}

Cookie.getCookie = function(key) {
    var arr1 = document.cookie.split("; ");//由于cookie是通过一个分号+空格的形式串联起来的，所以这里需要先按分号空格截断,变成[name=Jack,pwd=123456,age=22]数组类型；
    for (var i = 0; i < arr1.length; i++) {
        var arr2 = arr1[i].split("=");//通过=截断，把name=Jack截断成[name,Jack]数组；
        if (arr2[0] == key) {
            return decodeURI(arr2[1]);
        }
    }
}

Cookie.removeCookie = function(key) {
    Cookie.setCookie(key, "", -1);//把cookie设置为过期
}


$(function() {
    var username = Cookie.getCookie("qbsUsername");
    var type = Cookie.getCookie("qbsType");
    if(!username){
        window.location.href="/qbs/";
    }
    if (type == "2"){//应用宝
        window.location.href="channelDataInfo.html";
    }
    $("#userName").text(username);
});