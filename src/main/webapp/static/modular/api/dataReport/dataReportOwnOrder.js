/**
 * 数据报表信息统计
 */
var dataReportOwnOrder = {
    id: "dataReportOwnOrderTerraceTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
dataReportOwnOrder.initColumn = function () {
    return [
        {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle'},
        {title: '下单人数', field: 'orderNum',visible: true, align: 'center', valign: 'middle'},
        {title: '佣金', field: 'rake_back',visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 查询表单提交参数对象
 * @returns
 */
dataReportOwnOrder.formParams = function(type) {
    var queryData = {};
    if (type == 0){
        queryData['startTime'] = $("#startTime").val();
        queryData['endTime'] = $("#endTime").val();
    }else if (type == -1) {//今天
        var curDate = new Date();
        var nowDate = curDate.format("Y-m-d");
        queryData['startTime'] = nowDate + " 00:00:00";
        queryData['endTime'] = nowDate + " 23:59:59";
    }else if (type == 1){//昨天
        var curDate = new Date();
        curDate.setDate(curDate.getDate() - 1);
        var nowDate = curDate.format("Y-m-d");
        queryData['startTime'] = nowDate + " 00:00:00";
        queryData['endTime'] = nowDate + " 23:59:59";
    }else if (type == 2){//本月
        var thisMonth = dataReportChannel.getCurrentMonth();
        queryData['startTime'] = thisMonth[0];
        queryData['endTime'] = thisMonth[1];
    }else if (type == 3){//上月
        var thisMonth = dataReportChannel.getPreviousMonth();
        queryData['startTime'] = thisMonth[0];
        queryData['endTime'] = thisMonth[1];
    }
    return queryData;
};

/**
 * 查询列表
 */
dataReportOwnOrder.search = function (type) {
    dataReportOwnOrder.table.refresh({query: dataReportOwnOrder.formParams(type)});
};

$(function () {
    var defaultColunms = dataReportOwnOrder.initColumn();
    var table = new BSTable(dataReportOwnOrder.id, "/dataReport/ownOrderList", defaultColunms);
    table.setPaginationType("client");
    dataReportOwnOrder.table = table.init();
});
