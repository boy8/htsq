/**
 * 数据报表信息统计
 */
var dataReportRate = {
    id: "dataReportRateTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
dataReportRate.initColumn = function () {
    return [
        {title: '活跃人数', field: 'activityNum', visible: true, align: 'center', valign: 'middle'},
        {title: '下单人数', field: 'buyNum',visible: true, align: 'center', valign: 'middle'},
        {title: '订单数', field: 'orderNum',visible: true, align: 'center', valign: 'middle'},
        {title: '订单佣金', field: 'rake_back',visible: true, align: 'center', valign: 'middle'},
        {title: '下单率', field: 'buyRate',visible: true, align: 'center', valign: 'middle'},
        {title: '平均订单数', field: 'orderRate', visible: true, align: 'center', valign: 'middle'},
        {title: '下单人均佣金', field: 'arpuRate', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 查询表单提交参数对象
 * @returns
 */
dataReportRate.formParams = function(type) {
    var queryData = {};
    if (type == 0){
        queryData['startTime'] = $("#startTime").val();
        queryData['endTime'] = $("#endTime").val();
    }else {
        queryData['day'] = type;
    }
    return queryData;
}

/**
 * 查询列表
 */
dataReportRate.search = function (type) {
    dataReportRate.table.refresh({query: dataReportRate.formParams(type)});
};

$(function () {
    var defaultColunms = dataReportRate.initColumn();
    var table = new BSTable(dataReportRate.id, "/dataReport/rateList", defaultColunms);
    table.setPaginationType("client");
    dataReportRate.table = table.init();
});
