/**
 * 数据报表信息统计
 */
var dataReportTerrace = {
    id: "dataReportTerraceTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
dataReportTerrace.initColumn = function () {
    return [
        {title: '平台', field: 'type', visible: true, align: 'center', valign: 'middle'},
        {title: '自购', field: 'buytype0', visible: true, align: 'center', valign: 'middle'},
        {title: '分享', field: 'buytype1', visible: true, align: 'center', valign: 'middle'},
        {title: '订单数', field: 'orderNum',visible: true, align: 'center', valign: 'middle'},
        {title: '佣金', field: 'rake_back',visible: true, align: 'center', valign: 'middle'},
        {title: '用户返佣', field: 'user_rake_back', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
dataReportTerrace.formParams = function(type) {
    var queryData = {};
    if (type == 0){
        queryData['startTime'] = $("#startTime").val();
        queryData['endTime'] = $("#endTime").val();
    }else if (type == -1) {//今天
        var curDate = new Date();
        var nowDate = curDate.format("Y-m-d");
        queryData['startTime'] = nowDate + " 00:00:00";
        queryData['endTime'] = nowDate + " 23:59:59";
    }else if (type == 1){//昨天
        var curDate = new Date();
        curDate.setDate(curDate.getDate() - 1);
        var nowDate = curDate.format("Y-m-d");
        queryData['startTime'] = nowDate + " 00:00:00";
        queryData['endTime'] = nowDate + " 23:59:59";
    }else if (type == 2){//本月
        var thisMonth = dataReportChannel.getCurrentMonth();
        queryData['startTime'] = thisMonth[0];
        queryData['endTime'] = thisMonth[1];
    }else if (type == 3){//上月
        var thisMonth = dataReportChannel.getPreviousMonth();
        queryData['startTime'] = thisMonth[0];
        queryData['endTime'] = thisMonth[1];
    }
    return queryData;
}

/**
 * 查询列表
 */
dataReportTerrace.search = function (type) {
    dataReportTerrace.table.refresh({query: dataReportTerrace.formParams(type)});
};

$(function () {
    var defaultColunms = dataReportTerrace.initColumn();
    var table = new BSTable(dataReportTerrace.id, "/dataReport/terraceList", defaultColunms);
    table.setPaginationType("client");
    dataReportTerrace.table = table.init();
});
