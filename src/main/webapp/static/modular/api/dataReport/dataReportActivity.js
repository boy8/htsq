/**
 * 活跃报表信息统计
 */
var dataReportActivity = {
    id: "dataReportActivitiesTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
dataReportActivity.initColumn = function () {
    return [
        {title: '总注册人数', field: 'total', visible: true, align: 'center', valign: 'middle'},
        {title: '新增人数', field: 'increase', visible: true, align: 'center', valign: 'middle'},
        {title: '活跃数', field: 'activity',visible: true, align: 'center', valign: 'middle'},
        {title: '活跃比例', field: 'rate',visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 查询表单提交参数对象
 * @returns {{}}
 */
dataReportActivity.formParams = function(type) {
    var queryData = {};
    if (type == 0){
        queryData['startTime'] = $("#startTime").val();
        queryData['endTime'] = $("#endTime").val();
    }else if (type == -1) {//今天
        var curDate = new Date();
        var nowDate = curDate.format("Y-m-d");
        queryData['startTime'] = nowDate + " 00:00:00";
        queryData['endTime'] = nowDate + " 23:59:59";
    }else if (type == 1){//昨天
        var curDate = new Date();
        curDate.setDate(curDate.getDate() - 1);
        var nowDate = curDate.format("Y-m-d");
        queryData['startTime'] = nowDate + " 00:00:00";
        queryData['endTime'] = nowDate + " 23:59:59";
    }else if (type == 2){//本月
        var thisMonth = dataReportActivity.getCurrentMonth();
        queryData['startTime'] = thisMonth[0];
        queryData['endTime'] = thisMonth[1];
    }else if (type == 3){//上月
        var thisMonth = dataReportActivity.getPreviousMonth();
        queryData['startTime'] = thisMonth[0];
        queryData['endTime'] = thisMonth[1];
    }
    return queryData;
}

/**
 * 查询列表
 */
dataReportActivity.search = function (type) {
    dataReportActivity.table.refresh({query: dataReportActivity.formParams(type)});
};

/***
 * 获得本月的起止时间
 */
dataReportActivity.getCurrentMonth = function () {
    //起止日期数组
    var startStop = new Array();
    //获取当前时间
    var currentDate = new Date();
    //获得当前月份0-11
    var currentMonth = currentDate.getMonth();
    //获得当前年份4位年
    var currentYear = currentDate.getFullYear();
    //求出本月第一天
    var firstDay = new Date(currentYear, currentMonth, 1).format('Y-m-d H:i:s');
    //当为12月的时候年份需要加1
    //月份需要更新为0 也就是下一年的第一个月
    if (currentMonth == 11) {
        currentYear++;
        currentMonth = 0; //就为
    } else {
        //否则只是月份增加,以便求的下一月的第一天
        currentMonth++;
    }
    //一天的毫秒数
    var millisecond = 1000 * 60 * 60 * 24;
    //下月的第一天
    var nextMonthDayOne = new Date(currentYear, currentMonth, 1);
    //求出上月的最后一天
    var lastDay = new Date(nextMonthDayOne.getTime() - millisecond).format('Y-m-d H:i:s');
    //添加至数组中返回
    startStop.push(firstDay);
    startStop.push(lastDay);
    //返回
    return startStop;
};

/**
 * 获得上一月的起止日期
 * ***/
dataReportActivity.getPreviousMonth = function () {
    //起止日期数组
    var startStop = new Array();
    //获取当前时间
    var currentDate = new Date();
    //获得当前月份0-11
    var currentMonth = currentDate.getMonth();
    //获得当前年份4位年
    var currentYear = currentDate.getFullYear();
    //获得上一个月的第一天
    var priorMonthFirstDay = new Date(this.getPriorMonthFirstDay(currentYear, currentMonth).format('Y-m-d H:i:s'));
    //获得上一月的最后一天
    var priorMonthLastDay = new Date(priorMonthFirstDay.getFullYear(), priorMonthFirstDay.getMonth(),
        this.getMonthDays(priorMonthFirstDay.getFullYear(), priorMonthFirstDay.getMonth())).format('Y-m-d H:i:s');
    //添加至数组
    startStop.push(priorMonthFirstDay.format('Y-m-d H:i:s'));
    startStop.push(priorMonthLastDay);
    //返回
    return startStop;
};
/**
 * 返回上一个月的第一天Date类型
 * @param year 年
 * @param month 月
 **/
dataReportActivity.getPriorMonthFirstDay = function (year, month) {
    //年份为0代表,是本年的第一月,所以不能减
    if (month == 0) {
        month = 11; //月份为上年的最后月份
        year--; //年份减1
        return new Date(year, month, 1);
    }
    //否则,只减去月份
    month--;
    return new Date(year, month, 1); ;
};

/**
 * 获得该月的天数
 * @param year年份
 * @param month月份
 * */
dataReportActivity.getMonthDays = function (year, month) {
    //本月第一天 1-31
    var relativeDate = new Date(year, month, 1);
    //获得当前月份0-11
    var relativeMonth = relativeDate.getMonth();
    //获得当前年份4位年
    var relativeYear = relativeDate.getFullYear();

    //当为12月的时候年份需要加1
    //月份需要更新为0 也就是下一年的第一个月
    if (relativeMonth == 11) {
        relativeYear++;
        relativeMonth = 0;
    } else {
        //否则只是月份增加,以便求的下一月的第一天
        relativeMonth++;
    }
    //一天的毫秒数
    var millisecond = 1000 * 60 * 60 * 24;
    //下月的第一天
    var nextMonthDayOne = new Date(relativeYear, relativeMonth, 1);
    //返回得到上月的最后一天,也就是本月总天数
    return new Date(nextMonthDayOne.getTime() - millisecond).getDate();
};

$(function () {
    var defaultColunms = dataReportActivity.initColumn();
    var table = new BSTable(dataReportActivity.id, "/dataReport/activityList", defaultColunms);
    table.setPaginationType("client");
    dataReportActivity.table = table.init();
});
