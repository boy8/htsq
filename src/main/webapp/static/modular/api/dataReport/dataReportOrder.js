/**
 * 数据报表信息统计
 */
var dataReportOrder = {
    id: "dataReportOrderTerraceTable",	//表格id
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
dataReportOrder.initColumn = function () {
    return [
        {title: '渠道ID', field: 'channel_id', visible: true, align: 'center', valign: 'middle'},
        {title: '用户注册数', field: 'peopleNum',visible: true, align: 'center', valign: 'middle'},
        {title: '下单人数', field: 'buyNum',visible: true, align: 'center', valign: 'middle'},
        {title: '付款金额', field: 'total_actual_amount',visible: true, align: 'center', valign: 'middle'},
        {title: '佣金', field: 'total_rake_back',visible: true, align: 'center', valign: 'middle'},
        {title: '人均佣金', field: 'ave', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 查询表单提交参数对象
 * @returns
 */
dataReportOrder.formParams = function(type) {
    var queryData = {};
    queryData['channel_id'] = $("#channel_id").val();
    if (type == 0){
        queryData['startTime'] = $("#startTime").val();
        queryData['endTime'] = $("#endTime").val();
    }else {
        queryData['day'] = type;

    }
    return queryData;
}

/**
 * 查询列表
 */
dataReportOrder.search = function (type) {
    dataReportOrder.table.refresh({query: dataReportOrder.formParams(type)});
};

$(function () {
    var defaultColunms = dataReportOrder.initColumn();
    var table = new BSTable(dataReportOrder.id, "/dataReport/incomeList", defaultColunms);
    table.setPaginationType("client");
    dataReportOrder.table = table.init();
});
