/**
 * 初始化项目操作详情对话框
 */
var problemManageEdit = {
    seItem:null,
    AdvManageInfoData : {}
};

/**
 * 关闭此对话框
 */
problemManageEdit.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改问题
 */
problemManageEdit.updateSubmit = function() {

    var queryData = {};
    var is_show = $('input[name="is_show"]:checked').val();
    var is_reccomend = $('input[name="is_reccomend"]:checked').val();
    queryData['id'] = $("#id").val();
    queryData['sequence'] = $("#sequence").val();
    queryData['content'] = $("#content").val();
    queryData['title'] = $("#title").val();
    queryData['type'] = $("#type").val();
    queryData['is_show'] = is_show;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/helpCenter/updateProblemManage", function(data){
        if(data.code == 200){
            parent.problemManage.table.refresh();
            problemManageEdit.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var ajax1 = new $ax(Feng.ctxPath + "/helpCenter/problemTypeList", function (data) {
        if(data.data != null) {
            var str =  $("#type").html();;
            for (var i = 0; i < data.data.length; i++) {
                str += "<option value='" + data.data[i].id + "'>" + data.data[i].title+"</option>";
            }
            $("#type").html(str);
        }
    });
    ajax1.start();


    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/helpCenter/problemManageDetail/" + id, function(data){
        if (data.data != null) {
            var is_show = data.data.is_reccomend;
            $("#id").val(id);
            $("#sequence").val(data.data.sequence);
            $("#title").val(data.data.title);
            $("#content").val(data.data.content);
            $("#type").val(data.data.type);
            if (is_show == 0){
                $("input[name='is_show'][value='0']").attr("checked", true);
            }
            if (is_show == 1){
                $("input[name='is_show'][value='1']").attr("checked", true);
            }
        }
    });
    ajax.set(this.advManageInfoData);
    ajax.start();
});
