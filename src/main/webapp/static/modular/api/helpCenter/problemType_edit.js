/**
 * 初始化项目操作详情对话框
 */
var problemTypeEdit = {
    seItem:null,
    AdvManageInfoData : {}
};

/**
 * 关闭此对话框
 */
problemTypeEdit.close = function() {
    parent.layer.closeAll();
}

/**
 * 修改问题
 */
problemTypeEdit.updateSubmit = function() {

    var queryData = {};
    var is_show = $('input[name="is_show"]:checked').val();
    queryData ['id'] = $("#id").val();
    queryData ['sequence'] = $("#sequence").val();
    queryData ['name'] = $("#name").val();
    queryData ['url'] = $("#url").val();
    queryData ['is_show'] = is_show;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/helpCenter/updateProblemType", function(data){
        if(data.code == 200){
            parent.problemType.table.refresh();
            problemTypeEdit.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

$(function() {
    var id = window.parent.$("#updateId").val()
    var ajax = new $ax(Feng.ctxPath + "/helpCenter/problemTypeDetail/" + id, function(data){
        if (data.data != null) {
            var is_show = data.data.is_show;
            $("#id").val(id);
            $("#sequence").val(data.data.sequence);
            $("#name").val(data.data.title);
            $("#url").val(data.data.img_url);
            if (is_show == 0){
                $("input[name='is_show'][value='0']").attr("checked", true);
            }
            if (is_show == 1){
                $("input[name='is_show'][value='1']").attr("checked", true);
            }
        }
    });
    ajax.start();
});
