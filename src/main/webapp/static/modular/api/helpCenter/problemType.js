/**
 * 广告管理
 */
var problemType = {
    id: "problemTypeTable",	//表格id
    layerIndex: -1,
    table: null,
    delIds:[]           // 批量删除
};

/**
 * 初始化表格的列
 */
problemType.initColumn = function () {
    return [
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '分类名称', field: 'title', align: 'center', valign: 'middle'},
        {title: '分类图地址', field: 'img_url',align: 'center', valign: 'middle'},
        {title: '顺序', field: 'sequence', align: 'center', valign: 'middle'},
        {title: '状态', field: 'is_show', align: 'center', valign: 'middle',formatter: problemType.formatStatus},
        {title: '创建时间', field: 'created_time', align: 'center', valign: 'middle'},
        {title: '操作', field: 'opearte', align: 'center', valign: 'middle', sortable: false, width:'100px',formatter: problemType.formatOperate}
    ];
};

problemType.formatStatus = function (val, row, index) {
    if(row.is_show == "0"){
        return "不显示";
    }
    if(row.is_show == "1") {
        return "显示";
    }
    return "-";
}

problemType.formatOperate = function (val, row, index) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='problemType.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='problemType.deleteOne(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + "&nbsp;&nbsp;&nbsp;" + btn_del;
}

/**
 * 删除单个项目操作
 */
problemType.deleteOne = function (id) {

    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/helpCenter/deleteOneProblemType", function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                problemType.table.refresh();
            }
        });
        ajax.set("id",id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);

};

/**
 * 点击弹出修改问题分类弹框
 */
problemType.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改问题',
        area: ['50%', '60%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/helpCenter/problemType_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出增加问题分类弹框
 */
problemType.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加问题',
        area: ['50%', '60%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/helpCenter/problemType_add.html'
    });
    this.layerIndex = index;
};

/**
 * 新增广告
 */
problemType.addSubmit = function() {
    var queryData = {};
    var is_show = $('input[name="is_show"]:checked').val();
    queryData['sequence'] = $("#sequence").val();
    queryData['url'] = $("#url").val();
    queryData['name'] = $("#name").val();
    queryData['is_show'] = is_show;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/helpCenter/addProblemType", function(data){
        if(data.code == 200){
            Feng.success("保存成功!");
            parent.problemType.table.refresh();
            problemType.close();
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
problemType.close = function() {
    parent.layer.closeAll();
}

$(function () {
    var defaultColunms = problemType.initColumn();
    var table = new BSTable(problemType.id, "/helpCenter/problemTypeManageList", defaultColunms);
    table.setPaginationType("client");
    problemType.table = table.init();
});
