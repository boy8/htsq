/**
 * 意见反馈
 */
var feedbackManage = {
    id: "feedbackManageTable",	//表格id
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
feedbackManage.initColumn = function () {
    return [
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '手机号', field: 'phone', align: 'center', valign: 'middle'},
        {title: '用户昵称', field: 'nick_name',align: 'center', valign: 'middle'},
        {title: '用户意见反馈', field: 'description', align: 'center', valign: 'middle'},
        {title: '意见反馈时间', field: 'time', align: 'center', valign: 'middle'},
        {title: '回复', field: 'reply', align: 'center', valign: 'middle'},
        {title: '回复时间', field: 'replyTime', align: 'center', valign: 'middle'},
        {title: '操作', field: 'opearte', align: 'center', valign: 'middle', sortable: false, width:'100px',formatter: feedbackManage.formatOperate}
   ];
};

feedbackManage.formatOperate = function (val, row, index) {
    var id = row.id;
    var btn_reply = "<button class='btn btn-primary' onclick='feedbackManage.reply(" + id + ")'><i class='fa fa-edit'></i>回复</button>";
    return btn_reply;
}

/**
 * 回复
 */
feedbackManage.addSubmit = function() {

    var queryData = {};
    queryData['id'] = window.parent.$("#replyId").val();
    queryData['reply'] = $("#reply").val();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/Help/updateSuggestion", function(data){
        if(data.code == 200){
            parent.feedbackManage.table.refresh();
            feedbackManage.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
feedbackManage.close = function() {
    parent.layer.closeAll();
}

/**
 * 点击弹出修改问题弹框
 */
feedbackManage.reply = function (id) {
    $("#replyId").val(id);
    var index = layer.open({
        type: 2,
        title: '回复',
        area: ['40%', '50%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/helpCenter/replyPage'
    });
    this.layerIndex = index;
};

/**
 * 查询表单提交参数对象
 * @returns
 */
feedbackManage.formParams = function() {
    var queryData = {};

    queryData['phone'] = $("#phone").val();

    return queryData;
}

/**
 * 查询列表
 */
feedbackManage.search = function () {
    feedbackManage.table.refresh({query: feedbackManage.formParams()});
};

$(function () {
    var defaultColunms = feedbackManage.initColumn();
    var table = new BSTable(feedbackManage.id, "/helpCenter/feedbackList", defaultColunms);
    table.setPaginationType("client");
    feedbackManage.table = table.init();
});
