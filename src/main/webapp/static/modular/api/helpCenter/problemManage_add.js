/**
 * 新增问题管理
 */
var problemManageAdd = {
    layerIndex: -1,
};

/**
 * 新增广告
 */
problemManageAdd.addSubmit = function() {

    var queryData = {};
    var is_show = $('input[name="is_show"]:checked').val();
    var is_reccomend = $('input[name="is_reccomend"]:checked').val();
    queryData['sequence'] = $("#sequence").val();
    queryData['content'] = $("#content").val();
    queryData['title'] = $("#title").val();
    queryData['type'] = $("#type").val();
    queryData['is_show'] = is_show;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/helpCenter/addProblemManage", function(data){
        if(data.code == 200){
            parent.problemManage.table.refresh();
            problemManageAdd.close();
            Feng.success("修改成功!");
        }else {
            Feng.error(data.message);
        }
    });
    ajax.set(queryData);
    ajax.start();
}

/**
 * 关闭此对话框
 */
problemManageAdd.close = function() {
    parent.layer.closeAll();
}


$(function () {
    var ajax = new $ax(Feng.ctxPath + "/helpCenter/problemTypeList", function (data) {
        if(data.data != null) {
            var str =  $("#type").html();;
            for (var i = 0; i < data.data.length; i++) {
                str += "<option value='" + data.data[i].id + "'>" + data.data[i].title+"</option>";
            }
            $("#type").html(str);
        }
    });
    ajax.start();

});
