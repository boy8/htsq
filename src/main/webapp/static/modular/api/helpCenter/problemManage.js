/**
 * 问题管理
 */
var problemManage = {
    id: "problemManageTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    problemManageInfoData : {},
    delIds:[]           // 批量删除
};

/**
 * 初始化表格的列
 */
problemManage.initColumn = function () {
    return [
        // {field: 'selectItem', checkbox: true,visible: true, align: 'center', valign: 'middle'},
        {title: '编号', formatter: function (value, row, index) {return index+1;}, visible: true, align: 'center', valign: 'middle'},
        {title: '问题分类', field: 'type', visible: true, align: 'center', valign: 'middle'},
        {title: '问题标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: '问题内容', field: 'content', visible: true, width:'35%',align: 'center', valign: 'middle'},
        {title: '顺序', field: 'sequence', visible: true, align: 'center', valign: 'middle'},
        {title: ' 状态 ', field: 'is_show', visible: true, align: 'center', valign: 'middle', formatter: problemManage.formatStatus},
        {title: '操作', field: 'id', visible: true,align: 'center', valign: 'middle', formatter: problemManage.formatOperate}
    ];
};

problemManage.formatOperate = function (val, row, index) {
    var id = row.id;
    var btn_edit = "<button class='btn btn-primary'   onclick='problemManage.update(" + id + ")'><i class='fa fa-edit'></i> 编辑</button>";
    var btn_del = "<button class='btn btn-danger'   onclick='problemManage.deleteOne(" + id + ")'><i class='fa fa-trash-o'></i> 删除</button>";
    return btn_edit + btn_del;
}

problemManage.formatStatus = function (val, row, index) {
    if(row.is_show == "0"){
        return "不显示";
    }
    if(row.is_show == "1") {
        return "显示";
    }
       return "-";
}

/**
 * 检查是否选中
 */
problemManage.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        problemManage.seItem = selected[0];
        problemManage.dealDelIds(selected);
        return true;
    }
};

/**
 * 点击弹出增加问题弹框
 */
problemManage.add = function () {
    var index = layer.open({
        type: 2,
        title: '添加问题',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/helpCenter/problemManage_add.html'
    });
    this.layerIndex = index;
};

/**
 * 点击弹出修改问题弹框
 */
problemManage.update = function (id) {
    $("#updateId").val(id);
    var index = layer.open({
        type: 2,
        title: '修改问题',
        area: ['45%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api/helpCenter/problemManage_edit.html'
    });
    this.layerIndex = index;
};

/**
 * 清除数据
 */
problemManage.clearData = function() {
    this.problemManageInfoData = {};
}

/**
 * 关闭此对话框
 */
problemManage.close = function() {
    parent.layer.closeAll();
}

/**
 * 批量删除项目操作
 */
problemManage.delete = function () {
    if (this.check()) {
         var operation = function(){
             var ajax = new $ax(Feng.ctxPath + "/helpCenter/deleteAllProblem", function (data) {
                 if(data.code == 200) {
                     Feng.success("删除成功!");
                     problemManage.table.refresh();
                 }
                });
                ajax.set("ids",problemManage.delIds);
                ajax.start();
            };
            Feng.confirm("是否刪除选中项目操作?", operation);
         }
};

/**
 * 删除单个项目操作
 */
problemManage.deleteOne = function (id) {

    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/helpCenter/deleteOneProblem", function (data) {
            if(data.code == 200){
                Feng.success("删除成功!");
                problemManage.table.refresh();
            }
        });
        ajax.set("id",id);
        ajax.start();
    };
    Feng.confirm("是否刪除选中项目操作?", operation);

};

/**
 * 查询项目操作列表
 */
problemManage.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    problemManage.table.refresh({query: queryData});
};

// 封装批量删除的ids
problemManage.dealDelIds = function (selected) {
    problemManage.delIds = "";
    for(var i=0;i<selected.length;i++){
        if (selected[i] != null && i != selected.length -1){
            problemManage.delIds += selected[i].id+",";
        }else {
            problemManage.delIds += selected[i].id;
        }
    }
};

$(function () {
    var defaultColunms = problemManage.initColumn();
    var table = new BSTable(problemManage.id, "/helpCenter/problemManageList", defaultColunms);
    table.setPaginationType("client");
    problemManage.table = table.init();
});
