/**
 * 财务管理
 */
var financeManage = {
    id: "financeManageTable",	//表格id
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
financeManage.initColumn = function () {
    return [
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '交易编号', field: 'txid', align: 'center', valign: 'middle'},
        {title: '支付宝姓名', field: 'alipay_user_name',align: 'center', valign: 'middle'},
        {title: '支付宝账号', field: 'alipay_account', align: 'center', valign: 'middle'},
        {title: '手机号码', field: 'phone', align: 'center', valign: 'middle'},
        {title: '提现金额', field: 'money', align: 'center', valign: 'middle'},
        {title: '提现类型', field: 'type', align: 'center', valign: 'middle',formatter: financeManage.formatType},
        {title: '审核状态', field: 'status', align: 'center', valign: 'middle',formatter: financeManage.formatStatus},
        {title: '申请时间', field: 'create_time', align: 'center', valign: 'middle'},
        {title: '审核时间', field: 'approved_time', align: 'center', valign: 'middle'},
        {title: '是否标记', field: 'flag', align: 'center', valign: 'middle',formatter: financeManage.flag},
        {title: '操作', field: 'opearte', align: 'center', valign: 'middle', sortable: false, width:'100px',formatter: financeManage.formatOperate}
   ];
};

financeManage.formatOperate = function (val, row, index) {
    var id = row.id;
    var user_id = row.user_id;
    var flag = row.flag;
    var money = row.money;
    var txType = row.type;
    var btn_edit = "<button class='btn btn-primary' onclick='financeManage.auditing("+id+",1,"+money+",0,"+txType+")'><i class='fa fa-edit'></i> 通过</button>";
    var btn_del = "<button class='btn btn-danger' onclick='financeManage.refuse("+id+",0,"+money+","+txType+")'><i class='fa fa-trash-o'></i> 拒绝</button>";
    var btn_cancel = "<button class='btn btn-warning' onclick='financeManage.auditing("+id+",-2,"+money+",0,"+txType+")'><i class='fa fa-remove'></i> 取消</button>";
    var btn_sign = "<button class='btn btn-label' onclick='financeManage.sign("+user_id+",1)'><i class='fa fa-map-marker'></i> 标记</button>";
    var btn_unsign = "<button class='btn btn-label' onclick='financeManage.sign("+user_id+",0)'><i class='fa fa-map-marker'></i> 取消标记</button>";
    if(row.status == "0"){
        if (flag == "1"){
            return btn_edit +  btn_del+ btn_cancel + btn_unsign;
        }else if (flag == "0"){
            return btn_edit +  btn_del+ btn_cancel + btn_sign;
        }
        return btn_edit +  btn_del+ btn_cancel;
    }else {
        return "-";
    }
}

financeManage.formatStatus = function (val, row, index) {
    if(row.status == "0"){
        return "申请中";
    }
    if(row.status == "1"){
        return "已成功";
    }
    if(row.status == "-1"){
        return "已拒绝";
    }
    if(row.status == "-2"){
        return "已取消";
    }
    return "-";
}

financeManage.formatType = function (val, row, index) {
    if(val == 1){
        return "普通佣金";
    }else if(val == 2){
        return "奖励金";
    }else if(val == 3){
        return "活动佣金";
    }
    return "-";
}
financeManage.flag = function (val, row, index) {
    if(val == 1){
        return "是";
    }else if (val == 0){
        return "否";
    }
    return "-";
}

/**
 * 点击弹出拒绝理由弹框
 */
financeManage.refuse = function (id,type,money,txType) {
    var index = layer.open({
        type: 2,
        title: '拒绝理由',
        area: ['50%', '45%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/view/api//financeManage/finance_refuse.html',
        success: function(){
            var body = layer.getChildFrame('body', financeManage.layerIndex);
            body.find("#id").val(id);
            body.find("#type").val(type);
            body.find("#money").val(money);
            body.find("#txType").val(txType);
        }
    });
    this.layerIndex = index;
};

/**
 * 拒绝
 */
financeManage.addSubmit = function() {
    var id = $("#id").val();
    var type = $("#type").val();
    var money = $("#money").val();
    var reason = $("#reason").val();
    var txType = $("#txType").val();
    financeManage.auditing(id,type,money,reason,txType);
}

/**
 * 查询表单提交参数对象
 * @returns
 */
financeManage.formParams = function() {
    var queryData = {};
    queryData['txid'] = $("#txid").val();
    queryData['alipay_user_name'] = $("#alipay_user_name").val();
    queryData['alipay_account'] = $("#alipay_account").val();
    queryData['status'] = $("#status").val();
    queryData['phone'] = $("#phone").val();
    return queryData;
}

/**
 * 查询列表
 */
financeManage.search = function () {
    financeManage.table.refresh({query: financeManage.formParams()});
};

/**
 * 关闭此对话框
 */
financeManage.close = function() {
    parent.layer.closeAll();
}

/**
 * 审核申请
 * @param id
 */
financeManage.auditing = function (id,type,money,reason,txType) {
    var queryData = {};
    queryData['id'] = id;
    queryData['type'] = type;
    queryData['money'] = money;
    queryData['reason'] = reason;
    queryData['txType'] = txType;
        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/financeManage/auditing", function (data) {
                if (data.code == 200){
                    Feng.success("审核成功!");
                    if (type == 1){
                        financeManage.table.refresh();
                    }else {
                        window.parent.financeManage.table.refresh();
                    }
                    financeManage.close();
                }else {
                    Feng.error(data.message);
                }
            });
            ajax.set(queryData);
            ajax.start();
        };
        Feng.confirm("是否执行此操作?", operation);
};

/**
 * 标记用户
 */
financeManage.sign = function (user_id,flag) {
    var queryData = {};
    queryData['user_id'] = user_id;
    queryData['flag'] = flag;
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/financeManage/flagUser", function (data) {
            if (data.code == 200){
                Feng.success("操作成功!");
                financeManage.table.refresh();
                financeManage.close();
            }else {
                Feng.error(data.message);
            }
        });
        ajax.set(queryData);
        ajax.start();
    };
    Feng.confirm("是否执行此操作?", operation);
};

$(function () {
    var defaultColunms = financeManage.initColumn();
    var table = new BSTablePG(financeManage.id, "/financeManage/list", defaultColunms);
    financeManage.table = table.init();
});
