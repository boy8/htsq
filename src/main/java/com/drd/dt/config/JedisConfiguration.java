package com.drd.dt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedisPool;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan// 解决 Configuration 注解中使用 Autowired 注解 IDE 报错
public class JedisConfiguration {
    @Autowired
    RedisConfig redisConfig;

    @Bean
    public JedisPool redisPoolFactory()  throws Exception{
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(redisConfig.getMaxIdle());
        jedisPoolConfig.setMaxWaitMillis(redisConfig.getMaxWaitMillis());
        jedisPoolConfig.setMaxTotal(redisConfig.getMaxTotal());
        jedisPoolConfig.setTestOnBorrow(redisConfig.getTestOnBorrow());
        Integer port = Integer.valueOf(redisConfig.getPort());
        Integer maxWaitMillis = Integer.valueOf(String.valueOf(redisConfig.getMaxWaitMillis()));
        String host = redisConfig.getUrl();
        String pwd = redisConfig.getPwd();
        JedisPool jedisPool = new JedisPool(jedisPoolConfig,host ,port ,maxWaitMillis ,pwd);
        return jedisPool;
    }
}

