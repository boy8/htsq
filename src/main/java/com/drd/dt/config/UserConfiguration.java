package com.drd.dt.config;

import com.drd.dt.filter.UserInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 拦截器配置
 * @author zhangkai
 */
@Configuration
public class UserConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public UserInterceptor authInterceptor(){
        return new UserInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(authInterceptor()).addPathPatterns();
        super.addInterceptors(registry);
    }
}
