package com.drd.dt.back.dto;

import com.drd.dt.util.EmojiUtil;
import org.apache.http.util.TextUtils;

/**
 * Created by 86514 on 2019/11/19.
 */
public class UserManageDTO {
    private Integer id;
    private Integer offset;
    private String friend_code;
    private String introducer;
    private String introducer_code;
    private String nick_name;
    private Integer level;
    private Integer status;
    private Integer direct_fans;
    private Integer indirect_fans;
    private String phone;
    private String wx_num;
    private String lv_start_time;
    private String estimate;
    private String bounty_accumulated;
    private String activity_accumulated;
    private String owner_estimate;
    private String fans_estimate;
    private String lv_expire_time;
    private String create_time;
    private String alipay_account;
    private String alipay_user_name;

    public String getOwner_estimate() {
        return owner_estimate;
    }

    public void setOwner_estimate(String owner_estimate) {
        this.owner_estimate = owner_estimate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFriend_code() {
        return friend_code;
    }

    public void setFriend_code(String friend_code) {
        this.friend_code = friend_code;
    }

    public String getIntroducer() {
        return introducer;
    }

    public void setIntroducer(String introducer) {
        this.introducer = introducer;
    }

    public String getNick_name() {
        if (!TextUtils.isEmpty(nick_name)){
            nick_name= EmojiUtil.decode(nick_name);
        }
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        if (!TextUtils.isEmpty(nick_name)){
            nick_name = EmojiUtil.encode(nick_name);
        }
        this.nick_name = nick_name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWx_num() {
        return wx_num;
    }

    public void setWx_num(String wx_num) {
        this.wx_num = wx_num;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getLv_start_time() {
        return lv_start_time;
    }

    public void setLv_start_time(String lv_start_time) {
        this.lv_start_time = lv_start_time;
    }

    public String getLv_expire_time() {
        return lv_expire_time;
    }

    public void setLv_expire_time(String lv_expire_time) {
        this.lv_expire_time = lv_expire_time;
    }

    public Integer getDirect_fans() {
        return direct_fans;
    }

    public void setDirect_fans(Integer direct_fans) {
        this.direct_fans = direct_fans;
    }

    public Integer getIndirect_fans() {
        return indirect_fans;
    }

    public void setIndirect_fans(Integer indirect_fans) {
        this.indirect_fans = indirect_fans;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }

    public String getBounty_accumulated() {
        return bounty_accumulated;
    }

    public void setBounty_accumulated(String bounty_accumulated) {
        this.bounty_accumulated = bounty_accumulated;
    }

    public String getFans_estimate() {
        return fans_estimate;
    }

    public void setFans_estimate(String fans_estimate) {
        this.fans_estimate = fans_estimate;
    }

    public String getIntroducer_code() {
        return introducer_code;
    }

    public void setIntroducer_code(String introducer_code) {
        this.introducer_code = introducer_code;
    }

    public String getActivity_accumulated() {
        return activity_accumulated;
    }

    public void setActivity_accumulated(String activity_accumulated) {
        this.activity_accumulated = activity_accumulated;
    }

    public String getAlipay_account() {
        return alipay_account;
    }

    public void setAlipay_account(String alipay_account) {
        this.alipay_account = alipay_account;
    }

    public String getAlipay_user_name() {
        return alipay_user_name;
    }

    public void setAlipay_user_name(String alipay_user_name) {
        this.alipay_user_name = alipay_user_name;
    }
}
