package com.drd.dt.back.dto;

import java.util.Date;

/**
 * SiftOrderLog 精选核桃 商品 订单 售后记录表表 实体
 * */
public class SiftOrderLogDTO{
    private Integer id;
    private String goods_id;
    private String goods_type;
    private String order_id;
    private String cause;
    private String log_image;
    private Integer refund_status;
    private Integer sequence;
    private Date creation_time;
    private String parcel_number;
    private String parcel_type;

    public String getParcel_number() {
        return parcel_number;
    }

    public void setParcel_number(String parcel_number) {
        this.parcel_number = parcel_number;
    }

    public String getParcel_type() {
        return parcel_type;
    }

    public void setParcel_type(String parcel_type) {
        this.parcel_type = parcel_type;
    }

    public Date getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(Date creation_time) {
        this.creation_time = creation_time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_type() {
        return goods_type;
    }

    public void setGoods_type(String goods_type) {
        this.goods_type = goods_type;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getLog_image() {
        return log_image;
    }

    public void setLog_image(String log_image) {
        this.log_image = log_image;
    }

    public Integer getRefund_status() {
        return refund_status;
    }

    public void setRefund_status(Integer refund_status) {
        this.refund_status = refund_status;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }
}