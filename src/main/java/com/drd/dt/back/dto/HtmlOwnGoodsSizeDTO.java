package com.drd.dt.back.dto;

public class HtmlOwnGoodsSizeDTO extends HtmlOwnGoodsDTO {


    /**
     * 扩展：后台管理界面 需要更改的规格
     **/
    private String size;

    /**
     * 扩展：后台管理界面 需要新增的规格
     **/
    private String newsize;

    public String getNewsize() {
        return newsize;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setNewsize(String newsize) {
        this.newsize = newsize;
    }

}
