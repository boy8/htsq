package com.drd.dt.back.dto;

/**
 * Created by 86514 on 2019/4/12.
 */
public class ChannelInfoDTO {
    private String channel_id;
    private Integer userNum = 0;
    private Integer orderNum = 0;
    private Integer userActiveNum = 0;
    private Integer newlyUserNum = 0;
    private Integer orderPeopleNum = 0;
    private String time = "";

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public Integer getUserNum() {
        return userNum;
    }

    public void setUserNum(Integer userNum) {
        this.userNum = userNum;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getUserActiveNum() {
        return userActiveNum;
    }

    public void setUserActiveNum(Integer userActiveNum) {
        this.userActiveNum = userActiveNum;
    }

    public Integer getNewlyUserNum() {
        return newlyUserNum;
    }

    public void setNewlyUserNum(Integer newlyUserNum) {
        this.newlyUserNum = newlyUserNum;
    }

    public Integer getOrderPeopleNum() {
        return orderPeopleNum;
    }

    public void setOrderPeopleNum(Integer orderPeopleNum) {
        this.orderPeopleNum = orderPeopleNum;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "ChannelInfoDTO{" +
                "channel_id='" + channel_id + '\'' +
                ", userNum=" + userNum +
                ", orderNum=" + orderNum +
                ", userActiveNum=" + userActiveNum +
                ", newlyUserNum=" + newlyUserNum +
                ", orderPeopleNum=" + orderPeopleNum +
                ", time='" + time + '\'' +
                '}';
    }
}
