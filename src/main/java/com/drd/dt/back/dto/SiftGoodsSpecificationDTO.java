package com.drd.dt.back.dto;

/**
 * SiftGoodsSpecification 精选核桃 商品 规格 实体
 */
public class SiftGoodsSpecificationDTO {
    private Integer id;
    private String goods_id;
    private String etalon_name;
    private String etalon_value;
    private Integer sequence;
    private String file_path;
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getEtalon_name() {
        return etalon_name;
    }

    public void setEtalon_name(String etalon_name) {
        this.etalon_name = etalon_name;
    }

    public String getEtalon_value() {
        return etalon_value;
    }

    public void setEtalon_value(String etalon_value) {
        this.etalon_value = etalon_value;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}