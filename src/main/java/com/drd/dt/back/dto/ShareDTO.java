package com.drd.dt.back.dto;

import java.util.Arrays;

/**
 * Created by 86514 on 2019/6/12.
 */
public class ShareDTO {
    private Integer id;
    private String name;
    private String head_img;
    private String content;
    private String picture;
    private String[] pictureArr = new String[0];
    private String create_time;
    private String video;
    private Integer type;
    //分享后下载app的应用宝默认地址
    private String downUrl = "www.quanbashi.vip/qbs/redCoin/wx_userInfo?user_id=";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHead_img() {
        return head_img;
    }

    public void setHead_img(String head_img) {
        this.head_img = head_img;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String[] getPictureArr() {
        return pictureArr;
    }

    public void setPictureArr(String[] pictureArr) {
        this.pictureArr = pictureArr;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getDownUrl() {
        return downUrl;
    }

    public void setDownUrl(String downUrl) {
        this.downUrl = downUrl;
    }

    @Override
    public String toString() {
        return "ShareDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", head_img='" + head_img + '\'' +
                ", content='" + content + '\'' +
                ", picture='" + picture + '\'' +
                ", pictureArr=" + Arrays.toString(pictureArr) +
                ", create_time='" + create_time + '\'' +
                ", video='" + video + '\'' +
                ", type=" + type +
                ", downUrl='" + downUrl + '\'' +
                '}';
    }
}
