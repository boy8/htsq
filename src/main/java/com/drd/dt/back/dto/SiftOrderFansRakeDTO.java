package com.drd.dt.back.dto;

import java.util.Date;

/**
 * SiftOrderFansRake 核桃精选粉丝订单表 实体
 */
public class SiftOrderFansRakeDTO {

    private Long id;
    private Integer user_id;
    private Integer status;
    private Integer origin_id;
    private String channel_id;
    private String rake_type;
    private String order_id;
    private Double rake_back;
    private Double user_rake_back;
    private Double rake_rate;
    private Date create_time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(Integer origin_id) {
        this.origin_id = origin_id;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getRake_type() {
        return rake_type;
    }

    public void setRake_type(String rake_type) {
        this.rake_type = rake_type;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public Double getRake_back() {
        return rake_back;
    }

    public void setRake_back(Double rake_back) {
        this.rake_back = rake_back;
    }

    public Double getUser_rake_back() {
        return user_rake_back;
    }

    public void setUser_rake_back(Double user_rake_back) {
        this.user_rake_back = user_rake_back;
    }

    public Double getRake_rate() {
        return rake_rate;
    }

    public void setRake_rate(Double rake_rate) {
        this.rake_rate = rake_rate;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }
}
