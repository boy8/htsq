package com.drd.dt.back.dto;

import java.util.Date;

/**
 * SiftOrder 精选核桃 商品 订单表 实体
 */
public class SiftOrderDTO {
    private Long id;
    private Integer buy_type;
    private String product_id;
    private String product_pic_url;
    private String product_name;
    private Integer user_id;
    private String order_id;
    private Integer status;
    private Integer paytype;
    private Integer number;
    private Double actual_amount;
    private Double rake_back;
    private Double user_rake_back;
    private Double user_rake_back_yugu;
    private Double user_rake;
    private String goods_spe_id;
    private String shipping_address;
    private String shipping_user_name;
    private String shipping_phone;
    private String logistics_num;
    private String trade_no;
    private String buyer_id;
    private String buyer_logon_id;
    private Date update_time;
    private Date create_time;
    private Integer refund_status;
    private Integer redpacket;
    private int discounts_type;

    /***
     * 扩展 id  日志id
     * */
    private Long log_id;
    /***
     * 扩展 id  快递单号
     * */
    private String parcel_number;
    /***
     * 扩展 id   快递公司
     * */
    private String parcel_type;

    /***
     * 扩展 退款收货人信息
     * */
    private String consignee;
    private String consignee_p;
    private String consignee_site;

    public int getDiscounts_type() {
        return discounts_type;
    }

    public void setDiscounts_type(int discounts_type) {
        this.discounts_type = discounts_type;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getConsignee_p() {
        return consignee_p;
    }

    public void setConsignee_p(String consignee_p) {
        this.consignee_p = consignee_p;
    }

    public String getConsignee_site() {
        return consignee_site;
    }

    public void setConsignee_site(String consignee_site) {
        this.consignee_site = consignee_site;
    }

    public String getParcel_number() {
        return parcel_number;
    }

    public void setParcel_number(String parcel_number) {
        this.parcel_number = parcel_number;
    }

    public String getParcel_type() {
        return parcel_type;
    }

    public void setParcel_type(String parcel_type) {
        this.parcel_type = parcel_type;
    }

    public Long getLog_id() {
        return log_id;
    }

    public void setLog_id(Long log_id) {
        this.log_id = log_id;
    }

    public Integer getRedpacket() {
        return redpacket;
    }

    public void setRedpacket(Integer redpacket) {
        this.redpacket = redpacket;
    }

    public Integer getBuy_type() {
        return buy_type;
    }

    public void setBuy_type(Integer buy_type) {
        this.buy_type = buy_type;
    }

    public Double getRake_back() {
        return rake_back;
    }

    public void setRake_back(Double rake_back) {
        this.rake_back = rake_back;
    }

    public Double getUser_rake_back() {
        return user_rake_back;
    }

    public void setUser_rake_back(Double user_rake_back) {
        this.user_rake_back = user_rake_back;
    }

    public Double getUser_rake_back_yugu() {
        return user_rake_back_yugu;
    }

    public void setUser_rake_back_yugu(Double user_rake_back_yugu) {
        this.user_rake_back_yugu = user_rake_back_yugu;
    }

    public Double getUser_rake() {
        return user_rake;
    }

    public void setUser_rake(Double user_rake) {
        this.user_rake = user_rake;
    }

    /**
     * 扩展 goods_spe_id 规格名称集合、单位
     * **/
    private String goods_spe_idText;
    private String goods_units;

    public String getGoods_spe_idText() {
        return goods_spe_idText;
    }

    public String getGoods_units() {
        return goods_units;
    }

    public void setGoods_units(String goods_units) {
        this.goods_units = goods_units;
    }

    public void setGoods_spe_idText(String goods_spe_idText) {
        this.goods_spe_idText = goods_spe_idText;
    }

    public Integer getRefund_status() {
        return refund_status;
    }

    public void setRefund_status(Integer refund_status) {
        this.refund_status = refund_status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_pic_url() {
        return product_pic_url;
    }

    public void setProduct_pic_url(String product_pic_url) {
        this.product_pic_url = product_pic_url;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPaytype() {
        return paytype;
    }

    public void setPaytype(Integer paytype) {
        this.paytype = paytype;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getActual_amount() {
        return actual_amount;
    }

    public void setActual_amount(Double actual_amount) {
        this.actual_amount = actual_amount;
    }

    public String getGoods_spe_id() {
        return goods_spe_id;
    }

    public void setGoods_spe_id(String goods_spe_id) {
        this.goods_spe_id = goods_spe_id;
    }

    public String getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(String shipping_address) {
        this.shipping_address = shipping_address;
    }

    public String getShipping_user_name() {
        return shipping_user_name;
    }

    public void setShipping_user_name(String shipping_user_name) {
        this.shipping_user_name = shipping_user_name;
    }

    public String getShipping_phone() {
        return shipping_phone;
    }

    public void setShipping_phone(String shipping_phone) {
        this.shipping_phone = shipping_phone;
    }

    public String getLogistics_num() {
        return logistics_num;
    }

    public void setLogistics_num(String logistics_num) {
        this.logistics_num = logistics_num;
    }

    public String getTrade_no() {
        return trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }

    public String getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(String buyer_id) {
        this.buyer_id = buyer_id;
    }

    public String getBuyer_logon_id() {
        return buyer_logon_id;
    }

    public void setBuyer_logon_id(String buyer_logon_id) {
        this.buyer_logon_id = buyer_logon_id;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }
}