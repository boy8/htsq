package com.drd.dt.back.dto;

/**
 * Created by 86514 on 2019/11/29.
 */
public class JGPushDTO {
    private Integer id;
    private String title;
    private String content;
    private String goods_id;
    private String img_url;
    private Integer type;
    //1提现列表  2消息中心 3单品详情 4h5
    private Integer jump_action;
    private Integer jump_type;
    private Integer is_login;
    private Integer is_need_userid;
    private Integer discern_id;
    private Integer status;
    private Integer push_type;//推送类型（1单品 2活动）
    private String jump_url;
    private String time;//极光推送时间点
    private String start_time; // 起始时间
    private String end_time;//结束时间
    private String create_time; //创建时间

    public JGPushDTO() {
    }

    public Integer getJump_action() {
        return jump_action;
    }

    public void setJump_action(Integer jump_action) {
        this.jump_action = jump_action;
    }

    public Integer getPush_type() {
        return push_type;
    }

    public void setPush_type(Integer push_type) {
        this.push_type = push_type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getJump_type() {
        return jump_type;
    }

    public void setJump_type(Integer jump_type) {
        this.jump_type = jump_type;
    }

    public Integer getIs_login() {
        return is_login;
    }

    public void setIs_login(Integer is_login) {
        this.is_login = is_login;
    }

    public Integer getIs_need_userid() {
        return is_need_userid;
    }

    public void setIs_need_userid(Integer is_need_userid) {
        this.is_need_userid = is_need_userid;
    }

    public Integer getDiscern_id() {
        return discern_id;
    }

    public void setDiscern_id(Integer discern_id) {
        this.discern_id = discern_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getJump_url() {
        return jump_url;
    }

    public void setJump_url(String jump_url) {
        this.jump_url = jump_url;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public JGPushDTO(Integer id, String title, String content, String goods_id, String img_url, Integer type, Integer jump_type, Integer is_login, Integer is_need_userid, Integer discern_id, Integer status, Integer push_type, String jump_url, String time, String start_time, String end_time, String create_time) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.goods_id = goods_id;
        this.img_url = img_url;
        this.type = type;
        this.jump_type = jump_type;
        this.is_login = is_login;
        this.is_need_userid = is_need_userid;
        this.discern_id = discern_id;
        this.status = status;
        this.push_type = push_type;
        this.jump_url = jump_url;
        this.time = time;
        this.start_time = start_time;
        this.end_time = end_time;
        this.create_time = create_time;
    }
}
