package com.drd.dt.back.dto;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 自营商品
 **/


@ApiModel(value = "自营商品")
@TableName("jz_own_goods")
public class OwnGoodsDTO {
    @TableId(type = IdType.AUTO)
    private Integer dealer_id;
    private String title;
    private String describe;
    private String former_price;
    private String now_price;
    private String coupons_price;
    private String rebate_ratio;
    private Integer inventory;
    private Integer sell;
    private String carousel_imags;
    private String video;
    private Integer state;
    private String show_images;
    private String goods_units;
    private String create_time;
    private Integer type;
    private Integer sequence;

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }



    public Integer getDealer_id() {
        return dealer_id;
    }

    public void setDealer_id(Integer dealer_id) {
        this.dealer_id = dealer_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getFormer_price() {
        return former_price;
    }

    public void setFormer_price(String former_price) {
        this.former_price = former_price;
    }

    public String getNow_price() {
        return now_price;
    }

    public void setNow_price(String now_price) {
        this.now_price = now_price;
    }

    public String getCoupons_price() {
        return coupons_price;
    }

    public void setCoupons_price(String coupons_price) {
        this.coupons_price = coupons_price;
    }

    public String getRebate_ratio() {
        return rebate_ratio;
    }

    public void setRebate_ratio(String rebate_ratio) {
        this.rebate_ratio = rebate_ratio;
    }

    public Integer getInventory() {
        return inventory;
    }

    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    public Integer getSell() {
        return sell;
    }

    public void setSell(Integer sell) {
        this.sell = sell;
    }

    public String getCarousel_imags() {
        return carousel_imags;
    }

    public void setCarousel_imags(String carousel_imags) {
        this.carousel_imags = carousel_imags;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getShow_images() {
        return show_images;
    }

    public void setShow_images(String show_images) {
        this.show_images = show_images;
    }

    public String getGoods_units() {
        return goods_units;
    }

    public void setGoods_units(String goods_units) {
        this.goods_units = goods_units;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
