package com.drd.dt.back.dto;

/**
 * Created by 86514 on 2019/7/10.
 */
public class MessageCenterDTO {
    private Integer id;
    private String title;
    private String content;
    private String head_pic;
    private String create_time;
    private Integer type;
    private Double money;
    private Integer status;
    private Integer user_id;
    private Integer tx_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(String head_pic) {
        this.head_pic = head_pic;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getTx_id() {
        return tx_id;
    }

    public void setTx_id(Integer tx_id) {
        this.tx_id = tx_id;
    }

    @Override
    public String toString() {
        return "MessageCenterDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", head_pic='" + head_pic + '\'' +
                ", create_time='" + create_time + '\'' +
                ", type=" + type +
                ", money=" + money +
                ", status=" + status +
                ", user_id=" + user_id +
                ", tx_id=" + tx_id +
                '}';
    }
}
