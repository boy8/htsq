package com.drd.dt.back.dto;


import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@TableName("jz_business_academy")
public class BusinessAcademyDTO {
    @TableId(type = IdType.AUTO)
    private Integer business_id;
    private String title;
    private Integer first_module;
    private Integer second_module;
    private Integer third_module;
    private Integer type;
    private Integer like_num;
    private Integer transpond_num;
    private Integer comment_num;
    private Integer show_num;
    private String home_image;
    private String ima_s;
    private String video;
    private String text;
    private Integer show_lv;
    private Integer state;
    private String biz_id;
    private String create_time;

    public Integer getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(Integer business_id) {
        this.business_id = business_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getFirst_module() {
        return first_module;
    }

    public void setFirst_module(Integer first_module) {
        this.first_module = first_module;
    }

    public Integer getSecond_module() {
        return second_module;
    }

    public void setSecond_module(Integer second_module) {
        this.second_module = second_module;
    }

    public Integer getThird_module() {
        return third_module;
    }

    public void setThird_module(Integer third_module) {
        this.third_module = third_module;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getHome_image() {
        return home_image;
    }

    public void setHome_image(String home_image) {
        this.home_image = home_image;
    }

    public String getIma_s() {
        return ima_s;
    }

    public void setIma_s(String ima_s) {
        this.ima_s = ima_s;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getShow_lv() {
        return show_lv;
    }

    public void setShow_lv(Integer show_lv) {
        this.show_lv = show_lv;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getBiz_id() {
        return biz_id;
    }

    public void setBiz_id(String biz_id) {
        this.biz_id = biz_id;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Integer getLike_num() {
        return like_num;
    }

    public void setLike_num(Integer like_num) {
        this.like_num = like_num;
    }

    public Integer getTranspond_num() {
        return transpond_num;
    }

    public void setTranspond_num(Integer transpond_num) {
        this.transpond_num = transpond_num;
    }

    public Integer getComment_num() {
        return comment_num;
    }

    public void setComment_num(Integer comment_num) {
        this.comment_num = comment_num;
    }

    public Integer getShow_num() {
        return show_num;
    }

    public void setShow_num(Integer show_num) {
        this.show_num = show_num;
    }
}
