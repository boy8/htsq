package com.drd.dt.back.dto;

/**
 * Created by 86514 on 2019/11/21.
 */
public class OwnGoodsOrderDTO {
    private Integer id;
    private String product_name = "";
    private String order_id;
    private String product_id;
    private String product_pic_url;
    private String create_time;
    private String nick_name;
    private String phone;
    private String alipay_account;
    private String logistics_num = "";
    private Integer status;
    private String actual_amount;
    private String shipping_address;
    private String shipping_user_name;
    private String shipping_phone;
    private String start_time;
    private String end_time;
    private String paytype;
    private String order_status;
    private String trade_no = "";
    private String etalon_value = "";

    /**
     * 扩展： 核桃精选 订单退款状态
     *      兼顾 ：H5独立订单  退款行为 0-没有任何操作 1 退款中 2 退款成功 3 拒绝退款
     **/
    private Integer refund_status;
    /**
     * 扩展： 核桃精选 订单退款状态
     **/
    private String refund_statusText;
    /**
     * 扩展： 核桃精选 支付类型
     **/
    private String paytypeText;
    /**
     * 扩展： 核桃精选 订单状态
     **/
    private String statusText;
    /**
     * 扩展： 核桃精选 订单状态
     **/
    private Integer number;
    /**
     * 扩展： 核桃精选  快递单号
     **/
    private String parcel_number = "";
    /**
     * 扩展： 核桃精选 快递类型
     **/
    private String parcel_type= "";
    /**
     * 扩展： 核桃精选 用户id
     **/
    private Integer user_id;
    /**
     * 扩展： 核桃精选订单优惠类型  ：0 代表核桃精选订单 1代表线下折扣订单
     * */
    private int discounts_type;


    public int getDiscounts_type() {
        return discounts_type;
    }

    public void setDiscounts_type(int discounts_type) {
        this.discounts_type = discounts_type;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getParcel_number() {
        return parcel_number;
    }

    public void setParcel_number(String parcel_number) {
        this.parcel_number = parcel_number;
    }

    public String getParcel_type() {
        return parcel_type;
    }

    public void setParcel_type(String parcel_type) {
        this.parcel_type = parcel_type;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getRefund_statusText() {
        return refund_statusText;
    }

    public void setRefund_statusText(String refund_statusText) {
        this.refund_statusText = refund_statusText;
    }

    public String getPaytypeText() {
        return paytypeText;
    }

    public void setPaytypeText(String paytypeText) {
        this.paytypeText = paytypeText;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getRefund_status() {
        return refund_status;
    }

    public void setRefund_status(Integer refund_status) {
        this.refund_status = refund_status;
    }

    public String getTrade_no() {
        return trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_pic_url() {
        return product_pic_url;
    }

    public void setProduct_pic_url(String product_pic_url) {
        this.product_pic_url = product_pic_url;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getActual_amount() {
        return actual_amount;
    }

    public void setActual_amount(String actual_amount) {
        this.actual_amount = actual_amount;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(String shipping_address) {
        this.shipping_address = shipping_address;
    }

    public String getShipping_user_name() {
        return shipping_user_name;
    }

    public void setShipping_user_name(String shipping_user_name) {
        this.shipping_user_name = shipping_user_name;
    }

    public String getShipping_phone() {
        return shipping_phone;
    }

    public void setShipping_phone(String shipping_phone) {
        this.shipping_phone = shipping_phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getAlipay_account() {
        return alipay_account;
    }

    public void setAlipay_account(String alipay_account) {
        this.alipay_account = alipay_account;
    }

    public String getLogistics_num() {
        return logistics_num;
    }

    public void setLogistics_num(String logistics_num) {
        this.logistics_num = logistics_num;
    }

    public String getEtalon_value() {
        return etalon_value;
    }

    public void setEtalon_value(String etalon_value) {
        this.etalon_value = etalon_value;
    }
}
