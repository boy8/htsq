package com.drd.dt.back.dto;

import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * SiftGoodsDTO 精选核桃 商品 实体
 */
public class SiftGoodsDTO {
    private String id;
    private Integer goods_type;
    private String goods_url;
    private String goods_units;
    private String small_icon;
    private String top_image_list;
    private String top_home_image;
    private String top_void;
    private String title_s;
    private String title;
    private String content;
    private Double former_price;
    private Double now_price;
    private Integer sell;
    private Integer inventory;
    private Integer share_count;
    private Integer dateil_type;
    private String dateil_h5;
    private String dateil_list;
    private String sort;
    private Integer status;
    private Integer sequence;
    private Timestamp creation_time;
    private Timestamp updata_time;
    private Double rake_back;
    private String consignee;
    private String consignee_p;
    private String consignee_site;
    private int discounts_type;

    /**
     * 扩展  精选核桃商品规格集合
     **/
    private List<Map<String, Object>> specificationList;

    /**
     * 扩展 轮播图片列表、详情图片列表
     **/
    private List<String> top_image_listText;
    private List<String> dateil_listText;
    /**
     * 扩展 返佣 根据用户金额计算返佣
     * */
    private Double user_rake_back;

    public int getDiscounts_type() {
        return discounts_type;
    }

    public void setDiscounts_type(int discounts_type) {
        this.discounts_type = discounts_type;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getConsignee_p() {
        return consignee_p;
    }

    public void setConsignee_p(String consignee_p) {
        this.consignee_p = consignee_p;
    }

    public String getConsignee_site() {
        return consignee_site;
    }

    public void setConsignee_site(String consignee_site) {
        this.consignee_site = consignee_site;
    }

    public Double getUser_rake_back() {
        return user_rake_back;
    }

    public void setUser_rake_back(Double user_rake_back) {
        this.user_rake_back = user_rake_back;
    }

    public String getGoods_units() {
        return goods_units;
    }

    public void setGoods_units(String goods_units) {
        this.goods_units = goods_units;
    }

    public List<String> getTop_image_listText() {
        return top_image_listText;
    }

    public void setTop_image_listText(List<String> top_image_listText) {
        this.top_image_listText = top_image_listText;
    }

    public List<String> getDateil_listText() {
        return dateil_listText;
    }

    public void setDateil_listText(List<String> dateil_listText) {
        this.dateil_listText = dateil_listText;
    }

    public List<Map<String, Object>> getSpecificationList() {
        return specificationList;
    }

    public void setSpecificationList(List<Map<String, Object>> specificationList) {
        this.specificationList = specificationList;
    }

    public Double getRake_back() {
        return rake_back;
    }

    public void setRake_back(Double rake_back) {
        this.rake_back = rake_back;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getGoods_type() {
        return goods_type;
    }

    public void setGoods_type(Integer goods_type) {
        this.goods_type = goods_type;
    }

    public String getGoods_url() {
        return goods_url;
    }

    public void setGoods_url(String goods_url) {
        this.goods_url = goods_url;
    }

    public String getSmall_icon() {
        return small_icon;
    }

    public void setSmall_icon(String small_icon) {
        this.small_icon = small_icon;
    }

    public String getTop_image_list() {
        return top_image_list;
    }

    public void setTop_image_list(String top_image_list) {
        this.top_image_list = top_image_list;
        if (!StringUtils.isEmpty(top_image_list)) {
            if (null == this.top_image_listText) {
                this.top_image_listText = new ArrayList<String>();
            }
            if (top_image_list.indexOf(",") != -1) {
                for (String str : top_image_list.split(",")) {
                    this.top_image_listText.add(str);
                }
            } else {
                this.top_image_listText.add(top_image_list);
            }

        }
    }

    public String getTop_home_image() {
        return top_home_image;
    }

    public void setTop_home_image(String top_home_image) {
        this.top_home_image = top_home_image;
    }

    public String getTop_void() {
        return top_void;
    }

    public void setTop_void(String top_void) {
        this.top_void = top_void;
    }

    public String getTitle_s() {
        return title_s;
    }

    public void setTitle_s(String title_s) {
        this.title_s = title_s;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getFormer_price() {
        return former_price;
    }

    public void setFormer_price(Double former_price) {
        this.former_price = former_price;
    }

    public Double getNow_price() {
        return now_price;
    }

    public void setNow_price(Double now_price) {
        this.now_price = now_price;
    }

    public Integer getSell() {
        return sell;
    }

    public void setSell(Integer sell) {
        this.sell = sell;
    }

    public Integer getInventory() {
        return inventory;
    }

    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    public Integer getShare_count() {
        return share_count;
    }

    public void setShare_count(Integer share_count) {
        this.share_count = share_count;
    }

    public Integer getDateil_type() {
        return dateil_type;
    }

    public void setDateil_type(Integer dateil_type) {
        this.dateil_type = dateil_type;
    }

    public String getDateil_h5() {
        return dateil_h5;
    }

    public void setDateil_h5(String dateil_h5) {
        this.dateil_h5 = dateil_h5;
    }

    public String getDateil_list() {
        return dateil_list;
    }

    public void setDateil_list(String dateil_list) {
        this.dateil_list = dateil_list;
        if (!StringUtils.isEmpty(dateil_list)) {
            if (null == this.dateil_listText) {
                this.dateil_listText = new ArrayList<String>();
            }
            if (dateil_list.indexOf(",") != -1) {
                for (String str : dateil_list.split(",")) {
                    this.dateil_listText.add(str);
                }
            } else {
                this.dateil_listText.add(dateil_list);
            }

        }
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Timestamp getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(Timestamp creation_time) {
        this.creation_time = creation_time;
    }

    public Timestamp getUpdata_time() {
        return updata_time;
    }

    public void setUpdata_time(Timestamp updata_time) {
        this.updata_time = updata_time;
    }
}
