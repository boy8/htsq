package com.drd.dt.back.dto;

/**
 * Created by 86514 on 2019/4/12.
 */
public class ChannelDataInfoDTO {
    private int id;
    private String channel;
    private Integer install_num = 0;
    private String time = "";
    private String create_time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getInstall_num() {
        return install_num;
    }

    public void setInstall_num(Integer install_num) {
        this.install_num = install_num;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public ChannelDataInfoDTO() {
    }

    public ChannelDataInfoDTO(int id, String channel, Integer install_num, String time, String create_time) {
        this.id = id;
        this.channel = channel;
        this.install_num = install_num;
        this.time = time;
        this.create_time = create_time;
    }
}
