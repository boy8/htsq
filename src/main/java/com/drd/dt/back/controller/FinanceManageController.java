package com.drd.dt.back.controller;

import com.drd.dt.back.service.IFinanceManageService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 财务管理
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/financeManage")
@Controller
public class FinanceManageController {

    @Autowired
    private IFinanceManageService financeManageService;

    private String path = "/api/financeManage";

    /**
     * 跳转财务管理页
     **/
    @RequestMapping("")
    public String baseConfig() {
        return path + "/finance";
    }

    /**
     * 跳转拒绝申请页
     **/
    @RequestMapping("/refuseReason")
    public String refuse() {
        return path + "/finance_refuse";
    }

    /**
     * 获取财务管理列表
     **/
    @RequestMapping(value = "/list")
    @ResponseBody
    public Tip updateStatus(@RequestParam Map<Object, Object> map) throws Exception {
        Tip data = financeManageService.financeManageList(map);
        return data;
    }

    /**
     * 审核提现申请
     **/
    @RequestMapping(value = "/auditing")
    @ResponseBody
    public Tip auditing(@RequestParam Map<String, String> map) throws Exception {
        Tip data = financeManageService.auditing(map);
        return data;
    }

    /**
     * 标记用户
     **/
    @RequestMapping(value = "/flagUser")
    @ResponseBody
    public Tip flagUser(@RequestParam Map<String, String> map) throws Exception {
        Tip data = financeManageService.flagUser(map);
        return data;
    }

}
