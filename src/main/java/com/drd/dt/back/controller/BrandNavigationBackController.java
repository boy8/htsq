package com.drd.dt.back.controller;

import com.drd.dt.back.entity.BrandNavigation;
import com.drd.dt.back.service.IBrandNavigationBackService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 品牌导航管理
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/brandNavigation")
@RestController
public class BrandNavigationBackController {

    @Autowired
    private IBrandNavigationBackService navigationService;

    /**
     * 获取品牌导航列表
     **/
    @RequestMapping(value = "/list")
    public Tip list(@RequestParam(value = "type",required = false) String type) throws Exception {
        Tip data = navigationService.selectList(type);
        return data;
    }

    /**
     * 新增品牌导航
     **/
    @RequestMapping(value = "/insert")
    public Tip insert(BrandNavigation brandNavigation) throws Exception {
        Tip data = navigationService.insertBrandNavigation(brandNavigation);
        return data;
    }

    /**
     * 删除品牌导航
     **/
    @RequestMapping(value = "/delete")
    public Tip deleteOne(@RequestParam(value = "id") Integer id) throws Exception {
        Tip data = navigationService.delete(id);
        return data;
    }

    /**
     * 修改品牌导航
     **/
    @RequestMapping(value = "/update")
    public Tip update(BrandNavigation brandNavigation) throws Exception {
        Tip data = navigationService.update(brandNavigation);
        return data;
    }

    /**
     * 获取品牌导航详情
     **/
    @RequestMapping(value = "/detailInfo/{id}")
    public Tip advDetailInfo(@PathVariable Integer id) throws Exception {
        Tip data = navigationService.advDetailInfo(id);
        return data;
    }

}
