package com.drd.dt.back.controller;


import com.drd.dt.back.dto.SiftOrderLogDTO;
import com.drd.dt.back.service.ISiftOrderLogBaseService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * SiftOrderLog 精选核桃 商品 订单 售后记录表表 Controller层
 */
@RequestMapping("/siftOrderLogBase")
@RestController
public class SiftOrderLogBaseController {
    @Autowired
    private ISiftOrderLogBaseService siftOrderLogBaseService;

    @RequestMapping(value = "/insert")
    public Tip insert(SiftOrderLogDTO siftOrderLog) {
        Tip tip = siftOrderLogBaseService.insert(siftOrderLog);
        return tip;
    }

    @RequestMapping(value = "/update")
    public Tip update(SiftOrderLogDTO siftOrderLog) {
        Tip tip = siftOrderLogBaseService.update(siftOrderLog);
        return tip;
    }

    @RequestMapping(value = "/delete")
    public Tip delete(Integer id) {
        Tip tip = siftOrderLogBaseService.delete(id);
        return tip;
    }

    @RequestMapping(value = "/queryAll")
    public Tip queryAll(Integer id) {
        Tip tip = siftOrderLogBaseService.queryAll(id);
        return tip;
    }

    @RequestMapping(value = "/pageSelect")
    public Tip pageSelect(SiftOrderLogDTO siftOrderLog) {
        Tip tip = siftOrderLogBaseService.pageSelect(siftOrderLog);
        return tip;
    }

}
