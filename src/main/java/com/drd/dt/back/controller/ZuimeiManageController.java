package com.drd.dt.back.controller;

import com.drd.dt.back.service.IzuimeiManageService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 最美天气-小美贝商品
 */
@RequestMapping("/zuimei")
@RestController
public class ZuimeiManageController {

    @Autowired
    private IzuimeiManageService zuimeiManageService;

    /**
     * 小美贝商品列表
     **/
    @RequestMapping(value = "/list")
    public Tip list(@RequestParam Map<String, String> map) throws Exception {
        Tip data = zuimeiManageService.list(map);
        return data;
    }

    /**
     * 新增
     **/
    @RequestMapping(value = "/insert")
    public Tip insert(@RequestParam Map<String, String> map) throws Exception {
        Tip data = zuimeiManageService.insert(map);
        return data;
    }

    /**
     * 修改
     **/
    @RequestMapping(value = "/update")
    public Tip update(@RequestParam Map<String, String> map) throws Exception {
        Tip data = zuimeiManageService.update(map);
        return data;
    }

    /**
     * 删除
     **/
    @RequestMapping(value = "/delete/{id}")
    public Tip delete(@PathVariable Integer id) throws Exception {
        Tip data = zuimeiManageService.delete(id);
        return data;
    }

    /**
     * 详情
     **/
    @GetMapping(value = "/detail")
    public Tip detail(@RequestParam("id") Integer id) throws Exception {
        Tip data = zuimeiManageService.detail(id);
        return data;
    }
}
