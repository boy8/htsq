package com.drd.dt.back.controller;

import com.drd.dt.back.service.IDataReportService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/dataReport")
@Controller
public class DataReportController {

    @Autowired
    private IDataReportService dataReportService;

    private String path = "/api/dataReport";

    /**
     * 跳转数据报表信息页
     **/
    @RequestMapping("")
    public String index() {
        return path + "/dataReport";
    }

    /**
     * 数据报表信息统计-渠道汇总
     **/
    @RequestMapping(value = "/channelList")
    @ResponseBody
    public Tip channelList(@RequestParam Map<String, String> map) throws Exception{
        Tip data = dataReportService.channelList(map);
        return data;
    }

    /**
     * 数据报表信息统计-平台汇总
     **/
    @RequestMapping(value = "/terraceList")
    @ResponseBody
    public Tip terraceList(@RequestParam Map<String, String> map) throws Exception{
        Tip data = dataReportService.terraceList(map);
        return data;
    }

    /**
     * 数据报表信息统计-订单收益汇总
     **/
    @RequestMapping(value = "/incomeList")
    @ResponseBody
    public Tip incomeList(@RequestParam Map<String, String> map) throws Exception{
        Tip data = dataReportService.incomeList(map);
        return data;
    }

    /**
     * 数据报表信息统计-订单汇总
     **/
    @RequestMapping(value = "/rateList")
    @ResponseBody
    public Tip rateList(@RequestParam Map<String, String> map) throws Exception{
        Tip data = dataReportService.rateList(map);
        return data;
    }

    /**
     * 数据报表信息统计-自营订单收益汇总
     **/
    @RequestMapping(value = "/ownOrderList")
    @ResponseBody
    public Tip ownOrderList(@RequestParam Map<String, String> map) throws Exception{
        Tip data = dataReportService.ownOrderList(map);
        return data;
    }

    /**
     * 数据报表信息统计-活跃统计
     **/
    @RequestMapping(value = "/activityList")
    @ResponseBody
    public Tip activityList(@RequestParam Map<String, String> map) throws Exception{
        Tip data = dataReportService.activityList(map);
        return data;
    }
}
