package com.drd.dt.back.controller;

import com.drd.dt.back.dto.SiftGoodsSpecificationDTO;
import com.drd.dt.back.service.ISiftGoodsSpecificationBaseService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * SiftGoodsSpecification 精选核桃 商品 规格 Controller层
 */
@RequestMapping("/siftGoodsSpecificationBase")
@RestController
public class SiftGoodsSpecificationBaseController {
    @Autowired
    private ISiftGoodsSpecificationBaseService siftGoodsSpecificationBaseService;

    @RequestMapping(value = "/insert")
    public Tip insert(SiftGoodsSpecificationDTO siftGoodsSpecificationDTO) {
        Tip tip = siftGoodsSpecificationBaseService.insert(siftGoodsSpecificationDTO);
        return tip;
    }

    @RequestMapping(value = "/update")
    public Tip update(SiftGoodsSpecificationDTO siftGoodsSpecificationDTO) {
        Tip tip = siftGoodsSpecificationBaseService.update(siftGoodsSpecificationDTO);
        return tip;
    }

    @RequestMapping(value = "/delete")
    public Tip delete(String id) {
        Tip tip = siftGoodsSpecificationBaseService.delete(id);
        return tip;
    }

    @RequestMapping(value = "/queryAll/{id}")
    public Tip queryAll(@PathVariable String id) {
        Tip tip = siftGoodsSpecificationBaseService.queryAll(id);
        return tip;
    }

    @RequestMapping(value = "/pageSelect/{good_id}")
    public Tip pageSelect(@PathVariable String good_id) {
        Tip tip = siftGoodsSpecificationBaseService.pageSelect(good_id);
        return tip;
    }

}