package com.drd.dt.back.controller;


import com.drd.dt.back.dto.GoodsSpecificationDTO;
import com.drd.dt.back.entity.GoodsSpecification;
import com.drd.dt.back.service.IGoodsSpecificationBaseService;
import com.drd.dt.back.service.Impl.GoodsSpecificationBaseServiceImpl;
import com.drd.dt.common.tips.Tip;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * 后台自营商品规格设置
 */

@RequestMapping("/goodsSpecificationBase")
@Controller
public class GoodsSpecificationBaseController {
    @Autowired
    private IGoodsSpecificationBaseService goodsSpecificationBaseService;


    /**
     * 新增规格
     */
    @PostMapping("/addSpecifcation")
    @ResponseBody
    public Tip addGoodsSpecificationBase( GoodsSpecificationDTO goodsSpecification) {
        Tip tip = goodsSpecificationBaseService.addGoodsSpecificationBase(goodsSpecification);
        return tip;
    }

    /**
     * 修改规格
     */
    @PostMapping("/updateSpecifcation")
    @ResponseBody
    public Tip updateGoodsSpecificationBase( GoodsSpecificationDTO goodsSpecification) {
        Tip tip = goodsSpecificationBaseService.updateGoodsSpecificationBase(goodsSpecification);
        return tip;
    }


    /**
     * 查询单个
     */
    @PostMapping("/finaAll")
    @ResponseBody
    public Tip finaAll(@RequestParam(value = "id") Integer id) {
        Tip tip = goodsSpecificationBaseService.finaAll(id);
        return tip;
    }


    /**
     * 删除
     */
    @PostMapping("/delete")
    @ResponseBody
    public Tip delete(@RequestParam(value = "id") Integer id) {
        Tip tip = goodsSpecificationBaseService.delete(id);
        return tip;
    }

    /**
     * 查询规格列表  默认 返回一百条
     */
    @GetMapping("/selectSpecifcation")
    @ResponseBody
    //查询商品规格信息
    public Tip selectGoodsSpecificationDetail(@RequestParam(value = "id") Integer id) {
        Tip tip = goodsSpecificationBaseService.selectGoodsSpecificationDetail(id);
        return tip;
    }


}
