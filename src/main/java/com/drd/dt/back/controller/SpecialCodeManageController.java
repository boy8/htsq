package com.drd.dt.back.controller;

import com.drd.dt.back.service.SpecialCodeManageService;
import com.drd.dt.common.tips.Tip;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 特殊邀请码
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/specialCode")
@RestController
public class SpecialCodeManageController {

    @Resource(name = "specialCodeManageService")
    private SpecialCodeManageService specialCodeManageService;

    /**
     * 特殊邀请码列表
     **/
    @RequestMapping(value = "/list")
    public Tip list(@RequestParam Map<String, String> map) throws Exception {
        Tip data = specialCodeManageService.list(map);
        return data;
    }

    /**
     * 新增
     **/
    @RequestMapping(value = "/insert")
    public Tip insert(@RequestParam Map<String, String> map) throws Exception {
        Tip data = specialCodeManageService.insert(map);
        return data;
    }

    /**
     * 修改
     **/
    @RequestMapping(value = "/update")
    public Tip update(@RequestParam Map<String, String> map) throws Exception {
        Tip data = specialCodeManageService.update(map);
        return data;
    }

    /**
     * 删除
     **/
    @RequestMapping(value = "/delete/{id}")
    public Tip delete(@PathVariable Integer id) throws Exception {
        Tip data = specialCodeManageService.delete(id);
        return data;
    }

    /**
     * 详情
     **/
    @PostMapping(value = "/detail/{id}")
    public Tip detail(@PathVariable Integer id) throws Exception {
        Tip data = specialCodeManageService.detail(id);
        return data;
    }

}
