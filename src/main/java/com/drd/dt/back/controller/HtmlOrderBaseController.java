package com.drd.dt.back.controller;

import com.drd.dt.back.dto.*;
import com.drd.dt.back.service.HtmlOrderBaseService;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.HtmlOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/***
 * 脱离核桃用户ID H5 基础功能
 * HL 2020/7/2
 */
@RestController
@RequestMapping("/htmlOrderBase")
public class HtmlOrderBaseController {
    @Autowired
    private HtmlOrderBaseService htmlOrderBaseService;
    /**
     * 订单列表
     */
    @GetMapping("/orderList")
    Tip orderList(OwnGoodsOrderDTO orderDTO) throws Exception {
        return htmlOrderBaseService.orderList(orderDTO);
    }


    /**
     * 自营订单详情
     */
    @PostMapping("/orderDetail/{id}")
    Tip orderDetail(@PathVariable Integer id) throws Exception {
        return htmlOrderBaseService.orderDetail(id);
    }

    /**
     * 修改自营订单
     */
    @PostMapping("/orderUpdate")
    Tip orderUpdate(OwnGoodsOrderDTO orderDTO) throws Exception {
        return htmlOrderBaseService.orderUpdate(orderDTO);
    }

    /**
     * 导出自营订单数据
     **/
    @RequestMapping(value = "/excelExport")
    @ResponseBody
    public void excelExport(OwnGoodsOrderDTO orderDTO,HttpServletResponse response) throws Exception{
        htmlOrderBaseService.excelExport(orderDTO,response);
    }

    /**************************商品后端功能**************************************/
    /**
     * 查询数据
     */
    @GetMapping("/selectAll")
    Tip selectAll(@RequestParam(value = "title",required = false) String title,
                  @RequestParam(value = "state",required = false) String state) throws Exception {
        return htmlOrderBaseService.selectAll(title,state);
    }

    /**
     * 删除数据
     */
    @ApiOperation(value = "后台-删除", notes = "单个数据删除", response = HttpMessage.class)
    @PostMapping("/delete")
    Tip delete(@RequestParam("id") Integer id) throws Exception {
        return htmlOrderBaseService.delete(id);
    }

    /**
     * 添加数据
     */
    @ApiOperation(value = "后台-新增", notes = "新增数据", response = HttpMessage.class)
    @PostMapping("/insertOwnGoods")
    public Tip insertOwnGoods(OwnGoodsInsertDTO ownGoods) throws Exception {
        return htmlOrderBaseService.addOwnGoods(ownGoods);
    }

    /**
     * 修改数据
     */
    @ApiOperation(value = "后台-修改", notes = "单个数据修改", response = HttpMessage.class)
    @PostMapping("/update")
    Tip update(HtmlOwnGoodsSizeDTO ownGoods) throws Exception {
        return htmlOrderBaseService.update(ownGoods);
    }

    /**
     * 查询单条数据
     * **/
    @ApiOperation(value = "后台-查询数据单条数据", notes = "查询数据单条数据", response = HttpMessage.class)
    @PostMapping("/detail/{id}")
    public Tip detail(@PathVariable Integer id) throws Exception {
        return htmlOrderBaseService.detail(id);
    }


    /**************************商品规格后端功能**************************************/
    /**
     * 查询规格列表  默认 返回一百条
     */
    @GetMapping("/selectSpecifcation")
    @ResponseBody
    //查询商品规格信息
    public Tip selectGoodsSpecificationDetail(@RequestParam(value = "id") Integer id) {
        Tip tip = htmlOrderBaseService.selectGoodsSpecificationDetail(id);
        return tip;
    }

    /**
     * 新增规格
     */
    @PostMapping("/addSpecifcation")
    @ResponseBody
    public Tip addGoodsSpecificationBase( GoodsSpecificationDTO goodsSpecification) {
        Tip tip = htmlOrderBaseService.addGoodsSpecificationBase(goodsSpecification);
        return tip;
    }

    /**
     * 查询单个
     */
    @PostMapping("/finaAll")
    @ResponseBody
    public Tip finaAll(@RequestParam(value = "id") Integer id) {
        Tip tip = htmlOrderBaseService.finaAll(id);
        return tip;
    }

    /**
     * 修改规格
     */
    @PostMapping("/updateSpecifcation")
    @ResponseBody
    public Tip updateGoodsSpecificationBase( GoodsSpecificationDTO goodsSpecification) {
        Tip tip = htmlOrderBaseService.updateGoodsSpecificationBase(goodsSpecification);
        return tip;
    }

    /**
     * 删除
     */
    @PostMapping("/deleteSpecification")
    @ResponseBody
    public Tip deleteSpecification(@RequestParam(value = "id") Integer id) {
        Tip tip = htmlOrderBaseService.deleteSpecification(id);
        return tip;
    }



    //后端管理界面直接退款
    @RequestMapping(value = "/applyFor", method = {RequestMethod.POST, RequestMethod.GET})
    public Tip applyFor(@ApiParam(value = "主见id") @RequestParam(value = "id") Integer id)throws Exception {
        return htmlOrderBaseService.applyFor(id);
    }

    /**************************  统计功能  **************************************/
    //统计 0-24小时订单时间段
    @RequestMapping(value = "/dataReportHtmlOrder", method = {RequestMethod.POST, RequestMethod.GET})
    public Tip dataReportHtmlOrder()throws Exception {
        return htmlOrderBaseService.dataReportHtmlOrder();
    }

}
