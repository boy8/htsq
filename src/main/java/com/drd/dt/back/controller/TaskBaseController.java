package com.drd.dt.back.controller;

import com.drd.dt.back.service.ITaskBaseService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 测试服务器  测试功能 - 任务中心数据修改接口
 * 提示：改接口只能用于测试服务器数据
 */
@RequestMapping("/taskBase")
@RestController
public class TaskBaseController {

    @Autowired
    private ITaskBaseService iTaskBaseService;

    //查询金币信息
    @RequestMapping(value = "/selectCoin")
    public Tip selectCoin(@RequestParam(value = "user_id") int user_id) {
        return iTaskBaseService.selectCoin(user_id);
    }

    //修改任务中心金币信息
    @RequestMapping(value = "/updateMyComeGoOutGold")
    public Tip updateMyComeGoOutGold(@RequestParam(value = "user_id") int user_id,
                                     @RequestParam(value = "coin_num") int coin_num,
                                     @RequestParam(value = "gold_coin") int gold_coin,
                                     @RequestParam(value = "my_coin_num") int my_coin_num) {
        return iTaskBaseService.updateMyComeGoOutGold(user_id, coin_num, gold_coin, my_coin_num);
    }

    //查询任务
    @RequestMapping(value = "/selectTaskList")
    public Tip selectTaskList(@RequestParam(value = "user_id") int user_id) {
        return iTaskBaseService.selectTaskList(user_id);
    }

    //任务修改 格式： id/任务类型/需求完成次数/填写完成次数
    @RequestMapping(value = "/updateTaskS")
    public Tip updateTaskS(@RequestParam(value = "str") String str, @RequestParam(value = "user_id") int user_id) {
        return iTaskBaseService.updateTaskS(str, user_id);
    }

}
