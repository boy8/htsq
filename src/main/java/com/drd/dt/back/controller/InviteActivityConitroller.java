package com.drd.dt.back.controller;


import com.drd.dt.back.service.IInviteActivityService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 邀请赚 后端管理界面接口开发
 * 2020/6/18 HL
 * */
@RequestMapping("/inviteActivity")
@RestController
public class InviteActivityConitroller {

    @Autowired
    private IInviteActivityService iInviteActivityService;

    @RequestMapping("/dataReport")
    public Tip dataReport(){
        Tip tip  = iInviteActivityService.dataReport();
        return tip;
    }
}
