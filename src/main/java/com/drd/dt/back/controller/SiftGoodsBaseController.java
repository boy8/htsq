package com.drd.dt.back.controller;

import com.drd.dt.back.dto.SiftGoodsDTO;
import com.drd.dt.back.service.ISiftGoodsBaseService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * SiftGoodsDTO 精选核桃 商品 Controller层
 */
@RequestMapping("/siftGoodsBase")
@RestController
public class SiftGoodsBaseController {
    @Autowired
    private ISiftGoodsBaseService siftGoodsService;

    @RequestMapping(value = "/insert")
    public Tip insert(SiftGoodsDTO siftGoodsDTO) {
        Tip tip = siftGoodsService.insert(siftGoodsDTO);
        return tip;
    }

    @RequestMapping(value = "/update")
    public Tip update(SiftGoodsDTO siftGoodsDTO) {
        Tip tip = siftGoodsService.update(siftGoodsDTO);
        return tip;
    }

    @RequestMapping(value = "/delete")
    public Tip delete(String id) {
        Tip tip = siftGoodsService.delete(id);
        return tip;
    }

    @RequestMapping(value = "/queryAll/{id}")
    public Tip queryAll(@PathVariable String id) {
        Tip tip = siftGoodsService.queryAll(id);
        return tip;
    }

    @RequestMapping(value = "/pageSelect")
    public Tip pageSelect(SiftGoodsDTO siftGoodsDTO) {
        Tip tip = siftGoodsService.pageSelect(siftGoodsDTO);
        return tip;
    }

}