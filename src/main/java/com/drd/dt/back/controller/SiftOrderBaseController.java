package com.drd.dt.back.controller;

import com.drd.dt.back.dto.OwnGoodsOrderDTO;
import com.drd.dt.back.dto.SiftOrderDTO;
import com.drd.dt.back.service.ISiftOrderBaseService;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.ISiftOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * SiftOrder 精选核桃 商品 订单表 Controller层
 */
@RequestMapping("/siftOrderBase")
@RestController
public class SiftOrderBaseController {
    @Autowired
    private ISiftOrderBaseService siftOrderBaseService;
    @Autowired
    private ISiftOrderService siftOrderService;

    @RequestMapping(value = "/insert")
    public Tip insert(SiftOrderDTO siftOrderDTO) {
        Tip tip = siftOrderBaseService.insert(siftOrderDTO);
        return tip;
    }

    /**
     * 该功能目前就 只是修改订单状态
     * **/
    @RequestMapping(value = "/update")
    public Tip update(SiftOrderDTO siftOrderDTO) {
        Tip tip = siftOrderBaseService.update(siftOrderDTO);
        return tip;
    }

    @RequestMapping(value = "/delete")
    public Tip delete(Integer id) {
        Tip tip = siftOrderBaseService.delete(id);
        return tip;
    }

    @RequestMapping(value = "/queryAll/{id}")
    public Tip queryAll(@PathVariable Integer id) {
        Tip tip = siftOrderBaseService.queryAll(id);
        return tip;
    }

    @RequestMapping(value = "/pageSelect")
    public Tip pageSelect(OwnGoodsOrderDTO ownGoodsOrderDTO) {
        Tip tip = siftOrderBaseService.pageSelect(ownGoodsOrderDTO);
        return tip;
    }


    /**
     * 导出自营订单数据
     **/
    @RequestMapping(value = "/excelExport")
    @ResponseBody
    public void excelExport(OwnGoodsOrderDTO orderDTO, HttpServletResponse response) throws Exception{
        siftOrderBaseService.excelExport(orderDTO,response);
    }

    /**
     * 后端管理界面人工 审核 退款
     * **/
    @RequestMapping(value = "/refundStatusUpdate")
    public Tip refundStatusUpdate(SiftOrderDTO siftOrderDTO) throws Exception {
        Tip tip = siftOrderService.refundMoney(siftOrderDTO.getId().intValue(),
                siftOrderDTO.getOrder_id(),
                siftOrderDTO.getUser_id() ,
                siftOrderDTO.getRefund_status());
        return tip;
    }

}