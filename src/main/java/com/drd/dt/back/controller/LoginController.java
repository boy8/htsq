package com.drd.dt.back.controller;

import com.drd.dt.back.service.ILoginService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/login")
@Controller
public class LoginController{

    @Autowired
    private ILoginService loginService;

    private String path = "";

//    @Override
//    public String setPath() {
//        return path;
//    }

    /**
     * 跳转首页
     **/
    @RequestMapping("/index")
    public String index() {
        return "/index";
    }
    /**
     * 跳转登录页
     **/
    @RequestMapping("/loginpage")
    public String loginpage() {
        return "/login";
    }

    /**
     * 登录
     **/
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public Tip login(@RequestParam Map<Object, Object> map) throws Exception{
        Tip data = loginService.login(map);
        return data;
    }
}
