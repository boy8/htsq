package com.drd.dt.back.controller;

import com.drd.dt.back.dto.SiftOrderFansRakeDTO;
import com.drd.dt.back.service.ISiftOrderFansRakeBaseService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * SiftOrderFansRake 核桃精选粉丝订单表 Controller层
 */
@RequestMapping("/siftOrderFansRakeBase")
@RestController
public class SiftOrderFansRakeBaseController {
    @Autowired
    private ISiftOrderFansRakeBaseService siftOrderFansRakeServiceImpl;

    @RequestMapping(value = "/insert")
    public Tip insert(SiftOrderFansRakeDTO siftOrderFansRake) {
        Tip res = siftOrderFansRakeServiceImpl.insert(siftOrderFansRake);
        return res;
    }

    @RequestMapping(value = "/update")
    public Tip update(SiftOrderFansRakeDTO siftOrderFansRake) {
        Tip res = siftOrderFansRakeServiceImpl.update(siftOrderFansRake);
        return res;
    }

    @RequestMapping(value = "/delete")
    public Tip delete(Integer id) {
        Tip res = siftOrderFansRakeServiceImpl.delete(id);
        return res;
    }

    @RequestMapping(value = "/queryAll")
    public Tip queryAll(Integer id) {
        Tip res = siftOrderFansRakeServiceImpl.queryAll(id);
        return res;
    }

    @RequestMapping(value = "/pageSelect")
    public Tip pageSelect(SiftOrderFansRakeDTO siftOrderFansRake) {
        Tip res = siftOrderFansRakeServiceImpl.pageSelect(siftOrderFansRake);
        return res;
    }

}
