package com.drd.dt.back.controller;

import com.drd.dt.back.service.IChannelInfoService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/channel")
@Controller
public class ChannelInfoController {

    @Autowired
    private IChannelInfoService channelInfoService;

    private String path = "/api/channelInfo";

    /**
     * 跳转渠道信息页
     **/
    @RequestMapping("")
    public String index() {
        return path + "/channelInfo";
    }

    /**
     * 渠道信息统计
     **/
    @RequestMapping(value = "/list")
    @ResponseBody
    public Tip channelInfo(@RequestParam Map<Object, Object> map) throws Exception{
        Tip data = channelInfoService.channelInfo(map);
        return data;
    }
}
