package com.drd.dt.back.controller;

import com.drd.dt.back.entity.MessageActivity;
import com.drd.dt.back.service.INoticeManageService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 公告管理
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/notice")
@RestController
public class noticeManagerController {

    @Autowired
    private INoticeManageService noticeManageService;

    /**
     * 公告列表
     **/
    @RequestMapping(value = "/list")
    public Tip list() throws Exception {
        Tip data = noticeManageService.list();
        return data;
    }

    /**
     * 新增公告
     **/
    @RequestMapping(value = "/add")
    public Tip add(MessageActivity activity) throws Exception {
        Tip data = noticeManageService.add(activity);
        return data;
    }

    /**
     * 删除公告
     **/
    @RequestMapping(value = "/delete/{id}")
    public Tip delete(@PathVariable Integer id) throws Exception {
        Tip data = noticeManageService.delete(id);
        return data;
    }

    /**
     * 公告详情
     **/
    @RequestMapping(value = "/detail/{id}")
    public Tip detail(@PathVariable Integer id) throws Exception {
        Tip data = noticeManageService.detail(id);
        return data;
    }

    /**
     * 修改公告
     **/
    @RequestMapping(value = "/update")
    public Tip update(MessageActivity activity) throws Exception {
        Tip data = noticeManageService.update(activity);
        return data;
    }


}
