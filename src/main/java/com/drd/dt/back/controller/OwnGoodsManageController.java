package com.drd.dt.back.controller;

import com.drd.dt.back.dto.OwnGoodsInsertDTO;
import com.drd.dt.back.dto.OwnGoodsOrderDTO;
import com.drd.dt.back.dto.OwnGoodsSizeDTO;
import com.drd.dt.back.service.IOwnGoodsManageService;
import com.drd.dt.common.tips.Tip;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * 自营商品和订单管理
 * Created by 86514 on 2019/7/28.
 */
@RestController
@RequestMapping("ownGoods")
public class OwnGoodsManageController {

    @Autowired
    private IOwnGoodsManageService ownGoodsBaseService;

    /**
     * 查询数据
     */
    @GetMapping("/selectAll")
    Tip selectAll(@RequestParam(value = "title",required = false) String title,
                  @RequestParam(value = "state",required = false) String state) throws Exception {
        return ownGoodsBaseService.selectAll(title,state);
    }

    /**
     * 添加数据
     */
    @ApiOperation(value = "后台-新增", notes = "新增数据", response = HttpMessage.class)
    @PostMapping("/insertOwnGoods")
    public Tip insertOwnGoods(OwnGoodsInsertDTO ownGoods) throws Exception {
        return ownGoodsBaseService.addOwnGoods(ownGoods);
    }

    /**
     * 修改数据
     */
    @ApiOperation(value = "后台-修改", notes = "单个数据修改", response = HttpMessage.class)
    @PostMapping("/update")
    Tip update(OwnGoodsSizeDTO ownGoods) throws Exception {
        return ownGoodsBaseService.update(ownGoods);
    }

    /**
     * 删除数据
     */
    @ApiOperation(value = "后台-删除", notes = "单个数据删除", response = HttpMessage.class)
    @PostMapping("/delete")
    Tip delete(@RequestParam("id") Integer id) throws Exception {
        return ownGoodsBaseService.delete(id);
    }

    /**
     * 查询单条数据
     * **/
    @ApiOperation(value = "后台-查询数据单条数据", notes = "查询数据单条数据", response = HttpMessage.class)
    @PostMapping("/detail/{id}")
    public Tip detail(@PathVariable Integer id) throws Exception {
        return ownGoodsBaseService.detail(id);
    }

    /**
     * 自营订单列表
     */
    @GetMapping("/orderList")
    Tip orderList(OwnGoodsOrderDTO orderDTO) throws Exception {
        return ownGoodsBaseService.orderList(orderDTO);
    }

    /**
     * 自营订单详情
     */
    @PostMapping("/orderDetail/{id}")
    Tip orderDetail(@PathVariable Integer id) throws Exception {
        return ownGoodsBaseService.orderDetail(id);
    }

    /**
     * 修改自营订单
     */
    @PostMapping("/orderUpdate")
    Tip orderUpdate(OwnGoodsOrderDTO orderDTO) throws Exception {
        return ownGoodsBaseService.orderUpdate(orderDTO);
    }

    /**
     * 导出自营订单数据
     **/
    @RequestMapping(value = "/excelExport")
    @ResponseBody
    public void excelExport(OwnGoodsOrderDTO orderDTO,HttpServletResponse response) throws Exception{
        ownGoodsBaseService.excelExport(orderDTO,response);
    }

}
