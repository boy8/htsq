package com.drd.dt.back.controller;

import com.drd.dt.back.dto.JGPushDTO;
import com.drd.dt.back.service.JGPushService;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.util.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 极光推送接口定义
 */

@RestController
@RequestMapping("push")
public class JGPushController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JGPushService pushService;

    /**
     * 消息推送
     **/
    @PostMapping("/pushMsg")
    public Tip pushMsg(JGPushDTO pushDTO) throws Exception {
        String time = pushDTO.getTime(); //推送开始时间

        //定时推送
        if (!StringUtils.isEmpty(time)) {
            Date startTime = Constant.y_M_d_H_m_s.parse(time);
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {//定时
                public void run() {
                    try {
                        pushService.pushMsg(pushDTO);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, startTime);
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        } else {
            pushService.pushMsg(pushDTO);
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        }
    }


    /**
     * 查询 激光列表 默认分页 100
     **/
    @GetMapping("/pushList")
    public Tip pushList(@RequestParam(value = "page", required = false) int page,
                        @RequestParam(value = "size", required = false) int size) {
        return pushService.pushList(page, size);
    }

    /**
     * 查询详情
     **/
    @RequestMapping("/detail/{id}")
    public Tip pushList(@PathVariable("id") Integer id) {
        return pushService.detail(id);
    }

    /**
     * 更新
     **/
    @RequestMapping("/update")
    public Tip update(JGPushDTO pushDTO) {
        return pushService.update(pushDTO);
    }

    /**
     * 删除
     **/
    @RequestMapping("/delete")
    public Tip delete(@RequestParam("id") Integer id) {
        return pushService.delete(id);
    }

}
