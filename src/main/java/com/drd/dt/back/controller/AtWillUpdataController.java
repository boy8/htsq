package com.drd.dt.back.controller;

import com.drd.dt.back.service.IAtWillUpdataService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.entity.Order;
import com.drd.dt.modular.entity.UserMoney;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping("/atWillUpdata")
@RestController
public class AtWillUpdataController {

    @Autowired
    private IAtWillUpdataService atWillUpdataService;


    /**
     * 获取 用户的收益信息
     **/
    @RequestMapping(value = "/getUserProfit")
    public Tip getUserProfit(@RequestParam(value = "user_id") String user_id) {
        return atWillUpdataService.getUserProfit(user_id);
    }

    /**
     * 修改用户收益
     **/
    @RequestMapping(value = "/updataUserProfit")
    public Tip updataUserProfit(UserMoney userMoney) {
        Tip tip = atWillUpdataService.updataUserProfit(userMoney);
        return tip;
    }

    //获取粉丝信息
    @RequestMapping(value = "/getFSList")
    public Tip getFSList(@RequestParam(value = "user_id", required = false) String user_id,
                         @RequestParam(value = "fs_user_id", required = false) String fs_user_id,
                         @RequestParam(value = "directRelation", required = false) Integer directRelation,
                         @RequestParam(value = "offset") Integer offset,
                         @RequestParam(value = "limit") Integer limit,
                         @RequestParam(value = "is_vip", required = false) String is_vip) throws Exception {
        Tip tip = atWillUpdataService.getFSList(user_id, fs_user_id, directRelation, offset, limit, is_vip);
        return tip;
    }

    //获取某个粉丝信息
    @RequestMapping(value = "/getFsDetail")
    public Tip getFsDetail(@RequestParam(value = "user_id", required = false) String user_id) {
        Tip tip = atWillUpdataService.getFsDetail(user_id);
        return tip;
    }

    /**
     * 修改
     **/
    @RequestMapping(value = "/updateFsDetail")
    public Tip updateFsDetail(@RequestParam(value = "user_id", required = false) Integer user_id,
                              @RequestParam(value = "estimate_commission") Double estimate_commission,
                              @RequestParam(value = "estimate_bounty") Double estimate_bounty,
                              @RequestParam(value = "estimate_fans") Double estimate_fans,
                              @RequestParam(value = "owner_estimate") Double owner_estimate,
                              @RequestParam(value = "accumulated") Double accumulated,
                              @RequestParam(value = "activity_accumulated") Double activity_accumulated,
                              @RequestParam(value = "fans_estimate") Double fans_estimate,
                              @RequestParam(value = "bounty_accumulated") Double bounty_accumulated,
                              @RequestParam(value = "direct_fans") Integer direct_fans,
                              @RequestParam(value = "indirect_fans") Integer indirect_fans) {
        Tip tip = atWillUpdataService.updateFsDetail(user_id, estimate_commission, estimate_bounty, estimate_fans,
                owner_estimate, accumulated, activity_accumulated, fans_estimate, bounty_accumulated, direct_fans, indirect_fans);
        return tip;
    }

    /**
     * 新增 某一天 order 订单信息
     **/
    @RequestMapping(value = "/addOrder")
    public Tip addOrder(Order order) {
        Tip tip = atWillUpdataService.addOrder(order);
        return tip;
    }

    /**
     * 查询用户 某一天的  jz_day_fomr
     **/
    @RequestMapping(value = "/getUserFomr")
    public Tip getUserFomr(@RequestParam(value = "user_id") String user_id,
                           @RequestParam(value = "time") String time) {
        Tip tip = atWillUpdataService.getUserFomr(user_id, time);
        return tip;
    }

    /**
     * 修改用户 某一天的  jz_day_fomr
     **/
    @RequestMapping(value = "/updataFomr")
    public Tip updataFomr(@RequestParam(value = "id", required = false) String id,
                          @RequestParam(value = "user_id") String user_id,
                          @RequestParam(value = "time") String time,
                          @RequestParam(value = "estimate_commission") Double estimate_commission,
                          @RequestParam(value = "estimate_bounty") Double estimate_bounty,
                          @RequestParam(value = "estimate_fans") Double estimate_fans
    ) {
        Tip tip = atWillUpdataService.updataFomr(id, user_id, time, estimate_commission, estimate_bounty, estimate_fans);
        return tip;
    }


    /**
     * 修改 或 增加 粉丝礼包订单返佣
     **/
    @RequestMapping(value = "/addAndUpdataOrderGiftRake")
    public Tip addAndUpdataOrderGiftRake(@RequestParam(value = "id", required = false) String id,
                                         @RequestParam(value = "user_id") String user_id,
                                         @RequestParam(value = "user_rake_back") Double user_rake_back
    ) {
        Tip tip = atWillUpdataService.addAndUpdataOrderGiftRake(id, user_id, user_rake_back);
        return tip;
    }

    /**
     * 修改 或 增加 某用户 普通订单
     **/
    @RequestMapping(value = "/addAndUpdataOrder")
    public Tip addAndUpdataOrder(@RequestParam(value = "id", required = false) String id,
                                 @RequestParam(value = "user_id") String user_id,
                                 @RequestParam(value = "user_rake_back_yugu") Double user_rake_back_yugu
    ) {
        Tip tip = atWillUpdataService.addAndUpdataOrder(id, user_id, user_rake_back_yugu);
        return tip;
    }


    /**
     * 查询今日预估佣金 提示：获取今日最近的订单
     */
    @RequestMapping(value = "/getOrderMoney")
    public Tip getOrderMoney(@RequestParam(value = "user_id") String user_id) {
        Tip tip = atWillUpdataService.getOrderMoney(user_id);
        return tip;
    }

    /**
     * 查询今日预估奖励 提示：获取今日最近的订单 粉丝礼包订单返佣
     */
    @RequestMapping(value = "/getOrderGiftRakeMoney")
    public Tip getOrderGiftRakeMoney(@RequestParam(value = "user_id") String user_id) {
        Tip tip = atWillUpdataService.getOrderGiftRakeMoney(user_id);
        return tip;
    }

}
