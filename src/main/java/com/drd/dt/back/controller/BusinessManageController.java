package com.drd.dt.back.controller;

import com.drd.dt.back.dto.BusinessAcademyDTO;
import com.drd.dt.back.service.IBusinessManageService;
import com.drd.dt.common.tips.Tip;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

@Api(tags = "商学院后台", description = "商学院后台接口")
@RestController
@RequestMapping("businessManage")
public class BusinessManageController {

    @Autowired
    private IBusinessManageService businessManageService;

    /**
     * 列表
     */
    @ApiOperation(value = "列表", notes = "列表", response = HttpMessage.class)
    @GetMapping("/list")
    public Tip list(BusinessAcademyDTO businessAcademyDTO) throws Exception {
        return businessManageService.list(businessAcademyDTO);
    }

    /**
     * 添加数据
     */
    @ApiOperation(value = "后台-新增", notes = "新增数据", response = HttpMessage.class)
    @PostMapping("/insert")
    public Tip insert(BusinessAcademyDTO businessAcademyDTO) throws Exception {
        return businessManageService.insertBusiness(businessAcademyDTO);
    }

    /**
     * 删除数据
     */
    @ApiOperation(value = "后台-删除(批量)", notes = "批量删除", response = HttpMessage.class)
    @PostMapping("/delete")
    public Tip deleteAll(@RequestParam("id") Integer id) throws Exception {
        return businessManageService.delete(id);
    }

    /**
     * 修改数据
     */
    @ApiOperation(value = "后台-修改", notes = "单个数据修改", response = HttpMessage.class)
    @PostMapping("/update")
    public Tip update(BusinessAcademyDTO businessAcademyDTO) throws Exception {
        return businessManageService.update(businessAcademyDTO);
    }

    /**
     * 详情
     **/
    @ApiOperation(value = "后台-查询数据单条数据", notes = "查询数据单条数据", response = HttpMessage.class)
    @PostMapping("/detail/{id}")
    public Tip detail(@PathVariable Integer id) throws Exception{
        return businessManageService.detail(id);
    }
}
