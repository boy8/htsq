package com.drd.dt.back.controller;

import com.drd.dt.back.entity.BusinessDaily;
import com.drd.dt.back.service.IBusinessDailyService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 每日爆款
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/businessDaily")
@RestController
public class BusinessDailyController {

    @Autowired
    private IBusinessDailyService businessDailyService;

    /**
     * 获取每日爆款列表
     **/
    @RequestMapping(value = "/list")
    public Tip list(@RequestParam Map<String, String> param) throws Exception {
        Tip data = businessDailyService.getList(param);
        return data;
    }

    /**
     * 删除每日爆款
     **/
    @RequestMapping(value = "/delete")
    public Tip deleteOne(@RequestParam(value = "id") Integer id) throws Exception {
        Tip data = businessDailyService.delete(id);
        return data;
    }

    /**
     * 修改每日爆款
     **/
    @RequestMapping(value = "/update")
    public Tip update(BusinessDaily businessDaily) throws Exception {
        Tip data = businessDailyService.update(businessDaily);
        return data;
    }

    /**
     * 获取每日爆款详情
     **/
    @RequestMapping(value = "/detailInfo/{id}")
    public Tip advDetailInfo(@PathVariable Integer id) throws Exception {
        Tip data = businessDailyService.advDetailInfo(id);
        return data;
    }

}
