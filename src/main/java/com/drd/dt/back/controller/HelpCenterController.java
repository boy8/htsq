package com.drd.dt.back.controller;

import com.drd.dt.back.service.IHelpCenterService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/helpCenter")
@RestController
public class HelpCenterController {

    @Autowired
    private IHelpCenterService helpCenterService;

    /**
     * 问题分类管理列表
     **/
    @RequestMapping(value = "/problemTypeManageList")
    public Tip problemTypeManageList() throws Exception {
        Tip data = helpCenterService.problemTypeManageList();
        return data;
    }

    /**
     * 添加问题分类
     **/
    @RequestMapping(value = "/addProblemType")
    public Tip addProblemType(@RequestParam Map<String, String> map) throws Exception {
        Tip data = helpCenterService.addProblemType(map);
        return data;
    }

    /**
     * 删除问题分类
     **/
    @RequestMapping(value = "/deleteOneProblemType")
    public Tip deleteOneProblemType(@RequestParam(value = "id") String id) throws Exception {
        Tip data = helpCenterService.deleteOneProblemType(id);
        return data;
    }

    /**
     * 修改问题分类
     **/
    @RequestMapping(value = "/updateProblemType")
    public Tip updateProblemType(@RequestParam Map<String, String> map) throws Exception {
        Tip data = helpCenterService.updateProblemType(map);
        return data;
    }

    /**
     * 获取问题分类详情
     **/
    @RequestMapping(value = "/problemTypeDetail/{id}")
    public Tip problemTypeDetail(@PathVariable String id) throws Exception {
        Tip data = helpCenterService.problemTypeDetail(id);
        return data;
    }

    /**
     * 问题管理列表
     **/
    @RequestMapping(value = "/problemManageList")
    public Tip problemManageList() throws Exception {
        Tip data = helpCenterService.problemManageList();
        return data;
    }

    /**
     * 新增问题
     **/
    @RequestMapping(value = "/addProblemManage")
    public Tip addProblemManage(@RequestParam Map<String, String> map) throws Exception {
        Tip data = helpCenterService.addProblemManage(map);
        return data;
    }


    /**
     * 修改问题
     **/
    @RequestMapping(value = "/updateProblemManage")
    public Tip updateProblemManage(@RequestParam Map<String, String> map) throws Exception {
        Tip data = helpCenterService.updateProblemManage(map);
        return data;
    }

    /**
     * 获取问题详情
     **/
    @RequestMapping(value = "/problemManageDetail/{id}")
    public Tip problemManageDetail(@PathVariable String id) throws Exception {
        Tip data = helpCenterService.problemManageDetail(id);
        return data;
    }

    /**
     * 批量删除问题
     **/
    @RequestMapping(value = "/deleteAllProblem")
    public Tip deleteAllProblem(@RequestParam(value = "ids") String ids) throws Exception {
        Tip data = helpCenterService.deleteAllProblem(ids);
        return data;
    }

    /**
     * 删除问题
     **/
    @RequestMapping(value = "/deleteOneProblem")
    public Tip deleteOneProblem(@RequestParam(value = "id") String id) throws Exception {
        Tip data = helpCenterService.deleteOneProblem(id);
        return data;
    }

    /**
     * 获取问题分类
     **/
    @RequestMapping(value = "/problemTypeList")
    public Tip problemTypeList() throws Exception {
        Tip data = helpCenterService.problemTypeList();
        return data;
    }

}
