package com.drd.dt.back.controller;

import com.drd.dt.back.dto.UserManageDTO;
import com.drd.dt.back.service.IUserManageService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户管理
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/userManage")
@RestController
public class UserManageController {

    @Autowired
    private IUserManageService userManageService;

    /**
     * 获取用户列表
     **/
    @GetMapping(value = "/list")
    public Tip list(UserManageDTO userManageDTO) throws Exception {
        Tip data = userManageService.list(userManageDTO);
        return data;
    }


    /**
     * 修改用户等级
     **/
    @PostMapping(value = "/update")
    public Tip update(@RequestParam(value = "user_id") Integer user_id,
                      @RequestParam(value = "lv_expire_time") String lv_expire_time,
                      @RequestParam(value = "alipay_account") String alipay_account,
                      @RequestParam(value = "alipay_user_name") String alipay_user_name,
                      @RequestParam(value = "status") Integer status,
                      @RequestParam(value = "level") Integer level) throws Exception {
        Tip data = userManageService.update(user_id,level,lv_expire_time,status,alipay_user_name,alipay_account);
        return data;
    }

    /**
     * 用户详情
     **/
    @PostMapping(value = "/detail/{user_id}")
    public Tip update(@PathVariable Integer user_id) throws Exception {
        Tip data = userManageService.detail(user_id);
        return data;
    }

}
