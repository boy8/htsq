package com.drd.dt.back.controller;

import com.drd.dt.back.service.IOrderManageService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/orderManager")
@Controller
public class OrderManagerController {

    @Autowired
    private IOrderManageService orderManageService;

    /**
     * 订单列表
     **/
    @RequestMapping(value = "/list")
    @ResponseBody
    public Tip list(@RequestParam Map<Object, Object> map) throws Exception{
        Tip data = orderManageService.list(map);
        return data;
    }

    /**
     *  上传淘宝excel订单更新
     */
    @ResponseBody
    @RequestMapping("/taobaoImport")
    public Tip taobaoImport(@RequestParam("file") MultipartFile file) throws Exception{
        Tip data = orderManageService.taobaoImport(file);
        return data;
    }
    /**
     *  上传京东excel订单更新
     */
    @ResponseBody
    @RequestMapping("/jdImport")
    public Tip jdImport(@RequestParam("file") MultipartFile file) throws Exception{
        Tip data = orderManageService.jdImport(file);
        return data;
    }
    /**
     *  上传拼多多xcel订单更新
     */
    @ResponseBody
    @RequestMapping("/pddImport")
    public Tip pddImport(@RequestParam("file") MultipartFile file) throws Exception{
        Tip data = orderManageService.pddImport(file);
        return data;
    }
    /**
     *  上传唯品会excel订单更新
     */
    @ResponseBody
    @RequestMapping("/wphImport")
    public Tip wphImport(@RequestParam("file") MultipartFile file) throws Exception{
        Tip data = orderManageService.wphImport(file);
        return data;
    }
}
