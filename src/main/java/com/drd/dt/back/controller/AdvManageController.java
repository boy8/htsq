package com.drd.dt.back.controller;

import com.drd.dt.back.entity.AdvBack;
import com.drd.dt.back.service.IAdvManageService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 广告管理
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/advManage")
@RestController
public class AdvManageController {

    @Autowired
    private IAdvManageService advManageService;

    /**
     * 获取广告列表
     **/
    @RequestMapping(value = "/list")
    public Tip list() throws Exception {
        Tip data = advManageService.getAdvList();
        return data;
    }

    /**
     * 新增广告
     **/
    @RequestMapping(value = "/insert")
    public Tip insert(AdvBack advBack) throws Exception {
        Tip data = advManageService.insertAdv(advBack);
        return data;
    }

    /**
     * 删除广告
     **/
    @RequestMapping(value = "/delete")
    public Tip deleteOne(@RequestParam(value = "id") Integer id) throws Exception {
        Tip data = advManageService.delete(id);
        return data;
    }

    /**
     * 修改广告
     **/
    @RequestMapping(value = "/update")
    public Tip update(AdvBack advBack) throws Exception {
        Tip data = advManageService.update(advBack);
        return data;
    }

    /**
     * 获取广告详情
     **/
    @RequestMapping(value = "/advDetailInfo/{id}")
    public Tip advDetailInfo(@PathVariable Integer id) throws Exception {
        Tip data = advManageService.advDetailInfo(id);
        return data;
    }

}
