package com.drd.dt.back.controller;

import com.drd.dt.back.service.IZeroPurchaseService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 零元购
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/zeroPurchase")
@RestController
public class ZeroPurchaseController {

    @Autowired
    private IZeroPurchaseService zeroPurchaseService;

    /**
     * 零元购列表
     **/
    @RequestMapping(value = "/list")
    public Tip list(@RequestParam Map<Object, Object> map) throws Exception {
        Tip data = zeroPurchaseService.list(map);
        return data;
    }

    /**
     * 新增
     **/
    @RequestMapping(value = "/insert")
    public Tip insert(@RequestParam Map<String, String> map) throws Exception {
        Tip data = zeroPurchaseService.insert(map);
        return data;
    }

    /**
     * 修改
     **/
    @RequestMapping(value = "/update")
    public Tip update(@RequestParam Map<String, String> map) throws Exception {
        Tip data = zeroPurchaseService.update(map);
        return data;
    }

    /**
     * 删除
     **/
    @RequestMapping(value = "/delete/{id}")
    public Tip delete(@PathVariable Integer id) throws Exception {
        Tip data = zeroPurchaseService.delete(id);
        return data;
    }

    /**
     * 详情
     **/
    @GetMapping(value = "/detail")
    public Tip detail(@RequestParam("id") Integer id) throws Exception {
        Tip data = zeroPurchaseService.detail(id);
        return data;
    }
}
