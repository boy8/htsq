package com.drd.dt.back.controller;

import com.drd.dt.back.service.IShareService;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 分享素材
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/share")
@Controller
public class ShareController {

    @Autowired
    private IShareService shareService;

    /**
     * 分享素材列表
     **/
    @RequestMapping(value = "/list")
    @ResponseBody
    public Tip list(@RequestParam Map<Object, Object> map) throws Exception {
        Tip data = shareService.list(map);
        return data;
    }

    /**
     * 分享素材列表(前端展示列表)
     **/
    @RequestMapping(value = "/shareList")
    @ResponseBody
    public Tip shareList(@RequestParam Map<String, String> map) throws Exception {
        Tip data = shareService.shareList(map);
        return data;
    }

    /**
     * 新增
     **/
    @RequestMapping(value = "/insert")
    @ResponseBody
    public Tip insert(@RequestParam Map<String, String> map) throws Exception {
        Tip data = shareService.insert(map);
        return data;
    }

    /**
     * 修改
     **/
    @RequestMapping(value = "/update")
    @ResponseBody
    public Tip update(@RequestParam Map<String, String> map) throws Exception {
        Tip data = shareService.update(map);
        return data;
    }

    /**
     * 删除
     **/
    @RequestMapping(value = "/delete/{id}")
    @ResponseBody
    public Tip delete(@PathVariable Integer id) throws Exception {
        Tip data = shareService.delete(id);
        return data;
    }

    /**
     * 详情
     **/
    @RequestMapping(value = "/detail/{id}")
    @ResponseBody
    public Tip detail(@PathVariable Integer id) throws Exception {
        Tip data = shareService.detail(id);
        return data;
    }
}
