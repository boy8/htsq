package com.drd.dt.back.controller;

import com.drd.dt.back.service.INormalManageService;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.entity.InitConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@RequestMapping("/normalManage")
@RestController
public class NormalManageController {

    @Autowired
    private INormalManageService normalManageService;

    /**
     * 获取基础设置信息
     **/
    @PostMapping(value = "/baseInfo")
    public Tip baseInfo() throws Exception {
        Tip data = normalManageService.baseInfo();
        return data;
    }

    /**
     * 修改基础设置信息
     **/
    @PostMapping(value = "/updateBaseInfo")
    public Tip updateBaseInfo(@RequestParam Map<Object, Object> map) throws Exception {
        Tip data = normalManageService.updateBaseInfo(map);
        return data;
    }

    /**
     * 获取参数设置信息
     **/
    @RequestMapping(value = "/parameterInfo")
    public Tip parameterInfo() throws Exception {
        Tip data = normalManageService.parameterInfo();
        return data;
    }

    /**
     * 修改参数设置信息
     **/
    @RequestMapping(value = "/updateParameterInfo")
    public Tip updateParameterInfo(@RequestParam Map<Object, Object> map) throws Exception {
        Tip data = normalManageService.updateParameterInfo(map);
        return data;
    }

    /**
     * 获取初始化设置信息
     **/
    @RequestMapping(value = "/initInfo")
    public Tip initrInfo() throws Exception {
        Tip data = normalManageService.initrInfo();
        return data;
    }

    /**
     * 更新初始化设置信息
     **/
    @RequestMapping(value = "/updateInit")
    public Tip updateInit(InitConfig config) throws Exception {
        Tip data = normalManageService.updateInit(config);
        return data;
    }

}
