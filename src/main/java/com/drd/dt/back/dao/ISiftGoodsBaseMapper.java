package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dto.SiftGoodsDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * SiftGoodsDTO 精选核桃 商品 DAO
 */
public interface ISiftGoodsBaseMapper {

    int insert(SiftGoodsDTO siftGoodsDTO);

    int updata(SiftGoodsDTO siftGoodsDTO);

    void delete(@Param("id") String id);

    SiftGoodsDTO queryAll(@Param("id") String id);

    List<SiftGoodsDTO> pageSelect(SiftGoodsDTO siftGoodsDTO);

    /**
     * 库存 减少 销量增加
     * **/
    int updataGoodsSell(SiftGoodsDTO siftGoodsDTO);

}
