package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.entity.BusinessDaily;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/11.
 */
public interface BusinessDailyMapper extends BaseMapper<BusinessDaily> {

    /**
     * 获取每日爆款列表
     **/
    List<BusinessDaily> getList(Page page, Map<String, String> param);
}
