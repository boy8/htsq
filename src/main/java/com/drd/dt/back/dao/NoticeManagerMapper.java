package com.drd.dt.back.dao;


import com.drd.dt.back.entity.MessageActivity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/11.
 */
public interface NoticeManagerMapper {

    /**
     * 获取公告列表
     * @return
     */
    List<MessageActivity> getNoticeList();

}
