package com.drd.dt.back.dao;


import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/11.
 */
public interface HelpCenterMapper {

    /**
     * 添加问题分类
     **/
    void addProblemType(Map<String, String> map);

    /**
     * 问题管理列表
     * @return
     */
    List<Map> getProblemManageList();

    /**
     * 新增问题
     **/
    void addProblemManage(Map<String, String> map);

    /**
     * 修改问题
     **/
    void updateProblemManage(Map<String, String> map);
    /**
     * 问题详情
     * @param id
     * @return
     */
    Map problemManageDetail(@Param("id") Integer id);

    /**
     * 删除问题
     * @param id
     */
    void deleteProblem(@Param("id") String id);

    /**
     * 获取问题分类
     **/
    List<Map> problemTypeList();

    /**
     * 问题分类管理列表
     * @return
     */
    List<Map> getProblemTypeList();

    /**
     * 删除问题分类
     **/
    void deleteProblemType(String id);

    /**
     * 获取问题详情
     **/
    Map getProblemTypeDetail(String id);

    /**
     * 修改问题分类
     **/
    void updateProblemType(Map<String, String> map);
}
