package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dto.UserManageDTO;
import com.drd.dt.modular.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 86514 on 2019/4/11.
 */
public interface UserManageMapper extends BaseMapper<User> {


    List<UserManageDTO> selectUserList(Page page,UserManageDTO userManageDTO);
}
