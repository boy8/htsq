package com.drd.dt.back.dao;

import com.drd.dt.modular.entity.InitConfig;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/11.
 */
public interface NormalManageMapper {

    /**
     * 获取基础设置信息
     **/
    Map getBaseInfo();

    /**
     * 修改基础设置信息
     **/
    void updateBaseInfo(Map<Object, Object> map);

    /**
     * 获取参数设置信息
     **/
    Map getParameterInfo();

    /**
     * 修改参数设置信息
     **/
    void updateParameterInfo(Map<Object, Object> map);

    /**
     * 获取初始化配置
     * @return
     */
    InitConfig selectInitConfigInfo();

    /**
     * 更新初始化设置信息
     **/
    void updateInit(InitConfig config);
}
