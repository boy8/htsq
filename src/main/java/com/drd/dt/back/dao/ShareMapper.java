package com.drd.dt.back.dao;

import com.drd.dt.back.dto.ShareDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/12.
 */
public interface ShareMapper {


    List<ShareDTO> list(Map<Object, Object> map);

    List<ShareDTO> shareList(Map param);

    void insert(Map<String, String> map);

    void update(Map<String, String> map);

    void delete(@Param("id") Integer id);

    ShareDTO detail(@Param("id") Integer id);

    /**
     * 获取极光的appkey和secret
     * @return
     */
    List<Map> getJgAppKey(Map map);
}
