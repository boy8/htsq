package com.drd.dt.back.dao;


import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/11.
 */
public interface LoginMapper  {

    /**
     *获取admin用户
     */
    Map getAdminByUserName(@Param("userName") String userName);
}
