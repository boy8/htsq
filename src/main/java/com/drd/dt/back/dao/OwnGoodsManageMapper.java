package com.drd.dt.back.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.drd.dt.back.dto.OwnGoodsDTO;
import com.drd.dt.back.dto.OwnGoodsOrderDTO;
import com.drd.dt.back.dto.OwnGoodsSizeDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OwnGoodsManageMapper extends BaseMapper<OwnGoodsDTO> {

    /**
     * 查询商品
     * */
    List<OwnGoodsDTO> selectAll(@Param("title") String title,@Param("state") String state);

    /**
     * 自营订单列表
     */
    List<OwnGoodsOrderDTO> selectOrderList(OwnGoodsOrderDTO orderDTO);

    /**
     * 自营订单详情
     */
    OwnGoodsOrderDTO orderDetail(@Param("id") Integer id);

    /**
     * 修改自营订单
     */
    void orderUpdate(OwnGoodsOrderDTO orderDTO);

    /**
     * 新增商品规格
     */
    void insertGoodsSpecification(List<Map<String,Object>> list);

    /**
     * 删除商品规格
     */
    void deleteGoodsSpecification(@Param("id") Integer id);

    /**
     * 查询规格
     * */
    List<Map<String,String>> getGoodsSpecificationDetail(@Param("id") Integer id);

    /**
     * 查询单个商品信息
     * */
    OwnGoodsSizeDTO getDetailId(@Param("id") Integer id);

    //批量修改 规格信息
    void updateGoodsSpecification(List<Map<String,Object>> list);

    /**
     * 导出自营订单数据
     **/
    List<OwnGoodsOrderDTO> selectExportOrderList(OwnGoodsOrderDTO orderDTO);

    /**
     * 查询自营订单 待收货商品 列表
     * */
    List<OwnGoodsOrderDTO> selectStatusList(@Param("status") int status);
}
