package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.drd.dt.back.entity.AdvBack;

import java.util.List;

/**
 * Created by 86514 on 2019/4/11.
 */
public interface AdvManageMapper extends BaseMapper<AdvBack> {

    /**
     * 广告列表
     * @return
     */
    List<AdvBack> getAdvList();
}
