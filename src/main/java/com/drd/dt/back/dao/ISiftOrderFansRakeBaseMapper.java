package com.drd.dt.back.dao;

import com.drd.dt.back.dto.SiftOrderFansRakeDTO;

import java.util.List;

/**
 * SiftOrderFansRake 核桃精选粉丝订单表 DAO
 */
public interface ISiftOrderFansRakeBaseMapper {

    int insert(SiftOrderFansRakeDTO siftOrderFansRakeDTO);

    int update(SiftOrderFansRakeDTO siftOrderFansRakeDTO);

    void delete(Integer id);

    SiftOrderFansRakeDTO queryAll(Integer id);

    List<SiftOrderFansRakeDTO> pageSelect(SiftOrderFansRakeDTO siftOrderFansRakeDTO);
}
