package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.entity.AdvBack;
import com.drd.dt.modular.dto.UserMoneyDTO;
import com.drd.dt.modular.entity.UserMoney;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public interface AtWillUpdataMapper   extends BaseMapper<UserMoney> {

    /**
     * 获取 用户收益信息
     * **/
    UserMoney getUserProfile(@Param("user_id")String user_id);

    /**
     * 获取 用户某个直接粉丝的收益信息
     * **/
    UserMoneyDTO getUserZJFSProfile(Map map);

    /**
     * 修改用户收益
     * **/
    int updataUserProfile(UserMoney userProfile);


    /**
     * 批量查询
     * **/
    List<UserMoneyDTO> selectUserZJFSProfile(@Param("Page") Page page, @Param("list") List<Integer> list, @Param("is_vip")Integer is_vip);


    /**
     * 获取所有直属粉丝ID
     * **/
    List<Integer> getDirectFS(@Param("user_id")Integer user_id);

    /***
     * 获取所有非直属 （间接）粉丝
     * */
    List<Integer> getIndirectFS(@Param("list")List<Integer> list,@Param("user_id")Integer user_id);

    /***
     * 获取所有粉丝
     * */
    List<Integer> getAllFS(@Param("user_id")Integer user_id);

    /**
     * 查询指定用户 上月预估收益以及累计预估收益
     * **/
    List<UserMoneyDTO> getFSEarningsDetail(@Param("Page") Page page,@Param("list")List<Integer> list);

    /**
     *
     * */
    Map getFsDetail(@Param("user_id")Integer user_id);

    /**
     * 查询某用户上月收益
     * **/
    List<Map> selectForm(Map map);

    /**
     * 查询某用户上月收益
     * **/
    Map getForm(Map map);

    /**
     * 新增 某 用户上月收益
     * **/
    int addForm(Map map);

    /**
     * 修改 某 用户上月收益
     * */
    int updateForm(Map map);

    /**
     * 修改 某 用户某一天收益报表
     * */
    int updateFormTime(Map map);

    /**
     * 新增某用户 粉丝礼包订单发返佣
     * **/
    int addOrderGiftRake(Map map);

    /**
     * 修改某用户 粉丝礼包订单发返佣
     * **/
    int updataOrderGiftRake(Map map);

    /**
     * 新增某用户 普通订单
     * */
    int addOrder(Map map);

    /**
     * 修改某用户 普通订单
     * */
    int updataOrder(Map map);


    /**
     * 查询今日预估佣金 提示：获取今日最近的订单
     * */
    Map getOrderMoney(@Param("user_id") Integer user_id);

    /**
     * 查询今日预估奖励 提示：获取今日最近的订单 粉丝礼包订单返佣
     * */
    Map  getOrderGiftRakeMoney(@Param("user_id") Integer user_id);
}
