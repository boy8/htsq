package com.drd.dt.back.dao;

import com.drd.dt.back.dto.SiftOrderLogDTO;

import java.util.List;

/**
 * SiftOrderLog 精选核桃 商品 订单 售后记录表表 DAO
 */
public interface ISiftOrderLogBaseMapper {
    int insert(SiftOrderLogDTO siftOrderLog);

    int update(SiftOrderLogDTO siftOrderLog);

    void delete(Integer id);

    SiftOrderLogDTO queryAll(Integer id);

    List<SiftOrderLogDTO> pageSelect(SiftOrderLogDTO siftOrderLog);
}