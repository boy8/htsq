package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.plugins.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/11.
 */
public interface OrderManagerMapper {

    /**
     * 订单列表
     * @param map
     * @return
     */
    List<Map> getOrderList(Page page,Map map);

    List<Map> getXMBOrderList(Page page);
}
