package com.drd.dt.back.dao;

import com.drd.dt.back.dto.ZeroPurchaseDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/12.
 */
public interface ZeroPurchaseMapper {


    List<ZeroPurchaseDTO> selectList();

    void insert(Map<String, String> map);

    void update(Map<String, String> map);

    void delete(@Param("id") Integer id);

    ZeroPurchaseDTO detail(@Param("id") Integer id);

}
