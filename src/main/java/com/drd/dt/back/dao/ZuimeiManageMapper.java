package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.drd.dt.back.dto.ZeroPurchaseDTO;
import com.drd.dt.back.entity.ZuimeiGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/12.
 */
public interface ZuimeiManageMapper extends BaseMapper<ZuimeiGoods> {


    List<ZuimeiGoods> selectList();

    void insert(Map<String, String> map);

    void update(Map<String, String> map);

    void delete(@Param("id") Integer id);

    ZuimeiGoods detail(@Param("id") Integer id);

}
