package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dto.SiftGoodsSpecificationDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ISiftGoodsSpecificationBaseMapper {

    int insert(SiftGoodsSpecificationDTO siftGoodsSpecification);

    int updata(SiftGoodsSpecificationDTO siftGoodsSpecification);

    void delete(@Param("id") String id);

    SiftGoodsSpecificationDTO queryAll(@Param("id") String id);

    List<SiftGoodsSpecificationDTO> pageSelect(@Param("goods_id") String goods_id);
}
