package com.drd.dt.back.dao;

import com.drd.dt.back.dto.ChannelDataInfoDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/12.
 */
public interface ChannelDataManagerMapper {
    /**
     * 渠道数据信息统计（应用宝）
     **/
    List<ChannelDataInfoDTO> channelDataInfo(Map<String, String> map);

    /**
     * 新增渠道数据信息（应用宝）
     **/
    void insert(ChannelDataInfoDTO channelData);

    /**
     * 删除渠道数据信息（应用宝）
     **/
    void deleteById(@Param("id") Integer id);

    /**
     * 修改渠道数据信息（应用宝）
     **/
    void updateById(ChannelDataInfoDTO channelData);

    /**
     * 获取渠道数据信息详情（应用宝）
     **/
    ChannelDataInfoDTO getDetailById(@Param("id") Integer id);
}
