package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dto.GoodsSpecificationDTO;
import com.drd.dt.back.entity.GoodsSpecification;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GoodsSpecificationBaseMapper {

    //查询商品规格信息
    List<GoodsSpecification>  getGoodsSpecification(@Param("ids")List<Long> ids);


    int addGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification);

    int updateGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification);

    GoodsSpecificationDTO finaAll(@Param("id")Integer id);

    void delete(@Param("id")Integer id);

    List<GoodsSpecificationDTO>  selectGoodsSpecificationDetail(@Param("id")Integer id);

}
