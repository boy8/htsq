package com.drd.dt.back.dao;

import com.drd.dt.modular.entity.TaskCoin;
import com.drd.dt.modular.entity.TaskInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/***
 * 提示：直接修改测试服务器数据 所使用的功能如下
 * */
public interface TaskBaseMapper {

    //查询用户 今日最近一条粉丝金币
    TaskCoin userFans(@Param("user_id")int user_id,@Param("action_type")int action_type);
    //修改用户 今日最近一条粉丝金币
    int updateFansCoin(@Param("id")int id,@Param("coin_num")int coin_num);
    //新增用户 今日一条粉丝金币
    int addFansCoin(TaskCoin taskCoin);
    //修改用户 金币
    int updateUserCoin(@Param("id")int id,@Param("coin")int coin);
    //查询用户 金币信息
    int selectCoin(@Param("id")int id);
    //查询任务
    List<Map> selectTaskList(@Param("user_id")int user_id);



    //查询基础任务
    int  allUserTaskBase(@Param("user_id")int user_id,@Param("task_id")int task_id);
    //基础任务新增
    void addTaskBase(@Param("user_id")int user_id,@Param("task_id")int task_id,@Param("times")int times,@Param("status")int status);
    //基础任务修改
    void updateTaskBase(@Param("user_id")int user_id,@Param("task_id")int task_id,@Param("times")int times,@Param("status")int status);

    //查询日常任务
    int allUserTaskDaily(@Param("user_id")int user_id,@Param("task_id")int task_id);
    //日常任务新增
    void addTaskDaily(@Param("user_id")int user_id,@Param("task_id")int task_id,@Param("times")int times,@Param("status")int status);
    //日常任务修改
    void updateTaskDaily(@Param("user_id")int user_id,@Param("task_id")int task_id,@Param("times")int times,@Param("status")int status);

}
