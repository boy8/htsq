package com.drd.dt.back.dao;

import com.drd.dt.back.dto.OwnGoodsOrderDTO;
import com.drd.dt.back.dto.SiftOrderDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ISiftOrderBaseMapper {

    //添加订单信息
    int addOrderGift(SiftOrderDTO siftOrderDTO);

    int insert(SiftOrderDTO siftOrderDTO);

    int updata(SiftOrderDTO siftOrderDTO);

    /**
     *
     * 订单修改为失效订单
     * **/
    int updataNoUse(@Param("order_id") String order_id);
    /**
     *
     * 粉丝订单修改为失效订单jz_sift_order_fans_rake
     * **/
    int updataFansRakeNoUse(@Param("order_id") String order_id);

    /**
     * 根据订单号修改状态
     * */
    int updataStatus(SiftOrderDTO siftOrderDTO);

    void delete(@Param("id") Integer id);

    //删除粉丝订单
    void deleteOrderId(@Param("order_id") String order_id);

    SiftOrderDTO queryAll(@Param("id") Integer id);

    List<OwnGoodsOrderDTO> pageSelect(OwnGoodsOrderDTO ownGoodsOrderDTO);

    /**
     * 根据订单 号查询
     * */
    SiftOrderDTO queryAllOrderId(@Param("order_id") String order_id);

    /**
     * 导出自营订单数据
     **/
    List<OwnGoodsOrderDTO> selectExportOrderList(OwnGoodsOrderDTO orderDTO);


    /**
     *  后台人工审核修改 退款状态
     * */
    int refundStatusUpdate(@Param("id") Integer id,@Param("refund_status") Integer refund_status);


    /**
     * 根据订单号 查询 是否存在余额明细 变动
     * **/
    int orderPureInBalanceDetail(@Param("order_id")String order_id);

    /**
     * 查询菜单栏 活动创建时间作为 新用户起算时间
     * */
    String discountClassifyCreateTime();
}
