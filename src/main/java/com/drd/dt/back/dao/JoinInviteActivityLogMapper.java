package com.drd.dt.back.dao;


import org.apache.ibatis.annotations.Param;

/**
 * 邀新活动
 * */
public interface JoinInviteActivityLogMapper {
    //检查 是否已经参加活动
    int selectJoin(@Param("user_id")Integer user_id);

    //添加 参加邀新活动人员
    void addJoin(@Param("user_id")Integer user_id);

    //查询指定用户参加时间
    String getJoinInviteActivityTime(@Param("user_id")Integer user_id);
}
