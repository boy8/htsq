package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.drd.dt.back.entity.SpecialCode;

/**
 * Created by 86514 on 2019/4/11.
 */
public interface SpecialCodeManageMapper extends BaseMapper<SpecialCode> {


}
