package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.drd.dt.back.entity.BrandNavigation;

public interface BrandNavigationBackMapper extends BaseMapper<BrandNavigation> {

}
