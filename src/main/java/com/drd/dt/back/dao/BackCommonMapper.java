package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dto.ChannelDataInfoDTO;
import com.drd.dt.back.dto.ChannelInfoDTO;
import com.drd.dt.back.dto.MessageCenterDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/12.
 */
public interface BackCommonMapper {
    /**
     *渠道信息统计
     * @param map
     * @return
     */
    List<ChannelInfoDTO> userNumList(Map<Object, Object> map);
    List<ChannelInfoDTO> orderNumList(Map<Object, Object> map);
    List<ChannelInfoDTO> userActiveNumList(Map<Object, Object> map);
    List<ChannelInfoDTO> newlyUserNum(Map<Object, Object> map);
    List<ChannelInfoDTO> orderPeopleNum(Map<Object, Object> map);
    /**
     *渠道信息统计分天
     * @param map
     * @return
     */
    List<ChannelInfoDTO> userNumListByTime(Map<Object, Object> map);
    List<ChannelInfoDTO> orderNumListByTime(Map<Object, Object> map);
    List<ChannelInfoDTO> userActiveNumListByTime(Map<Object, Object> map);
    List<ChannelInfoDTO> newlyUserNumListByTime(Map<Object, Object> map);
    List<ChannelInfoDTO> orderPeopleNumListByTime(Map<Object, Object> map);

    /**
     * 获取财务管理列表
     * @param map
     * @return
     */
    List<Map> getFinanceList(Page page,Map<Object, Object> map);

    /**
     * 获取提现状态
     * @param id
     */
    Map getTxStatus(@Param("id") String id);

    /**
     * 审核提现申请
     * @param map
     */
    void auditing(Map<String, String> map);

    /**
     * 渠道数据信息统计（应用宝）
     **/
    List<ChannelDataInfoDTO> channelDataInfo(Map<String, String> map);

    /**
     * 新增提现消息
     */
    void insertTxMessage(MessageCenterDTO messageDTO);

    /**
     * 获取极光的appkey，secret和用户极光id
     * @return
     */
    Map getJgInfoAndUser(@Param("user_id") Integer user_id);

    /**
     * 获取标记用户
     */
    Integer getFlagByuserId(@Param("user_id") String user_id);

    void updateUserFlag(@Param("id") Integer id,@Param("flag") String flag);

    void insertUserFlag(@Param("user_id") String user_id,@Param("flag") String flag);
}
