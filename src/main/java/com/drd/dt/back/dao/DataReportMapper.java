package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.plugins.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/12.
 */
public interface DataReportMapper {

    /**
     * 渠道汇总
     * @param map
     * @return
     */
    List<Map> selectChannelDataList(Map<String, String> map);

    /**
     * 平台汇总
     * @param map
     * @return
     */
    List<Map> selectTerraceDataList(Map<String, String> map);

    /**
     * 数据报表信息统计-总注册人数
     **/
    List<Map> peopleInfoList(Map<String, String> map);

    /**
     * 数据报表信息统计-订单收益汇总
     **/
    List<Map> incomeList(Map<String, String> map);

    /**
     * 订单购买人数
     * @param map
     * @return
     */
    List<Map> buyInfoList(Map<String, String> map);

    /**
     * 数据报表信息统计-自营订单收益汇总
     **/
    List<Map> selectOwnOrderList(Map<String, String> map);

    /**
     * 数据报表信息统计-活跃统计
     **/
    List<Map> activityList(Map<String, String> map);

    List<Map> orderRate(Map<String, String> map);

    List<Map> selectByBuyType(Map<String, String> map);
}
