package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.drd.dt.back.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HtmlOrderBaseMapper extends BaseMapper<HtmlOwnGoodsDTO> {


    /**
     * 订单列表
     */
    List<OwnGoodsOrderDTO> selectOrderList(OwnGoodsOrderDTO orderDTO);

    /**
     * 订单详情
     */
    OwnGoodsOrderDTO orderDetail(@Param("id") Integer id);

    /**
     * 修改订单
     */
    int orderUpdate(OwnGoodsOrderDTO orderDTO);


    /**
     * 导出自营订单数据
     **/
    List<OwnGoodsOrderDTO> selectExportOrderList(OwnGoodsOrderDTO orderDTO);

    /********************************商品方法***********************************************/

    /**
     * 查询商品
     */
    List<OwnGoodsDTO> selectAll(@Param("title") String title, @Param("state") String state);

    /**
     * 删除商品
     */
    void deleteGoodsDateil(@Param("id") Integer id);

    /**
     * 删除商品规格
     */
    void deleteGoodsSpecification(@Param("id") Integer id);

    /**
     * 查询单个商品信息
     * */
    HtmlOwnGoodsSizeDTO getDetailId(@Param("id") Integer id);
    /**
     * 查询规格
     * */
    List<Map<String,String>> getGoodsSpecificationDetail(@Param("id") Integer id);

    /**
     * 规格列表
     * */
    List<GoodsSpecificationDTO>  selectGoodsSpecificationDetail(@Param("id")Integer id);

    int addGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification);

    GoodsSpecificationDTO finaAll(@Param("id")Integer id);

    int updateGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification);

    int deleteSpecification(@Param("id")Integer id);
}
