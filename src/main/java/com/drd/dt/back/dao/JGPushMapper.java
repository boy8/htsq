package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.entity.MessageActivity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface JGPushMapper extends BaseMapper<MessageActivity> {

    /**
     * 获取极光的appkey和secret
     *
     * @return
     */
    Map getJgInfo();


    /***
     * 查询 列表
     * */
    List<MessageActivity> selectMessageActivity(@Param("page") Page page,Map<String,Object> map);
}
