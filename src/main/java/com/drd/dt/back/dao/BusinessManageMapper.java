package com.drd.dt.back.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.drd.dt.back.dto.BusinessAcademyDTO;

import java.util.List;

/**
 * Created by 86514 on 2019/4/11.
 */
public interface BusinessManageMapper extends BaseMapper<BusinessAcademyDTO> {

    /**
     * 列表
     */
    List<BusinessAcademyDTO> list(BusinessAcademyDTO businessAcademyDTO);
}
