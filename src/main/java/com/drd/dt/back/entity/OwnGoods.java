package com.drd.dt.back.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 自营商品
 **/


@ApiModel(value = "自营商品")
@TableName("jz_own_goods")
public class OwnGoods {
    @TableId(type = IdType.AUTO)
    private Integer dealer_id;
    private String title;
    private String describe;
    private double former_price;
    private double now_price;
    private double coupons_price;
    private double rebate_ratio;
    private Integer inventory;
    private Integer sell;
    private String carousel_imags;
    private String video;
    private Integer state;
    private String show_images;
    private String goods_units;
    private Integer type;//类型(1正在热卖、2热销爆款、3即将上市)
    private Integer sequence;

    /**
     * 扩展 平台类型（0自营、1京东、2淘宝、3天猫、4拼多躲）
     */
    private String platform_typeText;
    /**
     * 扩展 轮播图 list
     */
    private List<String> carousel_imagsList;
    /**
     * 扩展 详解图集合 list
     */
    private List<String> show_imagesList;
    /**
     * 扩展  默认第一张
     */
    private String firstImage;
    /**
     * 扩展  type 类型//(1正在热卖、2热销爆款、3即将上市)
     */
    private String typeText;
    /***
     * 扩展 独立h5 商品邮费
     * */
    private double postage;
    /**
     * 扩展 独立h5 限购
     * **/
    private Integer restrict;

    public Integer getRestrict() {
        return restrict;
    }

    public void setRestrict(Integer restrict) {
        this.restrict = restrict;
    }

    public double getPostage() {
        return postage;
    }

    public void setPostage(double postage) {
        this.postage = postage;
    }

    public String getTypeText() {
        return typeText;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
    }

    public String getFirstImage() {

        if (null != carousel_imags) {
            String[] images = this.carousel_imags.split(",");
            if (images.length > 0) {
                return images[0];
            }
        }
        return "";
    }

    public String getGoods_units() {
        return goods_units;
    }

    public void setGoods_units(String goods_units) {
        this.goods_units = goods_units;
    }

    public void setFirstImage(String firstImage) {
        this.firstImage = firstImage;
    }

    public List<String> getShow_imagesList() {
        if (null == show_imagesList) {
            show_imagesList = new ArrayList<String>();
        }
        if (null != show_images) {
            String[] images = this.show_images.split(",");
            for (String str : images) {
                show_imagesList.add(str);
            }
        }
        return show_imagesList;
    }

    public void setShow_imagesList(List<String> show_imagesList) {
        this.show_imagesList = show_imagesList;
    }

    public List<String> getCarousel_imagsList() {
        if (null == carousel_imagsList) {
            carousel_imagsList = new ArrayList<String>();
        }
        if (!this.carousel_imags.isEmpty()) {
            String[] images = this.carousel_imags.split(",");
            for (int i = 1; i < images.length; i++) {
                carousel_imagsList.add(images[i]);
            }
        }
        return carousel_imagsList;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
        switch (type) {
            case 1:
                setTypeText("正在热卖");
                return;
            case 2:
                setTypeText("热销爆款");
                return;
            case 3:
                setTypeText("即将上市");
                return;
            default:
                setTypeText("未知类型");
                return;
        }
    }

    public void setCarousel_imagsList(List<String> carousel_imagsList) {
        this.carousel_imagsList = carousel_imagsList;
    }

    public String getPlatform_typeText() {
        return platform_typeText;
    }

    public void setPlatform_typeText(String platform_typeText) {
        this.platform_typeText = platform_typeText;
    }

    public Integer getDealer_id() {
        return dealer_id;
    }

    public void setDealer_id(Integer dealer_id) {
        this.dealer_id = dealer_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public double getFormer_price() {
        return former_price;
    }

    public void setFormer_price(double former_price) {
        this.former_price = former_price;
    }

    public double getNow_price() {
        return now_price;
    }

    public void setNow_price(double now_price) {
        this.now_price = now_price;
    }

    public double getCoupons_price() {
        return coupons_price;
    }

    public void setCoupons_price(double coupons_price) {
        this.coupons_price = coupons_price;
    }

    public double getRebate_ratio() {
        return rebate_ratio;
    }

    public void setRebate_ratio(double rebate_ratio) {
        this.rebate_ratio = rebate_ratio;
    }

    public Integer getInventory() {
        return inventory;
    }

    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    public Integer getSell() {
        return sell;
    }

    public void setSell(Integer sell) {
        this.sell = sell;
    }

    public String getCarousel_imags() {
        return carousel_imags;
    }

    public void setCarousel_imags(String carousel_imags) {
        this.carousel_imags = carousel_imags;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getShow_images() {
        return show_images;
    }

    public void setShow_images(String show_images) {
        this.show_images = show_images;
    }
}
