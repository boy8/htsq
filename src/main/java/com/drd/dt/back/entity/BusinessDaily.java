package com.drd.dt.back.entity;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * Created by tanhao on 2020/2/18.
 */
@TableName("jz_business_daily")
public class BusinessDaily {
    private Integer id;
    private String title;
    private String content;
    private String goods_name;
    private String goods_id;
    private String price;
    private String coupon;
    private String rake_back;
    private Integer ifcoupon;
    private String home_image;
    private String image;
    private String video;
    private Integer share_num;
    private Integer category;
    private Integer type;
    private Integer status;
    private String create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getRake_back() {
        return rake_back;
    }

    public void setRake_back(String rake_back) {
        this.rake_back = rake_back;
    }

    public Integer getIfcoupon() {
        return ifcoupon;
    }

    public void setIfcoupon(Integer ifcoupon) {
        this.ifcoupon = ifcoupon;
    }

    public String getHome_image() {
        return home_image;
    }

    public void setHome_image(String home_image) {
        this.home_image = home_image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Integer getShare_num() {
        return share_num;
    }

    public void setShare_num(Integer share_num) {
        this.share_num = share_num;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
