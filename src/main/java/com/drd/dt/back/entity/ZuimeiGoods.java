package com.drd.dt.back.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;

/**
 * 最美商品
 * Created by 86514 on 2019/11/7.
 */

@ApiModel(value="最美商品",description="最美商品")
@TableName("jz_zuimei_goods")
public class ZuimeiGoods {
    private Integer id;
    private String goods_name;
    private String goods_id;
    private String pic;
    private String itemdesc;
    private String shopname;
    private String price;
    private String price_coupons;
    private String price_after_coupons;
    private String anti_growth;
    private String createTime;
    private String updateTime;
    private Integer status;
    private Integer type;
    private Integer ifcoupon;
    private Integer sales;
    private String coupon_startTime;
    private String coupon_endTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getItemdesc() {
        return itemdesc;
    }

    public void setItemdesc(String itemdesc) {
        this.itemdesc = itemdesc;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_coupons() {
        return price_coupons;
    }

    public void setPrice_coupons(String price_coupons) {
        this.price_coupons = price_coupons;
    }

    public String getPrice_after_coupons() {
        return price_after_coupons;
    }

    public void setPrice_after_coupons(String price_after_coupons) {
        this.price_after_coupons = price_after_coupons;
    }

    public String getAnti_growth() {
        return anti_growth;
    }

    public void setAnti_growth(String anti_growth) {
        this.anti_growth = anti_growth;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIfcoupon() {
        return ifcoupon;
    }

    public void setIfcoupon(Integer ifcoupon) {
        this.ifcoupon = ifcoupon;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    public String getCoupon_startTime() {
        return coupon_startTime;
    }

    public void setCoupon_startTime(String coupon_startTime) {
        this.coupon_startTime = coupon_startTime;
    }

    public String getCoupon_endTime() {
        return coupon_endTime;
    }

    public void setCoupon_endTime(String coupon_endTime) {
        this.coupon_endTime = coupon_endTime;
    }
}
