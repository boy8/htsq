package com.drd.dt.back.entity;

/**
 * 自营商品规格
 */
public class GoodsSpecification {
    private int goods_id;
    private int id;
    private String etalon_name;
    private String etalon_value;
    private int sequence;
    private int status;
    private String file_path;

    /***
     * 扩展 是否选中 前端需要
     * */
    private int cl = 0;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCl() {
        return cl;
    }

    public void setCl(int cl) {
        this.cl = cl;
    }

    public int getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(int goods_id) {
        this.goods_id = goods_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEtalon_name() {
        return etalon_name;
    }

    public void setEtalon_name(String etalon_name) {
        this.etalon_name = etalon_name;
    }

    public String getEtalon_value() {
        return etalon_value;
    }

    public void setEtalon_value(String etalon_value) {
        this.etalon_value = etalon_value;
    }


    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }
}
