package com.drd.dt.back.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.drd.dt.common.Constant;

import java.text.ParseException;

/**
 * Created by 86514 on 2020/5/14.
 */
@TableName("jz_special_code")
public class SpecialCode {
    private Integer id;
    private String firend_code;
    private String create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirend_code() {
        return firend_code;
    }

    public void setFirend_code(String firend_code) {
        this.firend_code = firend_code;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        try {
            this.create_time = Constant.y_M_d_H_m_s.format(Constant.y_M_d_H_m_s.parse(create_time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
