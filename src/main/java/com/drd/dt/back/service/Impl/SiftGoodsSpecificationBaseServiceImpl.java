package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dao.ISiftGoodsSpecificationBaseMapper;
import com.drd.dt.back.dto.SiftGoodsSpecificationDTO;
import com.drd.dt.back.service.ISiftGoodsSpecificationBaseService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * SiftGoodsSpecification 精选核桃 商品 规格 接口实现
 */
@Transactional
@Service("siftGoodsSpecificationBaseService")
public class SiftGoodsSpecificationBaseServiceImpl implements ISiftGoodsSpecificationBaseService {
    @Autowired
    private ISiftGoodsSpecificationBaseMapper siftGoodsSpecificationBaseMapper;

    public Tip insert(SiftGoodsSpecificationDTO siftGoodsSpecificationDTO) {
        int req = siftGoodsSpecificationBaseMapper.insert(siftGoodsSpecificationDTO);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.ADD_SUCCESS.getCode(), SuccessResultEnum.ADD_SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.ADD_ERROR.getCode(), BizExceptionEnum.ADD_ERROR.getMessage());
    }

    public Tip update(SiftGoodsSpecificationDTO siftGoodsSpecificationDTO) {
        int req = siftGoodsSpecificationBaseMapper.updata(siftGoodsSpecificationDTO);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.UPDATE_SUCCESS.getCode(), SuccessResultEnum.UPDATE_SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.UPDATE_ERROR.getCode(), BizExceptionEnum.UPDATE_ERROR.getMessage());
    }

    public Tip delete(String id) {
        siftGoodsSpecificationBaseMapper.delete(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    public Tip queryAll(String id) {
        SiftGoodsSpecificationDTO siftGoodsSpecification = siftGoodsSpecificationBaseMapper.queryAll(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), siftGoodsSpecification);
    }

    public Tip pageSelect(String good_id) {
        List<SiftGoodsSpecificationDTO> list = siftGoodsSpecificationBaseMapper.pageSelect(good_id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);
    }
}
