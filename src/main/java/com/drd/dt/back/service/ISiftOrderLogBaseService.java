package com.drd.dt.back.service;

import com.drd.dt.back.dto.SiftOrderLogDTO;
import com.drd.dt.common.tips.Tip;

/**
 * SiftOrderLog 精选核桃 商品 订单 售后记录表表 接口定义
 */
public interface ISiftOrderLogBaseService {
    Tip insert(SiftOrderLogDTO siftOrderLog);

    Tip update(SiftOrderLogDTO siftOrderLog);

    Tip delete(Integer id);

    Tip queryAll(Integer id);

    Tip pageSelect(SiftOrderLogDTO siftOrderLog);
}