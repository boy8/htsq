package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.drd.dt.back.dao.AdvManageMapper;
import com.drd.dt.back.entity.AdvBack;
import com.drd.dt.back.service.IAdvManageService;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service
public class AdvManagerServiceImpl extends ServiceImpl<AdvManageMapper, AdvBack> implements IAdvManageService {

    @Autowired
    private AdvManageMapper advManageMapper;

    /**
     * 广告列表
     */
    @Override
    public Tip getAdvList() throws Exception {
        List<AdvBack> result = advManageMapper.getAdvList();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 新增广告
     * @return
     */
    @Override
    public Tip insertAdv(AdvBack advBack) throws Exception{
        String end_time = advBack.getEnd_time();
        if (StringUtils.isEmpty(end_time)){
            throw new BussinessException(BizExceptionEnum.END_TYPE_EMPTY);
        }
        advBack.setCreated_time(Constant.y_M_d_H_m_s.format(new Date()));
        advManageMapper.insert(advBack);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 删除广告
     **/
    @Override
    public Tip delete(Integer id) throws Exception{
        advManageMapper.deleteById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 修改
     */
    @Override
    public Tip update(AdvBack advBack) throws Exception{
        String end_time = advBack.getEnd_time();
        if (StringUtils.isEmpty(end_time)){
            throw new BussinessException(BizExceptionEnum.END_TYPE_EMPTY);
        }
        advManageMapper.updateById(advBack);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     *获取广告详情
     **/
    @Override
    public Tip advDetailInfo(Integer id) throws Exception{
        AdvBack advBack = advManageMapper.selectById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),advBack);
    }

    /**
     * @param id 主键ID
     * */
    public int getId(int id){
        int v = -1;
        v = 0;  //此处 获取sql  查询结果 select pent_id from table where id = #{id}  //拿到 父亲ID
        return v < 0 ? -1 : getId(v);
    }

}
