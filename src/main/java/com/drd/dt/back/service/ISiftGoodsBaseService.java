package com.drd.dt.back.service;

import com.drd.dt.back.dto.SiftGoodsDTO;
import com.drd.dt.common.tips.Tip;
/**
 * SiftGoodsDTO 精选核桃 商品 接口定义
 * */
public interface ISiftGoodsBaseService {

    Tip insert(SiftGoodsDTO siftGoodsDTO);

    Tip update(SiftGoodsDTO siftGoodsDTO);

    Tip delete(String id);

    Tip queryAll(String id);

    Tip pageSelect(SiftGoodsDTO siftGoodsDTO);
}
