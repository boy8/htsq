package com.drd.dt.back.service.Impl;

import com.drd.dt.back.dao.InviteActivityMapper;
import com.drd.dt.back.service.IInviteActivityService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service
public class InviteActivityServiceImpl implements IInviteActivityService {

    @Autowired
    private InviteActivityMapper inviteActivityMapper;

    @Override
    public Tip dataReport() {
        List<Map> list = new ArrayList<>();
        Map map =  inviteActivityMapper.dataReport();
        list.add(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),list);
    }
}
