package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dao.ISiftOrderBaseMapper;
import com.drd.dt.back.dao.ISiftOrderLogBaseMapper;
import com.drd.dt.back.dto.OwnGoodsOrderDTO;
import com.drd.dt.back.dto.SiftGoodsDTO;
import com.drd.dt.back.dto.SiftOrderDTO;
import com.drd.dt.back.service.ISiftOrderBaseService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.modular.dao.ISiftOrderLogMapper;
import com.drd.dt.modular.dao.SiftGoodsMapper;
import com.drd.dt.modular.dao.SiftOrderMapper;
import com.drd.dt.modular.dao.UserMapper;
import com.drd.dt.modular.entity.User;
import com.drd.dt.modular.service.impl.UserMoneyServiceImpl;
import com.drd.dt.util.ExcelUtil;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * SiftOrder 精选核桃 商品 订单表 接口实现
 */
@Transactional
@Service("siftOrderBaseService")
public class SiftOrderBaseServiceImpl implements ISiftOrderBaseService {
    @Autowired
    private ISiftOrderBaseMapper iSiftOrderMapper;
    @Autowired
    private UserMoneyServiceImpl userMoneyService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SiftGoodsMapper siftGoodsMapper;
    @Autowired
    private ISiftOrderLogMapper siftOrderLogMapper;

    public Tip insert(SiftOrderDTO siftOrderDTO) {
        int req = iSiftOrderMapper.insert(siftOrderDTO);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.ADD_SUCCESS.getCode(), SuccessResultEnum.ADD_SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.ADD_ERROR.getCode(), BizExceptionEnum.ADD_ERROR.getMessage());
    }

    public Tip update(SiftOrderDTO siftOrderDTO) {
        //根改状态
        if (siftOrderDTO.getStatus() == 1) { //结算前 先查询一次是否是已结算订单
            //启动  结算功能
            SiftOrderDTO userOrder = iSiftOrderMapper.queryAll(siftOrderDTO.getId().intValue());
            SiftGoodsDTO siftGoodsDTO = siftGoodsMapper.SiftGoodsDateil(userOrder.getProduct_id());
            Integer user_id = userOrder.getUser_id();
            User user = userMapper.selectUserId(user_id);
            if ((userOrder.getStatus() == 2 || userOrder.getStatus() == 3) && (userOrder.getRefund_status() == 0 || userOrder.getRefund_status() == 3)) {
                //校验 是否有 jz_balance_detail  信息记录 ：根据订单信息查询
                int reqCount = iSiftOrderMapper.orderPureInBalanceDetail(userOrder.getOrder_id());
                if (reqCount == 0) { //
                    userOrder.setStatus(1);
                    userMoneyService.updateSiftOrderBrokerage(user, userOrder, siftGoodsDTO);
                    //完成订单 结算 修改 用户返佣金额
                    siftOrderDTO.setUser_rake_back(userOrder.getUser_rake_back_yugu());
                }
            }
        }
        siftOrderDTO.setUpdate_time(new Date());
        int req = iSiftOrderMapper.updata(siftOrderDTO);
        if (req < 1) {
            return ResultUtil.result(BizExceptionEnum.UPDATE_ERROR.getCode(), BizExceptionEnum.UPDATE_ERROR.getMessage());
        }


        return ResultUtil.result(SuccessResultEnum.UPDATE_SUCCESS.getCode(), SuccessResultEnum.UPDATE_SUCCESS.getMessage());


    }

    public Tip delete(Integer id) {
        iSiftOrderMapper.delete(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    public Tip queryAll(Integer id) {
        SiftOrderDTO siftOrder = iSiftOrderMapper.queryAll(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), siftOrder);
    }

    public Tip pageSelect(OwnGoodsOrderDTO ownGoodsOrderDTO) {
        List<OwnGoodsOrderDTO> list = iSiftOrderMapper.pageSelect(ownGoodsOrderDTO);
        for (OwnGoodsOrderDTO dto : list) {

            List<Map> maps = siftOrderLogMapper.getLog(dto.getOrder_id());
            if (null != maps && maps.size() > 0) {
                Map map = maps.get(0);
                if (null != map) {
                    String parcel_number = "", parcel_type = "";
                    if (map.containsKey("parcel_number")) {
                        parcel_number = (String) map.get("parcel_number");
                    }
                    if (map.containsKey("parcel_type")) {
                        parcel_type = (String) map.get("parcel_type");
                    }
                    dto.setParcel_type(parcel_type);
                    dto.setParcel_number(parcel_number);
                }
            }

        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);
    }


    /**
     * 导出自营订单数据
     **/
    @Override
    public void excelExport(OwnGoodsOrderDTO orderDTO, HttpServletResponse response) {
        List<OwnGoodsOrderDTO> list = iSiftOrderMapper.selectExportOrderList(orderDTO);
        //开始导出
        Map<String, String> titleMap = new LinkedHashMap<>();
        titleMap.put("product_name", "产品名称");
        titleMap.put("etalon_value", "规格");
        titleMap.put("number", "数量");
        titleMap.put("order_id", "订单号");
        titleMap.put("shipping_user_name", "姓名");
        titleMap.put("shipping_phone", "电话");
        titleMap.put("shipping_address", "地址");
        titleMap.put("actual_amount", "付款金额");
        titleMap.put("logistics_num", "物流号");
        titleMap.put("trade_no", "交易号");
        titleMap.put("paytypeText", "支付方式");
        titleMap.put("statusText", "订单状态");
        titleMap.put("refund_statusText", "订单退款状态");
        titleMap.put("create_time", "创建时间");
        String fileName = "核桃精选订单信息";
        ExcelUtil.excelExport(list, titleMap, fileName, response);
    }


}
