package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.drd.dt.back.dao.OwnGoodsManageMapper;
import com.drd.dt.back.dto.OwnGoodsDTO;
import com.drd.dt.back.dto.OwnGoodsInsertDTO;
import com.drd.dt.back.dto.OwnGoodsOrderDTO;
import com.drd.dt.back.dto.OwnGoodsSizeDTO;
import com.drd.dt.back.service.IOwnGoodsManageService;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.util.ExcelUtil;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Service
@Transactional
public class OwnGoodsManageServiceImpl extends ServiceImpl<OwnGoodsManageMapper, OwnGoodsDTO> implements IOwnGoodsManageService {
    @Autowired
    private OwnGoodsManageMapper ownGoodsManageMapper;

    @Override
    public Tip selectAll(String title, String state) throws Exception {
        List<OwnGoodsDTO> all = ownGoodsManageMapper.selectAll(title, state);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), all);
    }

    @Override
    public Tip addOwnGoods(OwnGoodsInsertDTO insertDTO) throws Exception {
        OwnGoodsDTO ownGoods = new OwnGoodsDTO();
        BeanUtils.copyProperties(insertDTO, ownGoods);
        ownGoods.setCreate_time(Constant.y_M_d_H_m_s.format(new Date()));
        ownGoodsManageMapper.insert(ownGoods);
        return ResultUtil.result(SuccessResultEnum.ADD_SUCCESS.getCode(), SuccessResultEnum.ADD_SUCCESS.getMessage());
    }

    @Override
    public Tip update(OwnGoodsSizeDTO ownGoodsSizeDTO) throws Exception {
        OwnGoodsDTO ownGoods = (OwnGoodsDTO) ownGoodsSizeDTO;
        ownGoodsManageMapper.updateById(ownGoods);
        return ResultUtil.result(SuccessResultEnum.UPDATE_SUCCESS.getCode(), SuccessResultEnum.UPDATE_SUCCESS.getMessage());
    }

    public Tip detail(Integer id) throws Exception {
        OwnGoodsSizeDTO ownGoods = ownGoodsManageMapper.getDetailId(id);
        //查询规格
        List<Map<String, String>> maps = ownGoodsManageMapper.getGoodsSpecificationDetail(id);
        Map<String, String> m = new HashMap<String, String>();
        String size_S = "";
        if (null != maps && maps.size() > 0) {
            for (Map ma : maps) {//颜色:翡翠绿、玛瑙红、大理黑,尺寸:100mm*330
                String size = "";
                if (ma.containsKey("id")) {
                    size += (Integer) ma.get("id") + "-";
                }
                if (ma.containsKey("etalon_value")) {
                    size += (String) ma.get("etalon_value") + "-";
                }
                if (ma.containsKey("status")) {
                    size += "(" + (Integer) ma.get("status") + ")";
                }
                if (size_S.length() > 1) {
                    size_S += ",";
                }
                size_S += size;
            }
        }
        ownGoods.setSize(size_S);


        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), ownGoods);
    }

    /**
     * 删除数据
     */
    @Override
    public Tip delete(Integer id) throws Exception {
        ownGoodsManageMapper.deleteById(id);
        ownGoodsManageMapper.deleteGoodsSpecification(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 自营订单列表
     */
    @Override
    public Tip orderList(OwnGoodsOrderDTO orderDTO) throws Exception {
        List<OwnGoodsOrderDTO> list = ownGoodsManageMapper.selectOrderList(orderDTO);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);
    }

    /**
     * 自营订单详情
     */
    @Override
    public Tip orderDetail(Integer id) throws Exception {
        OwnGoodsOrderDTO dto = ownGoodsManageMapper.orderDetail(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), dto);
    }

    /**
     * 修改自营订单
     */
    @Override
    public Tip orderUpdate(OwnGoodsOrderDTO orderDTO) {
        ownGoodsManageMapper.orderUpdate(orderDTO);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 导出自营订单数据
     **/
    @Override
    public void excelExport(OwnGoodsOrderDTO orderDTO, HttpServletResponse response) {
        List<OwnGoodsOrderDTO> list = ownGoodsManageMapper.selectExportOrderList(orderDTO);
        //开始导出
        Map<String, String> titleMap = new LinkedHashMap<>();
        titleMap.put("product_name", "产品名称");
        titleMap.put("etalon_value", "规格");
        titleMap.put("order_id", "订单号");
        titleMap.put("shipping_user_name", "姓名");
        titleMap.put("shipping_phone", "电话");
        titleMap.put("shipping_address", "地址");
        titleMap.put("actual_amount", "付款金额");
        titleMap.put("logistics_num", "物流号");
        titleMap.put("trade_no", "交易号");
        titleMap.put("paytype", "支付方式");
        titleMap.put("order_status", "订单状态");
        titleMap.put("create_time", "创建时间");
        String fileName = "自营订单信息";
        ExcelUtil.excelExport(list, titleMap, fileName, response);
    }
}
