package com.drd.dt.back.service;

import com.drd.dt.back.dto.SiftGoodsSpecificationDTO;
import com.drd.dt.common.tips.Tip;

public interface ISiftGoodsSpecificationBaseService {

    Tip insert(SiftGoodsSpecificationDTO siftGoodsSpecification);

    Tip update(SiftGoodsSpecificationDTO siftGoodsSpecification);

    Tip delete(String id);

    Tip queryAll(String id);

    Tip pageSelect(String good_id);
}
