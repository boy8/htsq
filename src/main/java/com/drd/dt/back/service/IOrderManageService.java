package com.drd.dt.back.service;


import com.drd.dt.common.tips.Tip;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface IOrderManageService {
    /**
     * 获取订单列表
     * @return
     */
    Tip list(Map<Object, Object> map) throws Exception;

    /**
     *  上传淘宝excel订单更新
     */
    Tip taobaoImport(MultipartFile file) throws Exception;

    /**
     *  上传京东excel订单更新
     */
    Tip jdImport(MultipartFile file) throws Exception;

    /**
     *  上传拼多多xcel订单更新
     */
    Tip pddImport(MultipartFile file) throws Exception;

    /**
     *  上传唯品会excel订单更新
     */
    Tip wphImport(MultipartFile file) throws Exception;
}
