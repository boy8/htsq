package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.drd.dt.back.dao.AdvManageMapper;
import com.drd.dt.back.dao.BusinessDailyMapper;
import com.drd.dt.back.entity.AdvBack;
import com.drd.dt.back.entity.BusinessDaily;
import com.drd.dt.back.service.IAdvManageService;
import com.drd.dt.back.service.IBusinessDailyService;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service
public class BusinessDailyServiceImpl extends ServiceImpl<BusinessDailyMapper, BusinessDaily> implements IBusinessDailyService {

    @Autowired
    private BusinessDailyMapper businessDailyMapper;


    /**
     * 获取每日爆款列表
     **/
    @Override
    public Tip getList(Map<String, String> param) throws Exception{
        Integer offset = Integer.valueOf(param.get("offset"));
        Integer pageSize = Constant.PAGE_SIZE_20;
        Integer pageNum = offset / pageSize + 1;
        Page page = new Page(pageNum, pageSize);
        List<BusinessDaily> list = businessDailyMapper.getList(page,param);
        page.setRecords(list);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),page);
    }

    /**
     * 删除每日爆款
     **/
    @Override
    public Tip delete(Integer id) throws Exception {
        businessDailyMapper.deleteById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 修改每日爆款
     **/
    @Override
    public Tip update(BusinessDaily businessDaily) throws Exception {
        businessDailyMapper.updateById(businessDaily);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 获取每日爆款详情
     **/
    @Override
    public Tip advDetailInfo(Integer id) throws Exception {
        BusinessDaily businessDaily = businessDailyMapper.selectById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),businessDaily);
    }
}
