package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.drd.dt.back.dao.BrandNavigationBackMapper;
import com.drd.dt.back.entity.BrandNavigation;
import com.drd.dt.back.service.IBrandNavigationBackService;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BrandNavigationBackServiceImpl extends ServiceImpl<BrandNavigationBackMapper, BrandNavigation> implements IBrandNavigationBackService {

    @Autowired
    private BrandNavigationBackMapper brandNavigationBaseMapper;


    /**
     * 获取品牌导航列表
     **/
    @Override
    public Tip selectList(String type) throws Exception{
        Wrapper<BrandNavigation> wrapper = new EntityWrapper<>();
        if (!StringUtils.isEmpty(type)){
            wrapper.eq("type",type);
        }
        wrapper.orderBy("create_time",false);
        List<BrandNavigation> brandNavigations = brandNavigationBaseMapper.selectList(wrapper);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), brandNavigations);
    }

    /**
     * 新增品牌导航
     **/
    @Override
    public Tip insertBrandNavigation(BrandNavigation brandNavigation) throws Exception{
        brandNavigation.setCreate_time(Constant.y_M_d_H_m_s.format(new Date()));
        brandNavigationBaseMapper.insert(brandNavigation);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 删除品牌导航
     **/
    @Override
    public Tip delete(Integer id) throws Exception{
        brandNavigationBaseMapper.deleteById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 修改品牌导航
     **/
    @Override
    public Tip update(BrandNavigation brandNavigation) throws Exception{
        brandNavigationBaseMapper.updateById(brandNavigation);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 获取品牌导航详情
     **/
    @Override
    public Tip advDetailInfo(Integer id) throws Exception{
        BrandNavigation brandNavigation = brandNavigationBaseMapper.selectById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),brandNavigation);
    }
}
