package com.drd.dt.back.service;

import com.drd.dt.back.entity.BrandNavigation;
import com.drd.dt.common.tips.Tip;

public interface IBrandNavigationBackService {

    /**
     * 获取品牌导航列表
     **/
    Tip selectList(String type) throws Exception;

    /**
     * 新增品牌导航
     **/
    Tip insertBrandNavigation(BrandNavigation brandNavigation) throws Exception;

    /**
     * 删除品牌导航
     **/
    Tip delete(Integer id) throws Exception;

    /**
     * 修改品牌导航
     **/
    Tip update(BrandNavigation brandNavigation) throws Exception;

    /**
     * 获取品牌导航详情
     **/
    Tip advDetailInfo(Integer id) throws Exception;
}
