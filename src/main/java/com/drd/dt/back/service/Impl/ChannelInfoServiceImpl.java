package com.drd.dt.back.service.Impl;

import com.drd.dt.util.ResultUtil;
import com.drd.dt.back.dao.BackCommonMapper;
import com.drd.dt.back.dto.ChannelInfoDTO;
import com.drd.dt.back.service.IChannelInfoService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service
public class ChannelInfoServiceImpl implements IChannelInfoService {

    @Autowired
    private BackCommonMapper commonMapper;

    /**
     * 渠道信息统计
     * @param map
     * @return
     */
    @Override
    public Tip channelInfo(Map<Object, Object> map) throws Exception{
        List<ChannelInfoDTO> result = new ArrayList<>();
        String channel_id = "";
        String startTime = "";
        String endTime = "";
        if (map.containsKey("channel_id")){
            channel_id = map.get("channel_id").toString();
        }
        if (map.containsKey("startTime")){
            startTime = map.get("startTime").toString();
        }
        if (map.containsKey("endTime")){
            endTime = map.get("endTime").toString();
        }
        //当渠道id和时间都不为空时，分天显示
        if (!StringUtils.isEmpty(channel_id) && !StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime)){
            List<ChannelInfoDTO> userNum = commonMapper.userNumListByTime(map);
            List<ChannelInfoDTO> orderNum = commonMapper.orderNumListByTime(map);
            List<ChannelInfoDTO> userActiveNum = commonMapper.userActiveNumListByTime(map);
            List<ChannelInfoDTO> newlyUserNum = commonMapper.newlyUserNumListByTime(map);
            List<ChannelInfoDTO> orderPeopleNum = commonMapper.orderPeopleNumListByTime(map);
            List<String> daysDate = findDaysDate(startTime, endTime);
            String finalChannel_id = channel_id;
            final Integer[] user = {0,0,0,0,0};
            ChannelInfoDTO total = new ChannelInfoDTO();
            daysDate.stream().forEach(t ->{
                ChannelInfoDTO infoDTO = new ChannelInfoDTO();
                infoDTO.setTime(t);
                infoDTO.setChannel_id(finalChannel_id);
                userNum.stream().forEach(u -> {
                    if (t.equals(u.getTime())) {
                        infoDTO.setUserNum(u.getUserNum());
                        user[0] = user[0] + u.getUserNum();
                    }
                });
                orderNum.stream().forEach(o -> {
                    if (t.equals(o.getTime())) {
                        infoDTO.setOrderNum(o.getOrderNum());
                        user[1] = user[1] + o.getOrderNum();
                    }
                });
                userActiveNum.stream().forEach(u -> {
                    if (t.equals(u.getTime())) {
                        infoDTO.setUserActiveNum(u.getUserActiveNum());
                        user[2] = user[2] + u.getUserActiveNum();
                    }
                });
                newlyUserNum.stream().forEach(n -> {
                    if (t.equals(n.getTime())) {
                        infoDTO.setNewlyUserNum(n.getNewlyUserNum());
                        user[3] = user[3] + n.getNewlyUserNum();
                    }
                });
                orderPeopleNum.stream().forEach(n -> {
                    if (t.equals(n.getTime())) {
                        infoDTO.setOrderPeopleNum(n.getOrderPeopleNum());
                        user[4] = user[4] + n.getOrderPeopleNum();
                    }
                });
                result.add(infoDTO);
            });
            total.setChannel_id("总计");
            total.setUserNum(user[0]);
            total.setOrderNum(user[1]);
            total.setUserActiveNum(user[2]);
            total.setNewlyUserNum(user[3]);
            total.setOrderPeopleNum(user[4]);
            result.add(total);
        }else {
            List<ChannelInfoDTO> userNum = commonMapper.userNumList(map);
            List<ChannelInfoDTO> orderNum = commonMapper.orderNumList(map);
            List<ChannelInfoDTO> userActiveNum = commonMapper.userActiveNumList(map);
            List<ChannelInfoDTO> newlyUserNum = commonMapper.newlyUserNum(map);
            List<ChannelInfoDTO> orderPeopleNum = commonMapper.orderPeopleNum(map);

            List<String> list = new ArrayList<>();
            userNum.stream().forEach(t -> list.add(t.getChannel_id()));
            orderNum.stream().forEach(t -> list.add(t.getChannel_id()));
            List<String> channelIds = list.stream().distinct().collect(Collectors.toList());
            channelIds.stream().forEach(t -> {
                ChannelInfoDTO infoDTO = new ChannelInfoDTO();
                infoDTO.setChannel_id(t);
                userNum.stream().forEach(u -> {
                    if (t.equals(u.getChannel_id())) {
                        infoDTO.setUserNum(u.getUserNum());
                    }
                });
                orderNum.stream().forEach(o -> {
                    if (t.equals(o.getChannel_id())) {
                        infoDTO.setOrderNum(o.getOrderNum());
                    }
                });
                userActiveNum.stream().forEach(u -> {
                    if (t.equals(u.getChannel_id())) {
                        infoDTO.setUserActiveNum(u.getUserActiveNum());
                    }
                });
                newlyUserNum.stream().forEach(n -> {
                    if (t.equals(n.getChannel_id())) {
                        infoDTO.setNewlyUserNum(n.getNewlyUserNum());
                    }
                });
                orderPeopleNum.stream().forEach(n -> {
                    if (t.equals(n.getChannel_id())) {
                        infoDTO.setOrderPeopleNum(n.getOrderPeopleNum());
                    }
                });
                result.add(infoDTO);
            });
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 获取开始到结束时间段内的每一天日期
     * @param begintTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public static List<String> findDaysDate(String begintTime, String endTime) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dBegin = sdf.parse(begintTime);
        Date dEnd = sdf.parse(endTime);
        //存放每一天日期String对象的daysStrList
        List<String> daysStrList = new ArrayList<String>();
        //放入开始的那一天日期String
        daysStrList.add(sdf.format(dBegin));

        Calendar calBegin = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calBegin.setTime(dBegin);

        Calendar calEnd = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calEnd.setTime(dEnd);

        // 判断循环此日期是否在指定日期之后
        while (dEnd.after(calBegin.getTime())) {
            // 根据日历的规则，给定的日历字段增加或减去指定的时间量
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            String dayStr = sdf.format(calBegin.getTime());
            daysStrList.add(dayStr);
        }
        return daysStrList;
    }
}
