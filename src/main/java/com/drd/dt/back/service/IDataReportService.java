package com.drd.dt.back.service;

import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface IDataReportService {
    /**
     * 数据报表信息统计-渠道汇总
     **/
    Tip channelList(Map<String, String> map);

    /**
     * 数据报表信息统计-平台汇总
     **/
    Tip terraceList(Map<String, String> map);

    /**
     * 数据报表信息统计-订单收益汇总
     **/
    Tip incomeList(Map<String, String> map);

    /**
     * 数据报表信息统计-自营订单收益汇总
     **/
    Tip ownOrderList(Map<String, String> map);

    /**
     * 数据报表信息统计-活跃统计
     **/
    Tip activityList(Map<String, String> map) throws Exception;

    /**
     * 数据报表信息统计-订单汇总
     **/
    Tip rateList(Map<String, String> map);
}
