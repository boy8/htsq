package com.drd.dt.back.service;

import com.drd.dt.back.dto.*;
import com.drd.dt.common.tips.Tip;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface HtmlOrderBaseService {

    Tip orderList(OwnGoodsOrderDTO orderDTO) throws Exception;


    Tip orderDetail(Integer id) throws Exception;

    Tip orderUpdate(OwnGoodsOrderDTO orderDTO);

    /**
     * 导出订单数据
     **/
    void excelExport(OwnGoodsOrderDTO orderDTO, HttpServletResponse response);

    /************************漂亮分割线  商品功能*****************************/
    /**
     * 查询商品
     */
    Tip selectAll(String title, String state) throws Exception;

    /**
     * 删除数据
     */
    Tip delete(Integer id) throws Exception;

    /**
     * 新增  商品
     */
    Tip addOwnGoods(OwnGoodsInsertDTO insertDTO) throws Exception;

    /**
     * 修改  商品
     */
    Tip update(HtmlOwnGoodsSizeDTO htmlOwnGoodsSizeDTO) throws Exception;

    /**
     * 查询单个
     */
    Tip detail(Integer id) throws Exception;


    /**************************商品规格后端功能**************************************/
    //查询商品规格信息
    Tip selectGoodsSpecificationDetail(Integer id);

    Tip addGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification);

    Tip finaAll(Integer id);

    Tip updateGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification);

    Tip deleteSpecification(Integer id);


    /****************************退款***************************************************/
    Tip applyFor(Integer id)throws Exception;

    /**************************** 统计 ***************************************************/
    Tip dataReportHtmlOrder()throws Exception;
}
