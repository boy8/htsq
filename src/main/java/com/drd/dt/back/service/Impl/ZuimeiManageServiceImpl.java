package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.drd.dt.back.dao.ZeroPurchaseMapper;
import com.drd.dt.back.dao.ZuimeiManageMapper;
import com.drd.dt.back.dto.ZeroPurchaseDTO;
import com.drd.dt.back.entity.ZuimeiGoods;
import com.drd.dt.back.service.IzuimeiManageService;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service
public class ZuimeiManageServiceImpl extends ServiceImpl<ZuimeiManageMapper, ZuimeiGoods> implements IzuimeiManageService {

    @Autowired
    private ZuimeiManageMapper zuimeiManageMapper;


    /**
     * 列表
     */
    @Override
    public Tip list(Map<String, String> map) throws Exception{
        List<ZuimeiGoods> list = zuimeiManageMapper.selectList();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),list);
    }

    /**
     * 新增
     */
    @Override
    public Tip insert(Map<String,String> map) throws Exception{
        String goods_name = map.get("goods_name");
        String goods_id = map.get("goods_id");
        String pic = map.get("pic");
        String price = map.get("price");
        String price_coupons = map.get("price_coupons");
        String shopname = map.get("shopname");
        String price_after_coupons = map.get("price_after_coupons");
        String sales = map.get("price_after_coupons");
        String anti_growth = map.get("anti_growth");
        if (StringUtils.isEmpty(goods_name) || StringUtils.isEmpty(goods_id) || StringUtils.isEmpty(pic)
                || StringUtils.isEmpty(shopname) || StringUtils.isEmpty(price) || StringUtils.isEmpty(price_coupons)
                || StringUtils.isEmpty(price_after_coupons) || StringUtils.isEmpty(anti_growth) || StringUtils.isEmpty(sales)){
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        String now = Constant.y_M_d_H_m_s.format(new Date());
        map.put("create_time",now);
        zuimeiManageMapper.insert(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 更新
     */
    @Override
    public Tip update(Map<String,String> map) throws Exception{
        String goods_name = map.get("goods_name");
        String goods_id = map.get("goods_id");
        String pic = map.get("pic");
        String shopname = map.get("shopname");
        String price_coupons = map.get("price_coupons");
        String coupon_endTime = map.get("coupon_endTime");
        String sales = map.get("price_after_coupons");
        String coupon_startTime = map.get("coupon_startTime");
        String price = map.get("price");
        String price_after_coupons = map.get("price_after_coupons");
        String anti_growth = map.get("anti_growth");
        if (StringUtils.isEmpty(goods_name) || StringUtils.isEmpty(goods_id) || StringUtils.isEmpty(pic)
                || StringUtils.isEmpty(shopname) || StringUtils.isEmpty(price) || StringUtils.isEmpty(price_coupons)
                || StringUtils.isEmpty(price_after_coupons) || StringUtils.isEmpty(anti_growth)|| StringUtils.isEmpty(sales)
                || StringUtils.isEmpty(coupon_startTime) || StringUtils.isEmpty(coupon_endTime)){
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        String now = Constant.y_M_d_H_m_s.format(new Date());
        map.put("updateTime",now);
        zuimeiManageMapper.update(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 删除
     */
    @Override
    public Tip delete(Integer id) throws Exception{
        zuimeiManageMapper.delete(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 详情
     */
    @Override
    public Tip detail(Integer id) throws Exception{
        ZuimeiGoods zeroPurchaseDTO = zuimeiManageMapper.detail(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),zeroPurchaseDTO);
    }

}
