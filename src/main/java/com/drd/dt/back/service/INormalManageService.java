package com.drd.dt.back.service;


import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.entity.InitConfig;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface INormalManageService {
    /**
     * 获取基础设置信息
     * @return
     */
    Tip baseInfo() throws Exception;

    /**
     * 修改基础设置信息
     * @param map
     * @return
     */
    Tip updateBaseInfo(Map<Object, Object> map) throws Exception;

    /**
     * 获取参数设置信息
     **/
    Tip parameterInfo() throws Exception;

    /**
     * 修改参数设置信息
     **/
    Tip updateParameterInfo(Map<Object, Object> map) throws Exception;

    /**
     * 获取初始化设置信息
     **/
    Tip initrInfo() throws Exception;

    /**
     * 更新初始化设置信息
     **/
    Tip updateInit(InitConfig config);
}
