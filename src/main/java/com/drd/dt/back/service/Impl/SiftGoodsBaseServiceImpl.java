package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dao.ISiftGoodsBaseMapper;
import com.drd.dt.back.dto.SiftGoodsDTO;
import com.drd.dt.back.service.ISiftGoodsBaseService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * SiftGoodsDTO 精选核桃 商品 接口实现
 */

@Transactional
@Service("siftGoodsService")
public class SiftGoodsBaseServiceImpl implements ISiftGoodsBaseService {

    @Autowired
    private ISiftGoodsBaseMapper siftGoodsBaseMapper;

    public Tip insert(SiftGoodsDTO siftGoodsDTO) {
        int req = siftGoodsBaseMapper.insert(siftGoodsDTO);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.ADD_SUCCESS.getCode(), SuccessResultEnum.ADD_SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.ADD_ERROR.getCode(), BizExceptionEnum.ADD_ERROR.getMessage());
    }

    public Tip update(SiftGoodsDTO siftGoodsDTO) {
        int req = siftGoodsBaseMapper.updata(siftGoodsDTO);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.UPDATE_SUCCESS.getCode(), SuccessResultEnum.UPDATE_SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.UPDATE_ERROR.getCode(), BizExceptionEnum.UPDATE_ERROR.getMessage());
    }

    public Tip delete(String id) {
        siftGoodsBaseMapper.delete(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    public Tip queryAll(String id) {
        SiftGoodsDTO siftGoodsDTO = siftGoodsBaseMapper.queryAll(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), siftGoodsDTO);
    }

    public Tip pageSelect(SiftGoodsDTO siftGoodsDTO) {
        List<SiftGoodsDTO> list = siftGoodsBaseMapper.pageSelect(siftGoodsDTO);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);
    }
}