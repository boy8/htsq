package com.drd.dt.back.service;


import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface IzuimeiManageService {

    Tip list(Map<String, String> map) throws Exception;

    Tip insert(Map<String, String> map) throws Exception;

    Tip update(Map<String, String> map) throws Exception;

    Tip delete(Integer id) throws Exception;

    Tip detail(Integer id) throws Exception;

}
