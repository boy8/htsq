package com.drd.dt.back.service.Impl;

import com.drd.dt.common.Constant;
import com.drd.dt.util.ResultUtil;
import com.drd.dt.back.dao.ZeroPurchaseMapper;
import com.drd.dt.back.dto.ZeroPurchaseDTO;
import com.drd.dt.back.service.IZeroPurchaseService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service("zeroPurchaseService")
public class ZeroPurchaseServiceImpl implements IZeroPurchaseService {

    @Autowired
    private ZeroPurchaseMapper purchaseMapper;


    /**
     * 列表
     */
    @Override
    public Tip list(Map<Object, Object> map) throws Exception{
        List<ZeroPurchaseDTO> list = purchaseMapper.selectList();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),list);
    }

    /**
     * 新增
     */
    @Override
    public Tip insert(Map<String,String> map) throws Exception{
        String goods_name = map.get("goods_name");
        String goods_id = map.get("goods_id");
        String pic = map.get("pic");
        String shopname = map.get("shopname");
        String price = map.get("price");
        String price_coupons = map.get("price_coupons");
        String price_after_coupons = map.get("price_after_coupons");
        String sales = map.get("price_after_coupons");
        String anti_growth = map.get("anti_growth");
        if (StringUtils.isEmpty(goods_name) || StringUtils.isEmpty(goods_id) || StringUtils.isEmpty(pic)
                || StringUtils.isEmpty(shopname) || StringUtils.isEmpty(price) || StringUtils.isEmpty(price_coupons)
                || StringUtils.isEmpty(price_after_coupons) || StringUtils.isEmpty(anti_growth) || StringUtils.isEmpty(sales)){
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        String now = Constant.y_M_d_H_m_s.format(new Date());
        map.put("create_time",now);
        purchaseMapper.insert(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 更新
     */
    @Override
    public Tip update(Map<String,String> map) throws Exception{
        String goods_name = map.get("goods_name");
        String goods_id = map.get("goods_id");
        String pic = map.get("pic");
        String shopname = map.get("shopname");
        String price_coupons = map.get("price_coupons");
        String sales = map.get("price_after_coupons");
        String coupon_startTime = map.get("coupon_startTime");
        String coupon_endTime = map.get("coupon_endTime");
        String price = map.get("price");
        String price_after_coupons = map.get("price_after_coupons");
        String anti_growth = map.get("anti_growth");
        if (StringUtils.isEmpty(goods_name) || StringUtils.isEmpty(goods_id) || StringUtils.isEmpty(pic)
                || StringUtils.isEmpty(shopname) || StringUtils.isEmpty(price) || StringUtils.isEmpty(price_coupons)
                || StringUtils.isEmpty(price_after_coupons) || StringUtils.isEmpty(anti_growth)|| StringUtils.isEmpty(sales)
                || StringUtils.isEmpty(coupon_startTime) || StringUtils.isEmpty(coupon_endTime)){
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        String now = Constant.y_M_d_H_m_s.format(new Date());
        map.put("updateTime",now);
        purchaseMapper.update(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 删除
     */
    @Override
    public Tip delete(Integer id) throws Exception{
        purchaseMapper.delete(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 详情
     */
    @Override
    public Tip detail(Integer id) throws Exception{
        ZeroPurchaseDTO zeroPurchaseDTO = purchaseMapper.detail(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),zeroPurchaseDTO);
    }

}
