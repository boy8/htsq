package com.drd.dt.back.service.Impl;

import com.drd.dt.back.dao.ISiftOrderLogBaseMapper;
import com.drd.dt.back.dto.SiftOrderLogDTO;
import com.drd.dt.back.service.ISiftOrderLogBaseService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional
@Service("siftOrderLogBaseServiceImpl")
public class SiftOrderLogBaseServiceImpl implements ISiftOrderLogBaseService {
    @Autowired
    private ISiftOrderLogBaseMapper siftOrderLogMapper;

    public Tip insert(SiftOrderLogDTO siftOrderLog) {
        int req = siftOrderLogMapper.insert(siftOrderLog);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.ADD_SUCCESS.getCode(), SuccessResultEnum.ADD_SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.ADD_ERROR.getCode(), BizExceptionEnum.ADD_ERROR.getMessage());
    }

    public Tip update(SiftOrderLogDTO siftOrderLog) {
        int req = siftOrderLogMapper.update(siftOrderLog);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.UPDATE_SUCCESS.getCode(), SuccessResultEnum.UPDATE_SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.UPDATE_ERROR.getCode(), BizExceptionEnum.UPDATE_ERROR.getMessage());
    }

    public Tip delete(Integer id) {
        siftOrderLogMapper.delete(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    public Tip queryAll(Integer id) {
        SiftOrderLogDTO siftOrderLog = siftOrderLogMapper.queryAll(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), siftOrderLog);
    }

    public Tip pageSelect(SiftOrderLogDTO siftOrderLog) {
        List<SiftOrderLogDTO> list = siftOrderLogMapper.pageSelect(siftOrderLog);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);
    }

}
