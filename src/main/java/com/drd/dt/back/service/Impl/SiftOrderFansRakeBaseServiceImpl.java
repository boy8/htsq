package com.drd.dt.back.service.Impl;

import com.drd.dt.back.dao.ISiftOrderFansRakeBaseMapper;
import com.drd.dt.back.dto.SiftOrderFansRakeDTO;
import com.drd.dt.back.service.ISiftOrderFansRakeBaseService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service("siftOrderFansRakeBaseService")
public class SiftOrderFansRakeBaseServiceImpl implements ISiftOrderFansRakeBaseService {
    @Autowired
    private ISiftOrderFansRakeBaseMapper siftOrderFansRakeMapper;

    public Tip insert(SiftOrderFansRakeDTO siftOrderFansRake){
        int req = siftOrderFansRakeMapper.insert(siftOrderFansRake);
        if( req > 0){
            return ResultUtil.result(SuccessResultEnum.ADD_SUCCESS.getCode(), SuccessResultEnum.ADD_SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.ADD_ERROR.getCode(), BizExceptionEnum.ADD_ERROR.getMessage());
    }
    public Tip update(SiftOrderFansRakeDTO siftOrderFansRake){
        int req = siftOrderFansRakeMapper.update(siftOrderFansRake);
        if( req > 0){
            return ResultUtil.result(SuccessResultEnum.UPDATE_SUCCESS.getCode(), SuccessResultEnum.UPDATE_SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.UPDATE_ERROR.getCode(), BizExceptionEnum.UPDATE_ERROR.getMessage());
    }
    public Tip delete(Integer id){
        siftOrderFansRakeMapper.delete(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }
    public Tip queryAll(Integer id){
        SiftOrderFansRakeDTO siftOrderFansRake = siftOrderFansRakeMapper.queryAll(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),siftOrderFansRake);
    }
    public Tip pageSelect(SiftOrderFansRakeDTO siftOrderFansRake){
        List<SiftOrderFansRakeDTO> list = siftOrderFansRakeMapper.pageSelect(siftOrderFansRake);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),list);
    }
}
