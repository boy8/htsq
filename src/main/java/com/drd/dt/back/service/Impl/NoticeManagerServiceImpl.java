package com.drd.dt.back.service.Impl;

import com.drd.dt.back.dao.JGPushMapper;
import com.drd.dt.back.entity.MessageActivity;
import com.drd.dt.back.service.JGPushService;
import com.drd.dt.common.Constant;
import com.drd.dt.util.ResultUtil;
import com.drd.dt.util.StringUtil;
import com.drd.dt.back.dao.NoticeManagerMapper;
import com.drd.dt.back.service.INoticeManageService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service("noticeManageService")
public class NoticeManagerServiceImpl implements INoticeManageService {

    @Autowired
    private NoticeManagerMapper noticeManagerMapper;
    @Autowired
    private JGPushMapper jgPushMapper;

    /**
     * 公告列表
     * @return
     */
    @Override
    public Tip list() throws Exception{
        List<MessageActivity> noticeList = noticeManagerMapper.getNoticeList();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),noticeList);
    }

    /**
     * 新增公告
     **/
    @Override
    public Tip add(MessageActivity activity) throws Exception{
        activity.setActivity_type(1);
        activity.setCreate_time(Constant.y_M_d_H_m_s.format(new Date()));
        jgPushMapper.insert(activity);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 删除公告
     **/
    @Override
    public Tip delete(Integer id) {
        jgPushMapper.deleteById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 公告详情
     **/
    @Override
    public Tip detail(Integer id) {
        MessageActivity detail = jgPushMapper.selectById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),detail);
    }

    /**
     * 修改公告
     **/
    @Override
    public Tip update(MessageActivity activity) {
        jgPushMapper.updateById(activity);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }
}
