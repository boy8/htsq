package com.drd.dt.back.service;

import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.modular.entity.Order;
import com.drd.dt.modular.entity.UserMoney;
import com.drd.dt.util.ResultUtil;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

public interface IAtWillUpdataService {

    public Tip getUserProfit(String user_id);

    public Tip updataUserProfit(UserMoney userMoney);

    public Tip getFSList(String user_id,String fs_user_id,Integer directRelation,Integer page,Integer count,String is_vip) throws Exception;

    public Tip getFsDetail(String user_id);

    public Tip updateFsDetail(Integer user_id,Double estimate_commission,Double estimate_bounty,
                              Double estimate_fans,Double owner_estimate,
                              Double accumulated,Double activity_accumulated,Double fans_estimate,
                              Double bounty_accumulated,Integer direct_fans,Integer indirect_fans);

    public Tip addOrder(Order order);

    public Tip getUserFomr(String user_id, String time);

    public Tip updataFomr( String id,String user_id,String time,Double estimate_commission,Double estimate_bounty,Double estimate_fans);


    public Tip addAndUpdataOrderGiftRake(String id,String user_id,Double user_rake_back);

    public Tip addAndUpdataOrder(String id,String user_id,Double user_rake_back_yugu);

    public Tip getOrderMoney(String user_id) ;

    public Tip getOrderGiftRakeMoney(String user_id);

}
