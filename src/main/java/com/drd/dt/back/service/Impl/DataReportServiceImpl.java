package com.drd.dt.back.service.Impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.common.Constant;
import com.drd.dt.util.ResultUtil;
import com.drd.dt.back.dao.DataReportMapper;
import com.drd.dt.back.service.IDataReportService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service("dataReportService")
public class DataReportServiceImpl implements IDataReportService {

    @Autowired
    private DataReportMapper dataReportMapper;

    /**
     * 数据报表信息统计-渠道汇总
     **/
    @Override
    public Tip channelList(Map<String, String> map) {
        List<Map> data = dataReportMapper.selectChannelDataList(map);
        Map total = getTotal(data);
        total.put("channel_id","渠道汇总");
        data.add(total);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), data);
    }

    /**
     * 数据报表信息统计-平台汇总
     **/
    @Override
    public Tip terraceList(Map<String, String> map) {
        List<Map> data = dataReportMapper.selectTerraceDataList(map);
        List<Map> buytypeInfo = dataReportMapper.selectByBuyType(map);
        Map total = getTotal(data);
        final Integer[] t_buytype = {0,0};
        data.forEach(t -> {
            String t_type = t.get("type").toString();
            String orderNum = t.get("orderNum").toString();
            buytypeInfo.forEach(p ->{
                String p_type = p.get("type").toString();
                String buytype = p.get("buytype").toString();
                String buytypeNum = p.get("buytypeNum").toString();
                if (t_type.equals(p_type)){
                    if (p_type.equals("京东")){
                        if (buytype.equals("0")){
                            t.put("buytype0", orderNum);
                            t_buytype[0] = t_buytype[0] + Integer.valueOf(orderNum);
                        }else {
                            t.put("buytype1", 0);
                        }
                    }else {
                        if (buytype.equals("0")){
                            t.put("buytype0", buytypeNum);
                            t_buytype[0] = t_buytype[0] + Integer.valueOf(buytypeNum);
                        }else {
                            t.put("buytype1", buytypeNum);
                            t_buytype[1] = t_buytype[1] + Integer.valueOf(buytypeNum);
                        }
                    }
                }
            });
        });
        total.put("type","平台汇总");
        total.put("buytype0",t_buytype[0]);
        total.put("buytype1",t_buytype[1]);
        data.add(total);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), data);
    }

    /**
     * 数据报表信息统计-订单收益汇总
     **/
    @Override
    public Tip incomeList(Map<String, String> map) {
        JSONArray arr = new JSONArray();
        List<Map> peopleInfo = dataReportMapper.peopleInfoList(map);//总注册人数
        List<Map> orderInfo = dataReportMapper.incomeList(map);//订单总佣金
        List<Map> buyInfo = dataReportMapper.buyInfoList(map);//订单购买人数
        Set<String> channel = new HashSet<>();
        peopleInfo.forEach(t -> channel.add(String.valueOf(t.get("channel_id"))));
        orderInfo.forEach(t -> channel.add(String.valueOf(t.get("channel_id"))));
        JSONObject total = new JSONObject();
        final int[] total_info = {0,0};
        final double[] total_rake = {0,0,0};
        channel.forEach(t -> {
            JSONObject result = new JSONObject();
            String channel_id = t;
            result.put("channel_id", t);
            peopleInfo.forEach(p -> {
                String peopleNum = String.valueOf(p.get("peopleNum"));
                String p_channel_id = String.valueOf(p.get("channel_id"));
                if (p_channel_id.equals(channel_id)) {
                    result.put("peopleNum", peopleNum);
                    total_info[0] = total_info[0] + Integer.valueOf(peopleNum);
                }
            });
            orderInfo.forEach(o -> {
                String order_channel_id = String.valueOf(o.get("channel_id"));
                String total_rake_back = String.valueOf(o.get("total_rake_back"));
                String total_actual_amount = String.valueOf(o.get("total_actual_amount"));
                if (order_channel_id.equals(channel_id)) {
                    result.put("total_rake_back", total_rake_back);
                    result.put("total_actual_amount", total_actual_amount);
                    total_rake[0] = total_rake[0] + Double.valueOf(total_rake_back);
                    total_rake[1] = total_rake[1] + Double.valueOf(total_actual_amount);
                }
            });
            buyInfo.forEach(b -> {
                String order_channel_id = String.valueOf(b.get("channel_id"));
                String buyNum = String.valueOf(b.get("buyNum"));
                if (order_channel_id.equals(channel_id)) {
                    result.put("buyNum", buyNum);
                    total_info[1] = total_info[1] + Integer.valueOf(buyNum);
                }
            });

            Double money = result.getDouble("total_rake_back");
            Integer buyNum = result.getInteger("buyNum");
            Double ave = 0.0;//人均佣金
            if (null != buyNum && buyNum > 0) {
                ave = money / buyNum;
            }
            result.put("ave", String.format("%.2f", ave));
            arr.add(result);
        });
        total.put("channel_id","汇总");
        total.put("peopleNum",total_info[0]);
        total.put("buyNum",total_info[1]);
        total.put("total_rake_back",String.format("%.2f", total_rake[0]));
        total.put("total_actual_amount",String.format("%.2f", total_rake[1]));
        double ave = 0.0;
        if (total_info[1] > 0){
            ave = total_rake[0] / total_info[1];
        }
        total.put("ave",String.format("%.2f", ave));
        arr.add(total);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), arr);
    }

    /**
     * 数据报表信息统计-自营订单收益汇总
     **/
    @Override
    public Tip ownOrderList(Map<String, String> map) {
        List<Map> data = dataReportMapper.selectOwnOrderList(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), data);
    }

    /**
     * 数据报表信息统计-活跃统计
     **/
    @Override
    public Tip activityList(Map<String, String> map) throws Exception{
        String startTime = map.get("startTime");
        String endTime = map.get("endTime");
        String pre_date = StringUtil.yesterdayDay(Constant.y_M_d);//默认为昨天时间
        if (StringUtils.isEmpty(startTime)){
            startTime = pre_date + " 00:00:00";
            map.put("startTime",startTime);
        }
        if (StringUtils.isEmpty(endTime)){
            endTime = pre_date + " 23:59:59";
            map.put("endTime",endTime);
        }
        List<Map> data = dataReportMapper.activityList(map);
        Integer total = Integer.valueOf(data.get(0).get("total").toString());
        Integer activity = Integer.valueOf(data.get(0).get("activity").toString());
        Double rate = Double.valueOf(activity) / Double.valueOf(total) * 100;
        data.get(0).put("rate",String.format("%.2f",rate) + "%");
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), data);
    }

    /**
     * 数据报表信息统计-订单汇总
     **/
    @Override
    public Tip rateList(Map<String, String> map) {
        List<Map> info = dataReportMapper.orderRate(map);
        JSONArray arr = new JSONArray();
        JSONObject result = new JSONObject();
        info.forEach(t -> {
            Integer activityNum = Integer.valueOf(t.get("activityNum").toString());
            Integer buyNum = Integer.valueOf(t.get("buyNum").toString());
            Integer orderNum = Integer.valueOf(t.get("orderNum").toString());
            Double rake_back = Double.valueOf(t.get("rake_back").toString());
            Double user_rake_back = Double.valueOf(t.get("user_rake_back").toString());
            //下单率（下单人数除当天活跃人数），订单率（订单数除以订单人数），订单arpu（订单金额除以订单人数）
            Double buyRate = 100.0 * buyNum / activityNum;//下单率
            if (activityNum == 0){
                buyRate = 0.0;
            }
            Double orderRate = 1.0 * orderNum / buyNum;//订单率
            if (buyNum == 0){
                orderRate = 0.0;
            }
            Double arpuRate = rake_back / buyNum;//订单arpu
            if (buyNum == 0){
                arpuRate = 0.0;
            }
            result.put("activityNum",activityNum);
            result.put("buyNum",buyNum);
            result.put("orderNum",orderNum);
            result.put("rake_back",rake_back);
            result.put("user_rake_back",user_rake_back);
            result.put("buyRate",String.format("%.2f",buyRate) + "%");
            result.put("orderRate",String.format("%.2f",orderRate));
            result.put("arpuRate",String.format("%.2f",arpuRate));
        });
        arr.add(result);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),arr);
    }

    /**
     * 计算汇总
     * @return
     */
    private static Map getTotal(List<Map> data){
        Map total = new HashMap<>();
        Integer total_orderNum = 0;
        Double total_rake_back = 0.0;
        Double total_user_rake_back = 0.0;
        for (Map t:data){
            Integer orderNum = Integer.valueOf(t.get("orderNum").toString());
            Double rake_back = Double.valueOf(t.get("rake_back").toString());
            Double user_rake_back = Double.valueOf(t.get("user_rake_back").toString());
            total_orderNum = total_orderNum + orderNum;
            total_rake_back = rake_back + total_rake_back;
            total_user_rake_back = user_rake_back + total_user_rake_back;
        }
        total.put("orderNum",total_orderNum);
        total.put("rake_back",String.format("%.2f",total_rake_back));
        total.put("user_rake_back",String.format("%.2f",total_user_rake_back));
        return total;
    }
}
