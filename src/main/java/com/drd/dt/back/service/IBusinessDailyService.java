package com.drd.dt.back.service;

import com.drd.dt.back.entity.BusinessDaily;
import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface IBusinessDailyService {

    /**
     * 获取每日爆款列表
     **/
    Tip getList(Map<String, String> param) throws Exception;

    /**
     * 删除每日爆款
     **/
    Tip delete(Integer id) throws Exception;

    /**
     * 修改每日爆款
     **/
    Tip update(BusinessDaily businessDaily) throws Exception;

    /**
     * 获取每日爆款详情
     **/
    Tip advDetailInfo(Integer id) throws Exception;
}
