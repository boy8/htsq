package com.drd.dt.back.service;

import com.drd.dt.back.dto.SiftOrderFansRakeDTO;
import com.drd.dt.common.tips.Tip;

/**
 * SiftOrderFansRake 核桃精选粉丝订单表 接口定义
 */
public interface ISiftOrderFansRakeBaseService {

    Tip insert(SiftOrderFansRakeDTO siftOrderFansRakeDTO);

    Tip update(SiftOrderFansRakeDTO siftOrderFansRakeDTO);

    Tip delete(Integer id);

    Tip queryAll(Integer id);

    Tip pageSelect(SiftOrderFansRakeDTO siftOrderFansRakeDTO);
}
