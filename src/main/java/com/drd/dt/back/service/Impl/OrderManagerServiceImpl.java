package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.common.Constant;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.modular.dao.OrderMapper;
import com.drd.dt.modular.dto.ZeroPurchaseDTO;
import com.drd.dt.modular.entity.Order;
import com.drd.dt.modular.service.impl.UserMoneyServiceImpl;
import com.drd.dt.util.FilterGoodsUtil;
import com.drd.dt.util.ImportUtil;
import com.drd.dt.util.ResultUtil;
import com.drd.dt.back.dao.OrderManagerMapper;
import com.drd.dt.back.service.IOrderManageService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service("orderManageService")
public class OrderManagerServiceImpl implements IOrderManageService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OrderManagerMapper orderManagerMapper;
    @Autowired
    private FilterGoodsUtil goodsUtil;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UserMoneyServiceImpl testMoneyService;

    @Override
    public Tip list(Map<Object, Object> map) throws Exception{
        Integer offset = Integer.valueOf(map.get("offset").toString());
        Integer pageSize = Constant.PAGE_SIZE_20;
        Integer pageNum = offset / pageSize + 1;
        Page page = new Page(pageNum, pageSize);
        Object ser = map.get("serach_type");
        Integer serach_type = 0;
        if (null != ser){
            serach_type = Integer.valueOf(ser.toString());
        }
        List<Map> list;
        if (serach_type == 1){//小美贝商品
            list = orderManagerMapper.getXMBOrderList(page);
        }else {
            list = orderManagerMapper.getOrderList(page,map);
        }
        page.setRecords(list);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),page);
    }

    @Override
    public Tip taobaoImport(MultipartFile file) throws Exception{
        if (null == file){
            return ResultUtil.result(BizExceptionEnum.FILE_CONTENT_ERROR.getCode(), BizExceptionEnum.FILE_CONTENT_ERROR.getMessage());
        }
        String name = file.getOriginalFilename();
        if (StringUtils.isEmpty(name)){
            return ResultUtil.result(BizExceptionEnum.FILE_TITLE_ERROR.getCode(), BizExceptionEnum.FILE_TITLE_ERROR.getMessage());
        }
        if (!name.endsWith(".xls")){
            return ResultUtil.result(BizExceptionEnum.FILE_TYPE_ERROR.getCode(), BizExceptionEnum.FILE_TYPE_ERROR.getMessage());
        }

        Workbook wb = ImportUtil.readWorkBook(file);
        List<List<String>> lists = ImportUtil.read(wb);
        //获取属于app的订单
        List<List<String>> dataLists = lists.stream().filter(t -> (t.size() > 0 && (t.get(36).equals("券巴士分享2") || t.get(36).equals("券巴士自购2")))).collect(Collectors.toList());
        for (List<String> data:dataLists){
            String orderId = data.get(14);//订单子id
            String clear_time = data.get(3);//订单结算时间
            String order_status = data.get(15);//订单状态
            String goods_id = data.get(5);//商品id
            if (StringUtils.isEmpty(order_status) || (!order_status.equals("已结算") && !order_status.equals("已失效"))){
                continue;
            }
            //获取订单id相同的数据
            Order order = orderMapper.getRepeatOrder(orderId);//先使用子订单号查询订单
            String my_clear_time;//我们平台订单的结算时间
            Integer status;//我们平台订单的状态
            String rake_back;//我们平台订单的预估佣金
            Double zeroMoney = 0.0;// 零元购效果预估
            //获取所有零元购商品
            List<ZeroPurchaseDTO> zlists = orderMapper.getZeroPurchases();
            //通过商品id匹配是否是零元购商品
            List<ZeroPurchaseDTO> collect = zlists.stream().filter(t -> goods_id.equals(t.getGoods_id())).collect(Collectors.toList());
            if (collect.size() > 0) {//有零元购商品
                String anti = collect.get(0).getAnti_growth();
                if (StringUtils.isEmpty(anti)) {
                    anti = "0";
                }
                Double anti_growth = Double.valueOf(anti);//购买返价格
                zeroMoney = zeroMoney + anti_growth;
            }
            if (null != order){
                my_clear_time = order.getClear_time();//将我们平台订单数据赋值
                status = order.getStatus();
                rake_back = order.getRake_back();
            }else {
                continue;
            }
            if (order_status.equals("已结算") && !StringUtils.isEmpty(orderId) && !StringUtils.isEmpty(clear_time) && StringUtils.isEmpty(my_clear_time)
                    && status == 2 && !StringUtils.isEmpty(rake_back)) {//订单结算时 订单号和结算时间不为空
                goodsUtil.rakeMoneyNew(orderId, Double.valueOf(rake_back), String.valueOf(goods_id), clear_time, zeroMoney);//返佣金
            }else if (order_status.equals("已失效")) {//  订单失效
                if(order.getStatus() != 4){
                    String user_rake_back_yugu = order.getUser_rake_back_yugu();
                    orderMapper.updateStatus(orderId);
                    order.setStatus(4);
                    if (!StringUtils.isEmpty(user_rake_back_yugu) && Double.valueOf(user_rake_back_yugu) > 0){
                        testMoneyService.updateOrderFansRake(order);
                    }
                }
            }
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip jdImport(MultipartFile file) throws Exception{
        if (null == file){
            return ResultUtil.result(BizExceptionEnum.FILE_CONTENT_ERROR.getCode(), BizExceptionEnum.FILE_CONTENT_ERROR.getMessage());
        }
        String name = file.getOriginalFilename();
        if (StringUtils.isEmpty(name)){
            return ResultUtil.result(BizExceptionEnum.FILE_TITLE_ERROR.getCode(), BizExceptionEnum.FILE_TITLE_ERROR.getMessage());
        }
        if (!name.endsWith(".csv")){
            return ResultUtil.result(BizExceptionEnum.FILE_TYPE_ERROR.getCode(), BizExceptionEnum.FILE_TYPE_ERROR.getMessage());
        }

        List<String[]> lists = new ArrayList<>();
        BufferedReader reader=new BufferedReader(new InputStreamReader(file.getInputStream(),"GBK"));
        reader.readLine();//第一行信息，为标题信息，不用,如果需要，注释掉
        String line = null;
        while((line = reader.readLine()) != null) {
            String[] item = line.split(",");//CSV格式文件为逗号分隔符文件，这里根据逗号切分
            lists.add(item);
        }

        for (String[] data:lists){
            if (data.length < 2){
                break;
            }
            String orderId = data[0].replaceAll("\\t","");//订单id
            String clear_time = data[4].replaceAll("\\t","");//订单结算时间
            String order_status = data[2].replaceAll("\\t","");//订单状态
            String goods_id = data[5].replaceAll("\\t","");//商品id
            if (StringUtils.isEmpty(order_status) || (!order_status.equals("已完成") && !order_status.contains("无效"))){
                continue;
            }
            //获取订单id相同的数据
            Order order = orderMapper.getRepeatOrder(orderId);//使用订单号查询订单
            String my_clear_time;//我们平台订单的结算时间
            Integer status;//我们平台订单的状态
            String rake_back;//我们平台订单的预估佣金
            if (null != order){
                my_clear_time = order.getClear_time();//将我们平台订单数据赋值
                status = order.getStatus();
                rake_back = order.getRake_back();
            }else {
                continue;
            }

            Map map = new HashMap();
            map.put("order_id", orderId);
            if (order_status.equals("已完成") && !StringUtils.isEmpty(orderId) && !StringUtils.isEmpty(clear_time) && StringUtils.isEmpty(my_clear_time)
                    && status == 2 && !StringUtils.isEmpty(rake_back)) {//已经完成 （提前支付）
                map.put("clear_time", clear_time);
                map.put("rake_back", rake_back);
                logger.info("【黑桃省钱开始返佣金...订单号" + orderId + "的佣金为【" + rake_back + "】元】");
                    goodsUtil.rakeMoneyNew(orderId, Double.valueOf(rake_back), goods_id, clear_time, 0.0);//返佣金
            } else if (order_status.contains("无效") && status != 4) {//无效订单
                map.put("status", "4");
                //修改失效订单返佣
                orderMapper.updateOrder(map);
                if (order.getStatus() != 4) {//如果订单是第一次无效
                    order.setStatus(4);
                    String rakeBake = order.getRake_back();
                    Integer buytype = order.getBuytype();
                    String user_rake_back_yugu = order.getUser_rake_back_yugu();
                    if (StringUtils.isEmpty(rakeBake)){
                        order.setRake_back("0");
                    }
                    if (null == buytype){
                        order.setBuytype(0);
                    }
                    if (StringUtils.isEmpty(user_rake_back_yugu)){
                        order.setUser_rake_back_yugu("0");
                    }else {
                        if (Double.valueOf(user_rake_back_yugu) > 0){
                            testMoneyService.updateOrderFansRake(order);
                        }
                    }
                }
            }
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip pddImport(MultipartFile file) throws Exception {
        if (null == file){
            return ResultUtil.result(BizExceptionEnum.FILE_CONTENT_ERROR.getCode(), BizExceptionEnum.FILE_CONTENT_ERROR.getMessage());
        }
        String name = file.getOriginalFilename();
        if (StringUtils.isEmpty(name)){
            return ResultUtil.result(BizExceptionEnum.FILE_TITLE_ERROR.getCode(), BizExceptionEnum.FILE_TITLE_ERROR.getMessage());
        }
        if (!name.endsWith(".csv")){
            return ResultUtil.result(BizExceptionEnum.FILE_TYPE_ERROR.getCode(), BizExceptionEnum.FILE_TYPE_ERROR.getMessage());
        }

        List<String[]> lists = new ArrayList<>();
        BufferedReader reader=new BufferedReader(new InputStreamReader(file.getInputStream(),"UTF-8"));
        reader.readLine();//第一行信息，为标题信息，不用,如果需要，注释掉
        String line = null;
        while((line = reader.readLine()) != null) {
            String[] item = line.split(",");//CSV格式文件为逗号分隔符文件，这里根据逗号切分
            lists.add(item);
        }

        String clear_time = Constant.y_M_d_H_m_s.format(new Date());//订单结算时间
        for (String[] data:lists){
            if (data.length < 2){
                break;
            }
            String orderId = data[1].replaceAll("\\t","");//订单id
//            String clear_time = data[17].replaceAll("\\t","");//订单结算时间
            String order_status = data[5].replaceAll("\\t","");//订单状态
            String goods_id = data[4].replaceAll("\\t","");//商品id
             if (StringUtils.isEmpty(order_status) || (!order_status.equals("已结算") && !order_status.contains("审核失败"))){
                continue;
            }
            //获取订单id相同的数据
            Order order = orderMapper.getRepeatOrder(orderId);//使用订单号查询订单
            String my_clear_time;//我们平台订单的结算时间
            Integer status;//我们平台订单的状态
            String rake_back;//我们平台订单的预估佣金
            if (null != order){
                my_clear_time = order.getClear_time();//将我们平台订单数据赋值
                status = order.getStatus();
                rake_back = order.getRake_back();
            }else {
                continue;
            }

            Map map = new HashMap();
            map.put("order_id", orderId);
            if (order_status.equals("已结算") && !StringUtils.isEmpty(orderId) && !StringUtils.isEmpty(clear_time) && StringUtils.isEmpty(my_clear_time)
                    && status == 2 && !StringUtils.isEmpty(rake_back)) {//已经完成 （提前支付）
                logger.info("【黑桃省钱开始更新拼多多订单号为 【" + orderId + "】的订单佣金...】");
                goodsUtil.rakeMoneyNew(orderId, Double.valueOf(rake_back), goods_id, clear_time, 0.0);//返佣金
            } else if (order_status.contains("审核失败") && status != 4) {//无效订单
                logger.info("【黑桃省钱开始更新拼多多订单号为 【" + orderId + "】的订单状态...】");
                map.put("status", 4);
                orderMapper.updateOrderStatus(map);//更新order表
                order.setStatus(status);
                String user_rake_back_yugu = order.getUser_rake_back_yugu();
                if (Double.valueOf(user_rake_back_yugu) > 0){
                    testMoneyService.updateOrderFansRake(order);
                }
            }
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip wphImport(MultipartFile file) throws Exception {
        if (null == file){
            return ResultUtil.result(BizExceptionEnum.FILE_CONTENT_ERROR.getCode(), BizExceptionEnum.FILE_CONTENT_ERROR.getMessage());
        }
        String name = file.getOriginalFilename();
        if (StringUtils.isEmpty(name)){
            return ResultUtil.result(BizExceptionEnum.FILE_TITLE_ERROR.getCode(), BizExceptionEnum.FILE_TITLE_ERROR.getMessage());
        }
        if (!name.endsWith(".csv")){
            return ResultUtil.result(BizExceptionEnum.FILE_TYPE_ERROR.getCode(), BizExceptionEnum.FILE_TYPE_ERROR.getMessage());
        }

        List<String[]> lists = new ArrayList<>();
        BufferedReader reader=new BufferedReader(new InputStreamReader(file.getInputStream(),"GBK"));
        reader.readLine();//第一行信息，为标题信息，不用,如果需要，注释掉
        String line = null;
        while((line = reader.readLine()) != null) {
            String[] item = line.split(",");//CSV格式文件为逗号分隔符文件，这里根据逗号切分
            lists.add(item);
        }

        for (String[] data:lists){
            if (data.length < 2){
                break;
            }
            String orderId = data[7].replaceAll("[\"\\t]","");//订单id
            String clear_time = data[6].replaceAll("[\"\\t]","");//订单结算时间
            String order_status = data[12].replaceAll("[\"\\t]","");//订单状态
            String goods_id = data[18].replaceAll("[G\"\\t]","");//商品id
            if (StringUtils.isEmpty(order_status) || (!order_status.equals("已结算") && !order_status.equals("已失效"))){
                continue;
            }
            //获取订单id相同的数据
            Order order = orderMapper.getRepeatOrder(orderId);//使用订单号查询订单
            String my_clear_time;//我们平台订单的结算时间
            Integer status;//我们平台订单的状态
            String rake_back;//我们平台订单的预估佣金
            if (null != order){
                my_clear_time = order.getClear_time();//将我们平台订单数据赋值
                status = order.getStatus();
                rake_back = order.getRake_back();
            }else {
                continue;
            }

            Map map = new HashMap();
            map.put("order_id", orderId);
            if (order_status.equals("已结算") && !StringUtils.isEmpty(orderId) && !StringUtils.isEmpty(clear_time) && StringUtils.isEmpty(my_clear_time)
                    && status == 2 && !StringUtils.isEmpty(rake_back)) {//已经完成 （提前支付）
                logger.info("【黑桃省钱开始更新唯品会订单号为 【" + orderId + "】的订单佣金...】");
                goodsUtil.rakeMoneyNew(orderId, Double.valueOf(rake_back), goods_id, clear_time, 0.0);//返佣金
            } else if (order_status.contains("已失效") && status != 4) {//无效订单
                logger.info("【黑桃省钱开始更新唯品会订单号为 【" + orderId + "】的订单状态...】");
                map.put("status", 4);
                orderMapper.updateOrderStatus(map);//更新order表
                order.setStatus(status);
                String rakeBake = order.getRake_back();
                String user_rake_back_yugu = order.getUser_rake_back_yugu();
                if (StringUtils.isEmpty(rakeBake)){
                    order.setRake_back("0");
                }
                if (StringUtils.isEmpty(user_rake_back_yugu)){
                    order.setUser_rake_back_yugu("0");
                }else {
                    if (Double.valueOf(user_rake_back_yugu) > 0){
                        testMoneyService.updateOrderFansRake(order);
                    }
                }
            }
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }
}
