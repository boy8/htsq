package com.drd.dt.back.service;

import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface IChannelInfoService {
    /**
     * 渠道信息统计
     * @param map
     * @return
     */
    Tip channelInfo(Map<Object, Object> map) throws Exception;
}
