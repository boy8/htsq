package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.drd.dt.back.dao.SpecialCodeManageMapper;
import com.drd.dt.back.entity.SpecialCode;
import com.drd.dt.back.service.IzuimeiManageService;
import com.drd.dt.back.service.SpecialCodeManageService;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.util.ResultUtil;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service("specialCodeManageService")
public class SpecialCodeManageServiceImpl extends ServiceImpl<SpecialCodeManageMapper, SpecialCode> implements SpecialCodeManageService {

    @Autowired
    private SpecialCodeManageMapper specialCodeManageMapper;


    /**
     * 列表
     */
    @Override
    public Tip list(Map<String, String> map) throws Exception{
        Integer offset = Integer.valueOf(map.get("offset"));
        Integer pageSize = Constant.PAGE_SIZE_20;
        Integer pageNum = offset / pageSize + 1;
        Page<SpecialCode> page = new Page<>(pageNum, pageSize);
        String firend_code = map.get("firend_code");
        EntityWrapper<SpecialCode> wrapper = new EntityWrapper<>();
        if (!StringUtils.isEmpty(firend_code)){
            wrapper.like("firend_code",firend_code);
        }
        wrapper.orderBy("create_time",false);
        List<SpecialCode> list = specialCodeManageMapper.selectPage(page,wrapper);
        page.setRecords(list);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),page);
    }

    @Override
    public Tip insert(Map<String, String> map) throws Exception {
        SpecialCode specialCode = new SpecialCode();
        String firend_code = map.get("firend_code");
        specialCode.setFirend_code(firend_code.toUpperCase());
        specialCode.setCreate_time(Constant.y_M_d_H_m_s.format(new Date()));
        specialCodeManageMapper.insert(specialCode);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip update(Map<String, String> map) throws Exception {
        String firend_code = map.get("firend_code");
        Integer id = Integer.valueOf(map.get("id"));
        SpecialCode specialCode = specialCodeManageMapper.selectById(id);
        specialCode.setFirend_code(firend_code.toUpperCase());
        specialCodeManageMapper.updateById(specialCode);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip delete(Integer id) throws Exception {
        specialCodeManageMapper.deleteById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip detail(Integer id) throws Exception {
        SpecialCode specialCode = specialCodeManageMapper.selectById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),specialCode);
    }


}
