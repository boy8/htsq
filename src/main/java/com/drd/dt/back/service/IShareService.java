package com.drd.dt.back.service;


import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface IShareService {


    Tip list(Map<Object, Object> map);

    Tip shareList(Map<String, String> map);

    Tip insert(Map<String, String> map);

    Tip update(Map<String, String> map);

    Tip delete(Integer id);

    Tip detail(Integer id);
}
