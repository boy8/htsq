package com.drd.dt.back.service.Impl;

import com.drd.dt.util.ResultUtil;
import com.drd.dt.back.dao.NormalManageMapper;
import com.drd.dt.modular.entity.InitConfig;
import com.drd.dt.back.service.INormalManageService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service
public class NormalManagerServiceImpl implements INormalManageService {

    @Autowired
    private NormalManageMapper normalManageMapper;


    /**
     * 获取基础设置信息
     **/
    @Override
    public Tip baseInfo() throws Exception{
        Map map = normalManageMapper.getBaseInfo();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),map);
    }

    /**
     * 修改基础设置信息
     **/
    @Override
    public Tip updateBaseInfo(Map<Object, Object> map) throws Exception{
        normalManageMapper.updateBaseInfo(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),map);
    }

    /**
     * 获取参数设置信息
     **/
    @Override
    public Tip parameterInfo() throws Exception{
        Map map = normalManageMapper.getParameterInfo();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),map);
    }

    /**
     * 修改参数设置信息
     **/
    @Override
    public Tip updateParameterInfo(Map<Object, Object> map) throws Exception{
        normalManageMapper.updateParameterInfo(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 获取初始化设置信息
     **/
    @Override
    public Tip initrInfo() throws Exception{
        InitConfig config = normalManageMapper.selectInitConfigInfo();//获取初始化配置
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),config);
    }

    /**
     * 更新初始化设置信息
     **/
    @Override
    public Tip updateInit(InitConfig config) {
        normalManageMapper.updateInit(config);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }
}
