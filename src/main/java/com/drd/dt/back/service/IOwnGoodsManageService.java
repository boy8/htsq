package com.drd.dt.back.service;

import com.drd.dt.back.dto.OwnGoodsDTO;
import com.drd.dt.back.dto.OwnGoodsInsertDTO;
import com.drd.dt.back.dto.OwnGoodsOrderDTO;
import com.drd.dt.back.dto.OwnGoodsSizeDTO;
import com.drd.dt.common.tips.Tip;

import javax.servlet.http.HttpServletResponse;

public interface IOwnGoodsManageService {
    /**
     * 新增  商品
     * */
    Tip addOwnGoods(OwnGoodsInsertDTO ownGoods) throws Exception;

    /**
     * 修改  商品
     * */
    Tip update(OwnGoodsSizeDTO ownGoodsSizeDTO) throws Exception;

    /**
     * 查询商品
     * */
    Tip selectAll(String title,String state) throws Exception;

    /**
     * 查询单个
     * */
    Tip detail(Integer id) throws Exception;

    /**
     * 删除数据
     */
    Tip delete(Integer id) throws Exception;

    /**
     * 自营订单列表
     */
    Tip orderList(OwnGoodsOrderDTO orderDTO) throws Exception;

    /**
     * 自营订单详情
     */
    Tip orderDetail(Integer id) throws Exception;

    /**
     * 修改自营订单
     */
    Tip orderUpdate(OwnGoodsOrderDTO orderDTO);

    /**
     * 导出自营订单数据
     **/
    void excelExport(OwnGoodsOrderDTO orderDTO, HttpServletResponse response);
}
