package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.drd.dt.back.dao.BusinessManageMapper;
import com.drd.dt.back.dto.BusinessAcademyDTO;
import com.drd.dt.back.service.IBusinessManageService;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
@Transactional
public class BusinessManageServiceImpl extends ServiceImpl<BusinessManageMapper, BusinessAcademyDTO> implements IBusinessManageService {

    @Autowired
    private BusinessManageMapper businessManageMapper;

    /**
     * 列表
     */
    @Override
    public Tip list(BusinessAcademyDTO businessAcademyDTO) throws Exception{
        List<BusinessAcademyDTO> result = businessManageMapper.list(businessAcademyDTO);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 添加数据
     */
    @Override
    public Tip insertBusiness(BusinessAcademyDTO businessAcademyDTO) throws Exception{
        businessAcademyDTO.setCreate_time(Constant.y_M_d_H_m_s.format(new Date()));
        Random random = new Random();
        int transpond_num = random.nextInt(10000) + 5000;
        int ran = random.nextInt(1000) + 5000;
        int r = random.nextInt(500) + 3000;
        businessAcademyDTO.setTranspond_num(transpond_num);
        businessAcademyDTO.setShow_num(transpond_num + ran);
        businessAcademyDTO.setLike_num(transpond_num + r);
        businessManageMapper.insert(businessAcademyDTO);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 删除
     **/
    @Override
    public Tip delete(Integer id) throws Exception{
        businessManageMapper.deleteById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 更新
     **/
    @Override
    public Tip update(BusinessAcademyDTO businessAcademyDTO) throws Exception{
        businessManageMapper.updateById(businessAcademyDTO);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 详情
     **/
    @Override
    public Tip detail(Integer id) throws Exception{
        BusinessAcademyDTO businessAcademyDTO = businessManageMapper.selectById(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), businessAcademyDTO);
    }

}
