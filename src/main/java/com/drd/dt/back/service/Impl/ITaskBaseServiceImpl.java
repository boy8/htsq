package com.drd.dt.back.service.Impl;

import com.drd.dt.back.dao.TaskBaseMapper;
import com.drd.dt.back.service.ITaskBaseService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.modular.entity.TaskCoin;
import com.drd.dt.modular.entity.TaskInfo;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("taskBaseServiceImpl")
public class ITaskBaseServiceImpl implements ITaskBaseService {

    @Autowired
    private TaskBaseMapper taskBaseMapper;

    @Override
    public Tip selectCoin(int user_id) {
        int coin = taskBaseMapper.selectCoin(user_id);//任务中心金币
        TaskCoin task = taskBaseMapper.userFans(user_id, 3);//查询
        TaskCoin task2 = taskBaseMapper.userFans(user_id, 1);//查询
        Map map = new HashMap();
        map.put("gold_coin", coin);
        map.put("coin_num", null != task ? task.getCoin_num() : 0);
        map.put("my_coin_num", null != task2 ? task2.getCoin_num() : 0);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), map);
    }

    @Override
    public Tip updateMyComeGoOutGold(int user_id, int coin_num, int gold_coin, int my_coin_num) {
        //查询 今日 是否有金币记录取第一条
        TaskCoin task1 = taskBaseMapper.userFans(user_id, 1);
        TaskCoin task3 = taskBaseMapper.userFans(user_id, 3);
        TaskCoin task = new TaskCoin();
        task.setUser_id(user_id);
        task.setOrigin_id(user_id);
        task.setTask_id(-1);
        task.setRemark("测试数据 测试功能产生");

        int req1 = -1;
        int req3 = -1;

        //今日金币
        if (null != task1) {
            //修改
            req1 = taskBaseMapper.updateFansCoin(task1.getId(), my_coin_num);
        } else {
            task.setCoin_num(my_coin_num);
            task.setAction_type(1);
            req1 = taskBaseMapper.addFansCoin(task);
        }

        //粉丝贡献
        if (null != task3) {
            //修改
            task.setCoin_num(coin_num);
            req3 = taskBaseMapper.updateFansCoin(task3.getId(), coin_num);
        } else {
            //新增
            task.setAction_type(3);
            req3 = taskBaseMapper.addFansCoin(task);
        }
        //修改我的金币
        int userReq = taskBaseMapper.updateUserCoin(user_id, gold_coin);
        if (userReq < 1 || req1 < 1 || req3 < 1) {
            throw new BussinessException(BizExceptionEnum.UPDATE_ERROR);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip updateMyTask() {
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip selectTaskList(int user_id) {
        List<Map> list = taskBaseMapper.selectTaskList(user_id);
        if (null == list) {
            list = new ArrayList<>();
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);
    }

    @Override
    public Tip updateTaskS(String str, int user_id) {
        String[] taskList = str.split("\\|");
        for (int i = 0; i < taskList.length; i++) {
            String[] da = taskList[i].split("/");
            addAndUpdateTask(user_id, Integer.parseInt(da[0]), Integer.parseInt(da[1]), Integer.parseInt(da[2]), Integer.parseInt(da[3]));
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /***
     *  循环修改
     */
    private void addAndUpdateTask(int user_id, int task_id, int type, int times, int times_da) {
        //'type 1基础任务  2每日任务',
        if (type == 1) {
            if (taskBaseMapper.allUserTaskBase(user_id, task_id) > 0) {
                taskBaseMapper.updateTaskBase(user_id, task_id, times_da, (times_da == times ? 2 : 1));
            } else {
                taskBaseMapper.addTaskBase(user_id, task_id, times_da, (times_da == times ? 2 : 1));
            }
        } else if (type == 2) {
            if (taskBaseMapper.allUserTaskDaily(user_id, task_id) > 0) {
                taskBaseMapper.updateTaskDaily(user_id, task_id, times_da, (times_da == times ? 2 : 1));
            } else {
                taskBaseMapper.addTaskDaily(user_id, task_id, times_da, (times_da == times ? 2 : 1));
            }
        }
    }

}
