package com.drd.dt.back.service;

import com.drd.dt.back.dto.BusinessAcademyDTO;
import com.drd.dt.common.tips.Tip;

public interface IBusinessManageService {

    /**
     * 列表
     */
    Tip list(BusinessAcademyDTO businessAcademyDTO) throws Exception;

    /**
     * 添加数据
     */
    Tip insertBusiness(BusinessAcademyDTO businessAcademyDTO) throws Exception;

    /**
     * 删除数据
     */
    Tip delete(Integer id) throws Exception;

    /**
     * 修改数据
     */
    Tip update(BusinessAcademyDTO businessAcademyDTO) throws Exception;

    /**
     * 详情
     **/
    Tip detail(Integer id) throws Exception;
}
