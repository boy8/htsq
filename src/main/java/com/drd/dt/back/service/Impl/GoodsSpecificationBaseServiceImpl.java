package com.drd.dt.back.service.Impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dao.GoodsSpecificationBaseMapper;
import com.drd.dt.back.dto.GoodsSpecificationDTO;
import com.drd.dt.back.entity.GoodsSpecification;
import com.drd.dt.back.service.IGoodsSpecificationBaseService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional
@Service("GoodsSpecificationBaseService")
public class GoodsSpecificationBaseServiceImpl implements IGoodsSpecificationBaseService {

    @Autowired
    private GoodsSpecificationBaseMapper goodsSpecificationBaseMapper;

    @Override
    public Tip addGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification) {
        int req = goodsSpecificationBaseMapper.addGoodsSpecificationBase(goodsSpecification);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        } else {
            return ResultUtil.result(BizExceptionEnum.ADD_ERROR.getCode(), BizExceptionEnum.ADD_ERROR.getMessage());
        }
    }

    @Override
    public Tip updateGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification) {
        int req = goodsSpecificationBaseMapper.updateGoodsSpecificationBase(goodsSpecification);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        } else {
            return ResultUtil.result(BizExceptionEnum.UPDATE_ERROR.getCode(), BizExceptionEnum.UPDATE_ERROR.getMessage());
        }
    }

    @Override
    public Tip finaAll(Integer id) {
        GoodsSpecificationDTO goodsSpecification = goodsSpecificationBaseMapper.finaAll(id);
        if (null != goodsSpecification) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),goodsSpecification);
        } else {
            return ResultUtil.result(BizExceptionEnum.SELECT_ERROR.getCode(), BizExceptionEnum.SELECT_ERROR.getMessage());
        }
    }

    @Override
    public Tip delete(Integer id) {
        goodsSpecificationBaseMapper.delete(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip selectGoodsSpecificationDetail(Integer id) {
        List<GoodsSpecificationDTO> goodsSpecifications = goodsSpecificationBaseMapper.selectGoodsSpecificationDetail(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), goodsSpecifications);
    }


}
