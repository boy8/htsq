package com.drd.dt.back.service;

import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface ILoginService {
    /**
     * 登录
     * @param map
     * @return
     */
    Tip login(Map<Object, Object> map) throws Exception;
}
