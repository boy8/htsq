package com.drd.dt.back.service.Impl;

import com.drd.dt.common.Constant;
import com.drd.dt.util.ResultUtil;
import com.drd.dt.util.StringUtil;
import com.drd.dt.back.dao.HelpCenterMapper;
import com.drd.dt.back.service.IHelpCenterService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service("helpCenterService")
public class HelpCenterServiceImpl implements IHelpCenterService {

    @Autowired
    private HelpCenterMapper helpCenterMapper;

    /**
     * 添加问题分类
     **/
    @Override
    public Tip addProblemType(Map<String, String> map) throws Exception{
        String name = map.get("name").toString();
        String sequence = map.get("sequence").toString();
        String url = map.get("url").toString();
        if ( StringUtils.isEmpty(name)|| StringUtils.isEmpty(url)|| StringUtils.isEmpty(sequence)){
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        map.put("created_time", Constant.y_M_d_H_m_s.format(new Date()));
        helpCenterMapper.addProblemType(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 问题管理列表
     * @return
     */
    @Override
    public Tip problemManageList() throws Exception{
        List<Map> list = helpCenterMapper.getProblemManageList();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),list);
    }

    /**
     * 新增问题
     **/
    @Override
    public Tip addProblemManage(Map<String, String> map) throws Exception{
        String type = map.get("type").toString();
        String sequence = map.get("sequence").toString();
        String title = map.get("title").toString();
        String content = map.get("content").toString();
        if ( StringUtils.isEmpty(type)|| StringUtils.isEmpty(title)|| StringUtils.isEmpty(sequence)
                || StringUtils.isEmpty(content)){
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        map.put("created_time",Constant.y_M_d_H_m_s.format(new Date()));
        map.put("created_by","1");
        helpCenterMapper.addProblemManage(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 修改问题
     **/
    @Override
    public Tip updateProblemManage(Map<String, String> map) throws Exception{
        String type = map.get("type").toString();
        String sequence = map.get("sequence").toString();
        String title = map.get("title").toString();
        String content = map.get("content").toString();
        if ( StringUtils.isEmpty(type)|| StringUtils.isEmpty(title)|| StringUtils.isEmpty(sequence)
                 || StringUtils.isEmpty(content)){
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        map.put("updated_time",Constant.y_M_d_H_m_s.format(new Date()));
        map.put("updated_by","1");
        helpCenterMapper.updateProblemManage(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 问题详情
     * @param id
     * @return
     */
    @Override
    public Tip problemManageDetail(String id) throws Exception{
        Map detail = helpCenterMapper.problemManageDetail(Integer.valueOf(id));
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),detail);
    }

    /**
     * 批量删除问题
     **/
    @Override
    public Tip deleteAllProblem(String ids) throws Exception{
        String[] split = ids.split(",");
        List<String> list = Arrays.asList(split);
        list.stream().forEach(id -> helpCenterMapper.deleteProblem(id));
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 删除问题
     **/
    @Override
    public Tip deleteOneProblem(String id) throws Exception{
        helpCenterMapper.deleteProblem(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 获取问题分类
     **/
    @Override
    public Tip problemTypeList() throws Exception {
        List<Map> detail = helpCenterMapper.problemTypeList();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),detail);
    }

    /**
     * 问题分类管理列表
     * @return
     */
    @Override
    public Tip problemTypeManageList() throws Exception{
        List<Map> list = helpCenterMapper.getProblemTypeList();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),list);
    }

    /**
     * 删除问题分类
     **/
    @Override
    public Tip deleteOneProblemType(String id) throws Exception{
        helpCenterMapper.deleteProblemType(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 获取问题详情
     **/
    @Override
    public Tip problemTypeDetail(String id) throws Exception {
        Map detail = helpCenterMapper.getProblemTypeDetail(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),detail);
    }

    /**
     * 修改问题分类
     **/
    @Override
    public Tip updateProblemType(Map<String, String> map) throws Exception {
        map.put("updated_by","1");
        map.put("updated_time",Constant.y_M_d_H_m_s.format(new Date()));
        helpCenterMapper.updateProblemType(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }
}
