package com.drd.dt.back.service.Impl;

import com.drd.dt.back.dao.HtmlOrderBaseMapper;
import com.drd.dt.back.dto.*;
import com.drd.dt.back.service.HtmlOrderBaseService;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.modular.service.PayService;
import com.drd.dt.util.ExcelUtil;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Action;
import java.util.*;

@Service
public class HtmlOrderBaseServiceImpl implements HtmlOrderBaseService {

    @Autowired
    private HtmlOrderBaseMapper htmlOrderBaseMapper;
    @Autowired
    private PayService payService;


    /**
     * 订单列表
     */
    @Override
    public Tip orderList(OwnGoodsOrderDTO orderDTO) throws Exception {
        List<OwnGoodsOrderDTO> list = htmlOrderBaseMapper.selectOrderList(orderDTO);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);
    }


    /**
     * 订单详情
     */
    @Override
    public Tip orderDetail(Integer id) throws Exception {
        OwnGoodsOrderDTO dto = htmlOrderBaseMapper.orderDetail(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), dto);
    }


    /**
     * 修改订单
     */
    @Override
    public Tip orderUpdate(OwnGoodsOrderDTO orderDTO) {
        htmlOrderBaseMapper.orderUpdate(orderDTO);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 导出订单数据
     **/
    @Override
    public void excelExport(OwnGoodsOrderDTO orderDTO, HttpServletResponse response) {
        List<OwnGoodsOrderDTO> list = htmlOrderBaseMapper.selectExportOrderList(orderDTO);
        //开始导出
        Map<String, String> titleMap = new LinkedHashMap<>();
        titleMap.put("product_name", "产品名称");
        titleMap.put("etalon_value", "规格");
        titleMap.put("order_id", "订单号");
        titleMap.put("shipping_user_name", "姓名");
        titleMap.put("shipping_phone", "电话");
        titleMap.put("shipping_address", "地址");
        titleMap.put("actual_amount", "付款金额");
        titleMap.put("logistics_num", "物流号");
        titleMap.put("trade_no", "交易号");
        titleMap.put("paytype", "支付方式");
        titleMap.put("order_status", "订单状态");
        titleMap.put("create_time", "创建时间");
        String fileName = "订单信息表";
        ExcelUtil.excelExport(list, titleMap, fileName, response);
    }


    /*********************** 商品方法********************************************************/

    @Override
    public Tip selectAll(String title, String state) throws Exception {
        List<OwnGoodsDTO> all = htmlOrderBaseMapper.selectAll(title, state);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), all);
    }

    /**
     * 删除数据
     */
    @Override
    public Tip delete(Integer id) throws Exception {
        htmlOrderBaseMapper.deleteGoodsDateil(id);
        htmlOrderBaseMapper.deleteGoodsSpecification(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip addOwnGoods(OwnGoodsInsertDTO insertDTO) throws Exception {
        HtmlOwnGoodsDTO ownGoods = new HtmlOwnGoodsDTO();
        BeanUtils.copyProperties(insertDTO, ownGoods);
        ownGoods.setCreate_time(Constant.y_M_d_H_m_s.format(new Date()));
        htmlOrderBaseMapper.insert(ownGoods);
        return ResultUtil.result(SuccessResultEnum.ADD_SUCCESS.getCode(), SuccessResultEnum.ADD_SUCCESS.getMessage());
    }

    @Override
    public Tip update(HtmlOwnGoodsSizeDTO htmlOwnGoodsSizeDTO) throws Exception {
        HtmlOwnGoodsDTO ownGoods = (HtmlOwnGoodsDTO) htmlOwnGoodsSizeDTO;
        htmlOrderBaseMapper.updateById(ownGoods);
        return ResultUtil.result(SuccessResultEnum.UPDATE_SUCCESS.getCode(), SuccessResultEnum.UPDATE_SUCCESS.getMessage());
    }

    public Tip detail(Integer id) throws Exception {
        HtmlOwnGoodsSizeDTO ownGoods = htmlOrderBaseMapper.getDetailId(id);
        //查询规格
        List<Map<String, String>> maps = htmlOrderBaseMapper.getGoodsSpecificationDetail(id);
        Map<String, String> m = new HashMap<String, String>();
        String size_S = "";
        if (null != maps && maps.size() > 0) {
            for (Map ma : maps) {//颜色:翡翠绿、玛瑙红、大理黑,尺寸:100mm*330
                String size = "";
                if (ma.containsKey("id")) {
                    size += (Integer) ma.get("id") + "-";
                }
                if (ma.containsKey("etalon_value")) {
                    size += (String) ma.get("etalon_value") + "-";
                }
                if (ma.containsKey("status")) {
                    size += "(" + (Integer) ma.get("status") + ")";
                }
                if (size_S.length() > 1) {
                    size_S += ",";
                }
                size_S += size;
            }
        }
        ownGoods.setSize(size_S);


        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), ownGoods);
    }


    /**************************商品规格后端功能**************************************/

    @Override
    public Tip selectGoodsSpecificationDetail(Integer id) {
        List<GoodsSpecificationDTO> goodsSpecifications = htmlOrderBaseMapper.selectGoodsSpecificationDetail(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), goodsSpecifications);
    }

    @Override
    public Tip addGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification) {
        int req = htmlOrderBaseMapper.addGoodsSpecificationBase(goodsSpecification);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        } else {
            return ResultUtil.result(BizExceptionEnum.ADD_ERROR.getCode(), BizExceptionEnum.ADD_ERROR.getMessage());
        }
    }

    @Override
    public Tip finaAll(Integer id) {
        GoodsSpecificationDTO goodsSpecification = htmlOrderBaseMapper.finaAll(id);
        if (null != goodsSpecification) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), goodsSpecification);
        } else {
            return ResultUtil.result(BizExceptionEnum.SELECT_ERROR.getCode(), BizExceptionEnum.SELECT_ERROR.getMessage());
        }
    }

    @Override
    public Tip updateGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification) {
        int req = htmlOrderBaseMapper.updateGoodsSpecificationBase(goodsSpecification);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        } else {
            return ResultUtil.result(BizExceptionEnum.UPDATE_ERROR.getCode(), BizExceptionEnum.UPDATE_ERROR.getMessage());
        }
    }

    @Override
    public Tip deleteSpecification(Integer id) {
        htmlOrderBaseMapper.deleteSpecification(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip applyFor(Integer id) throws Exception {
        //查询订单信息
        OwnGoodsOrderDTO map = htmlOrderBaseMapper.orderDetail(id);
        if(null == map ){
            throw new BussinessException(BizExceptionEnum.GOODS_ORDER_LOSE);//退款失败
        }
        //走退款流程
        Boolean ifRefund = false;
        //4.1 根据不通支付类型走不通 退款
        switch (map.getPaytype()) {
            case "1": {//支付宝
                ifRefund = payService.zfbRefundMoney(map.getOrder_id(), map.getTrade_no(), Double.valueOf(map.getActual_amount()));
            }
            break;
            case "2": {//微信
                ifRefund = payService.wxRefundMoney(map.getOrder_id(), map.getTrade_no(),  Double.valueOf(map.getActual_amount()));
            }
            break;
            default: {
                throw new BussinessException(BizExceptionEnum.PATTERN_OF_PAYMENT_EXCEPTION);//用户登录异常
            }
        }
        //4.2 人工审核修改订单 同时退款接口退款成功 订单退款状态
        if (!ifRefund) {
            throw new BussinessException(BizExceptionEnum.REFUND_ERROR);//退款失败
        }

        //修改订单状态
        OwnGoodsOrderDTO ownGoodsOrderDTO = new OwnGoodsOrderDTO();
        ownGoodsOrderDTO.setId(id);
        ownGoodsOrderDTO.setRefund_status(2); //2-退款成功
        int req = htmlOrderBaseMapper.orderUpdate(ownGoodsOrderDTO);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        } else {
            return ResultUtil.result(BizExceptionEnum.UPDATE_ERROR.getCode(), BizExceptionEnum.UPDATE_ERROR.getMessage());
        }

    }

    @Override
    public Tip dataReportHtmlOrder() throws Exception {
        Map m = new HashMap();
        //

        return null;
    }
}
