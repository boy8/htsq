package com.drd.dt.back.service;

import com.drd.dt.back.entity.AdvBack;
import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface IAdvManageService {

    /**
     * 广告列表
     * @return
     * @throws Exception
     */
    Tip getAdvList() throws Exception;

    /**
     * 新增广告
     * @return
     */
    Tip insertAdv(AdvBack advBack) throws Exception;

    /**
     * 删除广告
     **/
    Tip delete(Integer id) throws Exception;

    /**
     * 修改
     */
    Tip update(AdvBack advBack) throws Exception;

    /**
     *获取广告详情
     **/
    Tip advDetailInfo(Integer id) throws Exception;
}
