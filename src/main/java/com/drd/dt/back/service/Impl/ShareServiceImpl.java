package com.drd.dt.back.service.Impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.drd.dt.common.Constant;
import com.drd.dt.util.HttpConnectionPoolUtil;
import com.drd.dt.util.ResultUtil;
import com.drd.dt.back.dao.ShareMapper;
import com.drd.dt.back.dto.ShareDTO;
import com.drd.dt.back.service.IShareService;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.properties.HDKPropertyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service("shareService")
public class ShareServiceImpl implements IShareService {

    @Autowired
    private ShareMapper shareMapper;
    @Autowired
    private HDKPropertyConfig hdkPropertyConfig;

    /**
     * 分享素材列表
     **/
    @Override
    public Tip list(Map<Object, Object> map) {
        List<ShareDTO> list = shareMapper.list(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),list);
    }

    /**
     * 分享素材列表(前端展示列表)
     **/
    @Override
    public Tip shareList(Map<String, String> map) {
        Integer page = 1;
        Integer type = 1;
        String app_packageName = "com.drd.quanbashi";
        if (map.containsKey("page")){
            page = Integer.valueOf(map.get("page"));
        }
        if (map.containsKey("type")){
            type = Integer.valueOf(map.get("type"));
        }
        if (map.containsKey("app_packageName")){
            app_packageName = String.valueOf(map.get("app_packageName"));
        }

        Map name = new HashMap();
        name.put("app_packageName",app_packageName);
        List<Map> app = shareMapper.getJgAppKey(name);
        String app_name = "核桃省钱";
        String app_head_img = "https://meimg.su.bcebos.com/htsq/htsq_icon.png";
        if (null != app && app.size() > 0){
            app_name = String.valueOf(app.get(0).get("app_name"));
            app_head_img = String.valueOf(app.get(0).get("app_head_img"));
        }

        if (type == 1){//商品推荐从好单库盆友圈获取
//            List list = getShareGoodsInfoByHDK(page,app_name,app_head_img);
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        }else {
            Integer pageSize = 20;
            Integer start = (page - 1) * pageSize;
            Map param = new HashMap();
            param.put("start",start);
            param.put("pageSize",pageSize);
            param.put("type",type);
            List<ShareDTO> list = shareMapper.shareList(param);
            String finalApp_head_img = app_head_img;
            String finalApp_name = app_name;
            list.stream().forEach(t -> {
                t.setHead_img(finalApp_head_img);
                t.setName(finalApp_name);
                String picture = t.getPicture();
                if (!StringUtils.isEmpty(picture) && picture.contains(",")){
                    t.setPictureArr(t.getPicture().split(","));
                }else {
                    t.setPictureArr(new String[]{t.getPicture()});
                }
            });
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),list);
        }
    }

    /**
     * 新增
     **/
    @Override
    public Tip insert(Map<String, String> map) {
        String name = map.get("name");
        String head_img = map.get("head_img");
        String content = map.get("content");
        if (content.equals("<p><br></p>")){
            content = "";
        }
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(head_img) || StringUtils.isEmpty(content)){
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        String now = Constant.y_M_d_H_m_s.format(new Date());
        map.put("create_time",now);
        shareMapper.insert(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 修改
     **/
    @Override
    public Tip update(Map<String, String> map) {
        String name = map.get("name");
        String head_img = map.get("head_img");
        String content = map.get("content");
        if (content.equals("<p><br></p>")){
            content = "";
        }
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(head_img) || StringUtils.isEmpty(content)){
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        String now = Constant.y_M_d_H_m_s.format(new Date());
        String str = content.replace("<p><br></p>","");
        map.put("update_time",now);
        map.put("content",str);
        shareMapper.update(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 删除
     **/
    @Override
    public Tip delete(Integer id) {
        shareMapper.delete(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 详情
     **/
    @Override
    public Tip detail(Integer id) {
        ShareDTO detail = shareMapper.detail(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),detail);
    }



}
