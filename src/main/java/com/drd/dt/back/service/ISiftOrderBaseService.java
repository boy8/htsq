package com.drd.dt.back.service;

import com.drd.dt.back.dto.OwnGoodsOrderDTO;
import com.drd.dt.back.dto.SiftOrderDTO;
import com.drd.dt.common.tips.Tip;

import javax.servlet.http.HttpServletResponse;

/**
 * SiftOrder 精选核桃 商品 订单表 接口定义
 */
public interface ISiftOrderBaseService {
    Tip insert(SiftOrderDTO siftOrderDTO);

    Tip update(SiftOrderDTO siftOrderDTO);

    Tip delete(Integer id);

    Tip queryAll(Integer id);

    Tip pageSelect(OwnGoodsOrderDTO ownGoodsOrderDTO);

    /**
     * 导出自营订单数据
     **/
    void excelExport(OwnGoodsOrderDTO orderDTO, HttpServletResponse response);


}