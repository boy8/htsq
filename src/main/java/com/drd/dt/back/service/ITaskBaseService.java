package com.drd.dt.back.service;

import com.drd.dt.common.tips.Tip;

public interface ITaskBaseService {

    /**
     * 查询用户金币信息
     */
    public Tip selectCoin(int user_id);

    /**
     * 修改/新增 我的金币、任务金币收入，粉丝金币收入
     */
    public Tip updateMyComeGoOutGold(int user_id, int coin_num, int gold_coin,int my_coin_num);

    /**
     * 修改 每日任务- 基础任务
     */
    public Tip updateMyTask();

    /*
    * 查询任务
    * */
    public Tip selectTaskList(int user_id);

    /**
     * 修改/新增 完成任务
     * */
    public Tip updateTaskS(String str,int user_id);
}
