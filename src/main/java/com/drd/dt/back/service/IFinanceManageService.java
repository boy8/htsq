package com.drd.dt.back.service;

import com.drd.dt.common.tips.Tip;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface IFinanceManageService {

    /**
     * 获取财务管理列表
     **/
    Tip financeManageList(@RequestParam Map<Object, Object> map) throws Exception;

    /**
     * 审核提现申请
     * @param map
     * @return
     */
    Tip auditing(Map<String, String> map) throws Exception;

    /**
     * 标记用户
     **/
    Tip flagUser(Map<String, String> map);
}
