package com.drd.dt.back.service;


import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface IHelpCenterService {

    /**
     * 添加问题分类
     **/
    Tip addProblemType(Map<String, String> map) throws Exception;

    /**
     * 问题管理列表
     * @return
     */
    Tip problemManageList() throws Exception;

    /**
     * 新增问题
     **/
    Tip addProblemManage(Map<String, String> map) throws Exception;

    /**
     * 修改问题
     **/
    Tip updateProblemManage(Map<String, String> map) throws Exception;

    /**
     * 获取问题详情
     **/
    Tip problemManageDetail(String id) throws Exception;

    /**
     * 批量删除问题
     **/
    Tip deleteAllProblem(String ids) throws Exception;

    /**
     * 删除问题
     **/
    Tip deleteOneProblem(String id) throws Exception;

    /**
     * 获取问题分类
     **/
    Tip problemTypeList() throws Exception;

    /**
     * 问题分类管理列表
     * @return
     */
    Tip problemTypeManageList() throws Exception;

    /**
     * 删除问题分类
     **/
    Tip deleteOneProblemType(String id) throws Exception;

    /**
     * 获取问题详情
     **/
    Tip problemTypeDetail(String id) throws Exception;

    /**
     * 修改问题分类
     **/
    Tip updateProblemType(Map<String, String> map) throws Exception;
}
