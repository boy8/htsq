package com.drd.dt.back.service;

import com.drd.dt.back.dto.JGPushDTO;
import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface JGPushService {

    /**
     * 消息推送
     **/
    Tip pushMsg(JGPushDTO pushDTO) throws Exception;

    /**
     *
     * **/
    Tip pushList(int page,int size);

    /**
     * 查询详情
     **/
    Tip detail(Integer id);

    /**
     * 更新
     **/
    Tip update(JGPushDTO pushDTO);

    /**
     * 删除
     **/
    Tip delete(Integer id);
}
