package com.drd.dt.back.service;


import com.drd.dt.back.entity.MessageActivity;
import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface INoticeManageService {

    /**
     * 公告列表
     * @return
     */
    Tip list() throws Exception;

    /**
     * 新增公告
     **/
    Tip add(MessageActivity activity) throws Exception;

    /**
     * 删除公告
     **/
    Tip delete(Integer id);

    /**
     * 公告详情
     **/
    Tip detail(Integer id);

    /**
     * 修改公告
     **/
    Tip update(MessageActivity activity);
}
