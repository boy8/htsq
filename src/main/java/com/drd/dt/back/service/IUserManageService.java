package com.drd.dt.back.service;

import com.drd.dt.back.dto.UserManageDTO;
import com.drd.dt.common.tips.Tip;

public interface IUserManageService {

    /**
     * 获取用户列表
     */
    Tip list(UserManageDTO userManageDTO);

    /**
     * 修改用户等级
     **/
    Tip update(Integer user_id, Integer level,String lv_expire_time,Integer status,String alipay_user_name,String alipay_account);

    /**
     * 用户详情
     **/
    Tip detail(Integer user_id);
}
