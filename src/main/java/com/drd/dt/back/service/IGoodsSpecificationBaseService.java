package com.drd.dt.back.service;

import com.drd.dt.back.dto.GoodsSpecificationDTO;
import com.drd.dt.back.entity.GoodsSpecification;
import com.drd.dt.common.tips.Tip;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IGoodsSpecificationBaseService {


    Tip addGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification);

    Tip updateGoodsSpecificationBase(GoodsSpecificationDTO goodsSpecification);

    Tip finaAll(Integer id);

    Tip delete(Integer id);

    //查询商品规格信息
    Tip selectGoodsSpecificationDetail(Integer id);
}
