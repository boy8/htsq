package com.drd.dt.filter;

import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.modular.service.impl.UserServiceImpl;
import com.drd.dt.util.JedisClient;
import com.drd.dt.util.ResultUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 86514 on 2020/3/25.
 */
@Aspect
@Component
public class NoRepeatSubmitAop {
    private final static Logger logger = LoggerFactory.getLogger(NoRepeatSubmitAop.class);

    @Autowired
    private JedisClient jedisClient;

    @Around("execution(* com.drd.dt.modular.controller.TaskController.taskCenterInfo(..)) && @annotation(nrs)")
    public Object arround(ProceedingJoinPoint pjp, NoRepeatSubmit nrs) throws Throwable{
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Object[] args = pjp.getArgs();
        String user_id = String.valueOf(args[0]);
        if (StringUtils.isEmpty(user_id)){
            user_id = RequestContextHolder.getRequestAttributes().getSessionId();
        }
        String key = user_id + request.getServletPath();
        Long ret = jedisClient.setnx("htsq:" + key, key, 2);
        if(ret == 1){
            logger.info("【第一次请求】");
            Object o = pjp.proceed();
            jedisClient.del("htsq:" + key);//完成请求后删除缓存
            return o;
        }else  {
            logger.info("【重复请求】");
            return null;
        }
    }
}
