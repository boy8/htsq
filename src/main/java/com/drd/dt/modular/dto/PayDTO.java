package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/11/13.
 */
public class PayDTO {
    //订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
    private String money;
    //商品的标题/交易标题/订单标题/订单关键字等。
    private String title;
    //商户网站唯一订单号
    private String order_id;
    //商品ID
    private String goods_id;
    //付款方式
    private Integer paytype;
    //订单类型  : 0-自营礼包订单 1 核桃精选订单
    private Integer orderType = 0;
    //订单购买类型：'购买方式：0自购 ，1来自分享'',',
    private Integer buyType = 0;

    public Integer getBuyType() {
        return buyType;
    }

    public void setBuyType(Integer buyType) {
        this.buyType = buyType;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getPaytype() {
        return paytype;
    }

    public void setPaytype(Integer paytype) {
        this.paytype = paytype;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}
