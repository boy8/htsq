package com.drd.dt.modular.dto;

/**
 * 最美商品
 * Created by 86514 on 2019/11/7.
 */
public class ZuimeiUserInfoDTO {
    private Integer id;
    private Integer user_id;
    private Integer type;
    private Integer status;
    private String nick_name;
    private String head_pic;
    private String rights_id;//淘礼金id
    private String goods_id;//使用淘礼金商品d
    private Integer zuimei_user_id;
    private String goods_name;
    private String pic;
    private String money;
    private String create_time;
    private String update_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getZuimei_user_id() {
        return zuimei_user_id;
    }

    public void setZuimei_user_id(Integer zuimei_user_id) {
        this.zuimei_user_id = zuimei_user_id;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(String head_pic) {
        this.head_pic = head_pic;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRights_id() {
        return rights_id;
    }

    public void setRights_id(String rights_id) {
        this.rights_id = rights_id;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
