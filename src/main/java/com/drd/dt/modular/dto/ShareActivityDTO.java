package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2020/1/9.
 */
public class ShareActivityDTO {
    private Integer id;
    private Integer user_id;
    private String activity_balance;
    private String activity_accumulated;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getActivity_balance() {
        return activity_balance;
    }

    public void setActivity_balance(String activity_balance) {
        this.activity_balance = activity_balance;
    }

    public String getActivity_accumulated() {
        return activity_accumulated;
    }

    public void setActivity_accumulated(String activity_accumulated) {
        this.activity_accumulated = activity_accumulated;
    }
}
