package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/11/7.
 */
public class ActivityDTO {
    private Integer id;
    private String title;
    private String content;
    private Integer jump_type;
    private Integer is_login;
    private Integer type;
    private Integer is_need_userid;
    private Integer discern_id;
    private String img_url;
    private String goods_id;
    private String jump_url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getJump_type() {
        return jump_type;
    }

    public void setJump_type(Integer jump_type) {
        this.jump_type = jump_type;
    }

    public Integer getIs_login() {
        return is_login;
    }

    public void setIs_login(Integer is_login) {
        this.is_login = is_login;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getJump_url() {
        return jump_url;
    }

    public void setJump_url(String jump_url) {
        this.jump_url = jump_url;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIs_need_userid() {
        return is_need_userid;
    }

    public void setIs_need_userid(Integer is_need_userid) {
        this.is_need_userid = is_need_userid;
    }

    public Integer getDiscern_id() {
        return discern_id;
    }

    public void setDiscern_id(Integer discern_id) {
        this.discern_id = discern_id;
    }
}
