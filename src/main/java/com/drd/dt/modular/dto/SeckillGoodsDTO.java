package com.drd.dt.modular.dto;

import java.util.List;

/**
 * Created by 86514 on 2019/11/7.
 */
public class SeckillGoodsDTO {
    private String goodsId;
    private String goodsName;
    private String goodsDesc;
    private String goodsUrl;
    private Boolean ifCoupon = false;//是否有优惠价卷
    private String shopType;//店铺类型
    private Integer type;//1 淘宝 2京东 3拼多多 4唯品会
    private String highCommissionUrl;//高佣地址
    private String couponUrl;//领券地址
    private String couponStartTime;
    private String couponEndTime;
    private String sales;//销量
    private String totalNum;//总量
    private String price;
    private String activity_type; //活动类型：普通活动; 聚划算; 淘抢购
    private String priceAfterCoupon;
    private String coupon = "0";//券金额
    private String commission;//佣金
    private String storeName;//店铺名
    private String goodsMainPicture;//主图
    private List<String> goodsCarouselPictures;//商品轮播图

    public String getGoodsId() {
        return goodsId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public String getGoodsUrl() {
        return goodsUrl;
    }

    public void setGoodsUrl(String goodsUrl) {
        this.goodsUrl = goodsUrl;
    }

    public Boolean getIfCoupon() {
        return ifCoupon;
    }

    public void setIfCoupon(Boolean ifCoupon) {
        this.ifCoupon = ifCoupon;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getHighCommissionUrl() {
        return highCommissionUrl;
    }

    public void setHighCommissionUrl(String highCommissionUrl) {
        this.highCommissionUrl = highCommissionUrl;
    }

    public String getCouponUrl() {
        return couponUrl;
    }

    public void setCouponUrl(String couponUrl) {
        this.couponUrl = couponUrl;
    }

    public String getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(String couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public String getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(String couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(String totalNum) {
        this.totalNum = totalNum;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getActivity_type() {
        return activity_type;
    }

    public void setActivity_type(String activity_type) {
        this.activity_type = activity_type;
    }

    public String getPriceAfterCoupon() {
        return priceAfterCoupon;
    }

    public void setPriceAfterCoupon(String priceAfterCoupon) {
        this.priceAfterCoupon = priceAfterCoupon;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getGoodsMainPicture() {
        return goodsMainPicture;
    }

    public void setGoodsMainPicture(String goodsMainPicture) {
        this.goodsMainPicture = goodsMainPicture;
    }

    public List<String> getGoodsCarouselPictures() {
        return goodsCarouselPictures;
    }

    public void setGoodsCarouselPictures(List<String> goodsCarouselPictures) {
        this.goodsCarouselPictures = goodsCarouselPictures;
    }
}
