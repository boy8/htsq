package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/11/16.
 */
public class MyProfitNewDTO {
    private String balance = "0";//账户余额
    private String today_total_commission = "0";//今日累计收益

    //我的订单收益
    private String my_yugu_commission = "0";//预估收益
    private String my_expire_order = "0";//失效订单收益

    //团队订单收益
    private String team_yugu_commission = "0";//预估收益
    private String team_expire_order = "0";//失效订单收益

    private String bounty_commission = "0";//奖励佣金
    private String activity_commission = "0";//活动佣金

    private Integer total_fans_num = 0;//累计粉丝量
    private Integer direct_num = 0;//专属粉丝数
    private Integer normal_num = 0;//普通粉丝数


    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getMy_yugu_commission() {
        return my_yugu_commission;
    }

    public void setMy_yugu_commission(String my_yugu_commission) {
        this.my_yugu_commission = my_yugu_commission;
    }

    public String getMy_expire_order() {
        return my_expire_order;
    }

    public void setMy_expire_order(String my_expire_order) {
        this.my_expire_order = my_expire_order;
    }

    public String getTeam_yugu_commission() {
        return team_yugu_commission;
    }

    public void setTeam_yugu_commission(String team_yugu_commission) {
        this.team_yugu_commission = team_yugu_commission;
    }

    public String getTeam_expire_order() {
        return team_expire_order;
    }

    public void setTeam_expire_order(String team_expire_order) {
        this.team_expire_order = team_expire_order;
    }

    public String getBounty_commission() {
        return bounty_commission;
    }

    public void setBounty_commission(String bounty_commission) {
        this.bounty_commission = bounty_commission;
    }

    public String getActivity_commission() {
        return activity_commission;
    }

    public void setActivity_commission(String activity_commission) {
        this.activity_commission = activity_commission;
    }

    public Integer getTotal_fans_num() {
        return total_fans_num;
    }

    public void setTotal_fans_num(Integer total_fans_num) {
        this.total_fans_num = total_fans_num;
    }

    public Integer getDirect_num() {
        return direct_num;
    }

    public void setDirect_num(Integer direct_num) {
        this.direct_num = direct_num;
    }

    public Integer getNormal_num() {
        return normal_num;
    }

    public void setNormal_num(Integer normal_num) {
        this.normal_num = normal_num;
    }

    public String getToday_total_commission() {
        return today_total_commission;
    }

    public void setToday_total_commission(String today_total_commission) {
        this.today_total_commission = today_total_commission;
    }
}
