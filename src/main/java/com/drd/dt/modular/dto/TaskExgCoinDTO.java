package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2020/3/27.
 */
public class TaskExgCoinDTO {
    private Integer now_coin;
    private Integer total_coin;
    private Integer fans_total_coin;
    private String money;

    public Integer getNow_coin() {
        return now_coin;
    }

    public void setNow_coin(Integer now_coin) {
        this.now_coin = now_coin;
    }

    public Integer getTotal_coin() {
        return total_coin;
    }

    public void setTotal_coin(Integer total_coin) {
        this.total_coin = total_coin;
    }

    public Integer getFans_total_coin() {
        return fans_total_coin;
    }

    public void setFans_total_coin(Integer fans_total_coin) {
        this.fans_total_coin = fans_total_coin;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
