package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/11/13.
 */
public class PersonalOrderDTO {
    private Integer type;
    private Integer status;
    private Integer buytype;
    private String product_pic_url;
    private String product_name = "";
    private String product_id = "";
    private String title;
    private String order_id;
    private String actual_amount;
    private String rake_back = "0";
    private String total_rake_back = "0";
    private String create_time;
    private String redpacket;
    private String istlj = "0";//是否是小美贝商品  1是 0否
    private String nick_name;


    /***
     * 扩展：order_type ：0 自营礼包订单  1 核桃精选订单
     * */
    private Integer order_type;
    /**
     * 扩展 核桃精选订单退款状态
     * */
    private int refund_status = 0;
    /**
     * 扩展 核桃精选 数量
     * */
    private int number;
    /**
     * 商品售卖类型：0核桃精选 1线下折扣
     * */
    private int discounts_type;

    public int getDiscounts_type() {
        return discounts_type;
    }

    public void setDiscounts_type(int discounts_type) {
        this.discounts_type = discounts_type;
    }

    public void setRefund_status(int refund_status) {
        this.refund_status = refund_status;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Integer getRefund_status() {
        return refund_status;
    }

    public void setRefund_status(Integer refund_status) {
        this.refund_status = refund_status;
    }

    public Integer getOrder_type() {
        return order_type;
    }

    public void setOrder_type(Integer order_type) {
        this.order_type = order_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getProduct_pic_url() {
        return product_pic_url;
    }

    public void setProduct_pic_url(String product_pic_url) {
        this.product_pic_url = product_pic_url;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getActual_amount() {
        return actual_amount;
    }

    public void setActual_amount(String actual_amount) {
        this.actual_amount = actual_amount;
    }

    public String getRake_back() {
        return rake_back;
    }

    public void setRake_back(String rake_back) {
        this.rake_back = rake_back;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getRedpacket() {
        return redpacket;
    }

    public void setRedpacket(String redpacket) {
        this.redpacket = redpacket;
    }

    public String getTotal_rake_back() {
        return total_rake_back;
    }

    public void setTotal_rake_back(String total_rake_back) {
        this.total_rake_back = total_rake_back;
    }

    public String getIstlj() {
        return istlj;
    }

    public void setIstlj(String istlj) {
        this.istlj = istlj;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public Integer getBuytype() {
        return buytype;
    }

    public void setBuytype(Integer buytype) {
        this.buytype = buytype;
    }
}
