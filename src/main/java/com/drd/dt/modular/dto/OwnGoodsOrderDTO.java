package com.drd.dt.modular.dto;


import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 提交订单模型
 */
public class OwnGoodsOrderDTO {
    @ApiModelProperty(value = "自营商品ID")
    private int goods_id;
    @ApiModelProperty(value = "商品图片地址", required = false)
    private String goods_image;
    @ApiModelProperty(value = "'商品名称'", required = false)
    private String goods_name;
    @ApiModelProperty(value = "用户ID")
    private int user_id;
    @ApiModelProperty(value = "'订单编号'", required = false)
    private String order_id;
    @ApiModelProperty(value = "'订单状态'")
    private int status;
    @ApiModelProperty(value = "数量")
    private int number;
    @ApiModelProperty(value = "实际付款")
    private Float actual_amount;

    @ApiModelProperty(value = "支付类型：1 支付宝、2 微信")
    private int actual_amount_type;
    @ApiModelProperty(value = "规格ID集合（',' 隔开）")
    private String goods_spe_id;
    @ApiModelProperty(value = "订单收货地址")
    private String shipping_address;
    @ApiModelProperty(value = "收货人名称")
    private String shipping_user_name;
    @ApiModelProperty(value = "收货人电话")
    private String shipping_phone;
    @ApiModelProperty(value = "该订单购买方式： 0 自购 1分享", required = false)
    private Integer buy_type = 0;
    //扩展 独立h5 购买界面新增 备注字段
    @ApiModelProperty(value = "扩展 独立h5 购买界面新增 备注字段", required = false)
    private String postscript;
    @ApiModelProperty(value = "扩展 独立h5 购买界面新增 邮费字段", required = false)
    private Float postage;


    public Float getPostage() {
        return postage;
    }

    public void setPostage(Float postage) {
        this.postage = postage;
    }

    public String getPostscript() {
        return postscript;
    }

    public void setPostscript(String postscript) {
        this.postscript = postscript;
    }

    public Integer getBuy_type() {
        return buy_type;
    }

    public void setBuy_type(Integer buy_type) {
        this.buy_type = buy_type;
    }

    public String getShipping_user_name() {
        return shipping_user_name;
    }

    public void setShipping_user_name(String shipping_user_name) {
        this.shipping_user_name = shipping_user_name;
    }

    public String getShipping_phone() {
        return shipping_phone;
    }

    public void setShipping_phone(String shipping_phone) {
        this.shipping_phone = shipping_phone;
    }

    public String getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(String shipping_address) {
        this.shipping_address = shipping_address;
    }

    public String getGoods_spe_id() {
        return goods_spe_id;
    }

    public void setGoods_spe_id(String goods_spe_id) {
        this.goods_spe_id = goods_spe_id;
    }

    public int getActual_amount_type() {
        return actual_amount_type;
    }

    public void setActual_amount_type(int actual_amount_type) {
        this.actual_amount_type = actual_amount_type;
    }

    public int getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(int goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_image() {
        return goods_image;
    }

    public void setGoods_image(String goods_image) {
        this.goods_image = goods_image;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }


    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Float getActual_amount() {
        return actual_amount;
    }

    public void setActual_amount(Float actual_amount) {
        this.actual_amount = actual_amount;
    }
}
