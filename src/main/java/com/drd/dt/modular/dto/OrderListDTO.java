package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/3/21.
 */
public class OrderListDTO {
    private Integer id;
    private Integer user_id;
    private Integer type;
    private Integer status;
    private Integer number;
    private String channel_id;
    private String product_id;
    private String product_pic_url;
    private String product_name;
    private String user_name;
    private String order_id;
    private Double actual_amount;
    private Double rake_back;
    private String create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_pic_url() {
        return product_pic_url;
    }

    public void setProduct_pic_url(String product_pic_url) {
        this.product_pic_url = product_pic_url;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public Double getActual_amount() {
        return actual_amount;
    }

    public void setActual_amount(Double actual_amount) {
        this.actual_amount = actual_amount;
    }

    public Double getRake_back() {
        return rake_back;
    }

    public void setRake_back(Double rake_back) {
        this.rake_back = rake_back;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
