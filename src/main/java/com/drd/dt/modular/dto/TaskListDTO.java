package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2020/3/17.
 */
public class TaskListDTO {
    private Integer id;
    private Integer task_id;
    private String task_name;
    private Integer times;
    private Integer total_times;
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTask_id() {
        return task_id;
    }

    public void setTask_id(Integer task_id) {
        this.task_id = task_id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTotal_times() {
        return total_times;
    }

    public void setTotal_times(Integer total_times) {
        this.total_times = total_times;
    }
}
