package com.drd.dt.modular.dto;

import com.drd.dt.util.EmojiUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.http.util.TextUtils;

/**
 * Created by 86514 on 2019/11/14.
 */
@ApiModel(value="修改用户信息对象",description="修改用户信息对象")
public class UpdateUserInfoDTO {
    @ApiModelProperty(value = "用户id",required = true)
    private Integer user_id;
    @ApiModelProperty(value = "token",required = true)
    private String token_id;
    @ApiModelProperty(value = "收货地址")
    private String addr;
    @ApiModelProperty(value = "性别：0女 1男")
    private Integer sex;
    @ApiModelProperty(value = "头像")
    private String head_pic;
    @ApiModelProperty(value = "昵称")
    private String nick_name;
    @ApiModelProperty(value = "微信号码")
    private String wx_num;
    @ApiModelProperty(value = "支付宝账号")
    private String alipay_account;
    @ApiModelProperty(value = "支付宝姓名")
    private String alipay_user_name;
    @ApiModelProperty(value = "淘宝用户id")
    private String taobao_user_id;
    @ApiModelProperty(value = "淘宝用户姓名")
    private String taobao_user_name;

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(String head_pic) {
        this.head_pic = head_pic;
    }

    public String getNick_name() {
        if (!TextUtils.isEmpty(nick_name)){
            nick_name= EmojiUtil.decode(nick_name);
        }
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        if (!TextUtils.isEmpty(nick_name)){
            nick_name = EmojiUtil.encode(nick_name);
        }
        this.nick_name = nick_name;
    }

    public String getWx_num() {
        return wx_num;
    }

    public void setWx_num(String wx_num) {
        this.wx_num = wx_num;
    }

    public String getAlipay_account() {
        return alipay_account;
    }

    public void setAlipay_account(String alipay_account) {
        this.alipay_account = alipay_account;
    }

    public String getAlipay_user_name() {
        return alipay_user_name;
    }

    public void setAlipay_user_name(String alipay_user_name) {
        this.alipay_user_name = alipay_user_name;
    }

    public String getTaobao_user_id() {
        return taobao_user_id;
    }

    public void setTaobao_user_id(String taobao_user_id) {
        this.taobao_user_id = taobao_user_id;
    }

    public String getTaobao_user_name() {
        return taobao_user_name;
    }

    public void setTaobao_user_name(String taobao_user_name) {
        this.taobao_user_name = taobao_user_name;
    }
}
