package com.drd.dt.modular.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 初始化参数
 * Created by 86514 on 2019/11/8.
 */
@ApiModel(value="初始化参数",description="初始化参数")
public class StuadDTO {
    @ApiModelProperty(value="渠道id")
    private String channel_id;
    @ApiModelProperty(value="手机唯一标识码")
    private String imei;
    @ApiModelProperty(value="客户端类型（Android:0  IOS:1）")
    private Integer deviceT;
    @ApiModelProperty(value="app版本号")
    private String app_version;
    @ApiModelProperty(value="浏览器信息")
    private String userAgent;
    @ApiModelProperty(value="app包名")
    private String app_package_name;
    @ApiModelProperty(value="创建时间",hidden = true)
    private String create_time;
    @ApiModelProperty(value="是否新增",hidden = true)
    private Integer is_new;
    private Integer userId;
    private Integer user_id;

    public Integer getIs_new() {
        return is_new;
    }

    public void setIs_new(Integer is_new) {
        this.is_new = is_new;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getDeviceT() {
        return deviceT;
    }

    public void setDeviceT(Integer deviceT) {
        this.deviceT = deviceT;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getApp_package_name() {
        return app_package_name;
    }

    public void setApp_package_name(String app_package_name) {
        this.app_package_name = app_package_name;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }
}
