package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2020/3/31.
 */
public class ZuimeiDTO {
    private String goodsId = "";
    private String goodsName = "";
    private Boolean ifCoupon = false;//是否有优惠价卷
    private String couponUrl = "";//领券地址
    private String couponStartTime = ""; //卷使用开始时间
    private String couponEndTime = "";//卷使用结束时间
    private String sales = "";//销量
    private String price = "0"; //价格
    private String priceAfterCoupon = "0";//优惠后价格
    private String coupon = "0";//券金额
    private String commission = "0";//佣金
    private String goodsMainPicture = "";//主图

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Boolean getIfCoupon() {
        return ifCoupon;
    }

    public void setIfCoupon(Boolean ifCoupon) {
        this.ifCoupon = ifCoupon;
    }

    public String getCouponUrl() {
        return couponUrl;
    }

    public void setCouponUrl(String couponUrl) {
        this.couponUrl = couponUrl;
    }

    public String getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(String couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public String getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(String couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceAfterCoupon() {
        return priceAfterCoupon;
    }

    public void setPriceAfterCoupon(String priceAfterCoupon) {
        this.priceAfterCoupon = priceAfterCoupon;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getGoodsMainPicture() {
        return goodsMainPicture;
    }

    public void setGoodsMainPicture(String goodsMainPicture) {
        this.goodsMainPicture = goodsMainPicture;
    }
}
