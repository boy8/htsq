package com.drd.dt.modular.dto;

import com.alibaba.fastjson.JSONArray;

/**
 * Created by tanhao on 2020/2/13.
 */
public class DailyGoodsDTO {
    private Integer id;
    private String title;
    private String content;
    private String goods_id;
    private String goods_name;
    private Integer ifcoupon;
    private String home_image;
    private String image;
    private String price;
    private String coupon;
    private String rake_back;
    private JSONArray images_list = new JSONArray();
    private String video;
    private Integer share_num;
    private Integer category;
    private Integer type;
    private String create_time;
    private String comment;//评论

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public Integer getIfcoupon() {
        return ifcoupon;
    }

    public void setIfcoupon(Integer ifcoupon) {
        this.ifcoupon = ifcoupon;
    }

    public String getHome_image() {
        return home_image;
    }

    public void setHome_image(String home_image) {
        this.home_image = home_image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Integer getShare_num() {
        return share_num;
    }

    public void setShare_num(Integer share_num) {
        this.share_num = share_num;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public JSONArray getImages_list() {
        return images_list;
    }

    public void setImages_list(JSONArray images_list) {
        this.images_list = images_list;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getRake_back() {
        return rake_back;
    }

    public void setRake_back(String rake_back) {
        this.rake_back = rake_back;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }
}
