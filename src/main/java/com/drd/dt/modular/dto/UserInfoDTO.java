package com.drd.dt.modular.dto;

import com.drd.dt.util.EmojiUtil;
import org.apache.http.util.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/11/14.
 */
public class UserInfoDTO {
    private Integer id;
    private String friend_code = "";
    private Integer level;
    private Integer sex = 1;
    private String head_pic = "";
    private String wx_num = "";
    private String nick_name = "";
    private String addr = "";
    private String gold_coin = "0";
    private String normal_balance = "0";
    private String bounty_balance = "0";
    private String activity_balance = "0";
    private String alipay_account = "";
    private String alipay_user_name = "";
    private String today_estimate_commission = "0";//今日预估佣金
    private String today_estimate_bounty = "0";//今日预估奖励
    private String total_estimate = "0";//累计预估收益
    private List<Map> banner = new ArrayList<>();//个人中心banner图
    private List<Map> littleIcon= new ArrayList<>();//个人中心 我的收益、订单、粉丝
    private List<Map> bigIcon = new ArrayList<>();//个人中心 邀请好友、我的金币
    private Integer saber = 1;//是否是审核版本（1是  0否）

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFriend_code() {
        return friend_code;
    }

    public void setFriend_code(String friend_code) {
        this.friend_code = friend_code;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(String head_pic) {
        this.head_pic = head_pic;
    }

    public String getNick_name()
    {
     if (!TextUtils.isEmpty(nick_name)){
            nick_name= EmojiUtil.decode(nick_name);
        }
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        if (!TextUtils.isEmpty(nick_name)){
            nick_name = EmojiUtil.encode(nick_name);
        }
        this.nick_name = nick_name;
    }

    public String getToday_estimate_commission() {
        return today_estimate_commission;
    }

    public void setToday_estimate_commission(String today_estimate_commission) {
        this.today_estimate_commission = today_estimate_commission;
    }

    public String getToday_estimate_bounty() {
        return today_estimate_bounty;
    }

    public void setToday_estimate_bounty(String today_estimate_bounty) {
        this.today_estimate_bounty = today_estimate_bounty;
    }

    public String getTotal_estimate() {
        return total_estimate;
    }

    public void setTotal_estimate(String total_estimate) {
        this.total_estimate = total_estimate;
    }

    public List<Map> getBanner() {
        return banner;
    }

    public void setBanner(List<Map> banner) {
        this.banner = banner;
    }

    public String getNormal_balance() {
        return normal_balance;
    }

    public void setNormal_balance(String normal_balance) {
        this.normal_balance = normal_balance;
    }

    public String getBounty_balance() {
        return bounty_balance;
    }

    public void setBounty_balance(String bounty_balance) {
        this.bounty_balance = bounty_balance;
    }

    public String getAlipay_account() {
        return alipay_account;
    }

    public void setAlipay_account(String alipay_account) {
        this.alipay_account = alipay_account;
    }

    public String getAlipay_user_name() {
        return alipay_user_name;
    }

    public void setAlipay_user_name(String alipay_user_name) {
        this.alipay_user_name = alipay_user_name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getWx_num() {
        return wx_num;
    }

    public void setWx_num(String wx_num) {
        this.wx_num = wx_num;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getGold_coin() {
        return gold_coin;
    }

    public void setGold_coin(String gold_coin) {
        this.gold_coin = gold_coin;
    }

    public List<Map> getLittleIcon() {
        return littleIcon;
    }

    public void setLittleIcon(List<Map> littleIcon) {
        this.littleIcon = littleIcon;
    }

    public List<Map> getBigIcon() {
        return bigIcon;
    }

    public void setBigIcon(List<Map> bigIcon) {
        this.bigIcon = bigIcon;
    }

    public Integer getSaber() {
        return saber;
    }

    public void setSaber(Integer saber) {
        this.saber = saber;
    }

    public String getActivity_balance() {
        return activity_balance;
    }

    public void setActivity_balance(String activity_balance) {
        this.activity_balance = activity_balance;
    }
}
