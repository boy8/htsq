package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2020/3/27.
 */
public class TaskExgHistoryDTO {
    private Integer id;
    private Integer coin_num;
    private String desc;
    private String create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCoin_num() {
        return coin_num;
    }

    public void setCoin_num(Integer coin_num) {
        this.coin_num = coin_num;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
