package com.drd.dt.modular.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by 86514 on 2020/1/13.
 */
public class VerifyDTO {
    @ApiModelProperty(required = true)
    private String opToken;//客户端返回的运营商token
    @ApiModelProperty(required = true)
    private String operator;//客户端返回的运营商，CMCC:中国移动通信, CUCC:中国联通通讯, CTCC:中国电信
    @ApiModelProperty(required = true)
    private String token;//客户端的token

    @ApiModelProperty(value="微信openid")
    private String wx_openid;
    @ApiModelProperty(value="微信wx_unionid")
    private String wx_unionid;
    @ApiModelProperty(value="第三方key")
    private String thirdKey;
    @ApiModelProperty(value="微信昵称")
    private String wx_nickname;
    @ApiModelProperty(value="头像图片")
    private String head_pic;
    @ApiModelProperty(value="客户端类型（Android:0  IOS:1）")
    private Integer deviceT;
    @ApiModelProperty(value="极光id")
    private String jp_id;
    @ApiModelProperty(value="性别：0女  1男")
    private Integer sex;
    @ApiModelProperty(value="app版本号")
    private String app_version;
    @ApiModelProperty(value="手机登录-电话号码")
    private String phone;
    @ApiModelProperty(value="手机登录-验证码")
    private String smsCode;
    @ApiModelProperty(value="用户id")
    private Integer user_id;
    @ApiModelProperty(value="浏览器UA")
    private String user_agent;
    @ApiModelProperty(value="登录时从剪切板获取后验证存在否")
    private String friend_code;
    private String xd_id;

    public String getOpToken() {
        return opToken;
    }

    public void setOpToken(String opToken) {
        this.opToken = opToken;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getWx_openid() {
        return wx_openid;
    }

    public void setWx_openid(String wx_openid) {
        this.wx_openid = wx_openid;
    }

    public String getThirdKey() {
        return thirdKey;
    }

    public void setThirdKey(String thirdKey) {
        this.thirdKey = thirdKey;
    }

    public String getWx_nickname() {
        return wx_nickname;
    }

    public void setWx_nickname(String wx_nickname) {
        this.wx_nickname = wx_nickname;
    }

    public String getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(String head_pic) {
        this.head_pic = head_pic;
    }

    public Integer getDeviceT() {
        return deviceT;
    }

    public void setDeviceT(Integer deviceT) {
        this.deviceT = deviceT;
    }

    public String getJp_id() {
        return jp_id;
    }

    public void setJp_id(String jp_id) {
        this.jp_id = jp_id;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUser_agent() {
        return user_agent;
    }

    public void setUser_agent(String user_agent) {
        this.user_agent = user_agent;
    }

    public String getFriend_code() {
        return friend_code;
    }

    public void setFriend_code(String friend_code) {
        this.friend_code = friend_code;
    }

    public String getXd_id() {
        return xd_id;
    }

    public void setXd_id(String xd_id) {
        this.xd_id = xd_id;
    }

    public String getWx_unionid() {
        return wx_unionid;
    }

    public void setWx_unionid(String wx_unionid) {
        this.wx_unionid = wx_unionid;
    }
}
