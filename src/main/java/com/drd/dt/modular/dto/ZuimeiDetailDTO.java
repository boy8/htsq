package com.drd.dt.modular.dto;

import com.alibaba.fastjson.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 86514 on 2020/3/31.
 */
public class ZuimeiDetailDTO {
    private String goodsId = "";
    private String goodsName = "";
    private String goodsDesc = "";
    private Boolean ifCoupon = false;//是否有优惠价卷
    private String couponStartTime = ""; //卷使用开始时间
    private String couponEndTime = "";//卷使用结束时间
    private String sales = "";//销量
    private String price = "0"; //价格
    private String priceAfterCoupon = "0";//优惠后价格
    private String coupon = "0";//券金额
    private String commission = "0";//佣金
    private String goodsMainPicture = "";//主图
    private String shopName = "";//店铺名称
    private String h5Url = "";//店铺名称
    private List<String> goodsCarouselPictures = new ArrayList<>();//轮播图

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Boolean getIfCoupon() {
        return ifCoupon;
    }

    public void setIfCoupon(Boolean ifCoupon) {
        this.ifCoupon = ifCoupon;
    }

    public String getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(String couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public String getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(String couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceAfterCoupon() {
        return priceAfterCoupon;
    }

    public void setPriceAfterCoupon(String priceAfterCoupon) {
        this.priceAfterCoupon = priceAfterCoupon;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getGoodsMainPicture() {
        return goodsMainPicture;
    }

    public void setGoodsMainPicture(String goodsMainPicture) {
        this.goodsMainPicture = goodsMainPicture;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public List<String> getGoodsCarouselPictures() {
        return goodsCarouselPictures;
    }

    public void setGoodsCarouselPictures(List<String> goodsCarouselPictures) {
        this.goodsCarouselPictures = goodsCarouselPictures;
    }

    public String getH5Url() {
        return h5Url;
    }

    public void setH5Url(String h5Url) {
        this.h5Url = h5Url;
    }
}
