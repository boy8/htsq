package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2020/3/2.
 */
public class WphShareDto {
    private String source;
    private String url;
    private String longUrl;
    private String ulUrl;
    private String deeplinkUrl;
    private String traFrom;
    private String noEvokeUrl;
    private String noEvokeLongUrl;
    private String vipWxUrl;
    private String vipWxCode;
    private String vipQuickAppUrl;
    private String friend_code;
    private String urlDesc = "";

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public String getUlUrl() {
        return ulUrl;
    }

    public void setUlUrl(String ulUrl) {
        this.ulUrl = ulUrl;
    }

    public String getDeeplinkUrl() {
        return deeplinkUrl;
    }

    public void setDeeplinkUrl(String deeplinkUrl) {
        this.deeplinkUrl = deeplinkUrl;
    }

    public String getTraFrom() {
        return traFrom;
    }

    public void setTraFrom(String traFrom) {
        this.traFrom = traFrom;
    }

    public String getNoEvokeUrl() {
        return noEvokeUrl;
    }

    public void setNoEvokeUrl(String noEvokeUrl) {
        this.noEvokeUrl = noEvokeUrl;
    }

    public String getNoEvokeLongUrl() {
        return noEvokeLongUrl;
    }

    public void setNoEvokeLongUrl(String noEvokeLongUrl) {
        this.noEvokeLongUrl = noEvokeLongUrl;
    }

    public String getVipWxUrl() {
        return vipWxUrl;
    }

    public void setVipWxUrl(String vipWxUrl) {
        this.vipWxUrl = vipWxUrl;
    }

    public String getVipWxCode() {
        return vipWxCode;
    }

    public void setVipWxCode(String vipWxCode) {
        this.vipWxCode = vipWxCode;
    }

    public String getVipQuickAppUrl() {
        return vipQuickAppUrl;
    }

    public void setVipQuickAppUrl(String vipQuickAppUrl) {
        this.vipQuickAppUrl = vipQuickAppUrl;
    }

    public String getFriend_code() {
        return friend_code;
    }

    public void setFriend_code(String friend_code) {
        this.friend_code = friend_code;
    }

    public String getUrlDesc() {
        return urlDesc;
    }

    public void setUrlDesc(String urlDesc) {
        this.urlDesc = urlDesc;
    }
}
