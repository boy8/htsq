package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/11/22.
 */
public class RollMSGDTO {
    private String title;
    private String content;
    private String create_time;
    private String rake_money;
    private Integer message_type = 0;
    private Integer type;
    private String nick_name;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getRake_money() {
        return rake_money;
    }

    public void setRake_money(String rake_money) {
        this.rake_money = rake_money;
    }

    public Integer getMessage_type() {
        return message_type;
    }

    public void setMessage_type(Integer message_type) {
        this.message_type = message_type;
    }
}
