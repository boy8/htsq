package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/3/28.
 */
public class TradeDTO {
    private Double withdraw_min_amount;
    //多少元以下自动转账
    private Double less_than_autopay;
    //可用余额
    private Double balance;
    //用户今日已提现次数
    private Integer count;
    //用户今日已提现次数
    private Integer alipyCount;
    //资产冻结 0表示未冻结 1表示资金被冻结
    private Integer balance_status;
    //用户每天提现次数
    private Integer withdraw_num_per_day;
    //提现自动转支付宝
    private Integer withdraw_auto_to_alipay;

    private String about_us;
    private String register_agreement;

    public Double getWithdraw_min_amount() {
        return withdraw_min_amount;
    }

    public void setWithdraw_min_amount(Double withdraw_min_amount) {
        this.withdraw_min_amount = withdraw_min_amount;
    }

    public Double getLess_than_autopay() {
        return less_than_autopay;
    }

    public void setLess_than_autopay(Double less_than_autopay) {
        this.less_than_autopay = less_than_autopay;
    }

    public Integer getWithdraw_num_per_day() {
        return withdraw_num_per_day;
    }

    public void setWithdraw_num_per_day(Integer withdraw_num_per_day) {
        this.withdraw_num_per_day = withdraw_num_per_day;
    }

    public Integer getWithdraw_auto_to_alipay() {
        return withdraw_auto_to_alipay;
    }

    public void setWithdraw_auto_to_alipay(Integer withdraw_auto_to_alipay) {
        this.withdraw_auto_to_alipay = withdraw_auto_to_alipay;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Integer getBalance_status() {
        return balance_status;
    }

    public void setBalance_status(Integer balance_status) {
        this.balance_status = balance_status;
    }

    public String getAbout_us() {
        return about_us;
    }

    public void setAbout_us(String about_us) {
        this.about_us = about_us;
    }

    public String getRegister_agreement() {
        return register_agreement;
    }

    public void setRegister_agreement(String register_agreement) {
        this.register_agreement = register_agreement;
    }

    public Integer getAlipyCount() {
        return alipyCount;
    }

    public void setAlipyCount(Integer alipyCount) {
        this.alipyCount = alipyCount;
    }
}
