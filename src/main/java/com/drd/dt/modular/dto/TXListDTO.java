package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/11/18.
 */
public class TXListDTO {
    private String txid;
    private String money;
    private String fee;
    private String final_money;
    private Integer status;
    private Integer type;
    private String create_time;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getFinal_money() {
        return final_money;
    }

    public void setFinal_money(String final_money) {
        this.final_money = final_money;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
