package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/11/7.
 */
public class SeckillTimeDTO {
    private String seckill_time;
    private String desc;
    private Integer sequence;
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getSeckill_time() {
        return seckill_time;
    }

    public void setSeckill_time(String seckill_time) {
        this.seckill_time = seckill_time;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }
}
