package com.drd.dt.modular.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/26.
 */
public class GoodsDetailDTO {
    private String goodsId = "";
    private String goodsName = "";
    private String goodsDesc = "";
    private String goodsUrl = "";
    private Boolean ifCoupon = false;//是否有优惠价卷
    private Integer type;//类型（1淘宝 2京东 3拼多多 4唯品会）
    private String highCommissionUrl = "";//高佣地址
    private String couponUrl = "";//领券地址
    private String couponStartTime = ""; //卷使用开始时间
    private String couponEndTime = "";//卷使用结束时间
    private String sales = "";//销量
    private String price = "0"; //价格
    private String priceAfterCoupon = "0";//优惠后价格
    private String coupon = "0";//券金额
    private String commission = "0";//佣金
    private String shopType = "";//店铺类型
    private String storeName = "";//店铺名
    private String storeUserId = "";//店铺ID
    private String storeIcon = "";//店铺icon
    private String storeURL = "";//店铺地址
    private String goodsMainPicture = "";//主图
    private List<String> goodsCarouselPictures = new ArrayList<>();//商品轮播图
    private String upcommission = "0";//上一级佣金
    private Integer saber = 1;//是否是审核版本（1是  0否）
    private String vipDesc;//会员升级提示描述

    public String getUpcommission() {
        return upcommission;
    }

    public void setUpcommission(String upcommission) {
        this.upcommission = upcommission;
    }

    /**
     * 扩展 详情界面 下面 H5详解
     */
    private String h5Url = "";
    /**
     * 扩展 相关推荐
     */
    private List<GoodsDetailDTO> relatedRecommendation = new ArrayList<GoodsDetailDTO>();
    /**
     * 扩展 店家详情
     */
    private Map<String, Object> storeGiveAMark = new HashMap<String, Object>();


    public String getStoreIcon() {
        return storeIcon;
    }

    public String getStoreURL() {
        return storeURL;
    }

    public void setStoreURL(String storeURL) {
        this.storeURL = storeURL;
    }

    public void setStoreIcon(String storeIcon) {
        this.storeIcon = storeIcon;
    }

    public Map<String, Object> getStoreGiveAMark() {
        return storeGiveAMark;
    }

    public void setStoreGiveAMark(Map<String, Object> storeGiveAMark) {
        this.storeGiveAMark = storeGiveAMark;
    }

    public List<GoodsDetailDTO> getRelatedRecommendation() {
        return relatedRecommendation;
    }

    public void setRelatedRecommendation(List<GoodsDetailDTO> relatedRecommendation) {
        this.relatedRecommendation = relatedRecommendation;
    }

    public String getH5Url() {
        return h5Url;
    }

    public void setH5Url(String h5Url) {
        this.h5Url = h5Url;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public String getGoodsUrl() {
        return goodsUrl;
    }

    public String getStoreUserId() {
        return storeUserId;
    }

    public void setStoreUserId(String storeUserId) {
        this.storeUserId = storeUserId;
    }

    public void setGoodsUrl(String goodsUrl) {
        this.goodsUrl = goodsUrl;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getHighCommissionUrl() {
        return highCommissionUrl;
    }

    public void setHighCommissionUrl(String highCommissionUrl) {
        this.highCommissionUrl = highCommissionUrl;
    }

    public String getCouponUrl() {
        return couponUrl;
    }

    public void setCouponUrl(String couponUrl) {
        this.couponUrl = couponUrl;
    }

    public String getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(String couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public String getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(String couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        Float f = 0f;
        if (null != price && !price.equals("")) {
            f = Float.valueOf(price);
        }
        this.price = String.valueOf(f);
    }

    public String getPriceAfterCoupon() {
        return priceAfterCoupon;
    }

    public void setPriceAfterCoupon(String priceAfterCoupon) {
        Float f = 0f;
        if (null != priceAfterCoupon && !priceAfterCoupon.equals("")) {
            f = Float.valueOf(priceAfterCoupon);
        }
        this.priceAfterCoupon = f + "";
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        Float f = 0f;
        if (null != coupon && !coupon.equals("")) {
            f = Float.valueOf(coupon);
        }
        this.coupon = f + "";
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        Float f = 0f;
        if (null != commission && !commission.equals("")) {
            f = Float.valueOf(commission);
        }
        this.commission = f + "";
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getGoodsMainPicture() {
        return goodsMainPicture;
    }

    public void setGoodsMainPicture(String goodsMainPicture) {
        this.goodsMainPicture = goodsMainPicture;
    }

    public List<String> getGoodsCarouselPictures() {
        return goodsCarouselPictures;
    }

    public void setGoodsCarouselPictures(List<String> goodsCarouselPictures) {
        this.goodsCarouselPictures = goodsCarouselPictures;
    }

    public Boolean getIfCoupon() {
        return ifCoupon;
    }

    public void setIfCoupon(Boolean ifCoupon) {
        this.ifCoupon = ifCoupon;
    }

    public Integer getSaber() {
        return saber;
    }

    public void setSaber(Integer saber) {
        this.saber = saber;
    }

    public String getVipDesc() {
        return vipDesc;
    }

    public void setVipDesc(String vipDesc) {
        this.vipDesc = vipDesc;
    }
}
