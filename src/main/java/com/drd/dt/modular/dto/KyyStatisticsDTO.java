package com.drd.dt.modular.dto;


import java.util.Date;

/**
 * 快应用 上报对象
 */
public class KyyStatisticsDTO {

    private int id;
    private int data_type;
    private Date creation_time;
    private String show_url;
    private String imei;
    private String ip;

    public KyyStatisticsDTO(int data_type, String show_url, String imei, String ip) {
        this.data_type = data_type;
        this.show_url = show_url;
        this.imei = imei;
        this.ip = ip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getData_type() {
        return data_type;
    }

    public void setData_type(int data_type) {
        this.data_type = data_type;
    }

    public Date getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(Date creation_time) {
        this.creation_time = creation_time;
    }

    public String getShow_url() {
        return show_url;
    }

    public void setShow_url(String show_url) {
        this.show_url = show_url;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
