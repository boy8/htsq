package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2020/3/3.
 */
public class TLJListInfoDTO {
    private Integer id;
    private Integer status;
    private Integer user_id;
    private Integer zuimei_user_id;
    private String rights_id;
    private String goods_id;
    private String goods_name;
    private String pic;
    private String money;
    private String create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getRights_id() {
        return rights_id;
    }

    public void setRights_id(String rights_id) {
        this.rights_id = rights_id;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getZuimei_user_id() {
        return zuimei_user_id;
    }

    public void setZuimei_user_id(Integer zuimei_user_id) {
        this.zuimei_user_id = zuimei_user_id;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
