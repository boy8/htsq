package com.drd.dt.modular.dto;

import com.drd.dt.util.EmojiUtil;

/**
 * Created by 86514 on 2020/3/16.
 */
public class TaskUserInfoDTO {
    private String head_pic;
    private String nick_name;
    private Integer total_coin;
    private String total_money;
    private Integer today_coin;
    private Integer today_fans_coin;

    public String getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(String head_pic) {
        this.head_pic = head_pic;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = EmojiUtil.decode(nick_name);
    }

    public Integer getTotal_coin() {
        return total_coin;
    }

    public void setTotal_coin(Integer total_coin) {
        this.total_coin = total_coin;
    }

    public String getTotal_money() {
        return total_money;
    }

    public void setTotal_money(String total_money) {
        this.total_money = total_money;
    }

    public Integer getToday_coin() {
        return today_coin;
    }

    public void setToday_coin(Integer today_coin) {
        this.today_coin = today_coin;
    }

    public Integer getToday_fans_coin() {
        return today_fans_coin;
    }

    public void setToday_fans_coin(Integer today_fans_coin) {
        this.today_fans_coin = today_fans_coin;
    }
}
