package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2020/3/26.
 */
public class ScanInfoDTO {
    private Integer id;
    private Integer user_id;
    private String union_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUnion_id() {
        return union_id;
    }

    public void setUnion_id(String union_id) {
        this.union_id = union_id;
    }
}
