package com.drd.dt.modular.dto;

import com.drd.dt.util.EmojiUtil;

/**
 * Created by 86514 on 2020/5/7.
 */
public class TaskFansContributionDTO {
    private Integer origin_id;
    private Integer coin_num;
    private String head_pic;
    private String nick_name;
    private Integer is_direct = 0;

    public Integer getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(Integer origin_id) {
        this.origin_id = origin_id;
    }

    public Integer getCoin_num() {
        return coin_num;
    }

    public void setCoin_num(Integer coin_num) {
        this.coin_num = coin_num;
    }

    public String getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(String head_pic) {
        this.head_pic = head_pic;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = EmojiUtil.decode(nick_name);
    }

    public Integer getIs_direct() {
        return is_direct;
    }

    public void setIs_direct(Integer is_direct) {
        this.is_direct = is_direct;
    }
}
