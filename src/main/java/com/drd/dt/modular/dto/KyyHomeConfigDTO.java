package com.drd.dt.modular.dto;


/**
 * 快应用首页配置 对象
 **/
public class KyyHomeConfigDTO {

    private int id;
    private String icon_list;
    private String search_url;
    private int show_ad;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcon_list() {
        return icon_list;
    }

    public void setIcon_list(String icon_list) {
        this.icon_list = icon_list;
    }

    public String getSearch_url() {
        return search_url;
    }

    public void setSearch_url(String search_url) {
        this.search_url = search_url;
    }

    public int getShow_ad() {
        return show_ad;
    }

    public void setShow_ad(int show_ad) {
        this.show_ad = show_ad;
    }
}
