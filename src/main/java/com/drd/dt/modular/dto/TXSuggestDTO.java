package com.drd.dt.modular.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 提现
 * Created by 86514 on 2019/11/18.
 */
@ApiModel(value="创建提现申请",description="创建提现申请")
public class TXSuggestDTO {
    @ApiModelProperty(value = "用户id",required = true)
    private Integer user_id;
    @ApiModelProperty(value = "token",required = true)
    private String token_id;
    @ApiModelProperty(value = "支付宝账号",required = true)
    private String alipay_account;
    @ApiModelProperty(value = "支付宝姓名",required = true)
    private String alipay_name;
    @ApiModelProperty(value = "提现金额",required = true)
    private String money;
    @ApiModelProperty(value = "提现类型(1普通佣金  2奖励金 3活动佣金)",required = true)
    private Integer type;
    private String time;

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getAlipay_account() {
        return alipay_account;
    }

    public void setAlipay_account(String alipay_account) {
        this.alipay_account = alipay_account;
    }

    public String getAlipay_name() {
        return alipay_name;
    }

    public void setAlipay_name(String alipay_name) {
        this.alipay_name = alipay_name;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
