package com.drd.dt.modular.dto;


/**
 * 信用卡订单信息
 */
public class CreditCardOrder {
    private Integer credit_card_order_id;
    private String id;
    private String completion_flag;
    private String bank_code;
    private String bank_name;
    private String card_name;
    private String card_icon;
    private String bank_icon;
    private String cust_no;
    private String cust_name;
    private String mobile_no;
    private String supplier_cust_no;
    private Double org_bonus;
    private Double bonus_rate;
    private Double share_rate;
    private String apply_status;
    private Double bonus;
    private long create_date_time;
    private long update_date_time;
    private String card_progress_switch;
    private String add_progress_info;
    private String crawl_id;
    private String issue_flag;
    private String remittance_flag;
    private String new_user_flag;
    private String fail_reason;
    private String cardId;
    private String is_out_stock;

    public Integer getCredit_card_order_id() {
        return credit_card_order_id;
    }

    public void setCredit_card_order_id(Integer credit_card_order_id) {
        this.credit_card_order_id = credit_card_order_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompletion_flag() {
        return completion_flag;
    }

    public void setCompletion_flag(String completion_flag) {
        this.completion_flag = completion_flag;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getCard_icon() {
        return card_icon;
    }

    public void setCard_icon(String card_icon) {
        this.card_icon = card_icon;
    }

    public String getBank_icon() {
        return bank_icon;
    }

    public void setBank_icon(String bank_icon) {
        this.bank_icon = bank_icon;
    }

    public String getCust_no() {
        return cust_no;
    }

    public void setCust_no(String cust_no) {
        this.cust_no = cust_no;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getSupplier_cust_no() {
        return supplier_cust_no;
    }

    public void setSupplier_cust_no(String supplier_cust_no) {
        this.supplier_cust_no = supplier_cust_no;
    }

    public Double getOrg_bonus() {
        return org_bonus;
    }

    public void setOrg_bonus(Double org_bonus) {
        this.org_bonus = org_bonus;
    }

    public Double getBonus_rate() {
        return bonus_rate;
    }

    public void setBonus_rate(Double bonus_rate) {
        this.bonus_rate = bonus_rate;
    }

    public Double getShare_rate() {
        return share_rate;
    }

    public void setShare_rate(Double share_rate) {
        this.share_rate = share_rate;
    }

    public String getApply_status() {
        return apply_status;
    }

    public void setApply_status(String apply_status) {
        this.apply_status = apply_status;
    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    public long getCreate_date_time() {
        return create_date_time;
    }

    public void setCreate_date_time(long create_date_time) {
        this.create_date_time = create_date_time;
    }

    public long getUpdate_date_time() {
        return update_date_time;
    }

    public void setUpdate_date_time(long update_date_time) {
        this.update_date_time = update_date_time;
    }

    public String getCard_progress_switch() {
        return card_progress_switch;
    }

    public void setCard_progress_switch(String card_progress_switch) {
        this.card_progress_switch = card_progress_switch;
    }

    public String getAdd_progress_info() {
        return add_progress_info;
    }

    public void setAdd_progress_info(String add_progress_info) {
        this.add_progress_info = add_progress_info;
    }

    public String getCrawl_id() {
        return crawl_id;
    }

    public void setCrawl_id(String crawl_id) {
        this.crawl_id = crawl_id;
    }

    public String getIssue_flag() {
        return issue_flag;
    }

    public void setIssue_flag(String issue_flag) {
        this.issue_flag = issue_flag;
    }

    public String getRemittance_flag() {
        return remittance_flag;
    }

    public void setRemittance_flag(String remittance_flag) {
        this.remittance_flag = remittance_flag;
    }

    public String getNew_user_flag() {
        return new_user_flag;
    }

    public void setNew_user_flag(String new_user_flag) {
        this.new_user_flag = new_user_flag;
    }

    public String getFail_reason() {
        return fail_reason;
    }

    public void setFail_reason(String fail_reason) {
        this.fail_reason = fail_reason;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getIs_out_stock() {
        return is_out_stock;
    }

    public void setIs_out_stock(String is_out_stock) {
        this.is_out_stock = is_out_stock;
    }
}


