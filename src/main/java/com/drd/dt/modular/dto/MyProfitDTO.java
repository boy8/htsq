package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/11/16.
 */
public class MyProfitDTO {
    private String balance = "0";//账户余额
    private String total_accumulated = "0";//累计到账
    private String total_commission = "0";//累计佣金
    private String total_bounty = "0";//累计奖励金

    private String yesterday_total = "0";//昨日预估收益
    private String yesterday_commission = "0";//昨日预估佣金
    private String yesterday_bounty = "0";//昨日预估奖励

    private String this_month_total = "0";//本月预估收益
    private String this_month_commission = "0";//本月预估佣金
    private String this_month_bounty = "0";//本月预估奖励

    private String pre_month_total = "0";//上月预估收益
    private String pre_month_commission = "0";//上月预估佣金
    private String pre_month_bounty = "0";//上月预估奖励

    private Integer total_fans_num = 0;//累计粉丝量
    private Integer direct_num = 0;//专属粉丝数
    private Integer normal_num = 0;//普通粉丝数

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getTotal_accumulated() {
        return total_accumulated;
    }

    public void setTotal_accumulated(String total_accumulated) {
        this.total_accumulated = total_accumulated;
    }

    public String getTotal_commission() {
        return total_commission;
    }

    public void setTotal_commission(String total_commission) {
        this.total_commission = total_commission;
    }

    public String getTotal_bounty() {
        return total_bounty;
    }

    public void setTotal_bounty(String total_bounty) {
        this.total_bounty = total_bounty;
    }

    public String getYesterday_total() {
        return yesterday_total;
    }

    public void setYesterday_total(String yesterday_total) {
        this.yesterday_total = yesterday_total;
    }

    public String getYesterday_commission() {
        return yesterday_commission;
    }

    public void setYesterday_commission(String yesterday_commission) {
        this.yesterday_commission = yesterday_commission;
    }

    public String getYesterday_bounty() {
        return yesterday_bounty;
    }

    public void setYesterday_bounty(String yesterday_bounty) {
        this.yesterday_bounty = yesterday_bounty;
    }

    public String getThis_month_total() {
        return this_month_total;
    }

    public void setThis_month_total(String this_month_total) {
        this.this_month_total = this_month_total;
    }

    public String getThis_month_commission() {
        return this_month_commission;
    }

    public void setThis_month_commission(String this_month_commission) {
        this.this_month_commission = this_month_commission;
    }

    public String getThis_month_bounty() {
        return this_month_bounty;
    }

    public void setThis_month_bounty(String this_month_bounty) {
        this.this_month_bounty = this_month_bounty;
    }

    public String getPre_month_total() {
        return pre_month_total;
    }

    public void setPre_month_total(String pre_month_total) {
        this.pre_month_total = pre_month_total;
    }

    public String getPre_month_commission() {
        return pre_month_commission;
    }

    public void setPre_month_commission(String pre_month_commission) {
        this.pre_month_commission = pre_month_commission;
    }

    public String getPre_month_bounty() {
        return pre_month_bounty;
    }

    public void setPre_month_bounty(String pre_month_bounty) {
        this.pre_month_bounty = pre_month_bounty;
    }

    public Integer getTotal_fans_num() {
        return total_fans_num;
    }

    public void setTotal_fans_num(Integer total_fans_num) {
        this.total_fans_num = total_fans_num;
    }

    public Integer getDirect_num() {
        return direct_num;
    }

    public void setDirect_num(Integer direct_num) {
        this.direct_num = direct_num;
    }

    public Integer getNormal_num() {
        return normal_num;
    }

    public void setNormal_num(Integer normal_num) {
        this.normal_num = normal_num;
    }
}
