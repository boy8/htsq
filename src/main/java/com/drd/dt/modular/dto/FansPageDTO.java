package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/12/11.
 */
public class FansPageDTO {
    private int id;
    private Integer t;//是否直属粉丝  1是  0否
    private double m;//佣金

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getM() {
        return m;
    }

    public void setM(double m) {
        this.m = m;
    }

    public Integer getT() {
        return t;
    }

    public void setT(Integer t) {
        this.t = t;
    }
}
