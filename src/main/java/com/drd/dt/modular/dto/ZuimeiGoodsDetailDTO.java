package com.drd.dt.modular.dto;

import java.util.List;

public class ZuimeiGoodsDetailDTO {
    private Integer id;
    private String goods_name;
    private String goods_id;
    private String pic;
    private String itemdesc;
    private String shopname;
    private String price;
    private String price_coupons;
    private String price_after_coupons;
    private String anti_growth;
    private Integer status;
    private Integer type;
    private boolean ifcoupon = false;
    private Integer sales;
    private String coupon_startTime;
    private String coupon_endTime;
    private String h5Url;
    private List<String> goodsCarouselPictures;
    private String vipDesc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getItemdesc() {
        return itemdesc;
    }

    public void setItemdesc(String itemdesc) {
        this.itemdesc = itemdesc;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_coupons() {
        return price_coupons;
    }

    public void setPrice_coupons(String price_coupons) {
        this.price_coupons = price_coupons;
    }

    public String getPrice_after_coupons() {
        return price_after_coupons;
    }

    public void setPrice_after_coupons(String price_after_coupons) {
        this.price_after_coupons = price_after_coupons;
    }

    public String getAnti_growth() {
        return anti_growth;
    }

    public void setAnti_growth(String anti_growth) {
        this.anti_growth = anti_growth;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public boolean isIfcoupon() {
        return ifcoupon;
    }

    public void setIfcoupon(boolean ifcoupon) {
        this.ifcoupon = ifcoupon;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    public String getCoupon_startTime() {
        return coupon_startTime;
    }

    public void setCoupon_startTime(String coupon_startTime) {
        this.coupon_startTime = coupon_startTime;
    }

    public String getCoupon_endTime() {
        return coupon_endTime;
    }

    public void setCoupon_endTime(String coupon_endTime) {
        this.coupon_endTime = coupon_endTime;
    }

    public String getH5Url() {
        return h5Url;
    }

    public void setH5Url(String h5Url) {
        this.h5Url = h5Url;
    }

    public List<String> getGoodsCarouselPictures() {
        return goodsCarouselPictures;
    }

    public void setGoodsCarouselPictures(List<String> goodsCarouselPictures) {
        this.goodsCarouselPictures = goodsCarouselPictures;
    }

    public String getVipDesc() {
        return vipDesc;
    }

    public void setVipDesc(String vipDesc) {
        this.vipDesc = vipDesc;
    }
}
