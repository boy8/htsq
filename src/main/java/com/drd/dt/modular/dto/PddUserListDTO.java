package com.drd.dt.modular.dto;

/**
 * Created by 86514 on 2019/3/21.
 */
public class PddUserListDTO {
    private Integer id;
    private String channel_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }
}
