package com.drd.dt.modular.dto;

import com.drd.dt.modular.entity.UserMoney;
import com.drd.dt.util.EmojiUtil;
import org.springframework.util.StringUtils;

public class UserMoneyDTO extends UserMoney {
    /**
     * 扩展
     * **/
    private String nick_name; //昵称
    private String level; //等级
    private String create_time;//创建时间
    private String introducerName;//邀请人昵称

    private String budgetEarnings;//上月预估收益   上月报表 自购预估佣金、礼包预估奖励、粉丝返来的预估 总和
    private String  accumulativeTotalEarnings;//累计预估收益 收益表：用户普通金额预估、普通佣金累计收益、分享活动累计收益、粉丝预估、奖励佣金累计收益 总和

    public String getNick_name() {
        if(!StringUtils.isEmpty(nick_name)){
            return EmojiUtil.decode(nick_name);
        }
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getIntroducerName() {
        if(!StringUtils.isEmpty(introducerName)){
            return EmojiUtil.decode(introducerName);
        }
        return introducerName;
    }

    public void setIntroducerName(String introducerName) {
        this.introducerName = introducerName;
    }

    public String getBudgetEarnings() {
        return budgetEarnings;
    }

    public void setBudgetEarnings(String budgetEarnings) {
        this.budgetEarnings = budgetEarnings;
    }

    public String getAccumulativeTotalEarnings() {
        return accumulativeTotalEarnings;
    }

    public void setAccumulativeTotalEarnings(String accumulativeTotalEarnings) {
        this.accumulativeTotalEarnings = accumulativeTotalEarnings;
    }
}
