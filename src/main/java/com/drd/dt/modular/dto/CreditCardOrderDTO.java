package com.drd.dt.modular.dto;


import java.text.SimpleDateFormat;

/**
 *  jz_credit_card_order 与 jz_order 相结合
 *  提示: 该实体对象 目前仅限于用 订单查询接口  返回对象
 */
public class CreditCardOrderDTO extends CreditCardOrder {

    private String create_date_time_text; //创建时间
    private String clear_time_text; //结算时间

    private Integer  orderType;//jz_order订单 类型 1淘宝,2京东，3拼多多，4唯品会 （10信用卡）',

    private Integer creditCardOrderStatus; //订单状态

    private String creditCardOrderStatusText; //订单状态 文字描述

    public Integer getCreditCardOrderStatus() {
        return creditCardOrderStatus;
    }

    public void setCreditCardOrderStatus(Integer creditCardOrderStatus) {
        this.creditCardOrderStatus = creditCardOrderStatus;
    }

    public String getCreditCardOrderStatusText() {
        //'订单状态 1-订单结算 2-订单付款 3-订单未付款 4-订单失效
        // 当type = 10   ,1订单结算 2 审核通过，3 审核中 ， 4 审核不 通过 5带查询  6未完成7 异常订单 8 失效订单'
        if(null != orderType && orderType == 10 && null != creditCardOrderStatus){
            switch (creditCardOrderStatus){
                case 1:return "订单结算";
                case 2:return "审核通过";
                case 3:return "审核中";
                case 4:return "审核不通过";
                case 5:return "带查询";
                case 6:return "未完成";
                case 7:return "异常订单";
                case 8:return "失效订单";
                default:return "";
            }
        }else{
            return "";
        }

    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public void setCreditCardOrderStatusText(String creditCardOrderStatusText) {
        this.creditCardOrderStatusText = creditCardOrderStatusText;
    }

    public String getCreate_date_time_text() {
        return create_date_time_text;
    }

    public void setCreate_date_time_text(String create_date_time_text) {
        this.create_date_time_text = create_date_time_text;
    }

    public String getClear_time_text() {
        return clear_time_text;
    }

    public void setClear_time_text(String clear_time_text) {
        this.clear_time_text = clear_time_text;
    }
}


