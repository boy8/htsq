package com.drd.dt.modular.dto;

import com.drd.dt.util.EmojiUtil;
import org.apache.http.util.TextUtils;

/**
 * Created by 86514 on 2019/11/15.
 */
public class FansInfoDTO {
    private Integer id;
    private Integer fans_num = 0;
    private String friend_code = "";
    private String head_pic = "";
    private String nick_name = "";
    private String wx_num = "";
    private String pre_month_accumulated = "0";//上月预估收益
    private String total_estimate = "0";//累计预估收益

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFans_num() {
        return fans_num;
    }

    public void setFans_num(Integer fans_num) {
        this.fans_num = fans_num;
    }

    public String getFriend_code() {
        return friend_code;
    }

    public void setFriend_code(String friend_code) {
        this.friend_code = friend_code;
    }

    public String getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(String head_pic) {
        this.head_pic = head_pic;
    }

    public String getNick_name() {
       if (!TextUtils.isEmpty(nick_name)){
            nick_name= EmojiUtil.decode(nick_name);
        }
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        if (!TextUtils.isEmpty(nick_name)){
            nick_name = EmojiUtil.encode(nick_name);
        }
        this.nick_name = nick_name;
    }

    public String getWx_num() {
        return wx_num;
    }

    public void setWx_num(String wx_num) {
        this.wx_num = wx_num;
    }

    public String getPre_month_accumulated() {
        return pre_month_accumulated;
    }

    public void setPre_month_accumulated(String pre_month_accumulated) {
        this.pre_month_accumulated = pre_month_accumulated;
    }

    public String getTotal_estimate() {
        return total_estimate;
    }

    public void setTotal_estimate(String total_estimate) {
        this.total_estimate = total_estimate;
    }
}
