package com.drd.dt.modular.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

public class BusinessAcademyDTO {
    /**
     * 查询(分页) 每日爆款/宣传素材/商学院 内容列表
     *
     * @param pageIndex   当前页数
     * @param pageSize    每页最大数
     * @param first_module  1每日爆款 2宣传素材 3商学院
     * @param keywords    二级分类
     * @param state       1 启用 0禁用
     * @param title       商学院-搜索文章 标题 模糊查询
     */
    @ApiModelProperty(value = "当前页数")
    private int pageIndex;
    @ApiModelProperty(value = "每页最大数")
    private int pageSize;
    @ApiModelProperty(value = "1每日爆款 2宣传素材 3商学院")
    private int first_module;
    @ApiModelProperty(value = "二级分类")
    private int second_module;
    @ApiModelProperty(value = "三级级分类")
    private int third_module;
    @ApiModelProperty(value = "  1 启用 0禁用")
    private int state;
    @ApiModelProperty(value = "  商学院-搜索文章 标题 模糊查询")
    private String title;


    /**
     * 获取详情 所需参数并 判断核对权限
     *
     * @param user_id     用户ID
     * @param business_id 主键ID
     * @param
     */
    @ApiModelProperty(value = " 用户ID")
    private int user_id;
    @ApiModelProperty(value = "主键ID")
    private int business_id;

    /**
     * 点赞、转发、评论、阅读 累计
     *
     * @param addTypeNum 当前累加字段名称 ：like_num((点赞数)、transpond_num(转发数)、comment_num(评论数)、show_num(阅读数)
     * @param business_id 主键iD
     */
    @ApiModelProperty(value = "当前累加字段名称 ：like_num((点赞数)、transpond_num(转发数)、comment_num(评论数)、show_num(阅读数)", hidden = true)
    private String addTypeNum;

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getFirst_module() {
        return first_module;
    }

    public void setFirst_module(int first_module) {
        this.first_module = first_module;
    }

    public int getSecond_module() {
        return second_module;
    }

    public void setSecond_module(int second_module) {
        this.second_module = second_module;
    }

    public int getThird_module() {
        return third_module;
    }

    public void setThird_module(int third_module) {
        this.third_module = third_module;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(int business_id) {
        this.business_id = business_id;
    }

    public String getAddTypeNum() {
        return addTypeNum;
    }

    public void setAddTypeNum(String addTypeNum) {
        this.addTypeNum = addTypeNum;
    }
}
