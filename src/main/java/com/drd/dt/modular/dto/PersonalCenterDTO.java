package com.drd.dt.modular.dto;

import com.drd.dt.util.EmojiUtil;
import org.apache.http.util.TextUtils;

/**
 * Created by 86514 on 2019/11/13.
 */
public class PersonalCenterDTO {
    private Integer id;
    private Integer introducer;
    private Integer level;
    private String friend_code = "";
    private String head_pic = "";
    private String nick_name = "";
    private String create_time = "";
    private String recent_login_time = "";
    private String bounty_accumulated = "0";
    private String accumulated = "0";
    private String wx_num = "";
    private Integer is_direct = 1;//是否直属粉丝  1是  0否
    private double total_accumulated;
    private String intro_people;//邀请人姓名
    private Integer fans_num;//粉丝数

    public double getTotal_accumulated() {
        return total_accumulated;
    }

    public void setTotal_accumulated(Double total_accumulated) {
        this.total_accumulated = total_accumulated;
    }

    public String getWx_num() {
        return wx_num;
    }

    public void setWx_num(String wx_num) {
        this.wx_num = wx_num;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Integer introducer) {
        this.introducer = introducer;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getFriend_code() {
        return friend_code;
    }

    public void setFriend_code(String friend_code) {
        this.friend_code = friend_code;
    }

    public String getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(String head_pic) {
        this.head_pic = head_pic;
    }

    public String getNick_name() {
        if (!TextUtils.isEmpty(nick_name)){
            nick_name= EmojiUtil.decode(nick_name);
        }
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        if (!TextUtils.isEmpty(nick_name)){
            nick_name = EmojiUtil.encode(nick_name);
        }
        this.nick_name = nick_name;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getBounty_accumulated() {
        return bounty_accumulated;
    }

    public void setBounty_accumulated(String bounty_accumulated) {
        this.bounty_accumulated = bounty_accumulated;
    }

    public String getAccumulated() {
        return accumulated;
    }

    public void setAccumulated(String accumulated) {
        this.accumulated = accumulated;
    }

    public Integer getIs_direct() {
        return is_direct;
    }

    public void setIs_direct(Integer is_direct) {
        this.is_direct = is_direct;
    }

    public void setTotal_accumulated(double total_accumulated) {
        this.total_accumulated = total_accumulated;
    }

    public String getIntro_people() {
        if (!TextUtils.isEmpty(intro_people)){
            intro_people= EmojiUtil.decode(intro_people);
        }
        return intro_people;
    }

    public void setIntro_people(String intro_people) {
        this.intro_people = intro_people;
    }

    public Integer getFans_num() {
        return fans_num;
    }

    public void setFans_num(Integer fans_num) {
        this.fans_num = fans_num;
    }

    public String getRecent_login_time() {
        return recent_login_time;
    }

    public void setRecent_login_time(String recent_login_time) {
        this.recent_login_time = recent_login_time;
    }
}
