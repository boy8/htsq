package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.*;
import com.drd.dt.modular.entity.ImageConfig;
import com.drd.dt.modular.entity.InitConfig;
import com.drd.dt.modular.entity.Adv;
import com.drd.dt.modular.entity.Mall;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface HomeMapper {

    /**
     * 获取首页菜单分类
     */
    List<Mall> selectCategory();

    /**
     *获取开屏或banner图
     **/
    List<Adv> getAds(@Param("block") Integer block);

    /**
     * 获取首页公告列表
     */
    List<ActivityDTO> selectNotice();

    /**
     *获取限时秒杀时间点
     **/
    List<SeckillTimeDTO> selectSeckillTime();

    /**
     * 获取初始化信息
     * @return
     */
    InitConfig selectInitConfigInfo();

    /**
     *通过渠道和imei获取初始化信息
     */
    List<StuadDTO> findByIMEI(StuadDTO stuadDTO);

    /**
     * 新增渠道表
     * @param stuadDTO
     */
    void insertChannel(StuadDTO stuadDTO);

    /**
     * 获取提现系统消息列表
     */
    List<RollMSGDTO> getSYSShowList(@Param("user_id") String user_id);

    /**
     * 获取提现佣金消息列表
     */
    List<RollMSGDTO> getcommissionList(@Param("user_id") String user_id);

    /**
     * 获取视频教程链接
     **/
    List<ImageConfig> getCourse();

    /**
     * 首页
     */
    Integer getReadStatus(@Param("user_id") String user_id);

    /**
     * 零元购列表
     **/
    List<ZeroPurchaseDTO> selectZeroList(@Param("page") Page page);

    /**
     * 首页 中间部位 限时抢购旁边 核桃精选图片与地址
     * */
    List<Map> getHomeseckillTimeAndSiftGoodsBanner();

    String get618Url(@Param("type") String type);
}
