package com.drd.dt.modular.dao;

import com.drd.dt.back.dto.SiftGoodsSpecificationDTO;
import com.drd.dt.back.entity.GoodsSpecification;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SiftGoodsSpecificationMapper {

    /**
     * 查询精选核桃商品规格
     */
    List<SiftGoodsSpecificationDTO> getSiftGoodsSpecification(@Param("goods_id") String goods_id);
    List<SiftGoodsSpecificationDTO>  getSiftGoodsSpecificationS(@Param("ids")List<Long> ids);
}
