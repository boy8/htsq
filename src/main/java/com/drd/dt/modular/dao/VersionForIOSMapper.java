package com.drd.dt.modular.dao;


import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface VersionForIOSMapper {


    void insertGoods(Map<String, String> map);

    List<Map> list(Map<String, String> map);

    void status(@Param("id") String id);
}
