package com.drd.dt.modular.dao;

import com.drd.dt.modular.dto.KyyHomeConfigDTO;
import com.drd.dt.modular.dto.KyyStatisticsDTO;

import java.util.List;

/**
 * 快应用 首页配置
 * **/
public interface KyyHomeConfigMapper {

    //获取配置信息
    KyyHomeConfigDTO getConfig();

    //上报
    void addStatistics(KyyStatisticsDTO kyyStatisticsDTO);
}
