package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dto.SiftGoodsDTO;
import com.drd.dt.back.entity.AdvBack;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SiftGoodsMapper {

    /**
     * 查询列表 分页
     * */
    List<SiftGoodsDTO> SiftGoodsList(@Param("page") Page page,SiftGoodsDTO siftGoodsDTO);


    /**
     * 查询精选核桃商品详情
     * */
    SiftGoodsDTO SiftGoodsDateil(@Param("id")String id);

    /**
     * 查询banner 图片
     * */
     List<AdvBack> getPersonalBanner();

     /**
      * 查询 商品 分类
      * */
    List<String> getTopSort();
}
