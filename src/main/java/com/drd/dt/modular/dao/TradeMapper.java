package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dto.MessageCenterDTO;
import com.drd.dt.modular.dto.TXListDTO;
import com.drd.dt.modular.dto.TradeDTO;
import com.drd.dt.modular.entity.TX;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface TradeMapper extends BaseMapper<TX> {

    /**
     * 提现列表
     */
    List<TXListDTO> getTxList(@Param("page") Page page, @Param("type") String type, @Param("user_id") Integer user_id);

    /**
     * 查询用户金额
     */
    Map findUserBalance(@Param("user_id") Integer user_id);

    /**
     * 获取提现配置信息
     */
    TradeDTO selectConfigInfo(@Param("user_id") Integer user_id,@Param("type") Integer type,
                              @Param("alipay_account") String alipay_account);

    /**
     * 查询支付宝账号今天提现次数
     */
    Integer getTXCountByAccount(@Param("alipay_account") String alipay_account);

    /**
     * 自动转账成功，更新状态
     */
    void updateTxStatus(Map map);

    /**
     * 插入消息
     */
    void insertTxMessage(MessageCenterDTO messageCenterDTO);

    /**
     * 获取极光的appkey，secret和用户极光id
     * @return
     */
    Map<String,String> getJgInfoAndUser(@Param("user_id") Integer user_id);

    /**
     * 获取最近提现的支付宝账号
     */
    String getTxUserInfo(@Param("user_id") Integer user_id);
}
