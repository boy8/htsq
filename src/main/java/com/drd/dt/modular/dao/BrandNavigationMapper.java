package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.modular.dto.BrandNavigationDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface BrandNavigationMapper {

    /**
     * 品牌导航-获取品牌信息
     * @param type
     * @return
     */
    List<BrandNavigationDTO> selectBrandsInfo(@Param("type") Integer type);

    /**
     * 品牌导航-获取旗舰店分页信息
     * @param type
     * @return
     */
    List<BrandNavigationDTO> selectStoresInfo(@Param("page") Page page,@Param("type") Integer type);
}
