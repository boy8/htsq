package com.drd.dt.modular.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.modular.dto.BusinessAcademyDTO;
import com.drd.dt.modular.dto.DailyGoodsDTO;
import com.drd.dt.modular.entity.BusinessAcademy;
import org.apache.ibatis.annotations.Param;

public interface BusinessAcademyMapper {

    List<BusinessAcademy> friendBusinessAcademym(@Param("page") Page page, BusinessAcademyDTO map);

    List<BusinessAcademy> friendBusinessAcademymBriefness(@Param("page") Page page, BusinessAcademyDTO map);

    BusinessAcademy getCommodity(@Param("id")int id);

    int synchronizationAccumulativeTotal(BusinessAcademyDTO map);

    List<String> getSecondModule(@Param("first_module") int first_module);

    List<String> getFirstModule();

    List<String> getThirdModule(@Param("first_module") int first_module,@Param("second_module")int second_module);

    void insertDailyGoods(List data);

    /**
     * 每日爆款商品列表
     **/
    List<DailyGoodsDTO> dailyGoods(Page page,@Param("category") String category);

    List<DailyGoodsDTO> getByGoodsId(@Param("goods_id") String goods_id);

    void updateDailyGoodsStatus(@Param("goodsId") String goodsId);
}
