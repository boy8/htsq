package com.drd.dt.modular.dao;

import org.apache.ibatis.annotations.Param;

/***
 * 邀请赚-分享日子记录
 * */
public interface JoinInviteShareLogMapper {
    void addLog(@Param("user_id") Integer user_id);
}
