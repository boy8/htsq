package com.drd.dt.modular.dao;


import com.drd.dt.modular.dto.OwnGoodsOrderDTO;
import com.drd.dt.modular.entity.OrderGift;
import com.drd.dt.modular.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 推荐(礼包)订单
 */
public interface OrderGiftMapper {

    //添加订单信息
    int addOrderGift(OwnGoodsOrderDTO ownGoodsOrderDTO);

    //修改订单状态
    int upOrderGift(Map<String, Object> map);


    // 根据 订单号金额查询订单
    int finOrDerGift(@Param("order_id") String order_id);

    //查询订单详情
    OrderGift getOrderGiftDetail(@org.apache.ibatis.annotations.Param("order_id") String order_id);

    //根据订单号 关联查询 User
    User getUserDetail(@Param("order_id") String order_id);

    //根据订单号 关联 查询自营商品 库存-1 售卖+1
    int upInventorySell(@Param("goods_id") String goods_id);

    //用户订单查询详情
    OrderGift userOrderGiftDetail(@Param("order_id") String order_id);


}
