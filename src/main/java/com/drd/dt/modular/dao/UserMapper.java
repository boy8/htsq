package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.modular.dto.*;
import com.drd.dt.modular.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 用户操作 公共
 **/
public interface UserMapper extends BaseMapper<User> {

    User getByOpenId(Map<String, String> map);

    User queryUseridByTaobaoid(Map<String, Object> params);

    User getUserIdByRidSid(Map map);
    User getUserIdByspecial_id(Map map);

    String getUserLevel(@Param("id")int id);

    Map getThisFans(@Param("user_id")int user_id);

    /**
     * 获取个人中心下级所有粉丝
     */
    List<PersonalCenterDTO> selectNextFans(@Param("friend_code") Integer friend_code);

    /**
     * 获取上级邀请人信息
     */
    PersonalCenterDTO getPreAndMyInfo(@Param("introducer") String introducer);

    /**
     * 用户获取自营订单
     */
    List<PersonalOrderDTO> selectOwnOrder(@Param("page") Page page,@Param("user_id") Integer user_id,
                                          @Param("status") Integer status);


    /**
     * 用户获取核桃精选订单
     */
    List<PersonalOrderDTO> selectSiftOrder(@Param("page") Page page,@Param("user_id") Integer user_id,
                                          @Param("status") Integer status);


    /**
     * 用户获取三方订单
     */
    List<PersonalOrderDTO> selectThirdOrder(@Param("page") Page page,@Param("user_id") Integer user_id,
                                            @Param("status") Integer status,@Param("type") Integer type);

    /**
     * 获取龙虎榜（排行）所有下级粉丝
     */
    List<PersonalCenterDTO> selectRankNextFans(@Param("friend_code") Integer friend_code);

    /**
     * 获取用户信息
     */
    UserInfoDTO getUserInfo(@Param("user_id") Integer user_id);
    int getUserStatus(@Param("user_id") Integer user_id);

    /**
     * 获取用户今日预估佣金、奖励金
     */
    UserInfoDTO getUserTodayEstimate(@Param("user_id") Integer user_id);
    UserInfoDTO getUserTodayEstimatePromote(@Param("user_id") Integer user_id);//内部添加 核桃精选订单金额


    /**
     * 获取粉丝详情
     */
    FansInfoDTO getMyFansInfo(@Param("fans_id") Integer fans_id);

    /**
     * 获取累计和上月预估收益
     */
    FansInfoDTO getPreMonthEstimate(@Param("fans_id") Integer fans_id,@Param("preMonthFirstDay") String preMonthFirstDay,
                                    @Param("preMonthLastDay") String preMonthLastDay);

    /**
     * 获取个人中心banner图
     */
    List<Map> getPersonalBanner();

    /**
     * 获取邀请好友的图片
     */
    List<String> selectInviteImg();

    /**
     * 生成礼包订单  地址校验  修改用户地址
     * */
    int updateUserAddr(User user);

    /**
     * 我的收益信息
     */
    MyProfitDTO getMyProfitInfo(@Param("user_id") Integer user_id);

    /**
     * 我的收益信息（新）
     */
    MyProfitNewDTO getMyHistoryProfit(Map<String, Object> param);
    /**
     *  我的收益信息（新）  补- 核桃精选信息
     */
    MyProfitNewDTO getMyHistoryProfitSift(Map<String, Object> param);

    /**
     * 获取不同时间的预估收益
     */
    MyProfitDTO getHistoryProfit(Map param);

    /**
     * 个人中心-常见问题-常见问题
     */
    List<Map> selectNormalProblem();

    /**
     * 个人中心-常见问题-问题分类 列表
     */
    List<Map> problemType();

    /**
     * 个人中心-常见问题-问题分类 答案
     */
    List<Map> problemList(@Param("id") Integer id);

    /**
     * 个人中心-隐私政策
     **/
    Map getPrivacyPolicy();

    /**
     * 粉丝订单-普通订单
     */
    List<PersonalOrderDTO> selectFansOrder(@Param("page") Page page,@Param("user_id") Integer user_id,
                                           @Param("status") Integer status);

    /**
     * 礼包订单
     */
    List<PersonalOrderDTO> selectGiftOrder(@Param("page") Page page,@Param("user_id") Integer user_id);
    /**
     * 核桃精选粉丝订单
     */
    List<PersonalOrderDTO> selectSiftFansRakeOrder(@Param("page") Page page,@Param("user_id") Integer user_id);

    /**
     * 获取ios审核版本号
     */
    String getVerifyVersion();

    /**
     * 获取用户粉丝数量
     */
    Map getUserFansNum(@Param("user_id") Integer user_id);

    /**
     * 获取分页的粉丝信息
     */
    List<PersonalCenterDTO> selectPageFansInfo(@Param("param") List param);

    /**
     * 获取分页的粉丝龙虎榜信息
     */
    List<PersonalCenterDTO> selectPageRankFansInfo(@Param("param") List param);

    /**
     * 分页获取直属粉丝(龙虎榜)
     */
    List<PersonalCenterDTO> selectPageRankNextFans(@Param("page") Page page,@Param("friend_code") Integer friend_code);

    /**
     * 分页获取全部粉丝(龙虎榜)
     */
    List<PersonalCenterDTO> selectPageAllFans(@Param("page") Page page,@Param("user_id") Integer user_id);

    /**
     * 获取下级所有粉丝id
     */
    List<Integer> selectRankNextIds(@Param("friend_code") Integer friend_code);

    /**
     * 分页获取间接粉丝(龙虎榜)
     */
    List<PersonalCenterDTO> selectPageRankNormalFans(@Param("page") Page page,@Param("user_id") Integer user_id,
                                                     @Param("param") List<Integer> param);

    /**
     * 分页获取全部粉丝(我的粉丝)
     */
    List<PersonalCenterDTO> selectPageAllMyFans(@Param("page") Page page,@Param("user_id") Integer user_id,
                                                @Param("time") String time,@Param("fans_num") String fans_num,
                                                @Param("is_vip") String is_vip,@Param("active") String active);

    /**
     * 分页获取直属粉丝(我的粉丝)
     */
    List<PersonalCenterDTO> selectPageDirectMyFans(@Param("page") Page page,@Param("friend_code") Integer friend_code,
                                                   @Param("time") String time,@Param("fans_num") String fans_num,
                                                   @Param("is_vip") String is_vip,@Param("active") String active);

    /**
     * 分页获取间接粉丝(我的粉丝)
     */
    List<PersonalCenterDTO> selectPageNormalFans(@Param("page") Page page,@Param("user_id") Integer user_id,
                                                 @Param("param") List<Integer> param,@Param("time") String time,
                                                 @Param("fans_num") String fans_num,@Param("is_vip") String is_vip,
                                                 @Param("active") String active);


    /***
     * 根据手机号码查询 用户信息
     */
    User selectPhone(@Param("phone")String phone);
    /***
     * 根据UserId查询 用户信息
     */
    User selectUserId(@Param("user_id")Integer user_id);
    User clientUserId(@Param("user_id")Integer user_id);

    /***************信用卡 用到以下方法*************************************/
    /**
     * 修改jz_user_profit 用户收益 直接返佣到 余额
     * */
    int updateUserProfit(@Param("userId")Integer userId,@Param("money")Double money);
    /**
     * 新增jz_balance_detail 用户收益明细
     * */
    int addBalanceDetail(@Param("userId")Integer userId,@Param("money")Double money);


    /****************邀新活动********************/
    /**
     * 查询用户订单状态以及用户昵称头像
     * */
    List<Map> selectFansFirstOrde(List<Map> list);

    /**
     * 查询用户订单状态以及用户昵称头像
     * */
    Map selectFansFirstSiftOrde(@Param("user_id")Integer user_id);

    /**
     * 查询用户订单结算并且 返红宝书
     * */
    Map selectFansFirstOrdeMessageMoney(List<Map> list,@Param("user_id")Integer user_id);


    /**
     * 查询用户参加活动之后 邀请的直接粉丝
     * */
    List<Map> invitedToEarnFansUserIdS(@Param("user_id")Integer user_id,@Param("time")String time);

    /**
     * 查询用户 邀请赚钱 message_type = 6 佣金变更信息
     * */
    double fansInvitedToEarnMoney(@Param("user_id")Integer user_id);

    /*****************核桃精选***********************/
    void consumeSiftGoodsAdv(@Param("user_id")Integer user_id);

    String getAndroidVerifyVersion();

    void saveScanInfo(@Param("user_id") Integer user_id,@Param("union_id") String union_id,@Param("time") String time);

    Integer getScanInfo(@Param("user_id") Integer user_id,@Param("union_id") String union_id);

    ScanInfoDTO geUserScanInfo(@Param("wx_unionid") String wx_unionid);

    String getByAlipayCount(@Param("user_id") Integer user_id);

    /*******************自动降级 2020-12-15 新增方法*******************************/
    //检测 会员是否已经到期
    void detectionLvExpireTime(@Param("id") Integer user_id);
}
