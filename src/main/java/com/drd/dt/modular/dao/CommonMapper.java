package com.drd.dt.modular.dao;

import com.drd.dt.modular.entity.ImageConfig;
import com.drd.dt.modular.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface CommonMapper {


    /**
     *获取返佣比例
     * @return
     */
    Map getConfigInfo();

    /**
     * 获取订单信息
     * @param order_id
     * @return
     */
    Order getOrderInfo(@Param("order_id") String order_id);

    /**
     * 更新订单状态
     * @param map
     */
    void updateOrder(Map map);

    /**
     * 获取上级（推荐人）
     * @param user_id
     * @return
     */
    Integer getIntroducer(@Param("user_id") Integer user_id);

    /**
     *获取上上级
     * @param introducer
     * @return
     */
    Integer getTwoLvIntroducer(@Param("introducer") Integer introducer);

    /**
     * 获取默认的头像图片
     * @return
     */
    ImageConfig getDefaultImg();

    /*
    * 获取短信类型
    * */
    Integer getSMSType();

    /**
     * 查询是否是特殊邀请码
     */
    Integer isSpecialCode(@Param("vCode") String vCode);
}
