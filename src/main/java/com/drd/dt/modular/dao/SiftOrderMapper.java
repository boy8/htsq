package com.drd.dt.modular.dao;

import com.drd.dt.back.dto.SiftOrderDTO;
import com.drd.dt.common.tips.Tip;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SiftOrderMapper {

    /**
     * 根据用户ID 查询订单总消费金额
     * **/
    Double getUserSiftOrderSumMoney(@Param("user_id")Integer user_id);

    /***
     * 根据用户ID 订单order_id 查询用户
     * */
    SiftOrderDTO getUserSiftOrderDateil(@Param("user_id")Integer user_id,@Param("order_id")String order_id);


    /***
     * 根据用户ID 订单order_id 查询用户
     * */
    List<SiftOrderDTO> getUserSiftOrderList(@Param("user_id")Integer user_id,@Param("refund_status")Integer refund_status);


    /***
     * 根据订单ID查询订单信息
     * */
    SiftOrderDTO siftOrderDetail(@Param("order_id")String order_id);

    //获取订单 id列表
    List<String> orderIdList(@Param("user_id")Integer user_id);

    /***
     * 根据商品id查询订单信息
     * */
    List<SiftOrderDTO> siftOrderDetailGoodsid(@Param("product_id")String product_id,@Param("user_id")Integer user_id);



}
