package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.drd.dt.back.dto.SiftOrderFansRakeDTO;
import com.drd.dt.modular.entity.*;

import java.util.List;
import java.util.Map;

/**
 * 用户收益相关操作
 */
public interface UserMoneyMapper extends BaseMapper<UserMoney> {
   UserMoney getByUserMoneyId(Map map);
    User getByUserId(Map map);

//    void insertOrder(Order order);
   // void insertOrderGift(OrderGiftMapper order);
    void insertFansRake(OrderFansRake fans);//插入一条粉丝返佣记录
    void insertSiftOrderFansRake(SiftOrderFansRakeDTO fans);//插入一条粉丝返佣记录 - 核桃精选粉丝订单
    void updateFansRake(Map map);//更新粉丝返佣订单状态
    void insertGiftRake(OrderGiftRake gift);//插入礼包返佣记录
    //void updateGiftRake(Map map);//更新礼
    void updateUserMoney(UserMoney money);//更新用户各种收益金额
    void insertMessageMoney(MessageMoney message);//插入一条佣金变动消息
    long countUserVipNumByLevel(User user);//查询此邀请人下边等级为level的用户数量
    void updateUserLevel(User user);//调整用户级别
    void updateUserLevelAndTime(User user);//调整用户级别和会员时间
     List<OrderFansRake> queryFansRakeByOrderId(Map map); //根据订单ID，查询满足条件的记录
    List<SiftOrderFansRakeDTO> querySiftOrderFansRakeByOrderId(Map map); //根据订单ID，查询满足条件的记录 - 核桃精选粉丝订单
  int insertBalanceDetailNew(Map map);//插入一条balance明细记录
    void insertSYSMessage(Map map);//插入一条系统 消息
     long checkRegisterMsg(Map map); //检查是否已经返佣过
    void insertMessageMoney2(MessageMoney message);//邀请赚-使用- 其他地方不可使用 - 添加
    int ifInsertMessageMoney2(MessageMoney message);//邀请赚-使用- 其他地方不可使用 - 查询
}
