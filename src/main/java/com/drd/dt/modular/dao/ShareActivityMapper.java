package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.drd.dt.modular.dto.ShareActivityDTO;
import com.drd.dt.modular.entity.ShareActivity;
import com.drd.dt.modular.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface ShareActivityMapper extends BaseMapper<ShareActivity> {

    /**
     * 获取当前用户最后一个分享拆红包活动
     */
    ShareActivity getMyRedPacketInfo(@Param("user_id") Integer user_id);

    /**
     * 获取活动期间邀请的人
     */
    List<User> getInvitePeople(@Param("user_id") Integer user_id, @Param("start_time") String start_time,
                               @Param("expire_time") String expire_time);

    /**
     * 插入balance表记录
     */
    void insertBalanceDetail(Map param);

    /**
     * 插入MessageMoney消息表记录
     */
    void insertMessageMoney(Map param);

    /**
     * 获取用户活动金额信息
     */
    ShareActivityDTO getUserProfitInfo(@Param("user_id") Integer user_id);

    /**
     * 更新用户活动佣金
     */
    void updateUserActivityBalance(Map param);

    /**
     * 更新用户活动佣金
     */
    void insertUserActivityBalance(Map param);
}
