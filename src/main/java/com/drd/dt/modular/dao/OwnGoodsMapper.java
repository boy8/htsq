package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.entity.GoodsSpecification;
import com.drd.dt.back.entity.OwnGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OwnGoodsMapper {

    /**
     * 所有商品 出去状态禁用 和 即将上线
     * */
    List<OwnGoods> getOwnGoodsNoTypeList(@Param("page") Page page);

    // 商品列表-分页
    List<OwnGoods> getOwnGoodsList(@Param("page") Page page, Map<String, Object> map);

    /**
     * 所有商品 出去状态禁用 和 即将上线
     * */
    List<OwnGoods> getOwnGoodsListNoPage();

    // 商品详情
    OwnGoods getOwnGoodsDetails(@Param("dealer_id") int dealer_id);

    //获取规格
    List<GoodsSpecification> getGoodsSpecificationDetails(@Param("id") int id);

    // 查询订单是否还可以购买

    //商品 类型 分组查询
    List<Map>  groupingType();
}
