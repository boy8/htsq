package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.modular.dto.*;
import com.drd.dt.modular.entity.ImageConfig;
import com.drd.dt.modular.entity.TaskCoin;
import com.drd.dt.modular.entity.TaskInfo;
import com.drd.dt.modular.entity.TaskList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface TaskMapper extends BaseMapper<TaskInfo> {

    /**
     * 获取任务中心个人信息
     */
    TaskUserInfoDTO getTaskUserInfo(@Param("user_id") Integer user_id);

    /**
     * 获取用户基本任务列表
     */
    List<TaskList> selectUserBaseTaskList(@Param("user_id") Integer user_id,@Param("level_coin") String level_coin);

    /**
     * 获取用户每日任务列表
     */
    List<TaskList> selectUserDailyTaskList(@Param("user_id") Integer user_id,@Param("level_coin") String level_coin);

    void insertBaseTask(List<TaskList> base);

    void insertDailyTask(List<TaskList> daily);

    /**
     * 获取任务中心默认配置图片
     */
    List<ImageConfig> selectTaskDefaultPic();

    /**
     * 获取用户基础任务详情
     */
    TaskList getUserBaseTaskDetail(@Param("user_id") Integer user_id,@Param("task_id") String task_id);

    /**
     * 获取用户每日任务详情
     */
    TaskList getUserDailyTaskDetail(@Param("user_id") Integer user_id,@Param("task_id") String task_id);

    /**
     * 更新基础任务完成次数
     */
    void updateUserBaseTask(@Param("user_id") Integer user_id,@Param("task_id") String task_id,@Param("status") Integer status);

    /**
     * 更新每日任务完成次数
     */
    void updateUserDailyTask(@Param("user_id") Integer user_id,@Param("task_id") String task_id,@Param("status") Integer status);

    /**
     * 更新用户金币数量
     */
    void updateUserCoin(@Param("user_id") Integer user_id,@Param("gold_coin") Integer gold_coin,@Param("coin_time") String coin_time);

    /**
     * 插入任务金币明细表
     */
    void insertTaskDetail(List<TaskCoin> coinList);

    /**
     * 查询用户的基本任务详情
     */
    TaskListDTO getBaseTaskDetail(@Param("user_id") Integer user_id,@Param("task_id") Integer task_id);

    /**
     * 查询用户的每日任务详情
     */
    TaskListDTO getDailyTaskDetail(@Param("user_id") Integer user_id,@Param("task_id") Integer task_id);

    /**
     * 减去用户金币
     */
    void updateUserDesCoin(@Param("coin") Integer coin,@Param("user_id") Integer user_id);

    /**
     * 获取用户首单信息
     */
    Integer getUserOrderInfo(@Param("user_id") Integer user_id,@Param("time") String time);

    List<Integer> getTodayUserFans(@Param("user_id") Integer user_id, @Param("time") String time);

    /**
     * 用户金币兑换历史记录列表
     **/
    List<TaskExgHistoryDTO> selectExchangeHistory(Page<TaskExgHistoryDTO> page, @Param("user_id") Integer user_id, @Param("type") Integer type);

    /**
     * 获取用户历史金币信息
     */
    TaskExgCoinDTO getExgHistoryCoin(@Param("user_id") Integer user_id);

    Integer getOwnOrderInfo(@Param("user_id") Integer user_id,@Param("time") String time);

    List<Integer> getFinishBaseTaskUser(@Param("user_id") Integer user_id);

    /**
     * 粉丝金币贡献列表记录
     **/
    List<TaskFansContributionDTO> selectUserFansContributionList(Page<TaskFansContributionDTO> page, @Param("user_id") Integer user_id,
                                                                 @Param("time") String time);

    List<Integer> selectUserDirectFans(@Param("user_id") Integer user_id);
}
