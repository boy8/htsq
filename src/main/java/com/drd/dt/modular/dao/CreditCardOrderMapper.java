package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.modular.dto.CreditCardOrder;
import com.drd.dt.modular.dto.CreditCardOrderDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CreditCardOrderMapper {


    //新增 信用卡订单
    int insertCreditCardOrder(Map<String, Object> map);

    //查询订单 参数：	客户号(custNo)、合作伙伴客户号(supplierCustNo)、创建时间戳(createDatetime)
    CreditCardOrder selectAll(Map<String, Object> map);

    //更新订单状态
    int updateStatue(@Param("supplierCustNo") String supplierCustNo, @Param("id") String id, @Param("applyStatus") String applyStatus, @Param("updateDateTime") String updateDateTime);

    //查询订单列表
    List<CreditCardOrderDTO> selectList(@Param("page") Page page,Map<String,String > map);

    //查询订单详情 联合 jz_order
    CreditCardOrder finDetail(@Param("id") Integer id);
}
