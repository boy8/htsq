package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dto.MessageCenterDTO;
import com.drd.dt.modular.dto.ActivityDTO;
import com.drd.dt.modular.dto.MessageDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface MessageMapper {

    /**
     * 佣金消息通知
     */
    List<MessageDTO> selectCommissionMessage(@Param("page") Page page, @Param("param")List param,@Param("user_id") Integer user_id);

    /**
     * 活动消息通知
     */
    List<MessageDTO> selectActivityMessage(@Param("page") Page page);

    /**
     * 系统消息通知（提现，注册等）
     */
    List<MessageDTO> selectSystemMessage(@Param("page") Page page,@Param("user_id") Integer user_id);

    /**
     * 获取用户最近一条佣金消息内容
     */
    MessageDTO getCommissionMessageFor1(@Param("user_id") Integer user_id);

    /**
     * 获取用户最近一条系统消息和活动消息内容
     */
    List<MessageDTO> getMessageInfo(@Param("user_id") Integer user_id);

    /**
     * 获取消息是否已读
     */
    List<MessageDTO> getReadStatus(@Param("user_id") Integer user_id);

    /**
     * 更新佣金通知消息读取状态
     */
    void updateCommissionMsgStatus(@Param("user_id") Integer user_id);

    /**
     * 更新提现通知消息读取状态
     */
    void updateTXMsgStatus(@Param("user_id") Integer user_id);

    /**
     * 更新系统通知消息读取状态
     */
    void updateSYSMsgStatus(@Param("user_id") Integer user_id);

    /**
     * 新增提现消息
     */
    void insertTxMessage(MessageCenterDTO messageCenterDTO);

    /**
     * 新增系统消息-注册
     */
    void insertSYSMessage(MessageCenterDTO messageCenterDTO);
}
