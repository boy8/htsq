package com.drd.dt.modular.dao;


import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface GoldCoinMapper {

    /**
     * 更新用户金币数
     * @param param
     */
    void updateUserCoin(Map param);

    /**
     * 获取金币时间和数量，判断今天是否已获得金币
     * @param userId
     * @return
     */
    Map<String,String> getCoin(@Param("userId") Integer userId);

    /**
     * 插入金币明细表
     * @param param
     */
    void insertCoinDetail(Map param);

    /**
     * 获取金币时间和数量,最近兑换金币时间
     * @return
     */
    Map<String,String> getCoinInfo(@Param("user_id") Integer user_id);

    /**
     * 更新用户金币
     * @param param
     */
    void updateUserRedpacket(Map param);

    /**
     * 插入余额明细表
     * @param param
     */
    void insertRedpacketDetail(Map param);

    /**
     * 插入红包表
     * @param param
     */
    void insertRedpacket(Map param);

    /**
     * 获取用户最近兑换且未过期的红包
     * @return
     */
    Map<String,Integer> getLatestOneRedpacket(@Param("user_id") String user_id);

    /**
     * 修改红包使用状态
     * @param id
     */
    void updateRedpacketStatus(@Param("id") Integer id);

    /**
     * 通过订单id在订单表获取红包信息
     * @param order_id
     * @return
     */
    Map<String,String> getRedPackageInfoByOrderId(@Param("order_id") String order_id);

    /**
     * 将红包加入用户余额
     * @param param
     */
    void updateUserBalance(Map param);



    /**
     * 获取返利红包信息
     */
    List<Map> selectRebateRedpacketInfo(Map param);

    Map getRPInfoFBanlanceDetail(@Param("order_id") String order_id);
}
