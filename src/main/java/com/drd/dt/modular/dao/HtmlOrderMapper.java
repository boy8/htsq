package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.entity.GoodsSpecification;
import com.drd.dt.back.entity.OwnGoods;
import com.drd.dt.modular.dto.OwnGoodsOrderDTO;
import com.drd.dt.modular.entity.HtmlOrderGift;
import com.drd.dt.modular.entity.OrderGift;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HtmlOrderMapper {
    //商品列表 不分页
    List<OwnGoods> goodsListNoPage();
    //分页
    List<OwnGoods> goodsCouponsListNoPage(@Param("page") Page page, @Param("coupons_price") Integer coupons_price);

    //商品详情
    OwnGoods goodsDetails(@Param("dealer_id") int dealer_id);

    //用户订单查询详情
    HtmlOrderGift userOrderGiftDetail(@Param("order_id") String order_id);

    //查询商品规格信息
    List<GoodsSpecification>  getGoodsSpecification(@Param("ids")List<Long> ids);
    //获取规格
    List<GoodsSpecification> getGoodsSpecificationDetails(@Param("id") int id);

    //查询订单详情
    OrderGift orderGiftDetail(@Param("order_id") String order_id);

    //添加订单信息
    int addOrderGift(OwnGoodsOrderDTO ownGoodsOrderDTO);

    // 根据 订单号金额查询订单
    int finOrDerGift(@Param("order_id") String order_id);

    //修改订单状态
    int upOrderGift(Map<String, Object> map);

    //根据订单号 关联 查询自营商品 库存-1 售卖+1
    int upInventorySell(@Param("goods_id") String goods_id);

}
