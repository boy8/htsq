package com.drd.dt.modular.dao;

import com.drd.dt.modular.dto.ZeroPurchaseDTO;
import com.drd.dt.modular.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OrderMapper {
    /**
     * 更新状态为3的订单
     * @param map
     */
    void updateOrder(Map map);

    List<ZeroPurchaseDTO> getZeroPurchases();

    void insertUserZeroOrder(@Param("user_id") int user_id,@Param("order_id") String order_id,@Param("type") int type);//用户0元购记录

    long queryUserZeroNum(@Param("user_id") int user_id);//根据用户ID，查询该用户0 元购订单数量

    /**
     * 更新订单状态
     */
    void updateOrderStatus(Map map);

    /**
     * 更新状态为失效
     */
    void updateStatus(@Param("order_id") String order_id);

    /**
     * 获取订单
     */
    Order getRepeatOrder(@Param("order_id") String order_id);

    void insert(Order createOrder);
    /**
     *  存储过程 ：分表插入数据 根据月分  表名+年月
     * */
    void insertYM(Order createOrder);

    /**
     * 获取唯品会联盟跳转链接
     * @return
     */
    String getWphLink();

    /**
     * 查询京东订单 创建时间
     * */
    List<String> getJDOrderTime(@Param("time")String time);

    /**
     * 更新订单预收差价
     */
    void updateOrderRakeback(Map<String, Object> map);
}
