package com.drd.dt.modular.dao;


import com.drd.dt.back.dto.SiftOrderLogDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * SiftOrderLog 精选核桃 商品 订单 售后记录表表 DAO
 */
public interface ISiftOrderLogMapper {

    //根据订单ID查询记录
    List<Map> siftOrderLogList(@Param("ids") List<String> orderId);

    /**
     * 修改 补全快递信息
     */
    int userParcel(@Param("parcel_number") String parcel_number, @Param("parcel_type") String parcel_type, @Param("id") Integer id);

    //查询退款申请中
    List<Map> getLog(@Param("order_id")String  order_id);
}
