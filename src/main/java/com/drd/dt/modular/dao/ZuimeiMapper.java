package com.drd.dt.modular.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.modular.dto.TLJHistoryDTO;
import com.drd.dt.modular.dto.TLJListInfoDTO;
import com.drd.dt.modular.dto.ZuimeiGoodsDTO;
import com.drd.dt.modular.dto.ZuimeiUserInfoDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 86514 on 2019/4/12.
 */
public interface ZuimeiMapper extends BaseMapper<ZuimeiGoodsDTO> {

    List<ZuimeiGoodsDTO> selectZuimeiList(Page page);

    /**
     * 获取用户与最美天气用户是否存在
     */
    ZuimeiUserInfoDTO getZuimeiBalance(@Param("zuimei_user_id") Integer zuimei_user_id);
    /**
     * 获取用户与最美天气用户是否存在-根据user_id 查询
     */
    long getZuimeiBalanceUserid(@Param("user_id") Integer user_id);

    /**
     * 新增最美用户兑换余额
     */
    Integer insertZuimeiBalance(ZuimeiUserInfoDTO zuimeiUserInfo);

    /**
     * 更新最美用户兑换余额
     */
    Integer updateZuimeiBalance(ZuimeiUserInfoDTO zuimeiUserInfo);

    /**
     * 新增最美兑换明细
     */
    void insertZuimeiDetail(ZuimeiUserInfoDTO zeimeidto);

    /**
     * 获取最美用户信息
     */
    ZuimeiUserInfoDTO getZuimeiUserInfo(@Param("user_id") Integer user_id);

    /**
     * 获取最美用户兑换历史
     */
    List<ZuimeiUserInfoDTO> selectHistoryList(Page page,@Param("zuimei_user_id") Integer zuimei_user_id,@Param("type") String type);

    /**
     * 扣除最美用户券金额
     */
    void DecreaseZuimeiBalance(@Param("user_id") Integer user_id,@Param("money") String money);

    /**
     * 绑定最美用户和核桃用户
     */
    void bindUserId(ZuimeiUserInfoDTO zuimeiBalance);

    /**
     * 通过最美用户id获取信息
     */
    ZuimeiUserInfoDTO getZuimeiUserInfoByZuimeiId(@Param("zuimei_user_id") Integer zuimei_user_id);

    /**
     * 现金券兑换淘礼金记录
     **/
    List<TLJHistoryDTO> selectTLJHistory(Page page,@Param("user_id") Integer user_id);

    /**
     * 淘礼金的换列表
     */
    List<TLJListInfoDTO> selectTLJList(@Param("time") String time);

    /**
     * 更新淘礼金使用状态
     */
    void updateTLJStatus(@Param("id") Integer id,@Param("status") Integer status);

    /**
     * 返回券额到用户账户
     */
    void IncreaseZuimeiBalance(@Param("user_id") Integer user_id,@Param("money") Double money);

    /**
     * 查看用户是否兑换商品的淘礼金
     */
    Integer getUserTLJInfo(@Param("user_id") Integer user_id,@Param("goods_id") String goods_id);

    /**
     *设置淘礼金使用状态
     */
    void updateTLJUseStatus(@Param("id") Integer id);
}
