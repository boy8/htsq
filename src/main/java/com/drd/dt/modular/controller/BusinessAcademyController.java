package com.drd.dt.modular.controller;


import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.BusinessAcademyDTO;
import com.drd.dt.modular.service.BusinessAcademyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 商学院
 * Created by 86514 on 2019/7/28.
 */
@Api(tags = "商学院", description = "商学院接口")
@RestController
@RequestMapping("/api/v2/htsq/businessAcademy")
public class BusinessAcademyController {
    @Autowired
    private BusinessAcademyService businessAcademyService;

    /**
     * 查询(分页) 每日爆款/宣传素材/商学院 内容列表
     *
     * @param page          当前页数
     * @param pageSize      每页最大数
     * @param first_module  一级分类：1每日爆款 2宣传素材 3商学院
     * @param second_module 二级分类    -1代表全部
     * @param third_module  三级分类    -1代表全部
     * @param state         1 启用 0禁用
     * @param title         商学院-搜索文章 标题 模糊查询
     *                      title 非必传   其他必传
     */
    @ApiOperation(value = " 每日爆款/宣传素材/商学院", notes = "每日爆款/宣传素材/商学院 内容列表", response = HttpMessage.class)
    @PostMapping("/businessAcademym")
    public Tip friendBusinessAcademym(@RequestParam(value = "page") Integer page,
                                      @RequestParam(value = "pageSize") int pageSize,
                                      @RequestParam(value = "first_module") int first_module,
                                      @RequestParam(value = "second_module") int second_module,
                                      @RequestParam(value = "third_module") int third_module,
                                      @RequestParam(value = "state") int state,
                                      @RequestParam(value = "title", required = false) String title
    ) throws Exception {
        BusinessAcademyDTO map = new BusinessAcademyDTO();
        map.setPageIndex(page);
        map.setPageSize(pageSize);
        map.setFirst_module(first_module);
        map.setSecond_module(second_module);
        map.setThird_module(third_module);
        map.setState(state);
        map.setTitle(title);

        return businessAcademyService.friendBusinessAcademym(map);
    }

    /**
     * 每日爆款分类
     **/
    @ApiOperation(value = "每日爆款分类", notes = "每日爆款分类", response = HttpMessage.class)
    @PostMapping("/dailyCategory")
    public Tip dailyCategory() throws Exception{
        Tip data = businessAcademyService.dailyCategory();
        return data;
    }

    /**
     * 每日爆款商品列表
     **/
    @ApiOperation(value = "每日爆款商品列表", notes = "每日爆款商品列表", response = HttpMessage.class)
    @PostMapping("/dailyGoods")
    public Tip dailyGoods(@ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                          @ApiParam(value = "类别（1美妆 2母婴 3服饰 4数码 5食品 6其他）") @RequestParam(value = "category",required = false) String category,
                          @ApiParam(value = "用户id") @RequestParam(value = "user_id",required = false) Integer user_id) throws Exception{
        Tip data = businessAcademyService.dailyGoods(page,category,user_id);
        return data;
    }

    /**
     * 每日爆款商品拉取
     **/
    @ApiOperation(value = "每日爆款商品拉取", notes = "每日爆款商品拉取", response = HttpMessage.class)
    @GetMapping("/insertDailyGoods")
    public Tip insertDailyGoods(@ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                         @ApiParam(value = "时间") @RequestParam(value = "time",required = false) String time) throws Exception{
        Tip data = businessAcademyService.insertDailyGoods(page,time);
        return data;
    }

    /**
     * 每日爆款商品-复制评论
     **/
    @ApiOperation(value = "每日爆款商品-复制评论", notes = "每日爆款商品-复制评论", response = HttpMessage.class)
    @PostMapping("/copyComment")
    public Tip copyComment(@ApiParam(value = "类型(1淘宝 2京东 3拼多多 4唯品会)") @RequestParam(value = "type",required = false) Integer type,
                           @ApiParam(value = "商品id") @RequestParam(value = "goods_id") String goods_id,
                           @ApiParam(value = "商品图片") @RequestParam(value = "goods_pic") String goods_pic,
                           @ApiParam(value = "商品名称") @RequestParam(value = "goods_name") String goods_name,
                           @ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                           @ApiParam(value = "是否有券(1是 0否)") @RequestParam(value = "ifCoupon") String ifCoupon,
                           @ApiParam(value = "原价") @RequestParam(value = "price") String price,
                           @ApiParam(value = "券后价") @RequestParam(value = "after_coupon_price") String after_coupon_price
    ) throws Exception{
        Tip data = businessAcademyService.copyComment(goods_id,user_id,ifCoupon,goods_pic,goods_name,price,after_coupon_price);
        return data;
    }

    /**
     * 获取详情 所需参数并 判断核对权限
     *
     * @param user_id     用户ID
     * @param business_id 主键ID
     * @param
     */
    @ApiOperation(value = "获取详情 所需参数并 判断核对权限", notes = "获取详情 所需参数并 判断核对权限", response = HttpMessage.class)
    @PostMapping("/ifUserLevel")
    public Tip ifUserLevel(@ApiParam(value = "用户ID") @RequestParam(value = "user_id") int user_id,
                           @ApiParam(value = "主键ID") @RequestParam(value = "business_id") int business_id
    ) throws Exception {
        BusinessAcademyDTO map = new BusinessAcademyDTO();
        map.setUser_id(user_id);
        map.setBusiness_id(business_id);

        return businessAcademyService.ifUserLevel(map);
    }

    /**
     * 点赞、转发、评论、阅读 累计
     *
     * @param addTypeNum  当前累加字段名称 ：like_num((点赞数)、transpond_num(转发数)、comment_num(评论数)、show_num(阅读数)
     * @param business_id 主键iD
     */
    @ApiOperation(value = " 点赞、转发、评论、阅读 累计", notes = "每次相关操作累计+1", response = HttpMessage.class)
    @PostMapping("/synAccumulativeTotal")
    public Tip synchronizationAccumulativeTotal(@ApiParam(value = "当前累加字段名称") @RequestParam(value = "addTypeNum") String addTypeNum,
                                                @ApiParam(value = "主键iD") @RequestParam(value = "business_id") int business_id
    ) throws Exception {
        BusinessAcademyDTO map = new BusinessAcademyDTO();
        map.setBusiness_id(business_id);
        map.setAddTypeNum(addTypeNum);
        return businessAcademyService.synchronizationAccumulativeTotal(map);
    }

    /**
     * 根据模块名称返回对应关键词分组
     *
     * @param first_module  一级分类
     * @param second_module 二级分类
     */
    @ApiOperation(value = " 模块对分组", notes = "根据模块名称返回对应关键词分组", response = HttpMessage.class)
    @PostMapping("/getModelCorrelationGrouping")
    public Tip getModelCorrelationGrouping(@RequestParam(name = "first_module", defaultValue = "1", required = false) int first_module,
                                           @RequestParam(name = "second_module", defaultValue = "1", required = false) int second_module,
                                           @RequestParam(name = "deviceT",required = false) int deviceT,
                                           @RequestParam(name = "app_version", required = false) String app_version,HttpServletRequest request
                                           ) throws Exception {
        return businessAcademyService.getModelCorrelationGrouping(first_module, second_module,app_version,request,deviceT);
    }


}
