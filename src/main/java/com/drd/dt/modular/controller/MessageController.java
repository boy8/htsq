package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 消息中心接口
 * Created by 86514 on 2019/7/28.
 */
@Api(tags = "消息中心", description = "消息中心接口")
@RestController
@RequestMapping("/api/v2/htsq/message")
public class MessageController {

    @Autowired
    private MessageService messageService;

    /**
     * 分类获取消息中心信息
     **/
    @ApiOperation(value = "分类获取消息中心信息", notes = "分类获取消息中心信息", response = HttpMessage.class)
    @GetMapping("/list")
    public Tip message(@ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                             @ApiParam(value = "类型(1佣金通知 2活动公告 3系统通知)") @RequestParam(value = "type") Integer type,
                             @ApiParam(value = "用户id(type=1,3时必传)") @RequestParam(value = "user_id") Integer user_id,
                             @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                             @ApiParam(value = "佣金类型(type=1时必传)(1普通佣金 2售后扣除 3奖励佣金 4活动佣金)") @RequestParam(value = "messageType",required = false) Integer messageType) throws Exception{
        Tip data = messageService.selectMessage(page,type,messageType,user_id,token_id);
        return data;
    }

    /**
     * 消息中心首页信息
     **/
    @ApiOperation(value = "消息中心首页信息", notes = "消息中心首页信息(jump_type:1佣金通知 2活动公告 3系统通知;read_status是否已读(0否 1是))", response = HttpMessage.class)
    @GetMapping("/simpleInfo")
    public Tip simpleInfo(@ApiParam(value = "用户id") @RequestParam(value = "user_id",required = false) Integer user_id,
                          @ApiParam(value = "用户token") @RequestParam(value = "token_id",required = false) String token_id) throws Exception{
        Tip data = messageService.simpleInfo(user_id,token_id);
        return data;
    }

}
