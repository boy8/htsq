package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.VersionForIOSService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 类中接口为IOS审核上架使用
 */

@Api(tags = "ios审核版本", description = "ios审核版本接口")
@RestController
@RequestMapping("/api/v2/htsq/iosVersion")
public class VersionForIOSController {

    @Autowired
    private VersionForIOSService forIOSService;

    /**
     * ios支付宝付款
     **/
    @RequestMapping("/alipay")
    public Tip alipay(@RequestParam Map<String,String> map) throws Exception {
        Tip tip = forIOSService.alipay(map);
        return tip;
    }

    /**
     * 微信支付付款
     **/
    @RequestMapping("/WXPay")
    public Tip wxpay(@RequestParam Map<String,String> map) throws Exception {
        Tip tip = forIOSService.wxpay(map);
        return tip;
    }

    /**
     * ios新增下单商品
     **/
    @RequestMapping("/goods")
    public Tip goods(@RequestParam Map<String,String> map) throws Exception {
        Tip tip = forIOSService.goods(map);
        return tip;
    }

    /**
     * ios下单商品列表
     **/
    @RequestMapping("/list")
    public Tip list(@RequestParam Map<String,String> map) throws Exception {
        Tip tip = forIOSService.list(map);
        return tip;
    }

    /**
     * ios下单商品状态修改
     **/
    @RequestMapping("/status")
    public Tip status(@RequestParam Map<String,String> map) throws Exception {
        Tip tip = forIOSService.status(map);
        return tip;
    }

}
