package com.drd.dt.modular.controller;


import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.CreditCardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 信用卡 controller
 * Created by 86514 on 2019/7/28.
 * 网址：http://www.kaduofen.cn/back/index.html#/app/application  账号：a626581041@vip.qq.com  密码：a626581041@vip.qq.com@20191225
 */
@Api(tags = "信用卡-卡多分", description = "信用卡-卡多分")
@RestController
@RequestMapping("/api/v2/htsq/creditCard")
public class CreditCardController {

    @Autowired
    private CreditCardService creditCardService;

    /**
     * 获取详情 所需参数并 判断核对权限
     *
     * @param user_id 用户ID
     * @param mobile  手机号
     * @param
     */
    @ApiOperation(value = "获取encrypt", notes = "获取encrypt", response = HttpMessage.class)
    @PostMapping("/encrypt")
    public Tip encrypt(@ApiParam(value = "用户ID") @RequestParam(value = "user_id") String user_id,
                       @ApiParam(value = "手机号") @RequestParam(value = "mobile") String mobile,
                       @ApiParam(value = "用户分佣比例") @RequestParam(value = "bonusRate", required = false) String bonusRate,
                       @ApiParam(value = "分享佣金比例") @RequestParam(value = "shareRate", required = false) String shareRate
    ) throws Exception {
        return creditCardService.createSmyToken(user_id, mobile, bonusRate, shareRate);
    }

    /***
     * 获取 url
     * */
    @ApiOperation(value = "获取 url", notes = "获取 url", response = HttpMessage.class)
    @RequestMapping("/creditCardURL")
    public Tip creditCardURL(@ApiParam(value = "用户ID") @RequestParam(value = "user_id") String user_id,
                             @ApiParam(value = "用户分佣比例") @RequestParam(value = "bonusRate", required = false) String bonusRate,
                             @ApiParam(value = "分享佣金比例") @RequestParam(value = "shareRate", required = false) String shareRate
    ) throws Exception {
        return creditCardService.creditCardURL(user_id, bonusRate, shareRate);
    }

    /***
     * 查询单个应用信用卡订单
     * 接口 /b2b/api/getOrderListBysupplier
     * @param dataFlag    String	是	1-根据创建时间查询,2-根据更新时间查询
     * @param startDate    String	是	开始时间 如:2019-06-17T00:00:00.000+0800
     * @param endDate    String	是	结束时间(开始时间和结束时间的时间差控制在3天内) 如:2019-06-17T00:00:00.000+0800
     * @param pageNo    int	是	第几页 从1开始
     * @param pageSize    int	是	分页大小，最大值500
     * */
    @ApiOperation(value = "查询单个应用信用卡订单", notes = "查询单个应用信用卡订单", response = HttpMessage.class)
    @PostMapping("/adhibitionCreditCardOrder")
    public Tip adhibitionCreditCardOrder(@ApiParam(value = "查询类型:1-根据创建时间查询,2-根据更新时间查询") @RequestParam(value = "dataFlag") int dataFlag,
                                         @ApiParam(value = "开始时间:yyyy-mm-dd") @RequestParam(value = "startDate") String startDate,
                                         @ApiParam(value = "结束时间:yyyy-mm-dd") @RequestParam(value = "endDate") String endDate,
                                         @ApiParam(value = "第几页 不传默认1") @RequestParam(value = "pageNo") int pageNo) throws Exception {
        return creditCardService.adhibitionCreditCardOrder(dataFlag, startDate, endDate, pageNo, 20);
    }


    /***
     * 查询单个用户信用卡订单
     * 接口 /b2b/api/getOrderListBysupplier
     * @param user_id    int	是	1-根据创建时间查询,2-根据更新时间查询
     * */
    @ApiOperation(value = "查询单个用户信用卡订单", notes = "查询单个用户信用卡订单", response = HttpMessage.class)
    @PostMapping("/userCreditCardOrder")
    public Tip userCreditCardOrder(@ApiParam(value = "用户id") @RequestParam(value = "user_id") int user_id) throws Exception {
        return creditCardService.userCreditCardOrder(user_id);
    }


    /***
     * 用户信用卡订单回调接口
     * */
    @ApiOperation(value = "用户信用卡订单回调接口", notes = "用户信用卡订单回调接口", response = HttpMessage.class)
    @PostMapping("/notification")
    public Tip notification(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Tip tip = creditCardService.notification(request, response);
        return tip;
    }

    /***
     * 单个用户订单结算
     * */
    @ApiOperation(value = "单个用户订单结算", notes = "单个用户订单结算", response = HttpMessage.class)
    @PostMapping("/synchronizationCreditCardOrder")
    public void synchronizationCreditCardOrder(@RequestParam(value = "userId") String userId,
                                               @RequestParam(value = "orderId") String orderId) throws Exception {
        creditCardService.synchronizationCreditCardOrder(userId, orderId);
    }

    /***
     * 订单列表查询接口
     * */
    @ApiOperation(value = "订单列表查询接口", notes = "订单列表查询接口", response = HttpMessage.class)
    @PostMapping("/creditCardOrderAll")
    public Tip creditCardOrderAll(@ApiParam(value = "用户id") @RequestParam(value = "user_id") String user_id,
                                  @ApiParam(value = "tokenId") @RequestParam(value = "token_id") String token_id,
                                  @ApiParam(value = "订单类型 -1代表所有") @RequestParam(value = "status") String status) throws Exception {
        Tip tip = creditCardService.creditCardOrderAll(user_id, token_id, status);
        return tip;
    }

    /***
     * 订单列表查询接口
     * */
    @ApiOperation(value = "订单列表查询接口", notes = "订单列表查询接口", response = HttpMessage.class)
    @PostMapping("/creditCardOrderDateli")
    public Tip creditCardOrderDateli(@ApiParam(value = "信用卡订单主键") @RequestParam(value = "id") String id) throws Exception {
        Tip tip = creditCardService.finDetail(id);
        return tip;
    }


}
