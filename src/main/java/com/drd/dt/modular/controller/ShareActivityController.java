package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.StuadDTO;
import com.drd.dt.modular.service.HomeService;
import com.drd.dt.modular.service.ShareActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 分享获得现金红包活动
 * Created by 86514 on 2020/1/8.
 */
@Api(tags = "分享", description = "分享获得现金红包活动")
@RestController
@RequestMapping("/api/v2/htsq/share")
public class ShareActivityController {

    @Autowired
    private ShareActivityService shareActivityService;

    /**
     * 用户点击拆红包（首页点红包进入）
     **/
    @ApiOperation(value = "用户点击拆红包（首页点红包进入）", notes = "用户点击拆红包（首页点红包进入）", response = HttpMessage.class)
    @PostMapping("/clickRedPacket")
    public Tip clickRedPacket(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                     @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id) throws Exception {
        Tip data = shareActivityService.clickRedPacket(user_id, token_id);
        return data;
    }

    /**
     * 用户拆红包、分享好友和群
     **/
    @ApiOperation(value = "用户拆红包、分享好友和群", notes = "用户拆红包、分享好友和群", response = HttpMessage.class)
    @PostMapping("/shareGroup")
    public Tip shareGroup(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                          @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                          @ApiParam(value = "类型（1开启红包 2分享好友  3分享群）") @RequestParam(value = "type") Integer type) throws Exception {
        Tip data = shareActivityService.shareGroup(user_id, token_id, type);
        return data;
    }

}
