package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.TXSuggestDTO;
import com.drd.dt.modular.service.TradeService;
import com.drd.dt.util.HttpConnectionPoolUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by 86514 on 2019/3/25.
 */

@Api(tags = "提现2", description = "提现相关接口2")
@RestController
@RequestMapping("/api/v2/htsq/Trade")
public class TradeListController {

    @Autowired
    private TradeService tradesService;

    /**
     * 我的提现列表
     **/
    @ApiOperation(value = "我的提现列表", notes = "我的提现列表", response = HttpMessage.class)
    @PostMapping("/tx")
    public Tip tx(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                  @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                  @ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                  @ApiParam(value = "提现类型(0全部 1普通佣金  2奖励金 3活动佣金)") @RequestParam(value = "type",required = false) String type) throws Exception {
            Tip data = tradesService.tx(user_id, token_id, page,type);
        return data;
    }

    /**
     * 创建提现申请
     **/
    @ApiOperation(value = "创建提现申请", notes = "创建提现申请", response = HttpMessage.class)
    @PostMapping("/addTx")
    public Tip addTx(TXSuggestDTO txSuggestDTO) throws Exception {
            Tip data = tradesService.addTx(txSuggestDTO);
            return data;
    }

}
