package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.StuadDTO;
import com.drd.dt.modular.service.HomeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 首页接口
 * Created by 86514 on 2019/7/28.
 */
@Api(tags = "首页", description = "首页商品等接口")
@RestController
@RequestMapping("/api/v2/htsq/home")
public class HomeController {

    @Autowired
    private HomeService homeService;

    /**
     * 参数返回(初始化)
     **/
    @ApiOperation(value = "初始化", notes = "初始化接口", response = HttpMessage.class)
    @PostMapping("/stuad")
    public Tip stuad(StuadDTO stuadDTO, HttpServletRequest request) throws Exception {
        Tip data = homeService.stuad(stuadDTO, request);
        return data;
    }

    /**
     * 首页请求数据合集
     **/
    @ApiOperation(value = "首页请求数据合集", notes = "首页请求数据合集", response = HttpMessage.class)
    @GetMapping("/list")
    public Tip list(@ApiParam(value = "用户id") @RequestParam(value = "user_id", required = false) String user_id,
                    @RequestParam(name = "deviceT", required = false) String deviceT,
                    @RequestParam(name = "app_version", required = false) String app_version) throws Exception {
        Tip data = homeService.list(user_id, deviceT, app_version);
        return data;
    }

    /**
     * 获取开屏或banner图
     **/
    @ApiOperation(value = "获取开屏或banner图", notes = "获取开屏或banner图", response = HttpMessage.class)
    @GetMapping("/getAds")
    public Tip getAds(@ApiParam(value = "1首页banner 2开屏 3首页gif 4签到 5浮图 6个人中心banner 7活动专区 8首页活动区背景图 9插屏 11品牌导航分类模块(不传获取全部)") @RequestParam(value = "block", required = false) Integer block,
                      @ApiParam(value = "imei") @RequestParam(value = "imei", required = false) String imei,
                      @RequestParam(name = "deviceT", required = false) String deviceT,
                      @RequestParam(name = "app_version", required = false) String app_version, HttpServletRequest request) throws Exception {
        Tip data = homeService.getAds(block, imei, request, deviceT, app_version);
        return data;
    }

    /**
     * 限时秒杀时间点
     **/
    @ApiOperation(value = "获取限时秒杀时间点", notes = "获取限时秒杀时间点", response = HttpMessage.class)
    @GetMapping("/seckillTime")
    public Tip seckillTime() throws Exception {
        Tip data = homeService.seckillTime();
        return data;
    }

    /**
     * 首页-中间部分-核桃精选+限时秒杀时间点（seckillTime接口）
     **/
    @ApiOperation(value = "首页-中间部分-核桃精选+限时秒杀时间点", notes = "首页-中间部分-核桃精选+限时秒杀时间点", response = HttpMessage.class)
    @GetMapping("/seckillTimeAndSiftGoods")
    public Tip seckillTimeAndSiftGoods() throws Exception {
        Tip data = homeService.seckillTimeAndSiftGoods();
        return data;
    }

    /**
     * 限时秒杀商品列表
     **/
    @ApiOperation(value = "获取限时秒杀商品列表", notes = "获取限时秒杀商品列表", response = HttpMessage.class)
    @GetMapping("/seckillGoods")
    public Tip seckillGoods(@ApiParam(value = "时间点（快抢时间点：6.今天的0点，7.今天10点，8.今天12点，9.今天15点，10.今天20点，" +
            "11.明天的0点）") @RequestParam(value = "type") String type,
                            @ApiParam(value = "页数") @RequestParam(value = "page") String page,
                            @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id) throws Exception {
        Tip data = homeService.seckillGoods(page, type, user_id);
        return data;
    }

    /**
     * 分类
     **/
    @ApiOperation(value = "分类", notes = "商品分类", response = HttpMessage.class)
    @GetMapping("/stulist")
    public Tip stulist() throws Exception {
        Tip data = homeService.stulist();
        return data;
    }

    /**
     * 好单库获取热搜关键词
     */
    @ApiOperation(value = "商品热搜关键词", notes = "商品热搜关键词", response = HttpMessage.class)
    @GetMapping("/hotSearch")
    public Tip hotSearch() throws Exception {
        Tip data = homeService.hotSearch();
        return data;
    }

    /**
     * 商品分享链接
     **/
    @ApiOperation(value = "商品分享链接", notes = "商品分享链接", response = HttpMessage.class)
    @RequestMapping("/shareGoods")
    public Tip shareGoods(@ApiParam(value = "类型:1淘宝 2京东 3拼多多 4唯品会") @RequestParam(value = "type") Integer type,
                          @ApiParam(value = "商品id") @RequestParam(value = "goods_id") String goods_id,
                          @ApiParam(value = "商品名称", required = false) @RequestParam(value = "goods_name") String goods_name,
                          @ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                          HttpServletRequest request) throws Exception {
        Tip data = homeService.shareGoods(type, goods_id, goods_name, user_id, request);
        return data;
    }

    /**
     * 跳转时转换链接
     **/
    @ApiOperation(value = "跳转时转换链接", notes = "跳转时转换链接", response = HttpMessage.class)
    @GetMapping("/exchangeUrl")
    public Tip exchangeUrl(@ApiParam(value = "识别id(7每日红包 11拼多多主题推广)") @RequestParam(value = "discern_id") Integer discern_id,
                           @ApiParam(value = "用户id") @RequestParam(value = "user_id", required = false) Integer user_id,
                           @ApiParam(value = "拼多多主题id") @RequestParam(value = "theme_id", required = false) String theme_id) throws Exception {
        Tip data = homeService.exchangeUrl(discern_id, user_id, theme_id);
        return data;
    }

    /**
     * 获取视频教程链接
     **/
    @ApiOperation(value = "获取视频教程链接", notes = "获取视频教程链接", response = HttpMessage.class)
    @GetMapping("/course")
    public Tip course() throws Exception {
        Tip data = homeService.course();
        return data;
    }

    /**
     * 零元购商品列表
     **/
    @ApiOperation(value = "零元购商品列表", notes = "零元购商品列表", response = HttpMessage.class)
    @PostMapping(value = "/zeroList")
    public Tip zeroList(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                        @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                        @ApiParam(value = "页数") @RequestParam(value = "page") Integer page) throws Exception {
        Tip data = homeService.zeroList(user_id, token_id, page);
        return data;
    }

    /**
     * 首页618活动链接信息
     **/
    @ApiOperation(value = "首页618活动链接信息", notes = "首页618活动链接信息", response = HttpMessage.class)
    @PostMapping(value = "/activity618")
    public Tip activity618(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                           @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                           @ApiParam(value = "会场活动名称") @RequestParam(value = "title", required = false) String title,
                           @ApiParam(value = "类型:1(淘宝618开门红—主会场),2(聚划算.主会场),3(进口母婴会场)," +
                                   "4(美妆洗护会场),5(数码家电主会场),6(猫超年中大赏会场)，7(618主会场)") @RequestParam(value = "type") String type) throws Exception {
        Tip data = homeService.activity618(user_id, token_id, type, title);
        return data;
    }

    /**
     * 618活动跳转时转换链接
     **/
    @ApiOperation(value = "618活动跳转时转换链接", notes = "618活动跳转时转换链接", response = HttpMessage.class)
    @GetMapping("/activity618Change")
    public Tip activity618Change(@ApiParam(value = "识别id") @RequestParam(value = "discern_id") Integer discern_id,
                                 @ApiParam(value = "用户id") @RequestParam(value = "user_id", required = false) Integer user_id,
                                 @ApiParam(value = "类型") @RequestParam(value = "type", required = false) String type,
                                 @ApiParam(value = "标题") @RequestParam(value = "title", required = false) String title) throws Exception {
        Tip data = homeService.activity618Change(discern_id, user_id, type, title);
        return data;
    }

    /**
     * 获取淘口令
     **/
    @ApiOperation(value = "获取淘口令", notes = "获取淘口令", response = HttpMessage.class)
    @RequestMapping("/exchangeTkl")
    public String exchangeTkl(@ApiParam(value = "url") @RequestParam(value = "url", required = false) String url,
                              @ApiParam(value = "title") @RequestParam(value = "title", required = false) String title,
                              @ApiParam(value = "img") @RequestParam(value = "img", required = false) String img) throws Exception {
        String data = homeService.exchangeTkl(url, title, img);
        return data;
    }

    /**
     * 邀请好友-邀请赚-6月-3号
     * 进入 邀请赚活动页面 调用此方法：
     * 1 用户自动参与活动，2 自动检擦 参加活动后 直属粉丝收单结算 并增加佣金变更信息，修改用户收益表
     * 返回：1 用户邀请赚以及邀请人数
     * 2 所有邀请人的首单 进度
     */
    @ApiOperation(value = "邀请好友-邀请赚", notes = "邀请好友-邀请赚", response = HttpMessage.class)
    @PostMapping("/invitedToEarn")
    public Tip invitedToEarn(@ApiParam(value = "user_id") @RequestParam(value = "user_id") Integer user_id) throws Exception {
        Tip data = homeService.invitedToEarn(user_id);
        return data;
    }

    /**
     * 检测是否 已经参加 邀请赚
     */
    @ApiOperation(value = "检测是否 已经参加 邀请赚", notes = "检测是否 已经参加 邀请赚", response = HttpMessage.class)
    @PostMapping("/addJoin")
    public Tip addJoin(@ApiParam(value = "user_id") @RequestParam(value = "user_id") Integer user_id) throws Exception {
        Tip data = homeService.addJoin(user_id);
        return data;
    }

    /***
     * 点击分享邀请赚按钮 添加一次记录
     * */
    @ApiOperation(value = "邀请赚-每次分享系统记录一次", notes = "邀请赚-每次分享系统记录一次", response = HttpMessage.class)
    @PostMapping("/addShare")
    public Tip addShare(@ApiParam(value = "user_id") @RequestParam(value = "user_id") Integer user_id) throws Exception {
        Tip data = homeService.addShare(user_id);
        return data;
    }
}
