package com.drd.dt.modular.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.TBGoodsService;
import com.drd.dt.modular.service.zuimeiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 最美天气-小美贝商品
 */
@Api(tags = "最美天气-小美贝", description = "最美天气-小美贝接口")
@RequestMapping("/api/v2/htsq/zuimeiInfo")
@RestController
public class ZuimeiController {

    @Autowired
    private zuimeiService zuimeiService;
    @Autowired
    private TBGoodsService tbGoodsService;

    /**
     * 小美贝商品列表
     **/
    @ApiOperation(value = "小美贝商品列表", notes = "小美贝商品列表", response = HttpMessage.class)
    @PostMapping(value = "/list")
    public Tip list(@ApiParam(value = "用户id") @RequestParam(value = "user_id",required = false) Integer user_id,
                    @ApiParam(value = "最美天气用户id") @RequestParam(value = "zuimei_user_id") Integer zuimei_user_id,
                    @ApiParam(value = "页数") @RequestParam(value = "page") Integer page) throws Exception {
        Tip data = zuimeiService.list(user_id,zuimei_user_id,page);
        return data;
    }

    /**
     * 获取最美用户账户信息
     **/
    @ApiOperation(value = "获取最美用户账户信息", notes = "获取最美用户账户信息", response = HttpMessage.class)
    @PostMapping(value = "/zuimeiUserInfo")
    public Tip zuimeiUserInfo(@ApiParam(value = "用户id") @RequestParam(value = "user_id",required = false) Integer user_id,
                              @ApiParam(value = "最美用户id") @RequestParam(value = "zuimei_user_id",required = false) Integer zuimei_user_id) throws Exception {
        Tip data = zuimeiService.zuimeiUserInfo(user_id,zuimei_user_id);
        return data;
    }

    /**
     * 小美贝-获取兑换记录
     **/
    @ApiOperation(value = "获取兑换记录", notes = "获取兑换记录", response = HttpMessage.class)
    @PostMapping(value = "/exchangeHistory")
    public Tip exchangeHistory(@ApiParam(value = "最美用户id") @RequestParam(value = "zuimei_user_id") Integer zuimei_user_id,
                               @ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                               @ApiParam(value = "1用户小美贝兑换核桃积分信息;2核桃积分兑换淘礼金记录") @RequestParam(value = "type",required = false) String type) throws Exception {
        Tip data = zuimeiService.exchangeHistory(zuimei_user_id,page,type);
        return data;
    }

    /**
     * 小美贝商品详情 201810014
     **/
    @ApiOperation(value = "小美贝商品详情", notes = "小美贝商品详情", response = HttpMessage.class)
    @PostMapping(value = "/detail")
    public Tip detail(@ApiParam(value = "用户id") @RequestParam(value = "user_id",required = false) Integer user_id,
                      @ApiParam(value = "商品id") @RequestParam(value = "goods_id") String goods_id) throws Exception {
        Tip data = zuimeiService.detail(goods_id,user_id);
        return data;
    }

    /**
     * 最美天气-小美贝用户信息接收
     **/
    @ApiOperation(value = "最美天气-小美贝用户信息接收", notes = "用户信息接收", response = HttpMessage.class)
    @PostMapping(value = "/receiveInfo")
    public Tip receiveInfo(@ApiParam(value = "最美天气用户id") @RequestParam(value = "zuimei_user_id") Integer zuimei_user_id,
                           @ApiParam(value = "兑换的金额") @RequestParam(value = "money") String money) throws Exception {
        Tip data = zuimeiService.receiveInfo(zuimei_user_id,money);
        return data;
    }

    /**
     * 最美天气-小美贝淘礼金兑换
     **/
    @ApiOperation(value = "小美贝淘礼金兑换", notes = "小美贝淘礼金兑换", response = HttpMessage.class)
    @PostMapping(value = "/tlj")
    public Tip tlj(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                   @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                   @ApiParam(value = "商品id") @RequestParam(value = "goods_id") String goods_id,
                   @ApiParam(value = "商品名称") @RequestParam(value = "goods_name") String goods_name,
                   @ApiParam(value = "商品图片") @RequestParam(value = "pic") String pic,
                   @ApiParam(value = "商品券后价") @RequestParam(value = "price") String price) throws Exception {
        Tip data = zuimeiService.tlj(user_id,token_id,price,goods_id,goods_name,pic);
        return data;
    }

    /**
     * 最美天气-小美贝淘礼金使用情况查询
     **/
    @ApiOperation(value = "小美贝淘礼金使用情况查询", notes = "小美贝淘礼金使用情况查询", response = HttpMessage.class)
    @GetMapping(value = "/tljUseDetail")
    public Tip tljUseDetail(@ApiParam(value = "淘礼金id") @RequestParam(value = "rights_id",required = false) String rights_id,
                            @ApiParam(value = "淘礼金兑换时间") @RequestParam(value = "time",required = false) String time) throws Exception {
        Tip data = zuimeiService.tljUseDetail(rights_id,time);
        return data;
    }

    /**
     * 现金券兑换淘礼金记录
     **/
    @ApiOperation(value = "现金券兑换淘礼金记录", notes = "现金券兑换淘礼金记录", response = HttpMessage.class)
    @PostMapping(value = "/tljHistory")
    public Tip tljHistory(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                          @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                          @ApiParam(value = "分页") @RequestParam(value = "page") Integer page) throws Exception {
        Tip data = zuimeiService.tljHistory(user_id,token_id,page);
        return data;
    }

    /**
     * 最美天气使用-商品列表
     **/
    @ApiOperation(value = "最美天气使用-商品列表", notes = "最美天气使用-商品列表", response = HttpMessage.class)
    @PostMapping(value = "/arrondiGoods")
    public Tip arrondiGoods(@ApiParam(value = "分页") @RequestParam(value = "page",required = false) Integer page,
                            @ApiParam(value = "每页条数") @RequestParam(value = "pageSize",required = false) Integer pageSize,
                            @ApiParam(value = "查询关键词") @RequestParam(value = "queryKey",required = false) String queryKey) throws Exception {
        Tip data = zuimeiService.arrondiGoods(page,pageSize,queryKey);
        return data;
    }

    /**
     * 最美天气使用-商品详情
     **/
    @ApiOperation(value = "最美天气使用-商品详情", notes = "最美天气使用-商品详情", response = HttpMessage.class)
    @PostMapping(value = "/goodsDetail")
    public Tip arrondiGoods(@ApiParam(value = "商品id") @RequestParam(value = "goods_id") String goods_id) throws Exception {
        Tip data = zuimeiService.goodsDetail(goods_id);
        return data;
    }

}
