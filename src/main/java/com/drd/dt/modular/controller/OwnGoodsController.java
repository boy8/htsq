package com.drd.dt.modular.controller;


import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.OwnGoodsOrderDTO;
import com.drd.dt.modular.service.OrderGiftService;
import com.drd.dt.modular.service.OwnGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 自营
 * Created by 86514 on 2019/7/28.
 */
@Api(tags = "自营前端", description = "自营前端接口")
@RestController
@RequestMapping("/api/v2/htsq/dealer")
public class OwnGoodsController {

    @Autowired
    private OwnGoodsService ownGoodsService;

    @Autowired
    private OrderGiftService orderGiftService;

    /**
     * 获取商品列表-分页
     *
     * @param pageIndex 当前分页小标
     * @param pageSize  每页最大数
     **/
    @ApiOperation(value = "自营商品查询", notes = "查询数据列表分页", response = HttpMessage.class)
    @PostMapping("/getOwnGoodsList")
    public Tip getOwnGoodsList(@RequestParam(name = "pageIndex") Integer pageIndex,
                               @RequestParam(name = "pageSize") Integer pageSize) throws Exception {
        return ownGoodsService.getOwnGoodsList(pageIndex, pageSize);
    }

    /**
     * 获取商品列表-分页-分组
     *
     * @param pageIndex 当前分页小标
     * @param pageSize  每页最大数
     * @param type  分组类型(1正在热卖、2热销爆款、3即将上市)',
     **/
    @ApiOperation(value = "自营商品查询-分组", notes = "查询数据列表分页-分组", response = HttpMessage.class)
    @PostMapping("/getOwnGoodsTypeList")
    public Tip getOwnGoodsTypeList(@RequestParam(name = "pageIndex") Integer pageIndex,
                               @RequestParam(name = "pageSize") Integer pageSize,
                                   @RequestParam(name = "type") int type) throws Exception {
        return ownGoodsService.getOwnGoodsTypeList(pageIndex, pageSize,type);
    }

    /**
     * 新 - 自营商品查询  不 分页
     * */
    @ApiOperation(value = "新 - 自营商品查询  不 分页", notes = "新 - 自营商品查询  不 分页", response = HttpMessage.class)
    @PostMapping("/getOwnGoodsListNoPage")
    public Tip getOwnGoodsListNoPage() throws Exception {
        return ownGoodsService.getOwnGoodsListNoPage();
    }



    /**
     * 获取商品详情
     *
     * @param dealer_id 商品ID
     */
    @ApiOperation(value = "获取商品详情", notes = "获取商品详情", response = HttpMessage.class)
    @PostMapping("/getOwnGoodsDetails")
    public Tip getOwnGoodsDetails(@RequestParam int dealer_id) throws Exception {
        return ownGoodsService.getOwnGoodsDetails(dealer_id);
    }

    /**
     * 获取商品规格
     **/
    @ApiOperation(value = "商品规格", notes = "获取商品规格", response = HttpMessage.class)
    @PostMapping("/getGoodsSpecificationDetails")
    public Tip getGoodsSpecificationDetails(@ApiParam(value = "商品ID") @RequestParam(value = "goods_id") Integer goods_id) throws Exception {
        Tip data = ownGoodsService.getGoodsSpecificationDetails(goods_id);
        return data;
    }


    /**
     * 生成订单
     */
    @ApiOperation(value = "生成订单", notes = "生成订单", response = HttpMessage.class)
    @PostMapping("/orderGift")
    public void addOrderGift(@RequestBody OwnGoodsOrderDTO ownGoodsOrderDTO, HttpServletResponse response, HttpServletRequest request) throws Exception {
        ownGoodsService.addOrderGift(ownGoodsOrderDTO, response, request);
    }

    /**
     * 自营商品订单详情
     */
    @ApiOperation(value = "自营商品订单详情", notes = "自营商品订单详情", response = HttpMessage.class)
    @PostMapping("/getOrderGiftDetail")
    public Tip getOrderGiftDetail(@ApiParam(value = "订单号") @RequestParam(value = "order_id") String order_id) {
        return ownGoodsService.getOrderGiftDetail(order_id);
    }


    /**
     * 调用该方法  更新所有待收货 自营商品状态
     */
    @ApiOperation(value = "手动更新自营订单待收货状态", notes = "手动更新自营订单待收货状态", response = HttpMessage.class)
    @PostMapping("/orderTask")
    public Tip orderTask() {
       return orderGiftService.orderTask();
    }
}
