package com.drd.dt.modular.controller;


import com.drd.dt.back.dto.SiftOrderLogDTO;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.impl.SiftOrderLogServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

/**
 * 核桃精选 订单 退款记录 Controller层
 */
@Api(tags = "核桃精选订单退款记录", description = "精选核桃商品订单退款记录接口")
@RestController
@RequestMapping("/api/v2/htsq/siftOrderLog")
public class SiftOrderLogController {


    @Autowired
    private SiftOrderLogServiceImpl siftOrderLogService;


    /**
     * 新增记录
     */
    @ApiOperation(value = "新增记录", notes = "新增记录", response = HttpMessage.class)
    @PostMapping("/addOrderGift")
    public Tip addOrderGift(@RequestBody SiftOrderLogDTO siftOrderLogDTO) throws Exception {
        Tip tip = siftOrderLogService.addLog(siftOrderLogDTO);
        return tip;
    }

    /**
     * 查询申请记录
     **/
    @ApiOperation(value = "查询申请记录", notes = "查询申请记录", response = HttpMessage.class)
    @PostMapping("/userOrderLogList")
    public Tip userOrderLogList(@RequestParam(value = "user_id") Integer user_id) throws Exception {
        Tip tip = siftOrderLogService.userOrderLogList(user_id);
        return tip;
    }

    /**
     * 申请中 补全 快递单号
     **/
    @ApiOperation(value = "申请中 补全 快递单号", notes = "申请中 补全 快递单号", response = HttpMessage.class)
    @PostMapping("/userParcel")
    public Tip userParcel(@RequestParam(value = "parcel_number") String parcel_number,
                          @RequestParam(value = "parcel_type") String parcel_type,
                          @RequestParam(value = "id") Integer id) throws Exception {
        Tip tip = siftOrderLogService.userParcel(parcel_number,parcel_type,id);
        return tip;
    }


}
