package com.drd.dt.modular.controller;


import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.ISiftGoodsService;
import com.drd.dt.modular.service.impl.SiftGoodsServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 精选核桃 商品 Controller层
 */
@Api(tags = "核桃精选", description = "精选核桃商品接口")
@RestController
@RequestMapping("/api/v2/htsq/siftGoods")
public class SiftGoodsController {

    @Autowired
    private ISiftGoodsService siftGoodsService;

    /**
     * 查询列表  分页
     */
    @ApiOperation(value = "查询列表  分页", notes = "查询列表  分页", response = HttpMessage.class)
    @RequestMapping("/siftGoodsList")
    Tip SiftGoodsList(@RequestParam(name = "index") Integer index,
                      @ApiParam(value = "长标题") @RequestParam(name = "title_s", required = false) String title_s,
                      @ApiParam(value = "分类") @RequestParam(name = "sort", required = false) String sort,
                      @ApiParam(value = "售卖类型：0核桃精选 1线下折扣 默认不传值为0") @RequestParam(name = "discounts_type", required = false,defaultValue ="0") int discounts_type) throws Exception {
        Tip tip = siftGoodsService.SiftGoodsList(index, title_s, sort,discounts_type);
        return tip;
    }

    /**
     * 查询精选核桃商品详情
     **/
    @ApiOperation(value = "商品详情", notes = "商品详情", response = HttpMessage.class)
    @RequestMapping("/siftGoodsDateil")
    Tip SiftGoodsDateil(@RequestParam(name = "id")String id,@RequestParam(name = "user_id")Integer user_id) throws Exception {
        Tip tip = siftGoodsService.SiftGoodsDateil(id,user_id);
        return tip;
    }

    /**
     * 查询 列表界面 活动图连接
     * **/
    @ApiOperation(value = "列表界面 活动图连接 顶部分类(包含地址名称电话)", notes = "顶部分类 列表界面 活动图连接(包含地址名称电话)", response = HttpMessage.class)
    @RequestMapping("/siftGoodsPageData/{user_id}")
    Tip SiftGoodsPageData(@PathVariable Integer user_id) throws Exception {
        Tip tip = siftGoodsService.siftGoodsPageData(user_id);
        return tip;
    }

    /***
     * 首页  精选好货- 正品包邮
     * */
    @ApiOperation(value = "首页", notes = "首页商品", response = HttpMessage.class)
    @RequestMapping("/homeSiftGoods")
    Tip homeSiftGoods() throws Exception {
        Tip tip = siftGoodsService.homeSiftGoods();
        return tip;
    }

}
