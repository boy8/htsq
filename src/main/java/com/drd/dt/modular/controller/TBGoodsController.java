package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.JDService;
import com.drd.dt.modular.service.PDDService;
import com.drd.dt.modular.service.TBGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.http.util.TextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 淘宝商品
 * Created by 86514 on 2019/3/25.
 */

@Api(tags = "淘宝", description = "淘宝商品")
@RestController
@RequestMapping("/api/v2/htsq/tbGoods")
public class TBGoodsController {

    @Autowired
    private TBGoodsService tbGoodsService;
    @Autowired
    private JDService jdService;
    @Autowired
    private PDDService pddService;

    /**
     * 商品详情
     */
    @ApiOperation(value = "商品详情", notes = "商品详情", response = HttpMessage.class)
    @RequestMapping("/goodsDetail")
    public Tip goodsDetail(@RequestParam("itemid") String itemid,
                           @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id,
                           @RequestParam(name = "deviceT", required = false) String deviceT,
                           @RequestParam(name = "app_version", required = false) String app_version, HttpServletRequest request) throws Exception {
        Tip data = tbGoodsService.goodsDetail(itemid, user_id, request, deviceT, app_version);
        return data;
    }


    /**
     * 首页搜索
     */
    @ApiOperation(value = "搜索", notes = "首页搜索和推荐商品", response = HttpMessage.class)
    @PostMapping("/goodsSearch")
    public Tip goodsSearch(@ApiParam(value = "关键词(填“推荐”为首页推荐商品)") @RequestParam(value = "keywords") String keywords,
                           @ApiParam(value = "分页") @RequestParam(value = "page") Integer page,
                           @ApiParam(value = "0是综合 1是最新 2是销量从高到低 4.到手价价格(低到高)，5.到手价价格（高到低）") @RequestParam(value = "sort") String sort,
                           @ApiParam(value = "是否有券  1有券  0全部") @RequestParam(value = "ifcoupon") String ifcoupon,
                           @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id,
                           @ApiParam(value = "商品id(智能搜索时使用)") @RequestParam(name = "goods_id", required = false) String goods_id,
                           @RequestParam(name = "imei", required = false) String imei,
                           @RequestParam(name = "DeviceT", required = false) String DeviceT,
                           @RequestParam(name = "deviceT", required = false) String deviceT) throws Exception {
        Tip data;
        if (!StringUtils.isEmpty(keywords) && keywords.equals("推荐")) {//首页推荐商品
            if (!TextUtils.isEmpty(deviceT)) {
                DeviceT = deviceT;
            }
            data = tbGoodsService.getRecommendList(page, user_id, imei, DeviceT);
        } else {
            data = tbGoodsService.goodsSearch(keywords, page, sort, ifcoupon, user_id, goods_id);
        }
        return data;
    }

    /**
     * 商品转高拥
     */
    @ApiOperation(value = "商品转高拥", notes = "商品转高拥", response = HttpMessage.class)
    @PostMapping("/goodsInfo")
    public Tip goodsInfo(@ApiParam(value = "商品id") @RequestParam(value = "goods_id") String goods_id,
                         @ApiParam(value = "用户id") @RequestParam(value = "user_id") String user_id,
                         @ApiParam(value = "渠道关系id") @RequestParam(value = "relation_id", required = false) String relation_id,
                         @ApiParam(value = "是否有卷 ： false 没用 、true 有") @RequestParam(value = "ifCoupon") Boolean ifCoupon,
                         @ApiParam(value = "购买类型，0自购 ，1来自分享") @RequestParam(value = "buytype") String buytype,
                         @ApiParam(value = "版本号") @RequestParam(value = "app_version", required = false) String app_version,
                         @ApiParam(value = "手机类别(Android:0 ios:1)") @RequestParam(value = "deviceT", required = false) String deviceT) throws Exception {
        Tip data = tbGoodsService.goodsInfo(goods_id, user_id, relation_id, ifCoupon, buytype, app_version, deviceT);
        return data;
    }

    /**
     * 智能搜索 接口-1:内容解析
     * 解析：
     **/
    @ApiOperation(value = "智能搜索", notes = "内容解析", response = HttpMessage.class)
    @GetMapping("/analysisContent")
    public Tip analysisContent(@RequestParam(value = "content", required = false) String content) throws Exception {
        Tip data = tbGoodsService.analysisContent(content);
        return data;
    }

    /**
     * 相关商品
     */
    @ApiOperation(value = "相关商品", notes = "相关商品", response = HttpMessage.class)
    @GetMapping("/recommendGet")
    public Tip recommendGet(@RequestParam("num_iid") Long num_iid,
                            @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id) throws Exception {
        Tip data = tbGoodsService.recommendGet(num_iid, user_id);
        return data;
    }

    /**
     * 9.9包邮
     */
    @ApiOperation(value = "9.9包邮", notes = "9.9包邮", response = HttpMessage.class)
    @RequestMapping("/sparePostage")
    public Tip sparePostage(@ApiParam(value = "0.综合（最新），1.券后价(低到高)，2.券后价（高到低），3.券面额（高到低），4.月销量（高到低），5.佣金比例（高到低），6.券面额（低到高），7.月销量（低到高），8.佣金比例（低到高），9.全天销量（高到低），10全天销量（低到高），11.近2小时销量（高到低），12.近2小时销量（低到高），13.优惠券领取量（高到低），14.好单库指数（高到低）")
                            @RequestParam("sort") int sort,
                            @ApiParam(value = "商品类目:0全部，1女装，2男装，3内衣，4美妆，5配饰，6鞋品，7箱包，8儿童，9母婴，10居家，11美食，12数码，13家电，14其他，15车品，16文体，17宠物")
                            @RequestParam("cid") int cid,
                            @ApiParam(value = "分页")
                            @RequestParam("page") int page,
                            @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id
    ) throws Exception {

        Tip data = tbGoodsService.sparePostage(sort, cid, page, user_id);
        return data;
    }

    /**
     * 高佣精品
     */
    @ApiOperation(value = "高佣精品-好单裤", notes = "高佣精品-好单裤", response = HttpMessage.class)
    @RequestMapping("/supersearchList")
    public Tip supersearchList(@ApiParam(value = "关键字：例如：精品") @RequestParam("keyword") String keyword,
                               @ApiParam(value = "0.综合，1.最新，2.销量（高到低），3.销量（低到高），4.价格(低到高)，5.价格（高到低），6.佣金比例（高到低）") @RequestParam("sort") int sort,
                               @ApiParam(value = "是否只取有券商品：0否；1是，默认是") @RequestParam("is_coupon") int is_coupon,
                               @ApiParam(value = "分页")
                               @RequestParam("page") int page,
                               @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id) throws Exception {

        Tip data = tbGoodsService.supersearchList(keyword, sort, is_coupon, page, user_id);
        return data;
    }

    @ApiOperation(value = "高佣精品-淘宝", notes = "高佣精品-淘宝", response = HttpMessage.class)
    @PostMapping("/supersearchListTB")
    public Tip supersearchListTB(@ApiParam(value = "分页") @RequestParam(value = "page") Integer page,
                                 @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id,
                                 @RequestParam(name = "imei", required = false) String imei,
                                 @RequestParam(name = "deviceT", required = false) String deviceT) throws Exception {

        Tip data = tbGoodsService.supersearchLis2(page, user_id, imei, deviceT);
        return data;
    }

    @ApiOperation(value = "(无用户登录)分页-根据关键字查询 商品列表")
    @RequestMapping(value = "/list", method = {RequestMethod.GET, RequestMethod.POST})
    public Tip getList(@ApiParam(value = "关键词") @RequestParam(value = "keywords") String keywords,
                       @ApiParam(value = "分页") @RequestParam(value = "page") Integer page,
                       @ApiParam(value = "0 淘宝，1京东 2拼多多") @RequestParam(value = "type", defaultValue = "0") Integer type,
                       HttpServletRequest request) {
        Tip data = null;
        if (type.intValue() == 0) {
            data = tbGoodsService.getList(keywords, page, request);
        } else if (type.intValue() == 2) {
            data = pddService.list(keywords, page,request);
        }
//        else if (type.intValue() == 1) {
//            data =  jdService.list(keywords,page,request);
//        }
        return data;
    }

    @ApiOperation(value = "(无用户登录)商品详情")
    @RequestMapping(value = "/detail", method = {RequestMethod.GET, RequestMethod.POST})
    public Tip detail(@ApiParam(value = "商品ID") @RequestParam(value = "itemId") String itemId,
                      @ApiParam(value = "0 淘宝，1京东 2拼多多") @RequestParam(value = "type", defaultValue = "0") Integer type,
                      HttpServletRequest request) {
        Tip data = null;
        if (type.intValue() == 0) {
            data = tbGoodsService.detail(itemId, request);
        } else if (type.intValue() == 2) {
            data = pddService.detail(itemId, request);
        }

        return data;
    }


}
