package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.JDService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/22.
 * 订单维度的有效码，不建议使用，可以用订单行sku维度validCode参考,（-1：未知,2.无效-拆单,3.无效-取消,4.无效-京东帮帮主订单,5.无效-账号异常,6.无效-赠品类目不返佣,7.无效-校园订单,8.无效-企业订单,9.无效-团购订单,10.无效-开增值税专用发票订单,11.无效-乡村推广员下单,12.无效-自己推广自己下单,13.无效-违规订单,14.无效-来源与备案网址不符,15.待付款,16.已付款,17.已完成,18.已结算（5.9号不再支持结算状态回写展示））
 */

@Api(tags = "京东", description = "京东商品和订单接口")
@RestController
@RequestMapping("/api/v2/htsq/jd")
public class JDController {

    @Autowired
    private JDService jdSdkService;

    /**
     * 获取最新订单
     */
    @ApiOperation(value = "获取最新订单", notes = "获取最新订单", response = HttpMessage.class)
    @GetMapping("/task")
    public Tip getOrderList() throws Exception {
        Tip data = jdSdkService.task();
        return data;
    }

    /**
     * 同步完成订单
     */
    @ApiOperation(value = "同步完成订单", notes = "同步完成订单", response = HttpMessage.class)
    @GetMapping("/successOrder")
    public Tip successOrder(@ApiParam(value = "开始时间 yyyyMMddHHmmss") @RequestParam(value = "beginTime") String beginTime,
                            @ApiParam(value = "订单时间查询类型(1：下单时间，2：完成时间，3：更新时间)") @RequestParam(value = "type") String type) throws Exception {
        Tip data = jdSdkService.successOrder(beginTime, type);
        return data;
    }

    /**
     * 该方法 每次JD结算调用 初始 结算上月
     **/
    @ApiOperation(value = "该方法 每次JD结算调用", notes = "该方法 每次JD结算调用", response = HttpMessage.class)
    @GetMapping("/settleAccountsJD")
    public void settleAccountsJD(@ApiParam(value = "时间年月yyyy-MM")@RequestParam String time, @RequestParam(value = "type") String type) throws Exception {

        List<String> times =  jdSdkService.getJDOrderTime(time);
        if(null != times){
            for(String t:times){
                jdSdkService.successOrder(t, type);
                System.out.println("******* start:" + t + "——end:" + t + "*******");
                Thread.sleep(2000);
            }
        }
//        DateFormat d = new SimpleDateFormat("yyyyMMddHHmmss");
//
//        DateFormat d2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date state = d2.parse(start_time);
//        Date end = d2.parse(end_time);
//
//        //获取时戳  毫秒
//        long currentTime = end.getTime();
//        long starttime = state.getTime();
//        while (starttime < currentTime) {
//            long endtime = starttime + 1 * 5 * 60 * 1000; //10分钟
//            Date date = new Date(endtime);
//
//            System.out.println("******* start:" + d2.format(starttime) + "——end:" + d2.format(date) + "*******");
//            Thread.sleep(3000);
//            starttime = endtime;
//        }

    }


    /**
     * 商品列表搜索 -商品搜索接口（支持佣金排序）
     */
    @ApiOperation(value = "商品列表搜索", notes = "商品列表搜索", response = HttpMessage.class)
    @PostMapping("/getJdBrokerageList")
    public Tip getJdBrokerageList(@ApiParam(value = "关键词") @RequestParam(value = "keywords") String keywords,
                                  @ApiParam(value = "页数") @RequestParam(value = "page") String page,
                                  @ApiParam(value = "分类 0:综合排序;5:销量排序;6:新品排序,9佣金排序") @RequestParam(value = "sort") String sort,
                                  @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id) throws Exception {
        Tip data = jdSdkService.getJdBrokerageList(keywords, page, sort, user_id);
        return data;
    }


    /**
     * 商品详情
     **/
    @ApiOperation(value = "商品详情", notes = "商品详情", response = HttpMessage.class)
    @GetMapping("/getJdDetail")
    public Tip getJdDetail(@ApiParam(value = "商品id") @RequestParam(value = "sku") String sku,
                           @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id,
                           @RequestParam(name = "deviceT", required = false) String deviceT,
                           @RequestParam(name = "app_version", required = false) String app_version, HttpServletRequest request) throws Exception {
        Tip data = jdSdkService.getJDDetail(sku, user_id, request, deviceT, app_version);
        return data;
    }
}
