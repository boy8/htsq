package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.WphService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 唯品会订单和商品接口
 * 2019-9-24 tanhao
 **/

@Api(tags = "唯品会", description = "唯品会订单和商品接口")
@RestController
@RequestMapping("/api/v2/htsq/wph")
public class WphController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WphService wphService;

    /**
     * 唯品会订单获取
     */
    @ApiOperation(value = "唯品会订单获取", notes = "唯品会订单获取", response = HttpMessage.class)
    @GetMapping("/wphOrder")
    public Tip wphOrder(@ApiParam(value = "开始时间(时间戳)") @RequestParam(value = "start_time") String start_time,
                        @ApiParam(value = "结束时间(时间戳)") @RequestParam(value = "end_time") String end_time,
                        @ApiParam(value = "订单状态:0-不合格，1-待定，2-已完结，该参数不设置默认代表全部状态") @RequestParam(value = "status", required = false) Integer status) throws Exception {
        Tip data = wphService.wphOrder(start_time, end_time, status);
        return data;
    }

    /**
     * 更新唯品会时间段内的订单
     * 只需填入订单开始时间即可三小时遍历
     */
    @ApiOperation(value = "更新唯品会时间段内的订单", notes = "更新唯品会时间段内的订单", response = HttpMessage.class)
    @GetMapping(value = "/updataOrder")
    public void updataOrder(@RequestParam String start_time, @RequestParam String end_time) throws Exception {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long endTime = df.parse("2019-12-31 23:59:59").getTime();
        Date start = df.parse("2019-12-03 18:00:00");
        if (!StringUtils.isEmpty(start_time)) {
            start = df.parse(start_time);
        }
        if (!StringUtils.isEmpty(end_time)){
            Date end = df.parse(end_time);
            endTime = end.getTime();
        }
        long starttime = start.getTime();
        while (starttime < endTime) {
            long endtime = starttime + 24 * 60 * 60 * 1000;//一天
            Date date1 = new Date(starttime);
            Date date2 = new Date(endtime);
            String format1 = df.format(date1);
            String format2 = df.format(date2);
            wphService.wphOrder(String.valueOf(starttime), String.valueOf(endtime), null);
            System.out.println("******* start:" + format1 + "——end:" + format2 + "*******");
            Thread.sleep(3000);
            starttime = endtime;
        }
    }

    /**
     * 唯品会关键词查询商品
     */
    @ApiOperation(value = "唯品会关键词查询商品", notes = "唯品会关键词查询商品", response = HttpMessage.class)
    @PostMapping("/query")
    public Tip wphQuery(@ApiParam(value = "关键字") @RequestParam(value = "keyword") String keyword,
                        @ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                        @ApiParam(value = "0-正序，1-逆序") @RequestParam(value = "order") Integer order,
                        @ApiParam(value = "排序字段：支持price和discount") @RequestParam(value = "fieldname") String fieldname,
                        @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id) throws Exception {
        Tip data = wphService.wphQuery(page, keyword, order, fieldname, user_id);
        return data;
    }

    /**
     * 唯品会链接生成购买链接（分享自购）
     **/
    @ApiOperation(value = "唯品会链接生成购买链接（分享自购）", notes = "唯品会链接生成购买链接（分享自购）", response = HttpMessage.class)
    @PostMapping("/generateLink")
    public Tip generateLink(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                            @ApiParam(value = "商品id") @RequestParam(value = "goods_id") String goods_id,
                            @ApiParam(value = "购买类型 0自购 ，1来自分享") @RequestParam(value = "buy_type") Integer buy_type) throws Exception {
        Tip data = wphService.wphGenerateLink(user_id, goods_id, buy_type);
        return data;
    }

    /**
     * 唯品会商品详情
     **/
    @ApiOperation(value = "商品详情", notes = "商品详情", response = HttpMessage.class)
    @GetMapping("/goodsDetail")
    public Tip wphGoodsDetail(@RequestParam("goods_id") String goods_id,
                               @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id,
                              @RequestParam(name = "deviceT", required = false) String deviceT,
                              @RequestParam(name = "app_version", required = false) String app_version, HttpServletRequest request) throws Exception {
        Tip data = wphService.wphGoodsDetail(goods_id,user_id,request,deviceT,app_version);
        return data;
    }


}
