package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.impl.ExpressTradeServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信，支付宝支付接口
 * Created by 86514 on 2019/7/28.
 */
@Api(tags = "物流接口", description = "订单物流详情接口")
@RestController
@RequestMapping("/api/v2/htsq/express_order")
public class ExpressController {
    @Autowired
    private ExpressTradeServiceImpl expressTradeService;
    /**
     * 支付宝支付
     **/
    @ApiOperation(value = "物流JSON", notes = "订单物流详情", response = HttpMessage.class)
    @PostMapping("/query")
    public Tip alipay(@ApiParam(value = "物流号")  @RequestParam(value = "express_id") String express_id,
                      @ApiParam(value = "订单号")  @RequestParam(value = "order_id") String order_id) throws Exception {
        Tip data = expressTradeService.queryExpress(order_id,express_id);
        return data;
    }
}
