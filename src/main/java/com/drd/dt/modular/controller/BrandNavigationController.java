package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.BrandNavigationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 品牌导航
 * Created by 86514 on 2019/7/28.
 */
@Api(tags = "品牌导航", description = "品牌导航接口")
@RestController
@RequestMapping("/api/v2/htsq/brand")
public class BrandNavigationController {

    @Autowired
    private BrandNavigationService brandNavigationService;

    /**
     * 获取品牌导航列表
     **/
    @ApiOperation(value = "获取品牌导航列表信息", notes = "获取品牌导航列表信息", response = HttpMessage.class)
    @PostMapping("/stuad")
    public Tip brandList(@ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                         @ApiParam(value = "类型(1品牌  2旗舰店)") @RequestParam(value = "type") Integer type) throws Exception{
        Tip data = brandNavigationService.brandList(page,type);
        return data;
    }

}
