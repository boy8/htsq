package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.PayDTO;
import com.drd.dt.modular.service.PayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 微信，支付宝支付接口
 * Created by 86514 on 2019/7/28.
 */
@Api(tags = "支付接口", description = "微信，支付宝支付接口")
@RestController
@RequestMapping("/api/v2/htsq/pay")
public class PayController {

    @Autowired
    private PayService payService;

    /**
     * 支付宝支付
     **/
    @ApiOperation(value = "支付宝支付", notes = "支付宝支付", response = HttpMessage.class)
    @PostMapping("/alipay")
    public Tip alipay(PayDTO payDTO) throws Exception {
        Tip data = payService.alipay(payDTO);
        return data;
    }
    @ApiOperation(value = "通过礼包订单找回", notes = "通过礼包订单找回", response = HttpMessage.class)
    @GetMapping(value = "/findGiftOrder")
    public Tip findGiftOrder(@ApiParam(value = "订单号") @RequestParam(value = "order_id") String order_id
    ) throws Exception {
        Tip data = payService.findOrder(order_id);
        return data;
    }
    /**
     * H5 支付接口
     **/
    @ApiOperation(value = "H5 支付接口", notes = "H5 支付接口", response = HttpMessage.class)
    @PostMapping("/alipayH5")
    public void alipayH5(HttpServletResponse response, HttpServletRequest request, @RequestBody PayDTO payDTO) throws Exception {
        payService.alipayH5(response, request, payDTO);
    }


    /**
     * H5 支付宝异步通知回调
     */
    @ApiOperation(value = "支付宝异步通知回调", notes = "支付宝异步通知回调", response = HttpMessage.class)
    @PostMapping("/alipayCallback")
    public void alipayPayCallback(HttpServletResponse response, HttpServletRequest request) throws Exception {
        payService.alipayPayCallback(response, request);

    }

    /**
     * 微信支付
     **/
    @ApiOperation(value = "微信支付", notes = "微信支付", response = HttpMessage.class)
    @PostMapping("/WXPay")
    public Tip wxpay(PayDTO payDTO) throws Exception {
        Tip data = payService.wxpay(payDTO);
        return data;
    }


    /**
     * 接收微信支付异步通知回调地址
     **/
    @ApiOperation(value = "微信支付异步通知回调地址", notes = "微信支付异步通知回调地址", response = HttpMessage.class)
    @RequestMapping("/WXPayCallback")
    public void wxPayCallback(HttpServletResponse response, HttpServletRequest request) throws Exception {
        payService.wxPayCallback(response, request);
    }


    /**
     * 微信支付H5
     **/
    @ApiOperation(value = "微信支付H5", notes = "微信支付H5", response = HttpMessage.class)
    @PostMapping("/wxpayH5")
    public void wxpayH5(HttpServletResponse response, HttpServletRequest request, @RequestBody PayDTO payDTO) throws Exception {
       payService.wxpayH5(response,request,payDTO);

    }


}
