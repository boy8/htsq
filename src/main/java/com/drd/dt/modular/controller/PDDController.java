package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.PDDService;
import com.drd.dt.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 拼多多商品和订单
 * 2019-3-18 tanhao
 **/
@Api(tags = "拼多多", description = "拼多多商品和订单接口")
@RestController
@RequestMapping("/api/v2/htsq/pdd")
public class PDDController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private PDDService pddService;

    @ApiOperation(value = "同步最新订单", notes = "同步最新订单", response = HttpMessage.class)
    @GetMapping("/getOrderListByCustomer")
    public Tip getOrderListByCustomer(@ApiParam(value = "开始时间(1用到秒时间戳 2用正常时间格式)") @RequestParam(value = "start_time") String start_time,
                                      @ApiParam(value = "结束时间(1用到秒时间戳 2用正常时间格式)") @RequestParam(value = "end_time") String end_time,
                                      @ApiParam(value = "订单更新方式(1:更新时间查 2:支付时间查)") @RequestParam(value = "type",required = false) String type) throws Exception {
        Tip data = pddService.getOrderListByCustomer(start_time, end_time,type);
        return data;
    }

    /**
     * 更新拼多多时间段内的订单
     * 只需填入订单开始时间即可三小时遍历
     */
    @ApiOperation(value = "更新拼多多时间段内的订单", notes = "更新拼多多时间段内的订单", response = HttpMessage.class)
    @GetMapping(value = "/updataOrder")
    public void updataOrder(@RequestParam String start_time, @RequestParam String end_time) throws Exception {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long endTime = df.parse("2019-12-31 23:59:59").getTime();
        Date start = df.parse("2019-12-03 18:00:00");
        if (!StringUtils.isEmpty(start_time)) {
            start = df.parse(start_time);
        }
        if (!StringUtils.isEmpty(end_time)){
            Date end = df.parse(end_time);
            endTime = end.getTime();
        }
        long starttime = start.getTime();
        while (starttime < endTime) {
            long endtime = starttime + 24 * 60 * 60 * 1000;//一天
            Date date1 = new Date(starttime);
            Date date2 = new Date(endtime);
            String format1 = df.format(date1);
            String format2 = df.format(date2);
            pddService.getOrderListByCustomer(format1, format2,"2");//支付时间查(到秒的时间戳)
            System.out.println("******* start:" + format1 + "——end:" + format2 + "*******");
            Thread.sleep(3000);
            starttime = endtime;
        }
    }

    /**
     * 拼多多搜索
     **/
    @ApiOperation(value = "拼多多搜索", notes = "拼多多搜索", response = HttpMessage.class)
    @RequestMapping("/goodsSearch")
    public Tip goodsSearch(@ApiParam(value = "搜索关键词") @RequestParam(value = "keywords") String keywords,
                           @ApiParam(value = "页数") @RequestParam(value = "page") String page,
                           @ApiParam(value = "0-综合排序;1-按佣金比率升序;" +
                                   "        2-按佣金比例降序;3-按价格升序;4-按价格降序;5-按销量升序;6-按销量降序;7-优惠券金额排序升序;" +
                                   "        8-优惠券金额排序降序;9-券后价升序排序;10-券后价降序排序;") @RequestParam(value = "sort_type") String sort_type,
                           @ApiParam(value = "是否有券 有券：true  无券：false") @RequestParam(value = "ifcoupon") String ifcoupon,
                           @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id
    ) throws Exception {
        Tip data = pddService.goodsSearch(keywords, page, sort_type, ifcoupon,user_id);
        return data;
    }

    /**
     * 拼多多商品详情
     */
    @ApiOperation(value = "拼多多商品详情", notes = "拼多多商品详情", response = HttpMessage.class)
    @GetMapping("/goodsDetail")
    public Tip goodsDetail(@ApiParam(value = "商品id") @RequestParam(value = "goods_id") String goods_id,
                           @ApiParam(value = "用户id") @RequestParam(name = "user_id", required = false) Integer user_id,
                           @RequestParam(name = "deviceT", required = false) String deviceT,
                           @RequestParam(name = "app_version", required = false) String app_version, HttpServletRequest request) throws Exception {
        Tip data = pddService.goodsDetail(goods_id,user_id, request,deviceT,app_version);
        return data;
    }

    /**
     * 获取拼多多推广位链接 （分享自购）
     */
    @ApiOperation(value = "获取拼多多推广位链接（分享自购）", notes = "获取拼多多推广位链接（分享自购）", response = HttpMessage.class)
    @GetMapping("/getPddUrl")
    public Tip getPddUrl(@ApiParam(value = "用户id") @RequestParam(value = "user_id") String user_id,
                         @ApiParam(value = "商品id") @RequestParam(value = "goods_id") String goods_id,
                         @ApiParam(value = "购买类型 0自购 1分享购") @RequestParam(value = "buytype") String buytype
    ) throws Exception {
        Tip data = pddService.getPddUrl(user_id, goods_id, buytype);
        return data;
    }

    /**
     * 获取拼多多主题推广活动主题id
     */
    @ApiOperation(value = "获取拼多多主题推广活动主题id", notes = "获取拼多多主题推广活动主题id", response = HttpMessage.class)
    @GetMapping("/getThemeid")
    public Tip getThemeid(@ApiParam(value = "主题名称") @RequestParam(value = "name") String name) throws Exception {
        Tip data = pddService.getThemeid(name);
        return data;
    }

    /**
     * 拼多多用户备案链接
     * **/
    @ApiOperation(value = "拼多多用户备案链接", notes = "拼多多用户备案链接", response = HttpMessage.class)
    @GetMapping("/pddPutOnRecords")
    public Tip pddPutOnRecords(@ApiParam(value = "用户id") @RequestParam(value = "user_id") String user_id) throws Exception {
        Tip data = pddService.pddPutOnRecords(user_id);
        return data;
    }


    /**
     * 拼多多-搜索接口-pid 备案
     * **/
    @ApiOperation(value = "拼多多-搜索接口-pid 备案", notes = "200表示已经备案，600表示需要备案", response = HttpMessage.class)
    @GetMapping("/getIsArchives")
    public Tip getIsArchives(HttpServletRequest request) throws Exception {
        String ip = StringUtil.getIpAddr(request);
        Tip data = pddService.getIsArchives2(ip);
        return data;
    }

}
