package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.filter.NoRepeatSubmit;
import com.drd.dt.modular.dto.UserloginDTO;
import com.drd.dt.modular.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 每日任务
 **/
@Api(tags = "每日任务", description = "每日任务相关接口")
@RestController
@RequestMapping("/api/v2/htsq/task")
public class TaskController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TaskService taskService;

    /**
     * 获取任务中心信息
     **/
    @ApiOperation(value = "获取任务中心信息", notes = "获取任务中心信息", response = HttpMessage.class)
    @NoRepeatSubmit
    @PostMapping("/taskCenterInfo")
    public Tip taskCenterInfo(@ApiParam(value = "用户user_id") @RequestParam(name = "user_id") Integer user_id,
                    @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id) throws Exception {
        Tip tip = taskService.taskCenterInfo(user_id, token_id);
        return tip;
    }

    /**
     * 用户完成任务
     **/
    @ApiOperation(value = "用户完成任务", notes = "用户完成任务", response = HttpMessage.class)
    @PostMapping("/finish")
    public Tip finish(@ApiParam(value = "用户user_id") @RequestParam(name = "user_id") Integer user_id,
                      @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                      @ApiParam(value = "任务id") @RequestParam(value = "task_id") String task_id,
                      @ApiParam(value = "是否双倍奖励 1是 0否") @RequestParam(value = "is_double") Integer is_double,
                      @ApiParam(value = "1基础任务 2每日任务") @RequestParam(name = "task_type") Integer task_type) throws Exception {
        Tip tip = taskService.finish(user_id, token_id, task_id,is_double,task_type);
        return tip;
    }

    /**
     * 查询用户的任务详情
     **/
    @ApiOperation(value = "查询用户的任务状态", notes = "查询用户的任务状态", response = HttpMessage.class)
    @PostMapping("/detail")
    public Tip detail(@ApiParam(value = "用户user_id") @RequestParam(name = "user_id") Integer user_id,
                      @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                      @ApiParam(value = "任务id") @RequestParam(value = "task_id") Integer task_id,
                      @ApiParam(value = "1基础任务 2每日任务") @RequestParam(name = "task_type") Integer task_type) throws Exception {
        Tip tip = taskService.detail(user_id, token_id, task_id,task_type);
        return tip;
    }

    /**
     * 用户金币兑换
     **/
    @ApiOperation(value = "用户金币兑换", notes = "用户金币兑换", response = HttpMessage.class)
    @PostMapping("/exchange")
    public Tip exchange(@ApiParam(value = "用户user_id") @RequestParam(name = "user_id") Integer user_id,
                      @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                      @ApiParam(value = "金币数量") @RequestParam(value = "coin") Integer coin) throws Exception {
        Tip tip = taskService.exchange(user_id, token_id, coin);
        return tip;
    }

    /**
     * 用户金币兑换历史记录列表
     **/
    @ApiOperation(value = "用户金币兑换历史记录列表", notes = "用户金币兑换历史记录列表", response = HttpMessage.class)
    @GetMapping("/exchangeHistory")
    public Tip exchangeHistory(@ApiParam(value = "用户user_id") @RequestParam(name = "user_id") Integer user_id,
                               @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                               @ApiParam(value = "页数") @RequestParam(name = "page") Integer page,
                               @ApiParam(value = "类型：1-任务收入， 2-支出，3-粉丝金币收入") @RequestParam(name = "type",required = false) Integer type
    ) throws Exception {
        Tip tip = taskService.exchangeHistory(user_id, token_id, page,type);
        return tip;
    }

    /**
     * 用户金币兑换历史记录金币信息
     **/
    @ApiOperation(value = "用户金币兑换历史记录金币信息", notes = "用户金币兑换历史记录金币信息", response = HttpMessage.class)
    @GetMapping("/exchangeCoinInfo")
    public Tip exchangeCoinInfo(@ApiParam(value = "用户user_id") @RequestParam(name = "user_id") Integer user_id,
                               @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id
    ) throws Exception {
        Tip tip = taskService.exchangeCoinInfo(user_id, token_id);
        return tip;
    }

    /**
     * 粉丝金币贡献列表记录
     **/
    @ApiOperation(value = "粉丝金币贡献列表记录", notes = "粉丝金币贡献列表记录", response = HttpMessage.class)
    @GetMapping("/fansContributionList")
    public Tip fansContributionList(@ApiParam(value = "用户user_id") @RequestParam(name = "user_id") Integer user_id,
                                    @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                                    @ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                                    @ApiParam(value = "类型（1贡献榜  2今日贡献榜）") @RequestParam(value = "type") Integer type
                               ) throws Exception {
        Tip tip = taskService.fansContributionList(user_id, token_id,page,type);
        return tip;
    }
}
