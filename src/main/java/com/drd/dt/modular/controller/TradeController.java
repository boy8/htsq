package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.TXSuggestDTO;
import com.drd.dt.modular.service.TradeService;
import com.drd.dt.util.HttpConnectionPoolUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by 86514 on 2019/3/25.
 */

@Api(tags = "提现", description = "提现相关接口")
@RestController
@RequestMapping("/Trade")
public class TradeController {

    @Autowired
    private TradeService tradesService;

    /**
     * 我的提现列表
     **/
    @ApiOperation(value = "我的提现列表", notes = "我的提现列表", response = HttpMessage.class)
    @PostMapping("/tx")
    public Tip tx(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                  @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                  @ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                  @ApiParam(value = "提现类型(0全部 1普通佣金  2奖励金)") @RequestParam(value = "type",required = false) String type) throws Exception {
            Tip data = tradesService.tx(user_id, token_id, page,type);
        return data;
    }

    /**
     * 创建提现申请
     **/
    @ApiOperation(value = "创建提现申请", notes = "创建提现申请", response = HttpMessage.class)
    @GetMapping("/addTx")
    public Tip addTx(TXSuggestDTO txSuggestDTO) throws Exception {
            Tip data = tradesService.addTx(txSuggestDTO);
            return data;
    }
//
//    /**
//     * 2021-05-06 蚂蚁金服提示:接口更新金额-接口升级完毕 测试 最小金额0.5 最大500+
//     * */
//    @ApiOperation(value = "直接转账接口(不许非开发人员使用)", notes = "直接转账接口(不许非开发人员使用)", response = HttpMessage.class)
//    @GetMapping("/testTX")
//    public Tip testTX(@ApiParam(value = "收款人手机号") @RequestParam(value = "phone") String phone,
//                      @ApiParam(value = "收款人名称") @RequestParam(value = "name") String name,
//                      @ApiParam(value = "金额") @RequestParam(value = "money") Double money )  {
//        Tip data = tradesService.testTX(phone,name,money);
//        return data;
//    }
}
