package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.ForVolumeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 365淘卷
 * Created by 86514 on 2020/1/10 HL
 */
@Api(tags = "365淘卷", description = "365淘卷")
@RestController
@RequestMapping("/api/v2/htsq/forVolume")
public class ForVolumeController {
    @Autowired
    private ForVolumeService forVolumeHome;

    /**
     * 获取365H 5界面
     * @param user_id   用户ID
     * @param productNo 365淘券品牌id，用于单品牌跳转 品牌参数列表请咨询商务）
     */
    @ApiOperation(value = "获取365 淘卷", notes = "获取365 淘卷", response = HttpMessage.class)
    @RequestMapping("/forVolumeHome")
    public Tip forVolumeHome(@ApiParam(value = "用户id") @RequestParam(value = "user_id") String user_id,
                             @ApiParam(value = "365淘券品牌id，用于单品牌跳转 品牌参数列表请咨询商务）") @RequestParam(value = "productNo", required = false) String productNo) throws Exception {
        return forVolumeHome.getForVolumeHome(user_id, productNo);
    }

    /**
     * 订单接口
     *
     * @param user_id 用户ID
     */
    @ApiOperation(value = "订单接口", notes = "订单接口", response = HttpMessage.class)
    @RequestMapping("/myOrder")
    public Tip myOrder(@ApiParam(value = "用户id") @RequestParam(value = "user_id") String user_id) throws Exception {
        return forVolumeHome.getMyOrder(user_id);
    }
}
