package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.UpdateUserInfoDTO;
import com.drd.dt.modular.dto.UserloginDTO;
import com.drd.dt.modular.dto.VerifyDTO;
import com.drd.dt.modular.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用户接口
 **/
@Api(tags = "用户", description = "用户相关接口")
@RestController
@RequestMapping("/api/v2/htsq/user")
public class UserController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    /**
     * 微信登陆
     **/
    @ApiOperation(value = "第三方登陆(微信登陆)", notes = "第三方登陆(微信登陆)", response = HttpMessage.class)
    @PostMapping("/wxLogin")
    public Tip thirdLogin(UserloginDTO userloginDTO, HttpServletRequest request) throws Exception {
        Tip tip = userService.thirdLogin(userloginDTO, request);
        return tip;
    }

    /**
     * 手机号登陆
     **/
    @ApiOperation(value = "手机号登陆", notes = "手机号登陆", response = HttpMessage.class)
    @PostMapping("/codeLogin")
    public Tip login(HttpServletRequest request, UserloginDTO userloginDTO) throws Exception {
        Tip data = userService.codeLogin(request, userloginDTO);
        return data;
    }

    /**
     * 秒验接口
     **/
    @ApiOperation(value = "秒验接口", notes = "秒验接口", response = HttpMessage.class)
    @PostMapping("/verify")
    public Tip verify(HttpServletRequest request,VerifyDTO verifyDTO) throws Exception {
        Tip data = userService.verify(request,verifyDTO);
        return data;
    }

    /**
     * 根据邀请码获取用户信息
     **/
    @ApiOperation(value = "根据邀请码获取用户信息", notes = "根据邀请码获取用户信息", response = HttpMessage.class)
    @GetMapping("/codeInfo")
    public Tip codeInfo(@ApiParam(value = "邀请码") @RequestParam(value = "code") String code) throws Exception {
        Tip data = userService.codeInfo(code);
        return data;
    }

    /**
     * 根据手机号绑定邀请码
     **/
    @ApiOperation(value = "根据手机号绑定邀请码", notes = "根据手机号绑定邀请码", response = HttpMessage.class)
    @GetMapping("/codeBind")
    public Tip codeBind(@ApiParam(value = "邀请码") @RequestParam(value = "code") String code,
                        @ApiParam(value = "手机号") @RequestParam(value = "phone") String phone) throws Exception {
        Tip data = userService.codeBind(code, phone);
        return data;
    }

    /**
     * 发短信获取验证码
     **/
    @ApiOperation(value = "发短信获取验证码", notes = "发短信获取验证码", response = HttpMessage.class)
    @GetMapping("/sendVerificationCode")
    public Tip sendVerificationCode(@ApiParam(value = "电话") @RequestParam(value = "phone") String phone) throws Exception {
        Tip data = userService.sendVerificationCode(phone);
        return data;
    }

    /**
     * 获取个人会员信息
     */
    @ApiOperation(value = "个人会员信息", notes = "个人会员信息", response = HttpMessage.class)
    @PostMapping("/personalVipInfo")
    public Tip getVipInfo(@ApiParam(value = "用户user_id") @RequestParam(name = "user_id") int user_id,
                          @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id) throws Exception {
        Tip data = userService.getVipInfo(user_id, token_id);
        return data;
    }

    /**
     * 个人中心-用户信息
     **/
    @ApiOperation(value = "个人中心-用户信息", notes = "个人中心-用户信息", response = HttpMessage.class)
    @RequestMapping("/userInfo")
    public Tip userInfo(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                        @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                        @RequestParam(name = "deviceT", required = false) String deviceT,
                        @RequestParam(name = "app_version", required = false) String app_version, HttpServletRequest request) throws Exception {
        Tip data = userService.userInfo(user_id, token_id, deviceT, app_version, request);
        return data;
    }

    /**
     * 个人中心-修改用户信息
     **/
    @ApiOperation(value = "个人中心-修改用户信息", notes = "个人中心-修改用户信息", response = HttpMessage.class)
    @PostMapping("/updateUser")
    public Tip updateUser(UpdateUserInfoDTO userInfoDTO) throws Exception {
        Tip data = userService.updateUser(userInfoDTO);
        return data;
    }

    /**
     * 个人中心-我的粉丝
     **/
    @ApiOperation(value = "个人中心-我的粉丝", notes = "个人中心-我的粉丝", response = HttpMessage.class)
    @GetMapping("/myFans")
    public Tip myFans(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                      @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                      @ApiParam(value = "分页") @RequestParam(value = "page",required = false) Integer page,
                      @ApiParam(value = "类型(0所有 1直属粉丝 2普通粉丝)") @RequestParam(value = "type",required = false) Integer type,
                      @ApiParam(value = "创建时间排序(1升序 2降序)") @RequestParam(value = "time",required = false) String time,
                      @ApiParam(value = "活跃度排序(1升序 2降序)") @RequestParam(value = "active",required = false) String active,
                      @ApiParam(value = "粉丝数排序(1升序 2降序)") @RequestParam(value = "fans_num",required = false) String fans_num,
                      @ApiParam(value = "vip筛选(1只查看vip)") @RequestParam(value = "is_vip",required = false) String is_vip) throws Exception {
//        Tip data = userService.myFans(user_id, token_id,page,type);
        Tip data = userService.myFansPage(user_id, token_id,page,type,time,fans_num,is_vip,active);
        return data;
    }

    /**
     * 个人中心-我的粉丝-粉丝详情
     **/
    @ApiOperation(value = "个人中心-我的粉丝-粉丝详情", notes = "个人中心-我的粉丝-粉丝详情", response = HttpMessage.class)
    @GetMapping("/myFansInfo")
    public Tip myFansInfo(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                          @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                          @ApiParam(value = "粉丝的user_id") @RequestParam(value = "fans_id") Integer fans_id) throws Exception {
        Tip data = userService.myFansInfo(user_id, token_id, fans_id);
        return data;
    }

    /**
     * 个人中心-我的订单
     **/
    @ApiOperation(value = "个人中心-我的订单", notes = "个人中心-我的订单", response = HttpMessage.class)
    @RequestMapping("/myOrder")
    public Tip myOrder(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                       @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                       @ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                       @ApiParam(value = "订单类型 (1淘宝,2京东，3拼多多，4唯品会，5自营，6粉丝订单)(为空默认为所有三方订单)") @RequestParam(value = "type", required = false) Integer type,
                       @ApiParam(value = "粉丝订单：1普通订单 2礼包订单") @RequestParam(value = "order_type", required = false) Integer order_type,
                       @ApiParam(value = "订单状态 (1-订单结算(已完成) 2-待结算(待收货) 3待发货)(为空默认为所有状态)") @RequestParam(value = "status", required = false) Integer status) throws Exception {
        Tip data = userService.myOrders(user_id, token_id, page, type, status, order_type);
        return data;
    }

    /**
     * 个人中心-龙虎榜
     **/
    @ApiOperation(value = "个人中心-龙虎榜", notes = "个人中心-龙虎榜", response = HttpMessage.class)
    @GetMapping("/rank")
    public Tip rank(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                    @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                    @ApiParam(value = "分页") @RequestParam(value = "page", required = false) Integer page,
                    @ApiParam(value = "类型(0所有 1直属粉丝 2普通粉丝)") @RequestParam(value = "type", required = false) Integer type
    ) throws Exception {
//        Tip data = userService.rank(user_id, token_id,page,type);//个人中心-龙虎榜（redis分页）
        Tip data = userService.rankPage(user_id, token_id, page, type);//个人中心-龙虎榜（数据库分页）
        return data;
    }

    /**
     * 个人中心-我的收益
     **/
    @ApiOperation(value = "个人中心-我的收益", notes = "个人中心-我的收益", response = HttpMessage.class)
    @GetMapping("/profit")
    public Tip profit(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                      @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id
    ) throws Exception {
        Tip data = userService.profit(user_id, token_id);
        return data;
    }

    /**
     * 个人中心-我的收益(新)
     **/
    @ApiOperation(value = "个人中心-我的收益(新)", notes = "个人中心-我的收益(新)", response = HttpMessage.class)
    @GetMapping("/myProfit")
    public Tip myProfit(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                        @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                        @ApiParam(value = "时间(1今日 2昨日 3本月 4上月)") @RequestParam(value = "time_type") Integer time_type
    ) throws Exception {
        Tip data = userService.myProfit(user_id, token_id,time_type);
        return data;
    }

    /**
     * 个人中心-邀请好友
     **/
    @ApiOperation(value = "个人中心-邀请好友", notes = "个人中心-邀请好友", response = HttpMessage.class)
    @GetMapping("/invite")
    public Tip invite(HttpServletRequest request,
                      @ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                      @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                      @ApiParam(value = "客户端类型（Android:0  IOS:1）") @RequestParam(value = "deviceT") Integer deviceT
    ) throws Exception {
        Tip data = userService.invite(user_id, token_id, deviceT, request);
        return data;
    }

    /**
     * 个人中心-邀请好友-扫描二维码
     **/
    @ApiOperation(value = "个人中心-邀请好友-扫描二维码", notes = "个人中心-邀请好友-扫描二维码", response = HttpMessage.class)
    @GetMapping("/scanQR")
    public void scanQR(HttpServletResponse response,HttpServletRequest request,
                       @ApiParam(value = "邀请码") @RequestParam(value = "friend_code",required = false) String friend_code,
                       @ApiParam(value = "用户id") @RequestParam(value = "user_id",required = false) Integer user_id
    ) throws Exception {
        userService.scanQR(response,request,friend_code,user_id);
    }

    /**
     * 扫描二维码-存用户微信信息
     **/
    @ApiOperation(value = "扫描二维码-存用户微信信息", notes = "扫描二维码-存用户微信信息", response = HttpMessage.class)
    @PostMapping("/saveScanInfo")
    public Tip saveScanInfo(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                            @ApiParam(value = "微信code") @RequestParam(value = "code") String code
    ) throws Exception {
        Tip data = userService.saveScanInfo(user_id,code);
        return data;
    }

    /**
     * 个人中心-常见问题-常见问题
     *
     * @return
     */
    @ApiOperation(value = "个人中心-常见问题-常见问题", notes = "个人中心-常见问题-常见问题", response = HttpMessage.class)
    @RequestMapping("/normalProblem")
    public Tip normalProblem() throws Exception {
        Tip data = userService.normalProblem();
        return data;
    }

    /**
     * 个人中心-常见问题-问题分类 列表
     *
     * @return
     */
    @ApiOperation(value = "个人中心-常见问题-问题分类 列表", notes = "个人中心-常见问题-问题分类 列表", response = HttpMessage.class)
    @RequestMapping("/problemType")
    public Tip problemType(@RequestParam(name = "deviceT", required = false) String deviceT,
                           @RequestParam(name = "app_version", required = false) String app_version, HttpServletRequest request) throws Exception {
        Tip data = userService.problemType(deviceT,app_version,request);
        return data;
    }

    /**
     * 个人中心-常见问题-问题分类 答案
     *
     * @return
     */
    @ApiOperation(value = "个人中心-常见问题-问题分类 答案", notes = "个人中心-常见问题-问题分类 答案", response = HttpMessage.class)
    @RequestMapping("/problemList")
    public Tip problemList(@ApiParam(value = "问题分类id") @RequestParam(value = "id") Integer id) throws Exception {
        Tip data = userService.problemList(id);
        return data;
    }

    /**
     * 个人中心-隐私政策
     **/
    @ApiOperation(value = "个人中心-隐私政策", notes = "个人中心-隐私政策", response = HttpMessage.class)
    @GetMapping("/privacyPolicy")
    public Tip privacyPolicy() throws Exception {
        Tip data = userService.privacyPolicy();
        return data;
    }

    /**
     * 个人中心-隐私政策
     **/
    @ApiOperation(value = "0元购权限", notes = "是否具备0元购权限", response = HttpMessage.class)
    @PostMapping("/checkZeroPolicy")
    public Tip checkZeroPolicy(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id) throws Exception {
        Tip data = userService.checkZeroPolicy(user_id);
        return data;
    }

    /***
     * VIP自营礼包 分享 自动注册
     * */
    @ApiOperation(value = "VIP自营礼包 分享 自动注册", notes = "VIP自营礼包 分享 自动注册", response = HttpMessage.class)
    @PostMapping("/vipLogin")
    public Tip vipLogin(HttpServletRequest request,@ApiParam(value = "分享人user_id") @RequestParam(value = "user_id") Integer user_id,
                        @ApiParam(value = "手机号") @RequestParam(value = "phone") String phone,
                        @ApiParam(value = "验证码") @RequestParam(value = "code") String code) throws Exception {
        Tip data = userService.vipLogin(request,user_id, phone, code);
        return data;
    }
}
