package com.drd.dt.modular.controller;

import com.drd.dt.back.dto.SiftGoodsDTO;
import com.drd.dt.back.dto.SiftOrderDTO;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dao.OrderMapper;
import com.drd.dt.modular.dao.SiftGoodsMapper;
import com.drd.dt.modular.dao.SiftOrderMapper;
import com.drd.dt.modular.dao.UserMoneyMapper;
import com.drd.dt.modular.dto.OwnGoodsOrderDTO;
import com.drd.dt.modular.entity.User;
import com.drd.dt.modular.service.ISiftOrderService;
import com.drd.dt.modular.service.PayService;
import com.drd.dt.modular.service.impl.UserMoneyServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * 精选核桃 商品 Controller层
 */
@Api(tags = "核桃精选订单", description = "精选核桃商品订单接口")
@RestController
@RequestMapping("/api/v2/htsq/siftOrder")
public class SiftOrderController {

    @Autowired
    private ISiftOrderService siftOrderService;
    @Autowired
    private UserMoneyServiceImpl userMoneyService;


    @Autowired
    private SiftOrderMapper siftOrderMapper;
    @Autowired
    private UserMoneyMapper userMoneyMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private SiftGoodsMapper siftGoodsMapper;

    /**
     * 生成订单
     */
    @ApiOperation(value = "生成订单", notes = "生成订单", response = HttpMessage.class)
    @PostMapping("/orderGift")
    public void addOrderGift(@RequestBody OwnGoodsOrderDTO ownGoodsOrderDTO, HttpServletResponse response, HttpServletRequest request) throws Exception {
        siftOrderService.addOrderGift(ownGoodsOrderDTO, response, request);
    }


    /**
     * 申请退款
     **/
    @ApiOperation(value = "申请退款", notes = "申请退款", response = HttpMessage.class)
    @PostMapping("/applyRefund")
    Tip applyRefund(@RequestParam(value = "user_id") Integer user_id,
                    @RequestParam(value = "token") String token,
                    @RequestParam(value = "order_id") String order_id,
                    @RequestParam(value = "cause") String cause,
                    @RequestParam(value = "log_image") String log_image) {
        return siftOrderService.applyRefund(user_id, token, order_id, cause, log_image);
    }

    /**
     * 取消退款
     **/
    @ApiOperation(value = "取消退款", notes = "取消退款", response = HttpMessage.class)
    @PostMapping("/cancelRefund")
    Tip cancelRefund(@RequestParam(value = "user_id") Integer user_id,
                    @RequestParam(value = "token") String token,
                    @RequestParam(value = "order_id") String order_id) {
        return siftOrderService.cancelRefund(user_id, token, order_id);
    }


    /**
     * 核桃精选商品订单详情
     */
    @ApiOperation(value = "订单详情", notes = "订单详情", response = HttpMessage.class)
    @PostMapping("/getSiftOrderDetail")
    public Tip getSiftOrderDetail(@ApiParam(value = "订单号") @RequestParam(value = "order_id") String order_id) throws Exception {
        return siftOrderService.getSiftOrderDetail(order_id);
    }

    /**
     * 核桃售后  - 售后申请/售后处理中
     * **/
    @ApiOperation(value = "核桃售后  - 售后申请/售后处理中", notes = "核桃售后  - 售后申请/售后处理中", response = HttpMessage.class)
    @PostMapping("/afterSale")
    public Tip afterSale(@RequestParam(value = "user_id") Integer user_id,
                         @RequestParam(value = "token") String token,
                         @RequestParam(value = "refund_status") Integer refund_status) throws Exception {
        return siftOrderService.afterSale(user_id,token,refund_status);
    }

    /**
     * 折扣专区 判断当前用户是否有权限购买并且只能买一个
     */
    @ApiOperation(value = "折扣专区 判断是否是新用户，并且只能购买一次", notes = "返回data:-1 没有购买资格    0 可以购买  1  已经购买过一次", response = HttpMessage.class)
    @PostMapping("/discountClassifyQualification")
    public Tip discountClassifyQualification(@RequestParam(value = "user_id") Integer user_id,
                         @RequestParam(value = "goods_id") String goods_id) throws Exception {
        return siftOrderService.discountClassifyQualification(user_id,goods_id);
    }

}
