package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.OwnGoodsOrderDTO;
import com.drd.dt.modular.service.HtmlOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/***
 * 脱离核桃用户ID H5
 * HL 2020/7/2
 */
@Api(tags = "脱离核桃用户ID H5", description = "脱离核桃用户ID H5")
@RestController
@RequestMapping("/api/v2/htsq/htmlOrder")
public class HtmlOrderController {

    @Autowired
    private HtmlOrderService htmlOrderService;

    //商品列表
    @ApiOperation(value = "所有商品列表  不 分页", notes = "所有商品列表  不 分页", response = HttpMessage.class)
    @RequestMapping(value = "/goodsListNoPage", method = {RequestMethod.POST, RequestMethod.GET})
    public Tip goodsListNoPage() throws Exception {
        return htmlOrderService.goodsListNoPage();
    }

    //商品列表
    @ApiOperation(value = "所有商品列表  分页", notes = "所有商品列表 分页", response = HttpMessage.class)
    @RequestMapping(value = "/goodsCouponsListNoPage", method = {RequestMethod.POST, RequestMethod.GET})
    public Tip goodsCouponsListNoPage(@RequestParam(name = "index") Integer index,
                                      @RequestParam(name = "coupons_price") int coupons_price) throws Exception {
        return htmlOrderService.goodsCouponsListNoPage(index,coupons_price);
    }


    //商品详情
    @ApiOperation(value = "获取商品详情", notes = "获取商品详情", response = HttpMessage.class)
    @RequestMapping(value = "/goodsDetails", method = {RequestMethod.POST, RequestMethod.GET})
    public Tip goodsDetails(@RequestParam int dealer_id) throws Exception {
        return htmlOrderService.goodsDetails(dealer_id);
    }

    //商品规格
    @ApiOperation(value = "商品规格", notes = "获取商品规格", response = HttpMessage.class)
    @RequestMapping(value = "/goodsSpecificationDetails", method = {RequestMethod.POST, RequestMethod.GET})
    public Tip goodsSpecificationDetails(@ApiParam(value = "商品ID") @RequestParam(value = "goods_id") Integer goods_id) throws Exception {
        Tip data = htmlOrderService.goodsSpecificationDetails(goods_id);
        return data;
    }

    //点击购买-- 生成订单未付款状态
    @ApiOperation(value = "生成订单", notes = "生成订单", response = HttpMessage.class)
    @RequestMapping(value = "/orderGift", method = {RequestMethod.POST})
    public void addOrderGift(@RequestBody OwnGoodsOrderDTO ownGoodsOrderDTO, HttpServletResponse response, HttpServletRequest request) throws Exception {
        try {
            htmlOrderService.addOrderGift(ownGoodsOrderDTO, response, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //购买成功/失败 回调
    //支付宝-回调
    @ApiOperation(value = "支付宝异步通知回调", notes = "支付宝异步通知回调", response = HttpMessage.class)
    @RequestMapping(value = "/alipayCallback", method = {RequestMethod.POST, RequestMethod.GET})
    public void alipayPayCallback(HttpServletResponse response, HttpServletRequest request) throws Exception {
        try {
            htmlOrderService.alipayPayCallback(response, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiOperation(value = "微信支付异步通知回调地址", notes = "微信支付异步通知回调地址", response = HttpMessage.class)
    @RequestMapping(value = "/WXPayCallback", method = {RequestMethod.POST, RequestMethod.GET})
    public void wxPayCallback(HttpServletResponse response, HttpServletRequest request) throws Exception {
        try {
            htmlOrderService.wxPayCallback(response, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //订单详情
    @ApiOperation(value = "订单详情", notes = "订单详情", response = HttpMessage.class)
    @RequestMapping(value = "/orderGiftDetail", method = {RequestMethod.POST, RequestMethod.GET})
    public Tip orderGiftDetail(@ApiParam(value = "订单号") @RequestParam(value = "order_id") String order_id) {
        return htmlOrderService.orderGiftDetail(order_id);
    }




}
