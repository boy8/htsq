package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.ITBOrdersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by 86514 on 2019/3/25.
 */

@Api(tags = "淘宝", description = "淘宝订单")
@RestController
@RequestMapping("/api/v2/htsq/Order")
public class TBOrderController {

    @Autowired
    private ITBOrdersService tbOrdersService;

    /**
     * 通过淘宝联盟
     * 获取订单
     */
    @ApiOperation(value = "通过淘宝联盟获取订单", notes = "通过淘宝联盟获取订单", response = HttpMessage.class)
    @GetMapping(value = "/getUnionOrder")
    public Tip getOrderByUnion(@ApiParam(value = "开始时间") @RequestParam(value = "start_time") String start_time,
                               @ApiParam(value = "结束时间") @RequestParam(value = "end_time") String end_time,
                               @ApiParam(value = "推广者角色类型,2:二方，3:三方，不传，表示所有角色") @RequestParam(value = "member_type",required = false) String member_type,
                               @ApiParam(value = "场景订单场景类型，1:常规订单，2:渠道订单，3:会员运营订单，默认为1") @RequestParam(value = "order_scene",required = false) String order_scene,
                               @ApiParam(value = "查询时间类型，1订单淘客创建时间查询，2订单淘客付款时间查询，3订单淘客结算时间查询") @RequestParam(value = "query_type") String query_type) throws Exception {
        Tip data = tbOrdersService.getOrderByUnion(start_time, end_time, order_scene, member_type, query_type);
        return data;
    }

    /**
     * 618预售商品差价返
     */
    @ApiOperation(value = "618预售商品差价返", notes = "618预售商品差价返", response = HttpMessage.class)
    @GetMapping(value = "/update618order")
    public Tip update618order(@ApiParam(value = "开始时间") @RequestParam(value = "start_time") String start_time,
                               @ApiParam(value = "结束时间") @RequestParam(value = "end_time") String end_time,
                               @ApiParam(value = "推广者角色类型,2:二方，3:三方，不传，表示所有角色") @RequestParam(value = "member_type",required = false) String member_type,
                               @ApiParam(value = "场景订单场景类型，1:常规订单，2:渠道订单，3:会员运营订单，默认为1") @RequestParam(value = "order_scene",required = false) String order_scene,
                               @ApiParam(value = "查询时间类型，1订单淘客创建时间查询，2订单淘客付款时间查询，3订单淘客结算时间查询") @RequestParam(value = "query_type") String query_type) throws Exception {
        Tip data = tbOrdersService.update618order(start_time, end_time, order_scene, member_type, query_type);
        return data;
    }

    /**
     * 更新淘宝联盟时间段内的订单
     * 只需填入订单开始时间即可三小时遍历
     */
    @ApiOperation(value = "更新淘宝联盟时间段内的订单", notes = "更新淘宝联盟时间段内的订单", response = HttpMessage.class)
    @GetMapping(value = "/updataOrder")
    public void updataOrder(@RequestParam String start_time, @RequestParam String end_time,
                            @RequestParam String order_scene) throws Exception {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long endTime = df.parse("2019-12-31 23:59:59").getTime();//默认结束时间
        Date start = df.parse("2019-12-03 18:00:00");//默认开始时间
        if (!StringUtils.isEmpty(start_time)) {
            start = df.parse(start_time);
        }
        if (!StringUtils.isEmpty(end_time)){
            Date end = df.parse(end_time);
            endTime = end.getTime();
        }
        long starttime = start.getTime();
        while (starttime < endTime) {
            long endtime = starttime + 3 * 60 * 60 * 1000;//3小时
            Date date1 = new Date(starttime);
            Date date2 = new Date(endtime);
            String format1 = df.format(date1);
            String format2 = df.format(date2);
            tbOrdersService.getOrderByUnion(format1, format2, order_scene, "", "1");
            System.out.println("******* start:" + format1 + "——end:" + format2 + "*******");
            Thread.sleep(3000);
            starttime = endtime;
        }
    }

}
