package com.drd.dt.modular.controller;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.GoldCoinService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 我的金币
 * Created by 86514 on 2019/7/28.
 */
@Api(tags = "金币", description = "我的金币相关接口")
@RestController
@RequestMapping("/api/v2/htsq/coin")
public class GoldCoinController {

    @Autowired
    private GoldCoinService coinService;

    /**
     * 用户登录获取随机金币
     **/
    @ApiOperation(value = "用户登录获取随机金币", notes = "用户登录获取随机金币", response = HttpMessage.class)
    @PostMapping("/coin")
    public Tip coin(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                    @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id) throws Exception {
        Tip data = coinService.getRandomCoin(user_id,token_id);
        return data;
    }

    /**
     * 金币兑换红包
     **/
    @ApiOperation(value = "金币兑换红包", notes = "金币兑换红包", response = HttpMessage.class)
    @PostMapping("/exchangeRed")
    public Tip exchangeRed(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                           @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                           @ApiParam(value = "金币数量") @RequestParam(value = "coin") Integer coin) throws Exception {
        Tip data = coinService.exchangeRed(user_id,token_id,coin);
        return data;
    }

    /**
     * 我的红包列表(金币兑换的红包)
     **/
    @ApiOperation(value = "我的红包列表(金币兑换的红包)", notes = "我的红包列表(金币兑换的红包)", response = HttpMessage.class)
    @PostMapping("/redPacketList")
    public Tip myRedpacket(@ApiParam(value = "用户id") @RequestParam(value = "user_id") Integer user_id,
                           @ApiParam(value = "用户token") @RequestParam(value = "token_id") String token_id,
                           @ApiParam(value = "分页") @RequestParam(value = "page") Integer page) throws Exception {
        Tip data = coinService.myRedpacket(user_id,token_id,page);
        return data;
    }

}
