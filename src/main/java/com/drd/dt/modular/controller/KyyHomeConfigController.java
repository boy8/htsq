package com.drd.dt.modular.controller;


import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.service.KyyHomeConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Api(tags = "快应用配置", description = "快应用配置")
@RestController
@RequestMapping("/api/v2/htsq/kyy")
public class KyyHomeConfigController {

    @Autowired
    private KyyHomeConfigService kyyHomeConfigService;

    @CrossOrigin
    @ApiOperation(value = "获取首页配置信息", notes = "获取首页配置信息", response = HttpMessage.class)
    @RequestMapping(value = "/getConfig", method = {RequestMethod.POST, RequestMethod.GET})
    public Tip getConfig() {
        Tip tip = kyyHomeConfigService.getConfig();
        return tip;
    }

    @CrossOrigin
    @ApiOperation(value = "上报", notes = "data_type 1进入hemo 2进入详情 3保存桌面 4 退出", response = HttpMessage.class)
    @RequestMapping(value = "/addStatistics", method = {RequestMethod.POST, RequestMethod.GET})
    public void addStatistics(HttpServletRequest request,
                             @ApiParam(value = "show_url访问地址") @RequestParam(value = "show_url") String show_url,
                             @ApiParam(value = "imei") @RequestParam(value = "imei") String imei,
                             @ApiParam(value = "data_type") @RequestParam(value = "data_type") int data_type) {
         kyyHomeConfigService.addStatistics(request,show_url, imei, data_type);
    }

}
