package com.drd.dt.modular.mavenSocket.model;


/**
 * 返回客户端 数据模型
 **/
public class SocketSessionResponse {
    private String type;//协议类型 000-连接服务器成功 001 收到消息  002 异地登录 003 对方不存在 004新连接接入
    private String session_id; //发起人 session_Id
    private String mess;//发起内容
    private String data;//注释:json字符 协议类型不同,返回数据内容不同


    /**
     * @param type 协议类型 000-连接成功 001 消息转发  002异地登录 003对方不在线 004重复登录
     * @param session_id 发起人 session_Id
     * @param mess 发起内容
     * @param data 注释:json字符 协议类型不同,返回数据内容不同
     **/
    public SocketSessionResponse(String type, String session_id, String mess, String data) {
        this.type = type;
        this.session_id = session_id;
        this.mess = mess;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
