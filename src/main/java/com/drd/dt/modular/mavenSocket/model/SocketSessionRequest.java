package com.drd.dt.modular.mavenSocket.model;

public class SocketSessionRequest {
    private String mes;//聊天信息  可包含图片
    private String session_id;//目标 session_id
    private Integer user_id;// 该字段 是主动对 某某发起 聊天,再没有获取session_id 的时候 试用


    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
}
