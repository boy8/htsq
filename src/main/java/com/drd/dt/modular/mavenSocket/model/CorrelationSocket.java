package com.drd.dt.modular.mavenSocket.model;

import com.drd.dt.back.dto.UserManageDTO;
import com.drd.dt.modular.entity.User;

import javax.websocket.Session;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public  class CorrelationSocket {
    private Session ses;//当前用户session 信息
    private Map user;//用户信息
    private List<Session> sessionList;//当前用户建立通讯的  其他sesion
    public CorrelationSocket(){

    }
    public CorrelationSocket(Session s, Map u){
        ses = s;
        user = u;
        sessionList = new ArrayList<>();
    }

    public Session getSes() {
        return ses;
    }

    public void setSes(Session ses) {
        this.ses = ses;
    }

    public Map getUser() {
        return user;
    }

    public void setUser(Map user) {
        this.user = user;
    }

    public List<Session> getSessionList() {
        return sessionList;
    }

    public void setSessionList(List<Session> sessionList) {
        this.sessionList = sessionList;
    }
}
