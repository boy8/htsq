package com.drd.dt.modular.mavenSocket;

import com.alibaba.fastjson.JSONObject;
import com.drd.dt.modular.mavenSocket.model.CorrelationSocket;
import com.drd.dt.modular.mavenSocket.model.SocketSessionRequest;
import com.drd.dt.modular.mavenSocket.model.SocketSessionResponse;
import com.drd.dt.modular.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Gjing
 **/
@Component
@ServerEndpoint("/api/v2/htsq/{id}")
public class MyWebsocketServer {
    private static final Logger log = LoggerFactory.getLogger(MyWebsocketServer.class);

    private static UserServiceImpl userService; //提示 此处 因 bean 是单例 而socketn 服务是多线程，所以采用这中方式调用

    @Autowired
    public void setUserService(UserServiceImpl userService) {
        MyWebsocketServer.userService = userService;
    }

    /**
     * 存放所有在线的客户端
     */
    private static Map<String, CorrelationSocket> clients = new ConcurrentHashMap<>();


    @OnOpen
    public void onOpen(Session session, @PathParam("id") Integer id) {
        //1 判断是否是 新上线客户端
        Map user = userService.clientUserId(id);
        for (Map.Entry<String, CorrelationSocket> sessionEntry : clients.entrySet()) {
            if (sessionEntry.getValue().getUser().get("id").equals(user.get("id"))) {
                clients.remove(sessionEntry.getValue().getSes().getId());
                break;
            }
        }
        //2 获取用户基本信息然后存入 在线客户端列表
        CorrelationSocket correlationSocke = new CorrelationSocket(session, user);
        clients.put(session.getId(), correlationSocke);
        //3 回复客户端并返回用户基本信息
        sendObject(session.getId(), JSONObject.toJSONString(new SocketSessionResponse("000", session.getId(), "客服连成功！", JSONObject.toJSONString(user))));
    }

    /**
     * 客户端关闭
     *
     * @param session session
     */
    @OnClose
    public void onClose(Session session) {
        //将掉线的用户移除在线的组里
        clients.remove(session.getId());
    }

    /**
     * 发生错误
     *
     * @param throwable e
     */
    @OnError
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
    }

    /**
     * 收到客户端发来消息
     *
     * @param message 消息对象
     */
    @OnMessage
    public void onMessage(Session session, String message) {
//        this.sendAll(message);//群发消息
        SocketSessionRequest socketSessionRequest = JSONObject.parseObject(message, SocketSessionRequest.class);
        //1 当用户 带有user_id 代表主动对某人发起请求,但是不知道session_id,
        Map user = null;
        if (null != socketSessionRequest.getUser_id() && socketSessionRequest.getUser_id() > -1) {
            user = userService.clientUserId(socketSessionRequest.getUser_id());
            for (Map.Entry<String, CorrelationSocket> sessionEntry : clients.entrySet()) {
                if (sessionEntry.getValue().getUser().get("id").equals(user.get("id"))) {
                    socketSessionRequest.setSession_id(sessionEntry.getValue().getSes().getId());
                    break;
                }
            }
        }
        //2 根据发送信息中  sessio_id 找到需要发送的对象
        if (clients.containsKey(socketSessionRequest.getSession_id())) {
            //3 当收到 存入
            Boolean addIf = true;
            String codeType = "";
            for (Session s : clients.get(socketSessionRequest.getSession_id()).getSessionList()) {
                if (s.getId().equals(session.getId())) {//存在
                    addIf = false;
                    break;
                }
            }
            if (addIf) {
                codeType = "004";
                //新增连接对话
                clients.get(socketSessionRequest.getSession_id()).getSessionList().add(session);
            } else {
                codeType = "001";
            }
            this.sendObject(socketSessionRequest.getSession_id(),
                    JSONObject.toJSONString(new SocketSessionResponse(codeType, session.getId(), socketSessionRequest.getMes(), JSONObject.toJSONString(clients.get(session.getId()).getUser()))));
        } else {
            this.sendObject(session.getId(), JSONObject.toJSONString(new SocketSessionResponse("003", session.getId(), "对方不在线", "")));
        }

    }

    /**
     * 向当前客户端发送对象
     * <p>
     * 所发送对象
     *
     * @throws IOException
     */

    public void sendObject(String id, String message) {
        try {
            Session session = clients.get(id).getSes();
            session.getBasicRemote().sendText(message);
            // this.session.getAsyncRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("发送数据错误，ip:{},msg：{}", id, message);
        }

    }


    /**
     * 群发消息
     *
     * @param message 消息内容
     */
    private void sendAll(String message) {
        for (Map.Entry<String, CorrelationSocket> sessionEntry : clients.entrySet()) {
            sessionEntry.getValue().getSes().getAsyncRemote().sendText(message);
        }
    }
}
