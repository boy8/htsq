package com.drd.dt.modular.EnumUtils;


import com.drd.dt.common.Constant;

import java.util.Date;

/**
 * 当月表名称
 * 近三月表名称
 **/
public class OrderYMName {

    public static String currentTimeOrderYMName() {
        return "jz_order" + Constant.yM.format(new Date());
    }

    public static String[] nearestOrderYMName() {
        String[] ym = {"", "", ""};
        for (String s : ym) {
            s = "jz_order" + Constant.yM.format(new Date());
        }
        return ym;
    }
}
