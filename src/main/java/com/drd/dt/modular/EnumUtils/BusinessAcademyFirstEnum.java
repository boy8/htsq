package com.drd.dt.modular.EnumUtils;

/**
 * 商学院 - 一级菜单
 * */
public class BusinessAcademyFirstEnum {
    public static String getStrValue(int code){
        switch (code){
            case 1:{return "每日爆款"; }
            case 2:{return "宣传素材"; }
            case 3:{return "商学院"; }
        }
        return "";
    }

}
