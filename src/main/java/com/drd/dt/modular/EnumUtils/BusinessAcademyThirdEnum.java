package com.drd.dt.modular.EnumUtils;

/**
 * 商学院 - 三级菜单
 */
public class BusinessAcademyThirdEnum {

    public static String getStrValue(int code,int parentCode,int parentParentCode) {
        switch (parentParentCode+"-"+parentCode+"-"+code) {
            case "2-2-1": {
                return "一";
            }
            case "2-2-2": {
                return "二";
            }
            case "2-2-3": {
                return "三";
            }
            case "2-2-4": {
                return "四";
            }
            case "2-2-5": {
                return "五";
            }
            case "2-2-6": {
                return "六";
            }
            case "2-2-7": {
                return "七";
            }
            case "2-2-8": {
                return "八";
            }
            case "2-2-9": {
                return "九";
            }
            case "2-2-1.": {
                return "十";
            }
            case "2-2-11": {
                return "十一";
            }
            case "2-2-12": {
                return "十二";
            }
            case "2-2-13": {
                return "十三";
            }
            case "2-2-14": {
                return "十四";
            }
            case "2-2-15": {
                return "食物";
            }
            case "2-2-16": {
                return "十六";
            }
            case "2-2-17": {
                return "十七";
            }
            case "2-2-18": {
                return "十八";
            }
            case "2-2-19": {
                return "十九";
            }
            case "2-2-20": {
                return "二十";
            }
            case "2-2-21": {
                return "二十一";
            }
            case "2-2-22": {
                return "二十二";
            }
            case "2-2-23": {
                return "二十岁";
            }
            case "2-2-24": {
                return "二十四";
            }
            case "2-2-25": {
                return "二十五";
            }
            case "2-2-26": {
                return "二十六";
            }
            case"2-2-27": {
                return "二十七";
            }
            case "2-2-28": {
                return "二十八";
            }
            case "2-2-29": {
                return "二十九";
            }
        }
        return "";
    }
}
