package com.drd.dt.modular.EnumUtils;

/**
 * 商学院 - 二级菜单
 * */
public class BusinessAcademySecondEnum {
    public static String getStrValue(int code,int parentCode){
        switch (parentCode+"-"+code){
            case "1-1":{return "淘宝"; }
            case "1-2":{return "vip"; }
            case "1-3":{return "唯品会"; }
            case "1-4":{return "拼多多"; }
            case "1-5":{return "京东"; }
            case "2-1":{return "拉新素材"; }
            case "2-2":{return "新人必发"; }
            case "3-1":{return "小白必看"; }
            case "3-2":{return "大咖风采"; }
            case "3-3":{return "百问百答"; }
            case "3-4":{return "初级课堂"; }
            case "3-5":{return "中级课堂"; }
            case "3-6":{return "高级课堂"; }
        }
        return "";
    }

    public static String getStrValueIcon(int code,int parentCode){
        switch (parentCode+"-"+code){
            case "3-1":{return "https://meimg.cdn.bcebos.com/htsq%2Fbusiness%2Ficon%2Fnoob.png"; }
            case "3-2":{return "https://meimg.cdn.bcebos.com/htsq%2Fbusiness%2Ficon%2Fbigshot.png"; }
            case "3-3":{return "https://meimg.cdn.bcebos.com/htsq%2Fbusiness%2Ficon%2Fanswer.png"; }
            case "3-4":{return "https://meimg.cdn.bcebos.com/htsq%2Fbusiness%2Ficon%2Fsmall.png"; }
            case "3-5":{return "https://meimg.cdn.bcebos.com/htsq%2Fbusiness%2Ficon%2Fmiddle.png"; }
            case "3-6":{return "https://meimg.cdn.bcebos.com/htsq%2Fbusiness%2Ficon%2Fhigh.png"; }
        }
        return "";
    }

}
