package com.drd.dt.modular.entity;

import com.baomidou.mybatisplus.annotations.TableName;

@TableName("jz_order_gift")
public class OrderGift {
    private Integer id;
    private String user_id;
    private String product_id;
    private String product_pic_url;
    private String product_name;
    private String order_id;
    private Integer status;
    private String number;
    private String actual_amount;
    private String update_time;
    private String create_time;
    private String goods_spe_id;
    private String logistics_num;
    private String shipping_phone;
    private String shipping_user_name;
    private String shipping_address;
    private String buyer_id;
    private String buyer_logon_id;
    private String  trade_no;

    public String getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(String buyer_id) {
        this.buyer_id = buyer_id;
    }

    public String getBuyer_logon_id() {
        return buyer_logon_id;
    }

    public void setBuyer_logon_id(String buyer_logon_id) {
        this.buyer_logon_id = buyer_logon_id;
    }

    public String getTrade_no() {
        return trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }

    /**
     * 扩展 单位
     */
    private String goods_units;
    /**
     * 扩展 规格文字描述
     * */
    private String getGoods_spe_idText;

    public String getGetGoods_spe_idText() {
        return getGoods_spe_idText;
    }

    public void setGetGoods_spe_idText(String getGoods_spe_idText) {
        this.getGoods_spe_idText = getGoods_spe_idText;
    }

    public String getGoods_units() {
        return goods_units;
    }

    public void setGoods_units(String goods_units) {
        this.goods_units = goods_units;
    }

    public String getGoods_spe_id() {
        return goods_spe_id;
    }

    public void setGoods_spe_id(String goods_spe_id) {
        this.goods_spe_id = goods_spe_id;
    }

    public String getShipping_phone() {
        return shipping_phone;
    }

    public void setShipping_phone(String shipping_phone) {
        this.shipping_phone = shipping_phone;
    }

    public String getShipping_user_name() {
        return shipping_user_name;
    }

    public void setShipping_user_name(String shipping_user_name) {
        this.shipping_user_name = shipping_user_name;
    }

    public String getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(String shipping_address) {
        this.shipping_address = shipping_address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getLogistics_num() {
        return logistics_num;
    }

    public void setLogistics_num(String logistics_num) {
        this.logistics_num = logistics_num;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_pic_url() {
        return product_pic_url;
    }

    public void setProduct_pic_url(String product_pic_url) {
        this.product_pic_url = product_pic_url;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getActual_amount() {
        return actual_amount;
    }

    public void setActual_amount(String actual_amount) {
        this.actual_amount = actual_amount;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}

