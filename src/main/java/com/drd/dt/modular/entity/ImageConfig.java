package com.drd.dt.modular.entity;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * Created by 86514 on 2019/11/6.
 */
@TableName("jz_image_config")
public class ImageConfig {
    private Integer id;
    private Integer type;
    private Integer status;
    private String file;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
