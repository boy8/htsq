package com.drd.dt.modular.entity;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * Created by 86514 on 2020/3/11.
 */
@TableName("jz_task_info")
public class TaskInfo {
    private Integer id;
    private String task_name;
    private String task_desc;
    private String task_pic;
    private Integer lv0_coin;
    private Integer lv1_coin;
    private Integer lv2_coin;
    private Integer lv3_coin;
    private Integer total_coin;
    private Integer times;
    private Integer is_double;
    private Integer is_up_rake;
    private Integer type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public Integer getLv0_coin() {
        return lv0_coin;
    }

    public void setLv0_coin(Integer lv0_coin) {
        this.lv0_coin = lv0_coin;
    }

    public Integer getLv1_coin() {
        return lv1_coin;
    }

    public void setLv1_coin(Integer lv1_coin) {
        this.lv1_coin = lv1_coin;
    }

    public Integer getLv2_coin() {
        return lv2_coin;
    }

    public void setLv2_coin(Integer lv2_coin) {
        this.lv2_coin = lv2_coin;
    }

    public Integer getLv3_coin() {
        return lv3_coin;
    }

    public void setLv3_coin(Integer lv3_coin) {
        this.lv3_coin = lv3_coin;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public String getTask_pic() {
        return task_pic;
    }

    public void setTask_pic(String task_pic) {
        this.task_pic = task_pic;
    }

    public Integer getIs_double() {
        return is_double;
    }

    public void setIs_double(Integer is_double) {
        this.is_double = is_double;
    }

    public Integer getTotal_coin() {
        return total_coin;
    }

    public void setTotal_coin(Integer total_coin) {
        this.total_coin = total_coin;
    }

    public Integer getIs_up_rake() {
        return is_up_rake;
    }

    public void setIs_up_rake(Integer is_up_rake) {
        this.is_up_rake = is_up_rake;
    }

    public String getTask_desc() {
        return task_desc;
    }

    public void setTask_desc(String task_desc) {
        this.task_desc = task_desc;
    }
}
