package com.drd.dt.modular.entity;

import com.baomidou.mybatisplus.annotations.TableName;

@TableName("jz_order_fans_rake")
public class OrderFansRake {

    private Integer user_id;
    private Integer origin_id;
    private String channel_id;
    private String rake_type;
    private String order_id;
    private String rake_back;
    private String user_rake_back;
    private String rake_rate;
    private String create_time;
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(Integer origin_id) {
        this.origin_id = origin_id;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getRake_type() {
        return rake_type;
    }

    public void setRake_type(String rake_type) {
        this.rake_type = rake_type;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getRake_back() {
        return rake_back;
    }

    public void setRake_back(String rake_back) {
        this.rake_back = rake_back;
    }

    public String getUser_rake_back() {
        return user_rake_back;
    }

    public void setUser_rake_back(String user_rake_back) {
        this.user_rake_back = user_rake_back;
    }

    public String getRake_rate() {
        return rake_rate;
    }

    public void setRake_rate(String rake_rate) {
        this.rake_rate = rake_rate;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}

