package com.drd.dt.modular.entity;

import com.drd.dt.modular.EnumUtils.OrderYMName;

public class Order {
    private Integer id;
    private String product_id;
    private String product_pic_url;
    private String product_name;
    private Integer user_id;
    private String channel_id;
    private String order_id;
    private Integer type;
    private Integer status;
    private String clear_time;
    private Integer number;
    private String actual_amount = "0";
    private String rake_back = "0";
    private String user_rake_back = "0";
    private String user_rake_back_yugu = "0";
    private String user_rake;
    private String create_time;
    private Integer buytype;
    private String taobao_rid;
    private String taobao_sid;
    private Integer redpacket;
    private Boolean is_zeroParchase = false;//是否有零元购商品（true 有）

    /***
     * 扩展 每月订单表名
     * */
    private String orderYMName;

    public Integer getId() {
        return id;
    }

    public String getOrderYMName() {
        return OrderYMName.currentTimeOrderYMName();
    }

    public void setOrderYMName(String orderYMName) {
        this.orderYMName = orderYMName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_pic_url() {
        return product_pic_url;
    }

    public void setProduct_pic_url(String product_pic_url) {
        this.product_pic_url = product_pic_url;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getClear_time() {
        return clear_time;
    }

    public void setClear_time(String clear_time) {
        this.clear_time = clear_time;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getActual_amount() {
        return actual_amount;
    }

    public void setActual_amount(String actual_amount) {
        this.actual_amount = actual_amount;
    }

    public String getRake_back() {
        return rake_back;
    }

    public void setRake_back(String rake_back) {
        this.rake_back = rake_back;
    }

    public String getUser_rake_back() {
        return user_rake_back;
    }

    public void setUser_rake_back(String user_rake_back) {
        this.user_rake_back = user_rake_back;
    }

    public String getUser_rake_back_yugu() {
        return user_rake_back_yugu;
    }

    public void setUser_rake_back_yugu(String user_rake_back_yugu) {
        this.user_rake_back_yugu = user_rake_back_yugu;
    }

    public String getUser_rake() {
        return user_rake;
    }

    public void setUser_rake(String user_rake) {
        this.user_rake = user_rake;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Integer getBuytype() {
        return buytype;
    }

    public void setBuytype(Integer buytype) {
        this.buytype = buytype;
    }

    public String getTaobao_rid() {
        return taobao_rid;
    }

    public void setTaobao_rid(String taobao_rid) {
        this.taobao_rid = taobao_rid;
    }

    public String getTaobao_sid() {
        return taobao_sid;
    }

    public void setTaobao_sid(String taobao_sid) {
        this.taobao_sid = taobao_sid;
    }

    public Integer getRedpacket() {
        return redpacket;
    }

    public void setRedpacket(Integer redpacket) {
        this.redpacket = redpacket;
    }

    public Boolean getIs_zeroParchase() {
        return is_zeroParchase;
    }

    public void setIs_zeroParchase(Boolean is_zeroParchase) {
        this.is_zeroParchase = is_zeroParchase;
    }
}

