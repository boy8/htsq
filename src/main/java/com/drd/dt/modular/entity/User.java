package com.drd.dt.modular.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.drd.dt.util.EmojiUtil;
import org.apache.http.util.TextUtils;

/**
 * Created by 86514 on 2019/10/31.
 */
@TableName("jz_user_base")
public class User {
    private Integer id;
    private Integer introducer;
    private String friend_code;
    private Integer sex;
    private String age;
    private String phone;
    private String addr;
    private Integer level;
    private String lv_start_time;
    private String lv_expire_time;
    private String password;
    private String token;
    private Integer status;
    private String lastip;
    private String head_pic;
    private String wx_openid;
    private String wx_unionid;
    private String nick_name;
    private String alipay_account;
    private String alipay_user_name;
    private Integer balance_status;
    private Integer deviceT;
    private String wx_nickname;
    private String wx_num;
    private String channel_id;
    private String app_version;
    private String app_packageName;
    private String taobao_relation_id;
    private String taobao_special_id;
    private String taobao_user_id;
    private String taobao_user_name;
    private String jp_id;
    private Integer gold_coin;
    private String coin_time;
    private Integer is_zero_purchase;
    private String update_time;
    private String recent_login_time;
    private String create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Integer introducer) {
        this.introducer = introducer;
    }

    public String getFriend_code() {
        return friend_code;
    }

    public void setFriend_code(String friend_code) {
        this.friend_code = friend_code;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLastip() {
        return lastip;
    }

    public void setLastip(String lastip) {
        this.lastip = lastip;
    }

    public String getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(String head_pic) {
        this.head_pic = head_pic;
    }

    public String getWx_openid() {
        return wx_openid;
    }

    public void setWx_openid(String wx_openid) {
        this.wx_openid = wx_openid;
    }

    public String getNick_name() {
      /*  if (!TextUtils.isEmpty(nick_name)){
            nick_name= EmojiUtil.decode(nick_name);
        }*/
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        if (!TextUtils.isEmpty(nick_name)){
            nick_name = EmojiUtil.encode(nick_name);
        }
        this.nick_name = nick_name;
    }

    public String getAlipay_account() {
        return alipay_account;
    }

    public void setAlipay_account(String alipay_account) {
        this.alipay_account = alipay_account;
    }

    public String getAlipay_user_name() {
        return alipay_user_name;
    }

    public void setAlipay_user_name(String alipay_user_name) {
        this.alipay_user_name = alipay_user_name;
    }

    public Integer getBalance_status() {
        return balance_status;
    }

    public void setBalance_status(Integer balance_status) {
        this.balance_status = balance_status;
    }

    public Integer getDeviceT() {
        return deviceT;
    }

    public void setDeviceT(Integer deviceT) {
        this.deviceT = deviceT;
    }

    public String getWx_nickname() {
       /* if (!TextUtils.isEmpty(wx_nickname)){
            wx_nickname= EmojiUtil.decode(wx_nickname);
        }*/
        return wx_nickname;
    }

    public void setWx_nickname(String wx_nickname) {
        if (!TextUtils.isEmpty(wx_nickname)){
            wx_nickname = EmojiUtil.encode(wx_nickname);
        }
        this.wx_nickname = wx_nickname;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    public String getApp_packageName() {
        return app_packageName;
    }

    public void setApp_packageName(String app_packageName) {
        this.app_packageName = app_packageName;
    }

    public String getTaobao_relation_id() {
        return taobao_relation_id;
    }

    public void setTaobao_relation_id(String taobao_relation_id) {
        this.taobao_relation_id = taobao_relation_id;
    }

    public String getTaobao_special_id() {
        return taobao_special_id;
    }

    public void setTaobao_special_id(String taobao_special_id) {
        this.taobao_special_id = taobao_special_id;
    }

    public String getTaobao_user_id() {
        return taobao_user_id;
    }

    public void setTaobao_user_id(String taobao_user_id) {
        this.taobao_user_id = taobao_user_id;
    }

    public String getTaobao_user_name() {
        return taobao_user_name;
    }

    public void setTaobao_user_name(String taobao_user_name) {
        this.taobao_user_name = taobao_user_name;
    }

    public String getJp_id() {
        return jp_id;
    }

    public void setJp_id(String jp_id) {
        this.jp_id = jp_id;
    }

    public Integer getGold_coin() {
        return gold_coin;
    }

    public void setGold_coin(Integer gold_coin) {
        this.gold_coin = gold_coin;
    }

    public String getCoin_time() {
        return coin_time;
    }

    public void setCoin_time(String coin_time) {
        this.coin_time = coin_time;
    }

    public Integer getIs_zero_purchase() {
        return is_zero_purchase;
    }

    public void setIs_zero_purchase(Integer is_zero_purchase) {
        this.is_zero_purchase = is_zero_purchase;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getRecent_login_time() {
        return recent_login_time;
    }

    public void setRecent_login_time(String recent_login_time) {
        this.recent_login_time = recent_login_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getLv_start_time() {
        return lv_start_time;
    }

    public void setLv_start_time(String lv_start_time) {
        this.lv_start_time = lv_start_time;
    }

    public String getLv_expire_time() {
        return lv_expire_time;
    }

    public void setLv_expire_time(String lv_expire_time) {
        this.lv_expire_time = lv_expire_time;
    }

    public String getWx_num() {
        return wx_num;
    }

    public void setWx_num(String wx_num) {
        this.wx_num = wx_num;
    }

    public String getWx_unionid() {
        return wx_unionid;
    }

    public void setWx_unionid(String wx_unionid) {
        this.wx_unionid = wx_unionid;
    }
}
