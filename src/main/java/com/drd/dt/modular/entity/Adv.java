package com.drd.dt.modular.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 广告banner图，开屏图
 * Created by 86514 on 2019/11/7.
 */

@ApiModel(value="广告banner图，开屏图",description="广告banner图，开屏图")
public class Adv {
    private Integer id;
    @ApiModelProperty(value="广告名称",name="name",example="广告名称")
    private String name;
    private String img_url;
    private String jump_url;
    private Integer type;
    private Integer jump_type;
    private String goods_id;
    private String color;
    private Integer is_login;
    private Integer block;
    private Integer discern_id;
    private Integer is_need_userid;
    private Integer is_need_tb_outh;

    public Integer getIs_need_userid() {
        return is_need_userid;
    }

    public void setIs_need_userid(Integer is_need_userid) {
        this.is_need_userid = is_need_userid;
    }

    public Integer getDiscern_id() {
        return discern_id;
    }

    public void setDiscern_id(Integer discern_id) {
        this.discern_id = discern_id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getJump_url() {
        return jump_url;
    }

    public void setJump_url(String jump_url) {
        this.jump_url = jump_url;
    }

    public Integer getJump_type() {
        return jump_type;
    }

    public void setJump_type(Integer jump_type) {
        this.jump_type = jump_type;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public Integer getIs_login() {
        return is_login;
    }

    public void setIs_login(Integer is_login) {
        this.is_login = is_login;
    }

    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIs_need_tb_outh() {
        return is_need_tb_outh;
    }

    public void setIs_need_tb_outh(Integer is_need_tb_outh) {
        this.is_need_tb_outh = is_need_tb_outh;
    }
}
