package com.drd.dt.modular.entity;

import com.baomidou.mybatisplus.annotations.TableName;

@TableName("jz_order_fans_rake")
public class OrderGiftRake {
    private Integer id;
    private String order_id;
    private String user_rake_back;
    private String rake_type;
    private Integer user_id;
    private Integer origin_id;
    private String create_time;
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getUser_rake_back() {
        return user_rake_back;
    }

    public void setUser_rake_back(String user_rake_back) {
        this.user_rake_back = user_rake_back;
    }

    public String getRake_type() {
        return rake_type;
    }

    public void setRake_type(String rake_type) {
        this.rake_type = rake_type;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(Integer origin_id) {
        this.origin_id = origin_id;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}

