package com.drd.dt.modular.entity;

public class UserMoney {
    /**
     * 会员ID
     */
    private Integer user_id;
    /**
     * 用户普通订单金额预估
     */
    private double owner_estimate;

    /**
     * 普通订单余额
     */
    private double normal_balance;
    /**
     * （人头佣金）奖励金余额
     */
    private double bounty_balance;
    /**
     * 累计收益 所有收益
     */
    private double accumulated;
    /**
     * 粉丝订单返佣预估
     */
    private double fans_estimate;
    /**
     * 奖励佣金（人头佣金）
     */
    private double bounty_accumulated;
    /**
     * 分享活动余额
     */
    private double activity_balance;
    /**
     * 分享活动累计收益
     */
    private double activity_accumulated;
    /**
     * 直接粉丝数
     */
    private double direct_fans;
    /**
     * 间接粉丝数
     */
    private double indirect_fans;

    /**
     * 更新时间
     */
    private String update_time;

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public double getOwner_estimate() {
        return owner_estimate;
    }

    public void setOwner_estimate(double owner_estimate) {
        this.owner_estimate = owner_estimate;
    }

    public double getNormal_balance() {
        return normal_balance;
    }

    public void setNormal_balance(double normal_balance) {
        this.normal_balance = normal_balance;
    }

    public double getBounty_balance() {
        return bounty_balance;
    }

    public void setBounty_balance(double bounty_balance) {
        this.bounty_balance = bounty_balance;
    }

    public double getAccumulated() {
        return accumulated;
    }

    public void setAccumulated(double accumulated) {
        this.accumulated = accumulated;
    }

    public double getFans_estimate() {
        return fans_estimate;
    }

    public void setFans_estimate(double fans_estimate) {
        this.fans_estimate = fans_estimate;
    }

    public double getBounty_accumulated() {
        return bounty_accumulated;
    }

    public void setBounty_accumulated(double bounty_accumulated) {
        this.bounty_accumulated = bounty_accumulated;
    }

    public double getDirect_fans() {
        return direct_fans;
    }

    public void setDirect_fans(double direct_fans) {
        this.direct_fans = direct_fans;
    }

    public double getIndirect_fans() {
        return indirect_fans;
    }

    public void setIndirect_fans(double indirect_fans) {
        this.indirect_fans = indirect_fans;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public double getActivity_balance() {
        return activity_balance;
    }

    public void setActivity_balance(double activity_balance) {
        this.activity_balance = activity_balance;
    }

    public double getActivity_accumulated() {
        return activity_accumulated;
    }

    public void setActivity_accumulated(double activity_accumulated) {
        this.activity_accumulated = activity_accumulated;
    }
}
