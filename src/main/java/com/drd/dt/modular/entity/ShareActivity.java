package com.drd.dt.modular.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.drd.dt.util.EmojiUtil;
import org.apache.http.util.TextUtils;

/**
 * Created by 86514 on 2019/10/31.
 */
@TableName("jz_share_activity")
public class ShareActivity {
    private Integer id;
    private Integer user_id;
    private String total_money;
    private Double random_money;
    private String group_first_click;
    private String group_second_click;
    private String invite_first_people;
    private String invite_second_people;
    private String invite_third_people;
    private String start_time;
    private String expire_time;
    private Integer status;
    private String create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getTotal_money() {
        return total_money;
    }

    public void setTotal_money(String total_money) {
        this.total_money = total_money;
    }

    public Double getRandom_money() {
        return random_money;
    }

    public void setRandom_money(Double random_money) {
        this.random_money = random_money;
    }

    public String getGroup_first_click() {
        return group_first_click;
    }

    public void setGroup_first_click(String group_first_click) {
        this.group_first_click = group_first_click;
    }

    public String getGroup_second_click() {
        return group_second_click;
    }

    public void setGroup_second_click(String group_second_click) {
        this.group_second_click = group_second_click;
    }

    public String getInvite_first_people() {
        return invite_first_people;
    }

    public void setInvite_first_people(String invite_first_people) {
        this.invite_first_people = invite_first_people;
    }

    public String getInvite_second_people() {
        return invite_second_people;
    }

    public void setInvite_second_people(String invite_second_people) {
        this.invite_second_people = invite_second_people;
    }

    public String getInvite_third_people() {
        return invite_third_people;
    }

    public void setInvite_third_people(String invite_third_people) {
        this.invite_third_people = invite_third_people;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(String expire_time) {
        this.expire_time = expire_time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
