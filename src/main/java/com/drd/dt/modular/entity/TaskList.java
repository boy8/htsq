package com.drd.dt.modular.entity;

/**
 * Created by 86514 on 2020/3/17.
 */
public class TaskList {
    private Integer id;
    private Integer user_id;
    private String task_name;
    private String task_desc;
    private String task_pic;
    private Integer task_id;
    private Integer coin;
    private Integer is_double;
    private Integer type;
    private Integer total_times;//用户需要完成任务的次数
    private Integer times;//用户已经完成任务的次数
    private Integer status;
    private String create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public Integer getTask_id() {
        return task_id;
    }

    public void setTask_id(Integer task_id) {
        this.task_id = task_id;
    }

    public Integer getTotal_times() {
        return total_times;
    }

    public void setTotal_times(Integer total_times) {
        this.total_times = total_times;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getTask_pic() {
        return task_pic;
    }

    public void setTask_pic(String task_pic) {
        this.task_pic = task_pic;
    }

    public Integer getCoin() {
        return coin;
    }

    public void setCoin(Integer coin) {
        this.coin = coin;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIs_double() {
        return is_double;
    }

    public void setIs_double(Integer is_double) {
        this.is_double = is_double;
    }

    public String getTask_desc() {
        return task_desc;
    }

    public void setTask_desc(String task_desc) {
        this.task_desc = task_desc;
    }
}
