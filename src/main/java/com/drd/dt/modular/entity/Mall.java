package com.drd.dt.modular.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 商城菜单分类
 * Created by 86514 on 2019/11/7.
 */

@ApiModel(value="菜单分类",description="菜单分类")
public class Mall {
    private Integer id;
    @ApiModelProperty(value="名称",name="name",example="淘宝商城")
    private String name;
    private String img_url;
    private String jump_url;
    private Integer type;
    private Integer jump_type;
    private Integer is_login;
    private Integer is_need_userid;
    private Integer discern_id;

    public Integer getIs_need_userid() {
        return is_need_userid;
    }

    public void setIs_need_userid(Integer is_need_userid) {
        this.is_need_userid = is_need_userid;
    }

    public Integer getDiscern_id() {
        return discern_id;
    }

    public void setDiscern_id(Integer discern_id) {
        this.discern_id = discern_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getJump_url() {
        return jump_url;
    }

    public void setJump_url(String jump_url) {
        this.jump_url = jump_url;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getJump_type() {
        return jump_type;
    }

    public void setJump_type(Integer jump_type) {
        this.jump_type = jump_type;
    }

    public Integer getIs_login() {
        return is_login;
    }

    public void setIs_login(Integer is_login) {
        this.is_login = is_login;
    }
}
