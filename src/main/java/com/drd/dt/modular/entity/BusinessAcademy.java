package com.drd.dt.modular.entity;


import com.baomidou.mybatisplus.annotations.TableName;
import com.drd.dt.modular.dto.GoodsDetailDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "商学院")
@TableName("jz_business_academy")
public class BusinessAcademy {
    @ApiModelProperty(value = "主键ID", example = "1")
    private int business_id;
    @ApiModelProperty(value = "1淘宝、2京东、3拼多多，4唯品会、5自营", example = "0", required = true)
    private int type;
    private int first_module;
    private String title;
    private String home_image;
    private String ima_s;
    private String video;
    private int like_num;
    private int transpond_num;
    private int comment_num;
    private int show_num;
    private Date create_time;
    private String text;
    @ApiModelProperty(value = "查看等级", example = "4", required = true)
    private int show_lv;
    private String second_module;
    @ApiModelProperty(value = "状态", example = "0", required = true)
    private int state;
    private int third_module;

    private String biz_id;
    @ApiModelProperty(value = "今日爆款携带商品详情", example = "0")
    private GoodsDetailDTO goodsDetailDTO;
    /**
     * 扩展 1淘宝、2京东、3拼多多，4唯品会、5自营
     */
    private String typeText;
    /**
     * 扩展 图片集合
     */
    private List<String> ima_sList;

    public GoodsDetailDTO getGoodsDetailDTO() {
        return goodsDetailDTO;
    }

    public void setGoodsDetailDTO(GoodsDetailDTO goodsDetailDTO) {
        this.goodsDetailDTO = goodsDetailDTO;
    }

    public List<String> getIma_sList() {
        if (null == ima_sList) {
            ima_sList = new ArrayList<String>();
        }
        if (null != ima_s) {
            String[] images = this.ima_s.split(",");
            for (String str : images) {
                ima_sList.add(str);
            }
        }
        return ima_sList;
    }

    public void setIma_sList(List<String> ima_sList) {
        this.ima_sList = ima_sList;
    }

    public String getTypeText() {
        switch (this.type) {
            case 1: {
                typeText = "淘宝";
            }
            break;
            case 2: {
                typeText = "京东";
            }
            break;
            case 3: {
                typeText = "拼多多";
            }
            break;
            case 4: {
                typeText = "唯品会";
            }
            break;
            case 5: {
                typeText = "自营";
            }
            break;
        }
        return typeText;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
    }

    public String getBiz_id() {
        return biz_id;
    }

    public void setBiz_id(String biz_id) {
        this.biz_id = biz_id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getComment_num() {
        return comment_num;
    }

    public void setComment_num(int comment_num) {
        this.comment_num = comment_num;
    }


    public int getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(int business_id) {
        this.business_id = business_id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHome_image() {
        return home_image;
    }

    public void setHome_image(String home_image) {
        this.home_image = home_image;
    }


    public String getIma_s() {
        return ima_s;
    }

    public void setIma_s(String ima_s) {
        this.ima_s = ima_s;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public int getLike_num() {
        return like_num;
    }

    public void setLike_num(int like_num) {
        this.like_num = like_num;
    }

    public int getTranspond_num() {
        return transpond_num;
    }

    public void setTranspond_num(int transpond_num) {
        this.transpond_num = transpond_num;
    }

    public int getShow_num() {
        return show_num;
    }

    public void setShow_num(int show_num) {
        this.show_num = show_num;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getShow_lv() {
        return show_lv;
    }

    public void setShow_lv(int show_lv) {
        this.show_lv = show_lv;
    }

    public int getFirst_module() {
        return first_module;
    }

    public void setFirst_module(int first_module) {
        this.first_module = first_module;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public String getSecond_module() {
        return second_module;
    }

    public void setSecond_module(String second_module) {
        this.second_module = second_module;
    }

    public int getThird_module() {
        return third_module;
    }

    public void setThird_module(int third_module) {
        this.third_module = third_module;
    }
}
