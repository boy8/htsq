package com.drd.dt.modular.entity;

/**
 * Created by 86514 on 2019/4/2.
 */
public class InitConfig {
    private Integer id;
    private String api_url;
    private String oauth_url;
    private String android_oauth_mode;
    private String ios_oauth_mode;
    private String access_key;
    private String secret_key;
    private String img_url;
    private String head_pic_url;
    private String verify_version;
    private Integer remind_promotion;
    private String remind_desc;
    private String remind_version;//ios提示升级（1不提示 2建议 3强制）
    private String create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApi_url() {
        return api_url;
    }

    public void setApi_url(String api_url) {
        this.api_url = api_url;
    }

    public String getOauth_url() {
        return oauth_url;
    }

    public void setOauth_url(String oauth_url) {
        this.oauth_url = oauth_url;
    }

    public String getAndroid_oauth_mode() {
        return android_oauth_mode;
    }

    public void setAndroid_oauth_mode(String android_oauth_mode) {
        this.android_oauth_mode = android_oauth_mode;
    }

    public String getIos_oauth_mode() {
        return ios_oauth_mode;
    }

    public void setIos_oauth_mode(String ios_oauth_mode) {
        this.ios_oauth_mode = ios_oauth_mode;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getHead_pic_url() {
        return head_pic_url;
    }

    public void setHead_pic_url(String head_pic_url) {
        this.head_pic_url = head_pic_url;
    }

    public String getVerify_version() {
        return verify_version;
    }

    public void setVerify_version(String verify_version) {
        this.verify_version = verify_version;
    }

    public Integer getRemind_promotion() {
        return remind_promotion;
    }

    public void setRemind_promotion(Integer remind_promotion) {
        this.remind_promotion = remind_promotion;
    }

    public String getRemind_desc() {
        return remind_desc;
    }

    public void setRemind_desc(String remind_desc) {
        this.remind_desc = remind_desc;
    }

    public String getRemind_version() {
        return remind_version;
    }

    public void setRemind_version(String remind_version) {
        this.remind_version = remind_version;
    }
}
