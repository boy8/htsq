package com.drd.dt.modular.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.modular.service.OrderGiftService;
import com.drd.dt.util.ResultUtil;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.rmi.runtime.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Transactional
@Service
public class ExpressTradeServiceImpl {
    private final Logger log = LoggerFactory.getLogger(ExpressTradeServiceImpl.class.getClass());

    @Autowired
    private OrderGiftService orderGiftService;
    /**
     * 根据物流ID和用户ID查询 物流进度
     * @param expressid
     * @param order_id
     * @return
     */
    public Tip queryExpress(String order_id,String expressid){
        try{
            String url="https://wuliu.market.alicloudapi.com/kdi?no="+expressid;
            String appcode = "94b6ae102c994782ba4553a580a83934";  // !!!替换填写自己的AppCode 在买家中心查看
            String auth= "APPCODE " + appcode;
            String result=doGet(url,auth);
            if(result!=null){
//                System.out.println("快递查询信息：" + result);
                JSONObject obj= JSON.parseObject(result);
                JSONObject data=obj.getJSONObject("result");
                /* 0：快递收件(揽件)1.在途中 2.正在派件 3.已签收 4.派送失败 5.疑难件 6.退件签收  */
                String type=data.getString("deliverystatus");//快递状态
                int status=2;//订单状态 0 未支付 1-已完成 2-订单付款（待发货） 3-待收货（已发货）
                if(type.equals("3")){
                    status=1;
                }else{
                    status=3;
                }
                Map<String, Object> m = new HashMap<>();
                m.put("id", order_id);
                m.put("status", status);
                int req = orderGiftService.upOrderGift(m);
                return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),data);

            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ResultUtil.result(BizExceptionEnum.SELECT_ERROR.getCode(),BizExceptionEnum.SELECT_ERROR.getMessage());

    }


    public  String doGet(String url,String Authorization) {
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;
        String result = "";
        try {
            // 通过址默认配置创建一个httpClient实例
            httpClient = HttpClients.createDefault();
            // 创建httpGet远程连接实例
            HttpGet httpGet = new HttpGet(url);
            // 设置请求头信息，鉴权
            httpGet.setHeader("Authorization", Authorization);
            // 设置配置请求参数
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(3000)// 连接主机服务超时时间
                    .setConnectionRequestTimeout(3000)// 请求超时时间
                    .setSocketTimeout(3000)// 数据读取超时时间
                    .build();
            // 为httpGet实例设置配置
            httpGet.setConfig(requestConfig);
            // 执行get请求得到返回对象
            response = httpClient.execute(httpGet);
            // 通过返回对象获取返回数据
            HttpEntity entity = response.getEntity();
            // 通过EntityUtils中的toString方法将结果转换为字符串
            result = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (null != response) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != httpClient) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
