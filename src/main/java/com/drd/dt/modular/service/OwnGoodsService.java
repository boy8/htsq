package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.OwnGoodsOrderDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface OwnGoodsService {
    /**
     * 查询商品列表-分页
     **/
    Tip getOwnGoodsList(int index, int size);

    /**
     * 查询商品列表-分页-分组
     **/
    public Tip getOwnGoodsTypeList(int index, int size,int type);

    /**
     * 新 商品列表 不分页
     * */
    Tip getOwnGoodsListNoPage();

    /**
     * 查询商品详情
     **/
    Tip getOwnGoodsDetails(int dealer_id);


    /**
     * 获取 自营商品规格
     */
    Tip getGoodsSpecificationDetails(int goods_id);


    /**
     * 生成订单
     */
    public void addOrderGift(OwnGoodsOrderDTO ownGoodsOrderDTO, HttpServletResponse response, HttpServletRequest request) throws Exception;

    /**
     * 订单详情
     * */
    public Tip getOrderGiftDetail(String order_id);
}
