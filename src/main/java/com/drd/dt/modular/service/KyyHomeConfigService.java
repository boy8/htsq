package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.KyyStatisticsDTO;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface KyyHomeConfigService {

    //查询快应用首页配置
    Tip getConfig();

    //上报
   void  addStatistics(HttpServletRequest request ,String show_url, String imei, int data_type);
}
