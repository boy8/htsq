package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.TXSuggestDTO;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface TradeService {

    /**
     * 我的提现列表
     */
    Tip tx(Integer user_id, String token_id, Integer page,String type) throws Exception;

    /**
     * 创建提现申请
     */
    Tip addTx(TXSuggestDTO txSuggestDTO) throws Exception;


    Tip testTX(String phone,String name,Double money);

}
