package com.drd.dt.modular.service;


import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * 大淘客  APi http://www.dataoke.com/pmc/api-d.html?id=8 账号密码 15982132721  WCQ123456
 */
public interface DtkService {


    public  String doGet(String url);
    /**
     * 请求 接口
     * */
    public  String sendGet(String getUrl, Map<String, String> paraMap);


    /******************************签名**********************************/
    /**
     * 获取签名的util
     *
     * @param map       请求参数
     * @param secretKey 密钥
     * @return
     */
    public String getSignStr(TreeMap<String, String> map, String secretKey);
    public String sign(String content, String key);

    /**
     * MD5加密算法
     *
     * @param s
     * @return
     * @see [类、类#方法、类#成员]
     */
    public String MD5(String s);
    /******************************签名**********************************/
}
