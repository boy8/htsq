package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/4/10.
 */
public interface zuimeiService {

    /**
     * 最美天气-小美贝商品信息
     **/
    Tip list(Integer user_id,Integer zuimei_user_id,Integer page) throws Exception;

    /**
     * 最美天气-小美贝用户信息接收
     **/
    Tip receiveInfo(Integer zuimei_user_id, String money)  throws Exception;

    /**
     * 小美贝商品详情
     **/
    Tip detail(String goods_id,Integer user_id) throws Exception;

    /**
     * 最美天气-小美贝淘礼金兑换
     **/
    Tip tlj(Integer user_id, String token_id, String price, String goods_id,String goods_name,String pic) throws Exception;

    /**
     * 获取最美用户账户信息
     **/
    Tip zuimeiUserInfo(Integer user_id, Integer zuimei_user_id) throws Exception;

    /**
     * 小美贝-获取兑换记录
     **/
    Tip exchangeHistory(Integer zuimei_user_id,Integer page,String type) throws Exception;

    /**
     * 最美天气-小美贝淘礼金使用情况查询
     **/
    Tip tljUseDetail(String rights_id,String time) throws Exception;

    /**
     * 现金券兑换淘礼金记录
     **/
    Tip tljHistory(Integer user_id, String token_id,Integer page) throws Exception;

    /**
     * 最美天气使用-商品列表
     **/
    Tip arrondiGoods(Integer page,Integer pageSize,String queryKey) throws Exception;

    /**
     * 最美天气使用-商品详情
     **/
    Tip goodsDetail(String goods_id) throws Exception;
}
