package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.PayDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface PayService {
    Tip findOrder(String orderid);
    /**
     * 支付宝支付
     **/
    Tip alipay(PayDTO payDTO) throws Exception;

    /**
     * 支付宝支付
     **/
    void alipayH5(HttpServletResponse response, HttpServletRequest request,PayDTO payDTO) throws Exception;
    /**
     * 支付宝异步通知回调
     * */
    void alipayPayCallback(HttpServletResponse response,HttpServletRequest request) throws Exception;

    /**
     * 微信支付
     **/
    Tip wxpay(PayDTO payDTO) throws Exception;
    /**
     * 微信支付H5
     **/
    void wxpayH5(HttpServletResponse response, HttpServletRequest request, PayDTO payDTO) throws Exception;

    /**
     * 接收微信支付异步通知回调地址
     **/
    void wxPayCallback(HttpServletResponse response,HttpServletRequest request) throws Exception;



    /**
     * 微信/支付宝退款
     * **/
    Tip refundMoney(Integer user_id,String token,String order_id)throws Exception;

    /**
     * 支付宝退款 精选核桃
     * **/
    public boolean zfbRefundMoney(String order_id, String trade_no, Double actual_amount) throws Exception;
    /**
     * 微信退款 精选核桃
     * **/
    public boolean wxRefundMoney(String order_id, String trade_no, Double actual_amount) throws Exception;
}
