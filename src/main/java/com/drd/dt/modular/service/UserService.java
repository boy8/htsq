package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.UpdateUserInfoDTO;
import com.drd.dt.modular.dto.UserloginDTO;
import com.drd.dt.modular.dto.VerifyDTO;
import com.drd.dt.modular.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 用户操作 公共接口 定义
 * 2019-3-13 hl
 **/
public interface UserService {
   Tip checkZeroPolicy(int user_id) throws Exception;
    /**
     * 第三方登陆
     **/
    Tip thirdLogin(UserloginDTO userloginDTO, HttpServletRequest request) throws Exception;

    /**
     * 发短信获取验证码
     **/
    Tip sendVerificationCode(String phone) throws Exception;

    /**
     * 手机号登陆
     **/
    Tip codeLogin(HttpServletRequest request,UserloginDTO userloginDTO) throws Exception;

    /**
     * 秒验接口
     **/
    Tip verify(HttpServletRequest request,VerifyDTO verifyDTO) throws Exception;

    /**
     * 查询用户当前多少直属粉丝
     **/
    Tip getVipInfo(int user_id,String token_id) throws Exception;

    /**
     * 根据邀请码获取用户信息
     **/
    Tip codeInfo(String code) throws Exception;

    /**
     * 根据手机号绑定邀请码
     **/
    Tip codeBind(String code, String phone) throws Exception;

    /**
     * 个人中心-用户信息
     **/
    Tip userInfo(Integer user_id, String token_id,String deviceT,String app_version,HttpServletRequest request) throws Exception;

    /**
     * 个人中心-修改用户信息
     **/
    Tip updateUser(UpdateUserInfoDTO userInfoDTO) throws Exception;

    /**
     * 个人中心-我的粉丝（redis分页）
     **/
    Tip myFans(Integer user_id, String token_id,Integer page,Integer type) throws Exception;

    /**
     * 个人中心-我的粉丝（数据库分页）
     **/
    Tip myFansPage(Integer user_id, String token_id,Integer page,Integer type,String time,String fans_num,String is_vip,String active) throws Exception;

    /**
     * 个人中心-我的粉丝-粉丝详情
     **/
    Tip myFansInfo(Integer user_id, String token_id, Integer fans_id) throws Exception;

    /**
     * 个人中心-我的订单
     **/
    Tip myOrders(Integer user_id, String token_id, Integer page, Integer type, Integer status,Integer order_type) throws Exception;


    /**
     * 个人中心-龙虎榜（redis分页）
     **/
    Tip rank(Integer user_id, String token_id,Integer page,Integer type) throws Exception;

    /**
     * 个人中心-龙虎榜（数据库分页）
     **/
    Tip rankPage(Integer user_id, String token_id,Integer page,Integer type) throws Exception;

    /**
     * 个人中心-我的收益
     **/
    Tip profit(Integer user_id, String token_id) throws Exception;

    /**
     * 个人中心-我的收益(新)
     **/
    Tip myProfit(Integer user_id, String token_id,Integer time_type);

    /**
     * 个人中心-邀请好友
     **/
    Tip invite(Integer user_id, String token_id,Integer deviceT,HttpServletRequest request) throws Exception;

    /**
     * 个人中心-常见问题-常见问题
     * @return
     */
    Tip normalProblem() throws Exception;

    /**
     * 个人中心-常见问题-问题分类 列表
     * @return
     */
    Tip problemType(String deviceT,String app_version,HttpServletRequest request) throws Exception;

    /**
     * 个人中心-常见问题-问题分类 答案
     * @return
     */
    Tip problemList(Integer id) throws Exception;

    /**
     * 个人中心-隐私政策
     **/
    Tip privacyPolicy() throws Exception;

    /**
     * 个人中心-邀请好友-扫描二维码
     **/
    void scanQR(HttpServletResponse response,HttpServletRequest request,String friend_code,Integer user_id) throws Exception;

    /***
     * vip 分享购买自动注册绑定 返回user_id token
     * */
    Tip vipLogin(HttpServletRequest request,Integer user_id,String phone,String code)throws Exception;

    /**
     * 根据用户id 获取用户信息
     * */
    Map clientUserId(Integer user_id);

    /**
     * 扫描二维码-存用户微信信息
     **/
    Tip saveScanInfo(Integer user_id,String code)  throws Exception;
}
