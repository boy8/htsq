package com.drd.dt.modular.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.modular.dao.ZuimeiMapper;
import com.drd.dt.modular.dto.*;
import com.drd.dt.modular.service.zuimeiService;
import com.drd.dt.properties.DTKPropertyConfig;
import com.drd.dt.properties.TBLMPropertyConfig;
import com.drd.dt.util.EmojiUtil;
import com.drd.dt.util.FilterGoodsUtil;
import com.drd.dt.util.ResultUtil;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkDgMaterialOptionalRequest;
import com.taobao.api.request.TbkDgVegasTljCreateRequest;
import com.taobao.api.request.TbkDgVegasTljInstanceReportRequest;
import com.taobao.api.response.TbkDgMaterialOptionalResponse;
import com.taobao.api.response.TbkDgVegasTljCreateResponse;
import com.taobao.api.response.TbkDgVegasTljInstanceReportResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service
public class ZuimeiServiceImpl extends ServiceImpl<ZuimeiMapper, ZuimeiGoodsDTO> implements zuimeiService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private ZuimeiMapper zuimeiMapper;
    @Autowired
    private TBGoodsServiceImpl tbGoodsServiceImpl;
    @Autowired
    private TBLMPropertyConfig tblmPropertyConfig;
    @Autowired
    private DtkServiceImpl dtkService;
    @Autowired
    private DTKPropertyConfig dtkPropertyConfig;
    @Autowired
    private UserMoneyServiceImpl testMoneyService;
    @Autowired
    private FilterGoodsUtil filterGoodsUtil;

    /**
     * 列表
     */
    @Override
    public Tip list(Integer user_id, Integer zuimei_user_id, Integer pageNum) throws Exception {
        if (StringUtils.isEmpty(zuimei_user_id)) {
            return ResultUtil.result(BizExceptionEnum.PARAM_EMPTY.getCode(), BizExceptionEnum.PARAM_EMPTY.getMessage());
        }
        if (StringUtils.isEmpty(pageNum)) {
            pageNum = 1;
        }

        Page page = new Page(pageNum, Constant.PAGE_SIZE_20);
        List<ZuimeiGoodsDTO> list = zuimeiMapper.selectZuimeiList(page);
        page.setRecords(list);

        //先判定user_id 是否已经被绑定 并且判定绑定最美id是否是当前传入id
        long countUser = zuimeiMapper.getZuimeiBalanceUserid(user_id);

        ZuimeiUserInfoDTO zuimeiBalance = zuimeiMapper.getZuimeiBalance(zuimei_user_id);
        if (null != zuimeiBalance) {
            Integer userId = zuimeiBalance.getUser_id();
            if (StringUtils.isEmpty(userId) && !StringUtils.isEmpty(user_id) && (countUser == 0)) {//数据user_id 没有并且 传入userid 不为空 进行绑定
                logger.info("【开始与最美用户：" + zuimei_user_id + "绑定核桃用户：" + userId + "】");
                zuimeiBalance.setUser_id(user_id);
                zuimeiMapper.bindUserId(zuimeiBalance);
            }
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), page);
    }

    /**
     * 小美贝商品详情
     **/
    @Override
    public Tip detail(String goods_id, Integer user_id) throws Exception {
        Wrapper<ZuimeiGoodsDTO> wrapper = new EntityWrapper<>();
        wrapper.eq("goods_id", goods_id);
        List<ZuimeiGoodsDTO> zuimeiGoodsDTO = zuimeiMapper.selectList(wrapper);
        if (zuimeiGoodsDTO.size() < 1) {
            return ResultUtil.result(BizExceptionEnum.GOODS_DETAIL_IS_EMPTY.getCode(), BizExceptionEnum.GOODS_DETAIL_IS_EMPTY.getMessage());
        }
        ZuimeiGoodsDTO dto = zuimeiGoodsDTO.get(0);

        ZuimeiGoodsDetailDTO detail = new ZuimeiGoodsDetailDTO();
        ZuimeiGoodsDetailDTO result = goodsDetail(detail, goods_id);
        Double anti_growth = Double.valueOf(dto.getAnti_growth());//购买返
        Integer ifcoupon = dto.getIfcoupon();
        if (ifcoupon == 1) {
            result.setIfcoupon(true);
        }
        BeanUtils.copyProperties(dto, result);
//        Double reback_money = testMoneyService.getSelftRakeBackRate(user_id);
//        Double commission = anti_growth * reback_money;
//        Double upreback_money = testMoneyService.getUpLevelRakeBackRate(reback_money);
//        Double upcommission = anti_growth * upreback_money;
//        String vipDesc = filterGoodsUtil.vipDesc(upcommission, user_id);
//        result.setAnti_growth(String.format("%.2f",commission));
//        result.setVipDesc(vipDesc);

        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 最美天气-小美贝淘礼金兑换
     **/
    @Override
    public Tip tlj(Integer user_id, String token_id, String price, String goods_id, String goods_name, String pic) throws Exception {
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id)
                || StringUtils.isEmpty(price) || StringUtils.isEmpty(goods_id)) {
            return ResultUtil.result(BizExceptionEnum.PARAM_EMPTY.getCode(), BizExceptionEnum.PARAM_EMPTY.getMessage());
        }
        String money = "0";//预兑换金额
        ZuimeiUserInfoDTO info = zuimeiMapper.getZuimeiUserInfo(user_id);
        if (null == info) {
            return ResultUtil.result(BizExceptionEnum.ZUIMEI_EXCHANGE_NOT_ENOUGH.getCode(), BizExceptionEnum.ZUIMEI_EXCHANGE_NOT_ENOUGH.getMessage());
        }
        String balance = info.getMoney();//用户余额
        if (Double.valueOf(balance) < 1) {//用户账户券额不足1元时
            return ResultUtil.result(BizExceptionEnum.ZUIMEI_EXCHANGE_NOT_ENOUGH.getCode(), BizExceptionEnum.ZUIMEI_EXCHANGE_NOT_ENOUGH.getMessage());
        }
        if (Double.valueOf(price) < 1) {//商品价格少于1元
            return ResultUtil.result(BizExceptionEnum.ZUIMEI_GOODS_PRICE_ERROR.getCode(), BizExceptionEnum.ZUIMEI_GOODS_PRICE_ERROR.getMessage());
        }
        if (Double.valueOf(balance) > Double.valueOf(price)) {//用户余额大余价格时
            money = price;
        }
        if (Double.valueOf(balance) < Double.valueOf(price) && Double.valueOf(balance) >= 1) {//用户余额小余价格时
            money = balance;
        }
        if (Double.valueOf(money) > 5) {//金额大于5元时,最多兑换5元
            money = "5";
        }
        JSONObject data = new JSONObject();
        String url = tblmPropertyConfig.getUrl();
        String appkey = tblmPropertyConfig.getAppKey();
        String secret = tblmPropertyConfig.getAppSecret();
        String[] adzoneIds = tblmPropertyConfig.getpId().split("_");
        String adzoneId = adzoneIds[adzoneIds.length - 1];
        TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
        TbkDgVegasTljCreateRequest req = new TbkDgVegasTljCreateRequest();
        req.setAdzoneId(Long.valueOf(adzoneId));
        req.setItemId(Long.valueOf(goods_id));
        req.setTotalNum(1L);
        req.setName("核桃现金券");
        req.setUserTotalWinNumLimit(1L);
        req.setSecuritySwitch(true);
        req.setPerFace(money);
        req.setSendStartTime(new Date());
        req.setUseEndTime("1");
        req.setUseEndTimeMode(1L);
        TbkDgVegasTljCreateResponse rsp = client.execute(req);
        String body = rsp.getBody();
        JSONObject jsonObject = JSON.parseObject(body);
        JSONObject result = jsonObject.getJSONObject("tbk_dg_vegas_tlj_create_response").getJSONObject("result");
        Boolean isSuccess = result.getBoolean("success");
        if (isSuccess) {//成功
            String send_url = result.getJSONObject("model").getString("send_url");//淘礼金领取url
            String rights_id = result.getJSONObject("model").getString("rights_id");//淘礼金id
            data.put("send_url", send_url);

            zuimeiMapper.DecreaseZuimeiBalance(user_id, money);//扣除最美用户券金额
            ZuimeiUserInfoDTO dto = new ZuimeiUserInfoDTO();
            dto.setUser_id(user_id);
            dto.setZuimei_user_id(info.getZuimei_user_id());
            dto.setMoney(money);
            dto.setType(2);
            dto.setGoods_name(goods_name);
            dto.setPic(pic);
            dto.setStatus(1);
            dto.setRights_id(rights_id);
            dto.setGoods_id(goods_id);
            dto.setCreate_time(Constant.y_M_d_H_m_s.format(new Date()));
            zuimeiMapper.insertZuimeiDetail(dto);//明细
        } else {
            String msg_info = result.getString("msg_info");
            return ResultUtil.result(815, msg_info);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), data);
    }

    /**
     * 获取最美用户账户信息
     **/
    @Override
    public Tip zuimeiUserInfo(Integer user_id, Integer zuimei_user_id) {
        if (StringUtils.isEmpty(user_id) && StringUtils.isEmpty(zuimei_user_id)) {
            return ResultUtil.result(BizExceptionEnum.PARAM_EMPTY.getCode(), BizExceptionEnum.PARAM_EMPTY.getMessage());
        }
        ZuimeiUserInfoDTO info;
        if (!StringUtils.isEmpty(zuimei_user_id)) {
            info = zuimeiMapper.getZuimeiUserInfoByZuimeiId(zuimei_user_id);
        } else if (!StringUtils.isEmpty(user_id)) {
            info = zuimeiMapper.getZuimeiUserInfo(user_id);
        } else {
            return ResultUtil.result(BizExceptionEnum.PARAM_EMPTY.getCode(), BizExceptionEnum.PARAM_EMPTY.getMessage());
        }

        if (null != info) {
            String nick_name = info.getNick_name();
            String money = info.getMoney();
            if (StringUtils.isEmpty(money)) {
                info.setMoney("0");
            }
            if (!StringUtils.isEmpty(nick_name)) {
                info.setNick_name(EmojiUtil.decode(nick_name));
            }
        } else {
            info = new ZuimeiUserInfoDTO();
            info.setMoney("0");
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), info);
    }

    /**
     * 小美贝-获取兑换记录
     **/
    @Override
    public Tip exchangeHistory(Integer zuimei_user_id, Integer pageNum, String type) throws Exception {
        if (StringUtils.isEmpty(zuimei_user_id)) {
            return ResultUtil.result(BizExceptionEnum.PARAM_EMPTY.getCode(), BizExceptionEnum.PARAM_EMPTY.getMessage());
        }
        Page<ZuimeiUserInfoDTO> page = new Page<>(pageNum, Constant.PAGE_SIZE_20);
        if (StringUtils.isEmpty(type)) {
            type = "1";
        }
        List<ZuimeiUserInfoDTO> list = zuimeiMapper.selectHistoryList(page, zuimei_user_id, type);
        page.setRecords(list);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), page);
    }

    /**
     * 最美天气-小美贝淘礼金使用情况查询
     **/
    @Override
    public Tip tljUseDetail(String rights_id, String time) throws Exception {
        String url = tblmPropertyConfig.getUrl();
        String appkey = tblmPropertyConfig.getAppKey();
        String secret = tblmPropertyConfig.getAppSecret();
        TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
        TbkDgVegasTljInstanceReportRequest req = new TbkDgVegasTljInstanceReportRequest();
        if (StringUtils.isEmpty(rights_id)) {
            List<TLJListInfoDTO> list = zuimeiMapper.selectTLJList(time);
            for (TLJListInfoDTO info : list) {
                String rightsId = info.getRights_id();
                Double money = Double.valueOf(info.getMoney());
                Integer id = info.getId();
                Integer user_id = info.getUser_id();
                if (StringUtils.isEmpty(rightsId)) {
                    continue;
                }
                req.setRightsId(rightsId);
                TbkDgVegasTljInstanceReportResponse rsp = client.execute(req);
                String body = rsp.getBody();
                JSONObject jsonObject = JSON.parseObject(body);
                JSONObject result = jsonObject.getJSONObject("tbk_dg_vegas_tlj_instance_report_response").getJSONObject("result");
                Boolean isSuccess = result.getBoolean("success");
                if (isSuccess) {//成功
                    JSONObject model = result.getJSONObject("model");
                    if (model.size() > 0) {//有返回数据时
                        double fp_refund_amount = result.getJSONObject("model").getDouble("fp_refund_amount");//退款红包金额
                        double use_amount = result.getJSONObject("model").getDouble("use_amount");//红包核销金额
                        double refund_amount = result.getJSONObject("model").getDouble("refund_amount");//失效回退金额
                        double unfreeze_amount = result.getJSONObject("model").getDouble("unfreeze_amount");//解冻金额
                        if ((fp_refund_amount > 0 && fp_refund_amount == money) || (unfreeze_amount > 0 && unfreeze_amount == money)
                                || (refund_amount > 0 && refund_amount == money)) {//未使用
                            //1.已兑换  2已使用  3已过期
                            zuimeiMapper.updateTLJStatus(id, 3);//更新淘礼金状态
                            zuimeiMapper.IncreaseZuimeiBalance(user_id, money);//返回券额到用户账户
                            ZuimeiUserInfoDTO dto = new ZuimeiUserInfoDTO();
                            dto.setUser_id(user_id);
                            dto.setZuimei_user_id(info.getZuimei_user_id());
                            dto.setMoney(String.valueOf(money));
                            dto.setType(3);
                            dto.setStatus(1);
                            dto.setRights_id(rightsId);
                            dto.setGoods_id(info.getGoods_id());
                            dto.setGoods_name(info.getGoods_name());
                            dto.setPic(info.getPic());
                            dto.setCreate_time(Constant.y_M_d_H_m_s.format(new Date()));
                            zuimeiMapper.insertZuimeiDetail(dto);//明细
                        } else if (use_amount > 0 && use_amount <= money) {//已使用
                            zuimeiMapper.updateTLJStatus(id, 2);//更新淘礼金状态
                        }
                    }
                } else {
                    String msg_info = result.getString("msg_info");
                    return ResultUtil.result(816, msg_info);
                }
            }
        } else {
            req.setRightsId(rights_id);
            TbkDgVegasTljInstanceReportResponse rsp = client.execute(req);
            String body = rsp.getBody();
            JSONObject jsonObject = JSON.parseObject(body);
            JSONObject result = jsonObject.getJSONObject("tbk_dg_vegas_tlj_instance_report_response").getJSONObject("result");
            Boolean isSuccess = result.getBoolean("success");
            if (isSuccess) {//成功
                JSONObject model = result.getJSONObject("model");
                return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), model);
            } else {
                String msg_info = result.getString("msg_info");
                return ResultUtil.result(816, msg_info);
            }
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 现金券兑换淘礼金记录
     **/
    @Override
    public Tip tljHistory(Integer user_id, String token_id, Integer pageNum) throws Exception {
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id)) {
            return ResultUtil.result(BizExceptionEnum.PARAM_EMPTY.getCode(), BizExceptionEnum.PARAM_EMPTY.getMessage());
        }
        Page<TLJHistoryDTO> page = new Page<>(pageNum, Constant.PAGE_SIZE_20);
        List<TLJHistoryDTO> list = zuimeiMapper.selectTLJHistory(page, user_id);
        page.setRecords(list);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), page);
    }

    /**
     * 最美天气使用-商品列表
     **/
    @Override
    public Tip arrondiGoods(Integer page, Integer pageSize, String queryKey) throws Exception {
        JSONArray result = new JSONArray();
        if (StringUtils.isEmpty(page)) {
            page = 1;
        }
        if (StringUtils.isEmpty(pageSize)) {
            pageSize = 10;
        }
        if (StringUtils.isEmpty(queryKey)) {
            queryKey = "精品";
        }
        String url = tblmPropertyConfig.getUrl();
        String appkey = tblmPropertyConfig.getAppKey();
        String secret = tblmPropertyConfig.getAppSecret();
        String[] adzoneIds = tblmPropertyConfig.getpId().split("_");
        String adzoneId = adzoneIds[adzoneIds.length - 1];
        TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
        TbkDgMaterialOptionalRequest req = new TbkDgMaterialOptionalRequest();
        req.setPageNo(Long.valueOf(page));
        req.setPageSize(Long.valueOf(pageSize));
        req.setAdzoneId(Long.valueOf(adzoneId));
        req.setMaterialId(28027L);//实时热销榜-大快消
        req.setQ(queryKey);
        //排序_des（降序），排序_asc（升序），销量（total_sales），淘客佣金比率（tk_rate）， 累计推广量（tk_total_sales），总支出佣金（tk_total_commi），价格（price）
        req.setSort("total_sales_des");
        TbkDgMaterialOptionalResponse execute = client.execute(req);
        String resultStr = execute.getBody();
        if (!StringUtils.isEmpty(resultStr)) {
            com.alibaba.fastjson.JSONObject jsonObject = JSON.parseObject(resultStr);
            if (jsonObject.containsKey("error_response")) {
                String msg = jsonObject.getJSONObject("error_response").getString("sub_msg");
                return ResultUtil.result(800, msg);
            }
            JSONObject result_list = jsonObject.getJSONObject("tbk_dg_material_optional_response").getJSONObject("result_list");
            JSONArray map_data = result_list.getJSONArray("map_data");
            if (map_data.size() < 1) {

            }
            SimpleDateFormat sdf = Constant.y_M_d;
            Long date = sdf.parse(sdf.format(new Date())).getTime();
            map_data.forEach(list -> {
                ZuimeiDTO detailDTO = new ZuimeiDTO();
                JSONObject t = JSON.parseObject(JSON.toJSONString(list));
                Double price = t.getDouble("zk_final_price");
                String goodsName = t.getString("title");

                Double coupon_amount = t.getDouble("coupon_amount");  //优惠卷卷额
                if (StringUtils.isEmpty(coupon_amount)) {
                    coupon_amount = 0.0;
                }
                Double price_after_coupons = price - coupon_amount;
                Double commission_rate = t.getDouble("commission_rate") / 100 / 100;
                Double anti_growth = price_after_coupons * commission_rate;
                Long coupon_start_time = t.getLong("coupon_start_time"); //优惠卷时间
                Long coupon_end_time = t.getLong("coupon_end_time");
                Long coupon = t.getLong("coupon_amount");
                if (StringUtils.isEmpty(coupon)) {
                    coupon = 0L;
                }
                boolean IfCoupon = false;
                if (!StringUtils.isEmpty(coupon_start_time) && !StringUtils.isEmpty(coupon_end_time)) {
                    if (coupon > 0 && (date >= coupon_start_time && date <= coupon_end_time)) { //判断是否有卷并且 存在有效期
                        IfCoupon = true;
                    }
                    detailDTO.setCouponEndTime(sdf.format(new Date(coupon_end_time)));
                    detailDTO.setCouponStartTime(sdf.format(new Date(coupon_start_time)));
                }
                detailDTO.setIfCoupon(IfCoupon);
                detailDTO.setGoodsName(goodsName);
                detailDTO.setPriceAfterCoupon(String.valueOf(coupon_amount));
                detailDTO.setGoodsId(t.getString("item_id"));
                detailDTO.setSales(t.getString("volume"));
                detailDTO.setGoodsMainPicture(t.getString("pict_url") + "_310x310.jpg");
//                detailDTO.setCouponUrl("https:"+t.getString("coupon_share_url"));
                detailDTO.setCouponUrl("https://meimg.su.bcebos.com/htsq/htsq-hwtq/htdq_hwtq.apk");
                detailDTO.setPrice(String.format("%.2f", price));
                detailDTO.setPriceAfterCoupon(String.format("%.2f", price_after_coupons));
                detailDTO.setCommission(String.format("%.2f", anti_growth));
                detailDTO.setCoupon(coupon.toString());
                result.add(detailDTO);
            });
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 最美天气使用-商品详情
     **/
    @Override
    public Tip goodsDetail(String goods_id) throws Exception {
        ZuimeiGoodsDetailDTO detailDTO = detailFromTaoBao(goods_id);
        if (null == detailDTO) {
            return ResultUtil.result(BizExceptionEnum.GOODS_DETAIL_IS_EMPTY.getCode(), BizExceptionEnum.GOODS_DETAIL_IS_EMPTY.getMessage());
        }
        ZuimeiDetailDTO result = new ZuimeiDetailDTO();
        result.setGoodsId(detailDTO.getGoods_id());
        result.setGoodsName(detailDTO.getGoods_name());
        result.setGoodsDesc(detailDTO.getItemdesc());
        result.setPrice(detailDTO.getPrice());
        result.setCoupon(detailDTO.getPrice_coupons());
        result.setPriceAfterCoupon(detailDTO.getPrice_after_coupons());
        result.setCommission(detailDTO.getAnti_growth());
        result.setCouponStartTime(detailDTO.getCoupon_startTime());
        result.setCouponEndTime(detailDTO.getCoupon_endTime());
        result.setIfCoupon(detailDTO.isIfcoupon());
        result.setGoodsMainPicture(detailDTO.getPic());
        result.setGoodsCarouselPictures(detailDTO.getGoodsCarouselPictures());
        result.setShopName(detailDTO.getShopname());
        result.setSales(String.valueOf(detailDTO.getSales()));
        result.setH5Url(detailDTO.getH5Url());
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 最美天气-小美贝用户信息接收
     **/
    @Override
    public Tip receiveInfo(Integer zuimei_user_id, String money) throws Exception {
        if (StringUtils.isEmpty(zuimei_user_id) || StringUtils.isEmpty(money)) {
            return ResultUtil.result(BizExceptionEnum.PARAM_EMPTY.getCode(), BizExceptionEnum.PARAM_EMPTY.getMessage());
        }
        logger.info("【开始最美天气-小美贝用户信息接收zuimei_user_id：" + zuimei_user_id + "】");
        ZuimeiUserInfoDTO zuimeiUserInfo = zuimeiMapper.getZuimeiBalance(zuimei_user_id);
        ZuimeiUserInfoDTO update = new ZuimeiUserInfoDTO();
        update.setMoney(money);
        update.setZuimei_user_id(zuimei_user_id);
        update.setCreate_time(Constant.y_M_d_H_m_s.format(new Date()));
        Integer id = 0;
        if (null == zuimeiUserInfo) {//不存在则新增
            id = zuimeiMapper.insertZuimeiBalance(update);
        } else {//更新
            id = zuimeiMapper.updateZuimeiBalance(update);
        }
        if (id > 0) {
            update.setStatus(1);
            update.setType(1);
            zuimeiMapper.insertZuimeiDetail(update);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 商品详情h5图片
     */
    public ZuimeiGoodsDetailDTO goodsDetail(ZuimeiGoodsDetailDTO detail, String itemid) throws Exception {
        String h5_url = "";
        List<String> resultList = new ArrayList();
        String dataStr = tbGoodsServiceImpl.getGoodsDetail(itemid);
        if (!StringUtils.isEmpty(dataStr)) {
            JSONObject json = JSON.parseObject(dataStr);
            if (json.containsKey("code") && json.getInteger("code") == 1) {
                JSONObject data = json.getJSONObject("data");
                if (null != data) {
                    String taobao_image = data.containsKey("taobao_image") ? data.getString("taobao_image") : ""; //轮播图
                    String itempic = data.containsKey("itempic") && null != data.getString("itempic") ? data.getString("itempic") : "";
                    if (!StringUtils.isEmpty(taobao_image)) {
                        if (taobao_image.contains(",")) {
                            String[] goodsCarouselPictures = taobao_image.split(",");
                            for (String s : goodsCarouselPictures) {
                                resultList.add(s);
                            }
                        } else {
                            resultList.add(taobao_image);
                        }
                    }
                    if (resultList.size() < 1) {
                        resultList.add(itempic);
                    }
                    //预览h5
                    h5_url = tblmPropertyConfig.getIntroduceUrl() + itemid;
                }
            } else {  //好单库没有 订单详情 则去 大淘客查询
                TreeMap<String, String> paraMap = new TreeMap<>();
                paraMap.put("version", dtkPropertyConfig.getVersion());
                paraMap.put("goodsId", itemid);
                paraMap.put("appKey", dtkPropertyConfig.getAppKey());
                paraMap.put("sign", dtkService.getSignStr(paraMap, dtkPropertyConfig.getAppSecret()));
                dataStr = dtkService.sendGet(dtkPropertyConfig.getDateil(), paraMap);
                if (!StringUtils.isEmpty(dataStr)) {
                    json = JSON.parseObject(dataStr);
                    if (null != json && json.containsKey("code") && json.getInteger("code") == 0) {
                        JSONObject data = json.getJSONObject("data");

                        String itempic = data.containsKey("mainPic") ? data.getString("mainPic") : null;
                        String detailPics = data.containsKey("detailPics") ? data.getString("detailPics") : "";
                        if (!StringUtils.isEmpty(detailPics)) {
                            if (detailPics.contains(",")) {
                                String[] goodsCarouselPictures = detailPics.split(",");
                                for (String s : goodsCarouselPictures) {
                                    resultList.add(s);
                                }
                            } else {
                                resultList.add(detailPics);
                            }
                        }
                        if (resultList.size() < 1) {
                            resultList.add(itempic);
                        }

                        //预览h5
                        h5_url = tblmPropertyConfig.getIntroduceUrl() + itemid;
                    }
                }
            }
        }
        detail.setH5Url(h5_url);
        detail.setGoodsCarouselPictures(resultList);
        return detail;
    }

    /**
     * 查询是否为小美贝商品
     */
    public Boolean isXMBGoods(Integer user_id, String goods_id) {
        Boolean result = false;
        if (!StringUtils.isEmpty(user_id) && !StringUtils.isEmpty(goods_id)) {
            Wrapper<ZuimeiGoodsDTO> wrapper = new EntityWrapper<>();
            wrapper.eq("goods_id", goods_id);
            List<ZuimeiGoodsDTO> zuimeiGoodsDTO = zuimeiMapper.selectList(wrapper);
            if (zuimeiGoodsDTO.size() > 0) {//是小美贝商品
                Integer id = zuimeiMapper.getUserTLJInfo(user_id, goods_id);//查看用户是否兑换商品的淘礼金并使用了
                if (null != id && id > 0) {//兑换了且未过期
                    result = true;
                    zuimeiMapper.updateTLJUseStatus(id);
                }
            }
        }
        return result;
    }

    /**
     * 从淘宝获取小美贝商品详情
     **/
    public ZuimeiGoodsDetailDTO detailFromTaoBao(String goods_id) throws Exception {
        ZuimeiGoodsDetailDTO detail = new ZuimeiGoodsDetailDTO();
        String dataStr = tbGoodsServiceImpl.getGoodsDetail(goods_id);
        if (!StringUtils.isEmpty(dataStr)) {
            JSONObject json = JSON.parseObject(dataStr);
            if (json.containsKey("code") && json.getInteger("code") == 1) {
                JSONObject data = json.getJSONObject("data");
                if (null != data) {
                    String goodId = data.containsKey("itemid") && null != data.getString("itemid") ? data.getString("itemid") : "";
                    String itemtitle = data.containsKey("itemtitle") && null != data.getString("itemtitle") ? data.getString("itemtitle") : "";
                    String itemprice = data.containsKey("itemprice") && null != data.getString("itemprice") ? data.getString("itemprice") : "";
                    String itemendprice = data.containsKey("itemendprice") && null != data.getString("itemendprice") ? data.getString("itemendprice") : "";
                    String itemsale = data.containsKey("itemsale") && null != data.getString("itemsale") ? data.getString("itemsale") : "";
                    String couponmoney = data.containsKey("couponmoney") && null != data.getString("couponmoney") ? data.getString("couponmoney") : "0";
                    String starttime = data.containsKey("couponstarttime") && null != data.getString("couponstarttime") ? data.getString("couponstarttime") : "";
                    String endtime = data.containsKey("couponendtime") && null != data.getString("couponendtime") ? data.getString("couponendtime") : "";
                    String itemdesc = data.containsKey("itemdesc") && null != data.getString("itemdesc") ? data.getString("itemdesc") : "";
                    String itempic = data.containsKey("itempic") && null != data.getString("itempic") ? data.getString("itempic") : "";
                    String sellernick = data.getString("sellernick");//店铺名
                    String couponstarttimeDate = "";
                    String couponendtimeDate = "";
                    SimpleDateFormat y_m_d = Constant.y_M_d;
                    if (!StringUtils.isEmpty(starttime)) {
                        couponstarttimeDate = y_m_d.format(new Date(new Long(starttime) * 1000));
                    }
                    if (!StringUtils.isEmpty(endtime)) {
                        couponendtimeDate = y_m_d.format(new Date(new Long(endtime) * 1000));
                    }
                    Double tkrates = data.getDouble("tkrates"); //佣金比例百分比
                    Double aDouble = Double.valueOf(itemendprice);
                    Double commission = aDouble * (tkrates / 100);

                    if (!StringUtils.isEmpty(couponmoney) && Double.valueOf(couponmoney) > 0) {
                        detail.setIfcoupon(true);
                    }
                    detail.setShopname(sellernick);
                    detail.setGoods_id(goodId);       //商品id
                    detail.setGoods_name(itemtitle);  //商品名称
                    detail.setPrice(itemprice);       //价格
                    detail.setPrice_after_coupons(itemendprice);  //优惠后价格
                    detail.setSales(Integer.valueOf(itemsale));  //销量
                    detail.setPrice_coupons(couponmoney);     //卷金额
                    detail.setCoupon_startTime(couponstarttimeDate); //圈开始时间
                    detail.setCoupon_endTime(couponendtimeDate);   //圈结束时间
                    detail.setAnti_growth(String.format("%.2f", commission));  //佣金
                    detail.setItemdesc(itemdesc);   //商品描述
                    detail.setPic(itempic);    //主图
                    detail.setType(1);   //平台类型

                    String taobao_image = data.containsKey("taobao_image") ? data.getString("taobao_image") : ""; //轮播图
                    List<String> resultList = new ArrayList();
                    if (!StringUtils.isEmpty(taobao_image)) {
                        if (taobao_image.contains(",")) {
                            String[] goodsCarouselPictures = taobao_image.split(",");
                            for (String s : goodsCarouselPictures) {
                                resultList.add(s);
                            }
                        } else {
                            resultList.add(taobao_image);
                        }
                    }
                    if (resultList.size() < 1) {
                        resultList.add(itempic);
                    }
                    detail.setGoodsCarouselPictures(resultList);
                    //预览h5
                    detail.setH5Url(tblmPropertyConfig.getIntroduceUrl() + goods_id);
                }
            } else {  //好单库没有 订单详情 则去 大淘客查询
                TreeMap<String, String> paraMap = new TreeMap<>();
                paraMap.put("version", dtkPropertyConfig.getVersion());
                paraMap.put("goodsId", goods_id);
                paraMap.put("appKey", dtkPropertyConfig.getAppKey());
                paraMap.put("sign", dtkService.getSignStr(paraMap, dtkPropertyConfig.getAppSecret()));
                dataStr = dtkService.sendGet(dtkPropertyConfig.getDateil(), paraMap);
                if (!StringUtils.isEmpty(dataStr)) {
                    json = JSON.parseObject(dataStr);
                    if (null != json && json.containsKey("code") && json.getInteger("code") == 0) {
                        JSONObject data = json.getJSONObject("data");
                        String goodId = data.containsKey("goodsId") ? data.getString("goodsId") : "";
                        String itemtitle = data.containsKey("dtitle") ? data.getString("dtitle") : "";
                        Integer itemprice = data.containsKey("originalPrice") ? data.getInteger("originalPrice") : 0;
                        Integer itemendprice = data.containsKey("actualPrice") ? data.getInteger("actualPrice") : 0;
                        Integer itemsale = data.containsKey("monthSales") ? data.getInteger("monthSales") : 0;
                        Integer couponmoney = data.containsKey("couponPrice") ? data.getInteger("couponPrice") : 0;
                        String couponstarttimeDate = data.containsKey("couponStartTime") ? data.getString("couponStartTime") : "";
                        String couponendtimeDate = data.containsKey("couponEndTime") ? data.getString("couponEndTime") : "";
                        String sellernick = data.containsKey("shopName") ? data.getString("shopName") : "";
                        Boolean ifCoupon = false;
                        if (couponmoney > 0.0) {
                            ifCoupon = true;
                        }
                        String couponstarttime = "";
                        String couponendtime = "";
                        SimpleDateFormat y_m_d = Constant.y_M_d;
                        if (!StringUtils.isEmpty(couponstarttimeDate)) {
                            couponstarttime = y_m_d.format(y_m_d.parse(couponstarttimeDate));
                        }
                        if (!StringUtils.isEmpty(couponendtimeDate)) {
                            couponendtime = y_m_d.format(y_m_d.parse(couponendtimeDate));
                        }
                        Double tkrates = data.containsKey("commissionRate") ? data.getDouble("commissionRate") : 0.0; //佣金比例百分比
                        Double commission = itemendprice.doubleValue() * (tkrates / 100);
                        String itemdesc = data.containsKey("desc") ? data.getString("desc") : null;
                        String itempic = data.containsKey("mainPic") ? data.getString("mainPic") : null;
                        detail.setShopname(sellernick);
                        detail.setIfcoupon(ifCoupon);  // 是否有优惠卷
                        detail.setGoods_id(goodId);  //商品id
                        detail.setGoods_name(itemtitle);  //商品名称
                        detail.setPrice(itemprice.toString());   //价格
                        detail.setPrice_after_coupons(itemendprice.toString());  //优惠后价格
                        detail.setSales(itemsale); //销量
                        detail.setPrice_coupons(couponmoney.toString());  //卷金额
                        detail.setCoupon_startTime(couponstarttime); //圈开始时间
                        detail.setCoupon_endTime(couponendtime);   //圈结束时间
                        detail.setAnti_growth(String.format("%.2f", commission));  //佣金
                        detail.setItemdesc(itemdesc);    //商品描述
                        detail.setPic(itempic);     //主图
                        detail.setType(1);    //平台类型
                        String detailPics = data.containsKey("detailPics") ? data.getString("detailPics") : "";
                        List<String> resultList = new ArrayList<>();
                        if (!StringUtils.isEmpty(detailPics)) {
                            resultList.add(detailPics);
                            if (detailPics.contains(",")) {
                                String[] goodsCarouselPictures = detailPics.split(",");
                                for (String s : goodsCarouselPictures) {
                                    resultList.add(s);
                                }
                            }
                        }
                        if (resultList.size() < 1) {
                            resultList.add(itempic);
                        }
                        detail.setGoodsCarouselPictures(resultList);
                        //预览h5
                        detail.setH5Url(tblmPropertyConfig.getIntroduceUrl() + goods_id);
                    } else {
                        return null;
                    }
                }
            }
        } else {
            return null;
        }
        return detail;
    }
}
