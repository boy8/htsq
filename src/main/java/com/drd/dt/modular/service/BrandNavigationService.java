package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface BrandNavigationService {

    /**
     * 获取品牌导航列表
     **/
    Tip brandList(Integer page, Integer type) throws Exception;
}
