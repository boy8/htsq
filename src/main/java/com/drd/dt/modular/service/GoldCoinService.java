package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface GoldCoinService {

    /**
     * 用户登录获取随机金币
     **/
    Tip getRandomCoin(Integer user_id,String token_id) throws Exception;

    /**
     * 金币兑换红包
     **/
    Tip exchangeRed(Integer user_id,String token_id,Integer coin) throws Exception;

    /**
     * 使用红包
     */
    Integer userRedpacket(String user_id) throws Exception;

    /**
     * 订单结算时给用户返下单时的红包
     */
   // void rebateUserRedpacket(String order_id) throws Exception;

    /**
     * 我的红包列表
     **/
    Tip myRedpacket(Integer user_id,String token_id,Integer page) throws Exception;

}
