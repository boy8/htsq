package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface ShareActivityService {

    /**
     * 用户点击拆红包（首页点红包进入）
     **/
    Tip clickRedPacket(Integer user_id, String token_id) throws Exception;

    /**
     * 用户拆红包、分享好友和群
     **/
    Tip shareGroup(Integer user_id, String token_id,Integer type);
}
