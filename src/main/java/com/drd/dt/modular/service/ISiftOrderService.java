package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.OwnGoodsOrderDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ISiftOrderService {

    /**
     * 生成订单
     */
    void addOrderGift(OwnGoodsOrderDTO ownGoodsOrderDTO, HttpServletResponse response, HttpServletRequest request) throws Exception;

    /*
     *  申请退款
     * **/
    Tip applyRefund(Integer user_id, String token, String order_id,String cause,String log_image);

    /**
     * 取消退款
     * */
    Tip cancelRefund(Integer user_id, String token, String order_id);

    /**
     * 核桃精选商品 微信/支付宝 退款
     **/
    public Tip refundMoney(Integer id, String order_id, Integer user_id, Integer refund_status) throws Exception;


    /**
     * 核桃精选商品订单详情
     */
    Tip getSiftOrderDetail(String order_id) throws Exception;

    /**
     *后申请/售后处理中
     * **/
    Tip afterSale(Integer user_id, String token,Integer refund_status);

    /**
     * 折扣专区 判断当前用户是否有权限购买并且只能买一个
     * */
    Tip discountClassifyQualification(Integer user_id,String goods_id) throws Exception;
}
