package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/***
 * 365 淘卷
 * */
public interface ForVolumeService {
    /**
     * 订单接口
     *
     * @param user_id   用户ID
     */
    public Tip getMyOrder(String user_id);

    /**
     * 获取365H 5界面
     *
     * @param user_id  用户ID
     * @param productNo 365淘券品牌id，用于单品牌跳转 品牌参数列表请咨询商务）
     */
     Tip getForVolumeHome(String user_id, String productNo);
}
