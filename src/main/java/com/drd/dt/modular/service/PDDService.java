package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;
import io.swagger.models.auth.In;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface PDDService {

    /**
     * 拼多多 訂單信息
     **/
    Tip getOrderListByCustomer(String start_time, String end_time, String type) throws Exception;

    /**
     * 拼多多搜索
     *
     * @return
     */
    Tip goodsSearch(String keywords, String page, String sortType, String ifcoupon, Integer user_id) throws Exception;

    /**
     * 拼多多商品详情
     */
    Tip goodsDetail(String goodsId, Integer user_id, HttpServletRequest request, String deviceT, String app_version) throws Exception;

    /**
     * 获取拼多多推广位链接
     */
    Tip getPddUrl(String user_id, String goods_id, String buytype) throws Exception;


    Tip pddPutOnRecords(String user_id) throws Exception;
    Tip getIsArchives2(String ip) ;


    /**
     * 生成拼多多红包推广链接
     */
    Tip pddRedpacket(String user_id) throws Exception;

    /**
     * 生成拼多多主题推广链接
     */
    Tip pddThemeUrl(String user_id, Long theme_id) throws Exception;

    /**
     * 获取拼多多主题推广活动主题id
     */
    Tip getThemeid(String name) throws Exception;


    Tip list(String keywords, Integer page,HttpServletRequest request);

    Tip detail(String goodsId, HttpServletRequest request);

}
