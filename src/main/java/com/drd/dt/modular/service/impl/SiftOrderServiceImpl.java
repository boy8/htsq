package com.drd.dt.modular.service.impl;

import com.alibaba.fastjson.JSON;
import com.drd.dt.back.dao.GoodsSpecificationBaseMapper;
import com.drd.dt.back.dao.ISiftGoodsBaseMapper;
import com.drd.dt.back.dao.ISiftOrderBaseMapper;
import com.drd.dt.back.dto.SiftGoodsDTO;
import com.drd.dt.back.dto.SiftGoodsSpecificationDTO;
import com.drd.dt.back.dto.SiftOrderDTO;
import com.drd.dt.back.dto.SiftOrderLogDTO;
import com.drd.dt.back.entity.OwnGoods;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.modular.dao.*;
import com.drd.dt.modular.dto.OwnGoodsOrderDTO;
import com.drd.dt.modular.dto.PayDTO;
import com.drd.dt.modular.dto.UserInfoDTO;
import com.drd.dt.modular.entity.OrderGift;
import com.drd.dt.modular.entity.User;
import com.drd.dt.modular.service.ISiftGoodsService;
import com.drd.dt.modular.service.ISiftOrderLogService;
import com.drd.dt.modular.service.ISiftOrderService;
import com.drd.dt.modular.service.PayService;
import com.drd.dt.util.JedisClient;
import com.drd.dt.util.ResultUtil;
import com.drd.dt.util.StringUtil;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SiftOrderServiceImpl implements ISiftOrderService {

    @Autowired
    private JedisClient jedisClient;

    @Autowired
    private OwnGoodsMapper ownGoodsMapper;

    @Autowired
    private PayService payService;

    @Autowired
    private OrderGiftMapper orderGiftMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private GoodsSpecificationBaseMapper mapper;

    @Autowired
    private ISiftGoodsBaseMapper siftGoodsBaseMapper;
    @Autowired
    private ISiftOrderBaseMapper siftOrderBaseMapper;

    @Autowired
    private SiftOrderMapper siftOrderMapper;
    @Autowired
    private SiftGoodsSpecificationMapper siftGoodsSpecificationMapper;

    @Autowired
    private SiftGoodsMapper siftGoodsMapper;
    @Autowired
    private ISiftOrderLogService iSiftOrderLogService;
    @Autowired
    private UserMoneyServiceImpl userMoneyService;
    @Autowired
    private GoldCoinServiceImpl goldCoinService;
    @Autowired
    private ISiftOrderLogMapper siftOrderLogMapper;

    @Override
    public void addOrderGift(OwnGoodsOrderDTO ownGoodsOrderDTO, HttpServletResponse response, HttpServletRequest request) throws Exception {
        Integer user_id = ownGoodsOrderDTO.getUser_id();
        Float actual_amount = ownGoodsOrderDTO.getActual_amount();
        if (StringUtils.isEmpty(actual_amount) || actual_amount <= 0) {//金额为空
            Tip result = ResultUtil.result(BizExceptionEnum.USER_MONEY_ERROR.getCode(), BizExceptionEnum.USER_MONEY_ERROR.getMessage());
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write(JSON.toJSONString(result));
            response.getWriter().flush();
            response.getWriter().close();
        }
        if (StringUtils.isEmpty(user_id)) {//用户id为空
            Tip result = ResultUtil.result(BizExceptionEnum.USER_USERNAME_EMPTY.getCode(), BizExceptionEnum.USER_USERNAME_EMPTY.getMessage());
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write(JSON.toJSONString(result));
            response.getWriter().flush();
            response.getWriter().close();
        } else {
            User user = userMapper.selectById(user_id);
            if (null == user) {//用户不存在
                Tip result = ResultUtil.result(BizExceptionEnum.OT_USER_ERROR.getCode(), BizExceptionEnum.OT_USER_ERROR.getMessage());
                response.setContentType("text/html;charset=UTF-8");
                response.getWriter().write(JSON.toJSONString(result));
                response.getWriter().flush();
                response.getWriter().close();
            }
        }

        /** 1 生成订单**/
        /** 1.1 生成订单号 规则 ：user_id + 时间+ goods_id **/
        String orderId = "";
        orderId = ownGoodsOrderDTO.getUser_id() + Constant.yyyymmddhhmmss.format(new Date()) + ownGoodsOrderDTO.getGoods_id();

        //1.1.1 提前预判 是否重复炒作
        String redisData = (String) jedisClient.get("htsq:addOrderGift:" + orderId);
        if (!StringUtils.isEmpty(redisData)) {
            return;
        }
        //1.1.2 提前防止用户多次操作 redis 缓存
        jedisClient.setWithExpire("htsq:addOrderGift:" + orderId, orderId, 10);

        ownGoodsOrderDTO.setStatus(0);
        ownGoodsOrderDTO.setOrder_id(orderId);
        /** 1.2 查询商品信息**/
        SiftGoodsDTO siftGoodsDTO = siftGoodsBaseMapper.queryAll(ownGoodsOrderDTO.getGoods_id() + "");

        if(siftGoodsDTO.getDiscounts_type() == 0) { //核桃精选 才考虑手机格式匹配
            if (!StringUtil.isMobileNO(ownGoodsOrderDTO.getShipping_phone())) { //手机格式不支持
                Tip result = ResultUtil.result(BizExceptionEnum.USER_PHONE_ERROR.getCode(), BizExceptionEnum.USER_PHONE_ERROR.getMessage());
                response.setContentType("text/html;charset=UTF-8");
                response.getWriter().write(JSON.toJSONString(result));
                response.getWriter().flush();
                response.getWriter().close();
            }
        }

        if (null != siftGoodsDTO) {
            /** 1.2.1 订单存货验证**/
            Integer number = ownGoodsOrderDTO.getNumber();
            if (null == number) {
                number = 1;
            }
            if (!(siftGoodsDTO.getInventory() >= number)) { //库存不够
                throw new BussinessException(BizExceptionEnum.GOODS_NUM_ERROR);
            }
            ownGoodsOrderDTO.setGoods_image(siftGoodsDTO.getSmall_icon());//展示图
            ownGoodsOrderDTO.setGoods_name(siftGoodsDTO.getTitle());
        } else {//商品不存在 返回提示
//            return ResultUtil.result(BizExceptionEnum.GOODS_DETAIL_IS_EMPTY.getCode(), BizExceptionEnum.GOODS_DETAIL_IS_EMPTY.getMessage());
        }
        /** 1.3 数据库写入订单**/
        SiftOrderDTO gift = siftOrderBaseMapper.queryAllOrderId(orderId);
        if (gift != null && gift.getOrder_id().equals(orderId)) {//orderID已经存在，不重复添加
            return;
        }

        //新增订单信息
        SiftOrderDTO addData = new SiftOrderDTO();
        addData.setBuy_type(ownGoodsOrderDTO.getBuy_type());//订单购买方式
        addData.setProduct_id(ownGoodsOrderDTO.getGoods_id() + "");//商品ID
        addData.setProduct_pic_url(ownGoodsOrderDTO.getGoods_image());//图片
        addData.setProduct_name(ownGoodsOrderDTO.getGoods_name());//商品名称
        addData.setUser_id(ownGoodsOrderDTO.getUser_id());//用户ID
        addData.setStatus(ownGoodsOrderDTO.getStatus());//订单状态
        addData.setOrder_id(ownGoodsOrderDTO.getOrder_id());//订单号
        addData.setNumber(ownGoodsOrderDTO.getNumber());//数量
        addData.setDiscounts_type(siftGoodsDTO.getDiscounts_type());

        addData.setActual_amount(siftGoodsDTO.getNow_price().doubleValue() * ownGoodsOrderDTO.getNumber());//实际付款  数据库商品单价* 购买数量
        addData.setPaytype(ownGoodsOrderDTO.getActual_amount_type());//支付类型：1 支付宝、2 微信
        addData.setGoods_spe_id(ownGoodsOrderDTO.getGoods_spe_id());//规格ID集合（',' 隔开）
        if(siftGoodsDTO.getDiscounts_type() == 0) { //核桃精选 才考虑存储收货地址
            addData.setShipping_address(ownGoodsOrderDTO.getShipping_address());//订单收货地址
            addData.setShipping_user_name(ownGoodsOrderDTO.getShipping_user_name());//收货人名称
            addData.setShipping_phone(ownGoodsOrderDTO.getShipping_phone());//收货人电话
        }else if(siftGoodsDTO.getDiscounts_type() == 1){
            addData.setShipping_address("线下折扣");//订单收货地址
            addData.setShipping_user_name("线下折扣");//收货人名称
            addData.setShipping_phone("线下折扣");//收货人电话
        }

        Double reback_money = userMoneyService.getSelftRakeBackRate(user_id);//佣金比例

        Double Rake_back = Double.parseDouble(String.format("%.2f", (siftGoodsDTO.getRake_back() * ownGoodsOrderDTO.getNumber())));//返佣 数量* 单个商品返佣
        addData.setRake_back(Rake_back);//返佣 金额
        addData.setUser_rake_back(0.0);//'用户返佣'  待结算 更新该值
        addData.setUser_rake_back_yugu(reback_money * Rake_back);//''预估总收益''
        addData.setUser_rake(reback_money);//当前用户佣金比例
        Integer redpacket = goldCoinService.userRedpacket(ownGoodsOrderDTO.getUser_id() + "");
        if (null == redpacket) {
            redpacket = 0;
        }
        addData.setRedpacket(redpacket);//红包金额
        addData.setStatus(0);
        int req = siftOrderBaseMapper.addOrderGift(addData);//sql 默认refund_status = 0
        if (req > 0) {
            if(siftGoodsDTO.getDiscounts_type() == 0){ //核桃精选 才考虑存储收货地址
                /** 1.4 如果没有user没有基础地址 保存一个 **/
                UserInfoDTO infoDTO = userMapper.getUserInfo(ownGoodsOrderDTO.getUser_id());
                if (null != infoDTO && StringUtils.isEmpty(infoDTO.getAddr())) {
                    User user = new User();
                    user.setAddr(ownGoodsOrderDTO.getShipping_address());
                    user.setId(ownGoodsOrderDTO.getUser_id());
                    userMapper.updateUserAddr(user);
                }
            }

            /** 2 根据actual_amount_type 走不同 付款方式**/
            switch (ownGoodsOrderDTO.getActual_amount_type()) {
                case 1: {
                    PayDTO payDTO = new PayDTO();
                    payDTO.setOrderType(1);
                    payDTO.setBuyType(ownGoodsOrderDTO.getBuy_type());
                    payDTO.setPaytype(1);
                    payDTO.setMoney(ownGoodsOrderDTO.getActual_amount() + "");
                    payDTO.setTitle(ownGoodsOrderDTO.getGoods_name() + "【核桃精选】");
                    payDTO.setOrder_id(ownGoodsOrderDTO.getOrder_id());
                    payDTO.setGoods_id(ownGoodsOrderDTO.getGoods_id() + "");
                    payService.alipayH5(response, request, payDTO);
//                return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), tip);
                }
                break;
                case 2: {
                    PayDTO payDTO = new PayDTO();
                    payDTO.setPaytype(2);
                    payDTO.setBuyType(ownGoodsOrderDTO.getBuy_type());
                    payDTO.setOrderType(1);
                    payDTO.setMoney(ownGoodsOrderDTO.getActual_amount() * 100 + "");
                    payDTO.setTitle(ownGoodsOrderDTO.getGoods_name() + "【核桃精选】");
                    payDTO.setOrder_id(ownGoodsOrderDTO.getOrder_id());
                    payDTO.setGoods_id(ownGoodsOrderDTO.getGoods_id() + "");
                    payService.wxpayH5(response, request, payDTO);
                }
                break;
                default: { //支付方式错误
//                return ResultUtil.result(BizExceptionEnum.PATTERN_OF_PAYMENT_EXCEPTION.getCode(), BizExceptionEnum.PATTERN_OF_PAYMENT_EXCEPTION.getMessage());
                }
            }

        } else {
            throw new BussinessException(BizExceptionEnum.PATTERN_OF_PAYMENT_EXCEPTION);
        }


    }


    @Override
    public Tip applyRefund(Integer user_id, String token, String order_id, String cause, String log_image) {
        //1 验证 用户是否存在
        User user = userMapper.selectUserId(user_id);
        if (!user.getToken().equals(token)) {
            throw new BussinessException(BizExceptionEnum.USER_TOKEN_ERROR);//用户登录异常
        }
        //2 验证订单谁是否存在、是否属于该用户
        SiftOrderDTO siftOrderDTO = siftOrderMapper.getUserSiftOrderDateil(user_id, order_id);
        if (null == siftOrderDTO) {
            throw new BussinessException(BizExceptionEnum.GOODS_ORDER_LOSE);//用户登录异常
        }
        //3 根据时间当前订单状态  更改状态退款中
        //3.0 检查当前订单 退款状态
        if (siftOrderDTO.getRefund_status() == 1 || siftOrderDTO.getRefund_status() == 2) {
            throw new BussinessException(BizExceptionEnum.SIFT_ORDER_ORDER_NO_REFUND);//当前订单 处于退款中或者已退款 不可再申请
        }
        //3.1 未付款 状态不可退款
        if (siftOrderDTO.getStatus() == 0) {
            throw new BussinessException(BizExceptionEnum.SIFT_ORDER_ORDER_NO_REFUND);//该订单不可退款
        }
        //3.2 完成之后七天内 无条件退款 超过时间 不可退款
        long time = siftOrderDTO.getUpdate_time().getTime() + 1000 * 7 * 24 * 60 * 60;//计算7天后时间毫秒
        if (siftOrderDTO.getStatus() == 1 && System.currentTimeMillis() >= time) {
            throw new BussinessException(BizExceptionEnum.SIFT_ORDER_ORDER_TIME_NO_REFUND);//该订单不可退款
        }

        //3.3 修改订单  退款状态
        SiftOrderDTO updata = new SiftOrderDTO();
        updata.setId(siftOrderDTO.getId());
        updata.setRefund_status(1);
        int req = siftOrderBaseMapper.updata(updata);
        if (req < 1) { //修改订单  退款状态   待 后台服务器 人工审核
            throw new BussinessException(BizExceptionEnum.UPDATE_ERROR);//修改失败
        }

        //3.4 返佣逻辑 （自购返佣、粉丝返佣）
        SiftGoodsDTO siftGoodsDTO = siftGoodsMapper.SiftGoodsDateil(siftOrderDTO.getProduct_id());
//        userMoneyService.updateSiftOrderBrokerage(user, siftOrderDTO, siftGoodsDTO);


        //3.5 添加记录
        SiftOrderLogDTO siftOrderLogDTO = new SiftOrderLogDTO();
        siftOrderLogDTO.setCause(cause);//退款原因
        siftOrderLogDTO.setLog_image(log_image);//图片集
        siftOrderLogDTO.setOrder_id(order_id);//订单号
        siftOrderLogDTO.setSequence(0);//排序号
        siftOrderLogDTO.setRefund_status(2);//退款状态 退款中
        siftOrderLogDTO.setGoods_id(siftOrderDTO.getProduct_id());//商品ID
        siftOrderLogDTO.setGoods_type(siftGoodsDTO.getGoods_type().toString());//商品ID
        siftOrderLogDTO.setParcel_number("");//退款快递单号
        siftOrderLogDTO.setParcel_type("");//退款快递公司
        Tip tip = iSiftOrderLogService.addLog(siftOrderLogDTO);
        if (tip.getCode() != 200) {
            throw new BussinessException(BizExceptionEnum.ADD_ERROR);//记录添加失败
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip cancelRefund(Integer user_id, String token, String order_id) {
        //1 验证 用户是否存在
        User user = userMapper.selectUserId(user_id);
        if (!user.getToken().equals(token)) {
            throw new BussinessException(BizExceptionEnum.USER_TOKEN_ERROR);//用户登录异常
        }
        //2 验证订单谁是否存在、是否属于该用户
        SiftOrderDTO siftOrderDTO = siftOrderMapper.getUserSiftOrderDateil(user_id, order_id);
        if (null == siftOrderDTO) {
            throw new BussinessException(BizExceptionEnum.GOODS_ORDER_LOSE);//用户登录异常
        }
        //3 更改订单退款状态
        SiftOrderDTO updata = new SiftOrderDTO();
        updata.setId(siftOrderDTO.getId());
        updata.setRefund_status(0);
        int req = siftOrderBaseMapper.updata(updata);
        if (req < 1) { //修改订单  退款状态   待 后台服务器 人工审核
            throw new BussinessException(BizExceptionEnum.UPDATE_ERROR);//修改失败
        }

        //4 添加取消订单记录
        SiftOrderLogDTO siftOrderLogDTO = new SiftOrderLogDTO();
        siftOrderLogDTO.setCause("");//退款原因
        siftOrderLogDTO.setLog_image("");//图片集
        siftOrderLogDTO.setOrder_id(order_id);//订单号
        siftOrderLogDTO.setSequence(0);//排序号
        siftOrderLogDTO.setRefund_status(3);//退款状态
        siftOrderLogDTO.setGoods_id(siftOrderDTO.getProduct_id());//商品ID
        SiftGoodsDTO siftGoodsDTO = siftGoodsMapper.SiftGoodsDateil(siftOrderDTO.getProduct_id());
        siftOrderLogDTO.setGoods_type(siftGoodsDTO.getGoods_type().toString());//商品ID
        siftOrderLogDTO.setParcel_number("");//退款快递单号
        siftOrderLogDTO.setParcel_type("");//退款快递公司
        Tip tip = iSiftOrderLogService.addLog(siftOrderLogDTO);
        if (tip.getCode() != 200) {
            throw new BussinessException(BizExceptionEnum.ADD_ERROR);//记录添加失败
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip refundMoney(Integer id, String order_id, Integer user_id, Integer refund_status) throws Exception {
        //1 验证 用户是否存在
        User user = userMapper.selectUserId(user_id);
        if (null == user) {
            throw new BussinessException(BizExceptionEnum.USER_TOKEN_ERROR);//用户登录异常
        }
        //2 验证订单谁是否存在、是否属于该用户
        SiftOrderDTO siftOrderDTO = siftOrderMapper.getUserSiftOrderDateil(user_id, order_id);
        if (null == siftOrderDTO) {
            throw new BussinessException(BizExceptionEnum.GOODS_ORDER_LOSE);//订单纯在异常
        }

        //3 校验订单  核桃精选订单只有申请中才可以进行退款、线下折扣可以直接退款
        if(siftOrderDTO.getDiscounts_type() == 0 && siftOrderDTO.getRefund_status() != 1 ){
            throw new BussinessException(BizExceptionEnum.THIS_ORDER_TRUE_REFUND);//只有退款申请中
        }

        //4 根据当前 处理方式  退款/驳回 执行 退款方法 :2 退款成功 3 退款失败',
        Boolean ifRefund = false;
        if (refund_status == 2) {// 审核 退款 走退款流程
            //4.1 根据不通支付类型走不通 退款
            switch (siftOrderDTO.getPaytype()) {
                case 1: {//支付宝
                    ifRefund = payService.zfbRefundMoney(siftOrderDTO.getOrder_id(), siftOrderDTO.getTrade_no(), siftOrderDTO.getActual_amount());
                }
                break;
                case 2: {//微信
                    ifRefund = payService.wxRefundMoney(siftOrderDTO.getOrder_id(), siftOrderDTO.getTrade_no(), siftOrderDTO.getActual_amount());
                }
                break;
                default: {
                    throw new BussinessException(BizExceptionEnum.PATTERN_OF_PAYMENT_EXCEPTION);//用户登录异常
                }
            }
            //4.2 人工审核修改订单 同时退款接口退款成功 订单退款状态
            if (!ifRefund) {
                throw new BussinessException(BizExceptionEnum.REFUND_ERROR);//退款失败
            }
        }

        //5 退款逻辑
        SiftOrderDTO userOrder = siftOrderBaseMapper.queryAll(siftOrderDTO.getId().intValue());
        SiftGoodsDTO siftGoodsDTO = siftGoodsMapper.SiftGoodsDateil(userOrder.getProduct_id());
        if (refund_status == 2) {
            userOrder.setRefund_status(2);
            userMoneyService.updateSiftOrderBrokerage(user, userOrder, siftGoodsDTO);
        }


        //6 修改订单 退款状态
        int req = siftOrderBaseMapper.refundStatusUpdate(id, refund_status);
        if (req < 1) {
            throw new BussinessException(BizExceptionEnum.GOODS_ORDER_LOSE);//用户登录异常
        }

        //7 查询申请记录 生成退款 追加记录
        SiftOrderLogDTO siftOrderLogDTO = new SiftOrderLogDTO();
        List<Map> maps = siftOrderLogMapper.getLog(order_id);
        String cause = "";
        String Log_image = "";
        String Parcel_number = "";
        String Parcel_type = "";
        if (null != maps && maps.size() > 0) {
            Map map = maps.get(0);
            if (map.containsKey("cause")) {
                cause = (String) map.get("cause");
            }
            if (map.containsKey("log_image")) {
                Log_image = (String) map.get("log_image");
            }
            if (map.containsKey("parcel_number")) {
                Parcel_number = (String) map.get("parcel_number");
            }
            if (map.containsKey("parcel_type")) {
                Parcel_type = (String) map.get("parcel_type");
            }
        }
        siftOrderLogDTO.setCause(cause);//退款原因
        siftOrderLogDTO.setLog_image(Log_image);//图片集
        siftOrderLogDTO.setOrder_id(order_id);//订单号
        siftOrderLogDTO.setSequence(0);//排序号
        if (refund_status == 3) { //退款驳回
            siftOrderLogDTO.setRefund_status(0);//退款状态
        } else if (refund_status == 2) {//同意退款
            siftOrderLogDTO.setRefund_status(1);//退款状态
        }
        siftOrderLogDTO.setParcel_number(Parcel_number);//退款快递单号
        siftOrderLogDTO.setParcel_type(Parcel_type);//退款快递公司
        siftOrderLogDTO.setGoods_id(siftOrderDTO.getProduct_id());//商品ID
        siftOrderLogDTO.setGoods_type(siftGoodsDTO.getGoods_type().toString());//商品ID
        Tip tip = iSiftOrderLogService.addLog(siftOrderLogDTO);
        if (tip.getCode() != 200) {
            throw new BussinessException(BizExceptionEnum.ADD_ERROR);//记录添加失败
        }
        if (refund_status == 2) {
            //8 修改订单状态为失效并且 更改粉丝订单状态
            if (siftOrderBaseMapper.updataNoUse(order_id) < 1) {
                throw new BussinessException(BizExceptionEnum.UPDATE_ERROR);//修改失败
            }
//        //9修改粉丝订单状态
            if(siftOrderDTO.getDiscounts_type() == 0){ //核桃精选订单才有返佣  既而才会有粉丝订单
                if (siftOrderBaseMapper.updataFansRakeNoUse(order_id) < 1) {
                    throw new BussinessException(BizExceptionEnum.UPDATE_ERROR);//修改失败
                }
            }

        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }


    @Override
    public Tip getSiftOrderDetail(String order_id) throws Exception {
        //查询订单详情
        SiftOrderDTO siftOrderDTO = siftOrderMapper.siftOrderDetail(order_id);
        //规格查询
        List<Long> ids = new ArrayList<>();
        if (null != siftOrderDTO && null != siftOrderDTO.getGoods_spe_id() && siftOrderDTO.getGoods_spe_id().length() > 0) {
            String[] id_s = siftOrderDTO.getGoods_spe_id().split(",");
            for (String s : id_s) {
                ids.add(Long.parseLong(s));
            }
        }
        String spec = "";
        for (SiftGoodsSpecificationDTO siftGoodsSpecificationDTO : siftGoodsSpecificationMapper.getSiftGoodsSpecificationS(ids)) {
            if (spec.length() > 0) {
                spec = spec + ",";
            }
            spec = spec + siftGoodsSpecificationDTO.getEtalon_value();
        }
        siftOrderDTO.setGoods_spe_idText(spec);
        //查询核桃精选商品-单位
        SiftGoodsDTO siftGoodsDTO = siftGoodsMapper.SiftGoodsDateil(siftOrderDTO.getProduct_id());
        if (null != siftGoodsDTO) {
            siftOrderDTO.setGoods_units(siftGoodsDTO.getGoods_units());
        }
        if (null != siftOrderDTO) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), siftOrderDTO);
        }
        return ResultUtil.result(BizExceptionEnum.SELECT_ERROR.getCode(), BizExceptionEnum.SELECT_ERROR.getMessage());
    }


    @Override
    public Tip afterSale(Integer user_id, String token, Integer refund_status) {
        //1 验证 用户是否存在
        User user = userMapper.selectUserId(user_id);
        if (!user.getToken().equals(token)) {
            throw new BussinessException(BizExceptionEnum.USER_TOKEN_ERROR);//用户登录异常
        }

        //2 查询订单信息
        List<SiftOrderDTO> list = siftOrderMapper.getUserSiftOrderList(user_id, refund_status);
        // 查询 申请中  带出 parcel_number，parcel_type 信息
        if (refund_status == 1) {
            for (SiftOrderDTO dto : list) {
                List<Map> maps = siftOrderLogMapper.getLog(dto.getOrder_id());
                if (null != maps && maps.size() > 0) {
                    Map map = maps.get(0);
                    if (null != map) {
                        String parcel_number = "", parcel_type = "";
                        Integer id = -1;
                        if (map.containsKey("parcel_number")) {
                            parcel_number = (String) map.get("parcel_number");
                        }
                        if (map.containsKey("parcel_type")) {
                            parcel_type = (String) map.get("parcel_type");
                        }
                        if (map.containsKey("id")) {
                            id = (Integer) map.get("id");
                        }
                        dto.setLog_id(id.longValue());
                        dto.setParcel_type(parcel_type);
                        dto.setParcel_number(parcel_number);
                    }
                }

            }
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);
    }

    @Override
    public Tip discountClassifyQualification(Integer user_id, String goods_id) throws Exception {
        SiftGoodsDTO siftGoodsDTO = siftGoodsBaseMapper.queryAll(goods_id);
        User user = userMapper.selectById(user_id);
        /** 1.2.1 如果是 下线折扣 则只有活动当天 新用户好才可购买 切只能购买一次*/
        int data = -1;// -1 没有购买资格    0 可以购买  1  已经购买过一次
        if(siftGoodsDTO.getDiscounts_type() == 1 ){//线下折扣
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String creatime =  siftOrderBaseMapper.discountClassifyCreateTime();
            List<SiftOrderDTO> reqSiftOrder = siftOrderMapper.siftOrderDetailGoodsid(goods_id,user_id);
            //获取活动拼凑时间
            if(sdf.parse(user.getCreate_time()).getTime() > sdf.parse(creatime).getTime()){ //新用户
                if(reqSiftOrder == null || reqSiftOrder.size()==0){
                    data = 0;
                }else{
                    data = 1;
                }
            }
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), data);
    }
}
