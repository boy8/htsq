package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;

public interface ISiftGoodsSpecificationService {

    /**
     * 查询精选核桃商品 规格
     * **/
    Tip getSiftGoodsSpecification(String goods_id);
}
