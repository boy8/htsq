package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.OwnGoodsOrderDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpMessage;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface HtmlOrderService {


    //订单列表
    Tip goodsListNoPage();

    //订单列表
    Tip goodsCouponsListNoPage(Integer index,int coupons_price);

    //订单详情页
    Tip goodsDetails(int dealer_id);

    //规格
    Tip goodsSpecificationDetails(int goods_id);

    //点击购买
    void addOrderGift(OwnGoodsOrderDTO ownGoodsOrderDTO, HttpServletResponse response, HttpServletRequest request) throws Exception;


    //购买成功/失败 回调
    //支付宝-回调
    void alipayPayCallback(HttpServletResponse response,HttpServletRequest request) throws Exception;
    //微信-回调
    void wxPayCallback(HttpServletResponse response,HttpServletRequest request) throws Exception;



    //订单详情
    Tip orderGiftDetail(String order_id);

}
