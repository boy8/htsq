package com.drd.dt.modular.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.drd.dt.back.dao.ISiftGoodsBaseMapper;
import com.drd.dt.back.dao.ISiftOrderBaseMapper;
import com.drd.dt.back.dto.SiftGoodsDTO;
import com.drd.dt.back.dto.SiftOrderDTO;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.modular.dao.*;
import com.drd.dt.modular.dto.PayDTO;
import com.drd.dt.modular.entity.Order;
import com.drd.dt.modular.entity.OrderGift;
import com.drd.dt.modular.entity.User;
import com.drd.dt.modular.service.OrderGiftService;
import com.drd.dt.modular.service.PayService;
import com.drd.dt.util.*;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.*;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


/**
 * Created by 86514 on 2019/3/25.
 */
@Service
@Transactional
public class PayServiceImpl implements PayService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OrderGiftService orderGiftService;
    @Autowired
    private UserMoneyServiceImpl userMoneyService;
    @Autowired
    private ISiftOrderBaseMapper iSiftOrderBaseMapper;
    @Autowired
    private ISiftGoodsBaseMapper iSiftGoodsBaseMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SiftOrderMapper siftOrderMapper;
    @Autowired
    private UserMoneyServiceImpl testMoneyService;
    @Autowired
    private UserMoneyMapper userMoneyMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private SiftGoodsMapper siftGoodsMapper;

    /*****************************测试**********************************/
    /**
     * 58.218.201.7 解析：ce.hetaosq.com:18003       H5地址
     * 58.218.201.7 解析：ce.hetaosq.com:18002          后台地址
     */
//    String return_url = "http://ce.hetaosq.com:18003/#/status?order_id="; //H5地址
//    final String alipay_notify_url = "http://ce.hetaosq.com:18002/api/v2/htsq/pay/alipayCallback"; //支付宝回调
//    final String wx_notify_url = "http://ce.hetaosq.com:18002/api/v2/htsq/pay/WXPayCallback";//接收微信支付异步通知回调地址
    /*****************************测试**********************************/


    String return_url = "http://p11.quanbashi.vip/#/status?order_id=";

    //支付宝支付
    final String alipay_notify_url = "http://www.quanbashi.vip:18002/api/v2/htsq/pay/alipayCallback";
    String gateway = "https://openapi.alipay.com/gateway.do";
    String app_id = "2019111269098691";
    String private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCTIChyISaWvtXc9dVKFcxaLQ6oNm06yE3FjdfjsaieIpkc2/FTR7WQb4TluVcQ5bgOXeR9RdvON8ti+pqx4cSQCnLnOeG2JBrX3gsra7IVmrB4QrmhrVXzzpZr184MI81HfK3/KTp+RbJNvLOYzSRN5z6t4b7GfIafDC60J6qpen+Zq+HeG8KBy3yx9N9NIrs/cPjp7Ar3jWgTk+V4uBBwY8PMYlTEa7m88Q+O418zDwMtOJE2W8B3e6xAocnLSE/j76qFyP16H2V/i2oOf2TfwevdPxH99lMfj7iDECF/N6ayOLJlzu7+2hW64JUXIJPyFL7QXXRkWyLzc9nQT3IDAgMBAAECggEAcMM2/LJ31xYQ6Dfq78thQsRB9Z8xfNpE8WNT0oo6CGnQHJRelEvj1v4CR+gT2TmYJjrEg1dlrtqIoiYdJrU5aIT5qEtuuaFWiZj/ypnSdkiHdMT/bfFibWkrVSCkJh3SmjXvTiAVWu6kSHyW0kh4yNSx76eUBqEutPwrV50Harj0KzuCaZ9Iw6ssnomuCWyRt8Vpcy6WI/Hij4P3HRMoRHr6BRVtYA86Kcy13ZjpW/eOkfo8PBtcNSyUz7D0fBCE/OzViX3YGM/xJE4bLOTbr5LSleylW+gso1zm9cYDh2oSORPeGwjoROfUmRP2cb30U4yYQ1yyGjuzyLMXI6ChAQKBgQDT5J8d9DufqElEC/IqUWcFxYIphi8+vJOi8iWcEJ4D2N86qsH4jdsEfp6TeIU+zxg4WgpgO3dfa3IjA5kVz7LJC6KKMMbS877IoF0EaQz8iAMI7GroJjOA/xeHnOyBQgocXIqwEY4vIZIx9PTCaF1US57xPvfuPJ3KB9k6hU0jwwKBgQCxwDPonPrxKd8ZdYKeDauGz3zyY8SUHVV+yn23RdyZKB79RXfcUqJPcyYN/XEKQbsb32sS3iuD5QD5VKcSkdNP4FWYK7rBWVquPVa+eWHE/FoDv6FqZJoD10ulPV3p6FKAn/9LCsvT7OVqp2kMPAbpAEmQ26YEwlbtR23lS67UwQKBgQCxDwoy8oHcELhgJwdNe0X2GauBXUIfT16iu2rX9T7FAjTOhh+dvZO1NsT1f7D/4fdgJ0UiUq6WSmdOgNQr2KmRu6ovhUCux8S1SuNPKyUR4+l9TGis4jcJZJ9Zz7QE8pUWS/IL3C+OjKPoIMNhq+nI5YPEHRT5uycyycooT3k9FwKBgBhdJwBQXBkoSxs7Py8Y4pT05cR3pl9C8hASA09WtRkNJcpDdl1tui+3sdSjE5Z7UgFNC2knqMFIZ2zFjKz/7f352uGjxNJxw/s0DyrGin8ss83lu2NQ7MdwAD9i/Pdtz7kvtRd9IFkpFH0c+0ohBDV1w5+ma8glNzMI4mhWil1BAoGAYdNIiFuWsXiRbpBQ14afLGCaURLgWJdmmmLMDU5QgbSMSUZD1znK0OBlMQyUMB9Nww5TYoLO1qtA0rxhXOCtI+p1tGet6Q9QBPc42fm1zjmorPoikbw/P+hhDd2zcwaQ8a42/SDNBp7akcEzWzSN0u/S+L5/TE2hAeYKh0mEp+A=";
    String charset = "UTF-8";
    String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqOkABsWIfwYPoWVEGUEsv1iyJwHjqDfIbzhO6n90Fqkvhkw3zIVhkcnYr7iO0fH9aTmz9+OFkIQ3Q694KRDQQHADysB5LfFlYNgJbSrULYbioRPPaT3FdY8wxmaSa31GChOdCiNph9OQGQHoY6Pc6lIZayG4Lni2hV5b8smCMVsvT5hX1klPLeTa4tWpL4oVcKVw83san82cPUL0ONbK8KhPlQ/SpYbGqPMLuZKt/5VmkHfi6u1b2YIH49Dcv1WkjqSKlgGCj2dXc2lXzbMUO5PGVnq/zuOcYf3A38YemzyqPBjnbgBqeeXRVNQ9k8wDkP1GJKPP3S6wq1koweLR+wIDAQAB";
    String sign_type = "RSA2";
    String encryptKey = "RGjvbHcphXkFd9SGlBUvRA==";


    //微信支付
    final String refund = "https://api.mch.weixin.qq.com/secapi/pay/refund";//退款接口地址
    final String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";//支付接口链接
    final String mch_id = "1560976561";//商户号
    final String spbill_create_ip = "221.229.162.103";//终端IP
    final String appid = "wx2dc4fc556b56aa12";//H5应用APPID
    final String key = "edd1f792e84b12a9326204b9b6d0bb73";//商户平台密钥key
    final String wx_notify_url = "http://www.quanbashi.vip:18002/api/v2/htsq/pay/WXPayCallback";//接收微信支付异步通知回调地址

    final String ssl_name = "D:\\wx\\apiclient_cert.p12";//ssl 文件


    /**
     * 支付宝支付
     **/
    @Override
    public Tip alipay(PayDTO payDTO) throws Exception {
        String total_amount = payDTO.getMoney();//订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
        String subject = payDTO.getTitle();//商品的标题/交易标题/订单标题/订单关键字等。
        String out_trade_no = payDTO.getOrder_id();//商户网站唯一订单号
        if (StringUtils.isEmpty(total_amount) || StringUtils.isEmpty(subject) || StringUtils.isEmpty(out_trade_no)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        AlipayClient alipayClient = new DefaultAlipayClient(gateway, app_id, private_key, "json", charset, alipay_public_key, sign_type);
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        request.setNotifyUrl(alipay_notify_url);//回调地址
        model.setSubject(subject);
        model.setOutTradeNo(out_trade_no);
        model.setTimeoutExpress("30m");
        model.setTotalAmount(total_amount);
        request.setBizModel(model);
        AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
        String body = response.getBody();
        if (response.isSuccess()) {
            logger.info("【支付宝付款调用成功...】");
        } else {
            logger.info("【支付宝付款调用失败...】");
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), body);
    }


    /**
     * 支付宝支付
     **/
    @Override
    public void alipayH5(HttpServletResponse response, HttpServletRequest request, PayDTO payDTO) throws Exception {
        String total_amount = payDTO.getMoney();//订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
        String subject = payDTO.getTitle();//商品的标题/交易标题/订单标题/订单关键字等。
        String out_trade_no = payDTO.getOrder_id();//商户网站唯一订单号
        String goods_id = payDTO.getGoods_id();//商品Id
        if (StringUtils.isEmpty(total_amount) || StringUtils.isEmpty(subject) || StringUtils.isEmpty(out_trade_no)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        AlipayClient alipayClient = new DefaultAlipayClient(gateway, app_id, private_key, "json", charset, alipay_public_key, sign_type);
        AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();
        AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
        alipayRequest.setNotifyUrl(alipay_notify_url);//回调地址
        alipayRequest.setReturnUrl(return_url + out_trade_no + "&order_type=" + payDTO.getOrderType());
        model.setSubject(subject);
        model.setOutTradeNo(out_trade_no);
        model.setPassbackParams(goods_id);
        model.setTimeoutExpress("30m");
        model.setTotalAmount(total_amount);
        model.setPassbackParams(JSONObject.toJSONString(payDTO)); //回传参数
        alipayRequest.setBizModel(model);
        String form = "";
        try {
            // 调用SDK生成表单
            form = alipayClient.pageExecute(alipayRequest).getBody();
        } catch (AlipayApiException e) {
            form = "err";
            e.printStackTrace();
        }
        response.setContentType("text/html;charset=UTF-8");
        //直接将完整的表单html输出到页面
        response.getWriter().write(form);
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * 微信支付
     **/
    @Override
    public Tip wxpay(PayDTO payDTO) throws Exception {
        String goods_desc = payDTO.getTitle();
        String money = payDTO.getMoney();
        Map param = new HashMap();
        param.put("appid", appid);
        param.put("mch_id", mch_id);
        String uuid = UUID.randomUUID().toString().replace("-", "");//随机32位字符串
        String order_id = Constant.yyyymmddhhmmss.format(new Date());
        param.put("nonce_str", uuid);
        param.put("body", goods_desc);//商品描述
        param.put("out_trade_no", order_id);//商户订单号
        param.put("total_fee", money);
        param.put("spbill_create_ip", spbill_create_ip);//调用微信支付API的机器IP
        param.put("notify_url", wx_notify_url);//接收微信支付异步通知回调地址
        param.put("trade_type", "APP");

        String sign = WXPaySign(param, key);
        param.put("sign", sign);
        String xml = XMLUtil.toXml(param);
        String result = HttpConnectionPoolUtil.httpXML(url, xml);
        JSONObject resultData = new JSONObject();
        if (!StringUtils.isEmpty(result)) {
            logger.info("调取微信支付接口成功");
            Map<String, Object> data = XMLUtil.toMap(result);
            String return_code = data.get("return_code").toString();
            if (return_code.equals("SUCCESS")) {
                resultData.put("appid", data.get("appid"));
                resultData.put("partnerid", data.get("mch_id"));//商户号
                resultData.put("noncestr", data.get("nonce_str"));
                resultData.put("prepayid", data.get("prepay_id"));
                resultData.put("package", "Sign=WXPay");
                String timeStamp = String.valueOf(System.currentTimeMillis()).substring(0, 10);
                resultData.put("timestamp", timeStamp);
                String returnSign = WXPaySign(resultData, key);
                resultData.put("sign", returnSign);
                logger.info("返回数据【" + resultData + "】");
            } else {
                throw new BussinessException(BizExceptionEnum.USER_PAY_FAIL);
            }
        } else {
            throw new BussinessException(BizExceptionEnum.USER_PAY_FAIL);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), resultData);
    }


    /**
     * 微信支付H5
     **/
    @Override
    public void wxpayH5(HttpServletResponse response, HttpServletRequest request, PayDTO payDTO) throws Exception {
        Double money = 0.0;
        String goods_id = payDTO.getGoods_id();//商品Id
        String goods_desc = payDTO.getTitle();
        String out_trade_no = payDTO.getOrder_id();

        if (null != payDTO.getMoney() && !payDTO.getMoney().equals("")) {
            money = Double.parseDouble(payDTO.getMoney());
        }
        Map param = new HashMap();
        JSONObject obj = new JSONObject();
        obj.put("goods_id", goods_id);
        obj.put("order_id", out_trade_no);
        obj.put("order_type", payDTO.getOrderType());
        obj.put("buy_type", payDTO.getOrderType());
        param.put("attach", JSONObject.toJSONString(obj));
        param.put("appid", appid);
        param.put("mch_id", mch_id);
        String uuid = UUID.randomUUID().toString().replace("-", "");//随机32位字符串
        param.put("nonce_str", uuid);
        param.put("body", goods_desc);//商品描述
        param.put("out_trade_no", out_trade_no);//商户订单号
        param.put("total_fee", money.intValue());
        param.put("spbill_create_ip", StringUtil.getIpAddr(request));//调用客户端IP
        param.put("notify_url", wx_notify_url);//接收微信支付异步通知回调地址
        param.put("trade_type", "MWEB");

        String sign = WXPaySign(param, key);
        param.put("sign", sign);

        String xml = XMLUtil.toXml(param);

        String result = HttpConnectionPoolUtil.httpXML(url, xml);
        logger.info("微信返回:" + result + "】");
        String mweb_url = "";
        if (!StringUtils.isEmpty(result)) {
            logger.info("H5调取微信支付接口成功");
            Map<String, Object> data = XMLUtil.toMap(result);
            String return_code = data.get("return_code").toString();
            if (return_code.equals("SUCCESS")) {
                mweb_url = (String) data.get("mweb_url") + "&redirect_url=" + URLEncoder.encode(return_url + out_trade_no + "&order_type=" + payDTO.getOrderType());
                logger.info("返回数据【" + mweb_url + "】");
            } else {
                logger.info("返回数据【" + result + "】");
                throw new BussinessException(BizExceptionEnum.USER_PAY_FAIL);
            }
        } else {
            throw new BussinessException(BizExceptionEnum.USER_PAY_FAIL);
        }
        response.setContentType("text/json;charset=UTF-8");
        //直接将完整的表单html输出到页面
        Tip tip = ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), mweb_url);
        response.getWriter().write(JSONObject.toJSONString(tip));
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * 接收微信支付异步通知回调地址
     **/
    @Override
    public void wxPayCallback(HttpServletResponse response, HttpServletRequest request) throws Exception {
        logger.info("【开始接收微信支付异步通知回调结果...】");
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        InputStream inStream = request.getInputStream();
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();

        String result = new String(outSteam.toByteArray(), "utf-8");
        Map<String, Object> map = XMLUtil.toMap(result);
        logger.info("WxGetPayResult返回结果:" + map);
        Map<String, Object> params = new HashMap<>();

        String return_code = "";
        String return_msg = "";
        if (map.containsKey("return_code")) {
            return_code = map.get("return_code").toString();
        }
        if (map.containsKey("return_msg")) {
            return_msg = map.get("return_msg").toString();
        }
        params.put("return_code", return_code);
        params.put("return_msg", return_msg);
        String xml = XMLUtil.toXml(params);
        out.print(xml);
        out.flush();
        out.close();

        if (!return_code.equals("SUCCESS")) {
            throw new BussinessException(BizExceptionEnum.USER_PAY_FAIL);
        }
        //后台订单业务逻辑
        JSONObject attach = null;
        if (map.containsKey("attach")) {
            attach = JSONObject.parseObject((String) map.get("attach"));
//            logger.info("订单同步开始:参数：attach-" + attach.toString());
        } else {
            throw new BussinessException(BizExceptionEnum.GOODS_ORDER_LOSE);
        }
        String order_id = "";
        String buyer_id = "";
        String buyer_logon_id = "";
        String trade_no = "";
        String goods_id = "";
        Integer order_type = -1;
        Integer buy_type = -1;//0自购 ，1来自分享'',',
        if (attach.containsKey("goods_id")) {
            goods_id = attach.getString("goods_id");
        }
        if (attach.containsKey("order_id")) {
            order_id = attach.getString("order_id");
        }
        if (map.containsKey("transaction_id")) {
            trade_no = (String) map.get("transaction_id");//微信交易订单号
        }
        if (attach.containsKey("order_type")) { //订单类型
            order_type = attach.getInteger("order_type");
        }
        if (attach.containsKey("buy_type")) { //订单购买方式
            buy_type = attach.getInteger("buy_type");
        }
        //修改订单状态
        ordersSynchronization(response, order_id, buyer_id, buyer_logon_id, trade_no, goods_id, "微信", order_type, buy_type);
    }


    @Override
    public void alipayPayCallback(HttpServletResponse response, HttpServletRequest request) throws Exception {
        logger.info("【开始接收支付宝支付异步通知回调结果...】");
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        Map<String, String> params = new HashMap<String, String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
            //  System.out.println(name+"="+valueStr);
        }

        if (!AlipaySignature.rsaCheckV1(params, alipay_public_key, charset, sign_type)) {//验签是否是支付宝的回调
            response.getWriter().println("faild");
            return;
            // return ResultUtil.result(BizExceptionEnum.GOODS_ORDER_ID_LOSE.getCode(), BizExceptionEnum.GOODS_ORDER_ID_LOSE.getMessage());
        }
        String order_id = request.getParameter("out_trade_no");
        String trade_no = request.getParameter("trade_no");//支付客宝交易号
        String parms = request.getParameter("passback_params");
        String goods_id = null;
        Integer order_type = -1;//订单类型
        Integer buy_type = -1;//0自购 ，1来自分享'',',
        if (parms != null) {
            JSONObject obj = JSONObject.parseObject(parms);
            logger.info("支付宝参数集合：obj：" + parms);
            goods_id = obj.getString("goods_id");
            order_type = obj.getInteger("orderType");
            buy_type = obj.getInteger("buyType");

        }
        String buyer_id = request.getParameter("buyer_id");
        String buyer_logon_id = request.getParameter("buyer_logon_id");
        String trade_status = request.getParameter("trade_status");
        if (!trade_status.equals("TRADE_SUCCESS")) {//交易成功
            logger.info("支付失败");
            response.getWriter().println("faild");
            return;
        }
        //订单修改
        ordersSynchronization(response, order_id, buyer_id, buyer_logon_id, trade_no, goods_id, "支付宝", order_type, buy_type);
    }

    /**
     * 订单修改接口
     *
     * @param order_id       订单号
     * @param buyer_id       买家ID
     * @param buyer_logon_id 买家账号
     * @param trade_no       支付宝交易号
     * @param goods_id       商品ID
     * @param type           付款方式
     * @param goods_type     订单类型：0-自营礼包订单   、1 核桃精选订单
     * @param buy_type       订单购买类型：0-自购   、1 分享
     */
    private void ordersSynchronization(HttpServletResponse response,
                                       String order_id,
                                       String buyer_id,
                                       String buyer_logon_id,
                                       String trade_no,
                                       String goods_id,
                                       String type,
                                       int goods_type, int buy_type) throws Exception {

        if (null == order_id && order_id.length() < 1) { //丢失 订单号
            response.getWriter().println("faild");
            return;
        }
        if (null == goods_id && goods_id.length() < 1) { //丢失 产品ID
            response.getWriter().println("faild");
            return;
        }

        if (goods_type == 0) {//自营礼包
            //验证 金额 订单号
            int ifOreder = orderGiftService.finOrDerGift(order_id);
            if (ifOreder < 1) {
                response.getWriter().println("faild");
                return;
            }

            OrderGift ordertest = orderGiftService.getOrderGiftDetail(order_id);
            if (type.equals("支付宝")) {
                if (TextUtils.isEmpty(ordertest.getBuyer_id())) {
                    //同步更新 数据库订单状态
                    Map<String, Object> m = new HashMap<>();
                    m.put("id", order_id);
                    m.put("status", 2);
                    m.put("buyer_id", buyer_id);
                    m.put("trade_no", trade_no);
                    m.put("buyer_logon_id", buyer_logon_id);
                    int req = orderGiftService.upOrderGift(m);
//                logger.info("更新订单：" + req);
                    if (req > 0) {
                        //更新库存 -, 售卖 +
                        orderGiftService.upInventorySell(goods_id);
                        //根据购买礼包用户，订单信息计算推荐佣金
                        OrderGift order = orderGiftService.getOrderGiftDetail(order_id);
                        String userid = order.getUser_id();
                        userMoneyService.updateOrderGiftRake(Integer.parseInt(userid), order);
                        response.getWriter().println("success");
                    }
                } else {
                    response.getWriter().println("success");
                }
                response.getWriter().println("faild");
            } else if (type.equals("微信")) {
                if (null != ordertest) {
                    //同步更新 数据库订单状态
                    if (TextUtils.isEmpty(ordertest.getTrade_no())) {//第一次支付回调更新
                        Map<String, Object> m = new HashMap<>();
                        m.put("id", order_id);
                        m.put("status", 2);
                        m.put("buyer_id", buyer_id);
                        m.put("trade_no", trade_no);
                        m.put("buyer_logon_id", buyer_logon_id);
                        int req = orderGiftService.upOrderGift(m);
//                    logger.info("更新订单：" + req);
                        if (req > 0) {
                            //更新库存 -, 售卖 +
                            orderGiftService.upInventorySell(goods_id);
                            //根据购买礼包用户，订单信息计算推荐佣金
                            OrderGift order = orderGiftService.getOrderGiftDetail(order_id);
                            String userid = order.getUser_id();
                            userMoneyService.updateOrderGiftRake(Integer.parseInt(userid), order);
                        }
                    }
                }
            }
        } else if (goods_type == 1) {//核桃精选礼包
            //验证 金额 订单号
            SiftOrderDTO ordertest = iSiftOrderBaseMapper.queryAllOrderId(order_id);
            if (null == ordertest) {
                response.getWriter().println("faild");
                return;
            }
            //获取到订单信息  以及用户信息、商品信息
            Map reqmap = new HashMap();
            SiftOrderDTO reqSiftOrder = siftOrderMapper.siftOrderDetail(order_id);
            reqmap.put("id", reqSiftOrder.getUser_id());
            User user = userMoneyMapper.getByUserId(reqmap);
            SiftGoodsDTO siftGoodsDTO = siftGoodsMapper.SiftGoodsDateil(reqSiftOrder.getProduct_id());

            if (type.equals("支付宝")) {
                if (TextUtils.isEmpty(ordertest.getBuyer_id())) {
                    //同步更新 数据库订单状态
                    SiftOrderDTO updata = new SiftOrderDTO();
                    updata.setOrder_id(order_id);
                    updata.setStatus(2);
                    updata.setBuyer_id(buyer_id);
                    updata.setTrade_no(trade_no);
                    updata.setBuyer_logon_id(buyer_logon_id);
                    int req = iSiftOrderBaseMapper.updataStatus(updata);
//                logger.info("更新订单：" + req);
                    if (req > 0) {
                        //更新库存 -, 售卖 +
                        SiftGoodsDTO upgoods = new SiftGoodsDTO();
                        upgoods.setId(goods_id);
                        upgoods.setSell(ordertest.getNumber());
                        iSiftGoodsBaseMapper.updataGoodsSell(upgoods);
                        //根据购买用户 是否是会员并计算佣金
                        reqSiftOrder.setStatus(2);
                        testMoneyService.updateSiftOrderBrokerage(user, reqSiftOrder, siftGoodsDTO);
                        response.getWriter().println("success");
                    }
                } else {
                    response.getWriter().println("success");
                }
                response.getWriter().println("faild");
            } else if (type.equals("微信")) {
                if (null != ordertest) {
                    //同步更新 数据库订单状态
                    if (TextUtils.isEmpty(ordertest.getTrade_no())) {//第一次支付回调更新
                        SiftOrderDTO updata = new SiftOrderDTO();
                        updata.setOrder_id(order_id);
                        updata.setStatus(2);
                        updata.setBuyer_id(buyer_id);
                        updata.setTrade_no(trade_no);
                        updata.setBuyer_logon_id(buyer_logon_id);
                        int req = iSiftOrderBaseMapper.updataStatus(updata);
//                    logger.info("更新订单：" + req);
                        if (req > 0) {
                            SiftGoodsDTO upgoods = new SiftGoodsDTO();
                            upgoods.setId(goods_id);
                            upgoods.setSell(ordertest.getNumber());
                            iSiftGoodsBaseMapper.updataGoodsSell(upgoods);
                            //根据购买用户 是否是会员并计算佣金
                            reqSiftOrder.setStatus(2);
                            testMoneyService.updateSiftOrderBrokerage(user, reqSiftOrder, siftGoodsDTO);
                        }
                    }
                }
            }
        }
    }

    /**
     * 自营三方平台商品 ：用户付款之后一系列逻辑操作
     * **/


    /**
     * 微信支付接口签名
     */
    public String WXPaySign(Map map, String key) throws Exception {
        List<Map.Entry<String, String>> sign = new ArrayList<>(map.entrySet());
        Collections.sort(sign, (map1, map2) -> map1.getKey().compareTo(map2.getKey()));
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < sign.size(); i++) {
            builder.append(sign.get(i) + "&");
        }
        builder.append("key=" + key);
        String str = builder.toString();
        String result = MD5Utils.MD5Encode(str, "utf8").toUpperCase();
        return result;
    }


    @Override
    public Tip findOrder(String order_id) {
        OrderGift order = orderGiftService.getOrderGiftDetail(order_id);
        if (order != null && !TextUtils.isEmpty(order.getUser_id())) {
            String userid = order.getUser_id();
            userMoneyService.updateOrderGiftRake(Integer.parseInt(userid), order);
        }

        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    @Override
    public Tip refundMoney(Integer user_id, String token, String order_id) throws Exception {
        //1 验证 用户是否存在
        User user = userMapper.selectUserId(user_id);
        if (!user.getToken().equals(token)) {
            throw new BussinessException(BizExceptionEnum.USER_TOKEN_ERROR);//用户登录异常
        }
        //2 验证订单谁是否存在、是否属于该用户
        SiftOrderDTO siftOrderDTO = siftOrderMapper.getUserSiftOrderDateil(user_id, order_id);
        if (null == siftOrderDTO) {
            throw new BussinessException(BizExceptionEnum.GOODS_ORDER_LOSE);//用户登录异常
        }
        //3 根据不通支付类型走不通 退款
        Boolean ifRefund = false;
        switch (siftOrderDTO.getPaytype()) {
            case 1: {//支付宝
                ifRefund = zfbRefundMoney(siftOrderDTO.getOrder_id(), siftOrderDTO.getTrade_no(), siftOrderDTO.getActual_amount());
            }
            break;
            case 2: {//微信
                ifRefund = wxRefundMoney(siftOrderDTO.getOrder_id(), siftOrderDTO.getTrade_no(), siftOrderDTO.getActual_amount());
            }
            break;
            default: {
                throw new BussinessException(BizExceptionEnum.PATTERN_OF_PAYMENT_EXCEPTION);//用户登录异常
            }
        }
        if (ifRefund) { //退款成功
            //4.1 更新订单状态
            //
        } else {//退款失败
            //4.1 消息通知 unfinished
            throw new BussinessException(BizExceptionEnum.REFUND_ERROR);//用户登录异常
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 支付宝退款
     **/
    public boolean zfbRefundMoney(String order_id, String trade_no, Double actual_amount) throws Exception {
        AlipayClient alipayClient = new DefaultAlipayClient(gateway, app_id, private_key, "json", charset, alipay_public_key, sign_type);
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", order_id);//订单id
        bizContent.put("trade_no", trade_no);//支付宝号
        bizContent.put("refund_amount", actual_amount);//退款金额 根据实际订单付款金额设定
        bizContent.put("trade_no", trade_no);//支付宝号
        request.setBizContent(bizContent.toJSONString());
        AlipayTradeRefundResponse response = alipayClient.execute(request);
        logger.info("支付宝退款：" + JSONObject.toJSONString(response));
        if (response.isSuccess()) {
            logger.info("退款成功:订单" + order_id);
        } else {
            logger.info("退款失败:订单" + order_id);
        }
        return response.isSuccess();
    }

    /**
     * 微信退款
     **/
    public boolean wxRefundMoney(String order_id, String trade_no, Double actual_amount) throws Exception {
        actual_amount = actual_amount * 100;
        Map param = new HashMap();
        param.put("appid", appid);//公众账号ID
        param.put("mch_id", mch_id);//商户号

        String uuid = UUID.randomUUID().toString().replace("-", "");//随机32位字符串
        param.put("nonce_str", uuid);
        param.put("out_trade_no", order_id);//商户订单号
        param.put("out_refund_no", trade_no);//商户退款单号
        param.put("total_fee", actual_amount.intValue());//订单金额
        param.put("refund_fee", actual_amount.intValue());//申请退款金额

        String sign = WXPaySign(param, key);

        param.put("sign", sign);

        String xml = XMLUtil.toXml(param);


        String result = HttpConnectionPoolUtil.httpsRequest(refund, xml, param, ssl_name);
        if (StringUtils.isEmpty(result)) {
//            logger.info("微信退款失败");
            throw new BussinessException(BizExceptionEnum.REFUND_ERROR);
        }
//        logger.info("微信退款result：" + JSONObject.toJSONString(result));
        Map<String, Object> data = XMLUtil.toMap(result);
        String return_code = data.get("return_code").toString();
//        logger.info("微信退款data：" + JSONObject.toJSONString(data));
        if (return_code.equals("SUCCESS")) {
            String result_code = data.get("result_code").toString();
            if (return_code.equals("SUCCESS")) {
                logger.info("退款成功:订单" + order_id);
                return true;
            }
        } else {
            throw new BussinessException(BizExceptionEnum.REFUND_ERROR);
        }
        logger.info("退款失败:订单" + order_id);
        return false;
    }
}
