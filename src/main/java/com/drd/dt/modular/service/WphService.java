package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface WphService {

    /**
     * 唯品会订单获取
     */
    Tip wphOrder(String start_time,String end_time,Integer status) throws Exception;


    /**
     * 唯品会链接生成购买链接
     **/
    Tip wphGenerateLink(Integer user_id, String goods_id, Integer buy_type) throws Exception;

    /**
     * 唯品会关键词查询商品
     */
    Tip wphQuery(Integer page, String keyword, Integer order, String fieldname,Integer user_id) throws Exception;

    /**
     * 唯品会商品详情
     **/
    Tip wphGoodsDetail(String goods_id, Integer user_id, HttpServletRequest request, String deviceT, String app_version) throws Exception;
}
