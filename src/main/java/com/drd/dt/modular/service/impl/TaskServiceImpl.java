package com.drd.dt.modular.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.filter.UserThreadLocal;
import com.drd.dt.modular.dao.ShareActivityMapper;
import com.drd.dt.modular.dao.TaskMapper;
import com.drd.dt.modular.dao.UserMapper;
import com.drd.dt.modular.dto.*;
import com.drd.dt.modular.entity.*;
import com.drd.dt.modular.service.TaskService;
import com.drd.dt.util.CodeUtils;
import com.drd.dt.util.JedisClient;
import com.drd.dt.util.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by 86514 on 2019/4/10.
 */
@Transactional
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, TaskInfo> implements TaskService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JedisClient jedisClient;
    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserMoneyServiceImpl userMoneyService;
    @Autowired
    private ShareActivityMapper activityMapper;

    /**
     * 获取用户的任务列表
     **/
    @Override
    public Tip taskCenterInfo(Integer user_id, String token_id) throws Exception{
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        logger.info("【开始获取用户:"+user_id+"的任务列表】");
        JSONObject result = new JSONObject();
        TaskUserInfoDTO taskUserInfo = taskMapper.getTaskUserInfo(user_id);//获取任务中心用户基本信息
        List<ImageConfig> imgs = taskMapper.selectTaskDefaultPic();
        User user = UserThreadLocal.get();
        Integer level = 0;
        if (null != user){
            level = user.getLevel();
            String wx_num = user.getWx_num();
            checkTask(user_id,token_id,wx_num);//检测用户任务情况
            Integer gold_coin = user.getGold_coin();
            taskUserInfo.setHead_pic(user.getHead_pic());
            taskUserInfo.setTotal_coin(gold_coin);
            taskUserInfo.setNick_name(user.getNick_name());
            taskUserInfo.setTotal_money(String.valueOf(Double.valueOf(gold_coin) / 100));
        }
        String level_coin = "lv"+level+"_coin";
        List<TaskList> baseList = taskMapper.selectUserBaseTaskList(user_id,level_coin);//获取用户基本任务列表
        List<TaskList> dailyList = taskMapper.selectUserDailyTaskList(user_id,level_coin);//获取用户每日任务列表
        List<TaskInfo> taskInfo = JSON.parseArray(jedisClient.get("htsq:taskInfo"), TaskInfo.class);
       if (baseList.size() < 1 && dailyList.size() < 1){
            if (null == taskInfo || taskInfo.size() < 1){
                EntityWrapper<TaskInfo> wrapper = new EntityWrapper<>();
                taskInfo = taskMapper.selectList(wrapper);
                jedisClient.set("htsq:taskInfo",JSON.toJSONString(taskInfo));
            }
            List<TaskInfo> baseCollect = taskInfo.stream().filter(t -> t.getType() == 1).collect(Collectors.toList());//基础任务
            List<TaskInfo> dailyCollect = taskInfo.stream().filter(t -> t.getType() == 2).collect(Collectors.toList());//每日任务
            List<TaskList> base = new ArrayList<>();
            List<TaskList> daily = new ArrayList<>();
            String time = Constant.y_M_d_H_m_s.format(new Date());
            Integer finalLevel = level;
            baseCollect.forEach(t -> dto2entity(base,t,user_id,time, finalLevel));
            dailyCollect.forEach(t -> dto2entity(daily,t,user_id,time,finalLevel));
            baseList = base;dailyList = daily;
            jedisClient.setWithExpire("htsq:taskList:" + user_id,"1",10);
            taskMapper.insertBaseTask(base);
            taskMapper.insertDailyTask(daily);
        }
        result.put("taskUserInfo",taskUserInfo);
        result.put("baseList",baseList);
        result.put("dailyList",dailyList);
        result.put("images",imgs);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),result);
    }

    private void checkTask(Integer user_id,String token_id,String wx_num){
        try {
            String today = Constant.y_M_d.format(new Date());
            TaskList wx_task = taskMapper.getUserBaseTaskDetail(user_id, "4");
            if (null != wx_task) {
                Integer wx_status = wx_task.getStatus();
                if (!StringUtils.isEmpty(wx_status) && wx_status == 1 && !StringUtils.isEmpty(wx_num)) {//已绑定微信
                    finish(user_id, token_id, "4", 0, 1);
                }
            }
            TaskList fo_task = taskMapper.getUserBaseTaskDetail(user_id, "5");
            if (null != fo_task) {
                Integer fo_status = fo_task.getStatus();
                if (!StringUtils.isEmpty(fo_status) && fo_status == 1) {
                    Integer count = taskMapper.getUserOrderInfo(user_id, "");//获取用户首单信息
                    Integer own_count = taskMapper.getOwnOrderInfo(user_id,"");
                    if (count + own_count > 0) {
                        finish(user_id, token_id, "5", 0, 1);
                    }
                }
            }
            TaskList to_task = taskMapper.getUserDailyTaskDetail(user_id, "9");
            if (null != to_task){
                Integer to_status = to_task.getStatus();
                if (!StringUtils.isEmpty(to_status) && to_status == 1) {
                    Integer today_count = taskMapper.getUserOrderInfo(user_id, today);//获取用户今日下单信息
                    Integer own_count = taskMapper.getOwnOrderInfo(user_id,today);
                    if (today_count + own_count > 0) {
                        finish(user_id, token_id, "9", 0, 2);
                    }
                }
            }

            TaskListDTO detail = taskMapper.getDailyTaskDetail(user_id, 11);
            if (null != detail) {
                Integer times = detail.getTimes();
                List<Integer> invite_ids = taskMapper.getTodayUserFans(user_id, today);//邀请新用户
                Integer invite = 0;
                for (Integer id:invite_ids) {
                    if (invite >= 2) {//当邀请满两个时，完成任务，不再查找
                        break;
                    }
                    List<Integer> task_count = taskMapper.getFinishBaseTaskUser(id);
                    if (task_count.size() >= 5) {//完成五个基础任务才算注册成功
                        invite = invite + 1;
                    }
                }
                if (invite > times) {
                    finish(user_id, token_id, "11", 0, 2);
                }
            }
        } catch (Exception e) {
            logger.error("{检测用户任务信息错误}", e.getMessage());
        }
    }

    /**
     * 用户完成任务
     **/
    @Override
    public Tip finish(Integer user_id, String token_id, String task_id,Integer is_double,Integer task_type) throws Exception{
        //原子性 redis 测试
        String key = "finish:user_id-" + user_id+":task_id-" +task_id;
        long setnx = jedisClient.setnx(key, key, 2);
        if (setnx != 1) {
            throw new BussinessException(BizExceptionEnum.OPERATION_TOO_FAST);
        }

        logger.info("【开始用户:"+user_id+"的任务："+task_id+"完成接口】");
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id) || StringUtils.isEmpty(task_id)
                || StringUtils.isEmpty(is_double) || StringUtils.isEmpty(task_type)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        JSONObject result = new JSONObject();
        Integer status = 1;
        List<TaskInfo> taskInfo = JSON.parseArray(jedisClient.get("htsq:taskInfo"), TaskInfo.class);
        TaskInfo info = JSON.parseObject(jedisClient.get("htsq:taskFinish:"+user_id+":"+task_id), TaskInfo.class);//查看redis是否存在，防止重复提交
        if (null != info){//已存在
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        }
        if (null == taskInfo || taskInfo.size() < 1) {
            EntityWrapper<TaskInfo> wrapper = new EntityWrapper<>();
            taskInfo = taskMapper.selectList(wrapper);
            jedisClient.set("htsq:taskInfo", JSON.toJSONString(taskInfo));
        }
        TaskList detail = new TaskList(); //获取用户任务详情
        if (task_type == 1){//基础任务
            detail = taskMapper.getUserBaseTaskDetail(user_id,task_id);
        } else if (task_type == 2){//每日任务
            detail = taskMapper.getUserDailyTaskDetail(user_id,task_id);
        }
        if (null == detail){
            return ResultUtil.result(BizExceptionEnum.TASK_INFO_NOT_EXIST.getCode(), BizExceptionEnum.TASK_INFO_NOT_EXIST.getMessage());
        }
        TaskList finalDetail = detail;
        List<TaskInfo> collect = taskInfo.stream().filter(t -> t.getId().equals(finalDetail.getTask_id())).collect(Collectors.toList());
        Integer coin = 0;
        if (null != collect && collect.size() == 1){
            jedisClient.setWithExpire("htsq:taskFinish:"+user_id+":"+task_id,JSON.toJSONString(collect.get(0)),10);
            Integer total_times = collect.get(0).getTimes();//需要完成的总次数
            Integer times = detail.getTimes();//用户已完成的次数
            Integer task_status = detail.getStatus();//用户任务状态
            if (times + 1 < total_times && task_status == 1){//用户本次还没完成任务时
                if (task_type == 1){//更新基础任务完成次数
                    taskMapper.updateUserBaseTask(user_id,task_id,null);
                } else if (task_type == 2) {//更新每日任务完成次数
                    taskMapper.updateUserDailyTask(user_id,task_id,null);
                }
            }else if (times + 1 == total_times && task_status == 1){//用户本次已完成任务时
                if (task_type == 1){//更新基础任务完成次数
                    taskMapper.updateUserBaseTask(user_id,task_id,2);
                } else if (task_type == 2) {//更新每日任务完成次数
                    taskMapper.updateUserDailyTask(user_id,task_id,2);
                }
                coin = insertCoinDetail(user_id, is_double, collect);//任务完成时返金币
                status = 2;
            }
        }
        result.put("status",status);
        result.put("coin",coin);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),result);
    }

    /**
     * 查询用户的任务详情
     */
    @Override
    public Tip detail(Integer user_id, String token_id, Integer task_id,Integer task_type) throws Exception {
        logger.info("【开始用户:"+user_id+"的任务："+task_id+"详情接口】");
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id) || StringUtils.isEmpty(task_id) || StringUtils.isEmpty(task_type)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        //1基础任务 2每日任务
        TaskListDTO detail = new TaskListDTO();
        if (task_type == 1){
            detail = taskMapper.getBaseTaskDetail(user_id,task_id);
        } else if (task_type == 2){
            detail = taskMapper.getDailyTaskDetail(user_id,task_id);
        }
        if (null == detail){
            return ResultUtil.result(BizExceptionEnum.TASK_INFO_NOT_EXIST.getCode(), BizExceptionEnum.TASK_INFO_NOT_EXIST.getMessage());
        }
        List<TaskInfo> taskInfo = JSON.parseArray(jedisClient.get("htsq:taskInfo"), TaskInfo.class);
        if (null == taskInfo || taskInfo.size() < 1) {
            EntityWrapper<TaskInfo> wrapper = new EntityWrapper<>();
            taskInfo = taskMapper.selectList(wrapper);
            jedisClient.set("htsq:taskInfo", JSON.toJSONString(taskInfo));
        }
        List<Integer> collect = taskInfo.stream().filter(t -> task_id.equals(t.getId())).map(TaskInfo::getTimes).collect(Collectors.toList());
        if (null != collect && collect.size() > 0){
            detail.setTotal_times(collect.get(0));
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),detail);
    }

    /**
     * 用户金币兑换
     */
    @Override
    public Tip exchange(Integer user_id, String token_id, Integer coin) {
        logger.info("【开始用户:"+user_id+"金币兑换coin："+coin+"详情接口】");
        JSONObject result = new JSONObject();
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id) || StringUtils.isEmpty(coin)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        if (coin < 100){//100金币起
            return ResultUtil.result(BizExceptionEnum.GOLD_COIN_EXCHANGE_NUM_ERROR.getCode(), BizExceptionEnum.GOLD_COIN_EXCHANGE_NUM_ERROR.getMessage());
        }
        //获取已有金币数量，判断能否兑换
        User user = UserThreadLocal.get();
        Integer hold_coin = 0;
        if (null != user){
            hold_coin = user.getGold_coin();
        }
        if (hold_coin >= coin) {//持有金币大于预兑换金币时
            List<TaskCoin> coinList = new ArrayList<>();
            //金币兑换红包金额
            Double money = Double.valueOf(coin) / 100;
            if (money > 0){
                TaskCoin taskInfo = JSON.parseObject(jedisClient.get("htsq:taskExchange:"+user_id), TaskCoin.class);
                if (null != taskInfo){//已兑换时（防止重复提交）
                    return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
                }
                TaskCoin taskCoin = new TaskCoin();
                taskCoin.setCoin_num(coin);
                taskCoin.setRemark("金币兑换");
                taskCoin.setAction_type(2);
                taskCoin.setUser_id(user_id);
                taskCoin.setOrigin_id(user_id);
                taskCoin.setCreate_time(Constant.y_M_d_H_m_s.format(new Date()));
                coinList.add(taskCoin);
                jedisClient.setWithExpire("htsq:taskExchange:"+user_id,JSON.toJSONString(taskCoin),5);
                taskMapper.insertTaskDetail(coinList);//插入任务金币明细
                taskMapper.updateUserDesCoin(coin,user_id);//减去用户金币

                Map<String,Object> param = new HashMap<>();
                param.put("user_id",user_id);
                param.put("money",money);
                param.put("type","9");
                param.put("remark","金币兑换活动");
                param.put("action_type","1");
                activityMapper.insertBalanceDetail(param);//插入balance表记录
                param.put("message_type","5");
                activityMapper.insertMessageMoney(param);//插入MessageMoney消息表记录
                ShareActivityDTO share = activityMapper.getUserProfitInfo(user_id);//获取用户活动金额信息
                if (null != share){//存在
                    param.put("id",share.getId());
                    activityMapper.updateUserActivityBalance(param);//更新用户活动佣金
                }else {
                    activityMapper.insertUserActivityBalance(param);//新增用户活动佣金
                }
                result.put("desc",coin + "金币兑换"+money+"元");
            }
        } else {
            return ResultUtil.result(BizExceptionEnum.GOLD_COIN_NOT_ENOUGH_ERROR.getCode(), BizExceptionEnum.GOLD_COIN_NOT_ENOUGH_ERROR.getMessage());
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),result);
    }

    /**
     * 用户金币兑换历史记录列表
     **/
    @Override
    public Tip exchangeHistory(Integer user_id, String token_id, Integer pageNum,Integer type) {
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        if (StringUtils.isEmpty(type)){
            type = 2;
        }
        Page<TaskExgHistoryDTO> page = new Page<>(pageNum,Constant.PAGE_SIZE_20);
        List<TaskExgHistoryDTO> list = taskMapper.selectExchangeHistory(page,user_id,type);
        list.forEach(t -> t.setDesc("兑换"+(Double.valueOf(t.getCoin_num())/100)+"元活动佣金"));
        page.setRecords(list);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),page);
    }

    /**
     * 用户金币兑换历史记录金币信息
     **/
    @Override
    public Tip exchangeCoinInfo(Integer user_id, String token_id) {
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        TaskExgCoinDTO data = taskMapper.getExgHistoryCoin(user_id);
        User user = UserThreadLocal.get();
        if (null != user){
            Integer gold_coin = user.getGold_coin();
            data.setNow_coin(gold_coin);
            data.setMoney(String.valueOf(Double.valueOf(gold_coin) / 100));
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),data);
    }

    /**
     * 粉丝金币贡献列表记录
     **/
    @Override
    public Tip fansContributionList(Integer user_id, String token_id,Integer pageNum,Integer type) {
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id) ||
                StringUtils.isEmpty(pageNum) || StringUtils.isEmpty(type)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        Page<TaskFansContributionDTO> page = new Page<>(pageNum,Constant.PAGE_SIZE_20);
        String time = "";
        if (type == 2){//今日贡献榜
            time = Constant.y_M_d.format(new Date());
        }
        List<TaskFansContributionDTO> lists = taskMapper.selectUserFansContributionList(page,user_id,time);
        //获取用户直属粉丝id
        List<Integer> direct_ids = taskMapper.selectUserDirectFans(user_id);
        lists.forEach(t -> {
            Integer origin_id = t.getOrigin_id();
            boolean is_direct = direct_ids.stream().anyMatch(id -> id.equals(origin_id));
            if (is_direct){
                t.setIs_direct(1);
            }
        });
        page.setRecords(lists);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),page);
    }

    private Integer insertCoinDetail(Integer user_id,Integer is_double,List<TaskInfo> collect){
        User user = UserThreadLocal.get();
        Integer return_coin = 0;
        if (null != user){
            TaskCoin taskCoin = new TaskCoin();
            String time = Constant.y_M_d_H_m_s.format(new Date());
            List<TaskCoin> coinList = new ArrayList<>();
            Integer level = user.getLevel();
            Integer coin = collect.get(0).getLv0_coin();
            String task_name = collect.get(0).getTask_name();
            Integer is_up_rake = collect.get(0).getIs_up_rake();//是否向上级返 1是 0否
            Integer total_coin = collect.get(0).getTotal_coin();
            Integer flag_double = collect.get(0).getIs_double();
            if (level == 1){
                coin = collect.get(0).getLv1_coin();
            } else if (level == 2){
                coin = collect.get(0).getLv2_coin();
            } else if (level == 3){
                coin = collect.get(0).getLv3_coin();
            }
            if (!StringUtils.isEmpty(task_name) && task_name.equals("签到")){//签到随机金额
                Random random = new Random();
                coin = random.nextInt(total_coin) + 1;
            }
            if (is_double == 1 && flag_double == 1){//是双倍时
                taskCoin.setCoin_num(coin * 2);
                taskCoin.setRemark("任务双倍收入");
            }else {
                taskCoin.setCoin_num(coin);
                taskCoin.setRemark("任务金币收入");
            }
            taskCoin.setUser_id(user_id);
            taskCoin.setTask_id(collect.get(0).getId());
            taskCoin.setOrigin_id(user_id);
            taskCoin.setAction_type(1);//任务收入
            taskCoin.setCreate_time(time);
            coinList.add(taskCoin);
            taskMapper.updateUserCoin(user_id,taskCoin.getCoin_num(),time);//更新用户表金币数量
            if (is_up_rake == 1){//需要向上级返时
                //获取所有上级用户
                List<User> userList = userMoneyService.queryUpUserListByBaseUser(user);
                if (null != userList && userList.size() > 0){//
                    if (level == 0){//当前用户为普通用户
                        User upUserByUser = userMoneyService.getUpUserByUser(userList, user);//根据当前用户去LIST查找他的直接上级
                        coinList = upNormalUserRake(userList,coinList,upUserByUser,user,total_coin,user_id,time);
                    } else {//vip下单
                        if (userMoneyService.checkUpVipLeveByList2(userList, user)) {
                            coinList = upVIPUserRake(coinList, userList, user, user, level, true, total_coin, time);
                        }
                    }
                }
            }
            return_coin = taskCoin.getCoin_num();
            taskMapper.insertTaskDetail(coinList);//插入任务金币明细表
        }
        return return_coin;
    }

    /**
     * VIP用户任务金币返
     */
    private List<TaskCoin> upVIPUserRake(List<TaskCoin> coinList,List<User> userList, User currUser, User Origin_User, int uplevel, boolean allowLevelEqual,Integer total_coin,String time){
        TaskCoin taskCoin = new TaskCoin();
        double rate = 0.0;
        Integer uId = 0;
        User upuser = userMoneyService.getUpUserByUser(userList, currUser);
        if (upuser != null) {//找到上一级,计算团队收益
            uId = upuser.getId();
            if (upuser.getLevel() < uplevel) {
                upVIPUserRake(coinList,userList, upuser, Origin_User, uplevel, allowLevelEqual,total_coin,time);//继续找更高LEVE VIP  上级
            } else if (uplevel == 1) {//会员
                if (upuser.getLevel() == 1) {
                    if (allowLevelEqual) {
                        uplevel = upuser.getLevel();
                        rate = 0.03;
                        taskCoin.setRemark("CPS间接返金币3%");
                    }
                    upVIPUserRake(coinList,userList, upuser, Origin_User, uplevel, false,total_coin,time);//继续找更高LEVE VIP  上级
                } else if (upuser.getLevel() == 2) {//直属
                    if (userMoneyService.checkUpVipLeveByList(userList, upuser)) {//预判断上级是否是平级
                        Long up_coin = Math.round(total_coin * 0.03);
                        taskCoin.setRemark("CPS间接返金币3%");
                        coinList = upRake(coinList,taskCoin,up_coin,Origin_User.getId(),upuser.getId(),time);
                        uplevel = upuser.getLevel();
                        upVIPUserRake(coinList,userList, upuser, Origin_User, uplevel, true,total_coin,time);//继续找更高LEVE VIP  上级
                    } else {
                        Long up_coin = Math.round(total_coin * 0.03);
                        taskCoin.setRemark("CPS间接返金币3%");
                        coinList = upRake(coinList,taskCoin,up_coin,Origin_User.getId(),upuser.getId(),time);
                        uplevel = upuser.getLevel();
                        if (userMoneyService.checkUpVipLeveByList2(userList, upuser)) {//预判断是否有上级
                            upVIPUserRake(coinList,userList, upuser, Origin_User, uplevel, false,total_coin,time);//继续找更高LEVE VIP  上级
                        }
                    }
                } else if (upuser.getLevel() == 3) {
                    if (userMoneyService.checkUpVipLeveByList(userList, upuser)) {//预判断上级是否是平级
                        Long up_coin = Math.round(total_coin * 0.09);
                        taskCoin.setRemark("CPS间接返金币9%");
                        coinList = upRake(coinList,taskCoin,up_coin,Origin_User.getId(),upuser.getId(),time);
                        uplevel = upuser.getLevel();
                        upVIPUserRake(coinList,userList, upuser, Origin_User, uplevel, false,total_coin,time);//继续找更高LEVE VIP  上级
                    } else {
                        Long up_coin = Math.round(total_coin * 0.1);
                        taskCoin.setRemark("CPS间接返金币10%");
                        coinList = upRake(coinList,taskCoin,up_coin,Origin_User.getId(),upuser.getId(),time);
                        uplevel = upuser.getLevel();
                        if (userMoneyService.checkUpVipLeveByList2(userList, upuser)) {//预判断是否有上级
                            upVIPUserRake(coinList,userList, upuser, Origin_User, uplevel, false,total_coin,time);//继续找更高LEVE VIP  上级
                        }
                    }
                }
            } else if (uplevel == 2) {//运营商
                if (upuser.getLevel() == 2) {
                    if (allowLevelEqual) {
                        uplevel = upuser.getLevel();
                        rate = 0.02;
                        taskCoin.setRemark("CPS间接返金币2%");
                    }
                    if (userMoneyService.checkUpVipLeveByList2(userList, upuser)) {//预判断是否有上级
                        upVIPUserRake(coinList,userList, upuser, Origin_User, uplevel, false,total_coin,time);//继续找更高LEVE VIP  上级
                    }
                } else {
                    if (upuser.getLevel() == 3) {
                        if (userMoneyService.checkUpVipLeveByList(userList, upuser)) {//预判断上级是否是平级
                            Long up_coin = Math.round(total_coin * 0.04);
                            taskCoin.setRemark("CPS间接返金币4%");
                            coinList = upRake(coinList,taskCoin,up_coin,Origin_User.getId(),upuser.getId(),time);
                            uplevel = upuser.getLevel();
                            upVIPUserRake(coinList,userList, upuser, Origin_User, uplevel, false,total_coin,time);//继续找更高LEVE VIP  上级
                        } else {
                            rate = 0.05;
                            taskCoin.setRemark("CPS间接返金币5%");
                        }
                    }
                }
            } else if (uplevel == 3) {//联创
                if (upuser.getLevel() == 3) {//联创平级的情况
                    rate = 0.01;
                    taskCoin.setRemark("CPS间接返金币1%");
                } else {
                    if (userMoneyService.checkUpVipLeveByList2(userList, upuser)) {//预判断是否有上级
                        upVIPUserRake(coinList,userList, upuser, Origin_User, uplevel, false,total_coin,time);//继续找更高LEVE VIP  上级
                    }
                }
            }
        } else {
            if (currUser.getLevel() >= uplevel) {
                uId = currUser.getId();
                if (currUser.getLevel() == 1 && uplevel == 1) {
                    rate = 0.03;
                    taskCoin.setRemark("CPS间接返金币3%");
                } else if (currUser.getLevel() == 2) {
                    if (uplevel == 1) {
                        rate = 0.05;
                        taskCoin.setRemark("CPS间接返金币5%");
                    } else if (uplevel == 2) {
                        rate = 0.02;
                        taskCoin.setRemark("CPS间接返金币2%");
                    }
                } else if (currUser.getLevel() == 3) {
                    if (uplevel == 1) {
                        rate = 0.1;
                        taskCoin.setRemark("CPS间接返金币10%");
                    } else if (uplevel == 2) {
                        rate = 0.05;
                        taskCoin.setRemark("CPS间接返金币5%");
                    } else if (uplevel == 3) {
                        rate = 0.01;
                        taskCoin.setRemark("CPS间接返金币1%");
                    }
                }
            }
        }
        Long r_coin = Math.round(rate * total_coin);
        int rate_coin = r_coin.intValue();
        if (rate_coin < 1) {//如果金币小于1，不做处理
            return coinList;
        }
        taskCoin.setUser_id(uId);
        taskCoin.setCoin_num(rate_coin);
        taskCoin.setOrigin_id(Origin_User.getId());
        taskCoin.setAction_type(3);//粉丝金币收入
        taskCoin.setCreate_time(time);
        taskCoin.setTask_id(coinList.get(0).getTask_id());
        coinList.add(taskCoin);
        taskMapper.updateUserCoin(uId,rate_coin,time);//更新用户表金币数量
        return coinList;
    }

    private List<TaskCoin> upRake(List<TaskCoin> coinList,TaskCoin taskCoin,Long r_coin,Integer user_id,Integer up_user_id,String time){
        int rate_coin = r_coin.intValue();
        if (rate_coin < 1) {//如果金币小于1，不做处理
            return coinList;
        }
        taskCoin.setCoin_num(rate_coin);
        taskCoin.setUser_id(up_user_id);
        taskCoin.setOrigin_id(user_id);
        taskCoin.setAction_type(3);//粉丝金币收入
        taskCoin.setCreate_time(time);
        taskCoin.setTask_id(coinList.get(0).getTask_id());
        coinList.add(taskCoin);
        taskMapper.updateUserCoin(up_user_id,rate_coin,time);//更新用户表金币数量
        return coinList;
    }

    /**
     * 普通用户任务金币返
     */
    private List<TaskCoin> upNormalUserRake(List<User> userList,List<TaskCoin> coinList,User upuser,User user,Integer total_coin,Integer user_id,String time){
        if (upuser != null) {//有上级
            TaskCoin taskCoin = new TaskCoin();
            double rate = 0.0;
            Integer uId = upuser.getId();
            Integer level = upuser.getLevel();
            if (level == 0) {//直接上级为普通级别，返5%
                rate = 0.05;//返总金币的5%
                taskCoin.setRemark("直接返金币5%");
                while (true) {//继续找最近的VIP
                    upuser = userMoneyService.getUpUserByUser(userList, upuser);
                    if (upuser != null) {//找到上一级,计算团队收益
                        TaskCoin taskCoin_up1 = new TaskCoin();
                        if (upuser.getLevel() > 0) {//找到VIP
                            double rate_up1 = 0.0;
                            if (upuser.getLevel() == 1) {//超级会员 22
                                if (userMoneyService.checkUpVipLeveByList(userList, upuser)) {//预上一级还是平级
                                    taskCoin_up1.setRemark("间接返金币12%");
                                    rate_up1 = 0.12;
                                } else {
                                    taskCoin_up1.setRemark("间接返金币15%");
                                    rate_up1 = 0.15;
                                }
                            } else if (upuser.getLevel() == 2) {//运营商 28
                                if (userMoneyService.checkUpVipLeveByList(userList, upuser)) {//预上一级还是平级
                                    taskCoin_up1.setRemark("间接返金币18%");
                                    rate_up1 = 0.18;
                                } else {
                                    taskCoin_up1.setRemark("间接返金币20%");
                                    rate_up1 = 0.2;
                                }
                            } else if (upuser.getLevel() == 3) {//联创 32
                                if (userMoneyService.checkUpVipLeveByList(userList, upuser)) {//预上一级还是平级
                                    taskCoin_up1.setRemark("间接返金币29%");
                                    rate_up1 = 0.24;
                                } else {
                                    taskCoin_up1.setRemark("间接返金币30%");
                                    rate_up1 = 0.25;
                                }
                            }
                            Long up1 = Math.round(rate_up1 * total_coin);
                            int coin_up1 = up1.intValue();
                            if (coin_up1 < 1) {//如果金币小于1，不做处理
                                return coinList;
                            }
                            taskCoin_up1.setCoin_num(coin_up1);
                            taskCoin_up1.setUser_id(upuser.getId());
                            taskCoin_up1.setOrigin_id(user_id);
                            taskCoin_up1.setTask_id(coinList.get(0).getTask_id());
                            taskCoin_up1.setAction_type(3);//粉丝金币收入
                            taskCoin_up1.setCreate_time(time);
                            coinList.add(taskCoin_up1);
                            taskMapper.updateUserCoin(upuser.getId(),coin_up1,time);//更新用户表金币数量
                            if (userMoneyService.checkUpVipLeveByList2(userList, user)) {
                                coinList = upVIPUserRake(coinList, userList, upuser, user, upuser.getLevel(), true, total_coin, time);
                            }
                            break;//找到间接返佣金目标，结束查找
                        }
                    } else {
                        break;
                    }
                }
            } else if (level == 1) {//超级会员 27
                rate = 0.2;
                taskCoin.setRemark("间接返金币20%");
                if (userMoneyService.checkUpVipLeveByList(userList, upuser)) {//预上一级还是平级
                    rate = 0.17;
                    taskCoin.setRemark("间接返金币17%");
                }
            } else if (level == 2) {//运营商 33
                rate = 0.25;
                taskCoin.setRemark("间接返金币25%");
                if (userMoneyService.checkUpVipLeveByList(userList, upuser)) {//预上一级还是平级
                    rate = 0.23;
                    taskCoin.setRemark("间接返金币23%");
                }
            } else if (level == 3) {//联创 37
                rate = 0.3;
                taskCoin.setRemark("间接返金币30%");
                if (userMoneyService.checkUpVipLeveByList(userList, upuser)) {//预上一级还是平级
                    rate = 0.29;
                    taskCoin.setRemark("间接返金币29%");
                }
            }
            Long r_coin = Math.round(rate * total_coin);
            int rate_coin = r_coin.intValue();
            if (rate_coin < 1) {//如果金币小于1，不做处理
                return coinList;
            }
            taskCoin.setCoin_num(rate_coin);
            taskCoin.setUser_id(uId);
            taskCoin.setOrigin_id(user_id);
            taskCoin.setTask_id(coinList.get(0).getTask_id());
            taskCoin.setAction_type(3);//粉丝金币收入
            taskCoin.setCreate_time(time);
            coinList.add(taskCoin);
            taskMapper.updateUserCoin(uId,rate_coin,time);//更新用户表金币数量
            if (level > 0 && userMoneyService.checkUpVipLeveByList2(userList, user)) {
                coinList = upVIPUserRake(coinList, userList, upuser, user, upuser.getLevel(), true, total_coin, time);
            }
        }
        return coinList;
    }

    /**
     * 获取上级用户
     */
    private User geUPUser(Integer introducer){
        User user = null;
        if (null != introducer && introducer > 0){
            String str = CodeUtils.idToCode(String.valueOf(introducer));
            EntityWrapper<User> wrapper = new EntityWrapper<>();
            wrapper.eq("friend_code",str);
            List<User> userList = userMapper.selectList(wrapper);
            if (null != userList && userList.size() > 0){
                user = userList.get(0);
            }
        }
        return user;
    }

    private static List<TaskList> dto2entity(List<TaskList> list,TaskInfo t,Integer user_id,String time,Integer level){
        TaskList task = new TaskList();
        Integer coin = t.getLv0_coin();
         if (level == 1){
             coin = t.getLv1_coin();
        } else if (level == 2){
             coin = t.getLv2_coin();
        } else if (level == 3){
             coin = t.getLv3_coin();
        }
        task.setTask_id(t.getId());
        task.setTask_name(t.getTask_name());
        task.setTask_pic(t.getTask_pic());
        task.setUser_id(user_id);
        task.setTask_desc(t.getTask_desc());
        task.setTotal_times(t.getTimes());
        task.setStatus(1);
        task.setTimes(0);
        task.setCreate_time(time);
        task.setCoin(coin);
        task.setType(t.getType());
        task.setIs_double(t.getIs_double());
        list.add(task);
        return list;
    }
}
