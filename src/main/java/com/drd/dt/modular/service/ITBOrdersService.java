package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface ITBOrdersService {

    /**
     *  通过淘宝联盟
     *  获取订单
     */
    Tip getOrderByUnion(String start_time, String end_time, String order_scene, String member_type, String query_type) throws Exception;

    /**
     * 618预售商品差价返
     */
    Tip update618order(String start_time, String end_time, String order_scene, String member_type, String query_type) throws Exception;
}
