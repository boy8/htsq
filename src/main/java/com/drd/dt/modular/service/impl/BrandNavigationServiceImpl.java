package com.drd.dt.modular.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.modular.dao.BrandNavigationMapper;
import com.drd.dt.modular.dao.HomeMapper;
import com.drd.dt.modular.dto.BrandNavigationDTO;
import com.drd.dt.modular.service.BrandNavigationService;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by 86514 on 2019/3/25.
 */
@Service
@Transactional
public class BrandNavigationServiceImpl implements BrandNavigationService {

    @Autowired
    private BrandNavigationMapper brandNavigationMapper;


    /**
     * 获取品牌导航列表
     **/
    @Override
    public Tip brandList(Integer pageNum, Integer type) throws Exception{
        if (null == type || (type != 1 && type != 2)){
            throw new BussinessException(BizExceptionEnum.PATAM_ERROR);
        }
        //1品牌  2旗舰店
        if (type == 1){
            List<BrandNavigationDTO> result = brandNavigationMapper.selectBrandsInfo(type);
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
        }else if (type == 2){
            Page page = new Page(pageNum, Constant.PAGE_SIZE_20);
            List<BrandNavigationDTO> list = brandNavigationMapper.selectStoresInfo(page, type);
            page.setRecords(list);
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), page);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }
}
