package com.drd.dt.modular.service.impl;

import com.drd.dt.back.dao.OwnGoodsManageMapper;
import com.drd.dt.back.dto.OwnGoodsOrderDTO;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.modular.dao.OrderGiftMapper;
import com.drd.dt.modular.dao.OwnGoodsMapper;
import com.drd.dt.modular.entity.OrderGift;
import com.drd.dt.modular.entity.User;
import com.drd.dt.modular.service.OrderGiftService;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OrderGiftServiceImpl implements OrderGiftService {

    @Autowired
    private OrderGiftMapper orderGiftMapper;
    @Autowired
    private OwnGoodsMapper ownGoodsMapper;
    @Autowired
    private ExpressTradeServiceImpl expressTradeService;
    @Autowired
    private OwnGoodsManageMapper ownGoodsManageMapper;


    @Override
    public int upOrderGift(Map<String, Object> map) throws Exception {
        return orderGiftMapper.upOrderGift(map);
    }

    @Override
    public int finOrDerGift(String order_id) throws Exception {
        return orderGiftMapper.finOrDerGift(order_id);
    }

    @Override
    public User getUserDetail(String order_id) {
        return orderGiftMapper.getUserDetail(order_id);
    }

    @Override
    public OrderGift getOrderGiftDetail(String order_id) {
        return orderGiftMapper.getOrderGiftDetail(order_id);
    }

    @Override
    public int upInventorySell(String goods_id) {
        return orderGiftMapper.upInventorySell(goods_id);
    }

    @Override
    public Tip orderTask() {
        //1 查询 自营订单 (3)待收货 订单列表
        List<OwnGoodsOrderDTO> list = ownGoodsManageMapper.selectStatusList(3);
        //2 循环查询
        list.forEach(obj -> {
            Tip tip = expressTradeService.queryExpress(obj.getOrder_id(), obj.getLogistics_num());
            if (null == tip || tip.getCode() != 200) {
                throw new BussinessException(BizExceptionEnum.USER_ORDER_ERROR_TASK);
            }
        });
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }
}
