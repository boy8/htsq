package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface MessageService {

    /**
     * 获取消息通知
     **/
    Tip selectMessage(Integer page, Integer type,Integer messageType,Integer user_id,String token_id) throws Exception;

    /**
     * 消息中心首页信息
     **/
    Tip simpleInfo(Integer user_id,String token_id) throws Exception;
}
