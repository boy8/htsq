package com.drd.dt.modular.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.modular.EnumUtils.BusinessAcademyFirstEnum;
import com.drd.dt.modular.EnumUtils.BusinessAcademySecondEnum;
import com.drd.dt.modular.EnumUtils.BusinessAcademyThirdEnum;
import com.drd.dt.modular.dao.BusinessAcademyMapper;
import com.drd.dt.modular.dao.UserMapper;
import com.drd.dt.modular.dto.BusinessAcademyDTO;
import com.drd.dt.modular.dto.DailyGoodsDTO;
import com.drd.dt.modular.dto.GoodsDetailDTO;
import com.drd.dt.modular.dto.PersonalCenterDTO;
import com.drd.dt.modular.entity.BusinessAcademy;
import com.drd.dt.modular.service.BusinessAcademyService;
import com.drd.dt.properties.HDKPropertyConfig;
import com.drd.dt.properties.PDDPropertyConfig;
import com.drd.dt.properties.TBLMPropertyConfig;
import com.drd.dt.util.HttpConnectionPoolUtil;
import com.drd.dt.util.ResultUtil;
import com.drd.dt.util.StringUtil;
import com.pdd.pop.sdk.common.util.JsonUtil;
import com.pdd.pop.sdk.http.PopClient;
import com.pdd.pop.sdk.http.PopHttpClient;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkGoodsRecommendGetRequest;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkOauthGoodsRecommendGetRequest;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsRecommendGetResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkOauthGoodsRecommendGetResponse;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkSpreadGetRequest;
import com.taobao.api.request.TbkTpwdCreateRequest;
import com.taobao.api.response.TbkSpreadGetResponse;
import com.taobao.api.response.TbkTpwdCreateResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Transactional
public class BusinessAcademyServiceImpl implements BusinessAcademyService {
    private static final Logger logger = LoggerFactory.getLogger(BusinessAcademyServiceImpl.class);

    @Autowired
    private BusinessAcademyMapper businessAcademyMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private HDKPropertyConfig hdkPropertyConfig;
    @Autowired
    private UserMoneyServiceImpl testMoneyService;
    @Autowired
    private TBGoodsServiceImpl tbGoodsService;
    @Autowired
    private TBLMPropertyConfig tblmPropertyConfig;

    @Override
    public Tip friendBusinessAcademym(BusinessAcademyDTO map) throws Exception {
        List<BusinessAcademy> all;
        int pageIndex = map.getPageIndex();
        int pageSize = map.getPageSize();
        int first_module = map.getFirst_module();
        int second_module = map.getSecond_module();
        Page page = new Page(pageIndex, pageSize);
        if (first_module == 1){//每日爆款
            if (second_module == 1){//淘宝
                JSONObject result = new JSONObject();
                List data = getRecommendGoodsInfoByHDK(pageIndex);
                result.put("datas",data);
                result.put("totalCounts",1000);
                return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
            }
        }
        if (map.getFirst_module() == 3) {
            all = businessAcademyMapper.friendBusinessAcademymBriefness(page, map);
        } else {
            all = businessAcademyMapper.friendBusinessAcademym(page, map);
        }
        page.setRecords(all);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), page);
    }

    @Override
    public Tip ifUserLevel(BusinessAcademyDTO map) {
        //1 如果是查看文章 校验 当前会员等级是否有权限
        String leve = userMapper.getUserLevel(map.getUser_id());
        BusinessAcademy businessAcademy = businessAcademyMapper.getCommodity(map.getBusiness_id());
        if (null != leve && !leve.equals("") && Integer.parseInt(leve) >= businessAcademy.getShow_lv()) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), businessAcademy);
        } else {//权限不足 返回提示
            return ResultUtil.result(BizExceptionEnum.BUSINESS_ACADEMY_AUTHORITY_LACK.getCode(), BizExceptionEnum.BUSINESS_ACADEMY_AUTHORITY_LACK.getMessage());
        }
    }

    @Override
    public Tip synchronizationAccumulativeTotal(BusinessAcademyDTO map) {

        int req = businessAcademyMapper.synchronizationAccumulativeTotal(map);
        if (req == 1) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        } else {
            return ResultUtil.result(BizExceptionEnum.BUSINESS_ACADEMY_DATA_ADD_ERROR.getCode(), BizExceptionEnum.BUSINESS_ACADEMY_DATA_ADD_ERROR.getMessage());
        }
    }

    @Override
    public Tip getModelCorrelationGrouping(int first_module, int second_module,String app_version,HttpServletRequest request,int deviceT) throws Exception {
        //一级分组
        List<Map<String, String>> first_moduleList = new ArrayList<>();
        String version = userMapper.getVerifyVersion();//获取ios审核版本号
        List<String> firstModule = businessAcademyMapper.getFirstModule();
        //判断是否为国外ip
        String ip = StringUtil.getIpAddr(request);
        String country = new StringUtil().getIPLocation(ip);
        for (String str : firstModule) {
            if (!StringUtils.isEmpty(deviceT) && deviceT == 1){//ios
                if (app_version.equals(version) || country.equals("美国")){//ios 审核版过滤
                    if (!str.equals("1")){
                        continue;
                    }
                }
            }
            Map<String, String> m = new HashMap<>();
            m.put("type", str);
            String typeName = BusinessAcademyFirstEnum.getStrValue(Integer.parseInt(str));
            if (!typeName.equals("")) {
                m.put("typeName", typeName);
                first_moduleList.add(m);
            }
        }
        //2级分组
        List<Map<String, String>> second_moduleList = new ArrayList<>();
        List<String> secondModule = businessAcademyMapper.getSecondModule(first_module);
        if (first_module == 3) {
            secondModule = new ArrayList<String>();
            for (int i = 1; i <= 6; i++) {
                secondModule.add(i + "");
            }
        }
        for (String str : secondModule) {
            Map<String, String> m = new HashMap<>();
            m.put("type", str);
            String typeName = BusinessAcademySecondEnum.getStrValue(Integer.parseInt(str), first_module);
            if (!typeName.equals("")) {
                m.put("typeName", typeName);
                if (first_module == 3) {
                    String icon = BusinessAcademySecondEnum.getStrValueIcon(Integer.parseInt(str), first_module);
                    if (!icon.equals("")) {
                        m.put("typeIcon", icon);
                    }
                }
                second_moduleList.add(m);
            }
        }
        //3级分组
        List<Map<String, String>> third_moduleList = new ArrayList<>();
        for (String str : businessAcademyMapper.getThirdModule(first_module, second_module)) {
            Map<String, String> m = new HashMap<>();
            m.put("type", str);
            String typeName = BusinessAcademyThirdEnum.getStrValue(Integer.parseInt(str), second_module, first_module);
            if (!typeName.equals("")) {
                if (first_module == 2 && second_module == 2) {
                    m.put("typeName", "新人第" + typeName + "天");
                } else {
                    m.put("typeName", typeName);
                }
                third_moduleList.add(m);
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("first_module", first_moduleList);
        map.put("second_module", second_moduleList);
        map.put("third_module", third_moduleList);
        return ResultUtil.result(SuccessResultEnum.ADD_SUCCESS.getCode(), SuccessResultEnum.ADD_SUCCESS.getMessage(), map);
    }

    /**
     * 每日爆款商品拉取
     **/
    @Override
    public Tip insertDailyGoods(Integer page, String time) throws Exception{
        for (int i = 1;i <= page; i++){
            List data = getRecommendGoodsInfoByHDK(i);
            if (data.size() > 0) {
                //type:类型（1美妆 2母婴 3服饰 4数码 5食品 6其他）
                final Boolean[] flag = {true};
                data.stream().forEach(t -> {
                    Map d = (Map) t;
                    Object ima_sList = d.get("ima_sList");
                    Object goods_id = d.get("biz_id");//商品id
                    Object create_time = d.get("create_time");//时间
                    Object price = d.get("price");//价格
                    Object coupon = d.get("coupon");//券
                    Object tkrates = d.get("tkrates");//比例
                    Object text = d.get("text");//内容
                    if (null != goods_id) {//查询商品是否存在
                        List<DailyGoodsDTO> dto = businessAcademyMapper.getByGoodsId(String.valueOf(goods_id));
                        if (dto.size() > 0) {
                            flag[0] = false;
                            return;
                        }
                    }
                    String category = "6";
                    if (null != text) {
                        category = goodsCategory(String.valueOf(text));
                    }
                    d.put("category", category);
                    if (null != ima_sList) {
                        String str = JSON.toJSONString(ima_sList);
                        d.put("ima_sList", str);
                    }
                    Double rake_back = 0.0;
                    if (null != price && null != coupon && null != tkrates) {
                        Double enprice = Double.valueOf(price.toString()) - Double.valueOf(coupon.toString());
                        rake_back = enprice * (Double.valueOf(tkrates.toString()) / 100);
                    }
                    d.put("rake_back", rake_back);//返佣
                });
                if (flag[0]) {
                    businessAcademyMapper.insertDailyGoods(data);
                }
            }
        }
        return ResultUtil.result(SuccessResultEnum.ADD_SUCCESS.getCode(), SuccessResultEnum.ADD_SUCCESS.getMessage());
    }

    /**
     * 每日爆款分类
     **/
    @Override
    public Tip dailyCategory() {
        logger.info("【每日爆款分类接口调用...】");
        JSONArray result = new JSONArray();
        Map map1 = new HashMap();
        Map map2 = new HashMap();
        Map map3 = new HashMap();
        Map map4 = new HashMap();
        Map map5 = new HashMap();
        Map map6 = new HashMap();
        Map map7 = new HashMap();
        //type:类型（1美妆 2母婴 3服饰 4数码 5食品）
        map1.put("name","全部");
        map1.put("type","");
        map2.put("name","美妆");
        map2.put("type","1");
        map3.put("name","母婴");
        map3.put("type","2");
        map4.put("name","服饰");
        map4.put("type","3");
        map5.put("name","数码");
        map5.put("type","4");
        map6.put("name","食品");
        map6.put("type","5");
        map7.put("name","其他");
        map7.put("type","6");
        result.add(map1);
        result.add(map2);
        result.add(map3);
        result.add(map4);
        result.add(map5);
        result.add(map6);
        result.add(map7);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),result);
    }

    /**
     * 每日爆款商品-复制评论
     **/
    @Override
    public Tip copyComment(String goods_id, Integer user_id, String ifCoupon, String goods_pic,String goods_name,String price,String after_coupon_price) throws Exception{
        String url = tblmPropertyConfig.getUrl();
        String appkey = tblmPropertyConfig.getAppKey();
        String secret = tblmPropertyConfig.getAppSecret();
        JSONObject result = new JSONObject();
        //获取购买url
        Boolean Coupon = false;
        if (!StringUtils.isEmpty(ifCoupon) && ifCoupon.equals("1")){
            Coupon = true;
        }
        Tip tip = tbGoodsService.goodsInfo(goods_id, String.valueOf(user_id), "", Coupon, "1", "1.1.1", "0");
        if(tip.getCode() != 200){
            businessAcademyMapper.updateDailyGoodsStatus(goods_id);
            return null;
        }
        Object obj = tip.getData();
        JSONObject jsonObj = JSON.parseObject(JSON.toJSONString(obj));
        String buyUrl = jsonObj.getString("url");

        TbkTpwdCreateRequest createRequest = new TbkTpwdCreateRequest();
        TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
        createRequest.setText(goods_name);
        createRequest.setUrl(buyUrl);
        createRequest.setLogo(goods_pic);
        TbkTpwdCreateResponse rsp = client.execute(createRequest);
        String body = rsp.getBody();
        JSONObject tpwd = JSON.parseObject(body);
        String model = tpwd.getJSONObject("tbk_tpwd_create_response").getJSONObject("data").getString("model");
        model = model.substring(1,model.length()-1);
        String kl = "";
        if (!StringUtils.isEmpty(ifCoupon) && ifCoupon.equals("1")){
            kl =  "\uD83C\uDF51"+ System.lineSeparator()+"【原价】 ￥" + price + "元" +  System.lineSeparator()
                + "【券后价】 ￥" + after_coupon_price + "元" + System.lineSeparator()
                + "----------------------"+ System.lineSeparator()
                +"覆制这条口令 ("+ model +")，进入【Tao宝】即可抢购✅";
        }else {
            kl =  "\uD83C\uDF51"+ System.lineSeparator()+"【原价】 ￥" + price + "元" +  System.lineSeparator()
                    + "----------------------"+ System.lineSeparator()
                    +"覆制这条口令 ("+ model +")，进入【Tao宝】即可抢购✅";
        }
        result.put("kl",kl);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),result);
    }

    /**
     * 每日爆款商品列表
     **/
    @Override
    public Tip dailyGoods(Integer pageNum,String category,Integer user_id) throws Exception{
        Page page = new Page(pageNum,Constant.PAGE_SIZE_20);
        List<DailyGoodsDTO> list = businessAcademyMapper.dailyGoods(page,category);
        Double reback_money = testMoneyService.getSelftRakeBackRate(user_id);
        list.stream().forEach(t -> {
            String image = t.getImage();
            String price = t.getPrice();
            String coupon = t.getCoupon();
            double after_coupon_price = Double.valueOf(price) - Double.valueOf(coupon);
            Double rake_back = Double.valueOf(t.getRake_back());
            if (!StringUtils.isEmpty(image)){
                JSONArray arr = JSON.parseArray(image);
                t.setImages_list(arr);
            }
            if (rake_back > 0){
                double rake = reback_money * rake_back;
                t.setRake_back(String.format("%.2f",rake));
            }
            String comment =  "\uD83C\uDF51"+ System.lineSeparator()+"【原价】 ￥" + price + "元" +  System.lineSeparator()
                    + "【券后价】 ￥" + String.format("%.2f",after_coupon_price) + "元" + System.lineSeparator()
                    + "----------------------"+ System.lineSeparator()
                    +"覆制这条口令，进入【Tao宝】即可抢购✅";
            t.setComment(comment);
        });
        page.setRecords(list);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),page);
    }

    /**
     * 从好单库获取朋友圈商品推荐(淘宝每日爆款)
     */
    private List getRecommendGoodsInfoByHDK(Integer min_id){
        String apikey = hdkPropertyConfig.getApiKey();
        String hdk_url = hdkPropertyConfig.getMomentsUrl() + "apikey/" + apikey+ "/min_id/" + min_id;
        String dataStr = HttpConnectionPoolUtil.get(hdk_url);
        List list = new ArrayList();
        SimpleDateFormat fmt = Constant.y_M_d;
        if (!StringUtils.isEmpty(dataStr)){
            JSONObject str = JSON.parseObject(dataStr, Feature.DisableSpecialKeyDetect);
            JSONArray datas = str.getJSONArray("data");
            Random random = new Random();
            for (int i = 0; i < datas.size() ; i ++){
                Map map= new HashMap();
                Object obj = datas.get(i);
                String content = JSON.parseObject(JSON.toJSONString(obj)).getString("content");//单品导购内容
                String show_comment = JSON.parseObject(JSON.toJSONString(obj)).getString("show_comment");//导购文案展示内容，含表情
                String copy_content = JSON.parseObject(JSON.toJSONString(obj)).getString("copy_content");
                String itemprice = JSON.parseObject(JSON.toJSONString(obj)).getString("itemprice");//价格
                String couponurl = JSON.parseObject(JSON.toJSONString(obj)).getString("couponurl");
                String tkrates = JSON.parseObject(JSON.toJSONString(obj)).getString("tkrates");//佣金比例
                String couponmoney = JSON.parseObject(JSON.toJSONString(obj)).getString("couponmoney");//优惠券金额
                String itemendprice = JSON.parseObject(JSON.toJSONString(obj)).getString("itemendprice");//	宝贝券后价
                JSONArray itempics = JSON.parseObject(JSON.toJSONString(obj)).getJSONArray("itempic");
                String goods_id = JSON.parseObject(JSON.toJSONString(obj)).getString("itemid");//单品id
                String show_time = JSON.parseObject(JSON.toJSONString(obj)).getString("show_time");//时间戳
                String title = JSON.parseObject(JSON.toJSONString(obj)).getString("title");

                Date date = new Date(new Long(show_time) * 1000);
                String time = fmt.format(date);
//                String cont = copy_content.replaceAll("[$emoji表情]","").replaceAll("\\[.*?\\]","");
                String cont = copy_content.replaceAll("[&lt;br&gt;]","");
                map.put("text",cont);
                map.put("price",itemprice);//价格
                map.put("coupon",couponmoney);//优惠券
                map.put("tkrates",tkrates);//佣金比例
                map.put("biz_id",goods_id);//商品Id
                map.put("goods_name",title);//商品名称
                map.put("create_time",time);
                map.put("type",1);
                map.put("video","");
                map.put("home_image","https://meimg.su.bcebos.com/htsq/htsq_icon.png");
                map.put("title","核桃省钱");
                map.put("transpond_num",random.nextInt(11000)+10000);
                if (!StringUtils.isEmpty(couponurl) && !StringUtils.isEmpty(couponmoney) && Double.valueOf(couponmoney) > 0){
                    map.put("ifcoupon",1);
                }else {
                    map.put("ifcoupon",0);
                }
                int size = itempics.size();
                if (size > 6 && size < 9){
                    size = 5;
                }else if (size > 3 && size < 6){
                    size = 3;
                }
                String[] pictureArr = new String[size];
                for (int k = 0; k < size; k ++){
                    String pic = String.valueOf(itempics.get(k));
                    //由于图片原图过大影响加载速度，建议加上后缀_310x310.jpg
                    pictureArr[k] = pic ;
                }
                map.put("ima_sList",pictureArr);
                list.add(map);
            }
        }
        return list;
    }

    /**
     * 每日爆款商品拉取分类
     */
    private static String goodsCategory(String content){
        String result = "6";
        content = content.replaceAll("[\\r\\n]","");
        //类型（1美妆 2母婴 3服饰 4数码 5食品 6其他）
        //美妆
        String MZ = ".*(彩妆|美妆工具|香水|护肤|面膜|面乳|护肤|面部护肤|身体护理|身体乳|口红|腮红|眉粉|美容|面霜).*";
        //母婴
        String MY = ".*(婴童|儿童玩具|孕产|尿不湿|婴带|童桌|童床|母婴).*";
        //服饰
        String FS = ".*(男装|女装|爸爸装|短裤|中裤|卫衣|背心|套装|POLO衫|衬衫|牛仔裤|休闲裤|T恤|皮裤|毛衣|职业装" +
                "皮衣|大衣|棉衣|羽绒服|风衣|夹克|棉裤|文胸|内裤|袜子|睡衣|塑身衣|内衣|连衣裙|毛针织衫|外套|裤子|衣服" +
                "西装|礼服|打底裤|蕾丝|上衣|吊带|妈妈装|衬衣|半身裙|裙子).*";
        //数码
        String SM = ".*(手机|游戏机|移动电源|相机|电脑|手机配件|路由器|镜头|电脑外设|移动硬盘|摄像机|内存卡|交换机|耳机" +
                "读卡器|电脑硬件|电池|U盘|MP3|学习工具|相机配件|数据线|usb|USB|手机膜).*";
        //食品
        String SP = ".*(谷物|冲饮|零食|肉食|坚果|蜜饯|糕点|饼干|水果|蔬菜|保健滋补|河海|生鲜|茶酒|冲饮|粮油调味|面包|蛋糕|方便面).*";

        boolean MZ_matches = Pattern.matches(MZ, content);
        if (MZ_matches){
            result = "1";
        } else{
            boolean MY_matches = Pattern.matches(MY, content);
            if (MY_matches){
                result = "2";
            }else {
                boolean FS_matches = Pattern.matches(FS, content);
                if (FS_matches){
                    result = "3";
                }else {
                    boolean SM_matches = Pattern.matches(SM, content);
                    if (SM_matches){
                        result = "4";
                    }else {
                        boolean SP_matches = Pattern.matches(SP, content);
                        if (SP_matches){
                            result = "5";
                        }
                    }
                }
            }
        }
        return result;
    }
}
