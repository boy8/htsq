package com.drd.dt.modular.service;

import com.drd.dt.back.dto.SiftOrderLogDTO;
import com.drd.dt.common.tips.Tip;

public interface ISiftOrderLogService {

    /**
     * 添加记录
     */
    Tip addLog(SiftOrderLogDTO siftOrderLogDTO);

    /***
     * 查询售后申请记录
     * **/
    Tip userOrderLogList(Integer user_id);

    /**
     * 补全快递单号
     */
    Tip userParcel(String parcel_number, String parcel_type, Integer id);



}
