package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.OwnGoodsOrderDTO;
import com.drd.dt.modular.entity.OrderGift;
import com.drd.dt.modular.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface OrderGiftService {

    /**
     * 修改订单状态
     */
    public int upOrderGift(Map<String,Object> map) throws Exception;

    /**
     * 根据 订单ID 和 金额查询数据
     */
    public int finOrDerGift( String order_id) throws Exception;

    /**
     * 根据订单号 关联查询获得用户 信息
     * */
    public User getUserDetail( String order_id);

    /**
     *  查询订单详情
     * */
    public OrderGift getOrderGiftDetail( String order_id);

    /**
     * 付款成功 根据订单ID 更改 自营商品  售卖+1 ，库存-1
     * */
    public int upInventorySell( String goods_id);

    public Tip orderTask() ;
}
