package com.drd.dt.modular.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dao.JoinInviteActivityLogMapper;
import com.drd.dt.back.dao.ZeroPurchaseMapper;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.filter.UserThreadLocal;
import com.drd.dt.modular.dao.HomeMapper;
import com.drd.dt.modular.dao.JoinInviteShareLogMapper;
import com.drd.dt.modular.dao.UserMapper;
import com.drd.dt.modular.dao.UserMoneyMapper;
import com.drd.dt.modular.dto.*;
import com.drd.dt.modular.entity.*;
import com.drd.dt.modular.service.HomeService;
import com.drd.dt.modular.service.PDDService;
import com.drd.dt.modular.service.WphService;
import com.drd.dt.properties.HDKPropertyConfig;
import com.drd.dt.properties.TBLMPropertyConfig;
import com.drd.dt.util.*;
import com.google.common.util.concurrent.AtomicDouble;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkSpreadGetRequest;
import com.taobao.api.request.TbkTpwdCreateRequest;
import com.taobao.api.response.TbkSpreadGetResponse;
import com.taobao.api.response.TbkTpwdCreateResponse;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by 86514 on 2019/3/25.
 */
@Service
@Transactional
public class HomeServiceImpl implements HomeService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private UserMoneyServiceImpl testMoneyService;

    @Autowired
    private HomeMapper homeMapper;
    @Autowired
    private HDKPropertyConfig hdkPropertyConfig;
    @Autowired
    private TBLMPropertyConfig tblmPropertyConfig;
    @Autowired
    private JedisClient jedisClient;
    @Autowired
    private TBGoodsServiceImpl tbGoodsService;
    @Autowired
    private PDDService pddService;
    @Autowired
    private WphService wphService;
    @Autowired
    private UserMoneyServiceImpl moneyService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private JoinInviteActivityLogMapper joinInviteActivityLogMapper;
    @Autowired
    private UserMoneyMapper userMoneyMapper;
    @Autowired
    private ZeroPurchaseMapper purchaseMapper;
    @Autowired
    private JoinInviteShareLogMapper joinInviteShareLogMapper;


    /**
     * 首页请求数据合集
     **/
    @Override
    public Tip list(String user_id, String deviceT, String app_version) throws Exception {
        JSONObject result = new JSONObject();
        //获取首页菜单分类
        List<Mall> category = selectCategory(deviceT, app_version);
        //获取首页公告活动列表
        List<ActivityDTO> notice = selectNotice();
        //获取首页底部提现滚动信息
        List<RollMSGDTO> rollList = selectRollList(user_id);
        //获取首页消息红点提示(是否已读（0否；1是）)
        Integer read_status = readStatus(user_id);
        result.put("category", category);
        result.put("notice", notice);
        result.put("roll", rollList);
        result.put("is_read", read_status);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 获取首页消息红点提示
     */
    private Integer readStatus(String user_id) throws Exception {
        Integer is_read = 0;
        if (!StringUtils.isEmpty(user_id)) {
            Integer count = homeMapper.getReadStatus(user_id);
            //是否已读（0否；1是）
            if (null != count && count < 1) {
                return 1;//已读
            }
        }
        return is_read;
    }

    /**
     * 获取首页菜单分类
     */
    private List<Mall> selectCategory(String deviceT, String app_version) throws Exception {
        List<Mall> list = homeMapper.selectCategory();
        Integer a_version = 0;
        if (!StringUtils.isEmpty(app_version) && app_version.contains(".")) {
            String str = app_version.replaceAll("\\.", "");
            if (str.length() > 3) {
                str = str.substring(0, 3);
            }
            a_version = Integer.valueOf(str);
        }
        if (!StringUtils.isEmpty(deviceT)) {
            if ((deviceT.equals("0") && a_version < 135) || (deviceT.equals("1") && a_version < 209)) {
                list = list.stream().filter(t -> !t.getDiscern_id().equals(13)).collect(Collectors.toList());
            }
        }
        return list;
    }

    /**
     * 获取首页公告活动列表
     */
    private List<ActivityDTO> selectNotice() throws Exception {
        List<ActivityDTO> list = homeMapper.selectNotice();
        return list;
    }

    /**
     * 获取首页底部滚动信息
     */
    private List<RollMSGDTO> selectRollList(String user_id) throws Exception {
        List<RollMSGDTO> result = new ArrayList<>();
        List<RollMSGDTO> txList = homeMapper.getSYSShowList(user_id);//系统消息
        //根据type拼接系统消息的内容
        //0 全局系统  1,自己注册 2,邀请粉丝成功，有新粉丝注册  3,自己VIP升级 4提现的
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat y_m_d_h_m_s = Constant.y_M_d_H_m_s;
        txList.stream().forEach(t -> {
            Integer type = t.getType();
            String nick_name = t.getNick_name();
            Integer mType = 0;
            if (type == 3) {//自己VIP升级
                try {
                    mType = 4;
                    cal.setTime(y_m_d_h_m_s.parse(t.getCreate_time()));
                    cal.add(Calendar.YEAR, 1);
                    t.setCreate_time(y_m_d_h_m_s.format(cal.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (type == 1) {//自己注册
                nick_name = "";
            }
            if (type != 0) {
                String messageText = moneyService.getMessageText(nick_name, t.getCreate_time(), "", mType);
                t.setContent(messageText);
            }
        });

        result.addAll(txList);
        if (!StringUtils.isEmpty(user_id)) {//用户id不为空时获取佣金消息
            List<RollMSGDTO> commissionList = homeMapper.getcommissionList(user_id);//佣金消息
            if (null != commissionList) {
                commissionList.stream().forEach(t -> {
                    //佣金类型 1直接销售收益，2粉丝收益，3奖励收益 ，4退款扣除
                    Integer message_type = t.getMessage_type();
                    String rake_money = t.getRake_money();
                    String create_time = t.getCreate_time();
                    RollMSGDTO rollMSGDTO = new RollMSGDTO();
                    String msg = "";
                    if (message_type == 1) {
                        rollMSGDTO.setTitle(Constant.SYS_COMISSION);
                        msg = "普通预估佣金";
                    } else if (message_type == 3) {
                        rollMSGDTO.setTitle(Constant.SYS_BOUNTY);
                        msg = "奖励佣金";
                    }
                    rollMSGDTO.setContent(" 您有一笔" + msg + rake_money + "元成功入账，请到消息中心查看详情");
                    rollMSGDTO.setCreate_time(create_time);
                    result.add(rollMSGDTO);
                });
            }
        }
        StringBuilder resultContent = new StringBuilder();//将content内容拼接为一条，方便前端展示
        final Integer[] flag = {0};
        result.stream().forEach(t -> {
            String content = t.getContent();
            if (!StringUtils.isEmpty(content)) {
                if (flag[0] == 0) {
                    resultContent.append(content);
                } else {
                    resultContent.append("  " + content);
                }
            }
            flag[0]++;
        });
        result.clear();
        RollMSGDTO dto = new RollMSGDTO();
        dto.setContent(String.valueOf(resultContent));
        result.add(dto);
        return result;
    }

    /**
     * 获取开屏或banner图
     **/
    public Tip getAds(Integer block, String imei, HttpServletRequest request, String deviceT, String app_version) throws Exception {
        JSONObject result = new JSONObject();
        JSONArray openScreen = new JSONArray();
        JSONArray homeBanner = new JSONArray();
        JSONArray gif = new JSONArray();
        JSONArray sign = new JSONArray();
        JSONArray floatImg = new JSONArray();
        JSONArray personal = new JSONArray();
        JSONArray activity = new JSONArray();
        JSONArray activity_bg = new JSONArray();
        JSONArray insertScreen = new JSONArray();
        JSONArray brand = new JSONArray();//品牌导航分类模块
        String time = Constant.y_M_d.format(new Date());
        Object value = jedisClient.get("htsq:adv:" + imei);
        Integer a_version = 0;
        if (!StringUtils.isEmpty(app_version) && app_version.contains(".")) {
            a_version = Integer.valueOf(app_version.replaceAll("\\.", ""));
        }
        Integer is_read = 0;
        String rTime;
        if (null != value) {
            rTime = String.valueOf(value);
            if (!StringUtils.isEmpty(rTime) && rTime.equals(time)) {//今天已看过插屏
                is_read = 1;
            } else {
                if (!StringUtils.isEmpty(imei)) {
                    jedisClient.setWithExpire("htsq:adv:" + imei, time, 60 * 60 * 24);
                }
            }
        } else {//redis没有时
            if (!StringUtils.isEmpty(imei)) {
                jedisClient.setWithExpire("htsq:adv:" + imei, time, 60 * 60 * 24);
            }
        }
        List<Adv> list = homeMapper.getAds(block);
        //判断是否为国外ip
        String ip = StringUtil.getIpAddr(request);
        String country = new StringUtil().getIPLocation(ip);
        String version = userMapper.getVerifyVersion();//获取ios审核版本号
        Integer finalIs_read = is_read;
        Integer finalA_version = a_version;
        list.forEach(t -> {
            Integer blockType = t.getBlock();
            //1首页banner 2开屏 3首页gif 4签到 5浮图 6个人中心banner 7活动专区 8首页活动区背景图 9插屏 11品牌导航分类模块
            switch (blockType) {
                case 1:
                    if ((!StringUtils.isEmpty(app_version) && !StringUtils.isEmpty(deviceT) && deviceT.equals("1")
                            && app_version.equals(version)) || country.equals("美国")) {
                        String name = t.getName();
                        if (!StringUtils.isEmpty(name) && !name.contains("零元购")) {
                            break;
                        }
                    }
                    homeBanner.add(t);
                    break;
                case 2:
                    openScreen.add(t);
                    break;
                case 3:
                    gif.add(t);
                    break;
                case 4:
                    sign.add(t);
                    break;
                case 5:
                    if (!StringUtils.isEmpty(app_version) && !app_version.equals(version) && !country.equals("美国")) {
                        if (StringUtils.isEmpty(deviceT) || deviceT.equals("0") || (deviceT.equals("1") && finalA_version > 204)) {
                            floatImg.add(t);
                        }
                    }
                    break;
                case 6:
                    personal.add(t);
                    break;
                case 7:
                    activity.add(t);
                    break;
                case 8:
                    activity_bg.add(t);
                    break;
                case 9:
                    if (finalIs_read == 0 && !app_version.equals(version) && !country.equals("美国")) {
                        insertScreen.add(t);
                    }
                    break;
                case 11:
                    String name = t.getName();
                    if (!StringUtils.isEmpty(deviceT) && !StringUtils.isEmpty(app_version) && (deviceT.equals("1") && version.equals(app_version))) {
                        if (!StringUtils.isEmpty(name)) {
                            if (name.equals("医疗防护") || name.equals("高佣爆款")) {
                                t.setJump_url("http://p11.quanbashi.vip/#/boutique");
                                t.setType(0);
                                t.setJump_type(4);
                                t.setIs_need_userid(1);
                            } else if (name.equals("生活优惠")) {
                                t.setJump_url("http://p11.quanbashi.vip/#/zero");
                                t.setType(0);
                                t.setJump_type(4);
                                t.setIs_need_userid(1);
                            } else if (name.equals("网络返佣")) {
                                t.setJump_url("http://p11.quanbashi.vip/#/boutique");
                                t.setType(0);
                                t.setJump_type(4);
                                t.setIs_need_userid(1);
                            }
                        }
                    }
                    brand.add(t);
                    break;
            }
        });
        result.put("homeBanner", homeBanner);
        result.put("openScreen", openScreen);
        result.put("gif", gif);
        result.put("sign", sign);
        result.put("floatImg", floatImg);
        result.put("personal", personal);
        result.put("activity", activity);
        result.put("activityBG", activity_bg);
        result.put("insertScreen", insertScreen);
        result.put("brand", brand);

        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 限时秒杀时间点
     **/
    @Override
    public Tip seckillTime() throws Exception {
        JSONObject result = new JSONObject();
        List<SeckillTimeDTO> seckills = homeMapper.selectSeckillTime();
        SimpleDateFormat H_m_s = Constant.H_m_s;
        SimpleDateFormat y_M_d_H_m_s = Constant.y_M_d_H_m_s;
        SimpleDateFormat y_M_d = Constant.y_M_d;
        Date date = new Date();
        String format = H_m_s.format(date);
        Date now = H_m_s.parse(format);
        SimpleDateFormat H_m = Constant.H_m;
        final Integer[] flag = {0};
        final Long[] sec = {0L};
        final Integer[] sequence = {0};
        final Integer[] type = new Integer[1];
        String StrYMD = y_M_d.format(date);
        Date lastTime = y_M_d_H_m_s.parse(StrYMD + " " + seckills.get(seckills.size() - 1).getSeckill_time());//最后一个秒杀时间
        if (date.getTime() >= lastTime.getTime()) {
            sequence[0] = seckills.get(seckills.size() - 1).getSequence();
            type[0] = seckills.get(seckills.size() - 1).getType();
            Date firstTime = y_M_d_H_m_s.parse(StrYMD + " " + seckills.get(0).getSeckill_time());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(firstTime);
            int day = calendar.get(Calendar.DATE);
            calendar.set(Calendar.DATE, day + 1);
            sec[0] = calendar.getTime().getTime() - date.getTime();
        }
        seckills.stream().forEach(t -> {
            String seckill_time = t.getSeckill_time();
            try {
                Date seckill = H_m_s.parse(seckill_time);
                if (seckill.getTime() >= now.getTime()) {//大于等于现在时间（即将开始）
                    if (flag[0] == 0) {
                        sequence[0] = t.getSequence() - 1;//获取抢购中的时间点
                        sec[0] = (seckill.getTime() - now.getTime());
                        type[0] = t.getType();
                    }
                    t.setDesc(Constant.INSTANTLY_OPEN);
                    flag[0]++;
                } else {
                    t.setDesc(Constant.IS_OPENED);
                }
                t.setSeckill_time(H_m.format(seckill));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
        seckills.stream().filter(t -> t.getSequence() == sequence[0]).forEach(p -> p.setDesc(Constant.IS_OPENING));
        result.put("seckillGoods", seckills);
        result.put("seckillTime", sec[0]);
        result.put("type", type[0]);
        result.put("seckillDesc", Constant.SECKILL_DESC);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    @Override
    public Tip seckillTimeAndSiftGoods() throws Exception {
        Tip tip = seckillTime();
        JSONObject result = null;
        // 限时秒杀时间点
        if (null != tip && null != tip.getData()) {
            result = (JSONObject) tip.getData();
        }
        // 获取旁边的 核桃精选图片 等信息
        List<Map> banner = homeMapper.getHomeseckillTimeAndSiftGoodsBanner();
        if (null != banner && banner.size() > 0) {
            result.put("siftGoodsBanner", banner.get(0));
        } else {
            result.put("siftGoodsBanner", null);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 限时秒杀商品列表
     **/
    @Override
    public Tip seckillGoods(String page, String type, Integer user_id) throws Exception {
        if (StringUtils.isEmpty(type)) {
            type = "7";
        }
        JSONArray result = new JSONArray();
        String apiKey = hdkPropertyConfig.getApiKey();
        String hdk_url = hdkPropertyConfig.getFastbuyUrl() + "apikey/" + apiKey + "/hour_type/" + type + "/min_id/" + page;
        String dataStr = HttpConnectionPoolUtil.get(hdk_url);
        if (!StringUtils.isEmpty(dataStr)) {
            JSONObject dataJson = JSON.parseObject(dataStr);
            Integer code = dataJson.getInteger("code");
            if (code == 1) {
                JSONArray datas = dataJson.getJSONArray("data");
                Double reback_money = testMoneyService.getSelftRakeBackRate(user_id);
                Random random = new Random();
                datas.stream().forEach(t -> {
                    JSONObject data = (JSONObject) t;
                    SeckillGoodsDTO seckillGoodsDTO = new SeckillGoodsDTO();
                    seckillGoodsDTO.setType(1);
                    seckillGoodsDTO.setGoodsName(data.getString("itemtitle"));
                    seckillGoodsDTO.setGoodsId(data.getString("itemid"));
                    seckillGoodsDTO.setGoodsMainPicture(data.getString("itempic") + "_310x310.jpg");
                    seckillGoodsDTO.setGoodsDesc(data.getString("itemdesc"));
                    String sale = data.getString("itemsale");
                    if (!StringUtils.isEmpty(sale) && Integer.valueOf(sale) == 0) {
                        sale = String.valueOf(random.nextInt(3000) + 2000);
                    }
                    seckillGoodsDTO.setSales(sale);//宝贝近2小时跑单
                    seckillGoodsDTO.setCoupon(data.getString("couponmoney"));
                    seckillGoodsDTO.setActivity_type(data.getString("activity_type"));
                    seckillGoodsDTO.setCouponUrl(data.getString("couponurl"));
                    String itemendprice = data.getString("itemendprice");
                    seckillGoodsDTO.setPriceAfterCoupon(itemendprice);
                    seckillGoodsDTO.setPrice(data.getString("itemprice"));

                    String time = Constant.Hm.format(new Date());

                    int ran = random.nextInt(6);
                    Double rate = (Double.valueOf(time) / 100 + ran) * 3.4 / 100;
                    Double num = Integer.valueOf(sale) / rate;
                    seckillGoodsDTO.setTotalNum(String.format("%.0f", num));
                    Long couponstarttime = data.getLong("couponstarttime");
                    Long couponendtime = data.getLong("couponendtime");
                    SimpleDateFormat y_m_d = Constant.y_M_d;
                    if (!StringUtils.isEmpty(couponstarttime)) {
                        String couponstart = y_m_d.format(new Date(couponstarttime));
                        seckillGoodsDTO.setCouponStartTime(couponstart);
                    }
                    if (!StringUtils.isEmpty(couponendtime)) {
                        String couponend = y_m_d.format(new Date(couponendtime));
                        seckillGoodsDTO.setCouponStartTime(couponend);
                    }

                    Double tkrates = data.getDouble("tkrates");
                    Double commission = Double.valueOf(itemendprice) * (tkrates / 100) * (reback_money);
                    seckillGoodsDTO.setCommission(String.format("%.2f", commission));


                    result.add(seckillGoodsDTO);
                });
            }
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 参数返回(初始化)
     **/
    @Override
    public Tip stuad(StuadDTO stuadDTO, HttpServletRequest request) throws Exception {
        InitConfig config = homeMapper.selectInitConfigInfo();//获取初始化配置
        String imei = stuadDTO.getImei();
        Integer deviceT = stuadDTO.getDeviceT();
        String app_version = stuadDTO.getApp_version();
        String channel_id = stuadDTO.getChannel_id();
        String api_url = config.getApi_url();
        String remind_version = config.getRemind_version();//ios提示升级版本号
        if (!StringUtils.isEmpty(deviceT) && deviceT == 1) {//ios
            stuadDTO.setChannel_id("ios");
            if (!StringUtils.isEmpty(remind_version) && !StringUtils.isEmpty(app_version) && app_version.equals(remind_version)) {
                config.setRemind_promotion(1);
            }
        }

        String url = "";
        if (!StringUtils.isEmpty(api_url)) {//随机地址
            List<String> urls = new ArrayList<>();
            JSONObject json = JSON.parseObject(api_url);
            urls.addAll(json.keySet());
            Random r = new Random();
            String s = urls.get(r.nextInt(urls.size()));
            url = json.getString(s);
        }
        config.setApi_url(url);
        SimpleDateFormat sd = Constant.y_M_d_H_m_s;
        String time = sd.format(new Date());
        Integer userId = stuadDTO.getUserId();
        if (StringUtils.isEmpty(userId)) {
            userId = stuadDTO.getUser_id();
        }
        if (!StringUtils.isEmpty(userId)) {//更新用户最近登录时间
            User user = new User();
            user.setId(userId);
            user.setRecent_login_time(time);
            if (!StringUtils.isEmpty(app_version)) {
                user.setApp_version(app_version);
            }
            if (!StringUtils.isEmpty(channel_id)) {
                user.setChannel_id(channel_id);
            }
            userMapper.updateById(user);
        }
        //不为空时，插入channel表
        if (!StringUtils.isEmpty(stuadDTO.getChannel_id()) && !StringUtils.isEmpty(imei)) {
            List<StuadDTO> list = homeMapper.findByIMEI(stuadDTO);
            stuadDTO.setCreate_time(time);
            SimpleDateFormat sdf = Constant.y_M_d;
            String now = sdf.format(new Date());
            if (list.size() > 0) {
                List<StuadDTO> collect = list.stream().filter(t -> now.equals(t.getCreate_time())).collect(Collectors.toList());
                if (collect.size() < 1) {
                    stuadDTO.setIs_new(0);
                    homeMapper.insertChannel(stuadDTO);
                }
            } else {
                stuadDTO.setIs_new(1);
                homeMapper.insertChannel(stuadDTO);
            }
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), config);
    }

    /**
     * 好单库获取热搜关键词
     */
    @Override
    public Tip hotSearch() {
        Object value = jedisClient.get("htsq:hotSearch:keywords");
        List list = new ArrayList<>();
        if (null == value) {//redis数据为空时，从好单库获取热搜关键词
            String apikey = hdkPropertyConfig.getApiKey();
            String hdk_url = hdkPropertyConfig.getHotUrl() + "apikey/" + apikey;
            String dataStr = HttpConnectionPoolUtil.get(hdk_url);
            if (!StringUtils.isEmpty(dataStr)) {
                JSONObject str = JSON.parseObject(dataStr);
                JSONArray datas = str.getJSONArray("data");
                int size = 0;
                if (null != datas) {
                    size = datas.size() >= 20 ? 20 : datas.size();
                }
                for (int i = 0; i < size; i++) {//获取前20条
                    Object obj = datas.get(i);
                    String keyword = JSON.parseObject(JSON.toJSONString(obj)).getString("keyword");
                    list.add(keyword);
                }
                jedisClient.setWithExpire("htsq:hotSearch:keywords", JSON.toJSONString(list), 60 * 60 * 24);
            }
        } else {
            String str = JSON.toJSONString(value).replaceAll("\"", "").replaceAll("\\\\", "").replace("[", "").replace("]", "");
            String[] split = str.split(",");
            list = Arrays.asList(split);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);
    }

    /**
     * 商品分享链接
     **/
    @Override
    public Tip shareGoods(Integer type, String goods_id, String goods_name, Integer user_id, HttpServletRequest request) throws Exception {
        JSONObject json = new JSONObject();
        //类型:1淘宝 2京东 3拼多多 4唯品会
        if (!StringUtils.isEmpty(type) && type == 1) {//淘宝
            json = getTaoBaoPWD(goods_id, user_id, request);
        } else if (!StringUtils.isEmpty(type) && type == 2) {//2京东(京东暂无分享)

        } else if (!StringUtils.isEmpty(type) && type == 3) {//3拼多多
            Tip pddUrl = pddService.getPddUrl(String.valueOf(user_id), goods_id, "1");
            Object data = pddUrl.getData();
            if (null != data) {
                String friend_code;
                User user = UserThreadLocal.get();
                if (null != user) {
                    friend_code = user.getFriend_code();
                } else {
                    User u = userMapper.selectById(user_id);
                    friend_code = u.getFriend_code();
                }
                JSONObject detail = (JSONObject) JSON.parseArray(JSON.toJSONString(data)).get(0);
                String goodsUrl = detail.getString("short_url");
                String command = "【" + goods_name + "】" + goodsUrl + "点击链接，再选择浏览器打开；或复制这条信息，打开手机拼多多";
                json.put("result", command);
                json.put("url", goodsUrl);
                json.put("urlDesc", goodsUrl + "点击链接，再选择浏览器打开；或复制这条信息，打开手机拼多多");
                json.put("kl", "");
                json.put("friend_code", friend_code);
            }
        } else if (!StringUtils.isEmpty(type) && type == 4) {
            Object ob = wphService.wphGenerateLink(user_id, goods_id, 1).getData();
            json = (JSONObject) ob;
        }
        if (StringUtils.isEmpty(json)) {
            return ResultUtil.result(BizExceptionEnum.GOODS_INFO_IS_NULL.getCode(), BizExceptionEnum.GOODS_INFO_IS_NULL.getMessage());
        }
        json.put("desc", "点击打开购买链接");
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), json);
    }

    /**
     * 跳转时需转换的链接
     **/
    @Override
    public Tip exchangeUrl(Integer discern_id, Integer user_id, String theme_id) throws Exception {
        if (StringUtils.isEmpty(discern_id)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        Tip tip = new Tip();
        if (discern_id == 7) {//拼多多每日红包
            tip = pddService.pddRedpacket(String.valueOf(user_id));
        } else if (discern_id == 11) {//拼多多主题推广
            tip = pddService.pddThemeUrl(String.valueOf(user_id), Long.valueOf(theme_id));
        }
        return tip;
    }

    /**
     * 获取视频教程链接
     **/
    @Override
    public Tip course() {
        List<ImageConfig> list = homeMapper.getCourse();
        JSONObject result = new JSONObject();
        list.stream().forEach(t -> {
            Integer type = t.getType();
            String file = t.getFile();
            if (type == 3) {//视频教程
                result.put("video_url", file);
            } else if (type == 4) {//第一帧图片
                result.put("pic_url", file);
            }
        });
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 零元购商品列表
     **/
    @Override
    public Tip zeroList(Integer user_id, String token_id, Integer pageNum) throws Exception {
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        Page page = new Page(pageNum, Constant.PAGE_SIZE_20);
        List<ZeroPurchaseDTO> result = homeMapper.selectZeroList(page);
        result.stream().forEach(t -> {
            String coupon_endTime = t.getCoupon_endTime();
            if (!StringUtils.isEmpty(coupon_endTime)) {
                try {
                    Date date = DateFormat.getDateInstance().parse(coupon_endTime);
                    Date now = new Date();
                    if (date.getTime() <= now.getTime()) {//当券结束时间小于等于现在时间时，把券价格设置为0
                        t.setPrice_coupons("0");
                        t.setIfcoupon(0);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        page.setRecords(result);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), page);
    }

    /**
     * 首页618活动链接信息
     **/
    @Override
    public Tip activity618(Integer user_id, String token_id, String type, String title) throws Exception {
        if (StringUtils.isEmpty(user_id) || StringUtils.isEmpty(token_id)) {
            return ResultUtil.result(BizExceptionEnum.PARAM_EMPTY.getCode(), BizExceptionEnum.PARAM_EMPTY.getMessage());
        }
        JSONObject result = new JSONObject();
        String appkey = tblmPropertyConfig.getAppKey();
        String secret = tblmPropertyConfig.getAppSecret();
        String tburl = tblmPropertyConfig.getUrl();
        StringBuffer url = get618Info(type);
        User user = UserThreadLocal.get();
        String relation_id;
        if (null != user) {
            relation_id = user.getTaobao_relation_id();
        } else {
            User u = userMapper.selectById(user_id);
            relation_id = u.getTaobao_relation_id();
        }
        if (!StringUtils.isEmpty(relation_id)) {
            url.append("&relationId=").append(relation_id);
        }
        if (type.equals("11")) {
            result.put("url", "https://s.click.taobao.com/ZfPAHvu");
            result.put("tkl", "(覆置文本￥J3Y2cibvfRt$达开桃宝或點击链街 https://m.tb.cn/h.4Y09zCO 至流览器【双11超级红 鉋】)");
        }else {
            TaobaoClient client = new DefaultTaobaoClient(tburl, appkey, secret);
            TbkTpwdCreateRequest createRequest = new TbkTpwdCreateRequest();
            createRequest.setText(title);
            createRequest.setUrl(url.toString());
            TbkTpwdCreateResponse rsp = client.execute(createRequest);
            String body = rsp.getBody();
            JSONObject tpwd = JSON.parseObject(body);
            String model = tpwd.getJSONObject("tbk_tpwd_create_response").getJSONObject("data").getString("model");
            model = model.substring(1);
            model = model.substring(0, model.length() - 1);
            result.put("url", url.toString());
            result.put("tkl", "(" + model + ")");
        }
        result.put("relation_id", relation_id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 618活动跳转时转换链接
     **/
    @Override
    public Tip activity618Change(Integer discern_id, Integer user_id, String type, String title) {
        User user = UserThreadLocal.get();
        String token;
        if (null != user) {
            token = user.getToken();
        } else {
            User u = userMapper.selectById(user_id);
            token = u.getToken();
        }
        String url = "http://p11.quanbashi.vip/#/activity_area11?type=" + type + "&title=" + title + "&user_id=" + user_id + "&token_id=" + token;
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), url);
    }

    /**
     * 获取淘口令
     **/
    @Override
    public String exchangeTkl(String url, String title, String img) throws Exception {
//        logger.info("{开始淘口令获取}");
        String t = jedisClient.get("htsq:tkl");
        if (!StringUtils.isEmpty(t)) {
//            logger.info("{从redis获取口令}");
            return t;
        }

        String appkey = tblmPropertyConfig.getAppKey();
        String secret = tblmPropertyConfig.getAppSecret();
        String tburl = tblmPropertyConfig.getUrl();
        if (StringUtils.isEmpty(url)) {
            url = "https://s.click.taobao.com/VOiwHhv";
        }
        if (StringUtils.isEmpty(img)) {
            img = "https://gw.alicdn.com/tfs/TB1alz7HYY1gK0jSZTEXXXDQVXa-800-450.jpg";
        }
        if (StringUtils.isEmpty(title)) {
            title = "2020年天猫618—天猫主会场";
        }
        TaobaoClient client = new DefaultTaobaoClient(tburl, appkey, secret);
        TbkTpwdCreateRequest createRequest = new TbkTpwdCreateRequest();
        createRequest.setText(title);
        createRequest.setLogo(img);
        createRequest.setUrl(url);
        TbkTpwdCreateResponse rsp = client.execute(createRequest);
        String body = rsp.getBody();
        JSONObject tpwd = JSON.parseObject(body);
        String model = tpwd.getJSONObject("tbk_tpwd_create_response").getJSONObject("data").getString("model");
        if (!StringUtils.isEmpty(model)) {
            jedisClient.setWithExpire("htsq:tkl", model, 60);
        }
//        logger.info("{淘口令获取结束}");
        return model;
    }

    /**
     * 获取淘口令
     *
     * @return
     */
    private JSONObject getTaoBaoPWD(String goodsId, Integer user_id, HttpServletRequest request) throws Exception {
        String url = tblmPropertyConfig.getUrl();
        String appkey = tblmPropertyConfig.getAppKey();
        String secret = tblmPropertyConfig.getAppSecret();
        JSONObject result = new JSONObject();
        //通过商品id获取淘宝商品详情
//        String strData = tbGoodsService.getGoodsDetail(goodsId);
        Tip detail_tip = tbGoodsService.goodsDetail(goodsId, user_id, request, "0", "0.0.1");
        Object detail_obj = detail_tip.getData();
        String strData = JSON.toJSONString(detail_obj);

        if (!StringUtils.isEmpty(strData)) {
            JSONObject detail = JSONObject.parseObject(strData);
//            JSONObject detail = data.getJSONObject("data");
            String itempic = detail.getString("goodsMainPicture");//图片
            String itemtitle = detail.getString("goodsName");//商品名称
            String couponmoney = detail.getString("coupon");//券额
            String price = detail.getString("price");//原价
            String itemendprice = detail.getString("priceAfterCoupon");//券后价
            String commission = detail.getString("commission");//返利
            Boolean ifCoupon = false;
            if (!StringUtils.isEmpty(couponmoney) && Double.valueOf(couponmoney) > 0) {
                ifCoupon = true;
            }
            //获取购买url
            Tip tip = tbGoodsService.goodsInfo(goodsId, String.valueOf(user_id), "", ifCoupon, "1", "1.1.1", "0");
            if (tip.getCode() != 200) {
                return null;
            }
            Object obj = tip.getData();
            JSONObject jsonObj = JSON.parseObject(JSON.toJSONString(obj));
            String buyUrl = jsonObj.getString("url");

            TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
            //转换为短链接
            TbkSpreadGetRequest req = new TbkSpreadGetRequest();
            List<TbkSpreadGetRequest.TbkSpreadRequest> list2 = new ArrayList<>();
            TbkSpreadGetRequest.TbkSpreadRequest spreadRequest = new TbkSpreadGetRequest.TbkSpreadRequest();
            spreadRequest.setUrl(buyUrl);
            list2.add(spreadRequest);
            req.setRequests(list2);
            TbkSpreadGetResponse response = client.execute(req);
            String shortUrl = response.getResults().get(0).getContent();

            TbkTpwdCreateRequest createRequest = new TbkTpwdCreateRequest();
            createRequest.setText(itemtitle);
            createRequest.setUrl(buyUrl);
            createRequest.setLogo(itempic);
            TbkTpwdCreateResponse rsp = client.execute(createRequest);
            String body = rsp.getBody();
            JSONObject tpwd = JSON.parseObject(body);
//            String model = tpwd.getJSONObject("tbk_tpwd_create_response").getJSONObject("data").getString("model");
            String model = tpwd.getJSONObject("tbk_tpwd_create_response").getJSONObject("data").getString("password_simple");
            model = model.substring(1);
            model = model.substring(0, model.length() - 1);
            if (!StringUtils.isEmpty(shortUrl)) {
                buyUrl = shortUrl;
            }
            String str = "";
            String goodsInfo = "";
            if (ifCoupon) {
                goodsInfo = itemtitle + "\uD83D\uDC47\uD83D\uDC47\uD83D\uDC47" + System.lineSeparator() + "【在售价】￥" + price + System.lineSeparator()
                        + "【券后价】￥" + itemendprice + System.lineSeparator();
                str = goodsInfo + "---------\uD83C\uDF51--------" + System.lineSeparator() + "覆制这段内容(" + model + ") 咑tao宝即可领券✅";
            } else {
                goodsInfo = itemtitle + "\uD83D\uDC47\uD83D\uDC47\uD83D\uDC47" + System.lineSeparator() + "【在售价】￥" + price + System.lineSeparator();
                str = goodsInfo + "--------\uD83C\uDF51---------" + System.lineSeparator() + "覆制这段内容(" + model + ")咑开tao宝即可✅";
            }

            String friend_code;
            User user = UserThreadLocal.get();
            if (null != user) {
                friend_code = user.getFriend_code();
            } else {
                User u = userMapper.selectById(user_id);
                friend_code = u.getFriend_code();
            }
            result.put("result", str);
            result.put("url", buyUrl);
            result.put("urlDesc", "");
            result.put("kl", "覆制这段内容(" + model + ")咑开tao宝即可✅");
            result.put("friend_code", friend_code);
            result.put("goodsInfo", goodsInfo);
        }
        return result;
    }

    /**
     * 分类
     **/
    @Override
    public Tip stulist() throws Exception {
        JSONObject result = new JSONObject();
        JSONArray mylist = new JSONArray();

        JSONObject mylist1 = new JSONObject(true);
        List<String> chlist1 = new ArrayList<>();
        List<String> urllist1 = new ArrayList<>();

        JSONObject mylist2 = new JSONObject(true);
        List<String> chlist2 = new ArrayList<>();
        List<String> urllist2 = new ArrayList<>();

        JSONObject mylist3 = new JSONObject(true);
        List<String> chlist3 = new ArrayList<>();
        List<String> urllist3 = new ArrayList<>();

        JSONObject mylist4 = new JSONObject(true);
        List<String> chlist4 = new ArrayList<>();
        List<String> urllist4 = new ArrayList<>();

        JSONObject mylist5 = new JSONObject(true);
        List<String> chlist5 = new ArrayList<>();
        List<String> urllist5 = new ArrayList<>();

        JSONObject mylist6 = new JSONObject(true);
        List<String> chlist6 = new ArrayList<>();
        List<String> urllist6 = new ArrayList<>();

        JSONObject mylist7 = new JSONObject(true);
        List<String> chlist7 = new ArrayList<>();
        List<String> urllist7 = new ArrayList<>();

        JSONObject mylist8 = new JSONObject(true);
        List<String> chlist8 = new ArrayList<>();
        List<String> urllist8 = new ArrayList<>();

        JSONObject mylist9 = new JSONObject(true);
        List<String> chlist9 = new ArrayList<>();
        List<String> urllist9 = new ArrayList<>();

        JSONObject mylist10 = new JSONObject(true);
        List<String> chlist10 = new ArrayList<>();
        List<String> urllist10 = new ArrayList<>();

        JSONObject mylist11 = new JSONObject(true);
        List<String> chlist11 = new ArrayList<>();
        List<String> urllist11 = new ArrayList<>();

        JSONObject mylist12 = new JSONObject(true);
        List<String> chlist12 = new ArrayList<>();
        List<String> urllist12 = new ArrayList<>();

        JSONObject mylist13 = new JSONObject(true);
        List<String> chlist13 = new ArrayList<>();
        List<String> urllist13 = new ArrayList<>();

        JSONObject mylist14 = new JSONObject(true);
        List<String> chlist14 = new ArrayList<>();
        List<String> urllist14 = new ArrayList<>();

        JSONObject mylist15 = new JSONObject(true);
        List<String> chlist15 = new ArrayList<>();
        List<String> urllist15 = new ArrayList<>();

        JSONObject mylist16 = new JSONObject(true);
        List<String> chlist16 = new ArrayList<>();
        List<String> urllist16 = new ArrayList<>();

        JSONObject mylist17 = new JSONObject(true);
        List<String> chlist17 = new ArrayList<>();
        List<String> urllist17 = new ArrayList<>();

        JSONObject mylist18 = new JSONObject(true);
        List<String> chlist18 = new ArrayList<>();
        List<String> urllist18 = new ArrayList<>();

        JSONObject mylist19 = new JSONObject(true);
        List<String> chlist19 = new ArrayList<>();
        List<String> urllist19 = new ArrayList<>();

        chlist1.add("婴童服饰");
        chlist1.add("玩具书籍");
        chlist1.add("婴童寝居");
        chlist1.add("婴童洗护");
        chlist1.add("喂养用品");
        chlist1.add("出行产品");
        chlist1.add("孕产用品");
        chlist1.add("尿不湿");
        chlist1.add("婴童湿巾");
        chlist1.add("妈咪包背婴带");
        chlist1.add("安全座椅");
        chlist1.add("童桌童床");
        urllist1.add("https://meimg.su.bcebos.com/img/1.jpg");
        urllist1.add("https://meimg.su.bcebos.com/img/2.png");
        urllist1.add("https://meimg.su.bcebos.com/img/3.jpg");
        urllist1.add("https://meimg.su.bcebos.com/img/4.png");
        urllist1.add("https://meimg.su.bcebos.com/img/5.jpg");
        urllist1.add("https://meimg.su.bcebos.com/img/6.jpg");
        urllist1.add("https://meimg.su.bcebos.com/img/7.jpg");
        urllist1.add("https://meimg.su.bcebos.com/img/8.jpg");
        urllist1.add("https://meimg.su.bcebos.com/img/9.jpg");
        urllist1.add("https://meimg.su.bcebos.com/img/10.png");
        urllist1.add("https://meimg.su.bcebos.com/img/11.png");
        urllist1.add("https://meimg.su.bcebos.com/img/12.png");
        mylist1.put("name", "母婴");
        mylist1.put("chlist", chlist1);
        mylist1.put("urllist", urllist1);

        chlist2.add("谷物冲饮");
        chlist2.add("休闲零食");
        chlist2.add("精选肉食");
        chlist2.add("坚果蜜饯");
        chlist2.add("糕点饼干");
        chlist2.add("水果蔬菜");
        chlist2.add("保健滋补");
        chlist2.add("河海生鲜");
        chlist2.add("茶酒冲饮");
        chlist2.add("粮油调味");
        urllist2.add("https://meimg.su.bcebos.com/img/13.png");
        urllist2.add("https://meimg.su.bcebos.com/img/14.png");
        urllist2.add("https://meimg.su.bcebos.com/img/15.png");
        urllist2.add("https://meimg.su.bcebos.com/img/16.png");
        urllist2.add("https://meimg.su.bcebos.com/img/17.png");
        urllist2.add("https://meimg.su.bcebos.com/img/18.png");
        urllist2.add("https://meimg.su.bcebos.com/img/19.jpg");
        urllist2.add("https://meimg.su.bcebos.com/img/20.png");
        urllist2.add("https://meimg.su.bcebos.com/img/21.png");
        urllist2.add("https://meimg.su.bcebos.com/img/22.png");
        mylist2.put("name", "食品");
        mylist2.put("chlist", chlist2);
        mylist2.put("urllist", urllist2);

        chlist3.add("高跟鞋");
        chlist3.add("运动鞋");
        chlist3.add("靴子");
        chlist3.add("帆布鞋");
        chlist3.add("低帮鞋");
        chlist3.add("雨鞋");
        chlist3.add("高帮鞋");
        chlist3.add("凉鞋");
        chlist3.add("拖鞋");
        for (int i = 23; i <= 31; i++) {
            urllist3.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist3.put("name", "女鞋");
        mylist3.put("chlist", chlist3);
        mylist3.put("urllist", urllist3);

        chlist4.add("连衣裙");
        chlist4.add("毛针织衫");
        chlist4.add("风衣");
        chlist4.add("套装");
        chlist4.add("棉衣棉服");
        chlist4.add("毛呢外套");
        chlist4.add("羽绒服");
        chlist4.add("大码女装");
        chlist4.add("裤子");
        chlist4.add("礼服");
        chlist4.add("西装裤");
        chlist4.add("打底裤");
        chlist4.add("棉裤羽绒裤");
        chlist4.add("职业装");
        chlist4.add("毛衣");
        chlist4.add("蕾丝雪纺衫");
        chlist4.add("上衣");
        chlist4.add("背心吊带");
        chlist4.add("妈妈装");
        chlist4.add("皮衣");
        chlist4.add("西装");
        chlist4.add("外套");
        chlist4.add("衬衣");
        chlist4.add("半身裙");
        chlist4.add("牛仔裤");
        chlist4.add("休闲裤");
        chlist4.add("T恤");
        for (int i = 32; i <= 58; i++) {
            urllist4.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist4.put("name", "女装");
        mylist4.put("chlist", chlist4);
        mylist4.put("urllist", urllist4);

        chlist5.add("彩妆");
        chlist5.add("美妆工具");
        chlist5.add("香水");
        chlist5.add("男士护肤");
        for (int i = 59; i <= 62; i++) {
            urllist5.add("https://meimg.su.bcebos.com/img/" + i + ".jpg");
        }
        mylist5.put("name", "彩妆");
        mylist5.put("chlist", chlist5);
        mylist5.put("urllist", urllist5);

        chlist6.add("家庭清洁");
        chlist6.add("洗发造型");
        chlist6.add("个人护理");
        chlist6.add("纸品湿巾");
        chlist6.add("口腔护理");
        chlist6.add("美容护肤");
        chlist6.add("洗浴用品");
        chlist6.add("女性护理");
        chlist6.add("衣物清洁");
        chlist6.add("驱蚊杀虫");
        chlist6.add("身体护理");
        chlist6.add("男士个护");
        for (int i = 63; i <= 74; i++) {
            urllist6.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist6.put("name", "洗护");
        mylist6.put("chlist", chlist6);
        mylist6.put("urllist", urllist6);

        chlist7.add("运动鞋服");
        chlist7.add("游泳");
        chlist7.add("骑行装备");
        chlist7.add("运动配件");
        chlist7.add("瑜伽");
        chlist7.add("泳衣");
        chlist7.add("户外鞋服");
        chlist7.add("户外装备");
        chlist7.add("运动工具");
        for (int i = 75; i <= 83; i++) {
            urllist7.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist7.put("name", "运动");
        mylist7.put("chlist", chlist7);
        mylist7.put("urllist", urllist7);

        chlist8.add("旅行箱包");
        chlist8.add("女包");
        chlist8.add("双肩包");
        chlist8.add("钱包");
        chlist8.add("男包");
        chlist8.add("功能包");
        chlist8.add("箱包配件");
        for (int i = 84; i <= 90; i++) {
            urllist8.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist8.put("name", "箱包");
        mylist8.put("chlist", chlist8);
        mylist8.put("urllist", urllist8);

        chlist9.add("文胸");
        chlist9.add("内裤");
        chlist9.add("袜子");
        chlist9.add("睡衣家居服");
        chlist9.add("文胸套装");
        chlist9.add("塑身衣");
        chlist9.add("背心T恤");
        chlist9.add("保暖内衣");
        chlist9.add("文胸配件");
        for (int i = 91; i <= 99; i++) {
            urllist9.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist9.put("name", "内衣");
        mylist9.put("chlist", chlist9);
        mylist9.put("urllist", urllist9);

        chlist10.add("音像制品");
        chlist10.add("驱蚊灭虫");
        chlist10.add("学生文具");
        chlist10.add("居家用品");
        chlist10.add("日用百货");
        chlist10.add("办公用品");
        chlist10.add("图书阅读");
        chlist10.add("毛巾浴巾");
        chlist10.add("收纳储物");
        chlist10.add("绿植园艺");
        chlist10.add("汽车用品");
        chlist10.add("桌游棋牌");
        chlist10.add("人偶娃娃");
        chlist10.add("模型手办");
        chlist10.add("美术兴趣");
        chlist10.add("毛笔相关");
        chlist10.add("洛丽塔");
        chlist10.add("乐器配件");
        chlist10.add("乐器");
        chlist10.add("教学用具");
        chlist10.add("户外广告");
        chlist10.add("古风外衣");
        chlist10.add("动漫影视周边");
        chlist10.add("点读机");
        chlist10.add("办公桌椅");
        chlist10.add("COS专区");
        chlist10.add("展柜货架");
        for (int i = 100; i <= 126; i++) {
            urllist10.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist10.put("name", "百货");
        mylist10.put("chlist", chlist10);
        mylist10.put("urllist", urllist10);

        chlist11.add("音响音箱");
        chlist11.add("大家电");
        chlist11.add("厨房大电");
        chlist11.add("生活电器");
        chlist11.add("商用电器");
        chlist11.add("家庭影音");
        chlist11.add("耳机配件");
        chlist11.add("厨房电器");
        for (int i = 127; i <= 134; i++) {
            urllist11.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist11.put("name", "家电");
        mylist11.put("chlist", chlist11);
        mylist11.put("urllist", urllist11);

        chlist12.add("床上用品");
        chlist12.add("餐具");
        chlist12.add("家具");
        chlist12.add("厨房用具");
        chlist12.add("家装");
        chlist12.add("厨房工具");
        chlist12.add("储物收纳");
        chlist12.add("灯饰光源");
        chlist12.add("装修建材");
        chlist12.add("装饰");
        chlist12.add("保温杯");
        chlist12.add("酒具");
        chlist12.add("茶具");
        chlist12.add("一次性用品");
        chlist12.add("咖啡相关");
        chlist12.add("饭盒");
        chlist12.add("杯类");
        chlist12.add("烧烤野炊");
        chlist12.add("烘焙");
        chlist12.add("机电五金");
        chlist12.add("工具");
        chlist12.add("测量仪器");
        chlist12.add("电子原件");
        chlist12.add("开关插座");
        chlist12.add("电路线材");
        chlist12.add("安防监控");
        chlist12.add("艺术品");
        for (int i = 135; i <= 161; i++) {
            urllist12.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist12.put("name", "家居");
        mylist12.put("chlist", chlist12);
        mylist12.put("urllist", urllist12);

        chlist13.add("计生用品");
        chlist13.add("避孕套");
        chlist13.add("情爱玩具");
        chlist13.add("情趣内衣");
        chlist13.add("女用玩具");
        for (int i = 162; i <= 166; i++) {
            urllist13.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist13.put("name", "成人");
        mylist13.put("chlist", chlist13);
        mylist13.put("urllist", urllist13);

        chlist14.add("手机");
        chlist14.add("游戏机");
        chlist14.add("移动电源");
        chlist14.add("相机");
        chlist14.add("台式电脑");
        chlist14.add("手机配件");
        chlist14.add("平板电脑");
        chlist14.add("品牌电脑");
        chlist14.add("笔记本");
        chlist14.add("路由器");
        chlist14.add("镜头");
        chlist14.add("电脑外设");
        chlist14.add("移动硬盘");
        chlist14.add("摄像机");
        chlist14.add("内存卡");
        chlist14.add("交换机");
        chlist14.add("耳机");
        chlist14.add("读卡器");
        chlist14.add("电脑硬件");
        chlist14.add("充电电池");
        chlist14.add("U盘");
        chlist14.add("MP3");
        chlist14.add("学习工具");
        chlist14.add("相机配件");
        for (int i = 167; i <= 190; i++) {
            urllist14.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist14.put("name", "数码");
        mylist14.put("chlist", chlist14);
        mylist14.put("urllist", urllist14);

        chlist15.add("面部护肤");
        chlist15.add("男士护肤");
        chlist15.add("身体护理");
        chlist15.add("T区护理");
        for (int i = 191; i <= 194; i++) {
            urllist15.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist15.put("name", "护肤");
        mylist15.put("chlist", chlist15);
        mylist15.put("urllist", urllist15);

        chlist16.add("爸爸装");
        chlist16.add("短裤中裤");
        chlist16.add("卫衣");
        chlist16.add("背心马甲");
        chlist16.add("套装");
        chlist16.add("POLO衫");
        chlist16.add("衬衫");
        chlist16.add("牛仔裤");
        chlist16.add("休闲裤");
        chlist16.add("T恤");
        chlist16.add("皮裤");
        chlist16.add("针织衫毛衣");
        chlist16.add("职业装");
        chlist16.add("皮衣");
        chlist16.add("毛呢大衣");
        chlist16.add("棉衣");
        chlist16.add("羽绒服");
        chlist16.add("风衣");
        chlist16.add("夹克");
        chlist16.add("棉裤");
        for (int i = 195; i <= 214; i++) {
            urllist16.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist16.put("name", "男装");
        mylist16.put("chlist", chlist16);
        mylist16.put("urllist", urllist16);

        chlist17.add("萌猫");
        chlist17.add("萌狗");
        chlist17.add("鱼水族");
        chlist17.add("鸟");
        chlist17.add("宠物美容");
        chlist17.add("宠物清洁");
        chlist17.add("护理保健");
        chlist17.add("食具食品");
        chlist17.add("玩具");
        chlist17.add("兔");
        chlist17.add("豚鼠");
        chlist17.add("宠物窝");
        chlist17.add("飞鼠");
        chlist17.add("仓鼠");
        chlist17.add("香猪");
        chlist17.add("龟");
        for (int i = 215; i <= 230; i++) {
            urllist17.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist17.put("name", "萌宠");
        mylist17.put("chlist", chlist17);
        mylist17.put("urllist", urllist17);

        chlist18.add("手表");
        chlist18.add("贵重饰品");
        chlist18.add("时尚饰品");
        chlist18.add("眼镜");
        chlist18.add("烟具");
        chlist18.add("瑞士军刀");
        for (int i = 231; i <= 236; i++) {
            urllist18.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist18.put("name", "配饰");
        mylist18.put("chlist", chlist18);
        mylist18.put("urllist", urllist18);

        chlist19.add("布鞋");
        chlist19.add("商务休闲");
        chlist19.add("正装鞋");
        chlist19.add("休闲鞋");
        chlist19.add("靴子");
        chlist19.add("高帮鞋");
        chlist19.add("拖鞋");
        chlist19.add("凉鞋");
        chlist19.add("帆布鞋");
        chlist19.add("低帮鞋");
        chlist19.add("增高鞋");
        chlist19.add("雨鞋");
        urllist19.add("https://meimg.su.bcebos.com/img/237.png");
        urllist19.add("https://meimg.su.bcebos.com/img/238.jpg");
        for (int i = 239; i <= 248; i++) {
            urllist19.add("https://meimg.su.bcebos.com/img/" + i + ".png");
        }
        mylist19.put("name", "男鞋");
        mylist19.put("chlist", chlist19);
        mylist19.put("urllist", urllist19);

        mylist.add(mylist1);
        mylist.add(mylist2);
        mylist.add(mylist3);
        mylist.add(mylist4);
        mylist.add(mylist5);
        mylist.add(mylist6);
        mylist.add(mylist7);
        mylist.add(mylist8);
        mylist.add(mylist9);
        mylist.add(mylist10);
        mylist.add(mylist11);
        mylist.add(mylist12);
        mylist.add(mylist13);
        mylist.add(mylist14);
        mylist.add(mylist15);
        mylist.add(mylist16);
        mylist.add(mylist17);
        mylist.add(mylist18);
        mylist.add(mylist19);
        result.put("mylist", mylist);

        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), result);
    }

    private StringBuffer get618Info(String type) {
        //淘宝618开门红—主会场
        StringBuffer result = new StringBuffer();
        result.append("https://s.click.taobao.com/t?e=m%3D2%26s%3DJ34%2FPSpXh0Rw4vFB6t2Z2iperVdZeJviA9baW3DwsQtyINtkUhsv0IXZ%2FD7jWya427tdjCZcB5taxX%2BMs2Te6EFkU2c0NcHZLbZf9qDH7neoG9M9jQ93%2BL2KkjFXWmXvQ%2BNeRhkrKyfCg%2Fx6BHtN2UfUVX1JYVActNIgQ9iMCzvn3f%2B%2FNQE2P3yKwiqD2WK0Bmf7LSR66ah9sLqZgfhqiHhFfjPlhOpnwFMQSe0DLrysHnz8fCEMFVPl3a%2BJy4rnK35Hc7I3FHjAL3LJSEgaCLIgC4IR%2FqToKZiW5u6ljW2FeIMElL0LTVdD3tYdlr9mRHJVmrqZSly7KEl6b4zk6ty6MiLyzQ42RsPnUPQPjsiiZ%2BQMlGz6FQ%3D%3D&union_lens=lensId%3AOPT%401590389110%400b150c71_0ef9_1724a93f5f7_4bb7%4001");
        if (!StringUtils.isEmpty(type) && !type.equals("undefined")) {
            result.delete(0, result.length());
            String url = homeMapper.get618Url(type);
            if (StringUtils.isEmpty(url)) {
                switch (type) {
                    case "2"://聚划算.主会场
                        result.append("https://s.click.taobao.com/t?e=m%3D2%26s%3DsQCuvrcZmb9w4vFB6t2Z2iperVdZeJviU%2F9%2F0taeK29yINtkUhsv0IXZ%2FD7jWya4sex2YzirA1taxX%2BMs2Te6EFkU2c0NcHZLbZf9qDH7neoG9M9jQ93%2BL2KkjFXWmXvQ%2BNeRhkrKyfCg%2Fx6BHtN2eIKl4JSR4lzy0DxCSzwFgFd1le1%2FF%2FLHXyKwiqD2WK0Bmf7LSR66ag4aIh%2BwnIONST8IooNW8SzO5CcJgHDOBKFzyYjawoqwHPcTBqw3dK2ZwIwbGyT%2BIgx%2FVsv6Vc%2Feb8ypPhJ4ccwKkasXMpORLaIVpiWEuIGaUx0inWuvg%2Fd&union_lens=lensId%3AOPT%401590389139%400b5c327f_1064_1724a9468fa_3e22%4001");
                        break;
                    case "3"://进口母婴会场
                        result.append("https://s.click.taobao.com/t?e=m%3D2%26s%3DPceUA2XiTX1w4vFB6t2Z2iperVdZeJviU%2F9%2F0taeK29yINtkUhsv0Jx2EO4FLpFN3aov8ftZy15axX%2BMs2Te6EFkU2c0NcHZLbZf9qDH7neoG9M9jQ93%2BL2KkjFXWmXvQ%2BNeRhkrKyfCg%2Fx6BHtN2eIKl4JSR4lzy0DxCSzwFgFd1le1%2FF%2FLHXyKwiqD2WK0Bmf7LSR66ag4aIh%2BwnIONST8IooNW8SzO5CcJgHDOBKFzyYjawoqwGqFT2L8zA8AD5oqx9jAclavLVpfR7iW8XuQsPw1%2Bhf3KZiW5u6ljW23A1QLKDcoWS7Tn%2B9UpZ3%2FxgxdTc00KD8%3D&union_lens=lensId%3AOPT%401590390376%400b5996e4_0f0a_1724aa74795_a529%4001");
                        break;
                    case "4"://美妆洗护会场
                        result.append("https://s.click.taobao.com/t?e=m%3D2%26s%3DPdSvoGglcU1w4vFB6t2Z2iperVdZeJviU%2F9%2F0taeK29yINtkUhsv0IXZ%2FD7jWya4ijNJEVZuO8BaxX%2BMs2Te6EFkU2c0NcHZLbZf9qDH7neoG9M9jQ93%2BL2KkjFXWmXvQ%2BNeRhkrKyfCg%2Fx6BHtN2eIKl4JSR4lzy0DxCSzwFgFd1le1%2FF%2FLHXyKwiqD2WK0Bmf7LSR66ag4aIh%2BwnIONST8IooNW8SzO5CcJgHDOBKFzyYjawoqwGqFT2L8zA8AD5oqx9jAclavLVpfR7iW8XuQsPw1%2Bhf3KZiW5u6ljW23A1QLKDcoWfxOMT65dx17xgxdTc00KD8%3D&union_lens=lensId%3AOPT%401590389171%400bba657f_0e9c_1724a94e4d2_ca45%4001");
                        break;
                    case "5"://数码家电主会场
                        result.append("https://s.click.taobao.com/t?e=m%3D2%26s%3DhrXLSG%2FfRHdw4vFB6t2Z2iperVdZeJviU%2F9%2F0taeK29yINtkUhsv0EgB2E5vIQuePgd1lUGYqrRaxX%2BMs2Te6EFkU2c0NcHZLbZf9qDH7neoG9M9jQ93%2BL2KkjFXWmXvQ%2BNeRhkrKyfCg%2Fx6BHtN2eIKl4JSR4lzy0DxCSzwFgFd1le1%2FF%2FLHXyKwiqD2WK0Bmf7LSR66ag4aIh%2BwnIONST8IooNW8SzO5CcJgHDOBKFzyYjawoqwGqFT2L8zA8AD5oqx9jAclavLVpfR7iW8XuQsPw1%2Bhf3KZiW5u6ljW23A1QLKDcoWfUdh4ZaM8qYxgxdTc00KD8%3D&union_lens=lensId%3AOPT%401590542027%400b5830b7_0f49_17253b14c68_e859%4001");
                        break;
                    case "6"://猫超年中大赏会场
                        result.append("https://s.click.taobao.com/t?e=m%3D2%26s%3DGQlon67PtAJw4vFB6t2Z2iperVdZeJviU%2F9%2F0taeK29yINtkUhsv0IXZ%2FD7jWya4IRTL3rb1AIxaxX%2BMs2Te6EFkU2c0NcHZLbZf9qDH7neoG9M9jQ93%2BL2KkjFXWmXvQ%2BNeRhkrKyfCg%2Fx6BHtN2eIKl4JSR4lzy0DxCSzwFgFd1le1%2FF%2FLHXyKwiqD2WK0Bmf7LSR66ag4aIh%2BwnIONST8IooNW8SzO5CcJgHDOBLHC5ze%2BTvdfWKche5LvsSoe5Cw%2FDX6F%2FfcujIi8s0ONp4Nzpr1BttKJVuDpJLooi%2F33HrLm2Z%2FCv3MVWYrqqoJq3Q6ahSKnxa4Z1%2BFeMHo0sYMXU3NNCg%2F&union_lens=lensId%3AOPT%401590389249%400bba6583_0e7d_1724a9616a2_4629%4001");
                        break;
                    case "7"://618主会场
                        result.append("https://s.click.taobao.com/t?e=m%3D2%26s%3DEFFfD3U0MSFw4vFB6t2Z2iperVdZeJviU%2F9%2F0taeK29yINtkUhsv0GOcQ6dD8nPske7DgLG9nJBaxX%2BMs2Te6EFkU2c0NcHZLbZf9qDH7neoG9M9jQ93%2BL2KkjFXWmXvQ%2BNeRhkrKyfCg%2Fx6BHtN2eIKl4JSR4lzy0DxCSzwFgFd1le1%2FF%2FLHXyKwiqD2WK0Bmf7LSR66aiAZRN1SCVoDhb3kPS3do6KsByyH5c6Km1TG%2FZ3Je4HhfD8DpA%2BLpWX2KHZceFa5Vu8Oi8EGH%2F9SbsoSXpvjOTq2pGh22PBM5tigULrnwEl0itrRgC%2BglzWc4kJL9WT%2BIYpzqMaJ0%2FwexGnF19q8ygx%2BtcF9N8ZYpGJo%2B7jy7C2uNDCdUrfAQABzMln6voI6XE%3D&union_lens=lensId%3AOPT%401590550861%400b1a25dc_0e68_172543815cb_8e30%4001");
                        break;
                    case "11":
                        result.append("https://s.click.taobao.com/ZfPAHvu");
                        break;
                    default://淘宝618开门红—主会场
                        result.append("https://s.click.taobao.com/t?e=m%3D2%26s%3DJ34%2FPSpXh0Rw4vFB6t2Z2iperVdZeJviA9baW3DwsQtyINtkUhsv0IXZ%2FD7jWya427tdjCZcB5taxX%2BMs2Te6EFkU2c0NcHZLbZf9qDH7neoG9M9jQ93%2BL2KkjFXWmXvQ%2BNeRhkrKyfCg%2Fx6BHtN2UfUVX1JYVActNIgQ9iMCzvn3f%2B%2FNQE2P3yKwiqD2WK0Bmf7LSR66ah9sLqZgfhqiHhFfjPlhOpnwFMQSe0DLrysHnz8fCEMFVPl3a%2BJy4rnK35Hc7I3FHjAL3LJSEgaCLIgC4IR%2FqToKZiW5u6ljW2FeIMElL0LTVdD3tYdlr9mRHJVmrqZSly7KEl6b4zk6ty6MiLyzQ42RsPnUPQPjsiiZ%2BQMlGz6FQ%3D%3D&union_lens=lensId%3AOPT%401590389110%400b150c71_0ef9_1724a93f5f7_4bb7%4001");
                        break;
                }
            } else {
                result.append(url);
            }
        }
        return result;
    }


    /**
     * 检测 是否参加邀请赚 默认自动添加
     */
    @Override
    public Tip addJoin(Integer user_id) {
        int req = joinInviteActivityLogMapper.selectJoin(user_id);
        if (req == 1) { //已参加

        } else if (req == 0) {//未参加
            joinInviteActivityLogMapper.addJoin(user_id);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), true);
    }

    /**
     * 邀请赚
     */
    @Override
    public Tip invitedToEarn(Integer user_id) {
        String key = "user_id:invitedToEarn-" + user_id;
        long setnx = jedisClient.setnx(key, user_id.toString(), 2);
        if (setnx != 1) {
            throw new BussinessException(BizExceptionEnum.OPERATION_TOO_FAST);
        }


        Map data = new HashMap();
        //第一部 根据加入活动时间 对粉丝首单结算进行 红包发放
        //1 查询邀请人、金额计算
        //1.1 获取用户参加活动时间
        String time = joinInviteActivityLogMapper.getJoinInviteActivityTime(user_id);
        //1.2 获取 用户参加活动之后 邀请的直接粉丝
        List<Map> user_s = new ArrayList<>();
        user_s = userMapper.invitedToEarnFansUserIdS(user_id, time);
        //2 直接粉丝 的收单进度
        List<Map> fans_first_order = fansFirstOrder(user_s, user_id);
        //2.1 邀请总收入： 去佣金变更查询
        Map map = new HashMap();
        if (fans_first_order.size() > 0) {
            map = fans_first_order.get(fans_first_order.size() - 1);
            fans_first_order.remove(fans_first_order.size() - 1);
        } else {
            map.put("fans_sum_money", 0);
        }
        //第二步 返回页面信息
        data.put("fans_count", user_s.size()); //粉丝数量
        data.putAll(map);                       // 邀请赚总
        data.put("fans_first_order", fans_first_order); //粉丝进度
        UserInfoDTO infoDTO = userMapper.getUserInfo(user_id);//获取用户信息
        data.put("friend_code", infoDTO.getFriend_code()); //邀请码

        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), data);
    }

    /**
     * 邀请赚 - 返回粉丝收单进度
     */
    private List<Map> fansFirstOrder(List<Map> user_s, Integer introducer) {
        List<Map> data = new ArrayList<Map>();
        List<Map> data_req = new ArrayList<Map>();
        if (user_s.size() > 0) {
            //三方订单信息
            data = userMapper.selectFansFirstOrde(user_s);
            //查询0元购商品
            //提前查询 已经返 红包 数量 规则：1-10:5     10-20:7   20-50:10   50-100:12   100-500:15   500> 20
            AtomicInteger stage = new AtomicInteger(); //粉丝 首单并结算 的数量
            AtomicDouble rake_money = new AtomicDouble();//粉丝 首单并结算 总共已经返红包总和
            Map mm = userMapper.selectFansFirstOrdeMessageMoney(user_s, introducer);


            for (Map userMap : user_s) {
                Integer id = ((Long) userMap.get("id")).intValue();
                String name = (String) userMap.get("nick_name");
                String pic = (String) userMap.get("head_pic");

                Map siftOrde_null = new HashMap();
                siftOrde_null.put("create_time", new Date());
                siftOrde_null.put("product_id", "-1");
                siftOrde_null.put("type", -1);
                siftOrde_null.put("order_id", -1);
                siftOrde_null.put("order_type", -1);
                siftOrde_null.put("status", 4);
                siftOrde_null.put("user_id", id);
                siftOrde_null.put("nick_name", name);
                siftOrde_null.put("head_pic", pic);

                for (int i = 0; i < data.size(); i++) {
                    Map m = data.get(i);
                    Integer u_id = (Integer) m.get("user_id");
                    Integer status = (Integer) m.get("status");
                    if ((!u_id.equals(id) && i == data.size() - 1)) { //遍历没有  没有三方订单 拿去精选订单
                        Map siftOrde = userMapper.selectFansFirstSiftOrde(id);
                        if (null != siftOrde) {
                            data_req.add(siftOrde);
                        }
                    }

                    if (u_id.equals(id) && status != 1) { //三方订单获取 不是结算订单，再差寻 精选订单并 判定是否是已结算，是，替换，否则不替换
                        Map siftOrde = userMapper.selectFansFirstSiftOrde(id);
                        if (null != siftOrde) {
                            Integer siftOrde_status = (Integer) siftOrde.get("status");
                            if (siftOrde_status == 1) {
                                data_req.add(siftOrde);//三方订单未结算，核桃精选订单结算 保留
                                break;
                            }
                        }

                    } else if (u_id.equals(id) && status == 1) {
                        data_req.add(m); //三方结算订单保留
                        break;
                    }
                }

                if (data.size() == 0) {
                    Map siftOrde = userMapper.selectFansFirstSiftOrde(id);
                    if (null != siftOrde) {
                        data_req.add(siftOrde);
                    } else {
                        data_req.add(siftOrde_null);
                    }
                }
            }
            data_req.forEach(m -> { //根据用户id查询返回用户昵称以及订单状态
                stage.incrementAndGet();
                Integer status = (Integer) m.get("status");
                if (null == status) {  //缺失订单 以失效订单处理
                    status = 4;
                }
                int[] statusList = new int[]{0, 0, 0};
                if (status == 2 || status == 1) {
                    statusList[0] = 1;
                }
                int rake = 0;
                //结算之后 粉丝的上级才会获取红包
                if (status == 1) { //订单状态 1-订单结算 2-订单付款 3-订单未付款 4-订单失效
                    statusList[1] = 1;
                    statusList[2] = 1;
                    //补红包
                    MessageMoney message = new MessageMoney();
                    message.setMoney(0);
                    message.setOrder_id((String) m.get("order_id"));
                    message.setOrder_type((Integer) m.get("type"));//'订单类型 1淘宝 2京东 3拼多多 4唯品会  6礼包(自营 核桃精选) 10 信用卡'
                    message.setOrigin_id((Integer) m.get("user_id"));
                    rake = stageMoney(stage.get());

                    message.setRake_money(rake);
                    message.setUser_id(introducer);
                    message.setMessage_type(6);
                    int req2 = userMoneyMapper.ifInsertMessageMoney2(message);
                    if (req2 == 0) { //没有记录需要添加
                        rake_money.addAndGet(rake);
                        userMoneyMapper.insertMessageMoney2(message);
                    }
                }

                m.put("fans_money", rake);
                m.put("progressBar", statusList);
                m.put("nick_name", EmojiUtil.decode((String) m.get("nick_name")));
            });
            Map map = new HashMap();
            map.put("fans_sum_money", rake_money.get() + ((BigDecimal) mm.get("rake_money")).doubleValue());
            data_req.add(map);

            //修改用户收益
            UserMoney userMoney = new UserMoney();
            userMoney.setUser_id(introducer);
            userMoney.setActivity_balance(rake_money.get());
            userMoney.setActivity_accumulated(rake_money.get());
            userMoneyMapper.updateUserMoney(userMoney);

        }
        return data_req;
    }

    //不同阶段返回的金额不同 :规则：1-10:5     10-20:7   20-50:10   50-100:12   100-500:15   500> 20
    private int stageMoney(int stage) {
        int money = 0;
        if (stage <= 10) {
            money = 5;
        } else if (stage > 10 && stage <= 20) {
            money = 7;
        } else if (stage > 20 && stage <= 50) {
            money = 10;
        } else if (stage > 50 && stage <= 100) {
            money = 12;
        } else if (stage > 100 && stage <= 500) {
            money = 15;
        } else if (stage > 500) {
            money = 20;
        }
        return money;
    }

    /**
     * 点击分享邀请赚按钮 添加一次记录
     */
    @Override
    public Tip addShare(Integer user_id) {
        User user = userMapper.selectUserId(user_id);
        if (null == user) {
            throw new BussinessException(BizExceptionEnum.USER_ID_DISAPPEAR);
        }
        joinInviteShareLogMapper.addLog(user_id);

        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }
}
