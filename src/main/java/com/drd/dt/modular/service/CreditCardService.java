package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface CreditCardService {


    /**
     * 获取url
     */
    public Tip creditCardURL(String userId, String bonusRate, String shareRate) throws Exception;

    /***
     *
     *  生成encrypt
     * */
    public Tip createSmyToken(String userId, String mobile, String bonusRate, String shareRate) throws Exception;


    /***
     * 查询单个应用信用卡订单
     * 接口 /b2b/api/getOrderListBysupplier
     * */
    public Tip adhibitionCreditCardOrder(int dataFlag, String startDate, String endDate, int pageNo, int pageSize) throws Exception;

    /***
     * 查询单个用户信用卡订单
     * 接口 /b2b/api/getOrderListByOpenId
     * */
    public Tip userCreditCardOrder(int user_id) throws Exception;

    /***
     * 用户信用卡回调
     * */
    public Tip notification(HttpServletRequest request, HttpServletResponse response) throws Exception;

    /**
     * 单个用户 结算
     */
    public void synchronizationCreditCardOrder(String userId, String orderId);

    /**
     * 订单列表查询接口
     **/
    public Tip creditCardOrderAll(String user_id, String token_id, String status);

    /**
     * 信用卡订单详情
     **/
    public Tip finDetail(String id);
}
