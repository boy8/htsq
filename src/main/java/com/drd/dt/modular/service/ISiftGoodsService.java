package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;

public interface ISiftGoodsService {

    /**
     * 查询列表 分页
     * */
    Tip SiftGoodsList(int index,String title_s,String sort,int discounts_type) throws Exception;

    /**
     * 查询精选核桃商品详情
     * */
    Tip SiftGoodsDateil(String id,Integer user_id) throws Exception;

    /**
     * 查询 列表界面 活动图连接
     * */
    Tip siftGoodsPageData(Integer user_id) throws Exception;

    /***
     * 首页  精选好货- 正品包邮
     * */
    Tip homeSiftGoods() throws Exception;
}
