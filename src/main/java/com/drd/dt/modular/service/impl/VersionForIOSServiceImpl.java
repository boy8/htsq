package com.drd.dt.modular.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.modular.dao.VersionForIOSMapper;
import com.drd.dt.modular.service.VersionForIOSService;
import com.drd.dt.util.HttpConnectionPoolUtil;
import com.drd.dt.util.MD5Utils;
import com.drd.dt.util.ResultUtil;
import com.drd.dt.util.XMLUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by 86514 on 2019/3/21.
 */
@Service("forIOSService")
@Transactional
public class VersionForIOSServiceImpl implements VersionForIOSService {

    private static final Logger logger = LoggerFactory.getLogger(VersionForIOSServiceImpl.class);

    //支付宝支付
    final String gateway="https://openapi.alipay.com/gateway.do";
    final String app_id="2019061165492954";
    final String private_key="MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCTIChyISaWvtXc9dVKFcxaLQ6oNm06yE3FjdfjsaieIpkc2/FTR7WQb4TluVcQ5bgOXeR9RdvON8ti+pqx4cSQCnLnOeG2JBrX3gsra7IVmrB4QrmhrVXzzpZr184MI81HfK3/KTp+RbJNvLOYzSRN5z6t4b7GfIafDC60J6qpen+Zq+HeG8KBy3yx9N9NIrs/cPjp7Ar3jWgTk+V4uBBwY8PMYlTEa7m88Q+O418zDwMtOJE2W8B3e6xAocnLSE/j76qFyP16H2V/i2oOf2TfwevdPxH99lMfj7iDECF/N6ayOLJlzu7+2hW64JUXIJPyFL7QXXRkWyLzc9nQT3IDAgMBAAECggEAcMM2/LJ31xYQ6Dfq78thQsRB9Z8xfNpE8WNT0oo6CGnQHJRelEvj1v4CR+gT2TmYJjrEg1dlrtqIoiYdJrU5aIT5qEtuuaFWiZj/ypnSdkiHdMT/bfFibWkrVSCkJh3SmjXvTiAVWu6kSHyW0kh4yNSx76eUBqEutPwrV50Harj0KzuCaZ9Iw6ssnomuCWyRt8Vpcy6WI/Hij4P3HRMoRHr6BRVtYA86Kcy13ZjpW/eOkfo8PBtcNSyUz7D0fBCE/OzViX3YGM/xJE4bLOTbr5LSleylW+gso1zm9cYDh2oSORPeGwjoROfUmRP2cb30U4yYQ1yyGjuzyLMXI6ChAQKBgQDT5J8d9DufqElEC/IqUWcFxYIphi8+vJOi8iWcEJ4D2N86qsH4jdsEfp6TeIU+zxg4WgpgO3dfa3IjA5kVz7LJC6KKMMbS877IoF0EaQz8iAMI7GroJjOA/xeHnOyBQgocXIqwEY4vIZIx9PTCaF1US57xPvfuPJ3KB9k6hU0jwwKBgQCxwDPonPrxKd8ZdYKeDauGz3zyY8SUHVV+yn23RdyZKB79RXfcUqJPcyYN/XEKQbsb32sS3iuD5QD5VKcSkdNP4FWYK7rBWVquPVa+eWHE/FoDv6FqZJoD10ulPV3p6FKAn/9LCsvT7OVqp2kMPAbpAEmQ26YEwlbtR23lS67UwQKBgQCxDwoy8oHcELhgJwdNe0X2GauBXUIfT16iu2rX9T7FAjTOhh+dvZO1NsT1f7D/4fdgJ0UiUq6WSmdOgNQr2KmRu6ovhUCux8S1SuNPKyUR4+l9TGis4jcJZJ9Zz7QE8pUWS/IL3C+OjKPoIMNhq+nI5YPEHRT5uycyycooT3k9FwKBgBhdJwBQXBkoSxs7Py8Y4pT05cR3pl9C8hASA09WtRkNJcpDdl1tui+3sdSjE5Z7UgFNC2knqMFIZ2zFjKz/7f352uGjxNJxw/s0DyrGin8ss83lu2NQ7MdwAD9i/Pdtz7kvtRd9IFkpFH0c+0ohBDV1w5+ma8glNzMI4mhWil1BAoGAYdNIiFuWsXiRbpBQ14afLGCaURLgWJdmmmLMDU5QgbSMSUZD1znK0OBlMQyUMB9Nww5TYoLO1qtA0rxhXOCtI+p1tGet6Q9QBPc42fm1zjmorPoikbw/P+hhDd2zcwaQ8a42/SDNBp7akcEzWzSN0u/S+L5/TE2hAeYKh0mEp+A=";
    final String charset="UTF-8";
    final String alipay_public_key="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqOkABsWIfwYPoWVEGUEsv1iyJwHjqDfIbzhO6n90Fqkvhkw3zIVhkcnYr7iO0fH9aTmz9+OFkIQ3Q694KRDQQHADysB5LfFlYNgJbSrULYbioRPPaT3FdY8wxmaSa31GChOdCiNph9OQGQHoY6Pc6lIZayG4Lni2hV5b8smCMVsvT5hX1klPLeTa4tWpL4oVcKVw83san82cPUL0ONbK8KhPlQ/SpYbGqPMLuZKt/5VmkHfi6u1b2YIH49Dcv1WkjqSKlgGCj2dXc2lXzbMUO5PGVnq/zuOcYf3A38YemzyqPBjnbgBqeeXRVNQ9k8wDkP1GJKPP3S6wq1koweLR+wIDAQAB";
    final String sign_type="RSA2";

    //微信支付
    final String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";//接口链接
    final String mch_id = "1560976561";//商户号
    final String appid = "wxdd0e2de308b00d98";//应用APPID
    final String key = "90C47766AD74C6C5179BAD32DF269515";//商户平台密钥key
    final String spbill_create_ip = "183.223.222.232";//终端IP
    final String notify_url = "http://www.quanbashi.vip:8083/qbs/iosVersion/WXPayCallback";//接收微信支付异步通知回调地址

    @Autowired
    private VersionForIOSMapper forIOSMapper;

    /**
     * ios支付宝付款
     **/
    @Override
    public Tip alipay(Map<String,String> map) throws Exception{
        String total_amount = "";//订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
        String subject = "";//商品的标题/交易标题/订单标题/订单关键字等。
        String out_trade_no = "";//商户网站唯一订单号
        if (map.containsKey("money")){
            DecimalFormat df = new DecimalFormat("#0.00");
            String money = map.get("money");
            total_amount = df.format(BigDecimal.valueOf(Double.valueOf(money)));
        }
        if (map.containsKey("title")){
            subject = map.get("title");
        }
        if (map.containsKey("order_id")){
            out_trade_no = map.get("order_id");
        }
        if (StringUtils.isEmpty(total_amount) || StringUtils.isEmpty(subject) || StringUtils.isEmpty(out_trade_no)){
            return ResultUtil.result(BizExceptionEnum.PARAM_EMPTY.getCode(), BizExceptionEnum.PARAM_EMPTY.getMessage());
        }
        AlipayClient alipayClient = new DefaultAlipayClient(gateway,app_id,private_key,"json",charset,alipay_public_key,sign_type);
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setSubject(subject);
        model.setOutTradeNo(out_trade_no);
        model.setTimeoutExpress("30m");
        model.setTotalAmount(total_amount);
        request.setBizModel(model);
        AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
        String body = response.getBody();
        if(response.isSuccess()){
            logger.info("【支付宝付款调用成功...】");
        } else {
            logger.info("【支付宝付款调用失败...】");
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),body);
    }

    /**
     * ios新增下单商品
     **/
    @Override
    public Tip goods(Map<String, String> map) {
        String create_time = Constant.y_M_d_H_m_s.format(new Date());
        map.put("create_time",create_time);
        map.put("status","1");
        forIOSMapper.insertGoods(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * ios下单商品列表
     */
    @Override
    public Tip list(Map<String, String> map) {
        List<Map> result = forIOSMapper.list(map);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),result);
    }

    /**
     * ios下单商品状态修改
     **/
    @Override
    public Tip status(Map<String, String> map) {
        String id = "0";
        if (map.containsKey("id")){
            id = map.get("id");
        }
        forIOSMapper.status(id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

    /**
     * 微信支付付款
     **/
    @Override
    public synchronized Tip wxpay(Map<String, String> map) throws Exception{
        String goods_desc = "";
        String money = "";
        if (map.containsKey("goods_desc")){
            goods_desc = map.get("goods_desc");
        }
        if (map.containsKey("money")){
            money = map.get("money");
        }
        Map param = new HashMap();
        param.put("appid",appid);
        param.put("mch_id",mch_id);
        String uuid = UUID.randomUUID().toString().replace("-", "");//随机32位字符串
        String order_id = Constant.yyyymmddhhmmss.format(new Date());
        param.put("nonce_str",uuid);
        param.put("body",goods_desc);//商品描述
        param.put("out_trade_no",order_id);//商户订单号
        param.put("total_fee",money);
        param.put("spbill_create_ip",spbill_create_ip);//调用微信支付API的机器IP
        param.put("notify_url",notify_url);//接收微信支付异步通知回调地址
        param.put("trade_type","APP");
        String sign = WXPaySign(param, key);
        param.put("sign", sign);
        String xml= XMLUtil.toXml(param);
        String result = HttpConnectionPoolUtil.httpXML(url, xml);
        JSONObject resultData = new JSONObject();
        if (!StringUtils.isEmpty(result)){
            logger.info("调取微信支付接口成功");
            Map<String, Object> data = XMLUtil.toMap(result);
            String return_code = data.get("return_code").toString();
            if (return_code.equals("SUCCESS")){
                resultData.put("appid",data.get("appid"));
                resultData.put("partnerid",data.get("mch_id"));//商户号
                resultData.put("prepayid",data.get("prepay_id"));
                resultData.put("noncestr",data.get("nonce_str"));
                resultData.put("package", "Sign=WXPay");
                String timeStamp = String.valueOf(System.currentTimeMillis()).substring(0,10);
                resultData.put("timestamp", timeStamp);
                String returnSign = WXPaySign(resultData, key);
                resultData.put("sign", returnSign);
                logger.info("返回数据【"+resultData+"】");
            }else {
                return ResultUtil.result(717, "支付失败");
            }
        }else {
            return ResultUtil.result(717, "支付失败");
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(),resultData);
    }

    /**
     *微信支付接口签名
     */
    public static String WXPaySign(Map map,String key) throws Exception{
        List<Map.Entry<String, String>> sign = new ArrayList<>(map.entrySet());
        Collections.sort(sign, (map1, map2) -> map1.getKey().compareTo(map2.getKey()));
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < sign.size(); i++){
            builder.append(sign.get(i) + "&");
        }
        builder.append("key=" + key);
        String str = builder.toString();
        String result = MD5Utils.MD5Encode(str, "utf8").toUpperCase();
        return result;
    }
}
