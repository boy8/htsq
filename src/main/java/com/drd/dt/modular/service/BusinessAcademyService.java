package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.BusinessAcademyDTO;

import javax.servlet.http.HttpServletRequest;

public interface BusinessAcademyService {

    /**
     * 分页查询 -- 用户好友以及官方 数据
     **/
    Tip friendBusinessAcademym(BusinessAcademyDTO map) throws Exception;

    /**
     * 获取详情 所需参数并 判断核对权限
     */
    Tip ifUserLevel(BusinessAcademyDTO map);

    /**
     * 点赞、转发、评论、阅读 累计
     */
    Tip synchronizationAccumulativeTotal(BusinessAcademyDTO map);

    /**
     * 返回模块对应关键字
     */
    Tip getModelCorrelationGrouping(int first_module,int second_module,String app_version,HttpServletRequest request,int deviceT) throws Exception;

    /**
     * 每日爆款商品拉取
     **/
    Tip insertDailyGoods(Integer page, String time) throws Exception;

    /**
     * 每日爆款商品列表
     **/
    Tip dailyGoods(Integer page,String category,Integer user_id) throws Exception;

    /**
     * 每日爆款分类
     **/
    Tip dailyCategory();

    /**
     * 每日爆款商品-复制评论
     **/
    Tip copyComment(String goods_id, Integer user_id, String ifCoupon, String goods_pic,
                    String goods_name,String price,String after_coupon_price) throws Exception;
}
