package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.GoodsDetailDTO;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface TBGoodsService {

    /**
     * 商品详情
     */
    Tip goodsDetail(String itemid, Integer user_id, HttpServletRequest request, String deviceT, String app_version) throws Exception;

    /**
     * 首页推荐商品(淘宝客-推广者-物料精选)
     */
    Tip getRecommendList(Integer page, Integer user_id, String imei, String DeviceT) throws Exception;

    /**
     * 搜索
     */
    Tip goodsSearch(String keyWords, Integer page, String sort, String ifcoupon, Integer user_id, String goods_id) throws Exception;

    /**
     * 商品转高拥
     */
    Tip goodsInfo(String goodsId, String userId, String relationId, Boolean ifCoupon, String buytype, String app_version, String deviceT) throws Exception;

    /**
     * 智能搜索 内容解析
     **/
    Tip analysisContent(String content) throws Exception;

    /**
     * 相关商品
     */
    Tip recommendGet(Long num_iid, Integer user_id) throws Exception;

    /**
     * 9.9 包邮
     */
    Tip sparePostage(int sort, int cid, int page, Integer user_id) throws Exception;


    /**
     * 高佣精品 好单裤
     */
    Tip supersearchList(String keyword, int sort, int is_coupon, int page, Integer user_id) throws Exception;


    /**
     * 高佣精品 淘宝
     */
    Tip supersearchLis2(Integer page, Integer user_id, String imei, String DeviceT) throws Exception;


    /*
     * 分页-根据关键字查询 商品列表及其详情
     * */
    Tip getList(String keywords, Integer page,HttpServletRequest request);
    Tip detail(String itemId, HttpServletRequest request);

}
