package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface HeiMaService {

    /**
     * 黑马渠道备案回调
     */
    Tip getBackInfo(String myid, String relation_id, String user_id, String special_id, String nikename) throws Exception;

}
