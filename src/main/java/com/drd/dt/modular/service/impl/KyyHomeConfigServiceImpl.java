package com.drd.dt.modular.service.impl;


import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dao.KyyHomeConfigMapper;
import com.drd.dt.modular.dto.KyyHomeConfigDTO;
import com.drd.dt.modular.dto.KyyStatisticsDTO;
import com.drd.dt.modular.service.KyyHomeConfigService;
import com.drd.dt.util.ResultUtil;
import com.drd.dt.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class KyyHomeConfigServiceImpl implements KyyHomeConfigService {

    @Autowired
    private KyyHomeConfigMapper kyyHomeConfigMapper;

    @Override
    public Tip getConfig() {
        KyyHomeConfigDTO k = kyyHomeConfigMapper.getConfig();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), k);
    }

    @Override
    public void addStatistics(HttpServletRequest request, String show_url, String imei, int data_type) {
        String ip = StringUtil.getIpAddr(request);
        KyyStatisticsDTO kyyStatisticsDTO = new KyyStatisticsDTO(data_type, show_url, imei, ip);
        kyyHomeConfigMapper.addStatistics(kyyStatisticsDTO);
    }
}
