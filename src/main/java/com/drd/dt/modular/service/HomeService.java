package com.drd.dt.modular.service;


import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dto.StuadDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface HomeService {

    /**
     * 分类
     **/
    Tip stulist() throws Exception;

    /**
     * 首页请求数据合集
     **/
    Tip list(String user_id,String deviceT,String app_version) throws Exception;

    /**
     * 获取开屏或banner图
     **/
    Tip getAds(Integer block, String imei, HttpServletRequest request, String deviceT, String app_version) throws Exception;

    /**
     * 限时秒杀时间点
     **/
    Tip seckillTime() throws Exception;

    /*
     * 首页-中间部分-核桃精选+限时秒杀时间点
     * */
    Tip seckillTimeAndSiftGoods() throws Exception;


    /**
     * 限时秒杀商品列表
     **/
    Tip seckillGoods(String page, String type, Integer user_id) throws Exception;

    /**
     * 参数返回(初始化)
     **/
    Tip stuad(StuadDTO stuadDTO, HttpServletRequest request) throws Exception;

    /**
     * 好单库获取热搜关键词
     */
    Tip hotSearch() throws Exception;

    /**
     * 商品分享链接
     **/
    Tip shareGoods(Integer type, String goods_id, String goods_name, Integer user_id, HttpServletRequest request) throws Exception;

    /**
     * 跳转时需转换的链接
     **/
    Tip exchangeUrl(Integer discern_id, Integer user_id, String theme_id) throws Exception;

    /**
     * 获取视频教程链接
     **/
    Tip course();

    /**
     * 零元购商品列表
     **/
    Tip zeroList(Integer user_id, String token_id, Integer page) throws Exception;

    /**
     * 首页618活动链接信息
     **/
    Tip activity618(Integer user_id, String token_id,String type,String title) throws Exception;

    /**
     * 618活动跳转时转换链接
     **/
    Tip activity618Change(Integer discern_id, Integer user_id, String type,String title);

    /**
     * 获取淘口令
     **/
    String exchangeTkl(String url,String title,String img) throws Exception;

    /**
     * 邀请赚
     * */
    Tip invitedToEarn(Integer user_id);

    /**
     * 检测 是否参加邀请赚 默认自动添加
     * */
    Tip addJoin(Integer user_id);

    /**
     * 点击分享邀请赚按钮 添加一次记录
     * */
    Tip addShare(Integer user_id);

}
