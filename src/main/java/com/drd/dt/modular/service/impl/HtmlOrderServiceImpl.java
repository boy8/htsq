package com.drd.dt.modular.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dto.SiftGoodsDTO;
import com.drd.dt.back.dto.SiftOrderDTO;
import com.drd.dt.back.entity.GoodsSpecification;
import com.drd.dt.back.entity.OwnGoods;
import com.drd.dt.common.Constant;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.modular.dao.HtmlOrderMapper;
import com.drd.dt.modular.dto.OwnGoodsOrderDTO;
import com.drd.dt.modular.dto.PayDTO;
import com.drd.dt.modular.dto.UserInfoDTO;
import com.drd.dt.modular.entity.HtmlOrderGift;
import com.drd.dt.modular.entity.OrderGift;
import com.drd.dt.modular.entity.User;
import com.drd.dt.modular.service.HtmlOrderService;
import com.drd.dt.util.*;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;


@Service
@Transactional
public class HtmlOrderServiceImpl implements HtmlOrderService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private HtmlOrderMapper htmlOrderMapper;
    @Autowired
    private JedisClient jedisClient;


    /*****************************测试**********************************/
    /**
     * 58.218.201.7 解析：ce.hetaosq.com:18003       H5地址
     * 58.218.201.7 解析：ce.hetaosq.com:18002          后台地址
     */
//    String return_url = "http://ce.hetaosq.com:18003/#/service_new"; //H5地址
//    final String alipay_notify_url = "http://ce.hetaosq.com:18002/api/v2/htsq/htmlOrder/alipayCallback"; //支付宝回调
//    final String wx_notify_url = "http://ce.hetaosq.com:18002/api/v2/htsq/htmlOrder/WXPayCallback";//接收微信支付异步通知回调地址
    /*****************************测试**********************************/


    String return_url = "http://p11.quanbashi.vip/#/service_new";

    //支付宝支付
    final String alipay_notify_url = "http://www.quanbashi.vip:18002/api/v2/htsq/htmlOrder/alipayCallback";
    String gateway = "https://openapi.alipay.com/gateway.do";
    String app_id = "2019111269098691";
    String private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCTIChyISaWvtXc9dVKFcxaLQ6oNm06yE3FjdfjsaieIpkc2/FTR7WQb4TluVcQ5bgOXeR9RdvON8ti+pqx4cSQCnLnOeG2JBrX3gsra7IVmrB4QrmhrVXzzpZr184MI81HfK3/KTp+RbJNvLOYzSRN5z6t4b7GfIafDC60J6qpen+Zq+HeG8KBy3yx9N9NIrs/cPjp7Ar3jWgTk+V4uBBwY8PMYlTEa7m88Q+O418zDwMtOJE2W8B3e6xAocnLSE/j76qFyP16H2V/i2oOf2TfwevdPxH99lMfj7iDECF/N6ayOLJlzu7+2hW64JUXIJPyFL7QXXRkWyLzc9nQT3IDAgMBAAECggEAcMM2/LJ31xYQ6Dfq78thQsRB9Z8xfNpE8WNT0oo6CGnQHJRelEvj1v4CR+gT2TmYJjrEg1dlrtqIoiYdJrU5aIT5qEtuuaFWiZj/ypnSdkiHdMT/bfFibWkrVSCkJh3SmjXvTiAVWu6kSHyW0kh4yNSx76eUBqEutPwrV50Harj0KzuCaZ9Iw6ssnomuCWyRt8Vpcy6WI/Hij4P3HRMoRHr6BRVtYA86Kcy13ZjpW/eOkfo8PBtcNSyUz7D0fBCE/OzViX3YGM/xJE4bLOTbr5LSleylW+gso1zm9cYDh2oSORPeGwjoROfUmRP2cb30U4yYQ1yyGjuzyLMXI6ChAQKBgQDT5J8d9DufqElEC/IqUWcFxYIphi8+vJOi8iWcEJ4D2N86qsH4jdsEfp6TeIU+zxg4WgpgO3dfa3IjA5kVz7LJC6KKMMbS877IoF0EaQz8iAMI7GroJjOA/xeHnOyBQgocXIqwEY4vIZIx9PTCaF1US57xPvfuPJ3KB9k6hU0jwwKBgQCxwDPonPrxKd8ZdYKeDauGz3zyY8SUHVV+yn23RdyZKB79RXfcUqJPcyYN/XEKQbsb32sS3iuD5QD5VKcSkdNP4FWYK7rBWVquPVa+eWHE/FoDv6FqZJoD10ulPV3p6FKAn/9LCsvT7OVqp2kMPAbpAEmQ26YEwlbtR23lS67UwQKBgQCxDwoy8oHcELhgJwdNe0X2GauBXUIfT16iu2rX9T7FAjTOhh+dvZO1NsT1f7D/4fdgJ0UiUq6WSmdOgNQr2KmRu6ovhUCux8S1SuNPKyUR4+l9TGis4jcJZJ9Zz7QE8pUWS/IL3C+OjKPoIMNhq+nI5YPEHRT5uycyycooT3k9FwKBgBhdJwBQXBkoSxs7Py8Y4pT05cR3pl9C8hASA09WtRkNJcpDdl1tui+3sdSjE5Z7UgFNC2knqMFIZ2zFjKz/7f352uGjxNJxw/s0DyrGin8ss83lu2NQ7MdwAD9i/Pdtz7kvtRd9IFkpFH0c+0ohBDV1w5+ma8glNzMI4mhWil1BAoGAYdNIiFuWsXiRbpBQ14afLGCaURLgWJdmmmLMDU5QgbSMSUZD1znK0OBlMQyUMB9Nww5TYoLO1qtA0rxhXOCtI+p1tGet6Q9QBPc42fm1zjmorPoikbw/P+hhDd2zcwaQ8a42/SDNBp7akcEzWzSN0u/S+L5/TE2hAeYKh0mEp+A=";
    String charset = "UTF-8";
    String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqOkABsWIfwYPoWVEGUEsv1iyJwHjqDfIbzhO6n90Fqkvhkw3zIVhkcnYr7iO0fH9aTmz9+OFkIQ3Q694KRDQQHADysB5LfFlYNgJbSrULYbioRPPaT3FdY8wxmaSa31GChOdCiNph9OQGQHoY6Pc6lIZayG4Lni2hV5b8smCMVsvT5hX1klPLeTa4tWpL4oVcKVw83san82cPUL0ONbK8KhPlQ/SpYbGqPMLuZKt/5VmkHfi6u1b2YIH49Dcv1WkjqSKlgGCj2dXc2lXzbMUO5PGVnq/zuOcYf3A38YemzyqPBjnbgBqeeXRVNQ9k8wDkP1GJKPP3S6wq1koweLR+wIDAQAB";
    String sign_type = "RSA2";
    String encryptKey = "RGjvbHcphXkFd9SGlBUvRA==";


    //微信支付
    final String refund = "https://api.mch.weixin.qq.com/secapi/pay/refund";//退款接口地址
    final String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";//支付接口链接
    final String mch_id = "1560976561";//商户号
    final String spbill_create_ip = "221.229.162.103";//终端IP
    final String appid = "wx2dc4fc556b56aa12";//H5应用APPID
    final String key = "edd1f792e84b12a9326204b9b6d0bb73";//商户平台密钥key
    final String wx_notify_url = "http://www.quanbashi.vip:18002/api/v2/htsq/htmlOrder/WXPayCallback";//接收微信支付异步通知回调地址

    final String ssl_name = "D:\\wx\\apiclient_cert.p12";//ssl 文件


    @Override
    public Tip goodsListNoPage() {
        List<OwnGoods> all = htmlOrderMapper.goodsListNoPage();
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), all);
    }

    @Override
    public Tip goodsCouponsListNoPage(Integer index, int coupons_price) {
        Integer pageNum = (index - 1) * 20;
        Page page = new Page(pageNum, 20);
        List<OwnGoods> list = htmlOrderMapper.goodsCouponsListNoPage(page, coupons_price);
        page.setRecords(list);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), page);
    }

    @Override
    public Tip goodsDetails(int dealer_id) {
        //商品详情
        OwnGoods ownGoods = htmlOrderMapper.goodsDetails(dealer_id);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), ownGoods);
    }


    @Override
    public Tip goodsSpecificationDetails(int goods_id) {
        List<GoodsSpecification> all = htmlOrderMapper.getGoodsSpecificationDetails(goods_id);
        Map<String, List<GoodsSpecification>> stringListMap = new HashMap<String, List<GoodsSpecification>>();
        for (GoodsSpecification h : all) {
            List<GoodsSpecification> reqData = null;
            if (stringListMap.containsKey(h.getEtalon_name())) {
                reqData = stringListMap.get(h.getEtalon_name());
            } else {
                reqData = new ArrayList<GoodsSpecification>();
            }
            reqData.add(h);
            stringListMap.put(h.getEtalon_name(), reqData);
        }
        //特殊处理 前端数据结构要求
        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
        for (String s : stringListMap.keySet()) {
            Map<String, Object> m = new HashMap<String, Object>();
            m.put("info", s);
            List<GoodsSpecification> reqData = stringListMap.get(s);
            m.put("project", reqData);
            dataList.add(m);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), dataList);
    }

    @Override
    public Tip orderGiftDetail(String order_id) {
        //查询订单详情
        HtmlOrderGift ownGoods = htmlOrderMapper.userOrderGiftDetail(order_id);
        //规格查询
        List<Long> ids = new ArrayList<>();
        if (null != ownGoods && null != ownGoods.getGoods_spe_id() && ownGoods.getGoods_spe_id().length() > 0) {
            String[] id_s = ownGoods.getGoods_spe_id().split(",");
            for (String s : id_s) {
                ids.add(Long.parseLong(s));
            }
        }
        String spec = "";
        for (GoodsSpecification specification : htmlOrderMapper.getGoodsSpecification(ids)) {
            if (spec.length() > 0) {
                spec = spec + ",";
            }
            spec = spec + specification.getEtalon_value();
        }
        ownGoods.setGetGoods_spe_idText(spec);
        //查询自营商品单位
        OwnGoods ownGoods1 = htmlOrderMapper.goodsDetails(Integer.parseInt(ownGoods.getProduct_id()));
        if (null != ownGoods1) {
            ownGoods.setGoods_units(ownGoods1.getGoods_units());
        }
        if (null != ownGoods) {
            if (ownGoods.getProduct_name().length() > 30) {
                ownGoods.setProduct_name(ownGoods.getProduct_name().substring(0, 30) + "...");
            }
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), ownGoods);
        }
        return ResultUtil.result(BizExceptionEnum.SELECT_ERROR.getCode(), BizExceptionEnum.SELECT_ERROR.getMessage());
    }


    @Override
    public void addOrderGift(OwnGoodsOrderDTO ownGoodsOrderDTO, HttpServletResponse response, HttpServletRequest request) throws Exception {
        Float actual_amount = ownGoodsOrderDTO.getActual_amount();
        if (StringUtils.isEmpty(actual_amount) || actual_amount <= 0) {//金额为空
            Tip result = ResultUtil.result(BizExceptionEnum.USER_MONEY_ERROR.getCode(), BizExceptionEnum.USER_MONEY_ERROR.getMessage());
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write(JSON.toJSONString(result));
            response.getWriter().flush();
            response.getWriter().close();
        }

        if (!StringUtil.isMobileNO(ownGoodsOrderDTO.getShipping_phone())) { //手机格式不支持
            Tip result = ResultUtil.result(BizExceptionEnum.USER_PHONE_ERROR.getCode(), BizExceptionEnum.USER_PHONE_ERROR.getMessage());
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write(JSON.toJSONString(result));
            response.getWriter().flush();
            response.getWriter().close();
        }

        /** 1 生成订单**/
        /** 1.1 生成订单号 规则 ：user_id + 时间+ goods_id **/
        String orderId = "";
        orderId = ownGoodsOrderDTO.getUser_id() + Constant.yyyymmddhhmmss.format(new Date()) + ownGoodsOrderDTO.getGoods_id();

        //1.1.1 提前预判 是否重复炒作
        String redisData = (String) jedisClient.get("htsq:addOrderGift:" + orderId);
        if (!StringUtils.isEmpty(redisData)) {
            return;
        }
        //1.1.2 提前防止用户多次操作 redis 缓存
        jedisClient.setWithExpire("htsq:addOrderGift:" + orderId, orderId, 10);

        ownGoodsOrderDTO.setStatus(0);
        ownGoodsOrderDTO.setOrder_id(orderId);
        /** 1.2 查询商品信息**/
        OwnGoods ownGoods = htmlOrderMapper.goodsDetails(ownGoodsOrderDTO.getGoods_id());
        if (null != ownGoods) {
            /** 1.2.1 订单存货验证**/
            Integer number = ownGoodsOrderDTO.getNumber();
            if (null == number) {
                number = 1;
            }
            if (!(ownGoods.getInventory() >= number)) { //库存不够
                throw new BussinessException(BizExceptionEnum.GOODS_NUM_ERROR);
            }
            if (null != ownGoods.getCarousel_imagsList() && ownGoods.getCarousel_imagsList().size() > 0) {
                ownGoodsOrderDTO.setGoods_image(ownGoods.getCarousel_imagsList().get(0));//默认轮播第一张图
            }
            ownGoodsOrderDTO.setGoods_name(ownGoods.getTitle());
        } else {//商品不存在 返回提示
//            return ResultUtil.result(BizExceptionEnum.GOODS_DETAIL_IS_EMPTY.getCode(), BizExceptionEnum.GOODS_DETAIL_IS_EMPTY.getMessage());
        }
        /** 1.3 数据库写入订单**/
        OrderGift gift = htmlOrderMapper.orderGiftDetail(orderId);
        if (gift != null && gift.getOrder_id().equals(orderId)) {//orderID已经存在，不重复添加
            return;
        }
        ownGoodsOrderDTO.setUser_id(0);
        /** 1.4 邮费计算*/
        //限购  提示：计算交给前端
        Double b = 0d;
        if (null != ownGoods.getRestrict() && ownGoods.getRestrict() == -1) { //不限购
            b = ownGoods.getPostage() * ownGoodsOrderDTO.getNumber();
        } else { //限购
            b = ownGoods.getPostage() * 1;
            if (null != ownGoods.getRestrict() && ownGoodsOrderDTO.getNumber() >= ownGoods.getRestrict()) {
                ownGoodsOrderDTO.setNumber(ownGoods.getRestrict());
            }
        }
        ownGoodsOrderDTO.setPostage(b.floatValue());


        /** 1.5 实际金额计*/
        Double coupons_price = ownGoods.getCoupons_price(); //优惠卷面额
        Double a = ownGoods.getNow_price() * ownGoodsOrderDTO.getNumber();
        if ((a - coupons_price) > 0) {
            ownGoodsOrderDTO.setActual_amount(a.floatValue() - coupons_price.floatValue() + b.floatValue()); //商品价格+ 邮费
        } else {
            ownGoodsOrderDTO.setActual_amount(0 + b.floatValue()); //商品价格+ 邮费
        }

        int req = htmlOrderMapper.addOrderGift(ownGoodsOrderDTO);
        if (req > 0) {
            /** 2 根据actual_amount_type 走不同 付款方式**/
            switch (ownGoodsOrderDTO.getActual_amount_type()) {
                case 1: {
                    PayDTO payDTO = new PayDTO();
                    payDTO.setPaytype(1);
                    payDTO.setOrderType(0);
                    payDTO.setMoney(ownGoodsOrderDTO.getActual_amount() + "");
                    payDTO.setTitle(ownGoodsOrderDTO.getGoods_name());
                    payDTO.setOrder_id(ownGoodsOrderDTO.getOrder_id());
                    payDTO.setGoods_id(ownGoodsOrderDTO.getGoods_id() + "");
                    alipayH5(response, request, payDTO);
                }
                break;
                case 2: {
                    PayDTO payDTO = new PayDTO();
                    payDTO.setPaytype(2);
                    payDTO.setOrderType(0);
                    payDTO.setMoney(ownGoodsOrderDTO.getActual_amount().toString());
                    payDTO.setTitle(ownGoodsOrderDTO.getGoods_name());
                    payDTO.setOrder_id(ownGoodsOrderDTO.getOrder_id());
                    payDTO.setGoods_id(ownGoodsOrderDTO.getGoods_id() + "");
                    wxpayH5(response, request, payDTO);
                }
                break;
                default: { //支付方式错误
                }
            }

        } else {
            throw new BussinessException(BizExceptionEnum.PATTERN_OF_PAYMENT_EXCEPTION);
        }
    }


    /**
     * 微信支付H5
     **/
    private void wxpayH5(HttpServletResponse response, HttpServletRequest request, PayDTO payDTO) throws Exception {
        int money = 0;
        String goods_id = payDTO.getGoods_id();//商品Id
        String goods_desc = payDTO.getTitle();
        String out_trade_no = payDTO.getOrder_id();

        if (null != payDTO.getMoney() && !payDTO.getMoney().equals("")) {
            BigDecimal re1 = new BigDecimal(payDTO.getMoney());
            BigDecimal re2 = new BigDecimal("100");
            money = re1.multiply(re2).intValue();
        }
        Map param = new HashMap();
        JSONObject obj = new JSONObject();
        obj.put("goods_id", goods_id);
        obj.put("order_id", out_trade_no);
        obj.put("order_type", payDTO.getOrderType());
        obj.put("buy_type", payDTO.getOrderType());
        param.put("attach", JSONObject.toJSONString(obj));
        param.put("appid", appid);
        param.put("mch_id", mch_id);
        String uuid = UUID.randomUUID().toString().replace("-", "");//随机32位字符串
        param.put("nonce_str", uuid);
        param.put("body", goods_desc);//商品描述
        param.put("out_trade_no", out_trade_no);//商户订单号
        param.put("total_fee", money);
        param.put("spbill_create_ip", StringUtil.getIpAddr(request));//调用客户端IP
        param.put("notify_url", wx_notify_url);//接收微信支付异步通知回调地址
        param.put("trade_type", "MWEB");

        String sign = WXPaySign(param, key);
        param.put("sign", sign);

        String xml = XMLUtil.toXml(param);

        String result = HttpConnectionPoolUtil.httpXML(url, xml);
        logger.info("微信返回:" + result + "】");
        String mweb_url = "";
        if (!StringUtils.isEmpty(result)) {
            logger.info("H5调取微信支付接口成功");
            Map<String, Object> data = XMLUtil.toMap(result);
            String return_code = data.get("return_code").toString();
            if (return_code.equals("SUCCESS")) {
                mweb_url = (String) data.get("mweb_url") + "&redirect_url=" + URLEncoder.encode(return_url);
                logger.info("返回数据【" + mweb_url + "】");
            } else {
                logger.info("返回数据【" + result + "】");
                throw new BussinessException(BizExceptionEnum.USER_PAY_FAIL);
            }
        } else {
            throw new BussinessException(BizExceptionEnum.USER_PAY_FAIL);
        }
        response.setContentType("text/json;charset=UTF-8");
        //直接将完整的表单html输出到页面
        Tip tip = ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), mweb_url);
        response.getWriter().write(JSONObject.toJSONString(tip));
        response.getWriter().flush();
        response.getWriter().close();
    }

    /***
     *  微信支付--回调
     * */
    @Override
    public void wxPayCallback(HttpServletResponse response, HttpServletRequest request) throws Exception {
        logger.info("【开始接收微信支付异步通知回调结果...】");
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        InputStream inStream = request.getInputStream();
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();

        String result = new String(outSteam.toByteArray(), "utf-8");
        Map<String, Object> map = XMLUtil.toMap(result);
        logger.info("WxGetPayResult返回结果:" + map);
        Map<String, Object> params = new HashMap<>();

        String return_code = "";
        String return_msg = "";
        if (map.containsKey("return_code")) {
            return_code = map.get("return_code").toString();
        }
        if (map.containsKey("return_msg")) {
            return_msg = map.get("return_msg").toString();
        }
        params.put("return_code", return_code);
        params.put("return_msg", return_msg);
        String xml = XMLUtil.toXml(params);
        out.print(xml);
        out.flush();
        out.close();

        if (!return_code.equals("SUCCESS")) {
            throw new BussinessException(BizExceptionEnum.USER_PAY_FAIL);
        }
        //后台订单业务逻辑
        JSONObject attach = null;
        if (map.containsKey("attach")) {
            attach = JSONObject.parseObject((String) map.get("attach"));
//            logger.info("订单同步开始:参数：attach-" + attach.toString());
        } else {
            throw new BussinessException(BizExceptionEnum.GOODS_ORDER_LOSE);
        }
        String order_id = "";
        String buyer_id = "";
        String buyer_logon_id = "";
        String trade_no = "";
        String goods_id = "";
        Integer order_type = -1;
        Integer buy_type = -1;//0自购 ，1来自分享'',',
        if (attach.containsKey("goods_id")) {
            goods_id = attach.getString("goods_id");
        }
        if (attach.containsKey("order_id")) {
            order_id = attach.getString("order_id");
        }
        if (map.containsKey("transaction_id")) {
            trade_no = (String) map.get("transaction_id");//微信交易订单号
        }
        if (attach.containsKey("order_type")) { //订单类型
            order_type = attach.getInteger("order_type");
        }
        if (attach.containsKey("buy_type")) { //订单购买方式
            buy_type = attach.getInteger("buy_type");
        }
        //修改订单状态
        ordersSynchronization(response, order_id, buyer_id, buyer_logon_id, trade_no, goods_id, "微信", order_type, buy_type);
    }


    /**
     * 支付宝支付
     **/
    private void alipayH5(HttpServletResponse response, HttpServletRequest request, PayDTO payDTO) throws Exception {
        String total_amount = payDTO.getMoney();//订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
        String subject = payDTO.getTitle();//商品的标题/交易标题/订单标题/订单关键字等。
        String out_trade_no = payDTO.getOrder_id();//商户网站唯一订单号
        String goods_id = payDTO.getGoods_id();//商品Id
        if (StringUtils.isEmpty(total_amount) || StringUtils.isEmpty(subject) || StringUtils.isEmpty(out_trade_no)) {
            throw new BussinessException(BizExceptionEnum.PARAM_EMPTY);
        }
        AlipayClient alipayClient = new DefaultAlipayClient(gateway,
                app_id,
                private_key,
                "json",
                charset,
                alipay_public_key,
                sign_type);
        AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();
        AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
        alipayRequest.setNotifyUrl(alipay_notify_url);//回调地址
        alipayRequest.setReturnUrl(return_url);
//        alipayRequest.setReturnUrl("order_type=" + payDTO.getOrderType());
        model.setSubject(subject);
        model.setOutTradeNo(out_trade_no);
        model.setPassbackParams(goods_id);
        model.setTimeoutExpress("30m");
        model.setTotalAmount(total_amount);
        model.setPassbackParams(JSONObject.toJSONString(payDTO)); //回传参数
        alipayRequest.setBizModel(model);
        String form = "";
        try {
            // 调用SDK生成表单
            form = alipayClient.pageExecute(alipayRequest).getBody();
        } catch (AlipayApiException e) {
            form = "err";
            e.printStackTrace();
        }
        response.setContentType("text/html;charset=UTF-8");
        //直接将完整的表单html输出到页面
        response.getWriter().write(form);
        response.getWriter().flush();
        response.getWriter().close();
    }

    /***
     *  支付宝支付--回调
     * */
    @Override
    public void alipayPayCallback(HttpServletResponse response, HttpServletRequest request) throws Exception {
        logger.info("【开始接收支付宝支付异步通知回调结果...】");
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        Map<String, String> params = new HashMap<String, String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
            //  System.out.println(name+"="+valueStr);
        }

        if (!AlipaySignature.rsaCheckV1(params, alipay_public_key, charset, sign_type)) {//验签是否是支付宝的回调
            response.getWriter().println("faild");
            return;
            // return ResultUtil.result(BizExceptionEnum.GOODS_ORDER_ID_LOSE.getCode(), BizExceptionEnum.GOODS_ORDER_ID_LOSE.getMessage());
        }
        String order_id = request.getParameter("out_trade_no");
        String trade_no = request.getParameter("trade_no");//支付客宝交易号
        String parms = request.getParameter("passback_params");
        String goods_id = null;
        Integer order_type = -1;//订单类型
        Integer buy_type = -1;//0自购 ，1来自分享'',',
        if (parms != null) {
            JSONObject obj = JSONObject.parseObject(parms);
            logger.info("支付宝参数集合：obj：" + parms);
            goods_id = obj.getString("goods_id");
            order_type = obj.getInteger("orderType");
            buy_type = obj.getInteger("buyType");

        }
        String buyer_id = request.getParameter("buyer_id");
        String buyer_logon_id = request.getParameter("buyer_logon_id");
        String trade_status = request.getParameter("trade_status");
        if (!trade_status.equals("TRADE_SUCCESS")) {//交易成功
            logger.info("支付失败");
            response.getWriter().println("faild");
            return;
        }
        //订单修改
        ordersSynchronization(response, order_id, buyer_id, buyer_logon_id, trade_no, goods_id, "支付宝", order_type, buy_type);
    }


    /**
     * 订单修改接口
     *
     * @param order_id       订单号
     * @param buyer_id       买家ID
     * @param buyer_logon_id 买家账号
     * @param trade_no       支付宝交易号
     * @param goods_id       商品ID
     * @param type           付款方式
     * @param goods_type     订单类型：0 - html订单独立h5脱了核桃
     * @param buy_type       订单购买类型：0-自购   、1 分享
     */
    private void ordersSynchronization(HttpServletResponse response,
                                       String order_id,
                                       String buyer_id,
                                       String buyer_logon_id,
                                       String trade_no,
                                       String goods_id,
                                       String type,
                                       int goods_type, int buy_type) throws Exception {

        if (null == order_id && order_id.length() < 1) { //丢失 订单号
            response.getWriter().println("faild");
            return;
        }
        if (null == goods_id && goods_id.length() < 1) { //丢失 产品ID
            response.getWriter().println("faild");
            return;
        }


        //验证 金额 订单号
        int ifOreder = htmlOrderMapper.finOrDerGift(order_id);
        if (ifOreder < 1) {
            response.getWriter().println("faild");
            return;
        }

        OrderGift ordertest = htmlOrderMapper.orderGiftDetail(order_id);
        if (type.equals("支付宝")) {
            if (TextUtils.isEmpty(ordertest.getBuyer_id())) {
                //同步更新 数据库订单状态
                Map<String, Object> m = new HashMap<>();
                m.put("id", order_id);
                m.put("status", 2);
                m.put("buyer_id", buyer_id);
                m.put("trade_no", trade_no);
                m.put("buyer_logon_id", buyer_logon_id);
                int req = htmlOrderMapper.upOrderGift(m);
//                logger.info("更新订单：" + req);
                if (req > 0) {
                    //更新库存 -, 售卖 +
                    htmlOrderMapper.upInventorySell(goods_id);
                    response.getWriter().println("success");
                }
            } else {
                response.getWriter().println("success");
            }
            response.getWriter().println("faild");
        } else if (type.equals("微信")) {
            if (null != ordertest) {
                //同步更新 数据库订单状态
                if (TextUtils.isEmpty(ordertest.getTrade_no())) {//第一次支付回调更新
                    Map<String, Object> m = new HashMap<>();
                    m.put("id", order_id);
                    m.put("status", 2);
                    m.put("buyer_id", buyer_id);
                    m.put("trade_no", trade_no);
                    m.put("buyer_logon_id", buyer_logon_id);
                    int req = htmlOrderMapper.upOrderGift(m);
//                    logger.info("更新订单：" + req);
                    if (req > 0) {
                        //更新库存 -, 售卖 +
                        htmlOrderMapper.upInventorySell(goods_id);
                    }
                }
            }
        }

    }

    /**
     * 微信支付接口签名
     */
    private String WXPaySign(Map map, String key) throws Exception {
        List<Map.Entry<String, String>> sign = new ArrayList<>(map.entrySet());
        Collections.sort(sign, (map1, map2) -> map1.getKey().compareTo(map2.getKey()));
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < sign.size(); i++) {
            builder.append(sign.get(i) + "&");
        }
        builder.append("key=" + key);
        String str = builder.toString();
        String result = MD5Utils.MD5Encode(str, "utf8").toUpperCase();
        return result;
    }
}
