package com.drd.dt.modular.service.impl;

import com.drd.dt.back.dao.ISiftOrderLogBaseMapper;
import com.drd.dt.back.dto.SiftOrderLogDTO;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.modular.dao.ISiftOrderLogMapper;
import com.drd.dt.modular.dao.SiftOrderMapper;
import com.drd.dt.modular.service.ISiftOrderLogService;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SiftOrderLogServiceImpl implements ISiftOrderLogService {

    @Autowired
    private ISiftOrderLogBaseMapper siftOrderLogBaseMapper;
    @Autowired
    private ISiftOrderLogMapper iSiftOrderLogMapper;
    @Autowired
    private SiftOrderMapper siftOrderMapper;
    @Autowired
    private ExpressTradeServiceImpl expressTradeService;

    @Override
    public Tip addLog(SiftOrderLogDTO siftOrderLogDTO) {
        siftOrderLogDTO.setCreation_time(new Date());
        int req = siftOrderLogBaseMapper.insert(siftOrderLogDTO);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.SELECT_ERROR.getCode(), BizExceptionEnum.SELECT_ERROR.getMessage());

    }

    @Override
    public Tip userOrderLogList(Integer user_id) {
        //1 查询用户订单
        List<String> ids = siftOrderMapper.orderIdList(user_id);
        List<Map> data = new ArrayList<Map>();
        //2 根据订单号查询售后记录
        if (null != ids && ids.size() > 0) {
            data = iSiftOrderLogMapper.siftOrderLogList(ids);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), data);
    }


    @Override
    public Tip userParcel(String parcel_number, String parcel_type, Integer id) {
        //1 校验订单号
        Tip data = expressTradeService.queryExpress("-1",parcel_number);
        if(data.getCode() != 200 || null == data.getData() ||  data.getData().equals("")){
            return ResultUtil.result(BizExceptionEnum.PARCELE_NUMBER_ERROR.getCode(), BizExceptionEnum.PARCELE_NUMBER_ERROR.getMessage());
        }
        //2 修改 记录
        int req = iSiftOrderLogMapper.userParcel(parcel_number, parcel_type, id);
        if (req > 0) {
            return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
        }
        return ResultUtil.result(BizExceptionEnum.UPDATE_ERROR.getCode(), BizExceptionEnum.UPDATE_ERROR.getMessage());
    }
}
