package com.drd.dt.modular.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.back.dto.SiftGoodsDTO;
import com.drd.dt.back.entity.AdvBack;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.exception.BizExceptionEnum;
import com.drd.dt.exception.BussinessException;
import com.drd.dt.modular.dao.SiftGoodsMapper;
import com.drd.dt.modular.dao.SiftOrderMapper;
import com.drd.dt.modular.dao.UserMapper;
import com.drd.dt.modular.entity.User;
import com.drd.dt.modular.service.ISiftGoodsService;
import com.drd.dt.modular.service.ISiftGoodsSpecificationService;
import com.drd.dt.modular.service.UserService;
import com.drd.dt.util.EmojiUtil;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SiftGoodsServiceImpl implements ISiftGoodsService {

    @Autowired
    private UserMoneyServiceImpl userMoneyService;

    @Autowired
    private SiftGoodsMapper siftGoodsMapper;
    @Autowired
    private ISiftGoodsSpecificationService iSiftGoodsSpecificationService;
    @Autowired
    private SiftOrderMapper siftOrderMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public Tip SiftGoodsList(int index, String title_s, String sort,int discounts_type) throws Exception {
        Integer pageNum = index / 20 + 1;
        Page page = new Page(pageNum, 20);
        SiftGoodsDTO siftGoodsDTO = new SiftGoodsDTO();
        siftGoodsDTO.setDiscounts_type(discounts_type);
        if (!StringUtils.isEmpty(title_s)) {
            siftGoodsDTO.setTitle_s("%" + title_s + "%");
        }
        if (!StringUtils.isEmpty(sort)) {
            siftGoodsDTO.setSort(sort);
        }
        List<SiftGoodsDTO> list = siftGoodsMapper.SiftGoodsList(page, siftGoodsDTO);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);
    }

    @Override
    public Tip SiftGoodsDateil(String id, Integer user_id) throws Exception {
        SiftGoodsDTO siftGoodsDTO = siftGoodsMapper.SiftGoodsDateil(id); //基础精选核桃商品信息
        Double userRakeBack = siftGoodsDTO.getRake_back();
        if (userRakeBack == null) {
            userRakeBack = 0.0;
        }
        Double reback_money = userMoneyService.getSelftRakeBackRate(user_id);//佣金比例
        userRakeBack = userRakeBack * reback_money;
        siftGoodsDTO.setUser_rake_back(Double.parseDouble(String.format("%.2f", userRakeBack)));//小数两位
        Tip tip = iSiftGoodsSpecificationService.getSiftGoodsSpecification(siftGoodsDTO.getId()); //规格
        if (null != tip.getData()) {
            siftGoodsDTO.setSpecificationList((List<Map<String, Object>>) tip.getData());
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), siftGoodsDTO);
    }


    @Override
    public Tip siftGoodsPageData(Integer user_id) throws Exception {
        Map map = new HashMap();
        //1 获取广告图
        List<AdvBack> list = siftGoodsMapper.getPersonalBanner();
        //2 用户当前进度条
        Double sumMoney = siftOrderMapper.getUserSiftOrderSumMoney(user_id);
        //3 顶部分类
        List<String> sort = siftGoodsMapper.getTopSort();
        //4 基础信息 昵称 电话 地址
        User user = userMapper.selectUserId(user_id);
        if (null == user) {
            throw new BussinessException(BizExceptionEnum.USER_MONEY_LOSE);//用户登录异常
        }
        map.put("addr", user.getAddr());//地址
        String name = "";
        if (!StringUtils.isEmpty(user.getAlipay_user_name())) {
            name = user.getAlipay_user_name();
        } else if (!StringUtils.isEmpty(user.getWx_nickname())) {
            name = user.getWx_nickname();
        } else if (!StringUtils.isEmpty(user.getNick_name())) {
            name = user.getNick_name();
        }
        map.put("head_pic", user.getHead_pic());//头像
        map.put("addr", user.getAddr());//地址
        map.put("name", EmojiUtil.decode(name));//昵称
        map.put("phone", user.getPhone());//电话
        map.put("bannerList", list);//列表
        map.put("sort", sort);//分类
        map.put("sumMoney", sumMoney);//当前共计消费
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), map);
    }


    @Override
    public Tip homeSiftGoods() throws Exception {
        Page page = new Page(0, 2);
        SiftGoodsDTO siftGoodsDTO = new SiftGoodsDTO();
        List<SiftGoodsDTO> list = siftGoodsMapper.SiftGoodsList(page, siftGoodsDTO);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), list);

    }
}
