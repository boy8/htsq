package com.drd.dt.modular.service.impl;

import com.drd.dt.back.dto.SiftGoodsSpecificationDTO;
import com.drd.dt.back.entity.GoodsSpecification;
import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dao.SiftGoodsSpecificationMapper;
import com.drd.dt.modular.service.ISiftGoodsSpecificationService;
import com.drd.dt.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SiftGoodsSpecificationImpl implements ISiftGoodsSpecificationService {

    @Autowired
    private SiftGoodsSpecificationMapper siftGoodsSpecificationMapper;


    @Override
    public Tip getSiftGoodsSpecification(String goods_id) {
        List<SiftGoodsSpecificationDTO>  all = siftGoodsSpecificationMapper.getSiftGoodsSpecification(goods_id);
        Map<String, List<SiftGoodsSpecificationDTO>> stringListMap = new HashMap<String, List<SiftGoodsSpecificationDTO>>();
        for (SiftGoodsSpecificationDTO h : all) {
            List<SiftGoodsSpecificationDTO> reqData = null;
            if (stringListMap.containsKey(h.getEtalon_name())) {
                reqData = stringListMap.get(h.getEtalon_name());
            } else {
                reqData = new ArrayList<SiftGoodsSpecificationDTO>();
            }
            reqData.add(h);
            stringListMap.put(h.getEtalon_name(), reqData);
        }
        //特殊处理 前端数据结构要求
        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
        for (String s : stringListMap.keySet()) {
            Map<String, Object> m = new HashMap<String, Object>();
            m.put("info", s);
            List<SiftGoodsSpecificationDTO> reqData = stringListMap.get(s);
            m.put("project", reqData);
            dataList.add(m);
        }
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage(), dataList);
    }
}
