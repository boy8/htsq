package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;

import java.util.Map;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface VersionForIOSService {

    /**
     * ios支付宝付款
     **/
    Tip alipay(Map<String, String> map) throws Exception;

    /**
     * ios新增下单商品
     **/
    Tip goods(Map<String, String> map) throws Exception;

    /**
     * ios下单商品列表
     */
    Tip list(Map<String, String> map) throws Exception;

    /**
     * ios下单商品状态修改
     **/
    Tip status(Map<String, String> map) throws Exception;

    /**
     * 微信支付付款
     **/
    Tip wxpay(Map<String, String> map) throws Exception;

}
