package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by 86514 on 2019/3/21.
 */
public interface JDService {

    /**
     * 获取最新订单
     */
    Tip task() throws Exception;

    /**
     * 同步完成订单
     */
    Tip successOrder(String beginTime, String type) throws Exception;

    /**
     * 商品详情
     */
    Tip getJDDetail(String sku, Integer user_id,HttpServletRequest request, String deviceT, String app_version) throws Exception;


    Tip getJdBrokerageList(String keywords,String page,String sort,Integer user_id) throws Exception;

    //获取京东订单创建时间  状态为
    List<String> getJDOrderTime(String time);


    Tip list(String keywords, Integer page, HttpServletRequest request);
}
