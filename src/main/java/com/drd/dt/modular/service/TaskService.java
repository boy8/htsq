package com.drd.dt.modular.service;

import com.drd.dt.common.tips.Tip;

/**
 * Created by 86514 on 2019/3/25.
 */
public interface TaskService {

    /**
     * 获取用户的任务列表
     **/
    Tip taskCenterInfo(Integer user_id, String token_id) throws Exception;

    /**
     * 用户完成任务
     **/
    Tip finish(Integer user_id, String token_id, String task_id,Integer is_double,Integer task_type) throws Exception;

    /**
     * 查询用户的任务详情
     */
    Tip detail(Integer user_id, String token_id, Integer task_id,Integer task_type) throws Exception;

    /**
     * 用户金币兑换
     */
    Tip exchange(Integer user_id, String token_id, Integer coin);

    /**
     * 用户金币兑换历史记录列表
     **/
    Tip exchangeHistory(Integer user_id, String token_id, Integer page,Integer type);

    /**
     * 用户金币兑换历史记录金币信息
     **/
    Tip exchangeCoinInfo(Integer user_id, String token_id);

    /**
     * 粉丝金币贡献列表记录
     **/
    Tip fansContributionList(Integer user_id, String token_id,Integer page,Integer type);
}
