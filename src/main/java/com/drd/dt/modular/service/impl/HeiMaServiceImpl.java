package com.drd.dt.modular.service.impl;

import com.drd.dt.common.SuccessResultEnum;
import com.drd.dt.common.tips.Tip;
import com.drd.dt.modular.dao.UserMapper;
import com.drd.dt.modular.entity.User;
import com.drd.dt.modular.service.HeiMaService;
import com.drd.dt.properties.HMTKPropertyConfig;
import com.drd.dt.util.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 86514 on 2019/3/21.
 */
@Transactional
@Service
public class HeiMaServiceImpl implements HeiMaService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserMapper userMapper;

    /**
     * 黑马渠道备案回调
     */
    @Override
    public Tip getBackInfo(String myid, String relation_id, String user_id, String special_id, String nikename) throws Exception {
        logger.info("【获取黑马渠道备案回调的参数为【myid：" + myid + "，relation_id：" + relation_id + ",user_id:" + user_id + "】】...");
        User user = new User();
        user.setId(Integer.valueOf(myid));
        user.setTaobao_relation_id(relation_id);
        user.setTaobao_special_id(special_id);
        user.setTaobao_user_id(user_id);
        user.setTaobao_user_name(nikename);

        logger.info("【开始更新用户【" + myid + "】在淘宝的的渠道关系ID【" + relation_id + "】和淘宝用户ID【" + user_id + "】...");
        //更新用户淘宝的渠道关系id和userid
        userMapper.updateById(user);
        return ResultUtil.result(SuccessResultEnum.SUCCESS.getCode(), SuccessResultEnum.SUCCESS.getMessage());
    }

}
