package com.drd.dt.common;

import java.text.SimpleDateFormat;

/**
 * 模块中的常量
 * @author th
 */
public class Constant {

    public static final String DEFAULT_HEAD_PIC = "https://meimg.su.bcebos.com/htsq/htsq_icon.png";
    public static final String QBS_JG_APPKEY = "cb26b3d41bdc8dd0c53511ae";
    public static final String QBS_JG_APPSECRET = "ff412eab6bd21fb352f2838d";

    //分隔符
    public static final String SEPARATOR = ",";

    //时间格式
    public static SimpleDateFormat y_M_d_H_m_s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat y_M_d_H_m = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static SimpleDateFormat yyyymmddhhmmss = new SimpleDateFormat("yyyyMMddHHmmss");
    public static SimpleDateFormat H_m_s = new SimpleDateFormat("HH:mm:ss");
    public static SimpleDateFormat H_m = new SimpleDateFormat("HH:mm");
    public static SimpleDateFormat Hm = new SimpleDateFormat("HHmm");
    public static SimpleDateFormat y_M_d = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat yMd = new SimpleDateFormat("yyyyMMdd");
    public static SimpleDateFormat yM = new SimpleDateFormat("yyyyMM");


    //请求参数
    public static final String CODE = "code";
    public static final String MSG = "msg";
    public static final String RESULT = "result";
    public static final String DATA = "data";
    public static final Integer PAGE_SIZE_20 = 20;
    public static final Integer PAGE_SIZE_40 = 40;

    public static final String OK = "OK";
    public static final String ERROR = "ERROR";
    public static final String IS_OPENED = "已结束";
    public static final String IS_OPENING = "正在疯抢";
    public static final String INSTANTLY_OPEN = "即将开始";
    public static final String SECKILL_DESC = "海量尖货，等你来抢";

    public static final String TX_WITHDRAW_TITLE = "提现提醒";
    public static final String TX_SUCCESS = "您的提现已到账请注意查收";
    public static final String MSG_WX_NOT = "暂无";

    public static final String SYS_REGISTER_MSG_TITLE = "系统消息";
    public static final String SYS_COMISSION = "佣金相关系统消息";
    public static final String COMMISSION_MSG = "暂无佣金消息";
    public static final String SYS_BOUNTY = "奖励佣金系统消息";
    public static final String SYS_REGISTER_MSG_CONTENT = "欢迎注册［核桃省钱］会员，免费使用核桃购买和分享都可以获得高额返佣，VIP可获得更多专属尊贵权益。快开启省&赚钱模式吧！";

    /**
     * 提现
     */
    public static final int BALANCE_TYPE_TX_SUCCESS=1;
    /**
     * 提现拒绝
     */
    public static final int BALANCE_TYPE_TX_FAILD=2;
    /**
     * 自购 收入
     */
    public static final int BALANCE_TYPE_BUY_SELF=3;
    /**
     * 分享 收入
     */
    public static final int BALANCE_TYPE_SHARE=4;
    /**
     * 粉丝 收入
     */
   public static final int BALANCE_TYPE_FANCE=5;
    /**
     * 礼包 奖励收入
     */
    public static final int BALANCE_TYPE_GIFT=6;
    /**
     * 维权订单
     */
    public static final int BALANCE_TYPE_LEGITORDER=7;
    /**
     * 订单红包
     */
    public static final int BALANCE_TYPE_ORDER_REDPAPER=8;
    /**
     * 收入
     */
    public static final int BALANCE_ACTION_ADD=1;
    /**
     * 支出
     */
    public static final int BALANCE_ACTION_REDUCE=2;



}
