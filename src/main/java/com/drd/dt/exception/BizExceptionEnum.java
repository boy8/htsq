package com.drd.dt.exception;

/**
 * 所有业务异常的枚举
 *
 * @author th
 * @date 2019年11月12日
 */
public enum BizExceptionEnum  {

    OT_LOGIN_ERROR(211,"未登录或已退出"),
    OT_WX_ILLEGAL(212,"微信已绑定,请激活"),
    OT_PHONE_ILLEGAL(213,"请绑定邀请码"),
    OT_USER_ERROR(214,"不存在此用户"),
    USER_ID_AND_PHONE(217,"用户ID与系统记录手机号不匹配"),
    OT_USER_ILLEGAL(215,"账号已被冻结，请联系管理员"),
    PARAM_EMPTY(600,"请填写必要字段！"),

    /**
     * 业务逻辑
     */
    PATAM_ERROR(502, "搜索条件有误"),
    TOO_LONG(503, "标识超过了限定长度"),
    SERVER_ERROR(504, "标识超过了限定长度"),

    /**
     *  备案信息
     * **/
    ARCHIVES_IS_ERROR(600,"需要备案"),


    /**
     * 用户异常
     */
    USER_ORDER_ERROR_TASK(600,"定时订单同步失败"),
    USER_LOGIN_TYPE_ERROR(601,"第三方登录类型错误"),
    USER_THIRD_KEY_ERROR(602,"third_key错误"),
    USER_LOGIN_PWD_ERROR(603,"密码错误"),
    USER_USERNAME_EMPTY(605,"用户名密码不能为空"),
    USER_FRIEND_CODE_EMPTY(606,"邀请码为空"),
    USER_FRIEND_CODE_NOT_EXIST(607,"邀请码不存在"),
    USER_MSG_SEND_FAIL(608,"短信发送失败"),
    USER_VALID_CODE_ERROR(609,"验证码错误"),
    USER_ID_DISAPPEAR(610,"用户丢失"),
    USER_PREFANS_EMPTY(611,"暂未获取到粉丝信息，请稍后再试"),
    USER_PAY_FAIL(612,"支付失败"),
    USER_FRIENDCODE_IS_BIND(613,"用户已绑定邀请码"),
    USER_FRIEND_CODE_ERROR(614,"请输入正确的邀请码"),
    USER_PHONE_ERROR(615,"手机号码输入有误"),
    USER_MONEY_ERROR(616,"购买金额有误，请重新进入购买"),
    USER_MONEY_LOSE(617,"用户查询出现异常,或者用户不存在"),
    USER_MIAOYAN_ERROR(618,"调取秒验接口错误"),
    USER_TOKEN_ERROR(619,"用户数据登录存在异常"),
    USER_UPDATE_ALIPAY_ERROR(620,"修改失败,此账号已有提现记录"),

    /**
     * 金币
     */
    GOLD_COIN_GET_ERROR(701,"今日已获得金币"),
    GOLD_COIN_EXCHANGE_ERROR(702,"红包每月限兑一个"),
    GOLD_COIN_NOT_ENOUGH_ERROR(703,"当前金币余额不足"),
    ZUIMEI_EXCHANGE_NOT_ENOUGH(704,"您的兑换券余额不足"),
    ZUIMEI_GOODS_PRICE_ERROR(705,"商品价格大于一元时可使用"),
    GOLD_COIN_EXCHANGE_NUM_ERROR(706,"金币大于100时可兑换"),
    TASK_INFO_NOT_EXIST(707,"当前任务信息不存在"),
    /**
     *商品类
     */
    GOODS_DETAIL_IS_EMPTY(801,"未请求到商品详情数据，请稍后再试"),
    GOODS_LIST_IS_EMPTY(802,"未请求到商品数据，请稍后再试"),
    GOODS_CONTENT_IS_NO(803,"搜索内容无法识别！"),
    GOODS_INFO_IS_NULL(804,"该商品已经下架！"),
    GOODS_SECKILL_EMPTY(805,"秒杀商品正在路上，请稍后再试"),
    GOODS_UNKNOWN_MARKET(806,"未知商品平台"),
    GOODS_SPEC_EMPTY(807,"请输入商品规格"),
    GOODS_ZERO_PURCHASE_FAIL(808,"您已购买零元购商品"),
    GOODS_ACTIVITY_EMPTY(809,"未查询到此主题活动"),


    /**
     * 后台管理
     */
    BACK_NOT_NEED_AUDIT(901,"无需审核！"),
    ADD_ERROR(200,"新增失败"),
    UPDATE_ERROR(200,"修改失败"),
    OPERATION_TOO_FAST(603,"操作太过频繁"),
    DEL_ERROR(200,"删除失败"),
    SELECT_ERROR(200,"查询失败"),
    END_TYPE_EMPTY(902,"结束时间不能为空"),
    FILE_CONTENT_ERROR(950,"文件内容为空"),
    FILE_TITLE_ERROR(951,"文件名为空"),
    FILE_TYPE_ERROR(952,"文件类型错误"),
    /**
     * 提现
     */
    TX_APPLYING(903,"您的提现正在申请中..."),
    TX_PUSH_MSG(905,"提现进度通知,请注意查收!"),
    HELPREDPACKET_SUCCESS(906,"您的助力红包已到账请注意查收。"),
    TX_REFUSE(907,"您的提现申请未能通过审核，具体原因可能有：1,可能存在恶意刷订单或恶意退款 2，可能存在恶意刷邀请赚收益。"),
    USER_MONEY_FORBID(908,"请注意，特殊人员不得提现的"),
    TX_STATUS_ERROR(909,"你的资产已被冻结，请联系客服！"),
    USER_MONEY_NOTENOUGHT(910,"申请失败,您的余额不足"),
    TX_APPLY_ILLEGAL(911,"申请失败,请稍后再试"),
    TX_MONEY_ERROR(912,"打款失败，提现金额不符"),
    TX_AUTOPAY_ERROR(913,"自动打款失败，请核实"),


    /**
     * 公告
     */
    NOTICE_TITLE_ISEMPTY(910,"公告标题不能为空"),
    SHARE_GROUP_IS_FINISH(912,"已分享完毕"),

    /**
     * 商学院
     * */
    BUSINESS_ACADEMY_DATA_ADD_ERROR(911,"商学院数据异常"),
    BUSINESS_ACADEMY_AUTHORITY_LACK(910,"会员权限不足"),

    /**
     * 消息
     */
    MESSAGE_COMMISSION_TYPE_EMPTY(950,"佣金类型不能为空"),
    MESSAGE_DATA_CONVERSION_FAILURE(951,"数据类型统一失败请联系管理员!"),
    DATE_TIME_ERROR(952,"时间格式转换失败"),


    /***
     * 支付
     * */
    PATTERN_OF_PAYMENT_EXCEPTION(1000,"支付方式错误"),
    OPERATION_FREQUENTly(1001,"支付频繁"),

    /**
     * 订单
     * */
    GOODS_ADD_ERROR(1010,"商品订单添加失败,请马上联系工作人员!"),
    GOODS_ORDER_ID_LOSE(1011,"商品订单ID丢失,请马上联系工作人员!"),
    GOODS_ORDER_LOSE(1012,"商品订单丢失,请马上联系工作人员!"),
    GOODS_ORDER_USER_ADDR_UPDATE_ERROR(1013,"用户收获地址信息变更失,请马上联系工作人员!"),
    GOODS_NUM_ERROR(1012,"仓库库存不足！购买失败"),

    /***
     * 卡多分
     * */
    CREATE_SYM_TOKEN(1030,"encrypt 生成失败"),


    /***
     * 精选核桃 订单
     * */
    SIFT_ORDER_ORDER_NO_REFUND(1050,"该订单不课退款"),
    SIFT_ORDER_ORDER_TIME_NO_REFUND(1051,"该订单超过七天无理由退款时间无法退款"),
    REFUND_ERROR(1052,"退款失败,请联系工作人员协同处理"),
    THIS_ORDER_TRUE_REFUND(1053,"该订单并没有处于申请中无法退款"),
    THIS_ORDER_PURE_IN_REBATE_LOG(1054,"该订单不可重复完成"),
    PARCELE_NUMBER_ERROR(1054,"快递单号错误请检查"),

    ;

    BizExceptionEnum(int code, String message) {
        this.friendlyCode = code;
        this.friendlyMsg = message;
    }


    private int friendlyCode;

    private String friendlyMsg;

    public int getCode() {
        return friendlyCode;
    }

    public void setCode(int code) {
        this.friendlyCode = code;
    }

    public String getMessage() {
        return friendlyMsg;
    }

    public void setMessage(String message) {
        this.friendlyMsg = message;
    }

}
