package com.drd.dt.exception;

import com.drd.dt.common.tips.Tip;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 全局的的异常拦截器（拦截所有的控制器）（带有@RequestMapping注解的方法上都会拦截）
 */
public abstract class BaseControllerExceptionHandler {

    /**
     * 拦截异常
     *
     * @author fengshuonan
     */
    @ExceptionHandler(Exception.class)
    //@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public abstract Tip handle(Exception e, HttpServletRequest request, HttpServletResponse response);

}
