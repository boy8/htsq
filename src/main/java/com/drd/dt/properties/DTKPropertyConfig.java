package com.drd.dt.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 大淘客属性配置
 */
@Configuration
@ConfigurationProperties(prefix = "dtk")
public class DTKPropertyConfig {
    private String appSecret;
    private String appKey;
    private String dateil;
    private String version;

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getDateil() {
        return dateil;
    }

    public void setDateil(String dateil) {
        this.dateil = dateil;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
