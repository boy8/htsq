package com.drd.dt.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 阿里云短信服务平台属性配置
 */
@Configuration
@ConfigurationProperties(prefix = "aly")
public class ALYPropertyConfig {
    private String name;
    private String regionFsid;
    private String regionJsid;
    private String accessKeyid;
    private String accessKeysecret;
    private String alySfyzid;
    private String alyDlqrid;
    private String alyDlycid;
    private String alyUhzcid;
    private String alyXgmmid;
    private String alyXxbgid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegionFsid() {
        return regionFsid;
    }

    public void setRegionFsid(String regionFsid) {
        this.regionFsid = regionFsid;
    }

    public String getRegionJsid() {
        return regionJsid;
    }

    public void setRegionJsid(String regionJsid) {
        this.regionJsid = regionJsid;
    }

    public String getAccessKeyid() {
        return accessKeyid;
    }

    public void setAccessKeyid(String accessKeyid) {
        this.accessKeyid = accessKeyid;
    }

    public String getAccessKeysecret() {
        return accessKeysecret;
    }

    public void setAccessKeysecret(String accessKeysecret) {
        this.accessKeysecret = accessKeysecret;
    }

    public String getAlySfyzid() {
        return alySfyzid;
    }

    public void setAlySfyzid(String alySfyzid) {
        this.alySfyzid = alySfyzid;
    }

    public String getAlyDlqrid() {
        return alyDlqrid;
    }

    public void setAlyDlqrid(String alyDlqrid) {
        this.alyDlqrid = alyDlqrid;
    }

    public String getAlyDlycid() {
        return alyDlycid;
    }

    public void setAlyDlycid(String alyDlycid) {
        this.alyDlycid = alyDlycid;
    }

    public String getAlyUhzcid() {
        return alyUhzcid;
    }

    public void setAlyUhzcid(String alyUhzcid) {
        this.alyUhzcid = alyUhzcid;
    }

    public String getAlyXgmmid() {
        return alyXgmmid;
    }

    public void setAlyXgmmid(String alyXgmmid) {
        this.alyXgmmid = alyXgmmid;
    }

    public String getAlyXxbgid() {
        return alyXxbgid;
    }

    public void setAlyXxbgid(String alyXxbgid) {
        this.alyXxbgid = alyXxbgid;
    }
}
