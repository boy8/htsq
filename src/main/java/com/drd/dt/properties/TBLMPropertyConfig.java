package com.drd.dt.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 淘宝联盟属性配置
 */
@Configuration
@ConfigurationProperties(prefix = "tblm")
public class TBLMPropertyConfig {
    private String name;
    private String url;
    private String appKey;
    private String appSecret;
    private String pId;
    private String chanaelId;
    private String klApikey;
    private String introduceUrl;
    private String shopget;
    private String infoget;
    private String chanaelId2;

    public String getChanaelId2() {
        return chanaelId2;
    }

    public void setChanaelId2(String chanaelId2) {
        this.chanaelId2 = chanaelId2;
    }

    public String getShopget() {
        return shopget;
    }

    public String getInfoget() {
        return infoget;
    }

    public void setInfoget(String infoget) {
        this.infoget = infoget;
    }

    public void setShopget(String shopget) {
        this.shopget = shopget;
    }

    public String getIntroduceUrl() {
        return introduceUrl;
    }

    public void setIntroduceUrl(String introduceUrl) {
        this.introduceUrl = introduceUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getChanaelId() {
        return chanaelId;
    }

    public void setChanaelId(String chanaelId) {
        this.chanaelId = chanaelId;
    }

    public String getKlApikey() {
        return klApikey;
    }

    public void setKlApikey(String klApikey) {
        this.klApikey = klApikey;
    }
}
