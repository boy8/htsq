package com.drd.dt.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 拼多多属性配置
 */
@Configuration
@ConfigurationProperties(prefix = "pdd")
public class PDDPropertyConfig {
    private String clientId;
    private String clientSecret;
    private String pid;
    private String channelId;
    private String pddsearchUrl;
    private String channelId2;

    public String getChannelId2() {
        return channelId2;
    }

    public void setChannelId2(String channelId2) {
        this.channelId2 = channelId2;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getPddsearchUrl() {
        return pddsearchUrl;
    }

    public void setPddsearchUrl(String pddsearchUrl) {
        this.pddsearchUrl = pddsearchUrl;
    }
}
