package com.drd.dt.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 京东联盟属性配置
 */
@Configuration
@ConfigurationProperties(prefix = "jdlm")
public class JDLMPropertyConfig {
    private String kpl_appKey;
    private String kpl_appSecret;
    private String jdlm_appKey;
    private String jdlm_appSecret;
    private String url;
    private String xuanpin;
    private String detailUrl;
    private String searchgoods;

    public String getKpl_appKey() {
        return kpl_appKey;
    }

    public void setKpl_appKey(String kpl_appKey) {
        this.kpl_appKey = kpl_appKey;
    }

    public String getKpl_appSecret() {
        return kpl_appSecret;
    }

    public void setKpl_appSecret(String kpl_appSecret) {
        this.kpl_appSecret = kpl_appSecret;
    }

    public String getJdlm_appKey() {
        return jdlm_appKey;
    }

    public void setJdlm_appKey(String jdlm_appKey) {
        this.jdlm_appKey = jdlm_appKey;
    }

    public String getJdlm_appSecret() {
        return jdlm_appSecret;
    }

    public void setJdlm_appSecret(String jdlm_appSecret) {
        this.jdlm_appSecret = jdlm_appSecret;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getXuanpin() {
        return xuanpin;
    }

    public void setXuanpin(String xuanpin) {
        this.xuanpin = xuanpin;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public String getSearchgoods() {
        return searchgoods;
    }

    public void setSearchgoods(String searchgoods) {
        this.searchgoods = searchgoods;
    }
}
