package com.drd.dt.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 黑马淘客属性配置
 */
@Configuration
@ConfigurationProperties(prefix = "hmtk")
public class HMTKPropertyConfig {
    private String appKey;
    private String appSecret;
    private String sId;
    private String gyzapi;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String getGyzapi() {
        return gyzapi;
    }

    public void setGyzapi(String gyzapi) {
        this.gyzapi = gyzapi;
    }
}
