package com.drd.dt.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 好单库属性配置
 */
@Configuration
@ConfigurationProperties(prefix = "hdk")
public class HDKPropertyConfig {
    private String apiKey;
    private String momentsUrl;
    private String hotUrl;
    private String trillUrl;
    private String detailUrl;
    private String ratesUrl;
    private String similarUrl;
    private String fastbuyUrl;
    private String brandUrl;
    private String deserveitemUrl;
    private String subjecthotUrl;
    private String saleslistUrl;
    private String columnUrl;
    private String supersearchUrl;
    private String keyworditemsUrl;
    private String itemlistUrl;

    public String getItemlistUrl() {
        return itemlistUrl;
    }

    public void setItemlistUrl(String itemlistUrl) {
        this.itemlistUrl = itemlistUrl;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getMomentsUrl() {
        return momentsUrl;
    }

    public void setMomentsUrl(String momentsUrl) {
        this.momentsUrl = momentsUrl;
    }

    public String getHotUrl() {
        return hotUrl;
    }

    public void setHotUrl(String hotUrl) {
        this.hotUrl = hotUrl;
    }

    public String getTrillUrl() {
        return trillUrl;
    }

    public void setTrillUrl(String trillUrl) {
        this.trillUrl = trillUrl;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public String getRatesUrl() {
        return ratesUrl;
    }

    public void setRatesUrl(String ratesUrl) {
        this.ratesUrl = ratesUrl;
    }

    public String getSimilarUrl() {
        return similarUrl;
    }

    public void setSimilarUrl(String similarUrl) {
        this.similarUrl = similarUrl;
    }

    public String getFastbuyUrl() {
        return fastbuyUrl;
    }

    public void setFastbuyUrl(String fastbuyUrl) {
        this.fastbuyUrl = fastbuyUrl;
    }

    public String getBrandUrl() {
        return brandUrl;
    }

    public void setBrandUrl(String brandUrl) {
        this.brandUrl = brandUrl;
    }

    public String getDeserveitemUrl() {
        return deserveitemUrl;
    }

    public void setDeserveitemUrl(String deserveitemUrl) {
        this.deserveitemUrl = deserveitemUrl;
    }

    public String getSubjecthotUrl() {
        return subjecthotUrl;
    }

    public void setSubjecthotUrl(String subjecthotUrl) {
        this.subjecthotUrl = subjecthotUrl;
    }

    public String getSaleslistUrl() {
        return saleslistUrl;
    }

    public void setSaleslistUrl(String saleslistUrl) {
        this.saleslistUrl = saleslistUrl;
    }

    public String getColumnUrl() {
        return columnUrl;
    }

    public void setColumnUrl(String columnUrl) {
        this.columnUrl = columnUrl;
    }

    public String getSupersearchUrl() {
        return supersearchUrl;
    }

    public void setSupersearchUrl(String supersearchUrl) {
        this.supersearchUrl = supersearchUrl;
    }

    public String getKeyworditemsUrl() {
        return keyworditemsUrl;
    }

    public void setKeyworditemsUrl(String keyworditemsUrl) {
        this.keyworditemsUrl = keyworditemsUrl;
    }
}
