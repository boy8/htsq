package com.drd.dt.util;

import com.drd.dt.modular.dao.CommonMapper;
import com.drd.dt.modular.dao.UserMapper;
import com.drd.dt.modular.dao.UserMoneyMapper;
import com.drd.dt.modular.dto.GoodsDetailDTO;
import com.drd.dt.modular.entity.Order;
import com.drd.dt.modular.entity.User;
import com.drd.dt.modular.service.impl.UserMoneyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 86514 on 2019/11/4.
 */
@Component
public class FilterGoodsUtil {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CommonMapper commonMapper;
    @Autowired
    private UserMoneyMapper userMoneyMapper;
    @Autowired
    private UserMoneyServiceImpl testMoneyService;

    /**
     * 新方法 订单结算返佣
     * @param order_id
     * @param money
     * @param goods_id
     * @param clear_time
     * @param zeroMoney
     */
    public void rakeMoneyNew(String order_id, Double money, String goods_id, String clear_time,Double zeroMoney) {
        Order order = commonMapper.getOrderInfo(order_id);
        if (null != order) {
            Map mapuser = new HashMap();
            int userid = order.getUser_id();
            mapuser.put("id",userid);
            User user = userMoneyMapper.getByUserId(mapuser);
            if (null == user){
                return;
            }
            String rakeBake = order.getRake_back();
            String user_rake_back_yugu = order.getUser_rake_back_yugu();
            Integer buytype = order.getBuytype();
            if (StringUtils.isEmpty(rakeBake)){
                order.setRake_back("0");
            }
            if (StringUtils.isEmpty(user_rake_back_yugu)){
                order.setUser_rake_back_yugu("0");
            }
            if (null == buytype){
                order.setBuytype(0);
            }
            double user_rake = testMoneyService.getRakeBackRatebyUser(user);//返佣比例
            Double user_rake_back = user_rake * money + zeroMoney;//零元购商品价钱直接返佣

            //更新订单状态
            Map map = new HashMap();
            map.put("product_id", goods_id);
            map.put("clear_time", clear_time);
            map.put("rake_back", money);
            map.put("status", 1);
            map.put("order_id", order_id);
            map.put("user_rake_back", String.valueOf(user_rake_back));
            map.put("user_rake", user_rake);
            commonMapper.updateOrder(map);//更新order表  返佣
            order.setStatus(1);//结算状态
            testMoneyService.updateOrderFansRake(user,order);
        }
    }


    /**
     * ios过审版本过滤
     */
    public GoodsDetailDTO filterIOSVersion(GoodsDetailDTO dto, HttpServletRequest request, String app_version) throws Exception {
        //判断是否为国外ip
        String ip = StringUtil.getIpAddr(request);
        String country = new StringUtil().getIPLocation(ip);
        String version = userMapper.getVerifyVersion();//获取ios审核版本号
        if (!app_version.equals(version) && !country.equals("美国")) {//ios 审核版过滤
            dto.setSaber(0);
        }
        return dto;
    }

    public String vipDesc(Double upcommission,Integer id){
        String desc = "";
        if (!StringUtils.isEmpty(upcommission) && !StringUtils.isEmpty(id)){
            String commission = String.format("%.2f",upcommission);
            String user_level = userMapper.getUserLevel(id);
            if (!StringUtils.isEmpty(user_level)){
                Integer level = Integer.valueOf(user_level);
                if (level == 0) {
                    desc = "升级超级会员返"+commission+"元";
                }else if (level == 1){
                    desc = "升级运营商返"+commission+"元";
                }else if (level == 2){
                    desc = "升级联合创始人返"+commission+"元";
                }
            }
        }else if (!StringUtils.isEmpty(upcommission) && StringUtils.isEmpty(id)){
            String commission = String.format("%.2f",upcommission);
            desc = "升级超级会员返"+commission+"元";
        }
        return desc;
    }
}
