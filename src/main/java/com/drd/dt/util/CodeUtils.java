package com.drd.dt.util;

public class CodeUtils {
    /**
     * 自定义进制(0,1没有加入,容易与o,l混淆)
     */

    private static final char[] r = new char[]{'F', 'L', 'G', 'W', 'X', 'C', 'Z', 'M', 'Y', 'R', 'T', 'H', 'S', 'D', 'V', 'E', 'J', 'K', 'Q', 'P', 'U', 'A', 'N', 'B'};


    /**
     * 进制长度
     */

    private static final int binLen = r.length;

    //private static Logger logger = Logger.getLogger(ShareCodeUtil.class);

    //     private static final long startNumber = 1048576L;

    private static final long startNumber = 7876000;

    /**
     * @param id ID
     * @return 随机码
     */

    public static String idToCode(long id, long costomStartNumber) {

        if (costomStartNumber < 0) {

            costomStartNumber = startNumber;

        }

        id += costomStartNumber;

        char[] buf = new char[32];

        int charPos = 32;

        while ((id / binLen) > 0) {

            int ind = (int) (id % binLen);

            // System.out.println(num + "-->" + ind);

            buf[--charPos] = r[ind];

            id /= binLen;

        }

        buf[--charPos] = r[(int) (id % binLen)];

        //         System.out.println(num + "-->" + num % binLen);

        String str = new String(buf, charPos, (32 - charPos));

        return str.toUpperCase();

    }

    public static int codeToId(String code) {

        code = code.toUpperCase();

        char chs[] = code.toCharArray();

        int res = 0;

        for (int i = 0; i < chs.length; i++) {

            int ind = 0;

            for (int j = 0; j < binLen; j++) {

                if (chs[i] == r[j]) {

                    ind = j;

                    break;

                }

            }

            if (i > 0) {

                res = res * binLen + ind;

            } else {

                res = ind;

            }

            //           System.out.println(ind + "-->" + res);

        }

        res -= startNumber;

        return res;

    }

    public static String toSerialCode(long idL) {

        return idToCode(idL, -1L);

    }

    public static String idToCode(String id) {

        long idL = Long.parseLong(id);

        return idToCode(idL, -1L);

    }

    public static String idToCode(String id, long costomStartNumber) {

        long idL = Long.parseLong(id);

        return idToCode(idL, costomStartNumber);

    }
}
