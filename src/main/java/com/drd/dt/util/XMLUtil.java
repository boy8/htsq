package com.drd.dt.util;


import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.*;

/**
 * Created by 86514 on 2019/10/30.
 */
public class XMLUtil {
    /**
     * xml格式字符串与map集合之间互相转换的工具
     */

    //map集合转xml字符串
    public static String toXml(Map<String, Object> params) {
        StringBuffer xml = new StringBuffer();
//        xml.append("<?xml version='1.0' encoding='ISO8859-1' standalone='yes' ?><xml>");
        xml.append("<xml>");
        ArrayList<String> arr = new ArrayList<>();
        for (String key : params.keySet()) {
            if (params.get(key) != null && !params.get(key).equals("")) {
                arr.add(key);
            }
        }
        Collections.sort(arr);
        String sing = "";
        for (int i = 0; i < arr.size(); i++) {
            String k = arr.get(i);
            if (params.get(k) != null) {
                String v = params.get(k).toString();
//                xml.append("<" + k + ">" + "<![CDATA[" + v + "]]></" + k + ">");
                if(k.equals("sign")){
                    sing = "<" + k + ">" + v + "</" + k + ">";
                    continue;
                }
                xml.append("<" + k + "><![CDATA[" + v + "]]></" + k + ">");
            }
        }
        if(!sing.equals("")){
            xml.append(sing);
        }
        xml.append("</xml>");
//        String xml2 = "";
        //            xml2 = new String(xml.toString().getBytes(), "UTF-8");
        String xml2 = xml.toString();
        return xml2;
    }

    //map集合转xml字符串 提示微信退款使用
    public static String toXml2(Map<String, Object> params) {
        StringBuffer xml = new StringBuffer();
        xml.append("<xml>");
        ArrayList<String> arr = new ArrayList<>();
        for (String key : params.keySet()) {
            if (params.get(key) != null && !params.get(key).equals("")) {
                arr.add(key);
            }
        }
        Collections.sort(arr);
        String sing = "";
        for (int i = 0; i < arr.size(); i++) {
            String k = arr.get(i);
            if (params.get(k) != null) {
                String v = params.get(k).toString();
                if(k.equals("sign")){
                    sing = "<" + k + ">" + v + "</" + k + ">";
                    continue;
                }
                xml.append("<" + k + ">" + v + "</" + k + ">");
            }
        }
        if(!sing.equals("")){
            xml.append(sing);
        }
        xml.append("</xml>");
        String xml2 = xml.toString();
        return xml2;
    }

    //xml字符串转map集合
    public static Map<String, Object> toMap(String xml) {
        Map<String, Object> result = new HashMap<>();
        if (xml.equals("")) {
            return result;
        }
        try {
            StringReader read = new StringReader(xml);
            InputSource source = new InputSource(read);
            SAXBuilder sb = new SAXBuilder();

            Document doc = (Document) sb.build(source);
            Element root = doc.getRootElement();
            result.put(root.getName(), root.getText());
            result = parse(root, result);
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        result.remove("xml");
        return result;
    }

    private static Map<String, Object> parse(Element root,
                                             Map<String, Object> result) {
        List<Element> nodes = root.getChildren();
        int len = nodes.size();
        if (len == 0) {
            result.put(root.getName(), root.getText());
        } else {
            for (int i = 0; i < len; i++) {
                Element element = (Element) nodes.get(i);// 循环依次得到子元素
                result.put(element.getName(), element.getText());
                // parse(element,result);
            }
        }
        return result;
    }
}
