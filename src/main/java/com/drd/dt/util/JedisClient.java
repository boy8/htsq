package com.drd.dt.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Component
public class JedisClient {

    private Logger logger = LoggerFactory.getLogger(JedisClient.class);

    @Autowired
    private JedisPool jedisPool;

    public String set(String key,String value){
        Jedis jedis = null;
        try{
           jedis = jedisPool.getResource();
           return jedis.set(key,value);
        } catch (Exception e){
            logger.error("Redis保存"+key+"异常"+e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    public Long setnx(String key, String value, int second){
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            Long ret = jedis.setnx(key,value);
            jedis.expire(key, second);
            return ret;
        } catch (Exception e){
            logger.error("Redis保存"+key+"异常"+e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }
    
    public void setWithExpire(String key,String value,int second){
        this.set(key,value);
        this.expire(key, second);
    }

    public boolean exists(String key) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            return jedis.exists(key);
        } catch (Exception e) {
            logger.error("Redis exists"+key+"异常"+e.getMessage());
        } finally {
            //返还到连接池
            jedis.close();
        }
        return false;
    }
    
    public String get(String key){
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            return jedis.get(key);
        } catch (Exception e){
            logger.error("Redis查询"+key+"异常"+e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    public long del(String key){
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            return jedis.del(key);
        } catch (Exception e){
            logger.error("Redis删除"+key+"异常"+e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return 0;
    }

    public long expire(String key,int second){
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            return jedis.expire(key,second);
        } catch (Exception e){
            logger.error("Redis设置"+key+"超时时间异常"+e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return 0;
    }
}
