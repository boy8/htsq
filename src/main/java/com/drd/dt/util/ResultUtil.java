package com.drd.dt.util;

import com.baomidou.mybatisplus.plugins.Page;
import com.drd.dt.common.tips.Tip;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author th
 * Created on 2019/10/27.
 */
public class ResultUtil {

    public static Tip result(int code, String msg, Object... objs){
        Tip tip = new Tip();
        tip.setCode(code);
        tip.setMessage(msg);
        if (objs.length == 0){
            tip.setData("");
        }else{
            Map<String, Object> resultMap = new HashMap<String,Object>();
            Object obj = objs[0];
            if (obj instanceof List){
                List list = (List) obj;
                tip.setData(list);
            }else if (obj instanceof Page){
                Page page = (Page)obj;
                resultMap.put("datas", page.getRecords());
                resultMap.put("totalCounts", page.getTotal());
                tip.setData(resultMap);
            }else{
                tip.setData(obj);
            }
        }
        return tip;
    }
}
